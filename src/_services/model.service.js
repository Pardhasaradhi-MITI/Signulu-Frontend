export const FLAG_SELECTOR_OPTION_LIST= [
        {
          id: "1",
          name: "English",
          displayText: "English-EN",
          englishName: 'United States',
          locale: "en",
          flag: ''
        },
        {
            id: "966",
            name: "Arabic",
            displayText: "Arabic-AR",
            englishName: 'Arabic',
            locale: "ar",
            flag: ''
          },
          {
            id: "91",
            name: "Hindi",
            displayText: "Hindi-HD",
            englishName: 'Hindi',
            locale: "hn",
            flag: ''
          },
          {
            id: "86",
            name: "French",
            displayText: "French-FR",
            englishName: 'France',
            locale: "fr",
            flag: ''
          }
    ]
    export const data = [
        {
            id: "86",
            name: "French",
            displayText: "French-FR",
            englishName: 'France',
            locale: "fr",
            flag: ''
          }
        ]
        export const addDocStep2Options= {
          responsive: {
              0: {
                  items: 4,
                  margin: 0
              },
              599: {
                  items: 4,
                  margin: 0
              },
              1000: {
                  items: 4,
                  margin: 0
              },
              1280: {
                  items: 4,
                  margin: 0
              },
          },
      };
      export const addDocStep3Options= {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };
    export function setSessionItem(type, data) {
      sessionStorage.setItem(type, JSON.stringify(data))
    }
    export function getSessionItem(type) {
     return sessionStorage.getItem(type)
    }
    export function removeSessionItem(type) {
      sessionStorage.removeItem(type)
    }

    export function generateUQID(ctrlCode, length, returnType) {
      // tslint:disable-next-line: new-parens
      const timestamp = +new Date;
      const ts = timestamp.toString();
      const parts = ts.split('').reverse();
      let id = '';
  
      for (let i = 0; i < length; ++i) {
        const index = Math.floor(Math.random() * (parts.length - 1 - 0 + 1)) + 0
        id += parts[index];
      }
  
      if (returnType === 'int') {
        return id;
      } else {
        return ctrlCode + '_' + id;
      }
  
    }
    export function GenerateOTP() {
      const digits = '123456789';
      const otpLength = 4;
      let otp = '';
      for (let i = 1; i <= otpLength; i++) {
          const index = Math.floor(Math.random() * (digits.length));
          otp = otp + digits[index];
      }
      if (otp.length < 4) { GenerateOTP(); }
      return otp;
  }
    export const  colorTypes = [
      "att-ad-recipient-pure-blue",
      "att-ad-recipient-pure-yellow",
      "att-ad-recipient-pure-green",
      "att-ad-recipient-pure-pink",
      "att-ad-recipient-pure-black",
      "att-ad-recipient-pure-purple",
      "att-ad-recipient-pure-orange",
      "att-ad-recip-pure-oliveblue",
      "att-ad-recipient-pure-violet",
      "att-ad-recip-pure-violetred",
    ]
      // "att-ad-recipient-medium-green",
      // "att-ad-recipient-medium-purple",
      // "att-ad-recipient-caribbean-green",
      // "att-ad-recipient-gray",
      // "att-ad-recipient-light-pink",
      // "att-ad-recipient-yellow",
      // "att-ad-recipient-light-green",
      // "att-ad-recipient-light-gold",
      // "att-ad-recipient-light-orange",
      // "att-ad-recipient-chetwode-blue-border",
      // "att-ad-recipient-lusty-border",
      // "att-ad-recipient-atomic-tangerine-border",
      // "att-ad-recipient-geraldine-border",
      // "att-ad-recipient-tyrian-purple-border",
      // "att-ad-recipient-golden-yellow-border",
      // "att-ad-recipient-deep-sky-blue-border",
      // "att-ad-recipient-magic-mint-border",
      // "att-ad-recipient-hacienda-border"
      export  function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
          byteString = atob(dataURI.split(',')[1]);
        }
        else {
          byteString = unescape(dataURI.split(',')[1]);
        }
        // separate out the mime component
        const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], { type: mimeString });
      }

      export const States = [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "District Of Columbia",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming"
    ]