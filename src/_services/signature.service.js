import { BehaviorSubject } from 'rxjs';
import config from 'config';
import { fetchWrapper, history } from '@/_helpers';
import {authService} from "./auth.service";
// const baseUrl = `${config.apiUrl}/settings`;
// const baseUrl = `https://sandbox.signulu.com/index1.php/api/settings`;


export const SignatureService = {
	update,getSing
}

function update(data) {
	return authService.post(`settings/update`, data);
}

function getSing(data) {
	return authService.get(`settings/user/signature/`+ data);
}