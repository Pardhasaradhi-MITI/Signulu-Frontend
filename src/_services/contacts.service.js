import {authService} from "./auth.service";

export const contactsService = {
    contactList:getContactList,
    addContact:addContact,
    editContact:editContact,
    getContactTypes:getContactTypes,
    deleteContact:deleteContact,
    uploadContacts:uploadContacts,
    checkEmailExist:checkEmailExist
}
function getContactList(body) {
    return authService.post('contacts/list',body).then(res => {
        return res.Entity;
    });
}
function addContact(body) {
    return authService.post('contacts/add',body).then(res => {
        return res;
    });
}

function editContact(id,body) {
    return authService.put('contacts/update/'+id,body).then(res => {
        return res;
    });
}
function getContactTypes(body) {
    return authService.post('contacts/contacttypes',body).then(res => {
        return res.Entity;
    });
}

function deleteContact(id) {
    return authService.delete('contacts/'+id).then(res => {
        return res;
    });
}

function uploadContacts(file) {
    let formData = new FormData();
    if (file) {
        formData.append('uploads[]', file)
    } else {
        formData.append('uploads[]', '')
    }
    return authService.PostAxios(`contacts/import`,formData)
        .then(res => {
            return res.data;
        });
}

function checkEmailExist(email) {
    return authService.get('contacts/checkmail/'+email).then(res => {
        return res.Entity;
    });
}
