import { constant } from "lodash";
import {authService} from "../_services/auth.service";

export const manageservice={
    getdoclist:getdoclist,
    deletedocument:deletedocument,
    Updatemanagevalidity:Updatemanagevalidity,
    getRecipientsbasedondoc:getRecipientsbasedondoc,
    Senddocumenttorecipients:Senddocumenttorecipients,
    Revokedocument:Revokedocument,
    CreateTemplate:CreateTemplate,
    getdoctotalcount:getdoctotalcount,
    Resendotp:Resendotp,
    getaudittrailinfo:getaudittrailinfo,
    ShareDocument:ShareDocument,
    gettemplateCategorys:gettemplateCategorys,
    getgoogledrivedata:getgoogledrivedata,
    getgoogledrivechilddata:getgoogledrivechilddata,
    getdropboxdrivedata:getdropboxdrivedata,
    getdropboxdrivechilddata:getdropboxdrivechilddata,
    savetodropbox:savetodropbox,
    savetoGoogledrive:savetoGoogledrive,
    getonedrivedata:getonedrivedata,
    getonedrivechilddata:getonedrivechilddata,
    savetoonedrive:savetoonedrive,
    getDocumentData:getDocumentData,
    approveDocument:approveDocument,
    downloadDocument:downloadDocument,
    downloadUnauthDocument:downloadUnauthDocument,
    approveDocumentAadhar:approveDocumentAadhar
}

function getdoclist(data){
    return authService.post('adocuments/list',data).then(res=>{
          return res;
    })
}
function deletedocument(data){
    return authService.put('adocuments/delete/multiple',data)
}
function Updatemanagevalidity(data,id){
    return authService.put(`adocuments/extend/duration/${id}`,data)
}
function getRecipientsbasedondoc(id){
    return authService.get(`adocuments/recipients/${id}`)
}
function Senddocumenttorecipients(data){
    return authService.post(`adocuments/send`,data)
}
function Revokedocument(data){
    return authService.put(`adocuments/revoke/${data.ADocumentId}`,data)
}
function CreateTemplate(data){
    return authService.put(`adocuments/template/create/${data.ADocumnetId}`,data)
}
function getdoctotalcount(){
    return authService.get(`adocuments/record/status`)
}
function Resendotp(ADocumentId){
    return authService.get(`adocuments/resendsms/${ADocumentId}`)
}
function getaudittrailinfo(ADocumentId){
    return authService.get(`adocuments/audittrial/info/${ADocumentId}`)
}
function ShareDocument(payload1){
    return authService.put(`adocuments/share/${payload1.ADocumentId}`,payload1)
} 
function gettemplateCategorys(){
    return authService.get(`templatecategory/list`)
}
function getgoogledrivedata(){
    return authService.get(`google/contents`)
}
function getgoogledrivechilddata(id){
    return authService.get(`google/contents/${id}`)
}
function getdropboxdrivedata(){
    return authService.get(`dropbox/contents`)
}
function getdropboxdrivechilddata(id){
    return authService.get(`dropbox/contents/${id}`)
}
function getonedrivedata(){
    return authService.get(`onedrive/contents`)
}
function getonedrivechilddata(id){
    return authService.get(`onedrive/contents/${id}`)
}

function savetodropbox(payload){
    return authService.post(`dropbox/upload`,payload)

}
function savetoGoogledrive(payload){
    return authService.post(`google/upload`,payload)

}
function savetoonedrive(payload){
    return authService.post(`onedrive/upload`,payload)
}

function getDocumentData(id , loader){
    return authService.get('adocuments/'+id , loader)
        .then(res => {
            return res;
        })
}
function approveDocument(id, recipient, data){
    return authService.post(`adocuments/saveandapprove/${id}/${recipient}`,data)
    .then(res => {
        return res;
    })
}
function downloadDocument(docId) {
    return authService.getFile('adocuments/download/design/pdf/'+docId)
    .then(res => {
        return res;
    })
}
function downloadUnauthDocument(docId, PID) {
    return authService.getFile(`adocuments/unauth/download/design/pdf/${docId}/${PID}`)
    .then(res => {
        return res;
    })
}
function approveDocumentAadhar(id, recipient, data){
    return authService.sandboxAxiouspostAadhar(`adocuments/saveandapprove/${id}/${recipient}`,data)
    .then(res => {
        return res;
    })
}
