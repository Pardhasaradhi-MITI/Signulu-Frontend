import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
import { authService } from './auth.service';
import config from 'config';
const fileEndPoint = config.fileServerUrl
import React from 'react';
import ReactTooltip from "react-tooltip";

const document = new BehaviorSubject(null);
const templatedocument = new BehaviorSubject(null);
const docauthdocument = new BehaviorSubject(null);
const cloudDocuments = new BehaviorSubject(null);
const userInfoObj = new BehaviorSubject(null);
export const commonService = {
    userInfo,
    userTrailDays,
    setDocument,
    getImageUrl,
    convertFileToPDF,
    getfileData,
    toolTipFunction,
    settemplateDocument,
    setDocauthDocument,
    setCloudDocuments,
    getFaceComapreMatch,
    getFaceSubmit,
    getFeedBackTypes,
    feedBack,
    getFacedoubleComapreMatch,
    document: document.asObservable(),
    get documentValue () { return document.value },
    templatedocument: templatedocument.asObservable(),
    get templatedocumentValue () { return templatedocument.value },
    docauthdocument: docauthdocument.asObservable(),
    get docauthdocumentValue () { return docauthdocument.value },
    userInfoObj: userInfoObj.asObservable(),
    get userInfoObj () { return userInfoObj.value },
    cloudDocuments: cloudDocuments.asObservable(),
    get cloudDocuments () { return cloudDocuments.value },
    activatemailuserInfo,
    activatemailuserTrailDays
};

function toolTipFunction(value, position, effect) {
    return (
        <ReactTooltip id="registerTip" place={position} effect={effect}>
            {value}
        </ReactTooltip>
    )
}
function setDocument(data) {
    document.next(data);
}
function settemplateDocument(data) {
    templatedocument.next(data);
}
function setDocauthDocument(data) {
    docauthdocument.next(data);
}
function setCloudDocuments(data) {
    cloudDocuments.next(data);
}

function userInfo() {
    return authService.get(`users/current/userInfo` , true)
        .then(res => {
            console.log(res);
            userInfoObj.next(res.Data)
            return res;
        });
}
function userTrailDays() {
    return authService.get(`users/free/trial/days` ,true)
        .then(res => {
            return res;
        },error=> {
            console.log(error, '')
        });
}

function activatemailuserInfo() {
    return authService.verificationMail(`users/current/userInfo`)
        .then(res => {
            userInfoObj.next(res.Data)
            return res;
        });
}
function activatemailuserTrailDays() {
    return authService.verificationMail(`users/free/trial/days`)
        .then(res => {
            return res;
        },error=> {
            console.log(error, '')
        });
}
function convertFileToPDF(objdata, file) {
    let formData = new FormData();
    formData.append('model', JSON.stringify(objdata))
    formData.append('Document[]', file)
    console.log(formData, 'formDataformDataformData')
    return authService.PostAxios1(`adocuments/convert/document/autodetectpdf`,formData)
        .then(res => {
            return res.data;
        });
}
function getfileData(api){
    return authService.getFile('adocuments/download/'+ api)
    .then(res => {
        return res;
    });
}
function getImageUrl() {
   return config.fileServerUrl
}
function getUserPreferences(userId){
    return authService.get('settings/user/dateformat/'+ userId)
    .then(res => {
        return res;
    });
}
function getFaceComapreMatch(blob, filePath, domain){
    let formData = new FormData();
    formData.append('canvasImage', blob, "inoutFile")
    formData.append('fileName', filePath)
    formData.append('domain', domain)
    console.log(formData, 'formDataformDataformData')
    return authService.facialPost(`face-match/face-compare-match`,formData)
        .then(res => {
            return res.data;
        });
}
function getFacedoubleComapreMatch(body){
    console.log(body, 'formDataformDataformData')
    return authService.unauthPost(`adocuments/unauth/face/detect`,body)
        .then(res => {
            return res;
        });
}
function getFaceSubmit(object){
    return authService.unauthPost(`users/unauth/photo/capture`,object)
        .then(res => {
            return res.data;
        });
}
function feedBack(object){
    return authService.PostAxios(`users/saveandsend/feedback`,object)
        .then(res => {
            return res.data;
        });
}
function getFeedBackTypes(){
    return authService.get('users/feedback/types')
    .then(res => {
        return res;
    });
}



