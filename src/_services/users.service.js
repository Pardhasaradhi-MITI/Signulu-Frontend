import {authService} from "./auth.service";

export const usersService = {
    userList:getUserList,
    roleList:getRoleList,
    addUser:addUser,
    getUserById:getUserById,
    editUser:editUser,
    getCurrentUserData:getCurrentUserData,
    getCurrentprofile:getCurrentprofile,
    updateCurrentprofile:updateCurrentprofile,
    getCompanyDetails:getCompanyDetails,
    // getContactTypes:getContactTypes,
    // deleteContact:deleteContact,
    // uploadContacts:uploadContacts,
    checkEmailExist:checkEmailExist,
	resetPassword:resetPassword,
    updateCompanyProfile:updateCompanyProfile,
    getSettingList:getSettingList,
    getSettingUser:getSettingUser,
    updateUserSetting:updateUserSetting,
    getCurrentUserclodu:getCurrentUserclodu,
    getUserLimit:getUserLimit,
    getCompanyPackageCode:getCompanyPackageCode,
    getUserDataCurrent:getUserDataCurrent
}
function getUserList(body) {

    return authService.post('users/list',body).then(res => {
        return res.Entity;
    });
}

function getRoleList() {
    return authService.post('users/company/roles',{}).then(res => {
        return res.Entity;
    });
}
function addUser(file,body) {
    
    let formData = new FormData();
    formData.append('model', JSON.stringify(body))
    if (file) {
        formData.append('ProfilePic[]', file)
    } else {
        formData.append('ProfilePic[]', '')
    }
    return authService.PostAxios(`users/add`,formData)
        .then(res => {
            return res.data;
        });
}
function checkEmailExist(email) {
    return authService.get('users/usersearch/'+email).then(res => {
        return res;
    });
}
function getUserById(id) {
    return authService.get('users/'+id).then(res => {
        return res.Entity;
    });
}
function editUser(id,body) {
    return authService.put('users/update/'+id,body).then(res => {
        return res;
    });
}

function getCurrentUserData() {
    return authService.get('users/current/userInfo',true).then(res => {
        return res;
    });
}
function getUserDataCurrent() {
    return authService.get('users/current/userData' , true).then(res => {
        return res;
    });
}
function getCurrentUserclodu() {
    return authService.get('users/cloud/storage/list').then(res => {
        return res;
    });
}

function getUserLimit(companyId) {
    return authService.get('users/subscription/limit/+'+companyId).then(res => {
        return res;
    });
}


function getCurrentprofile() {
    return authService.get('users/profile').then(res => {
        return res;
    });
}

function getCompanyDetails() {
    return authService.get('company/profile').then(res => {
        return res;
    });
}

function getCompanyPackageCode() {
    return authService.get('adocuments/limit/count').then(res => {
        return res;
    });
}


function getSettingList() {
    return authService.get('settings/list').then(res => {
        return res;
    });
}


function getSettingUser() {
    return authService.get('settings/user').then(res => {
        return res;
    });
}function updateUserSetting(body) {
    return authService.post('settings/update',body).then(res => {
        return res;
    });
}

function updateCompanyProfile(body) {
    return authService.post('company/profile',body).then(res => {
        return res;
    });
}





function updateCurrentprofile(body) {
    return authService.put('users/profile/update',body).then(res => {
        return res;
    });
}

function addSine(file,body) {
    let formData = new FormData();
    formData.append('model', JSON.stringify(body))
    if (file) {
        formData.append('ProfilePic[]', file)
    } else {
        formData.append('ProfilePic[]', '')
    }
    return authService.PostAxios(`settings/update `,formData)
        .then(res => {
            return res.data;
        });
}

//
// function editContact(id,body) {
//     return authService.put('contacts/update/'+id,body).then(res => {
//         return res;
//     });
// }
// function getContactTypes(body) {
//     return authService.post('contacts/contacttypes',body).then(res => {
//         return res.Entity;
//     });
// }
//
// function deleteContact(id) {
//     return authService.delete('contacts/'+id).then(res => {
//         return res;
//     });
// }
//
// function uploadContacts(file) {
//     let formData = new FormData();
//     if (file) {
//         formData.append('uploads[]', file)
//     } else {
//         formData.append('uploads[]', '')
//     }
//     return authService.PostAxios(`contacts/import`,formData)
//         .then(res => {
//             return res.data;
//         });
// }
//
// function checkEmailExist(email) {
//     return authService.get('contacts/checkmail/'+email).then(res => {
//         return res.Entity;
//     });
// }

function resetPassword(id) {
	return authService.get('users/resetpassword/'+id).then(res => {
		return res.Entity;
	});
}
