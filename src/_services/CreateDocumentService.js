import {authService} from "../_services/auth.service";

export const createdocumentservice={
    createDocument:createDocument,
    UpdateDocument:UpdateDocument,
}
function createDocument(data){
    return authService.postbulksend('adocuments/create/document',data)
}
function UpdateDocument(data){
    return authService.postbulksend('adocuments/scratch/update',data)
}