import {authService} from "./auth.service";

export const billingService = {
    billingList:getBillingList,
    billById:getBillById,
    billingHistory:getBillingHistory,
    getBillStatistics:getBillStatistics
}
function getBillingList(body) {
    return authService.post('billing/list ',body).then(res => {
        return res.Entity;
    });
}
function getBillById(id) {
    return authService.get('billing/bill/'+id).then(res => {
        return res.Entity;
    });
}
function getBillingHistory() {
    return authService.post('billing/payment/history',{}).then(res => {
        return res.Entity;
    });
}

function getBillStatistics(id) {
    return authService.get('users/bill/statistics').then(res => {
        return res;
    });
}

