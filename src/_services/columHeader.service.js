export const columnHeaderService = {
    documentSelectContactHeaders
}

function documentSelectContactHeaders() {
    return [{ header: 'Name', field: 'ContactName', width: '14rem',minwidth: '14rem', maxWidth: '', filter: '' },
    { header: 'Email', field: 'EmailAddress',  width: '14rem', minwidth: '14rem', maxWidth: '', filter: '' },
    { header: 'Contact Type', field: 'ContactType', width: '14rem', minwidth: '14rem', maxWidth: '', filter: '' },
    { header: 'Company', field: 'Company',  width: '14rem',minwidth: '14rem', maxWidth: '', filter: '' }]
}