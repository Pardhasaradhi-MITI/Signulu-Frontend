import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import { authService } from './auth.service';
const user = new BehaviorSubject(null);
export const loginService = {
    login,
    update_currentUser: update_currentUser,
    storeLoginTokenKey: storeLoginTokenKey,
    storeVerifyMailTokenKey: storeVerifyMailTokenKey,
    sessionLoginToSignulu: sessionLoginToSignulu,
    resendVerficationEmail: resendVerficationEmail,
    logout:logout,
    signUp:signUp,
    getCountryCodes:getCountryCodes,
    credentialCryptedString:credentialCryptedString,
    deCryptedString:deCryptedString,
    sendOPT:sendOPT,
    verifyOTP:verifyOTP,
    update_subject_user:update_subject_user,
    user: user.asObservable(),
    get userValue () { return user.value },
    getMessage: () => user.asObservable()

};

function update_currentUser(data) {
    localStorage.setItem('UserObj', JSON.stringify(data));
}
function update_subject_user(data) {
    user.next(data);
}
function storeLoginTokenKey(token) {
    localStorage.removeItem('LoginToken');
    localStorage.setItem('LoginToken', token);
}
function storeVerifyMailTokenKey(token) {
    localStorage.removeItem('verifyMailToken');
    localStorage.setItem('verifyMailToken', token);
}
function cryptedString(string) {
    let ckey = CryptoJS.enc.Utf8.parse('1234567890123456');
    let encrypt = CryptoJS.AES.encrypt(string, ckey, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    })
    return encrypt.ciphertext.toString()

}
function credentialCryptedString(string) {
return  CryptoJS.AES.encrypt(string, '1234567890123456').toString();
}
function deCryptedString(string) {
    var bytes  = CryptoJS.AES.decrypt(string, '1234567890123456');
    return bytes.toString(CryptoJS.enc.Utf8);
}

function login(email, pw,) {
    const password = cryptedString(pw)
    let data = new FormData();
    data.append('Email', email);
    data.append('Password', password);
    data.append('IsReact',1)
    document.body.classList.add('loading-indicator');

    return authService.loginPostAxios(`users/login`, data)
        .then(res => {
            document.body.classList.remove('loading-indicator');
            // publish user to subscribers and start timer to refresh token
            // userSubject.next(user);
            // startRefreshTokenTimer();
            return res.data;
        });
}
function sessionLoginToSignulu(email, pw) {
    const password = cryptedString(pw)
    let data = new FormData();
    data.append('Email', email);
    data.append('Password', password);
    data.append('ContinueLoginStatus', '1');
    return authService.loginPostAxios(`users/login`, data)
        .then(res => {
            return res.data;
        });
}
function resendVerficationEmail(data) {
    return authService.VerficationEmailPost(`users/resendverficationemail`, data)
        .then(res => {
            return res.data;
        });
}

function logout(user_id,first_name, last_name) {
    document.body.classList.add('loading-indicator');
    let data = new FormData();
    data.append('UserId', user_id);
    data.append('FirstName', first_name);
    data.append('LastName', last_name);
    return authService.loginPostAxios(`users/logout`, data)
    .then(res => {
        localStorage.removeItem('LoginToken');
        document.body.classList.remove('loading-indicator');
        return res.data;
    });
}

function signUp(obj) {
    document.body.classList.add('loading-indicator');
    obj.ConfirmPassword = cryptedString(obj.ConfirmPassword)
    obj.Password = cryptedString(obj.Password)
    return authService.post(`users/register`, obj)
        .then(res => {
            document.body.classList.remove('loading-indicator');
            return res;
        });
}

function getCountryCodes() {
    return authService.withOutTokenGet(`users/unauth/countries`)
        .then(res => {
            return res;
        });
}
function sendOPT(userId) {
    let data = new FormData();
    data.append('UserId', userId);
    data.append('IsReact', 1);
    return authService.loginPostAxios(`users/unauth/send/otp`, data)
        .then(res => {
            return res.data;
        });
}
function verifyOTP(userid, OTP) {
    let data = new FormData();
    data.append('UserId', userid);
    data.append('OTP', OTP);
    return authService.loginPostAxios(`users/unauth/verify/otp`, data)
        .then(res => {
            return res.data;
        });
}
