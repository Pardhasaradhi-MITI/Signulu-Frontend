import {authService} from "../_services/auth.service";

export const TemplateServise={
    getmytemplates:getmytemplates,
    getmytemplatesfile:getmytemplatesfile,
    Updatetemplatedocument:Updatetemplatedocument,
    Deletetemplatedocument:Deletetemplatedocument,
    GetTemplatedetails:GetTemplatedetails,
    BulksendTemplates:BulksendTemplates,
    GetTemplatedetailswithid:GetTemplatedetailswithid
}
function getmytemplates(data){
    return authService.post('adocuments/template/list',data)
}
function getmytemplatesfile(fileName){
    return authService.getFile(`adocuments/download/`+ fileName)
}
function Updatetemplatedocument(data){
    return authService.put(`adocuments/template/${data.TemplateId}/update`,data)
}
function Deletetemplatedocument(data){
    return authService.put(`adocuments/template/delete/multiple`,data)
} 
function GetTemplatedetails(TemplateId){
    return authService.get(`adocuments/template/${TemplateId}/details`)
}
function BulksendTemplates(payload){
    return authService.postbulksend(`adocuments/send/multiple/recipents`,payload)
}
function GetTemplatedetailswithid(TemplateId){
    return authService.get(`adocuments/template/${TemplateId}`)
}



