import {authService} from "../_services/auth.service";

export const reportsservice={

    getpichartdocumentlist:getpichartdocumentlist,
    getmultibarchartdoclist:getmultibarchartdoclist,
    getreportlist:getreportlist,
    getdataforcards:getdataforcards,
    getdocumenttractchart:getdocumenttractchart,
}
function getpichartdocumentlist(){
    return authService.get('dashboard/documentpiechart').then(res=>{
          return res;
    })
}
function getmultibarchartdoclist(year){
    return authService.get(`dashboard/adocuments/${year}/chart`).then(res=>{
        return res;
  })
}
function getreportlist(){
    return authService.get(`dashboard/document/report`).then(res=>{
        return res;
  })
} 
function getdataforcards(){
    return authService.get(`users/account/summary/report`).then(res=>{
        return res;
  })
} 
function getdocumenttractchart(){
    return authService.get(`dashboard/documenttrackchart/years`).then(res=>{
        return res;
  })
} 

