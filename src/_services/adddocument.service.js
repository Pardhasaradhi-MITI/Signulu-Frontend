import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
import { authService } from './auth.service';
const dataObj = new BehaviorSubject(null);
const userPreferencesObj = new BehaviorSubject(null);
export const addDocumentService = {
    addDocument,
    getWorkFlowTypes,
    getDocumentInfo,
    getDocumentFile,
    updateDocumentInfo,
    getContactsListInfo,
    getUserListInfo,
    getDocumentPreviewDetaild,
    finalSubmit,
    getControlsTypes,
    updateDocument,
    getGoogleDocDataById,
    getOneDriveDocDataById,
    getDropBoxDocDataById,
    getgoogledrivedata,
    getdropboxdrivedata,
    getonedrivedata,
    unauthGetDocumentData,
    unauthGetVerify,
    unauthSaveSubmit,
    unauthGetFile,
    unauthSaveOnly,
    unauthSubscribtion,
    getdropboxdrivechilddata,
    getgoogledrivechilddata,
    getonedrivechilddata,
    unauthRejectDocument,
    unauthTermsConditions,
    getSelectpagelist,
    getSignaturepositionlist,
    eKYCVerify,
    getekycverify,
    getUserSettings,
    getStatesList,
    getUserPreferences,
    setUserPreferences,
    getUserDeatailsByEmail,
    getdscsignresponse,
    ekycdobandusernameverify,
    getAadharsignresponse,
    sandboxunauthSaveSubmit,
    updateDropDocumentInfo,
    unauthReassignSubmit,
    getrejecteddocumentreasion,
    dataObj: dataObj.asObservable(),
    get userValue () { return dataObj.value },
    userPreferencesObj : userPreferencesObj.asObservable(),
    get userPreferences () { return userPreferencesObj.value },
};

function addDocument(objdata, files) {
    let formData = new FormData();
    formData.append('model', JSON.stringify(objdata))
    if (files) {
for (let index = 0; index < files.length; index++) {
    formData.append('Document[]', files[index])    
}
    } else {
        formData.append('Document[]', '')
    }
    return authService.PostAxios(`adocuments/add`,formData)
        .then(res => {
            return res.data;
        });
}

function getWorkFlowTypes(loader){
    return authService.PostAxios(`adocuments/workflow/types`, '',loader)
        .then(res => {
            return res.data;
        });
    
}

function getDocumentInfo(docid , loader){
    return authService.get(`adocuments/`+ docid ,loader)
        .then(res => {
            return res;
        });
    
}
function getrejecteddocumentreasion(docid){
    return authService.get(`adocuments/rejectreason/`+ docid)
        .then(res => {
            return res;
        });
    
}

function getContactsListInfo(contactTypeId, pageNumber, pageSize, searchTerm){
   let DatObj = { ContactTypeId: contactTypeId,
                   PageNumber: pageNumber,
                   PageSize: pageSize,
                   SearchByText: searchTerm}
    return authService.PostAxios(`contacts/list`, DatObj)
        .then(res => {
            return res.data;
        });
    
}
function getDocumentFile(fileName){
    return authService.getFile(`adocuments/download/`+ fileName)
        .then(res => {
            return res;
        });
}
function updateDocumentInfo(id, objdata){
    let formData = new FormData();
    formData.append('model', JSON.stringify(objdata))
    return authService.PostAxios(`adocuments/update/`+ id, formData)
        .then(res => {
            return res;
        });
    
}
function updateDropDocumentInfo(id, objdata){
    let formData = new FormData();
    formData.append('model', JSON.stringify(objdata))
    const plainFormData = Object.fromEntries(formData.entries());
    return authService.PostFetchWithoutLoader(`adocuments/update/`+ id, plainFormData)
        .then(res => {
            return res;
        });
    
}
function updateDocument(id, objdata, files) {
    let formData = new FormData();
    formData.append('model', JSON.stringify(objdata))
    if (files && files.length>0) {
  for (let index = 0; index < files.length; index++) {
        formData.append('Document[]', files[index])    
    
     }
    }
    return authService.PostAxios(`adocuments/update/`+ id, formData)
        .then(res => {
            return res.data;
        });
}
function getUserListInfo(searchTerm){
     return authService.get(`adocuments/search/` + searchTerm)
         .then(res => {
             return res;
         });
     
 }
 function getDocumentPreviewDetaild(id){
    return authService.get(`adocuments/preview/${id}`)
        .then(res => {
            return res;
        });
    
}
function finalSubmit(id, data){
    return authService.post(`adocuments/saveandsubmit/${id}`, data)
        .then(res => {
            return res;
        });
    
}
async function getControlsTypes(){
    return authService.post(`adocuments/control/types`)
        .then(res => {
            return res;
        });
    
}
async function getGoogleDocDataById(Id, name) {
    document.body.classList.add('loading-indicator');
    return authService.get(`google/download/${Id}/${name}`)
        .then(res => {
            document.body.classList.remove('loading-indicator');
            return res;
        });

}
async function getOneDriveDocDataById(Id, name) {
    document.body.classList.add('loading-indicator');
    return authService.get(`onedrive/download/${Id}/${name}`)
        .then(res => {
            document.body.classList.remove('loading-indicator');
            return res;
        });

}
async function getDropBoxDocDataById(Id, name) {
    document.body.classList.add('loading-indicator');
    return authService.get(`dropbox/download/${Id}/${name}`)
        .then(res => {
            document.body.classList.remove('loading-indicator');
            return res;
        });

}
async function getgoogledrivedata(){
    document.body.classList.add('loading-indicator');
    return authService.get(`google/contents`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    });
}
async function getdropboxdrivedata(){
    document.body.classList.add('loading-indicator');
    return authService.get(`dropbox/contents`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}

async function getonedrivedata(){
    document.body.classList.add('loading-indicator');
    return authService.get(`onedrive/contents`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })

    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthGetVerify(docId, PID) {
    document.body.classList.add('loading-indicator');
    return authService.unauthGet(`adocuments/unauth/verify/${docId}/${PID}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthGetDocumentData(docId, PID) {
    document.body.classList.add('loading-indicator');
    return authService.unauthGet(`adocuments/unauth/view/${docId}/${PID}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthGetFile(docId, PID) {
    return authService.unauthGetFile(`adocuments/unauth/download/design/pdf/${docId}/${PID}`)
    .then(res => {
        return res;
    }).catch(err => {
    return err;}  ) 
}
function unauthSubscribtion(docId) {
    document.body.classList.add('loading-indicator');
    return authService.unauthGet(`adocuments/unauth/subcription/${docId}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthTermsConditions(docId, PID) {
    document.body.classList.add('loading-indicator');
    return authService.unauthGet(`adocuments/accept/tos/${docId}/${PID}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}

function unauthSaveSubmit(docId, PID, payload) {
    document.body.classList.add('loading-indicator');
    return authService.unauthPost(`adocuments/unauth/saveandsubmit/${docId}/${PID}`, payload)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthSaveOnly(docId, PID, payload) {
    document.body.classList.add('loading-indicator');
    let formData = new FormData();
    formData.append('model', JSON.stringify(payload))
    return authService.unauthPost(`adocuments/unauth/save/${docId}/${PID}`, payload)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthRejectDocument(docId, PID, payload){
    document.body.classList.add('loading-indicator');
    return authService.unauthPut(`adocuments/unauth/reject/${docId}/${PID}`, payload)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}

function getgoogledrivechilddata(id){
    document.body.classList.add('loading-indicator');
    return authService.get(`google/contents/${id}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function getdropboxdrivechilddata(id){
    document.body.classList.add('loading-indicator');
    return authService.get(`dropbox/contents/${id}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
async function getonedrivechilddata(id){
    document.body.classList.add('loading-indicator');
    return authService.get(`onedrive/contents/${id}`)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function getSelectpagelist(){
    return authService.post(`adocuments/select/page`)
    .then(res => {
        return res;
    });
}
function getSignaturepositionlist(){
    return authService.post(`adocuments/signature/position`)
    .then(res => {
        return res;
    });
}
function eKYCVerify(objdata) {
    return authService.SandboxPostAxios(`aadhaar/unauth/unauthDocumentEkycUrl`,objdata)
        .then(res => {
            return res.data;
        });
}
function getekycverify(DocId,PID,ReqID) {
    return authService.SandboxGetapi(`aadhaar/unauth/unauthDocumentVerifyEkyc/${DocId}/${PID}/${ReqID}`)
        .then(res => {
            return res;
        });
    }
    function getUserSettings(id){
        return authService.get(`settings/user/signature/${id}`)
            .then(res => {
                return res;
            });
        
    }
    function getStatesList(){
        return authService.staticApi(`/assets/states.json`)
            .then(res => {
                return res;
            });
    }
function getUserPreferences(userId) {
    return authService.get(`settings/user/dateformat/${userId}`)
        .then(res => {
            if (res && res.Entity && res.Entity.length > 0) {
                setUserPreferences(res.Entity[0])
            }
            return res;
        });
}
function setUserPreferences(data){
    userPreferencesObj.next(data);
}
function getUserDeatailsByEmail(email){
    return authService.get(`users/unauth/check/${email}`)
        .then(res => {
            return res;
        });
    }
    
function  getdscsignresponse(data,DocId, PID){
    return authService.post(`adocuments/unauth/dsc/request/${DocId}/${PID}`,data)
    .then(res => {
        return res;
    });
}


function ekycdobandusernameverify(data){
    return authService.SandboxPostAxios(`aadhaar/unauth/unauthDocumentVerifyAadhaardetails/${data.ADocumentId}/${data.RecipientId}`,data)
        .then(res => {
            return res.data;
        });
}
function  getAadharsignresponse(data,DocId, PID){
    return authService.SandboxPostAxiosAadhar(`adocuments/unauth/aadhaar/request/${DocId}/${PID}`,data)
    .then(res => {
        return res;
    });
}
function sandboxunauthSaveSubmit(docId, PID, payload) {
    document.body.classList.add('loading-indicator');
    return authService.unauthPost(`adocuments/unauth/saveandsubmit/${docId}/${PID}`, payload)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  ) 
}
function unauthReassignSubmit(docId,PID,payload) {
    document.body.classList.add('loading-indicator');
    return authService.unauthPost(`adocuments/reassign/${docId}/${PID}`, payload)
    .then(res => {
        document.body.classList.remove('loading-indicator');
        return res;
    })
    .catch(err => {document.body.classList.remove('loading-indicator');
    return err;}  )
}

