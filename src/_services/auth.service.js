import { accountService } from '@/_services';
import { history } from '@/_helpers';
import axios from 'axios';
import config from 'config';
import {useHistory } from 'react-router-dom';

let API_END_POINT = window.location.href.includes(`/index1.php`)?config.redirectserverUrl:config.serverUrl
const API_END_POINT_SANDBOX = config.sandboxserverUrl

const FILE_END_POINT = config.fileServerUrl
const FACE_REC_API = config.faceRecApiUrl
export const authService = {
    get,
    post,
    put,
    PostAxios,
    PostAxios1,
    delete: _delete,
    loginpost: loginPost,
    loginPostAxios: loginPostAxios,
    VerficationEmailPost: VerficationEmailPost,
    withOutTokenGet: withOutTokenGet,
    getFile,
    postbulksend,
    unauthGet,
    unauthPost,
    unauthPut,
    unauthGetFile,
    SandboxPostAxios,
    SandboxGetapi,
    facialPost,
    staticApi,
    verificationMail,
    SandboxPostAxiosAadhar,
    sandboxAxiouspostAadhar,
    PostFetchWithoutLoader,
    PostAxios1
}
// All Axios Api Calls
function loginPostAxios(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            Accept: 'application/json',
        },
    };
    return axios.post(API_END_POINT + url, body, requestOptions).then(res => { 
        document.body.classList.remove('loading-indicator');
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});

}
function PostAxios(url, body , loader) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...authHeader(url) },

    };
    document.body.classList.add('loading-indicator');
    return axios.post(API_END_POINT + url, body, requestOptions).then(res => { 
        if (!loader) document.body.classList.remove('loading-indicator');
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});

}
function PostAxios1(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...authHeader(url) },

    };
    return axios.post(API_END_POINT + url, body, requestOptions).then(res => { 
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});

}
async function PostFetchWithoutLoader(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...authHeader(url) },
        body: JSON.stringify(body)
    };
    // document.body.classList.add('loading-indicator');
    return await fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}


function VerficationEmailPost(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            Accept: 'application/json',
            userauthtoken: localStorage.getItem('verifyMailToken')
        },
    };
    return axios.post(API_END_POINT + url, body, requestOptions).then(res => { 
        document.body.classList.remove('loading-indicator');
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});

}
// All Axios Api Calls End Here

function withOutTokenGet(url) {
    const requestOptions = {
        method: 'GET',
        withCredentials: true,
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            Accept: 'application/json',
        },
    };
    return fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}

function loginPost(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            Accept: 'application/json',
        },
        body: JSON.stringify(body)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}


// All featch Api Calls
function get(url,loader) {
    const requestOptions = {
        withCredentials: true,
        credentials: "same-origin",
        method: 'GET',
        headers:authHeader(url),
    };
     document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then((response)=>{
     if (!loader)   document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}

function getFile(url) {
    const requestOptions = {
        method: 'GET',
        observe: 'response',
         responseType: 'arraybuffer',
        headers:authHeader(url),
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(handleFileResponse);
}

function post(url, body) {
       const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...authHeader(url) },
        body: JSON.stringify(body)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}
function put(url, body) {
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', ...authHeader(url) },
        body: JSON.stringify(body)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(url) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(url)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}
// helper functions
function authHeader(url) {
    // return auth header with jwt if user is logged in and request is to the api url
    // const user = accountService.userValue;
    API_END_POINT = window.location.href.includes(`/index1.php`)?config.redirectserverUrl:config.serverUrl
    let logintimeouttime = localStorage.getItem('Logintime');
    let now = new Date();
    let date1=new Date(logintimeouttime).getTime()
    let date2=new Date().getTime()
    if(date2>=date1){
        localStorage.removeItem('LoginToken');
        document.body.classList.remove('loading-indicator');
        history.push("/account/login");
    }
    else{
    const token = localStorage.getItem('LoginToken');
    if (token) {
        return { userauthtoken: `${token}`,'Content-Type': 'application/json', Accept: 'application/json'};
    } else {
        localStorage.removeItem('LoginToken');
        history.push("/account/login");   
    }
   }
}
function unauthHeader(url, nofile) {
    if (nofile) {
        return {'Content-Type': 'application/json', Accept: 'application/json'};
    } else {
        
    }
}
function handleResponse(response) {
    // document.body.classList.remove('loading-indicator');
    return response.json().then(text => {
        const data = text
        // console.log(response.ok)
        if (!response.ok) {
            if ([401, 403].includes(response.status) && accountService.userValue) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                accountService.logout();
                history.push("/account/login");
            }
            const error = Object.assign({}, json, {
                status: response.status,
                statusText: response.statusText ? response.statusText : json.error || '',
            });
            return Promise.reject(error);
        } else {
        }
        return data;
    }).catch(error =>{
        console.log(error, '8')
    });
}
function handleFileResponse(response) {
    document.body.classList.remove('loading-indicator');
    return response.arrayBuffer().then(text => {
        const data = text;
        if (!response.ok) {
            if ([401, 403].includes(response.status) && accountService.userValue) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                accountService.logout();
               history.push("/account/login");
            }
            const error = Object.assign({}, json, {
                status: response.status,
                statusText: response.statusText ? response.statusText : json.error || '',
            });
            return Promise.reject(error);
        }
        return data;
    });
}
// function postbulksend(url, body){
//     let formData = new FormData();
//     formData.append('model', JSON.stringify(body))
//         const requestOptions = {
//             method: 'POST',
//             headers: { 'Content-Type': 'application/json', ...authHeader(url) },
//             body: formData
//         };
//         document.body.classList.add('loading-indicator');
//         return fetch(API_END_POINT + url, requestOptions).then(response=>{handleResponse(response);document.body.classList.remove('loading-indicator')});
    
// }
function postbulksend(url, objdata){
    let formData = new FormData();
    formData.append('model', JSON.stringify(objdata))
    return authService.PostAxios(url, formData)
        .then(res => {
            return res;
        });
    
}


// API CALLS WITHOUT TOKEN 
function unauthGet(url) {
    const requestOptions = {
        method: 'GET',
        headers:unauthHeader(url, true),
    };
     document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(unauthhandleResponse);
}

function unauthPost(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...unauthHeader(url, true) },
        body: JSON.stringify(body)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(unauthhandleResponse);
}
function unauthPut(url, body) {
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', ...unauthHeader(url, true) },
        body: JSON.stringify(body)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(unauthhandleResponse);
}
function unauthGetFile(url) {
    const requestOptions = {
        method: 'GET',
        observe: 'response',
         responseType: 'arraybuffer',
        headers:unauthHeader(url),
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(unauthhandleFileResponse);
}
function SandboxPostAxios(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...unauthHeader(url) },

    };
    document.body.classList.add('loading-indicator');
    return axios.post(API_END_POINT + url, body, requestOptions).then(res => { 
       document.body.classList.remove('loading-indicator');
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});


}
function SandboxGetapi(url) {
    const requestOptions = {
        withCredentials: true,
        credentials: "same-origin",
        method: 'GET',
        headers:unauthHeader(url),
    };
     //document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT + url, requestOptions).then(unauthhandleResponse);
}
function facialPost(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...unauthHeader(url) },

    };
    document.body.classList.add('loading-indicator');
    return axios.post(FACE_REC_API + url, body, requestOptions).then(res => { 
        document.body.classList.remove('loading-indicator');
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});
}
function unauthhandleResponse(response) {
    document.body.classList.remove('loading-indicator');
    return response.json().then(text => {
        const data = text
        // console.log(response.ok)
        if (!response.ok) {
            if ([401, 403].includes(response.status) && accountService.userValue) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                //accountService.logout();
                //history.push("/account/login");
            }
            const error = Object.assign({}, json, {
                status: response.status,
                statusText: response.statusText ? response.statusText : json.error || '',
            });
            return Promise.reject(error);
        } else {
        }
        return data;
    }).catch(error =>{
        console.log(error, '8')
    });
}

function unauthhandleFileResponse(response) {
    document.body.classList.remove('loading-indicator');
    return response.arrayBuffer().then(text => {
        const data = text;
        if (!response.ok) {
            if ([401, 403].includes(response.status) && accountService.userValue) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                //accountService.logout();
               // history.push("/account/login");
            }
            const error = Object.assign({}, json, {
                status: response.status,
                statusText: response.statusText ? response.statusText : json.error || '',
            });
            return Promise.reject(error);
        }
        return data;
    });
}

// All featch Api Calls
function staticApi(url) {
    const requestOptions = {
        withCredentials: true,
        credentials: "same-origin",
        method: 'GET',
        headers:unauthHeader(url),
    };
    return fetch(url, requestOptions).then((response)=>{
        handleResponse(response)
        document.body.classList.remove('loading-indicator');
    });
}

function verificationMail(url) {
    const requestOptions = {
        withCredentials: true,
        credentials: "same-origin",
        method: 'GET',
        headers:verifyemailauth(url),
    };
    return fetch(API_END_POINT + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}

function verifyemailauth(url) {
 
    const token = localStorage.getItem('verifyMailToken');
    if (token) {
        return { userauthtoken: `${token}`,'Content-Type': 'application/json', Accept: 'application/json'};
    } else {

    }
   }
   function SandboxPostAxiosAadhar(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...unauthHeader(url) },

    };
    document.body.classList.add('loading-indicator');
    return axios.post(API_END_POINT_SANDBOX + url, body, requestOptions).then(res => { 
       document.body.classList.remove('loading-indicator');
    return res}).
    catch(err => {document.body.classList.remove('loading-indicator');
    return err;     
});
}
function sandboxAxiouspostAadhar(url, body) {
   
    console.log(url, body)
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', ...authHeader(url) },
        body: JSON.stringify(body)
    };
    document.body.classList.add('loading-indicator');
    return fetch(API_END_POINT_SANDBOX + url, requestOptions).then(response=>{
        document.body.classList.remove('loading-indicator');
        return handleResponse(response);
    });
}


