import React, { useState, useEffect } from 'react';
import { Route, Switch, Redirect, useLocation, useHistory } from 'react-router-dom';

import config from 'config';
import { Role } from '@/_helpers';
import { accountService } from '@/_services';
import { Nav, PrivateRoute, Alert } from '@/_components';
import { Home } from '@/home';
import { Profile } from '@/profile';
import { Admin } from '@/admin';
import { Account } from '@/account';
import "primeflex/primeflex.css";
import { StyleClass } from 'primereact/styleclass';
import { Supperadmin } from '@/supperadmin';
console.log(config?.redirectserverUrl, "test url")
const apiUrl = config.redirectserverUrl;
console.log(apiUrl, "test url")

function App() {
    debugger
    const { pathname } = useLocation();
    const [user, setUser] = useState({});

    useEffect(() => {
        const subscription = accountService.user.subscribe(x => setUser(x));
        return subscription.unsubscribe;
    }, []);

    return (
        <div className={'app-container' + (user && ' bg-light')}>
            <Nav />
            <Alert />
            <Switch>
                <Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />
                <PrivateRoute exact path="/" component={Home} />
                <PrivateRoute path="/profile" component={Profile} />
                <PrivateRoute path="/admin" roles={[Role.Admin]} component={Admin} />
                <Route path="/account" component={Account} />
                <Route path="/supperadmin" component={Supperadmin} />
                <Route
                    path="/index1.php/**"
                    render={({ match }) => {
                        console.log(`${apiUrl}${match.url}`)
                        window.location.replace(`${apiUrl}${match.url}`);
                        return null;
                    }}
                />
                {/* <Redirect from="*" to="/" /> */}

            </Switch>
        </div>
    );
}

export { App }; 