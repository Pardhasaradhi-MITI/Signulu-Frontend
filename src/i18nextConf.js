import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import HttpApi from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

const availableLanguages = ['en', 'te', 'fr', 'hn'];

i18n
  .use(initReactI18next)// pass the i18n instance to react-i18next.
  .use(LanguageDetector) // detect user language
  .use(HttpApi)  // load translations using http (default public/assets/locals/en/translations)
  .init({
    fallbackLng: "en", // fallback language is english.

    detection: {
        order: ['querystring', 'localStorage','navigator','htmlTag', 'cookie', 'path', 'subdomain'],
      checkWhitelist: true, // options for language detection
    },

    debug: false,

    whitelist: availableLanguages,

    interpolation: {
      escapeValue: false, // no need for react. it escapes by default
    },
    backend: {
        loadPath: 'src/locales/{{lng}}/translation.json'
    },
    react: {
        useSuspense: false
    }
  });

export default i18n;