import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { createEntityAdapter } from '@reduxjs/toolkit';

function PersonalInformations({ history, location }) {
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content personalinformations">
                    <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <h3>Profile Information</h3>
                                <p>To configure what information is shared when you sign.</p>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
                                <div class="progress">
                                    <div class="progress-bar bar-width-md" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        <span class="value__bar_sm">25% Complete</span>
                                        <span class="value__bar_md">50% Complete</span>
                                        <span class="value__bar_lg">75% Complete</span>
                                        <span class="value__bar_xl">100% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='row align-items-center'>
                        <div className='profile-info-content'>
                            <div className='row'>
                                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                                    <div className='profile-information'>
                                        <div className='user-profile-img'>
                                            <img src='src/images/profile.jpg' />
                                            <div class="crez-btn">
                                                <input type="file" id="real-file" hidden="hidden" />
                                                <button type="button" id="custom-button"><img src="src/images/camera.svg" alt="" /></button>
                                            </div>
                                        </div>
                                        <span class="user-name">Edit Profile Picture</span>
                                    </div>
                                    <div className='tab-list-content'>
                                        <ul className='tab-list'>
                                            <li><button class="link-tab active done"><div class="li-count">1</div> <div class="text-btn">Personal Information</div> </button></li>
                                            <li><button class="link-tab active"><div class="li-count">2</div> <div class="text-btn">Company Information</div>  </button></li>
                                            <li><button class="link-tab"><div class="li-count">3</div> <div class="text-btn">Location Information</div> </button></li>
                                            <li><button class="link-tab"><div class="li-count">4</div> <div class="text-btn">Contact Information</div>  </button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                    <div className='tab-content'>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Personal Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="James" />
                                                            <label for="">First Name<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="Franco" />
                                                            <label for="">Last Name<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="info.james@gmail.com" />
                                                            <label for="">Email<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='company-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Company Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder=" California" />
                                                            <label for="">Job Title</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="Businessman" />
                                                            <label for="">Designation<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea />
                                                            <label for="">Additional Information</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Location Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea placeholder=' California' />
                                                            <label for="">Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea placeholder=' California' />
                                                            <label for="">Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="San Francisco" />
                                                            <label for="">City<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="Oakland" />
                                                            <label for="">State<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" />
                                                            <label for="">Zip Code<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='contact-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Contact Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" />
                                                            <label for="">Phone<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <select>
                                                                <option style={{ backgroundImage: "url(src/images/instagram_icon.svg)" }}>Instagram</option>
                                                            </select>
                                                            <label for="">Select Social Media</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-8 col-lg-8 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" />
                                                            <label for="">Add Link</label>
                                                            <button className="btn add_link_btn">ADD</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="social-tags">
                                                        <a href="#" className="link"><img src="src/images/twitter.svg" alt="" /> Twitter <span className="hide">x</span></a>
                                                        <a href="#" className="link"><img src="src/images/linkedin.svg" alt="" /> Linkedin <span className="hide">x</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div class="subf_pagn_btn">
                                                    <button class="btn">Save</button>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { PersonalInformations }; 