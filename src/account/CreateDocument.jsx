import React, { useRef } from 'react';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { addDocumentService } from '../_services/adddocument.service';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useEffect, useState } from "react";
import { commonService } from '../_services/common.service';
import config from 'config';
import { Editor } from '@tinymce/tinymce-react';
import {usersService} from "../_services/users.service"
import {createdocumentservice} from "../_services/CreateDocumentService"
import { manageservice } from '../_services/manageservice';
import Swal from "sweetalert2";
import { Button, Modal } from 'react-bootstrap';

function CreateDocument(location) {
    const usehistory = useHistory();
    const [loginuserdata,setloginuserdata] = useState({})
    const [documentdata,setdocumentdata]= useState({})
    const [documentId,setdocumentId] = useState();
    const [buttontype,setbuttontype] = useState(true)
    const [saveddocresponse,setsaveddocresponse]=useState([])
    const [documentinfo,setdocumentinfo] = useState([])
    const [documentsplitid,setdocumentsplitid] = useState(-1)
    const [documentName,setDocumentName]=useState("Document1")
    const [defaultdocumentName,setdefaultdocumentName]=useState("Document1")
    const [showdocumentTitlemodule,setshowdocumentTitlemodule]=useState(false)
    const [showbuttons,setshowbuttons]=useState(false)

   
    useEffect(()=>{
        getCurrentUserData();
    },[])
    useEffect(() => {
        setTimeout(function () {
            setshowbuttons(true);
        }, 5000);
      }, []);
    const getCurrentUserData = ()=>{
        usersService.getCurrentUserData()
        .then((result) => {
            if(result){
                setloginuserdata(result.Data)
                if(usehistory?.location?.state === undefined){
                CreateDocument(result?.Data)}
                else{
                    getdocumentinfo();
                }
            }
        })
    }
    const CreateDocument=(data)=>{
        let payload={
             
            "ADocumentId":-1,
            "Title":"Document1",
            "IsParallelSigning":true,
            "UserId":data.UserId,
            "CompanyId":data.company_id,
            "IsFromScratchDocument" : true,
            "Height":1188,
            "Width":918,
            "Recipients":[
               {
                  "UserId":data.UserId,
                  "EmailAddress":data.EmailAddress,
                  "FirstName":data.FirstName,
                  "LastName":data.LastName,
                  "FullName":data.FullName,
                //   "IsOwner":data.IsOwner === 1?true:false,
                  "IsOwner":true,
                  "IsSignatory":true,
                  "SignType":"",
                  "Initial":"",
                  "Color":"att-ad-recipient-aqua",
                  "StatusCode":"DRAFT",
                  "IsInPerson":0
               }
            ]
         }
       createdocumentservice.createDocument(payload).then((result)=>{
            if(result?.data?.Entity?.Data){
                setdocumentdata(result?.data?.Entity?.Data);
            }
       })

    }
    const getdocumentinfo=()=>{
          manageservice.getDocumentData(usehistory?.location?.state?.DocumentID).then((result)=>{
            if(result?.Entity?.Data)
            setdocumentdata(result?.Entity?.Data)
             setdocumentinfo(result?.Entity?.Data)
             setDocumentName(result?.Entity?.Data?.Title)
             setdefaultdocumentName(result?.Entity?.Data?.Title)
             setdocumentsplitid(result?.Entity?.Data?.UploadDocuments[0].ADocumentSplitId)
          }).catch({

          })
    }
    //Vertical Tabs
    const editorRef = useRef(null);

    let { id } = useParams();

    const saveasdoc=()=>{
        if (editorRef.current) {
            let filedata= editorRef.current.getContent()
            if(filedata.length>0){
                setshowdocumentTitlemodule(true);
               }
               else{
                Swal.fire({
                    icon:"info",
                    title: `You Cannot Save Empty Document `,
                    confirmButtonText: 'OKAY',
                  })
               }
            }
         
    }

    const log = (type) => {
        if (editorRef.current) {
            let filedata= editorRef.current.getContent()
            let base64 = btoa(unescape(encodeURIComponent(filedata)));
           if(filedata.length>0){
            updatedocument(base64,type)
           }
           else{
            Swal.fire({
                icon:"info",
                title: `You Cannot Save Empty Document`,
                confirmButtonText: 'OKAY',
              })
           }
        }
      };
      
    const updatedocument=(base64file,type)=>{
       let payload={
         
        "ADocumentId":documentdata.ADocumentId,
        "Title":`${documentName}`,
        "IsParallelSigning":true,
        "UserId":loginuserdata.UserId,
        "CompanyId":loginuserdata.company_id,
        "Height":1188,
        "Width":918,
        'TotalPages': 1,
        "UploadDocuments":[
           {
              "ADocumentSplitId": documentsplitid,
              "OriginalFileName":`${documentName}.pdf`,
              "FileName":"",
              "Position":1,
              "DocumentSource":"upload",
              "IsDelete":false
           }
        ],
        "ExternalDocuments":[
           {
              "FileName":`${documentName}.pdf`,
              "FileContent":`${base64file}`
           }
        ]
       }
       createdocumentservice.UpdateDocument(payload).then((result)=>{
       
        if(result){
        setdocumentId(result?.data?.Entity?.Data.ADocumentId)
       if(type === "save"|| type === "saveas"){
        setshowdocumentTitlemodule(false); 
        setsaveddocresponse(result?.data?.Entity?.Data);
        setdocumentdata(result?.data?.Entity?.Data)
        setdocumentsplitid(result?.data?.Entity?.Data?.UploadDocuments[0].ADocumentSplitId)
        Swal.fire({
            title: 'Document Saved As  Draft',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'GO TO MANAGE SECTION',
            cancelButtonText:"CLOSE",
            cancelButtonColor:'#565e64',
          }).then((result) => {
            if (result.isConfirmed) {
                usehistory.push('/account/manage')
           
            }
          })
       }
       if(type === "cancel"){
        Swal.fire({
            icon: 'success',
            title: 'Document Saved AS Draft',
            showConfirmButton: true,
            confirmButtonText:"OKAY",
            timer: 1000
          })
        backtohome()
       }
       else{
        // type === "next" ? next(result?.data?.Entity?.Data.ADocumentId):""}
        type === "next" ? next(result?.data?.Entity?.Data):""}
        }
       })
    }

    function next(data){
        console.log(data,"nextbutton")
        commonService.setDocauthDocument(data)
        usehistory.push('/account/adddocumentsstep')
        // usehistory.push({
        //     pathname: `/account/adddocumentsstep/${id}`,
        //     state: {DocumentID:`${id}`
        // }})
    }
    const  backtohome=()=>{
        usehistory.push('/account/dashboard')
    }


    /****** To Go to Back dashboard Page  */
    function back() {
        usehistory.push('/account/documentselect/' + step3Data.ADocumentId)
    }
    function getNext() {
        usehistory.push('/account/documentreview/' + step3Data.ADocumentId)
    }
  

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />
                
                <div className="main-content wizard">
                    <div className='row'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <h3>Create Document</h3>
                            <div id="wizard_step">
                                {/* <div className="step-wizard" role="navigation">
                                    <ul>
                                        <li className={count === 0 || count < 4 ? 'active done' : ''}>
                                            <button>
                                                <div className="step">1</div>
                                                <div className="title">Add</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 1 || (count < 4 && count != 0) ? 'active done' : ''}>
                                            <button id="step2">
                                                <div className="step">2</div>
                                                <div className="title">Select</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 2 || (count < 4 && count != 1 && count != 0) ? 'active done' : ''}>
                                            <button id="step3">
                                                <div className="step">3</div>
                                                <div className="title">Prepare</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 3 ? 'active done' : ''}>
                                            <button id="step4">
                                                <div className="step">4</div>
                                                <div className="title">Review</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                    </ul>
                                </div> */}

                                <div className='wizard-panel' id="wizard-pane-3" >
                                    <div className='content'>
                                  
                                    <>
                                    <Editor
        apiKey='pxqba6kj12rdz151yv2w5tvxat7jusc1pvolx2e3q7x6xsrg'
        onInit={(evt, editor) => editorRef.current = editor}
        initialValue={`${atob(unescape(encodeURIComponent(usehistory?.location?.state === undefined ?"": documentinfo.length===0?"":documentinfo.UploadDocuments.length === 0?"":documentinfo.UploadDocuments[0].FileContent)))}`}
        init={{
          placeholder:"Type here to create your document",
          height: 600,
          // width: 1024,
          menubar: true,
          plugins: [
            'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'quickbars', 'template', 'autosave lists autolink', 'ruler pagebreak', 'image code',
            'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen', 'hr', 'noneditable', 'print', 'save', 'autosave', 'image imagetools',
            'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount', 'emoticons', 'pagebreak', 'paste', 'textpattern', 'textpattern link', 'textpattern lists', 'strikethrough'
          ],
          toolbar: 'undo redo|code ' +
            'bold italic underline strikethrough styleselect forecolor backcolor|fontfamily fontsize blocks |' +
            ' alignleft aligncenter alignright alignjustify|bullist numlist outdent indent |restoredraft insertfile|link image media template anchor codesample |' +
            'link charmap emoticons removeformat |fullscreen hr insertdatetime |pagebreak paste pastetext| visualblocks' +
            'preview print save|wordcount imagetools | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
          font_formats: 'Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace; AkrutiKndPadmini=Akpdmi-n',
          noneditable_editable_class: 'mceEditable',
          noneditable_noneditable_class: 'mceNonEditable',
          autosave_ask_before_unload: true,
          a11y_advanced_options: true,
          autosave_restore_when_empty: true,
          quickbars_selection_toolbar: 'bold italic underline | formatselect quickimage | quicklink blockquote',
          font_size_formats: '8px 10px 12px 14px 16px 18px 24px 36px 48px',
          template_cdate_classes: 'cdate creationdate',
          template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
          template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
          templates: [
            { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
            { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
            { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
          ],
          imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
          content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
          file_picker_types: 'file image media',
          automatic_uploads: true,
          file_picker_callback: function (cb, value, meta) {
            var input = document.createElement("input");
            input.setAttribute("type", "file");
            input.setAttribute("accept", "image/*");
            input.onchange = function () {
              var file = this.files[0];
              var reader = new FileReader();
              reader.onload = function () {
                
                var id = "blobid" + new Date().getTime();
                var blobCache = editorRef.current.editorUpload.blobCache;
                var base64 = reader.result.split(",")[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
                /* call the callback and populate the Title field with the file name */
                cb(blobInfo.blobUri(), { title: file.name });
              };
              reader.readAsDataURL(file);
            };
            input.click();
          },
        }}
      />
      {/* <button onClick={log}>Log editor content</button> */}
      {showbuttons === true?
      <div className='flex justify-content-end mt-3'>
      {/* <button className='px-1 py-2 flex justify-content-center' style={{border:"1px solid #e75c1e",borderRadius:"3px",color:"#FFFF",backgroundColor:"#e75c1e",width:"145px"}} onClick={log}>Next</button> */}
      {/* <button className='px-1 py-2 flex justify-content-center' style={{border:"none",borderRadius:"3px", color:"#858F9F",width:"145px"}} onClick={log}>Cancel</button> */}
      <Button variant="info" onClick={()=>saveasdoc()} >
        {`${" "} SAVE AS${" "} `}
      </Button>
      <Button variant="success" onClick={()=>log("save")} >
        {`${" "} SAVE ${" "} `}
      </Button>
      <Button variant="primary" onClick={()=>log("next")} >
        {`${" "} NEXT ${" "} `}
      </Button>
      <Button variant="secondary"  onClick={()=>
        Swal.fire({
            title: `Want to save your changes to "${documentName}"?`,
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'SAVE',
            cancelButtonText:"CANCEL",
            denyButtonText: `DON'T SAVE`,
          }).then((result) => {
            if (result.isConfirmed) {
                log("cancel")
            //   Swal.fire('Document Saved AS Draft', '', 'success')
            //   backtohome();
            } else if (result.isDenied) {
                // Swal.fire({
                //     icon: 'info',
                //     title: 'Document not saved',
                //     showConfirmButton: true,
                //     timer: 1500
                //   })
              backtohome();
            }
            else if(result.dismiss){
                backtohome();
            }
          })
    }>
        CANCEL
      </Button>
      </div>:""}
    </>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={showdocumentTitlemodule} onHide={() => {
                    setshowdocumentTitlemodule(false); 
                    setDocumentName('')
                }}
                    size=""
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p>Save your changes to this document?</p>
                        </Modal.Title>
                        <button type="button" className="close"  style={{backgroundColor:"#ffff"}} onClick={() => {
                            setshowdocumentTitlemodule(false); 
                            setDocumentName('Document1')
                        }} >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <label htmlFor="docName">File Name <span style={{ color: 'red' }}>*</span> </label>
                        <input type="text" id="docName" className='docinput' name="docName" value={documentName} onChange={(e)=>setDocumentName(e.target.value)} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary"
                         onClick={() => {
                          log("saveas")
                        }}>SAVE</Button>
                        <Button variant="danger"
                         onClick={() => {
                          setDocumentName(defaultdocumentName);
                          backtohome()
                        }}>DON'T SAVE</Button>
                        <Button variant="secondary" onClick={() => {
                            setshowdocumentTitlemodule(false); 
                            setDocumentName(defaultdocumentName);
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
        </div>
    )
}

export { CreateDocument }; 