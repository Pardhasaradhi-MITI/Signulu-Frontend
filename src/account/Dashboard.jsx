import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';

import { accountService, alertService } from '@/_services';

import {Sidebar } from './Common/Sidebar';
import {Topbar } from './Common/Topbar';
import {ContentOne } from './Content/ContentOne';
import {ContentTWO } from './Content/ContentTWO';
import {ContentTHree } from './Content/ContentTHree';

function Dashboard({ history, location }) {    
    return (
        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar/>
        <div className="main" id="main">
            <Topbar/>
            <div className="main-content">
            <ToastContainer />
            <ContentOne/>
            <ContentTWO/>
            <ContentTHree/>
            </div>
        </div>
    </div>
    )
}

export { Dashboard }; 