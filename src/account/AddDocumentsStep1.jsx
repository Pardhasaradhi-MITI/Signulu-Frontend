import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';




function AddDocumentsStep1() {
    //Vertical Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    //Documents Dropdown
    function dropdown1() {
        var srcElement = document.getElementById('dropdown');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }


    return (

        <div className='step wizard-panel'>
            <div className='content'>
                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                        <div className="tab">
                            <button className="tablinks" onClick={(e) => openTab(e, 'firstTab')}>Add Document</button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'secondTab')}>
                                <span className="folder"><img src="src/images/folder_small.svg" alt='' /></span>Use Template
                            </button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}>
                                <span className="google_drive"><img src="src/images/google_drive_small.svg" alt='' /></span>Google Drive
                            </button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}>
                                <span className="one_drive"><img src="src/images/one_drive_small.svg" alt='' /></span>One Drive
                            </button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'fifthTab')}>
                                <span className="drop_box"><img src="src/images/drop_box_small.svg" alt='' /></span>Drop Box
                            </button>
                        </div>

                        <div id="firstTab" className="tabcontent">
                            <h3>Please Add Your Documents <span className='tooltip-info'><img src="src/images/info.svg" alt='' /></span></h3>
                            <div className='row'>
                                <div className='col-xl-5 col-lg-5 col-md-5 col-sm-12'>
                                    <div className="preview">
                                        <div className="dots">
                                            <img src="src/images/dots.svg" alt='' />
                                        </div>
                                        <div className="content">
                                            <img src="src/images/larg_doc.svg" alt='' className='mx-auto d-block' />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12">
                                    <div className="other_docs">
                                        <div className="row">
                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots" onClick={dropdown1}>
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div id="dropdown" className="dropdown" style={{ display: 'none' }}>
                                                        <ul>
                                                            <li>
                                                                <a href="#">
                                                                    Delect <span><img src="src/images/delete_icon.svg" /></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Edit <span><img src="src/images/edit_icon.svg" /></span>
                                                                </a>
                                                            </li>
                                                            <li className="active">
                                                                <a href="#">
                                                                    Replace <span><img src="src/images/replace_icon.svg" /></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <ul className="other">
                                                            <li className="folder">
                                                                <a href="#"><img src="src/images/folder_small.svg" /></a>
                                                            </li>
                                                            <li className="google_drive">
                                                                <a href="#"><img src="src/images/google_drive_small.svg" /></a>
                                                            </li>
                                                            <li className="one_drive">
                                                                <a href="#"><img src="src/images/one_drive_small.svg" /></a>
                                                            </li>
                                                            <li className="drop_box">
                                                                <a href="#"><img src="src/images/drop_box_small.svg" /></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots">
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots">
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots">
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="navigation slide-tab-btn">
                                    <button className="btn prev-btn">BACK</button>
                                    <button className="btn next-btn">NEXT</button>
                                    <div className='draft_btn'>
                                        <button className="btn draf-btn">Draft</button>
                                        <button className="btn cancel-btn">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="secondTab" className="tabcontent"></div>
                        <div id="thirdTab" className="tabcontent"></div>
                        <div id="fourthTab" className="tabcontent"></div>
                        <div id="fifthTab" className="tabcontent"></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { AddDocumentsStep1 }; 