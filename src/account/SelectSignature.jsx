import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function SelectSignature({ history, location }) {
    //Vertical Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    return (


        <div className="main-content main-from-content selectsignature">
            <div className='container'>
                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                        <div className='logo_sec mt-4 mb-4'>
                            <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block'></img>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                        <div className='selectsignature_content'>
                            <div className='header'>
                                <h3>Select Signature</h3>
                                <div className='close'>
                                    <img src='src/images/close.svg' />
                                </div>
                            </div>
                            <div className='content'>
                                <div className='row'>
                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                        <div class="item">
                                            <h5>Recommended Settings:</h5>
                                            <p>Image dimensions: 670 X 280px<br />File Size: 50KB(Max). <br />File Types: .jpg, .png, .j peg. <br />Note: Use transparent image for better result.          </p>
                                        </div>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                        <div className='form_sec'>
                                            <div class="form-group"><label>Name</label> <input type="text" placeholder="James Franco " readonly="" /></div>
                                            <div class="form-group"><label>Initials </label><input type="text" placeholder="JF " readonly="" /></div>
                                        </div>
                                    </div>
                                </div>

                                <div className='row'>
                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                        <div className="tab">
                                            <button className="tablinks active" onClick={(e) => openTab(e, 'firstTab')}>CHOOSE</button>
                                            <button className="tablinks" onClick={(e) => openTab(e, 'secondTab')}>UPLOAD</button>
                                            <button className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}>SIGN</button>
                                        </div>
                                        <div id="firstTab" className="tabcontent">
                                            <div className='list_height'>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature3.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                                <div className='list'>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            <img src='src/images/signature1.svg' />
                                                        </label>
                                                    </div>
                                                    <div className='sign'>
                                                        <img src='src/images/signature2.svg' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="secondTab" className="tabcontent" style={{ display: 'none' }}>

                                        </div>
                                        <div id="thirdTab" className="tabcontent" style={{ display: 'none' }}>

                                        </div>
                                    </div>
                                </div>

                                <div className='row'>
                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                        <div className="slide-tab-btn">
                                            <button className="btn clr-btn">Cancel</button>
                                            <button className="btn clr-btn">CLEAR</button>
                                            <button className="btn save-btn">SAVE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { SelectSignature }; 