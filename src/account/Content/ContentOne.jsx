import React from 'react';
import { Link, useHistory} from 'react-router-dom';
import { useEffect, useState} from "react";
import { commonService } from '../../_services/common.service';
import { ToastContainer, toast } from 'react-toastify';
import '../../css/contentone.css'
import { useTranslation } from 'react-i18next';
import CloseIcon from '@mui/icons-material/Close';
import { Button, Modal, ProgressBar } from 'react-bootstrap';
import DoneIcon from '@mui/icons-material/Done';
import { addDocumentService } from '../../_services/adddocument.service';
import ReactTooltip from 'react-tooltip';
import Tooltip from "@material-ui/core/Tooltip";
function ContentOne({ history, location }) {
   //Upload
   const [showdrivemodel,setshowdrivemodel]=useState(false);
   const [Goodrivedata,setGoodrivedata]=useState([]);
   const [Exporttitle,setExporttitle]=useState(null)
   const [userInfo,setUserInfo]=useState({})
   const [childArr,setChildArr]=useState([])
   const usehistory = useHistory();
   const { t, i18n } = useTranslation();
    function uploadDiv() {
        var srcElement = document.getElementById('upload');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    useEffect(() => {
     getTags();
     getUserInfo()
   }, [])
    // Drag and Drop File Upload //
   function  getTags() {
    document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
        const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
     
        dropZoneElement.addEventListener("click", (e) => {
          inputElement.click();
        });
      
        inputElement.addEventListener("change", (e) => {
          if (inputElement.files.length) {
            updateThumbnail(dropZoneElement, inputElement.files);
          }
        });
      
        dropZoneElement.addEventListener("dragover", (e) => {
          e.preventDefault();
          dropZoneElement.classList.add("drop-zone--over");
        });
      
        ["dragleave", "dragend"].forEach((type) => {
          dropZoneElement.addEventListener(type, (e) => {
            dropZoneElement.classList.remove("drop-zone--over");
          });
        });
      
        dropZoneElement.addEventListener("drop", (e) => {
          e.preventDefault();
      
          // if (e.dataTransfer.files.length) {
            inputElement.files = e.dataTransfer.files;
            updateThumbnail(dropZoneElement, e.dataTransfer.files);
          // }
      
          dropZoneElement.classList.remove("drop-zone--over");
        });
      });
    
    }
      /**
       * Updates the thumbnail on a drop zone element.
       *
       * @param {HTMLElement} dropZoneElement
       * @param {File} file
       */
      function updateThumbnail(dropZoneElement, file) {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
      
        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
          dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }
      
        // First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
          thumbnailElement = document.createElement("div");
          thumbnailElement.classList.add("drop-zone__thumb");
          dropZoneElement.appendChild(thumbnailElement);
        }
      
        // thumbnailElement.dataset.label = file.name;
      
        // Show thumbnail for image files
        // if (file.type.startsWith("image/")) {
        //   const reader = new FileReader();
      
        //   reader.readAsDataURL(file);
        //   reader.onload = () => {
        //     thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
        //   };
        // } else {
        //   thumbnailElement.style.backgroundImage = null;
        // }
        console.log(file, 'filefilefile')
        commonService.setDocument(file)
        usehistory.push('/account/adddocumentsstep')

    // let fileExtExtract = file.name.split('.');
		// let	fileExtName = ('.' + fileExtExtract[fileExtExtract.length-1]).toLowerCase();
    //     if (fileExtName == '.pdf') {
    //       commonService.setDocument(file)
    //       localStorage.removeItem("addDocumentPage")
    //       usehistory.push('/account/adddocumentsstep')
    //     } else {
    //     let documentInfo = {
    //         "Document" : [{
    //             "error": false,
    //             "UploadedOn": new Date,
    //             "IsSelected": true,
    //             "Position": 1,
    //             "isFileConvertedToPdf": true,
    //             "DocumentSource": "upload",
    //             "Recipients": [],
    //             "OriginalFileName": file.name,
    //             "FileName": file.name
    //           }]
    //       }
    //       commonService.convertFileToPDF(documentInfo, file).then((apiResult) => {
    //         console.log(apiResult, '##########')
    //         documentInfo.Document[0].FileName = apiResult.Entity.FileName;
    //         documentInfo.Document[0].OriginalFileName = apiResult.Entity.OriginalFileName;
    //         documentInfo.Document[0].DocumentSource = 'upload-nonpdf';
    //         commonService.getfileData(apiResult.Entity.FileName).then(data => {
    //            commonService.setDocument(data)
    //            localStorage.removeItem("addDocumentPage")
    //         usehistory.push('/account/adddocumentsstep')
    //         })
           
    //                 });
    //     }


      }
     function underDevevelopment(){
        toast.warn('Under Development', {
          position: "top-center",
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: false,
          progress: undefined,
          });
      }
      function redirecttoTemplate(){
       
            usehistory.push({
              pathname: `/account/template/`,
              state: {path:`Home`
          }})
      }
  function  redirectToCreateDoc(){
    usehistory.push('/account/createdocument')

   }
   const Exporttodrive = () => {
    setChildArr([])
    setGoodrivedata([])
    addDocumentService.getgoogledrivedata().then((result) => {
        if (result && result.Entity) {
          result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
          setGoodrivedata(result?.Entity)
        }
    }).catch({
    })
    setExporttitle("Google Drive")
    setshowdrivemodel(true)
}
const Exporttodropboxdrive = () => {
    setGoodrivedata([])
    setChildArr([])
    addDocumentService.getdropboxdrivedata().then((result) => {
        if (result && result.Entity) {
          result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
          setGoodrivedata(result?.Entity)
        }
    }).catch({
    })
    setExporttitle("Drop Box")
    setshowdrivemodel(true)
}
const Exporttoonedrivedrive = () => {
    setGoodrivedata([])
    setChildArr([])
    addDocumentService.getonedrivedata().then((result) => {
        if (result && result.Entity) {
          result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
          setGoodrivedata(result?.Entity)
        }
    }).catch({
    })
    setExporttitle("One Drive")
    setshowdrivemodel(true)
}
function getUserInfo() {
    commonService.userInfo().then((userData) => {
        if(userData && userData.Data) {
            setUserInfo(userData.Data)
        }
      });
}

const getchilddata = (item) => {
  if(item.IsFolder &&  item.children && item.children.length == 0) {
    addDocumentService.getgoogledrivechilddata(item.Id).then((result) => {
        if (result?.Entity?.length > 0) {
            item.children = result.Entity;
            setGoodrivedata([...Goodrivedata])
        }
    })
  } else {
    item.open = item.open ? false: true
    setGoodrivedata([...Goodrivedata])
}
}

const getdropboxchilddata = (item) => {
  if(item.IsFolder &&  item.children && item.children.length == 0) {
    addDocumentService.getdropboxdrivechilddata(item.Id).then((result) => {
        if (result?.Entity?.length > 0) {
            item.children = result.Entity;
            item.open = item.open ? false: true
            setGoodrivedata([...Goodrivedata])
        }
    })
  } else {
    item.open = item.open ? false: true
    setGoodrivedata([...Goodrivedata])
}
}

const checkBoxHandler = (value, item) => {
if(value){
    setChildArr([...childArr,item])
}
else{
    let index =childArr.indexOf(e=>e.Id==item.Id)
    console.log(index)
    childArr.splice(index,1);
    setChildArr([...childArr])
}}

const getonedrivechilddata=(item)=>{
  if(item.IsFolder &&  item.children && item.children.length == 0) {
addDocumentService.getonedrivechilddata(item.Id).then((result)=>{
       if(result?.Entity?.length>0){
        item.children=result.Entity;
        item.open = item.open ? false: true
        setGoodrivedata([...Goodrivedata])
        }
 })
} else {
  item.open = item.open ? false: true
  setGoodrivedata([...Goodrivedata])
}
}

const insertHandler=()=>{
commonService.setCloudDocuments({DocsData:childArr, type: Exporttitle})
setshowdrivemodel(false)
uploadDiv()
usehistory.push('/account/adddocumentsstep')

}
    return (
      <>
        <div>
            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <h3 style={{fontSize:35,fontWeight:300}}>{t('WHAT_WOULD_YOU_LIKE_TO_DO')}</h3>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
                    <div className="create-card-box-item" onClick={redirectToCreateDoc}>
                        <img src="src/images/document-1_icon.svg" alt="" />
                        <h6 style={{fontSize:20}}>{t('CREATE_DOCUMENT')}</h6>
                        <p>{t('START_NEW_DOCUMENT')}</p>
                    </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
                    <div className="create-card-box-item" onClick={redirecttoTemplate}>
                        <img src="src/images/document-2_icon.svg" alt="" />
                        <h6 style={{fontSize:20}}>{t('CREATE_FORM_TEMPLATE')}</h6>
                        <p>{t('CREATE_DOCUMENT_BY_SELECT')}</p>
                    </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
                    <div className="create-card-box-item" onClick={uploadDiv}>
                        <img src="src/images/document-3_icon.svg" alt="" />
                        <h6 style={{fontSize:20}} className=' text-align-center'>{t('UPLOAD_YOUR_FILES')}
                          
                          <Tooltip  placement='top' title={<p style={{fontSize:11,fontWeight:300}}>Supported File Formats: .PDF,.DOC,.DOCX,.XLS,.XLSX,.JPG,.PNG,.JPEG</p>}>
                            <img className='ml-1' style={{width:10,height:10,marginBottom:2}} src="src/images/info-circle.svg" alt="info icon" />
                            </Tooltip>
                          </h6>
                        
                        <p>{t('USE_TEMPLATE_THIRD_PARTY')} </p>
                        {/* <b/>  */}
                        {/* <span>Supported files: .PDF,.DOC,.DOCX,.PPT,
                .PPTX,.XLS,.XLSX,.JPG,.PNG,.JPEG</span> */}
            </div>

            <div className="upload_sec popup_ht" id="upload" style={{ display: "none" }}>
              <div className="content">
                <div className='pop_close'>
                  <h4>{t('UPLOAD_YOUR_FILES')} 
                  <Tooltip placement='top' title='Supported File Formats: .PDF,.DOC,.DOCX,.XLS,.XLSX,.JPG,.PNG,.JPEG'>
                            <img className='ml-1' style={{width:10,height:10,marginBottom:2, backgroundcolor:"blue"}} src="src/images/info-circle.svg" alt="info icon" />
                            </Tooltip>
                  </h4>
                  <CloseIcon className="close_add_doc_pop" onClick={() => { uploadDiv(), removeSessionItem('replaceIndex') }} />
                </div>
                <h5>{t('USE_TEMPLATE_THIRD_PARTY')}</h5>
                <div className="upload_block">
                  {/* <div className="use_templete" onClick={()=>{usehistory.push('/account/template')}}>
                            <img src="src/images/folder.svg" alt='' />
                            <h3>{t('USE_TEMPLATE')}</h3>
                        </div> */}
                                                       <div className="use_templete"  onClick={()=>{if(userInfo.IsGoogleDriveEnabled){Exporttodrive()}}}>
                                                        <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/google_drive.svg" alt='' />
                                                        <div data-tip data-for='google_drive'>
                                                        {userInfo.IsGoogleDriveEnabled?
                                                        <DoneIcon  style={{color:"green"}} onClick={(event)=>{event.stopPropagation();}}/>
                                                        : <CloseIcon  style={{color:"red"}} />                      
                                                        }
                                                        <ReactTooltip id="google_drive" place="top" effect="solid">
                                                        {userInfo.IsGoogleDriveEnabled?<>Connected</>:<>Disconnected</>}</ReactTooltip>
                                                        </div>
                                                        </div>
                                                        <h3>{t('GOOGLE_DRIVE')}</h3>
                                                        {/* <h4>1,357</h4> */}
                                                    </div>
                                                    <div className="use_templete" onClick={()=>{if(userInfo.IsOneDriveEnabled){Exporttoonedrivedrive()}}}>
                                                    <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/one_drive.svg" alt='' />
                                                        <div  data-tip data-for='one_drive'>
                                                        {userInfo.IsOneDriveEnabled?<DoneIcon   onClick={(event)=>{event.stopPropagation();}} style={{color:"green"}} />:<CloseIcon style={{color:"red"}} />}
                                                        <ReactTooltip id="one_drive" place="top" effect="solid">
                                                        {userInfo.IsOneDriveEnabled?<>Connected</>:<>Disconnected</>}</ReactTooltip>
                                                        </div>
                                                        </div>
                                                        <h3>{t('ONE_DRIVE')}</h3>
                                                        {/* <h4>2,14777 Files</h4> */}
                                                    </div>
                                                    <div className="use_templete" onClick={()=>{if(userInfo.IsDropBoxEnabled){Exporttodropboxdrive()}}}>
                                                    <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/drop_box.svg" alt='' />
                                                        <div  data-tip data-for='drop_box'>
                                                        {userInfo.IsDropBoxEnabled?<DoneIcon  onClick={(event)=>{event.stopPropagation();}} style={{color:"green"}} />:<CloseIcon style={{color:"red"}} />}
                                                        <ReactTooltip id="drop_box" place="top" effect="solid">
                                                        {userInfo.IsDropBoxEnabled?<>Connected</>:<>Disconnected</>}</ReactTooltip>
                                                        </div>
                                                        </div>
                                                        <h3>{t('DROP_BOX')}</h3>
                                                        {/* <h4>1,457 Files</h4> */}
                                                    </div>
                                                    <div className="use_templete drop-zone card_design">
                                                    <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/folder_1.svg" alt='' />
                                                        <div>
                                                        <input type="file" multiple  accept=".doc,.docx,image/png,image/jpg,image/jpeg,.xls,.xlsx,.pdf" name="myFile" className="drop-zone__input" />
                                                        </div>
                                                        </div>
                                                        <h3 className="drop-zone__prompt font">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h3>
                                                        {/* <h4>2,14777 Files</h4> */}
                                                    </div>
                        {/* <div className="use_templete drop-zone">
                            <img src="src/images/folder_1.svg" alt='' className="img-fluid mx-auto d-block" />
                            <h4 className="drop-zone__prompt">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h4>
                            <input type="file" onClick={(event) => { event.target.value=null }} accept=".doc,.docx,image/png,image/jpg,image/jpeg,.ppt,.pptx,.xls,.xlsx,.pdf" name="myFile" className="drop-zone__input" />
                        </div> */}
                    </div>
                    {/* <div className="back"onClick={uploadDiv}>
                        <h6 className="fileback"><span ><img src="src/images/back.svg" alt='' /></span>{t('BACK')}</h6>
                    </div> */}
                </div>
            </div>
                </div>
            </div>
        </div>
        <Modal show={showdrivemodel}
                onHide={() => { setshowdrivemodel(false);setChildArr([]) }}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                scrollable={true}>
                <Modal.Header  >
                    <Modal.Title id="example-modal-sizes-title-lg">
                        <p> {Exporttitle}</p>
                    </Modal.Title>
                    <button type="button" className="close" style={{ backgroundColor: "#ffff" }} onClick={() => { setshowdrivemodel(false) }}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Modal.Header>
                <Modal.Body className='error-modal-content'>
                    <div>
                          <ul className="list-group">
                            {Goodrivedata.length>0?Goodrivedata.map(item=>{
                                 return(
                                    item.IsFolder === true?
                                    <div>
                                        <li key={item.id} className="list-group-item" style={{backgroundColor:"#e2dbdb",cursor:"pointer", }} onClick={()=>Exporttitle==="Google Drive"?getchilddata(item):Exporttitle==="Drop Box"?getdropboxchilddata(item):Exporttitle==="One Drive"?getonedrivechilddata(item): ""}>
                                         
                                          <i className="fa fa-folder mr-1" style={{color:"#2F7FED"}}></i>
                                           <span>{item.roleName}</span></li>
                                           {item.children.length>0 && <ul className="list-group" style={{borderRadius: '0', display:"block", display: 'block', padding: '0.2rem', background: 'rgb(226 219 219)'}}>
                                              { item.children.map(item1=>{
                                                
                                                return( <li key={item1.id} className="list-group-item" style={{cursor:"default" }}>
                                                    <input type="checkbox" style={{marginRight:"30px", height:"15px",width:"15px"}}  onClick={(event)=>{checkBoxHandler(event.target.checked,item1)}}></input>
                                                        {item1.roleName}</li>)
                                              })}
                                              </ul>}
                                           </div>
                                           :
                                         <li key={item.id} className="list-group-item">
                                            <input type="checkbox"  style={{marginRight:"30px", height:"15px",width:"15px"}}  onClick={(event)=>{checkBoxHandler(event.target.checked,item)}}></input>
                                            <span>{item.roleName}</span>
                                            </li>
                                             ) })
                        :"Drive is Empty"}
                        </ul>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => { setshowdrivemodel(false) }}>CANCEL</Button>
                    <Button variant="primary"onClick={insertHandler}>INSERT</Button>
                </Modal.Footer>
            </Modal>
        </>

    )


}



export { ContentOne }; 