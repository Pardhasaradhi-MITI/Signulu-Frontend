import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDrag, useDrop } from 'react-dnd'
import { useEffect, useState, useRef } from "react";
import { ProgressBar } from "react-bootstrap";
import Spinner from 'react-bootstrap/Spinner';
import "../../css/adddocumentstep1.css"; 
import { Document, Page, pdfjs } from "react-pdf";
import ReactTooltip from 'react-tooltip';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
function DocumentCard({
  data,
  index,
  id,
  model,
  subDocumentsLoadSuccess,
  onDocumentErrorLoad,
  onDocumentSourceErrorLoad,
  documentPageLoad,
  onPdfView,
  moreOptionDetail,
  getReplaceIndex,
  downloadDocument,
  applyTemplate,
  ondocumentRename,
  moveCard,
  onPageLoadSuccess,
  onPassword
}) {
  const wrapperRef1 = useRef(null);
  const { t, i18n } = useTranslation();
  const [prog, setProg]=useState(0)
  const dndRef = useRef(null);
  const pageRef = useRef(null);
  const [{ handlerId }, drop] = useDrop({
    accept: 'card',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      }
    },

    hover(item, monitor) {
      if (!dndRef.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = index

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }
      // Determine rectangle on screen
      const hoverBoundingRect = dndRef.current?.getBoundingClientRect()
      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      // Determine mouse position
      const clientOffset = monitor.getClientOffset()
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      //   if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      //     return
      //   }
      // Dragging upwards
      //   if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      //     return
      //   }
      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)
      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })
  const [{ isDragging }, drag] = useDrag({
    type: 'card',
    item: (data) => {
      return { id, index }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  function getModalData(index) {
    model(index)
  }
  function getPdfView(data) {
    onPdfView(data)
  }

  function getOnDocumentRename(e, data, index) {
    ondocumentRename(e, data, index)
  }
  function getDocumentReplaceIndex(e, index) {
    getReplaceIndex(e, index)
  }
  function getMoreOptionDetail(index) {
    moreOptionDetail(index)
  }
  function getDocumentPageLoad(e, index) {
    documentPageLoad(e, index)
  }
  function getsubDocumentsLoadSuccess(e, index) {
   data.process=false
    subDocumentsLoadSuccess(e, index)
  }
  function getDocumentDownload(e, data, index) {
    downloadDocument(e, data, index)
  }
  function getApplyTemplate(e, data, index) {
    applyTemplate(e, data, index)
  }
  function getOnLoadSuccess(e, index) {
   data.process=false;
    const importPDFCanvas= document.querySelector('.import-pdf-page canvas');
    if (pageRef && pageRef.current && pageRef.current.pageElement && pageRef.current.pageElement.current
      && pageRef.current.pageElement.current.firstChild ) {
        onPageLoadSuccess(e, index, data, pageRef.current.pageElement.current.firstChild.toDataURL())
      } else {
        onPageLoadSuccess(e, index, data, importPDFCanvas.toDataURL())
      }
  
  }
  
  function  getDocumentErrorLoad(e, index, data) {
    onDocumentErrorLoad(e, index, data)
  }
  drag(drop(dndRef))
  const loadSpinner=()=>{
    return <div style={{display:"flex",alignContent:'center',alignItems: "center",justifyContent: "center",marginTop:'30%'}}>
    <Spinner variant="primary"  animation="border"/></div>}

  function progressHandler(e){
    setProg((e.loaded / e.total) * 100 )
  }

  const useOutsideAlerter1 = (ref1) => {
    useEffect(() => {
        function handleClickOutside(event) {
            if (ref1.current && !ref1.current.contains(event.target) && event.target.id != 'user_profile_id') {
              getMoreOptionDetail()
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref1]);
    
}
useOutsideAlerter1(wrapperRef1);

  return (
      <div className='col-3'>
        <div className='file file-upload' id="file">
          <div className="file-close-btn-wrapper" data-tip={'cardDelete'} data-for="cardDelete">
            <a href='/' className='file-close-btn file-tooltip' onClick={(event) => { event.preventDefault(); getModalData(index) }}><svg xmlns="http://www.w3.org/2000/svg" className='vertical-initial' fill="none" viewBox="0 0 24 24" stroke="#fff" strokeWidth="2">
              <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
            </svg>
              {/* <span className="file-tooltiptext">Delete</span> */}
            </a>
            <ReactTooltip id="cardDelete" place="top" effect="solid">
            Delete
            </ReactTooltip>
          </div>
          <div className='file-preview' ref={dndRef} key={index + data?.documentName}>
            <div className="content">

              {(data && data?.canvasImageUrl) ? <img src={data?.canvasImageUrl} alt="" /> :
              <Document
                file={data?.document} 
                className="import-pdf-page"
                loading={loadSpinner}
                onLoadSuccess={(e) => getsubDocumentsLoadSuccess(e, index)}
                onLoadProgress={(e)=>progressHandler(e)}
                onLoadError={(e) => getDocumentErrorLoad(e, index, data)}
                onSourceError={onDocumentSourceErrorLoad}
                onPassword={(e) => { e.preventDefault();
                  onPassword(e, index, data) ;
                return false}}>

                <Page onRenderSuccess={(e) => getOnLoadSuccess(e, index, data)} ref={pageRef}  className={"mx-auto d-block document_intial import-pdf-page" + ' ' + index} scale={1} noData={'Please select a File'} pageNumber={1} />
              </Document>
               } 
            </div>

            <div className="file-view-wrapper">
              <a href="/" className='file-view-btn' onClick={(e) => { e.preventDefault(); getPdfView(data) }}>view</a>
            </div>
          </div>
          <div className='progressbar_doccard'>
          {data.process?<p style={{fontSize:"10px"}}>{Math.round(prog)} % uploaded</p>:null}
          {data.process?<ProgressBar style={{ width: "100%", height: "1vh",marginLeftL:"4px"}} animated now={prog} />:null}
          </div>
          <div className='file-detail-container'>
            <div className="more-detail-wrapper">
              <div>
              <p className='file-detail overflowtestdots' style={{lineHeight:'1.5', display: 'block', width: '130px'}}  data-tip={data?.documentName +index} data-for={data?.documentName+index}>{data?.documentName} </p>
                <ReactTooltip id={data?.documentName +index} place="top" effect="solid">
                  {data?.documentName}
            </ReactTooltip>
                {(data?.Pages) ? <p className='file-detail-pages'>No of Pages: {data?.Pages}</p> : null}
              </div>
              <div className='more-detail-list-wrapper' data-tip={'cardmoreOptions'} data-for="cardmoreOptions">
                <a href='/' id="user_profile_id" className='more-detail file-detail-tooltip' onClick={(e) => { e.preventDefault(); getMoreOptionDetail(index) }}>
                  <svg className='more-detail-icon' xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z" />
                  </svg>
                  {/* <span className="file-detail-tooltiptext">More Options</span> */}
                  <ReactTooltip id="cardmoreOptions" place="top" effect="solid">
                  More Options
            </ReactTooltip>
                </a>
                {(data?.isMoreOption) ?
                  <ul className='more-option-wrapper' ref={wrapperRef1} style={{width: '25%', borderBottom: '1px solid rgb(204, 204, 204)'}}>
                    {/* <li className='more-option-item'><a href="/" className='more-option-link' onClick={(e) => { e.preventDefault(); applyTemplate(e, data, index) }}>Apply Templates</a></li> */}
                    <li className='more-option-item'><a href="/" className='more-option-link' onClick={(e) => { e.preventDefault(); getDocumentReplaceIndex(e, index) }}>Replace</a></li>
                    <li className='more-option-item'><a href="/" className='more-option-link overflowtestdots'style={{width: '170px'}} onClick={(e) => { e.preventDefault(); getDocumentDownload(e, data, index) }}>Download {data?.documentName}</a></li>
                    <li className='more-option-item'><a href="/" className='more-option-link' onClick={(e) => { e.preventDefault(); getOnDocumentRename(e, data, index) }}>Rename Document</a></li>
                    {/* <li className='more-option-item'><a href="/" className='more-option-link' onClick={(e) => {
                      e.preventDefault();
                      getModalData(index)
                    }}>Delete Document</a></li>
                    <li className='more-option-item'><a href="/" onClick={(e) => {
                      e.preventDefault(); getPdfView(data)
                    }} className='more-option-link'>View Document</a></li> */}

                  </ul>
                  : null}
              </div>
            </div>

          </div>
        </div>
      </div>
  )
}
export { DocumentCard }; 