import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';

function TemplatesCard(props){
    return (
        <tr>
            <td><input className='check' type="checkbox" name="" id={'check_'+props.id} value={props.id} /><label htmlFor="check"> {props.title}</label></td>
            <td>{props.client}</td>
            <td>{props.date}</td>
            <td>{props.user}</td>
            <td colSpan={2}><div className='table_btn'><button className={'btn '+props.cssclass}>{props.status}</button><a href={props.detailsurl} className="btn more-btn">More<span>...</span></a></div></td>
        </tr>

    )
}
export { TemplatesCard }; 