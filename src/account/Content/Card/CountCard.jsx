import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { useTranslation } from 'react-i18next';
import { useHistory} from 'react-router-dom';

import { useState } from 'react';
import {ReactTooltip} from 'react-tooltip';
import Tooltip from "@material-ui/core/Tooltip";


function CountCard(props){
    const history = useHistory();
    const { t, i18n } = useTranslation();
    const applyfilter=(updateState)=>{
        history.push("manage", { state: updateState,loginuseremail:props.useremail });
    }
    if(props.title==="Rejected/Cancelled"){
        return (
           <Tooltip placement='top' title={props.title ==="Rejected/Cancelled"?<><p style={{fontSize:13,fontWeight:300}}>Rejected: Documents that are rejected by recipients for various reasons. (Note: Rejected documents can be resent after correction)</p>
            <p style={{fontSize:13,fontWeight:300}}>Cancelled: Documents that are created by me and cancelled by me for various reasons.(Note: Cancelled documents cannot be resent).</p> 
           </>:""}><div className="item" style={{cursor:"pointer"}}>
            <img src={props.image} alt="" />
            <div className='flex justify-content-start'>
            <div onClick={()=>applyfilter("REJECTED")}><h5>{props.counter}</h5></div>
            <div onClick={()=>applyfilter("CANCELLED")}><h5>/ {props.counter1}</h5></div></div>
            <p>{t(props.title)}</p>
           </div></Tooltip> )
    }else{
        return (
           <Tooltip placement='top' title={props.title==="Drafts"?<p style={{fontSize:13,fontWeight:300}}>Documents that are in progress.</p>:
           props.title==="To Sign"?<p style={{fontSize:13,fontWeight:300}}>Documents pending for my signature.</p>:
           props.title ==="Waiting for Others"?<p style={{fontSize:13,fontWeight:300}}>Documents pending for others’ signatures.</p>:
           props.title ==="My Documents"?<p style={{fontSize:13,fontWeight:300}}>Documents that are initiated by me and signed/approved by other recipients.</p>:
           props.title ==="Total Signed Documents"?<p style={{fontSize:13,fontWeight:300}}>Total Documents that are signed/approved.</p>:
           props.title ==="Rejected/Cancelled"?`Rejeced: Documents that are rejected by recipients for various reasons. (Note: Rejected documents can be resent after correction),
           Cancelled: Documents that are created by me and cancelled by me for various reasons.(Note: Cancelled documents cannot be resent).`:
           ""} arrow="true" >
        <div  onClick={()=>applyfilter(props.title==="Drafts"?"DRAFT":props.title==="To Sign"?"TOSIGN":
        props.title==="Waiting for Others"?"WAITFOROTHERS":props.title==="My Documents"?"MYDOCUMENTS":props.title === "Total Signed Documents"?"TOTALDOCUMENTS":""
           
            
            )} className="item" style={{cursor:"pointer"}}>
        <img src={props.image} alt="" />
        <h5>{props.counter}</h5>
        <p>{t(props.title)}</p>
       </div>
       </Tooltip> 
       )
    }
    
}
export { CountCard }; 