import React, { useState,useEffect,useRef } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { TemplatesCard } from '../Content/Card/TemplatesCard';
import { useTranslation } from 'react-i18next';
import { manageservice } from './../../_services/manageservice';
import Tooltip from "@material-ui/core/Tooltip";
import moment from 'moment';
import { Button, ButtonToolbar, Modal,ListGroup,ListGroupItem } from 'react-bootstrap';
import {usersService} from "./../../_services/users.service"
import DatePicker from "react-datepicker"
import { Calendar } from 'primereact/calendar';
import config from 'config';
import Swal from "sweetalert2"
import CloseIcon from '@mui/icons-material/Close';
import {Tag,XCircle} from "react-feather"
import ReactToPrint from 'react-to-print';
import { Link, useHistory,useParams,useLocation} from 'react-router-dom';


function ContentTHree({ history, location }) {
    const { t, i18n } = useTranslation();
    const wrapperRef2 = useRef(null);
    const usehistory = useHistory();
    const [doclist,setdoclist]=useState([]);
    const [currentdocid,setcurrentdocid] = useState(null);
    const [deletedocreason,setdeletedocreason]=useState(null);
    const [showdeletedocumentmodule,setshowdeletedocumentmodule] = useState(false)
    const [Selecteddoc,setSelecteddoc] = useState(null)
    const [updatedManagevalidityDate,setupdatedManagevalidityDate]=useState(null);
    const [showmanagevaliditydocumentmodule,setshowmanagevaliditydocumentmodule]=useState("");
    const [showAuditmodule,setshowAuditmodule] = useState(false)
    const [Audittraildata,setAudittraildata]=useState([]);
    const [showsharedocument,setshowsharedocument]=useState(false)
    const [Goodrivedata,setGoodrivedata]=useState([])
    const [showdrivemodel,setshowdrivemodel]=useState(false);
    const [showdrivemodel1,setshowdrivemodel1]=useState(false);
    const [templatecategoryslist,settemplatecategoryslist]=useState([]);
    const [Selectedtemplatecategory,setSelectedtemplatecategory]=useState("");
    const [showcreatetemplatemodule,setshowcreatetemplatemodule]=useState(false);
    const [Exporttitle,setExporttitle]=useState(null)
    const[selectedfolder,setselectedfolder]=useState({});
    const [isfolderopen,setisfolderopen]=useState(false);
    const printRef = useRef(null);
    const [childitem,setchilditem]=useState({})
    const [tags, setTags] = useState([]);
    const [prefill,setprefill]=useState("");
    const [Ispublic,setIspublic]=useState("");
    const [Title,setTitle]=useState("");
    const [userdata,setuserdata]=useState({})
    const [Inputfields,setinputfield]=useState([
        {EmailAddress:"",
        FirstName: "",
        IsDelete: 0,
        LastName: "",
        RecipientId:null ,
        SendToUser: true,
        id: null  } ,
     ])
    // function explore_all_data() {
    //     console.log('content heare')
    // }
    const API_END_POINT = config.serverUrl;

    let data=
    {"OwnedBy": null,
    "PageNumber": 1,
    "PageSize": 10,
    "SearchByText": "",
    "SharedUser": null,
    "StatusCode": null,
    "Title": null,
    "IsReact":1,
     }
    useEffect(()=>{
        getdoclist(data)
        getCurrentUserData()  
    },[])
  const getdoclist=(data)=>{
    manageservice.getdoclist(data).then((result)=>{
      if(result?.Entity?.data){
        setdoclist(result?.Entity?.data)
      }
    }).catch({

    })
  }
   // table more dropdown
   function dropdown4(id){
    if(currentdocid === null){
        setcurrentdocid(id)
    }
    else if(id !== currentdocid){
        let item1=currentdocid.toString();
        var srcElement1 = document.getElementById(item1)
        if (srcElement1 != null) {
                srcElement1.style.display = "none";
            }
        setcurrentdocid(id)
    }
    let item=id.toString();
    var srcElement = document.getElementById(item)
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = "none";
        } else {
            srcElement.style.display = "block";
        }
        return false;
    }
}
const docreasondelete =(e)=>{
    setdeletedocreason(e.target.value)
 }
 const deletedocument=()=>{
     let data={
        "Ids": [Selecteddoc.ADocumentId],
        "NotifyRecipients": false,
        "Reason": deletedocreason,
     }

     manageservice.deletedocument(data).then((result)=>{
           if(result.HttpStatusCode === 200){
            // setShowDelertalert(true)
            getdoclist();
           }
     }).catch({

     })
 }
 const getCurrentUserData = ()=>{
    usersService.getCurrentUserData()
    .then((result) => {
        if(result){
            // setUserInfo(result.Data)
             setuserdata(result.Data)
        }
    })
}

 const updatemanagevalidity=()=>{
    let date = moment(updatedManagevalidityDate).add(1,'d')
    let data={"ADocumentId":Selecteddoc.ADocumentId,"ExpirationDate":date}
   manageservice.Updatemanagevalidity(data,Selecteddoc.ADocumentId).then((result)=>{
    if(result.HttpStatusCode === 200){
        getdoclist();
        Swal.fire({
            icon: 'success',
            title: 'Document Validity Date Updated',
            showConfirmButton: true,
            confirmButtonText:"OKAY",
           // timer: 2000
          }) 
    }
   }).catch({

   })
}
const Getrecipients=(id)=>{
    let ReIDs=[];
    manageservice.getRecipientsbasedondoc(id).then((result)=>{
       if(result?.Entity){
         
         result?.Entity.map(item=>{
            ReIDs.push(item.RecipientId)
         })
        //  setRecipientIds(ReIDs)
       }
    })
    Swal.fire({  
        text: 'An email with a link will be sent to all recipients.',  
         showCancelButton: true,  
        confirmButtonText: `SEND`,
        cancelButtonText:`CANCEL`, 
        showConfirmButton:true 
      }).then(function(isConfirm) {  
        let d = new Date();
        let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
        let timeSplit = time.split(' ');
        let timeZone = timeSplit[1];
          if (isConfirm.isConfirmed === true) { 
            // const datea=(moment(Date()).format('YYYY-MM-DD h:mm:ss')).toString();
            const datea=moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            const timezonea = timeZone;
            let datap={"ADocumentId":id,"DateTime":`"${datea.toString()}"`, "TimeZone":`"${timezonea.toString()}"`,"RecipientIds":ReIDs}  
            manageservice.Senddocumenttorecipients(datap).then((result)=>{
               if(result.HttpStatusCode=== 200){
                //setRecipientIds(null) 
              //  Swal.fire('Sent', '', 'success') 
                Swal.fire({
                    icon: 'success',
                    title: 'Sent',
                    showConfirmButton: true,
                    confirmButtonText:"OKAY",
                   // timer: 2000
                  }) 
                
               }
            }) 
          } else { 
            //setRecipientIds(null)   
              Swal.fire('Document has not been sent', '', 'info')  
             
           }
      });
}
 const Revokethedocment=(id)=>{
    let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
    //const date=(moment(new Date()).format('YYYY-MM-DD h:mm:ss')).toString();
    const date=moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    const timezone = timeZone;
    
    Swal.fire({
        title: "Are You Sure You Want to Cancel Document",
        text: "Please Enter the reason for the cancellation of Document",
        input: "text",
        showCancelButton: true,
        confirmButtonColor: "#1c8fec",
        cancelButtonColor: "#fa013b",
        cancelButtonText:"CANCEL",
        confirmButtonText:"OKAY",
        width:"800px"
      }).then(function(result){
        if (result.isConfirmed === true &&result.value ==='') {
          //  Swal.fire('You can not cancel without giving a reason', '', 'info')
            Swal.fire({
                icon: 'success',
                title: 'You can not cancel without giving a reason',
                showConfirmButton: true,
                confirmButtonText:"OKAY",
               // timer: 2000
              })
            .then((result)=>{
                if(result.isConfirmed){
                    Revokethedocment(id)
                }
            }) 
        }
        else if(result.isConfirmed === true && result.value !== ""){
            let payload ={"ADocumentId":id,"DateTime":`"${date.toString()}"`,"NotifyRecipients":false,"Reason":result.value,"TimeZone":`"${timezone.toString()}"`}
           manageservice.Revokedocument(payload).then((result)=>{
             if(result.HttpStatusCode === 200){
                getdoclist();
               // Swal.fire('Document Cancelled!', '', 'success') 
                Swal.fire({
                    icon: 'success',
                    title: 'Document Cancelled!',
                    showConfirmButton: true,
                    confirmButtonText:"OKAY",
                   // timer: 2000
                  })
             }
             else{
               // Swal.fire('Document Not Revoked', '', 'info') 
                Swal.fire({
                    icon: 'info',
                    title: 'Document Not Revoked',
                    showConfirmButton: true,
                    confirmButtonText:"OKAY",
                   // timer: 2000
                  })
             }
           }).catch({

           })
        
        }
  
 })
}
const Createtemplate=(id)=>{
    manageservice.gettemplateCategorys().then((result)=>{
        if(result){
            settemplatecategoryslist(result?.Entity)
        }
    }).catch({

    })
    setshowcreatetemplatemodule(true)
    //  Swal.fire({
    //     title:"Create Template",
    //     //width:"800px",  
    //     html: 
    //        `<div>
    //        <label >Title</label>
    //        <input id="title" class="form-control mt-1" type="text" placeholder="Title is Required" aria-label="default input example">
    //        </div>
    //        <div class="form-check form-check-inline mt-4 ml-2">
    //        <input class="form-check-input" type="checkbox" id="Ispublic" value="option1">
    //        <label class="form-check-label mt-1" for="inlineCheckbox1">Is Public</label>
    //        </div>
    //        <div class="form-check form-check-inline mt-4 ml-2">
    //        <input class="form-check-input" type="checkbox" id="prefill" value="option2">
    //        <label class="form-check-label mt-1" for="inlineCheckbox2">Pre-fill</label></div>`,
    //     showCancelButton: true,
    //     confirmButtonColor: "#1c8fec",
    //     cancelButtonColor: "#fa013b",
    //     preConfirm:()=>{
    //         var Ispublic = Swal.getPopup().querySelector('#Ispublic').checked
    //         var prefill = Swal.getPopup().querySelector('#prefill').checked
    //         var Title = Swal.getPopup().querySelector('#title').value
    //         return {Ispublic: Ispublic, prefill: prefill,Title:Title}
    //     }
    //  }).then((result)=>{
    //      if(result.isConfirmed === true &&result.value.Title !== ""){
    //         let payload={"ADocumnetId":id, "IsPrefilled":result.value.prefill,"IsPublic":result.value.Ispublic,"TemplateTitle":result.value.Title}
    //         manageservice.CreateTemplate(payload).then((result)=>{
    //           if(result.HttpStatusCode === 200){
    //             Swal.fire('Template Created!', '', 'success') 
    //           }
    //         }).catch({})
            
    //      }
    //      else if(result.isConfirmed === true && result.value.Title === "")
    //       {
    //         Swal.fire('You can not Created Template without giving a Title', '', 'info').then((result)=>{
    //             if(result.isConfirmed){
    //                 Createtemplate(id)
    //             }
    //         })
    //      }
    //  })
}
const Savetemplate=()=>{
    let alltags =tags.toString()
    let payload={
    "ADocumnetId": Selecteddoc.ADocumentId,
    "IsPrefilled":prefill,
    "IsPublic":Ispublic,
    "TemplateTitle":Title,
    "TemplateCategoryId":Selectedtemplatecategory,
    "Tags":alltags
    }
   Title==="" 
//    || Selectedtemplatecategory==="" || alltags===""?
//     Swal.fire(`Please fill all mandatory fields`, '', 'info') 
//     :
    manageservice.CreateTemplate(payload).then((result)=>{
        if(result.HttpStatusCode === 200){
          setshowcreatetemplatemodule(false)
          setSelectedtemplatecategory("")
          setIspublic("")
          setTitle("")
          setprefill("")
          setTags([])
        //  Swal.fire('Template Created', '', 'success') 
          Swal.fire({
            icon: 'success',
            title: 'Template Created',
            showConfirmButton: true,
            confirmButtonText:"OKAY",
           // timer: 2000
          })
          
        }
    }).catch({})
    
}
// const DownloadDocument=(id)=>{
//     //window.open('/index1.php/api/adocuments/download/design/pdf/'+id);
//     window.open(API_END_POINT + 'adocuments/download/design/pdf/' + id, '_blank').focus();
//     //window.open(API_END_POINT + 'billing/' + invoiceId + '/pdf/' + userData.user_id, '_blank').focus();
// }
const DownloadDocument=(item)=>{
    window.open(API_END_POINT+`adocuments/download/design/pdf/`+item.ADocumentId);
    // let FileName = item.Title
    // let Extension = item.StatusCode ==="APPROVED"?'.zip':'.pdf'
    // if(item.Title) {
    //     FileName = item.Title.split('.pdf')[0]  + Extension
    // }
    // manageservice.downloadDocument(item.ADocumentId)
    // .then(response=> {
    //     const blob = new Blob([response], { type: 'application/octet-stream' });
    //     const element = document.createElement('a');
    //     element.href = URL.createObjectURL(blob);
    //     element.download = FileName;
    //     document.body.appendChild(element);
    //     element.click();
    // })
}
const Resendotp =(id)=>{
    manageservice.Resendotp(id).then((result)=>{
       if(result.HttpStatusCode === 200){
          // Swal.fire('Resend OTP', '', 'success') 
           Swal.fire({
            icon: 'success',
            title: 'Resend OTP',
            showConfirmButton: true,
            confirmButtonText:"OKAY",
           // timer: 2000
          })
       }
    })
}
const ViewRejectedreason =(ADocumentId)=>{
    addDocumentService.getrejecteddocumentreasion(ADocumentId).then(result=>{
        if(result){
             Swal.fire('', `${result?.Entity.actionReason}`, '') 
        }
    })
//    Swal.fire(`${reason}`, '', '') 
}

const Statuswidth  = (status , total , approvedCount) => {
    let count = total
    let Number = approvedCount
    let val = (Math.round((Number/(count)*100)))
    // let val = status ==="DRAFT"? 10:status ==="SENT"?vale :status ==="REJECTED"?0: status ==="CANCELLED"?0 :status ==="APPROVED"?100:0

    return (
        <>{status !== "APPROVED" ? <><div className="progress" style={{ width: "100px", height: '0.5rem' }}>
            <div className="progress-bar bg-success" role="progressbar" style={{ width: val + "%" }} aria-valuenow={val} aria-valuemin="0" aria-valuemax="100"></div>
        </div><span style={{ fontSize: '12px', marginTop: '-5px',marginLeft:'3px' }}>{Number}/{count} done</span></>:<p>Completed</p>}
        </>
    )
}

const getaudittrailinfo=(id)=>{
    manageservice.getaudittrailinfo(id).then((result)=>{
       if(result){
        setAudittraildata(result?.Entity)
        setshowAuditmodule(true)
       }
    }).catch({

    })

}
const reactToPrintContent = React.useCallback(() => {
    return printRef.current;
  }, [printRef.current]);

  const handilechangeinput =(index,e)=>{
        const values=[...Inputfields]
        values[index][e.target.name]=e.target.value;
        values[index].RecipientId=-(index+1)
        values[index].id=(`choice${(index+1)}`)
        //values[index][SendToUser]=true
       // values[index][IsDelete]=0
        setinputfield(values)
  }
  const addrecipients=()=>{
    Inputfields.map(item=>{
         if(item.EmailAddress==="" ||item.FirstName==="" ||item.LastName===""){
          //  Swal.fire("Please Fill All Details then only need to add new ","","info")
            Swal.fire({
                icon: 'info',
                title: 'Please Fill All Details then only need to add new ',
                showConfirmButton: true,
                confirmButtonText:"OKAY",
               // timer: 2000
              })
         }
         else{
            setinputfield([...Inputfields,{
                EmailAddress:"",
                FirstName: "",
                IsDelete: 0,
                LastName: "",
                RecipientId:null ,
                SendToUser: true,
                id: null }])
         }
    })
   
  }
  const removeRecipients=(index)=>{
    const values=[...Inputfields]
    values.splice(index,1)
    setinputfield(values)
  }
  const Sharedocument=()=>{
    let payload1={"ADocumentId":Selecteddoc.ADocumentId,"Recipients":Inputfields}
    manageservice.ShareDocument(payload1).then((result)=>{
       if(result){
        if(result.HttpStatusCode=== 200){
            setshowsharedocument(false)
            setinputfield([])
            Swal.fire({
                icon: 'success',
                title: 'Document has been Shared',
                showConfirmButton: true,
                confirmButtonText:"OKAY"
                //timer: 2000
              }) 
              
        }
       }
    }).catch({

    })
  }
  const Exporttodrive=()=>{
    
    manageservice.getgoogledrivedata().then((result)=>{
         if(result){
           setGoodrivedata(result?.Entity)
         }
    }).catch({

    })
    setExporttitle("Google Drive")
    setshowdrivemodel(true)
 }
 const getchilddata=(item)=>{
   setselectedfolder(item);
    manageservice.getgoogledrivechilddata(item.Id).then((result)=>{
          if(result?.Entity.length>0){
           item.children=result.Entity;
           setchilditem(item)
           }
    })
   setisfolderopen(!isfolderopen);
   }
 const Exporttodropboxdrive=()=>{
   
    manageservice.getdropboxdrivedata().then((result)=>{
         if(result){
           setGoodrivedata(result?.Entity)
         }
    }).catch({

    })
   
    setExporttitle("Drop Box")
    setshowdrivemodel(true)
 }
 const getdropboxchilddata=(item)=>{
   setselectedfolder(item);
    manageservice.getdropboxdrivechilddata(item.Id).then((result)=>{
          if(result?.Entity.length>0){
           item.children=result.Entity;
           setchilditem(item)
           }
    })
   setisfolderopen(!isfolderopen);
   }

   const Exporttoonedrivedrive=()=>{
   
       manageservice.getonedrivedata().then((result)=>{
            if(result){
              setGoodrivedata(result?.Entity)
            }
       }).catch({
  
       })
      
       setExporttitle("One Drive")
       setshowdrivemodel(true)
    }
    const getonedrivechilddata=(item)=>{
      setselectedfolder(item);
       manageservice.getonedrivechilddata(item.Id).then((result)=>{
             if(result?.Entity.length>0){
              item.children=result.Entity;
              setchilditem(item)
              }
       })
      setisfolderopen(!isfolderopen);
      }

   const ExporttoCloud=()=>{
   {selectedfolder.IsFolder === true?Selecteddoc.FolderId= `${selectedfolder.Id}`:""}
       {Exporttitle==="Drop Box"? manageservice.savetodropbox(Selecteddoc).then((result)=>{
           if(result){
               if(result?.HttpStatusCode === 200){
                   Swal.fire({
                       icon: 'success',
                       title: 'Document has been exported successfully.',
                       showConfirmButton: true,
                       confirmButtonText:"OKAY"
                       //timer: 2000
                     })
               }
               else{
                   Swal.fire({
                       icon: 'worning',
                       title: 'Somthing is Wrong',
                       showConfirmButton: true,
                       confirmButtonText:"OKAY"
                       //timer: 2000
                     })
               }
           }
       }): 
       Exporttitle==="Google Drive"?manageservice.savetoGoogledrive(Selecteddoc).then((result)=>{
           if(result){
               if(result?.HttpStatusCode === 200){
                   Swal.fire({
                       icon: 'success',
                       title: 'Document has been exported successfully.',
                       showConfirmButton: true,
                       confirmButtonText:"OKAY",
                       //timer: 2000
                     })
               }
               else{
                   Swal.fire({
                       icon: 'worning',
                       title: 'Somthing is Wrong',
                       showConfirmButton: true,
                       confirmButtonText:"OKAY",
                       //timer: 2000
                     })
               }
           }
       }
       ):Exporttitle==="One Drive"?manageservice.savetoonedrive(Selecteddoc).then((result)=>{
           if(result){
               if(result?.HttpStatusCode === 200){
                   Swal.fire({
                       icon: 'success',
                       title: 'Document has been exported successfully.',
                       showConfirmButton: true,
                       confirmButtonText:"OKAY",
                       //timer: 2000
                     })
               }
               else{
                   Swal.fire({
                       icon: 'worning',
                       title: 'Somthing is Wrong',
                       showConfirmButton: true,
                       confirmButtonText:"OKAY",
                       //timer: 2000
                     })
               }
           }
       }
       ): ""}
       setshowdrivemodel(false)
   }
   const openApprovePage = (item) => {
    usehistory.push('/account/documentagreement/'+ item.ADocumentId)
}
const viewApprovePage = (item) => {
    usehistory.push('/account/documentagreementview/'+ item.ADocumentId)
}
const EditDocument =(documentID)=>{
    usehistory.push({
        pathname: `/account/createdocument`,
        state: {DocumentID:`${documentID}`
    }})
}
  

    return (
        <div>
            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <h3 style={{fontSize:"25px" ,fontWeight:300}}>{t('RECENTLY_MODIFIED')}</h3>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div className="table__row__conts table-responsive">
                        <table className="table">
                        <thead style={{visibility:"collapse"}} className='first_head'>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                {/* 1-8 on <span>8</span> */}
                                                </th>
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th style={{ cursor:"auto",position:"relative",textAlign:"start",fontSize:14,paddingLeft:47}}> Document</th>
                                            <th className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>Shared with</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14}}>Created On</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14}}>Updated On</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14}}>Owner</th> 
                                            <th style={{fontSize:14}}>Status</th>
                                            {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead>
                            <tbody>
                                {doclist.length>0&&doclist.map(item=>{
                                   return(
                                    <tr>
                                     <td  style={{position:"relative",textAlign:"start",width:"28%"}}
                                    //    onClick={()=>{dropdown4(item.ADocumentId)}}
                                       >
                                               {/* <img src='src/images/down_arrow.svg' alt=""  style={{padding:5,border:"1px solid #c7a9a9 ",backgroundColor:"#ededed"}}/> */}
                                              <button style={{padding:0}} id={`${"dropdownMenuButton1"+item.ADocumentId}`} data-bs-toggle="dropdown" aria-expanded="false" className="btn doc_btn"><img   src="src/images/plus_icon.svg" alt="" /></button>
                                                <label className='px-2' style={{cursor:"auto"}}   htmlFor="check1">
                                                {item.Title!== null&&item.Title.length>20?
                                                <Tooltip placement="top" title={item.Title} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                      width: 180,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        marginBottom:"-4px"
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.Title}
                                                    </div>
                                                </Tooltip>:item.Title}
                                                
                                                </label>
                                                {/* <div  id={item.ADocumentId} className=" dropdown-menu" style={{ display: 'none' }}> */}
                                               {/* <ul className=" pl-0 mb-0 dropdown-menu p-2"  aria-labelledby={`${"dropdownMenuButton1"+item.ADocumentId}`}>
                                               {item.IsOwner === 1 && (item.StatusCode === "DRAFT")&& item.WorkflowCode ==='STANDARD' && item.OwnedBy === "You" && item.IsFromScratchDocument === 1 ?
                                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>{EditDocument(item.ADocumentId)}}>Edit</a></li>:""}  
                                                    {item.IsOwner === 1 && (item.StatusCode === "DRAFT" || item.StatusCode === "REJECTED")&& item.orkflowCode ==='STANDARD' && item.OwnedBy === "You" ?
                                                    <li><a className="dropdown-item" href="#">Design Mode</a></li>:""}
                                                    {item.IsSecured === 1 && item.StatusCode === "SENT" && item.TotalRecipients !== item.RecipientApprovedCount?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{Resendotp(item.ADocumentId)}}><a className="dropdown-item" >Resend OTP</a></li>:""}
                                                    {(item.IsOwner === 1 && item.StatusCode === "APPROVED") || (item.StatusCode === "DRAFT" && item.TotalRecipients === 1) && item.orkflowCode ==='STANDARD' ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowsharedocument(true)}}><a className="dropdown-item" >Share</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "SENT" &&item.OwnedBy === "You" ?
                                                    <><li style={{cursor:"pointer"}} onClick={()=>{Getrecipients(item.ADocumentId)}}><a className="dropdown-item">Resend</a></li>
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowmanagevaliditydocumentmodule(true)}}><a className="dropdown-item">Manage Validity</a></li>
                                                    <li style={{cursor:"pointer"}} onClick={()=>{Revokethedocment(item.ADocumentId)}}><a className="dropdown-item">Revoke</a></li>
                                                    </>
                                                    :""}
                                                    {item.IsOwner === 1 && (item.StatusCode === "DRAFT" || item.StatusCode === "REJECTED") && item.TotalRecipients > 1 && item.OwnedBy === "You" && item.IsFromScratchDocument === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{history.push(`documentselect/${item.ADocumentId}`)}}><a className="dropdown-item" >Fill and Send</a></li>:""}
                                                    {item.StatusCode === "SENT" && item.ViewerStatusCode === "SENT" && item.Position === null || item.Position === 1 && item.OwnedBy === "You"?
                                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>openApprovePage(item)}>{item.RecipientStatus}</a></li>:""}

                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsGoogleDriveEnabled === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttodrive(item.ADocumentId)}} ><a className="dropdown-item">Export to drive</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsOneDriveEnabled === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttoonedrivedrive(item.ADocumentId)}} ><a className="dropdown-item" >Export to one drive</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsDropBoxEnabled === 1 ?
                                                    <li  style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttodropboxdrive(item.ADocumentId)}} > <a className="dropdown-item">Export to drop box</a></li>:""}
                                                    {item.IsOwner === 1 && item.WorkflowCode === "STANDARD" && item.OwnedBy === "You" ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item), Createtemplate(item.ADocumentId)}} ><a className="dropdown-item">Create Template</a></li>:""}
                                                      {item.IsOwner === 1 && item.WorkflowCode === "STANDARD"?
                                                    <li style={{cursor:"pointer"}}  onClick={()=>{getaudittrailinfo(item.ADocumentId)}}><a className="dropdown-item">Audit Trail</a></li>:""}
                                                    {(item.IsOwner === 1 && item.StatusCode !== "SENT") ?item.StatusCode !== "CAMPAIGNSTARTED"?
                                                    <li onClick={()=>{setSelecteddoc(item),setshowdeletedocumentmodule(true)}}><a className="dropdown-item">Delete</a></li>:"":""}
                                                     {item.WorkflowCode === "STANDARD"?
                                                     <>
                                                     <li><a className="dropdown-item" onClick={()=>viewApprovePage(item)}>View</a></li>
                                                     <li style={{cursor:"pointer"}} onClick={()=>{DownloadDocument(item.ADocumentId)}}><a className="dropdown-item">Download</a></li>
                                                     </>
                                                    :""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "DRAFT"?
                                                    <li><a className="dropdown-item" href="#">AStart Campaign</a></li>:""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "CAMPAIGNSTARTED"?
                                                    <>
                                                    <li><a className="dropdown-item" href="#">Stop Campaign</a></li>
                                                    <li><a className="dropdown-item" href="#">Download Petition</a></li>
                                                    <li><a className="dropdown-item" href="#">Request Signature</a></li>
                                                    <li><a className="dropdown-item" href="#">Signatory Count</a></li>
                                                    </>
                                                     :""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "CAMPAIGNSTOPPED"?
                                                    <>
                                                    <li><a className="dropdown-item" href="#">Download Petition + 10 pages</a></li>
                                                    <li><a className="dropdown-item" href="#">Download</a></li>
                                                    </>
                                                    :""}
                                                    { item.IsOwner === 1 && item.StatusCode === "REJECTED" && item.Reason !== null?
                                                     <li style={{cursor:"pointer"}} onClick={()=>{ViewRejectedreason(item.Reason)}}><a className="dropdown-item">View Reason</a></li>:""
                                                    }
                                                </ul> */}
                                                 <ul className="pl-0 mb-0 dropdown-menu p-2"  aria-labelledby={`${"dropdownMenuButton1"+item.ADocumentId}`}>
                                                   {item.IsOwner === 1 && (item.StatusCode === "DRAFT")&& item.WorkflowCode ==='STANDARD' && item.OwnedBy === "You" && item.IsFromScratchDocument === 1 ?
                                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>{EditDocument(item.ADocumentId)}}>Edit</a></li>:""}  
                                                    {item.IsOwner === 1 && (item.StatusCode === "DRAFT" || item.StatusCode === "REJECTED")&& item.orkflowCode ==='STANDARD' && item.OwnedBy === "You" ?
                                                    <li><a className="dropdown-item" href="#">Design Mode</a></li>:""}
                                                    {item.IsSecured === 1 && item.StatusCode === "SENT" && item.TotalRecipients !== item.RecipientApprovedCount?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{Resendotp(item.ADocumentId)}}><a className="dropdown-item">Resend OTP</a></li>:""}
                                                    {(item.IsOwner === 1 && item.StatusCode === "APPROVED") || (item.StatusCode === "DRAFT" && item.TotalRecipients === 1) && item.orkflowCode ==='STANDARD' ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowsharedocument(true)}}><a className="dropdown-item">Share</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "SENT" &&item.OwnedBy === "You" ?
                                                    <><li style={{cursor:"pointer"}} onClick={()=>{Getrecipients(item.ADocumentId)}}><a className="dropdown-item">Resend</a></li>
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowmanagevaliditydocumentmodule(true)}}><a className="dropdown-item" >Manage Validity</a></li>
                                                    <li style={{cursor:"pointer"}} onClick={()=>{Revokethedocment(item.ADocumentId)}}><a className="dropdown-item">Revoke</a></li>
                                                    </>
                                                    :""}
                                                    {item.IsOwner === 1 && (item.StatusCode === "DRAFT" || item.StatusCode === "REJECTED") && item.TotalRecipients >=1 &&item.IsFromScratchDocument === 0 && item.OwnedBy === "You" ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{usehistory.push(`documentselect/${item.ADocumentId}`)}}><a className="dropdown-item"> Fill and Send</a></li>:""}
                                                    {(item.IsOwner === 1 && item.OwnedBy === "You" && item.StatusCode === "SENT" && item.ViewerStatusCode === "SENT")?
                                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>openApprovePage(item)}>{item.RecipientStatus}</a></li>:""}
                                                    {/* <li onClick={()=>openApprovePage(item)}><a className="dropdown-item" href="javascript:void(0);" >Approve</a></li> */}

                                                     {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsGoogleDriveEnabled === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttodrive(item.ADocumentId)}} ><a className="dropdown-item">Export to drive</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsOneDriveEnabled === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttoonedrivedrive(item.ADocumentId)}} ><a className="dropdown-item" >Export to one drive</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsDropBoxEnabled === 1 ?
                                                    <li  style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttodropboxdrive(item.ADocumentId)}} > <a className="dropdown-item">Export to drop box</a></li>:""}
                                                    {item.IsOwner === 1 && item.WorkflowCode === "STANDARD" && item.OwnedBy === "You" ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item), Createtemplate(item.ADocumentId)}} ><a className="dropdown-item">Create Template</a></li>:""}
                                                      {item.IsOwner === 1 && item.WorkflowCode === "STANDARD"?
                                                    <li style={{cursor:"pointer"}}  onClick={()=>{getaudittrailinfo(item.ADocumentId)}}><a className="dropdown-item">Audit Trail</a></li>:""}
                                                    {(item.IsOwner === 1 && item.StatusCode !== "SENT") ?item.StatusCode !== "CAMPAIGNSTARTED"?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowdeletedocumentmodule(true)}}><a className="dropdown-item">Delete</a></li>:"":""}
                                                     {item.WorkflowCode === "STANDARD"?
                                                     <>
                                                     <li style={{cursor:"pointer"}} ><a className="dropdown-item" onClick={()=>viewApprovePage(item)}>View</a></li>
                                                     <li style={{cursor:"pointer"}} onClick={()=>{DownloadDocument(item)}}><a className="dropdown-item">Download</a></li>
                                                     </>
                                                    :""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "DRAFT"?
                                                    <li><a className="dropdown-item" href="#">AStart Campaign</a></li>:""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "CAMPAIGNSTARTED"?
                                                    <>
                                                    <li><a className="dropdown-item" href="#">Stop Campaign</a></li>
                                                    <li><a className="dropdown-item" href="#">Download Petition</a></li>
                                                    <li><a className="dropdown-item" href="#">Request Signature</a></li>
                                                    <li><a className="dropdown-item" href="#">Signatory Count</a></li>
                                                    </>
                                                     :""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "CAMPAIGNSTOPPED"?
                                                    <>
                                                    <li><a className="dropdown-item" href="#">Download Petition + 10 pages</a></li>
                                                    <li><a className="dropdown-item" href="#">Download</a></li>
                                                    </>
                                                    :""}
                                                    { item.IsOwner === 1 && item.StatusCode === "REJECTED" && item.Reason !== null?
                                                     <li style={{cursor:"pointer"}} onClick={()=>{ViewRejectedreason(item.ADocumentId)}}><a className="dropdown-item">View Reason</a></li>:""
                                                    }
                                                </ul>
                                               {/* </div> */}
                                                </td>
                                                <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item.RecipientNames!== null&&item.RecipientNames.length>10?
                                             <Tooltip placement="top" title={item.RecipientNames} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        width: 80,
                                                        position:"relative",
                                                        textAlign:"start",
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.RecipientNames}
                                                    </div>
                                                </Tooltip>:item.RecipientNames !== null?item.RecipientNames:"-"}
                                                
                                             </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>{
                                                <><div>{moment.utc(item?.CreatedOn).local().format('MM/DD/YYYY')}</div>
                                                <div>{moment.utc(item?.CreatedOn).local().format('h:mm A')}</div></>
                                             }</td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>{
                                                <><div>{item.UpdatedOn !== null ?(moment.utc(item?.UpdatedOn).local().format('MM/DD/YYYY')):(moment.utc(item?.CreatedOn).local().format('MM/DD/YYYY'))}</div>
                                                <div>{item.UpdatedOn !== null ?(moment.utc(item?.UpdatedOn).local().format('h:mm A')):(moment.utc(item?.CreatedOn).local().format('h:mm A'))}</div></>
                                             }</td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item.OwnedBy!== null&&item.OwnedBy.length>15?
                                             <Tooltip placement="top" title={item.OwnedBy} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        width: 100,
                                                       position:"relative",
                                                       textAlign:"start",
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.OwnedBy}
                                                    </div>
                                                </Tooltip>:item.OwnedBy !== null?item.OwnedBy:"-"}
                                                </td>
                                                <td><div className='table_btn'  style={{width:"max-content"}}>
                                               {/* <button   style={{cursor:"context-menu",margin:0, border:"0.5px solid black",background:"none"}}
                                                // className={`managestatusbutton btn ${item.StatusCode ==="DRAFT"?"draft":
                                                //item.StatusCode ==="WAITFOROTHERS"?"pending":
                                               // item.StatusCode ==="ACTIONREQUIRED"?"sign":
                                              //  item.StatusCode ==="APPROVED" && item.OwnedBy==="You"?"invitation":
                                               // item.StatusCode ==="APPROVED" ?"completed":
                                              //  item.StatusCode ==="SENT"?"pending":
                                              //  item.StatusCode ==="REJECTED"?"rejected":
                                               //</div> item.StatusCode ==="CANCELLED"?"cancelled"
                                               // :""          
                                               // }-btn`} 
                                               >{item.StatusCode ==="DRAFT"?<img src= 'src/images/draft.svg' title="Draft" style={{width:30,height:30,padding:3}}/>:item.StatusCode ==="SENT"?<img src= 'src/images/send.svg' title="Sent" style={{width:30,height:25,padding:4}}/>:item.StatusCode ==="REJECTED"?<img src= 'src/images/Rejected.svg' title="Rejected" style={{width:30,height:30,padding:3}}/>:item.StatusCode ==="CANCELLED"?<img src= 'src/images/Cancelled.svg' title="Cancelled" style={{width:30,height:30,padding:4}}/>:item.StatusCode ==="APPROVED"?<img src= 'src/images/Approved.svg' title="Approved" style={{width:30,height:30,padding:4}}/>:""}</button> */}
                                               {Statuswidth(item.StatusCode,item.TotalRecipients,item.RecipientApprovedCount)}
                                                </div>
                                                {item.StatusCode ==="DRAFT"?<span style={{color:'blue' , fontSize:'14px'}}>Document is in Draft</span>:item.StatusCode ==="SENT"?<span style={{color:'blue' , fontSize:'14px'}}>Waiting for others</span>:item.StatusCode ==="REJECTED"?<span style={{color:'red' , fontSize:'14px'}}>Document is Rejected</span>:item.StatusCode ==="CANCELLED"?<span style={{color:'red' , fontSize:'14px'}}>Document is Cancelled</span>:""}
                                                </td>
                                </tr>
                                   )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <Modal show={showcreatetemplatemodule} 
                    //size="sm"
                    width="800px"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#949494f0"}} > Create Template</p>
                        </Modal.Title>
                        <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setshowcreatetemplatemodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowcreatetemplatemodule(false)}}/> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                         <div>
                            <label >Title<span style={{color:"red"}}>*</span></label>
                            <input id="title" onChange={(event) => {setTitle(event.target.value)}} className="form-control mt-1" type="text"  aria-label="default input example"/>
                            </div>
                            {userdata?.IsOwner===1&&<div className="form-check form-check-inline mt-4 ml-2">
                            <input onChange={(event) => {setIspublic(event.target.value)}}   className="form-check-input" type="checkbox" id="Ispublic" value="option1"/>
                            <label className="form-check-label " for="inlineCheckbox1">Is Public</label>
                            </div>}
                            <div className="form-check form-check-inline mt-4 ml-2">
                            <input onChange={(event) => {setprefill(event.target.value)}} className="form-check-input" type="checkbox" id="prefill" value="option2"/>
                            <label className="form-check-label " for="inlineCheckbox2">Pre-fill</label>
                             </div>
                        {/* <div className='mt-3 mb-3'>
                        <label className='mb-1'>Category<span style={{color:"red"}}>*</span></label>
                        <select style={{fontSize:16, fontWeight:300}}  onChange={(event) => {setSelectedtemplatecategory(event.target.value)}} className="form-select form-select-sm form-select-border"  id='select-box' searchable>
                            <option style={{fontSize:16, fontWeight:300}} selected>Select </option>
                            {templatecategoryslist.at.length>0&&templatecategoryslist.map(item=>{
                               return(
                               <option style={{fontSize:16, fontWeight:300}} value={item.TemplateCategoryId}>
                               {item.TemplateCategoryName}
                             </option>)
                            })}
                        </select>
                         </div>
                            <div>
                            <label >Tags<span style={{color:"red"}}>*</span></label>
                            <input className="form-control mt-1 mb-3" type="text" 
                             placeholder="Add a tag and click on enter..."
                             aria-label="default input example"
                             onKeyPress={event => {
                                if (event.key === "Enter") {
                                  setTags([...tags, event.target.value]);
                                  event.target.value = "";
                                }
                              }}
                              autofocus
                             />
                            </div> */}
                            {/* <ul className="TagListelement">
                                {tags.map(tag => (
                                    <li className="Tagelement">
                                    {tag}
                                    <XCircle
                                        className="TagIconelement"
                                        size="16"
                                        onClick={() => {
                                        setTags([...tags.filter(word => word !== tag)]);
                                        }}
                                    />
                                    </li>
                                ))}
                            </ul> */}
                        
                    </Modal.Body>
                    <Modal.Footer>
                        
                        <Button variant="primary"  onClick={() => {
                           Savetemplate()
                        }}>CREATE</Button> 
                         <Button variant="secondary" onClick={() => {
                            setshowcreatetemplatemodule(false)
                            setSelectedtemplatecategory("")
                            setIspublic("")
                            setTitle("")
                            setprefill("")
                            setTags([])
                           
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={showsharedocument} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p> Share Document</p>
                            
                        </Modal.Title>
                        <button type="button" className="close" style={{backgroundColor:"#ffff"}} onClick={()=>{setshowsharedocument(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <div className='flex'>
                        <p className='-mt-1 mb-4' style={{fontSize:13,color:"#696969"}}>ADD RECIPIENT ( Selected recipients will receive a copy of the final approved document.)</p>
                        <div type="button" onClick={addrecipients}><img className='-mt-2 mb-4'  src="src/images/plus_icon.svg"  /></div>
                        </div>
                        
                       
                        {Inputfields.map((inputfield,index) =>{
                             return(
                            <div className='flex justify-content-center'>
                            <div>
                            <input id="id" type="text" SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="EmailAddress" value={inputfield.EmailAddress} onChange={e=>handilechangeinput(index,e)} className='docinput' placeholder=' Email*'  />
                            </div>
                            <div>
                                <input id="id" type="text" SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="FirstName"  value={inputfield.FirstName} onChange={e=>handilechangeinput(index,e)}  className='docinput' placeholder=' First Name*'  />
                            </div>
                            <div>
                            <input id="id" type="text"  SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="LastName" value={inputfield.LastName} onChange={e=>handilechangeinput(index,e)}  className='docinput' placeholder=' Last Name*'  />
                            </div>
                            <div  type="button" onClick={()=>{removeRecipients(index)}}>
                            <img className='ml-2 mt-1'
								src='src/images/cancel_close_cross_delete_remove_icon.svg'
								width={"25px"} alt=''/>
                                </div> 
                             </div>)})}
                        
                        
                        
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => {
                             let allinputs = true
                             Inputfields.map(item=>{
                                 if(item.EmailAddress==="" ||item.FirstName==="" ||item.LastName===""){
                                     allinputs=false;
                                    // Swal.fire("Please Fill All Details then only need to Share Document ","","info")
                                     Swal.fire({
                                        icon: 'info',
                                        title: 'lease Fill All Details then only need to Share Document',
                                        showConfirmButton: true,
                                        confirmButtonText:"OKAY",
                                        //timer: 2000
                                      })
                                  }
                             })
                             if(allinputs === true){
                                 Sharedocument();
                              }
                        }}>SHARE</Button> 
                         <Button variant="secondary" onClick={() => {
                           setshowsharedocument(false)
                           setinputfield([])
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={showdeletedocumentmodule} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}}> Delete document</p>
                            
                        </Modal.Title>
                        <button type="button" class="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        {/* <div className='flex '>
                        <div >
                        <textarea  type="text"  className='docinput' placeholder=' Reason*'   />
                        </div> 
                        </div> */}
                        <div class="form-group">
                        <label for="comment">Reason*:</label>
                        <textarea  type="text" class="form-control" rows="5" id="comment"  value={deletedocreason} onChange={(e)=>docreasondelete(e)}/>
                        </div>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" disabled={deletedocreason === null?true:false} onClick={() => {
                           deletedocument()
                           setshowdeletedocumentmodule(false)
                           setdeletedocreason(null)
                        }}>DELETE</Button> 
                         <Button variant="secondary" onClick={() => {
                            setshowdeletedocumentmodule(false)
                            setSelecteddoc(null)
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={showmanagevaliditydocumentmodule} 
                    size=""
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}}> Manage document validity</p>
                            
                        </Modal.Title>
                        <button type="button" className="close" style={{backgroundColor:"#ffff"}} onClick={()=>{setshowmanagevaliditydocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        {Selecteddoc!== null &&Selecteddoc.ExpirationDate === null?<><div style={{color:"#736a6a"}}>
                            The validity date for this transaction has not been set by the sender.
                            </div><div style={{color:"#736a6a"}}>
                            Please contact the sender of the document.</div></>:
                        <div className='flex '>
                        <div >
                        <div>Current Validity Date</div>
                        <input type="text" style={{height:"50px",fontSize:"15px",color:"grey"}} className='docinput' value={Selecteddoc!== null &&Selecteddoc.ExpirationDate !== null?moment(Selecteddoc.ExpirationDate).format('MM/DD/yyyy'):""} />
                        </div> 
                        <div >
                        <div>New Validity Date</div>
                        <div className='App'>
                        {/* <DatePicker 
                    selected={updatedManagevalidityDate=== ""?null:updatedManagevalidityDate} 
                    onChange={(date) => setupdatedManagevalidityDate(date)}
                    minDate={moment().toDate()}
                    placeholderText={'  Please select a date'}
                    dateFormat="dd-MM-yyyy"
                    
                    /> */}
                    <Calendar 
                       placeholder='Select'
                       minDate={new Date()} 
                       readOnlyInput 
                       value={updatedManagevalidityDate}
                       onChange={(e) => setupdatedManagevalidityDate(e.value)} dateFormat="mm/dd/yy" showIcon />
                        </div>
                        </div> 
                        </div>}
                    </Modal.Body>
                    <Modal.Footer>
                        {Selecteddoc!== null &&Selecteddoc.ExpirationDate !== null&& <Button disabled ={updatedManagevalidityDate !== null? false:true} variant="primary" onClick={() => {
                           updatemanagevalidity()
                           setshowmanagevaliditydocumentmodule(false)
                           setupdatedManagevalidityDate(null)
                        }}>UPDATE</Button> }

                    </Modal.Footer>
                </Modal>
                <Modal show={showAuditmodule} 
                    size="xl"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        {/* <Modal.Title id="example-modal-sizes-title-lg">
                            <p> Audit Trail</p>
                            
                        </Modal.Title> */}
                        {/* <button type="button" className="close" style={{backgroundColor:"#ffff"}} onClick={()=>{setshowAuditmodule(false)}} 
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                    <div style={{maxHeight:300,overflowY:"auto",color:"#949494f0"}}>
                    <div ref={printRef}>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}}> Audit Trail</p>
                        </Modal.Title>
                    </Modal.Header>
                    <div className="table__row__conts table-responsive">
                    <table className="table">
                    <thead style={{visibility:"collapse"}} className='first_head'>
                                        <tr>
                                            <th></th>
                                            <th style={{width:150}}></th>
                                            <th></th>
                                            <th style={{display:"none"}}>
                                                {/* 1-8 on <span>8</span> */}
                                                </th>
                                                
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"#515762"}}>Date</th>
                                            <th className='px-5' style={{position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"#515762"}}>Details</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"#515762"}}>Change </th> 
                                           <th style={{display:"none"}}></th> {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead>
                        <tbody>
                            {Audittraildata.length>0?Audittraildata.map(item=>{
                                return(
                            <tr>
                            <td  style={{fontSize:14,fontWeight:300,color:"#404A5A",width:295,wordWrap:"break-word"}}>{item.Date}</td>
                            <td className='px-5' style={{fontSize:14,fontWeight:300,color:"#404A5A",maxWidth:200}}>{item.IPAddress}</td>
                            <td className='px-5' style={{fontSize:14,fontWeight:300,color:"#404A5A",width:310,wordWrap:"break-word"}}>{item.Description}
                            </td>
                            </tr>
                                )
                            }):""}
                        </tbody>
                        </table>
                        </div>
                        </div>
                        </div>
                       
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <ReactToPrint trigger={()=>{
                             return <Button variant="primary">PRINT</Button>
                         }} 
                         content={reactToPrintContent}
                         documentTitle="AuditTrail Document"
                         pageStyle="print"
                         removeAfterPrint
                         />
                        <Button variant="secondary" onClick={()=>{setshowAuditmodule(false)}}>CLOSE</Button>
                         
                    </Modal.Footer>
                </Modal>
                <Modal show={showdrivemodel} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    scrollable={true}
                    >
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p> {Exporttitle}</p>
                            
                        </Modal.Title>
                        <button type="button" className="close" style={{backgroundColor:"#ffff"}} onClick={()=>{setshowdrivemodel(false)}} 
                         >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                        <Modal.Body className='error-modal-content'>
                         <div>
                         <ul className="list-group">
                            {Goodrivedata.length>0?Goodrivedata.map(item=>{
                                 return(
                                    item.IsFolder === true?
                                        <li className="list-group-item" style={{backgroundColor:"#e2dbdb",cursor:"pointer"}} onClick={()=>Exporttitle==="Google Drive"?getchilddata(item):Exporttitle==="Drop Box"?getdropboxchilddata(item):Exporttitle==="One Drive"?getonedrivechilddata(item): ""}>
                                          <>
                                          <i className="fa fa-folder mr-1" style={{color:"#2F7FED"}}></i>
                                           <span>{item.roleName}</span>
                                           {item.children.length>0&&<ul className="list-group" style={{display:"block"}}>
                                              { item.children.map(item1=>{
                                                return(
                                                    <li className="list-group-item" style={{cursor:"default"}} >{item1.roleName}</li>
                                                )
                                              })}
                                              </ul>}
                                           </> </li>
                                           :
                                         <li className="list-group-item" onClick={()=>Exporttitle==="Google Drive"?getchilddata(item):Exporttitle==="Drop Box"?getdropboxchilddata(item):Exporttitle==="One Drive"?getonedrivechilddata(item):""}><span>{item.roleName}</span></li>
                                 )
                            })
                        
                        :"Drive is Empty"}
                        
                        </ul>
                        </div>
                        </Modal.Body>
                    
                    
                    <Modal.Footer>
                    <Button variant="primary" onClick={()=>{ExporttoCloud()}}>EXPORT</Button>
                        
                        <Button variant="secondary" onClick={()=>{setshowdrivemodel(false)}}>CANCEL</Button>
                         
                    </Modal.Footer>
                </Modal>
            </div>

            {/* <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <button type="button" className="explore_btn" onClick={explore_all_data}>{t('EXPLORE_ALL')}</button>
                </div>
            </div> */}



        </div>
    )
}

export { ContentTHree }; 