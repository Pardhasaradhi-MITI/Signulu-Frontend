import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { manageservice } from '../../_services/manageservice';
import {CountCard } from '../Content/Card/CountCard';
import { useTranslation } from 'react-i18next';
import {usersService} from '../../_services/users.service'

function ContentTWO({ history, location }) {   
    const { t, i18n } = useTranslation();
    const [Cardsinfo,setcardsinfo]=useState([])
    const [Draft,setDraft]=useState(0);
    const [Cancelled,setCancelled]=useState(0);
    const [Rejected,setRejected]=useState(0);
    const [MyDocuments,setMyDocuments]=useState(0);
    const [Sent,setSent]=useState(0);
    const [Pending,setPending]=useState(0);
    const [loginuserdata,setloginuserdata]=useState({})
    const [Completed,setCompleted]=useState(0)
    const [totalsigned,settotalsigned]=useState(0)
    

    useEffect(()=>{
        getcardsinfo()
        getloginuserdata()
    },[]);
    const getloginuserdata=()=>{
        usersService.getCurrentUserData()
        .then((result) => {
            if(result){
                setloginuserdata(result.Data)
            }
        })
    }
  const getcardsinfo=()=>{
    manageservice.getdoctotalcount().then((result)=>{
                 if(result){
                    setcardsinfo(result.Entity);
                    result?.Entity?.map(item=>{
                        item.Name === "Cancelled"?setCancelled(item.RecordCount):item.Name === "Draft"
                        ?setDraft(item.RecordCount):item.Name==='Rejected'?
                        setRejected(item.RecordCount):item.Name==="My Documents"?   
                        setMyDocuments(item.RecordCount):item.Name==="To Sign"?
                        setSent(item.RecordCount):item.Name === "Pending"?
                        setPending(item.RecordCount):item.Name === "Completed"?setCompleted(item.RecordCount):""
                    })
                 }
    }).catch({

    })
  }


    return (
        <div>
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <h3 style={{fontSize:35,fontWeight:300}}>{t('DASHBOARD')}</h3>
                    </div>
                </div>
                <div className="dashboard_item">
                <CountCard title="Drafts" useremail={loginuserdata.EmailAddress} image="src/images/document-7_icon.svg" counter={Draft}/> 
                <CountCard title="To Sign" useremail={loginuserdata.EmailAddress} image="src/images/document-4_icon.svg" counter={Sent}/>
                <CountCard title="Waiting for Others" useremail={loginuserdata.EmailAddress} image="src/images/document-5_icon.svg" counter={Pending}/> 
                <CountCard title="My Documents" useremail={loginuserdata.EmailAddress} image="src/images/document-8_icon.svg" counter={MyDocuments}/> 
                {/* <CountCard title="Total Signed Documents" useremail={loginuserdata.EmailAddress} image="src/images/document-8_icon.svg" counter={0}/>  */}
                <CountCard title="Total Signed Documents" useremail={loginuserdata.EmailAddress} image="src/images/document-8_icon.svg" counter={(Completed+MyDocuments)}/> 
                <CountCard title="Rejected/Cancelled" useremail={loginuserdata.EmailAddress} image="src/images/document-6_icon.svg" counter={Rejected} counter1={Cancelled}/> 
                
               
                </div>
            </div>     

    )
}

export { ContentTWO }; 