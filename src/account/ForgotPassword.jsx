import React,{useState} from 'react';
import { useHistory} from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Reaptcha from 'reaptcha';
import { Button, Modal, ModalFooter } from 'react-bootstrap';
import Swal from 'sweetalert2';

import { accountService, alertService } from '@/_services';

function ForgotPassword() {
    const [errormessage,setErrormessage]=useState("")
    const usehistory = useHistory();
    const [validCaptcha,setvalidCaptcha]=useState()
   
    function getForm() {
    const initialValues = {
        email: '',
        recaptcha:''
    };

    const validationSchema = Yup.object().shape({
        email:Yup.string()
        .required('Email is required'),
       
     });
   
    function onSubmit({ email }, { setSubmitting }) {
        setErrormessage("")
        alertService.clear();
        validCaptcha=== true &&  accountService.forgotpaswordget(email)
            .then((result) => Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Please check your email for password reset instructions',
                showConfirmButton: true,
                confirmButtonText:"OKAY",
              })).then((result) => {
                if (result.isConfirmed) {
                    usehistory.push('/account/login');
                }
            })
            .catch(error => alertService.error(error))
            .finally(() => setSubmitting(false));
    }
    function onCaptchaVerify(recaptchaResponse) {
        //validCaptcha = true
        setvalidCaptcha(true)
       
    };
    function onCaptchaExpire(recaptchaResponse) {
        //validCaptcha = false
        setvalidCaptcha(false)
    } 

    const handleCancel = () => {
        usehistory.push(`/account/login`)
    };
    const loginPage = ()=>{
        usehistory.push(`/account/login`)
    }

    return (
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit} >
            {({ errors, touched, isSubmitting }) => (
                <Form>
                    {/* <h3 className="card-header">Forgot Password</h3> */}
                    <p style={{ color: "red", textAlign: "left" }}>{errormessage}</p>
                    <div className="card-body">
                        <div className="form-group">
                            <label>Email</label>
                            <Field name="email" type="email" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                            <ErrorMessage name="email" component="div" className="error-message" style={{color:"red"}} />
                        </div>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group mt-4">
                                    <div className="captcha">
                                            <Reaptcha id="recaptcha" name="recaptcha"
                                                    sitekey="6LcljLYUAAAAANiWkP0pM3hMy-wakNZ0T0pYakKO"
                                                    onVerify={onCaptchaVerify}
                                                    onExpire={onCaptchaExpire} />
                                         {validCaptcha ===false  && <span className='error-message'>Captcha is required</span>}
                                                {/* {state && state.errors && state.errors.recaptcha ?
                                             <span className='error-message'>{state.errors.recaptcha}</span>
                                              : null} */}
                                        </div>
                                    </div>
                                </div>
                        <div className="form-row">
                            <div className="form-group col">
                            <button type="submit" className="btn btn-secondary " style={{margin:7,marginLeft:-10}} onClick={handleCancel}>CANCEL</button> 
                                <button type="submit"  style={{margin:7}}  className="btn btn-primary" onClick={()=>{validCaptcha !== true?setvalidCaptcha(false):""}}>
                                    {/* {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>} */}
                                    SUBMIT
                                </button>
                            </div>
                        </div>
                    </div>
               </Form>
            )}
        </Formik>   
    )
    }
    function getBody() {
        return getForm();
        // switch (tokenStatus) {
        //     case TokenStatus.Valid:
        //         return getForm();
        //     case TokenStatus.Invalid:
        //         return <div>Token validation failed, if the token has expired you can get a new one at the <Link to="forgot-password">forgot password</Link> page.</div>;
        //     case TokenStatus.Validating:
        //         return <div>Validating token...</div>;
        // }
    }

    return (
        <Modal show={true}>
        <div>
            <h3 className="card-header">Forgot Password</h3>
            <div className="card-body">{getBody()}</div>
        </div>
        </Modal>
     )
}

export { ForgotPassword }; 