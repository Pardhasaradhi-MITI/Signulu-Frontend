import React, { useState,useRef } from 'react';
import { Link, useHistory,useParams,useLocation} from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { commonService,user } from './../_services/common.service';
import { useTranslation } from 'react-i18next';
import { manageservice } from '../_services/manageservice';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { useEffect } from 'react';
//import Pagination from "react-responsive-pagination";  
import Pagination, { bootstrap5PaginationPreset } from 'react-responsive-pagination';
import 'bootstrap/dist/css/bootstrap.css';
import moment from 'moment';
import Tooltip from "@material-ui/core/Tooltip";
import {usersService} from "../_services/users.service"
import { Button, ButtonToolbar, Modal,ListGroup,ListGroupItem } from 'react-bootstrap';
 import Swal from "sweetalert2"
 import config from 'config';
 import DatePicker from "react-datepicker"
 import { Calendar } from 'primereact/calendar';
 import "react-datepicker/dist/react-datepicker.css";
import { result } from 'lodash';
import ReactTooltip from 'react-tooltip';
import ReactToPrint from 'react-to-print';
import CloseIcon from '@mui/icons-material/Close';
import {Tag,XCircle} from "react-feather"
import { reloadResources } from 'i18next';
import DoneIcon from '@mui/icons-material/Done';
import { addDocumentService } from '../_services/adddocument.service';
import { CSVLink } from 'react-csv';


import { read, utils, writeFile } from 'xlsx';
function Manage({ history, location }) {
    const wrapperRef1 = useRef(null);
    const [checked, setChecked] =useState(true);
    const usehistory = useHistory();
    const wrapperRef2 = useRef(null);
    const { t, i18n } = useTranslation();
    const [doclist,setdoclist]=useState([]);
    const [StatusCode,setStatusCode]=useState(location.state !== undefined?location.state.state:null);
    const [pageNumber,setPageNumber]=useState(null)
    const [perpage,setperpage] =useState([]);
    const [PageCount,setPageCount]=useState(1)
    const [email,setemail]=useState(false)
    const [userdata,setuserdata]=useState({})
    const [currentdocid,setcurrentdocid] = useState(null)
    const [Recipientcount,setRecipientcount]=useState(1)
    const [deletedocreason,setdeletedocreason]=useState("");
    const [showdeletedocumentmodule,setshowdeletedocumentmodule] = useState(false)
    const [showrevokedocumentpopup,setshowrevokedocumentpopup] = useState(false)
    const [Revokethedocmentid,setRevokethedocmentid] = useState(false)
    
    const [revokedocreasion,setrevokedocreasion] = useState("")
    const [Selecteddoc,setSelecteddoc] = useState(null)
    const [ShowDelertalert, setShowDelertalert] = useState(false);
    const [updatedManagevalidityDate,setupdatedManagevalidityDate]=useState(null);
    const [showmanagevaliditydocumentmodule,setshowmanagevaliditydocumentmodule]=useState("");
    const [RecipientIds,setRecipientIds]=useState();
    const [showAuditmodule,setshowAuditmodule] = useState(false)
    const [Audittraildata,setAudittraildata]=useState([]);
    const [showsharedocument,setshowsharedocument]=useState(false);
    const [Goodrivedata,setGoodrivedata]=useState([])
    const [showdrivemodel,setshowdrivemodel]=useState(false);
    const [showdrivemodel1,setshowdrivemodel1]=useState(false);
    const [templatecategoryslist,settemplatecategoryslist]=useState([]);
    const [Selectedtemplatecategory,setSelectedtemplatecategory]=useState("");
    const [showcreatetemplatemodule,setshowcreatetemplatemodule]=useState(false);
    const [Exporttitle,setExporttitle]=useState(null)
    const[selectedfolder,setselectedfolder]=useState({});
    const [isfolderopen,setisfolderopen]=useState(false);
    const printRef = useRef(null);
    const [childitem,setchilditem]=useState({})
    const [tags, setTags] = useState([]);
    const [prefill,setprefill]=useState(0);
    const [Ispublic,setIspublic]=useState(false);
    const [Title,setTitle]=useState("");
    const [userInfo,setUserInfo]=useState({});
    const [childArr,setChildArr]=useState([]);
    const [showImportDrivemodel,setShowImportDrivemodel]=useState(false);
    const [print,setprint]=useState(false)
    const [shareerror, setshareError] = useState({
		FirstName: '',
		LastName: '',
		EmailAddress: '',
	});
   const [exceldata,setexceldata]=useState([])
   const API_END_POINT = config.serverUrl;
   
    const data2 ={
        "OwnedBy": null,
        "PageNumber": 1,
        "PageSize": 100000,
        "SearchByText": "",
        "SharedUser": null,
        "StatusCode": StatusCode,
        "Title": null,
        "IsReact":1,
    }
    const data1 ={
        "OwnedBy": null,
        "PageNumber": 1,
        "PageSize": 100000,
        "SearchByText": "",
        "SharedUser": null,
        "StatusCode": StatusCode,
        "Title": null,
        "EmailAddress":userdata.EmailAddress,
        "IsReact":1,
    }
    const data3= {"OwnedBy": null,
    "PageNumber": 1,
    "PageSize": 100000,
    "SearchByText": "",
    "SharedUser": null,
    "StatusCode": StatusCode,
    "Title": null,
    "CurrentUserEmail":location?.state?.loginuseremail=== undefined?userdata.EmailAddress:location?.state?.loginuseremail,
     "IsReact":1,
    }
 
    const [Inputfields,setinputfield]=useState([
    {EmailAddress:"",
    FirstName: "",
    IsDelete: 0,
    LastName: "",
    RecipientId:null ,
    SendToUser: true,
    id: null  } ,
 ])
    const data = StatusCode ==="TOSIGN"? data3 :StatusCode !=="TOSIGN"&&email === true?data1:data2
    useEffect(()=>{
        getCurrentUserData()  
        getTags()   
    },[])
    useEffect(()=>{
      getdocdata(data)
      setPageNumber(null)
    },[StatusCode,email])
    useEffect(()=>{

    },[isfolderopen])
    const getCurrentUserData = ()=>{
        usersService.getCurrentUserData()
        .then((result) => {
            if(result){
                setUserInfo(result.Data)
                 setuserdata(result.Data)
            }
        })
    }
    const getdocdata=()=>{
        if(data.StatusCode == null){
            data.PageSize =10;
            manageservice.getdoclist(data).then((result)=>{
                const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => {
                    return moment.utc(obj.UpdatedOn===null?obj.CreatedOn:obj.UpdatedOn).local().format('YYYY-MM-DD h:mm a')
                }],['desc'])
                   // new Date(obj.UpdatedOn===null?obj.CreatedOn:obj.UpdatedOn)], ['desc'])
                //setdoclist(sortedArray);
                setperpage(sortedArray)
            })
            data.PageSize = 1000000;
            manageservice.getdoclist(data).then((result)=>{
                const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => {
                    return moment.utc(obj.UpdatedOn===null?obj.CreatedOn:obj.UpdatedOn).local().format('YYYY-MM-DD h:mm a')
                }],['desc'])
                //const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => new Date(obj.UpdatedOn===null?obj.CreatedOn:obj.UpdatedOn)], ['desc'])
                setdoclist(sortedArray);
                setperpage([])
                if(pageNumber === null){
                    setperpage(sortedArray.slice(0,10));
                    setPageNumber(1)
                }
                else{
                    setperpage(sortedArray.slice((pageNumber*10)-10,pageNumber*10));
                }
                let pages = 0;
                for(let i=1;i<Math.ceil(sortedArray.length/10)+1;i++){
                    pages = i
                }
                setPageCount(pages)
               //setdoclist(result?.Entity?.data)  
            }).catch({
    
            })
        }
        else{
        manageservice.getdoclist(data).then((result)=>{
            const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => {
                return moment(obj.UpdatedOn===null?obj.CreatedOn:obj.UpdatedOn).format('YYYY-MM-DD h:mm A')
            }],['desc'])
           // const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => new Date(obj.UpdatedOn===null?obj.CreatedOn:obj.UpdatedOn)], ['desc'])
            setdoclist(sortedArray);
           
            let pages = 0;
            for(let i=1;i<Math.ceil(sortedArray.length/10)+1;i++){
                pages = i
            }
            setPageCount(pages)
            if(pageNumber === null){
                setperpage(sortedArray.slice(0,10));
                setPageNumber(1)
            }
            else{
                setperpage([])
                setperpage(sortedArray.slice((pageNumber*10)-10,pageNumber*10));
            }
           //setdoclist(result?.Entity?.data)  
        }).catch({

        })
       }
    }
    //Filter Dropdown
    function dropdown2() {
        var srcElement = document.getElementById('dropdown');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    function dropdownclose() {
        var srcElement = document.getElementById('dropdown');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } 
            return false;
        }
    }
    //add document dropdown
    function dropdown3() {
        var srcElement = document.getElementById('dropdown1');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    // table more dropdown
    function dropdown4(id){
        if(currentdocid === null){
            setcurrentdocid(id)
        }
        else if(id !== currentdocid){
            let item1=currentdocid.toString();
            var srcElement1 = document.getElementById(item1)
            if (srcElement1 != null) {
                    srcElement1.style.display = "none";
                }
            setcurrentdocid(id)
        }
        let item=id.toString();
        var srcElement = document.getElementById(item)
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
//Upload
function uploadDiv() {
    
    var srcElement = document.getElementById('upload');
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = "none";
        } else {
            srcElement.style.display = "block";
        }
        return false;
    }
}
function revokedocument(payload){
    manageservice.Revokedocument(payload).then((result)=>{
        if(result.HttpStatusCode === 200){
           getdocdata();
          // Swal.fire('Document Cancelled!', '', 'success') 
           Swal.fire({
               icon: 'success',
               title: 'Document Cancelled!',
               showConfirmButton: true,
               confirmButtonText:"OKAY",
             //  timer: 2000,
               allowOutsideClick:true
             })              }
        else{
         //  Swal.fire('Document Not Revoked', '', 'info')
           Swal.fire({
               icon: 'info',
               title: 'Document Not Revoked',
               showConfirmButton: true,
               confirmButtonText:"OKAY",
             //  timer: 2000,
               allowOutsideClick:true
             })  
        }
      }).catch({

      })
}

function  getTags() {
    document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
        const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
     
        dropZoneElement.addEventListener("click", (e) => {
          inputElement.click();
        });
      
        inputElement.addEventListener("change", (e) => {
          if (inputElement.files.length) {
            updateThumbnail(dropZoneElement, inputElement.files);
          }
        });
      
        dropZoneElement.addEventListener("dragover", (e) => {
          e.preventDefault();
          dropZoneElement.classList.add("drop-zone--over");
        });
      
        ["dragleave", "dragend"].forEach((type) => {
          dropZoneElement.addEventListener(type, (e) => {
            dropZoneElement.classList.remove("drop-zone--over");
          });
        });
      
        dropZoneElement.addEventListener("drop", (e) => {
          e.preventDefault();
      
          if (e.dataTransfer.files.length) {
            inputElement.files = e.dataTransfer.files;
            updateThumbnail(dropZoneElement, e.dataTransfer.files);
          }
      
          dropZoneElement.classList.remove("drop-zone--over");
        });
      });
    
    }
      /**
       * Updates the thumbnail on a drop zone element.
       *
       * @param {HTMLElement} dropZoneElement
       * @param {File} file
       */
      function updateThumbnail(dropZoneElement, file) {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
      
        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
          dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }
      
        // First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
          thumbnailElement = document.createElement("div");
          thumbnailElement.classList.add("drop-zone__thumb");
          dropZoneElement.appendChild(thumbnailElement);
        }
      
        // thumbnailElement.dataset.label = file.name;
      
        // Show thumbnail for image files
        // if (file.type.startsWith("image/")) {
        //   const reader = new FileReader();
      
        //   reader.readAsDataURL(file);
        //   reader.onload = () => {
        //     thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
        //   };
        // } else {
        //   thumbnailElement.style.backgroundImage = null;
        // }
        commonService.setDocument(file)
        usehistory.push('/account/adddocumentsstep')
    }
    // Progress Bar
    const Statuswidth  = (status , total , approvedCount) => {
        let count = total
        let Number = approvedCount
        let val = (Math.round((Number/(count)*100)))
        // let val = status ==="DRAFT"? 10:status ==="SENT"?vale :status ==="REJECTED"?0: status ==="CANCELLED"?0 :status ==="APPROVED"?100:0
             
        return (
            <>{status !== "APPROVED" ? <><div className="progress" style={{ width: "100px", height: '0.5rem' }}>
                <div className="progress-bar bg-success" role="progressbar" style={{ width: val + "%" }} aria-valuenow={val} aria-valuemin="0" aria-valuemax="100"></div>
            </div><span style={{ fontSize: '12px', marginTop: '-5px',marginLeft:'3px' }}>{Number}/{count} done</span></>:<p>Completed</p>}
            </>
        )
    }

  const handlePagination = (newPage) => {
    setPageNumber(newPage);
        setperpage([])
        setperpage(doclist.slice((newPage*10)-10,newPage*10));
   }
  

 const Changecount =()=>{
    setRecipientcount(Recipientcount+1)
 }
 const docreasondelete =(e)=>{
    setdeletedocreason(e.target.value)
 }
 const deletedocument=()=>{
     let data={
        "Ids": [Selecteddoc.ADocumentId],
        "NotifyRecipients": false,
        "Reason": deletedocreason,
     }

     manageservice.deletedocument(data).then((result)=>{
           if(result.HttpStatusCode === 200){
            handlePagination(pageNumber)
            getdocdata();
            Swal.fire({
                icon: 'success',
                title: 'Document has been Deleted',
                showConfirmButton: true,
                confirmButtonText:"OKAY",
              //  timer: 2000,
                allowOutsideClick:true
              }) 
          
           }
     }).catch({

     })
 }
 const updatemanagevalidity=()=>{
    let date = moment(updatedManagevalidityDate).add(1,'d')
    let data={"ADocumentId":Selecteddoc.ADocumentId,"ExpirationDate":date}
   manageservice.Updatemanagevalidity(data,Selecteddoc.ADocumentId).then((result)=>{
    if(result.HttpStatusCode === 200){
        getdocdata();
        Swal.fire({
            icon: 'success',
            title: 'Document Validity Date Updated',
            showConfirmButton: true,
            confirmButtonText:"OKAY",
            allowOutsideClick:true
            
          }) 
    }
   }).catch({

   })
}
const Getrecipients=(id)=>{
    let ReIDs=[];
    manageservice.getRecipientsbasedondoc(id).then((result)=>{
       if(result?.Entity){
         
         result?.Entity.map(item=>{
            ReIDs.push(item.RecipientId)
         })
        //  setRecipientIds(ReIDs)
       }
    })
    Swal.fire({  
        text: 'An email with a link will be sent to all recipients.',  
        showConfirmButton:true ,
        showCancelButton: true,  
        confirmButtonText: `SEND`,
        cancelButtonText:"CANCEL", 
        showConfirmButton:true 
      }).then(function(isConfirm) {  
          if (isConfirm.isConfirmed === true) { 
            let d = new Date();
            let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
            let timeSplit = time.split(' ');
            let timeZone = timeSplit[1];
            //const datea=(moment(Date()).format('YYYY-MM-DD h:mm:ss')).toString();
            const datea=moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            const timezonea = timeZone;
            let datap={"ADocumentId":id,"DateTime":datea, "TimeZone":timezonea,"RecipientIds":ReIDs,"IsReact":1}
            manageservice.Senddocumenttorecipients(datap).then((result)=>{
               if(result.HttpStatusCode=== 200){
                //setRecipientIds(null) 
               // Swal.fire('Sent', '', 'success') 
                Swal.fire({
                    icon: 'success',
                    title: 'Sent',
                    showConfirmButton: true,
                    confirmButtonText:"OKAY",
                  //  timer: 2000,
                    allowOutsideClick:true
                  }) 
               }
            }) 
          } else { 
            setRecipientIds(null)   
            //  Swal.fire('Document has not been sent', '', 'info')  
            //   Swal.fire({
            //     icon: 'info',
            //     title: 'Document has not been sent',
            //     showConfirmButton: true,
            //     confirmButtonText:"OKAY",
            //   //  timer: 2000,
            //     allowOutsideClick:true
            //   }) 
           }
      });
}
 const Revokethedocment=()=>{
    let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
    // const date=(moment(new Date()).format('YYYY-MM-DD h:mm:ss')).toString();
    const date=moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    const timezone = timeZone;

    let payload ={"ADocumentId":Revokethedocmentid,"DateTime":`"${date.toString()}"`,"NotifyRecipients":false,"Reason":revokedocreasion,"TimeZone":`"${timezone.toString()}"`}
       revokedocument(payload)
//     Swal.fire({
        
//         title: "Revoke Document",
//         text: "By Revoking this document, you are cancelling all remaining signing activities. Recipients who have finished and not finished signing will receive an email notification that includes your reason for avoiding. Recipients who have not yet signed will not be able to view or sign the enclosed documents.",
//         input: "text",
//         showCancelButton: true,
//         showConfirmButton:true,
//         confirmButtonText:"REVOKE",
//         cancelButtonText:"CANCEL",
//         confirmButtonColor: "#1c8fec",
//         width:"800px",
//         display:"flex",
//         allowOutsideClick:true,
//       }).then(function(result){
//         if (result.isConfirmed === true &&result.value ==='') {
//           //  Swal.fire(`You can't Revoke Document without reason`, '', 'info')
//             Swal.fire({
//                 icon: 'info',
//                 title: `You can't Revoke Document without reason`,
//                 showConfirmButton: true,
//                 confirmButtonText:"OKAY",
//               //  timer: 2000,
//                 allowOutsideClick:true
//               }) 
//             .then((result)=>{
//                 if(result.isConfirmed){
//                     Revokethedocment(id)
//                 }
//             }) 
//         }
//         else if(result.isConfirmed === true && result.value !== ""){
//             let payload ={"ADocumentId":id,"DateTime":`"${date.toString()}"`,"NotifyRecipients":false,"Reason":result.value,"TimeZone":`"${timezone.toString()}"`}
          
        
//         }
  
//  })
}
const Createtemplate=(id)=>{
    manageservice.gettemplateCategorys().then((result)=>{
        if(result){
            settemplatecategoryslist(result?.Entity)
        }
    }).catch({

    })
    setshowcreatetemplatemodule(true)
    //  Swal.fire({
    //     title:"Create Template",
    //     //width:"800px",  
    //     html: 
    //        `<div>
    //        <label >Title</label>
    //        <input id="title" class="form-control mt-1" type="text" placeholder="Title is Required" aria-label="default input example">
    //        </div>
    //        <div class="form-check form-check-inline mt-4 ml-2">
    //        <input class="form-check-input" type="checkbox" id="Ispublic" value="option1">
    //        <label class="form-check-label mt-1" for="inlineCheckbox1">Is Public</label>
    //        </div>
    //        <div class="form-check form-check-inline mt-4 ml-2">
    //        <input class="form-check-input" type="checkbox" id="prefill" value="option2">
    //        <label class="form-check-label mt-1" for="inlineCheckbox2">Pre-fill</label></div>`,
    //     showCancelButton: true,
    //     confirmButtonColor: "#1c8fec",
    //     cancelButtonColor: "#fa013b",
    //     preConfirm:()=>{
    //         var Ispublic = Swal.getPopup().querySelector('#Ispublic').checked
    //         var prefill = Swal.getPopup().querySelector('#prefill').checked
    //         var Title = Swal.getPopup().querySelector('#title').value
    //         return {Ispublic: Ispublic, prefill: prefill,Title:Title}
    //     }
    //  }).then((result)=>{
    //      if(result.isConfirmed === true &&result.value.Title !== ""){
    //         let payload={"ADocumnetId":id, "IsPrefilled":result.value.prefill,"IsPublic":result.value.Ispublic,"TemplateTitle":result.value.Title}
    //         manageservice.CreateTemplate(payload).then((result)=>{
    //           if(result.HttpStatusCode === 200){
    //             Swal.fire('Template Created!', '', 'success') 
    //           }
    //         }).catch({})
            
    //      }
    //      else if(result.isConfirmed === true && result.value.Title === "")
    //       {
    //         Swal.fire('You can not Created Template without giving a Title', '', 'info').then((result)=>{
    //             if(result.isConfirmed){
    //                 Createtemplate(id)
    //             }
    //         })
    //      }
    //  })
}
const Savetemplate=()=>{
    let alltags =tags.toString()
    let payload={
    "ADocumnetId": Selecteddoc.ADocumentId,
    "IsPrefilled":prefill,
    "IsPublic":Ispublic,
    "TemplateTitle":Title,
    "TemplateCategoryId":Selectedtemplatecategory,
    "Tags":alltags,
    "DateTime":moment(new Date()).local().format('YYYY-MM-DD HH:mm:ss')
    }
//    Title===""?
    // Swal.fire(`please fill all mandatory fields`, '', 'info') :
    manageservice.CreateTemplate(payload).then((result)=>{
        if(result.HttpStatusCode === 200){
          setshowcreatetemplatemodule(false)
          setSelectedtemplatecategory("")
          setIspublic(false)
          setTitle("")
          setprefill(0)
          setTags([])
       //   Swal.fire('Template Created', '', 'success') 
          Swal.fire({
            icon: 'success',
            title: 'Template Created',
            showConfirmButton: true,
            confirmButtonText:"OKAY",
          //  timer: 2000,
            allowOutsideClick:true
          }) 
        }
    }).catch({})
    
}
// const DownloadDocument=(id)=>{
//     //window.open('/index1.php/api/adocuments/download/design/pdf/'+id);
//     window.open(API_END_POINT + 'adocuments/download/design/pdf/' + id, '_blank').focus();
//     //window.open(API_END_POINT + 'billing/' + invoiceId + '/pdf/' + userData.user_id, '_blank').focus();
// }
const DownloadDocument=(item)=>{
    if(item.AadhaarEsignCount> 0){
        window.open(`https://sandbox.signulu.com/index1.php/api/adocuments/download/design/pdf/`+item.ADocumentId);
    }
    else{
        window.open(API_END_POINT+`adocuments/download/design/pdf/`+item.ADocumentId);}
    // let FileName = item.Title
    // let Extension = item.StatusCode ==="APPROVED"?'.zip':'.pdf'
    // if(item.Title) {
    //     FileName = item.Title.split('.pdf')[0]  + Extension
    // }
    // manageservice.downloadDocument(item.ADocumentId)
    // .then(response=> {
    //     const blob = new Blob([response], { type: 'application/octet-stream' });
    //     const element = document.createElement('a');
    //     element.href = URL.createObjectURL(blob);
    //     element.download = FileName;
    //     document.body.appendChild(element);
    //     element.click();
    // })
}

const Resendotp =(id)=>{
     manageservice.Resendotp(id).then((result)=>{
        if(result.HttpStatusCode === 200){
          //  Swal.fire('Resent OTP', '', 'success') 
            Swal.fire({
                icon: 'success',
                title: 'OTP Resent Successfully',
                showConfirmButton: true,
                confirmButtonText:"OKAY",
              //  timer: 2000,
                allowOutsideClick:true
              }) 
        }
     })
}
const ViewRejectedreason =(ADocumentId)=>{
    addDocumentService.getrejecteddocumentreasion(ADocumentId).then(result=>{
        if(result){
             //Swal.fire('', `${result?.Entity.actionReason}`, '') 
             Swal.fire({
                //icon: 'success',
                text: `${result?.Entity.actionReason}`,
                showConfirmButton: true,
                confirmButtonText:"OKAY",
              //  timer: 2000,
                //allowOutsideClick:true
              }) 
        }
    })
    //Swal.fire(`${reason}`, '', '') 
}
const getaudittrailinfo=(id)=>{
    manageservice.getaudittrailinfo(id).then((result)=>{
       if(result){
        setAudittraildata(result?.Entity)
        setshowAuditmodule(true)
       }
    }).catch({

    })

}
const reactToPrintContent = React.useCallback(() => {
    setprint(true)
    return printRef.current;
  }, [printRef.current]);

  const handilechangeinput =(index,e)=>{
        const values=[...Inputfields]
        values[index][e.target.name]=e.target.value;
        values[index].RecipientId=-(index+1)
        values[index].id=(`choice${(index+1)}`)
        //values[index][SendToUser]=true
       // values[index][IsDelete]=0
        setinputfield(values)
  }
  const addrecipients=()=>{
    Inputfields.map(item=>{
         if(item.EmailAddress==="" ||item.FirstName==="" ||item.LastName===""){
           // Swal.fire("Unable to add a new recipient ","","info")
           Swal.fire({
            icon: 'info',
            title: 'Unable to add a new recipient ',
            showConfirmButton: true,
            confirmButtonColor:"OKAY",
            //timer: 2000
          })
         }
         else{
            setinputfield([...Inputfields,{
                EmailAddress:"",
                FirstName: "",
                IsDelete: 0,
                LastName: "",
                RecipientId:null ,
                SendToUser: true,
                id: null }])
         }
    })
   
  }
  const removeRecipients=(index)=>{
    const values=[...Inputfields]
    values.splice(index,1)
    setinputfield(values)
  }
  const Sharedocument=()=>{
    let payload1={"ADocumentId":Selecteddoc.ADocumentId,"Recipients":Inputfields}
    manageservice.ShareDocument(payload1).then((result)=>{
       if(result){
        if(result.HttpStatusCode=== 200){
            setshowsharedocument(false)
            setinputfield([])
            Swal.fire({
                icon: 'success',
                title: 'Document has been Shared',
                showConfirmButton: true,
               // confirmButtonColor:"OKAY",
                confirmButtonText:"OKAY"
                //timer: 2000
              }) 
              
        }
       }
    }).catch({

    })
  }
  const Exporttodrive = (name) => {
    setChildArr([])
    setGoodrivedata([])
    addDocumentService.getgoogledrivedata().then((result) => {
        if (result && result.Entity) {
            console.log(result, 'googleData')
            result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
            setGoodrivedata(result?.Entity)
        }
    }).catch({
    })
    setExporttitle("Google Drive")
    if(name=="googleDrive"){
      setShowImportDrivemodel(true)
    }
    else{
        setshowdrivemodel(true)
    }
   
}
  
const getchilddata = (item) => {
    setselectedfolder(item);
    addDocumentService.getgoogledrivechilddata(item.Id).then((result) => {
        if (result?.Entity?.length > 0) {
            item.children = result.Entity;
            setchilditem(item)
        }
    })
    setisfolderopen(!isfolderopen);
}
    const Exporttodropboxdrive = (name) => {
        setGoodrivedata([])
        setChildArr([])
        addDocumentService.getdropboxdrivedata().then((result) => {
            if (result && result.Entity) {
                result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
                setGoodrivedata(result?.Entity)
            }
        }).catch({
        })
        setExporttitle("Drop Box")
        if(name=="dropBox"){
            setShowImportDrivemodel(true)
          }
          else{
              setshowdrivemodel(true)
          }
    }
    const getdropboxchilddata = (item) => {
        setselectedfolder(item);
        addDocumentService.getdropboxdrivechilddata(item.Id).then((result) => {
            if (result?.Entity?.length > 0) {
                item.children = result.Entity;
                setchilditem(item)
            }
        })
        setisfolderopen(!isfolderopen);
    }

    const Exporttoonedrivedrive = (name) => {
        setGoodrivedata([])
        setChildArr([])
        addDocumentService.getonedrivedata().then((result) => {
            if (result && result.Entity) {
                result.Entity.forEach(e => { e.isChecked = false })
                setGoodrivedata(result?.Entity)
            }
        }).catch({
        })
        setExporttitle("One Drive")
        if(name=="oneDrive"){
            setShowImportDrivemodel(true)
          }
          else{
              setshowdrivemodel(true)
          }
    }
    const getonedrivechilddata=(item)=>{
        setselectedfolder(item);
        addDocumentService.getonedrivechilddata(item.Id).then((result)=>{
               if(result?.Entity?.length>0){
                item.children=result.Entity;
                 setchilditem(item)
                }
         })
        setisfolderopen(!isfolderopen);
        }

    const ExporttoCloud=()=>{
    {selectedfolder.IsFolder === true?Selecteddoc.FolderId= `${selectedfolder.Id}`:""}
        {Exporttitle==="Drop Box"? manageservice.savetodropbox(Selecteddoc).then((result)=>{
            if(result){
                if(result?.HttpStatusCode === 200){
                    Swal.fire({
                        icon: 'success',
                        title: 'Document has been exported successfully.',
                        showConfirmButton: true,
                        confirmButtonText:"OKAY",
                        //timer: 2000
                      })
                }
                else{
                    Swal.fire({
                        icon: 'worning',
                        title: 'Somthing is Wrong',
                        showConfirmButton: true,
                        confirmButtonText:"OKAY"
                        //timer: 2000
                      })
                }
            }
        }): 
        Exporttitle==="Google Drive"?manageservice.savetoGoogledrive(Selecteddoc).then((result)=>{
            if(result){
                if(result?.HttpStatusCode === 200){
                    Swal.fire({
                        icon: 'success',
                        title: 'Document has been exported successfully.',
                        showConfirmButton: true,
                        confirmButtonText:"OKAY",
                        //timer: 2000
                      })
                }
                else{
                    Swal.fire({
                        icon: 'worning',
                        title: 'Somthing is Wrong',
                        showConfirmButton: true,
                        confirmButtonText:"OKAY"
                        //timer: 2000
                      })
                }
            }
        }
        ):Exporttitle==="One Drive"?manageservice.savetoonedrive(Selecteddoc).then((result)=>{
            if(result){
                if(result?.HttpStatusCode === 200){
                    Swal.fire({
                        icon: 'success',
                        title: 'Document has been exported successfully.',
                        showConfirmButton: true,
                        confirmButtonText:"OKAY"
                        //timer: 2000
                      })
                }
                else{
                    Swal.fire({
                        icon: 'worning',
                        title: 'Somthing is Wrong',
                        showConfirmButton: true,
                        confirmButtonText:"OKAY"
                        //timer: 2000
                      })
                }
            }
        }
        ): ""}
        setshowdrivemodel(false)
    }
    
    const openApprovePage = (item) => {
        usehistory.push('/account/documentagreement/'+ item.ADocumentId)
    }
    const viewApprovePage = (item) => {
        usehistory.push('/account/documentagreementview/'+ item.ADocumentId)
    }
    const EditDocument =(documentID)=>{
        usehistory.push({
            pathname: `/account/createdocument`,
            state: {DocumentID:`${documentID}`
        }})
    }
    const checkBoxHandler = (value, item) => {
        if(value){
            setChildArr([...childArr,item])
        }
        else{
            let index =childArr.indexOf(e=>e.Id==item.Id)
            console.log(index)
            childArr.splice(index,1);
            setChildArr([...childArr])
        }
        
        }
    const insertHandler=()=>{
        console.log(Goodrivedata, 'Goodrivedata')
        commonService.setCloudDocuments({DocsData:childArr, type: Exporttitle})
        setShowImportDrivemodel(false)
        uploadDiv()
        usehistory.push('/account/adddocumentsstep')
        }
        const filterexceldata=()=>{
            let exceldata = []
            doclist.map(item=>{
                let newitem = {}
                newitem.Document = item.Title
                newitem.Sharedwith =item.RecipientNames
                newitem.CreatedOn = item.CreatedOn
                newitem.UpdatedOn = item.UpdatedOn
                newitem.Owner = item.OwnedBy
                newitem.Status = item.StatusName
                exceldata.push(newitem)
            })
            setexceldata(exceldata)
            const headings = [[
                'Document Name',
                'Shared With',
                'Created Date',
                'Updated Date',
                'Owner',
                'Status'
            ]]
            const wb = utils.book_new();
            const ws = utils.json_to_sheet([]);
            utils.sheet_add_aoa(ws, headings);
            utils.sheet_add_json(ws, exceldata, { origin: 'A2', skipHeader: true });
            utils.book_append_sheet(wb, ws, 'Report');
            writeFile(wb, 'Documents Report.xlsx');
        }
        const useOutsideAlerter1 = (ref1) => {
            useEffect(() => {
                function handleClickOutside(event) {
                    if (ref1.current && !ref1.current.contains(event.target) && event.target.id != 'filter_dropdown'&& event.target.id != 'wrapper_other') {
                        dropdownclose()
                    }
                }
                document.addEventListener("mousedown", handleClickOutside);
                return () => {
                    document.removeEventListener("mousedown", handleClickOutside);
                };
            }, [ref1]);
        }
        useOutsideAlerter1(wrapperRef1);
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content manage_content managehome">
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                            <h3 style={{fontSize:"35px" ,fontWeight:300}} >Manage</h3>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                            <div className="recipient-btn">
                            <button className="btn back_btn" onClick={()=>history.push('/account/dashboard')} ><img src="src/images/home_icon.svg" alt=""   style={{marginRight:"2px",width: "15px",height: "15px"}} /></button>
                               
                                {/* <div onMouseLeave={dropdown3} id="dropdown1" className="adddropdown" style={{ display: 'none' }}>
                                                     <ul className='mt-2 p-0'> 
                                                       <li className='mb-2'><a onClick={()=>history.push('/account/createdocument')} className='pl-2' style={{fontSize:18,color:"#000" ,fontWeight:300}} >Create Document</a></li>
                                                       <li className='mb-2'><a aria-selected="false" className='pl-2' style={{fontSize:18,color:"#000",fontWeight:300}}>Create Form Template</a></li>
                                                       <li className='mb-2'><a onClick={()=>{uploadDiv();dropdown3();}} className='pl-2' style={{fontSize:18,color:"#000",fontWeight:300}}>Upload Files</a></li>
                                                      
                                                        </ul>
                                   </div> */}
                                 <div class="dropdown">
                                {/* <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown button
                                </button> */}
                                 <button id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false" className="btn doc_btn"><img   src="src/images/plus_icon1.svg" alt="" />New Document</button>
                                <ul className="dropdown-menu p-2" id="dropdown1" aria-labelledby="dropdownMenuButton1">
                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>history.push('/account/createdocument')}>Create Document</a></li>
                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>history.push('/account/template')}>Create From Template</a></li>
                                    <li style={{cursor:"pointer"}}><a className="dropdown-item"onClick={()=>{uploadDiv()}}>Upload Files</a></li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <div className="upload_sec popup_ht" id="upload" style={{ display: "none" }}>
              <div className="content">
                <div className='pop_close'>
                  <h4>{t('UPLOAD_YOUR_FILES')}
                  <Tooltip placement='top' title='Supported File Formats: .PDF,.DOC,.DOCX,.XLS,.XLSX,.JPG,.PNG,.JPEG'>
                            <img className='ml-1' style={{width:10,height:10,marginBottom:2, backgroundcolor:"blue"}} src="src/images/info-circle.svg" alt="info icon" />
                            </Tooltip>
                  </h4>
                  <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={() => { uploadDiv()}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                  {/* <CloseIcon className="close_add_doc_pop" onClick={() => { uploadDiv(), removeSessionItem('replaceIndex') }} /> */}
                </div>
                <h5>{t('USE_TEMPLATE_THIRD_PARTY')}</h5>
                <div className="upload_block">
                  {/* <div className="use_templete">
                            <img src="src/images/folder.svg" alt='' />
                            <h3>{t('USE_TEMPLATE')}</h3>
                        </div> */}
                        <div className="use_templete"  onClick={()=>{if(userInfo.IsGoogleDriveEnabled){Exporttodrive("googleDrive")}}}>
                                                        <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/google_drive.svg" alt='' />
                                                        <div data-tip data-for='google_drive'>
                                                        {userInfo.IsGoogleDriveEnabled?
                                                        <DoneIcon  style={{color:"green"}} onClick={(event)=>{event.stopPropagation();}}/>
                                                        : <CloseIcon  style={{color:"red"}} />                      
                                                        }
                                                        <ReactTooltip id="google_drive" place="top" effect="solid">
                                                        {userInfo.IsGoogleDriveEnabled?<>Connected</>:<>Disconnected</>}</ReactTooltip>
                                                        </div>
                                                        </div>
                                                        <h3>{t('GOOGLE_DRIVE')}</h3>
                                                        {/* <h4>1,357</h4> */}
                                                    </div>
                                                    <div className="use_templete"  onClick={()=>{if(userInfo.IsOneDriveEnabled){Exporttoonedrivedrive("oneDrive")}}}>
                                                    <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/one_drive.svg" alt='' />
                                                        <div  data-tip data-for='one_drive'>
                                                        {userInfo.IsOneDriveEnabled?<DoneIcon   onClick={(event)=>{event.stopPropagation();}} style={{color:"green"}} />:<CloseIcon style={{color:"red"}} />}
                                                        <ReactTooltip id="one_drive" place="top" effect="solid">
                                                        {userInfo.IsOneDriveEnabled?<>Connected</>:<>Disconnected</>}</ReactTooltip>
                                                        </div>
                                                        </div>
                                                        <h3>{t('ONE_DRIVE')}</h3>
                                                        {/* <h4>2,14777 Files</h4> */}
                                                    </div>
                                                    <div className="use_templete" onClick={()=>{if(userInfo.IsDropBoxEnabled){Exporttodropboxdrive("dropBox")}}}>
                                                    <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/drop_box.svg" alt='' />
                                                        <div  data-tip data-for='drop_box'>
                                                        {userInfo.IsDropBoxEnabled?<DoneIcon  onClick={(event)=>{event.stopPropagation();}} style={{color:"green"}} />:<CloseIcon style={{color:"red"}} />}
                                                        <ReactTooltip id="drop_box" place="top" effect="solid">
                                                        {userInfo.IsDropBoxEnabled?<>Connected</>:<>Disconnected</>}</ReactTooltip>
                                                        </div>
                                                        </div>
                                                        <h3>{t('DROP_BOX')}</h3>
                                                        {/* <h4>1,457 Files</h4> */}
                                                    </div>
                                                    <div className="use_templete drop-zone card_design"  onClick={getTags}>
                                                    <div style={{display:"flex",justifyContent: "space-between"}}>
                                                        <img src="src/images/folder_1.svg" alt='' />
                                                        <div >
                                                        <input type="file" multiple accept=".doc,.docx,image/png,image/jpg,image/jpeg,.xls,.xlsx,.pdf" name="myFile" className="drop-zone__input" />
                                                        </div>
                                                        </div>
                                                        <h3 className="drop-zone__prompt font">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h3>
                                                        {/* <h4>2,14777 Files</h4> */}
                                                    </div>
                        {/* <div className="use_templete drop-zone" onClick={getTags}>
                            <img src="src/images/folder_1.svg" alt='' className="img-fluid mx-auto d-block" />
                            <h4 className="drop-zone__prompt">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h4>
                            <input type="file"  accept=".doc,.docx,image/png,image/jpg,image/jpeg,.ppt,.pptx,.xls,.xlsx,.pdf" name="myFile" className="drop-zone__input" />
                        </div> */}
                    </div>
                    {/* <div className="back"onClick={uploadDiv}>
                        <h6 className="fileback"><span ><img src="src/images/back.svg" alt='' /></span>{t('BACK')}</h6>
                    </div> */}
                </div>
            </div>
                        {/* <div className="upload_sec" id="upload" style={{display: "none"}}>
                <div className="content">
                    <h4>Upload Your Files</h4>
                    <h5>Use Template, Google Drive, One Drive And Drop Box</h5>
                    <div className="upload_block">
                        <div className="use_templete">
                            <img src="src/images/folder.svg" alt='' />
                            <h3>Use Template</h3>
                            <h4>1,235 Files </h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/google_drive.svg" alt='' />
                            <h3>Google Drive</h3>
                            <h4>1,357</h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/one_drive.svg" alt='' />
                            <h3>One Drive</h3>
                            <h4>2,143 Files</h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/drop_box.svg" alt='' />
                            <h3>Drop Box</h3>
                            <h4>1,457 Files</h4>
                        </div>
                        <div className="use_templete drop-zone" onClick={getTags}>
                            <img src="src/images/folder_1.svg" alt className="img-fluid mx-auto d-block" />
                            <h4 className="drop-zone__prompt" >Click To Upload or Drag &amp; Drop Your File Here</h4>
                            <input type="file" name="myFile" accept=".doc,.docx,image/png,image/jpg,image/jpeg,.ppt,.pptx,.xls,.xlsx,.pdf" className="drop-zone__input" />
                        </div>
                    </div>
                 
                </div>
            </div> */}
                    </div>
                    <div className='flex justify-content-end p-3'>
                        {/* <CSVLink data={exceldata} fileName="AllDocumentsList"><img src="src/images/XLSX.png" alt="" onClick={()=>{filterexceldata()}} style={{width:35,height:35}}/></CSVLink> */}
                       
                        </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="table__row__conts table-responsive" style={{marginTop:10}}>
                                <table className="table " >
                                    <thead className='first_head'>
                                        <tr>
                                            <th className='px-4'><div className="top" >
                                            <div className="Filters dropdown-toggle"><span><img style={{cursor:"context-menu"}} src="src/images/filters.svg" alt="" /></span> <a id="filter_dropdown" onClick={dropdown2}>Filters </a><span><img style={{cursor:"context-menu"}} src="src/images/down_arrow1.svg" alt="" /></span>
                                            <div id="dropdown" className="filter_dropdown dropdown-menu " ref={wrapperRef1} style={{ display: 'none' }}>
                                                <ul >
                                                <li  onClick={()=>{setStatusCode(null),setemail(false),dropdownclose()}}><a><span ></span>All</a></li>
                                                       <li data-tip data-for="Draft"  onClick={()=>{dropdownclose(),setStatusCode("DRAFT"),setemail(false)}}><a><span className="drafts"></span>Drafts</a></li>
                                                       <ReactTooltip id="Draft" place="top" effect="solid">Documents that are in progress.</ReactTooltip>
                                                       <li  data-tip data-for="pending" onClick={()=>{dropdownclose(),setStatusCode("WAITFOROTHERS"),setemail(false)}}><a ><span className="pending"></span>Pending</a></li>
                                                       <ReactTooltip id="pending" place="top" effect="solid">Documents pending for others’ signatures.</ReactTooltip>
                                                        <li data-tip data-for="sign" onClick={()=>{dropdownclose(),setStatusCode("TOSIGN"),setemail(true)}}><a><span className="sign"></span>To Sign</a></li>
                                                        <ReactTooltip id="sign" place="top" effect="solid">Documents pending for my signature.</ReactTooltip>
                                                        <li data-tip data-for="mydoc" onClick={()=>{dropdownclose(),setStatusCode("MYDOCUMENTS"),setemail(false)}} ><a><span className="invitation"></span>My Documents</a></li>
                                                        <ReactTooltip id="mydoc" place="top" effect="solid">Documents that are initiated by me and signed/approved by other recipients.</ReactTooltip>
                                                        <li data-tip data-for="completed" onClick={()=>{dropdownclose(),setStatusCode("APPROVED"),setemail(true)}}><a ><span className="completed"></span>Completed</a></li>
                                                        <ReactTooltip id="completed" place="top" effect="solid"> Documents that are initiated by others and signed/approved by myself and others.</ReactTooltip>
                                                        <li data-tip data-for="rejected" onClick={()=>{dropdownclose(),setStatusCode("REJECTED"),setemail(false)}}><a ><span className="rejected"></span>Rejected</a></li>
                                                        <ReactTooltip id="rejected" place="top" effect="solid">Documents that are rejected by recipients for various reasons. (Note: Rejected documents can be resent after correction)</ReactTooltip>
                                                        <li data-tip  data-for="cancelled" onClick={()=>{dropdownclose(),setStatusCode("CANCELLED"),setemail(false)}}><a ><span className="cancelled"></span>Cancelled</a></li>
                                                        <ReactTooltip id="cancelled" place="top" effect="solid">Documents that are cancelled by me for various reasons. (Note: Cancelled documents cannot be resent)</ReactTooltip>
                                                        </ul>
                                                         </div>
                                                         </div>
                                                         </div></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>
                                            <div data-tip data-for="XLSX" onClick={()=>{filterexceldata()}} ><img src="src/images/XLSX.png" alt="" style={{width:35,height:35,cursor:"pointer",marginRight:20}}/>
                                                            <ReactTooltip id="XLSX" place="top" effect="solid">Export to Excel</ReactTooltip>
                                                         </div>
                                                </th>
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th  style={{cursor:"auto",position:"relative",textAlign:"start",fontSize:14,paddingLeft:47}}>Document</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:32}}>Shared with</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:33}}>Created On</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:33}}>Updated On</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:33}}>Owner</th> 
                                            <th>Status</th>
                                            {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {perpage.length>0?perpage.map(item=>{
                                            
                                            return(
                                             <tr>
                                             <td  style={{position:"relative",textAlign:"start",width:"28%"}}>
                                               {/* <img onClick={()=>{dropdown4(item.ADocumentId)}} src='src/images/down_arrow.svg' alt=""  style={{padding:5,border:"1px solid #c7a9a9 ",backgroundColor:"#ededed"}}/> */}
                                             
                                                <button style={{padding:0}} id={`${"dropdownMenuButton1"+item.ADocumentId}`} data-bs-toggle="dropdown" aria-expanded="false" className="btn doc_btn"><img   src="src/images/plus_icon.svg" alt="" /></button>
                                                <label className='px-2' style={{cursor:"auto"}}  htmlFor="check1">
                                                {item.Title!== null&&item.Title.length>20?<Tooltip placement="top" title={item.Title} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                         width: 180,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        marginBottom:"-4px"
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.Title}
                                                    </div>
                                                </Tooltip>:item.Title}
                                                
                                                </label>
                                                
                                                {/* <div  id={item.ADocumentId} className=" dropdown-menu" style={{ display: 'none' }}> */}
                                               <ul className="dropdown-menu p-2"  aria-labelledby={`${"dropdownMenuButton1"+item.ADocumentId}`}>
                                                   {item.IsOwner === 1 && (item.StatusCode === "DRAFT")&& item.WorkflowCode ==='STANDARD' && item.OwnedBy === "You" && item.IsFromScratchDocument === 1 ?
                                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>{EditDocument(item.ADocumentId)}}>Edit</a></li>:""}  
                                                    {item.IsOwner === 1 && (item.StatusCode === "DRAFT" || item.StatusCode === "REJECTED")&& item.orkflowCode ==='STANDARD' && item.OwnedBy === "You" ?
                                                    <li><a className="dropdown-item" href="#">Design Mode</a></li>:""}
                                                    {item.IsSecured === 1 && item.StatusCode === "SENT" && item.TotalRecipients !== item.RecipientApprovedCount?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{Resendotp(item.ADocumentId)}}><a className="dropdown-item">Resend OTP</a></li>:""}
                                                    {(item.IsOwner === 1 && item.StatusCode === "APPROVED") || (item.StatusCode === "DRAFT" && item.TotalRecipients === 1) && item.orkflowCode ==='STANDARD' ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowsharedocument(true)}}><a className="dropdown-item">Share</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "SENT" &&item.OwnedBy === "You" ?
                                                    <><li style={{cursor:"pointer"}} onClick={()=>{Getrecipients(item.ADocumentId)}}><a className="dropdown-item">Resend</a></li>
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowmanagevaliditydocumentmodule(true)}}><a className="dropdown-item" >Manage Validity</a></li>
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setRevokethedocmentid(item.ADocumentId),setshowrevokedocumentpopup(true)}}><a className="dropdown-item">Revoke</a></li>
                                                    </>
                                                    :""}
                                                    {item.IsOwner === 1 && (item.StatusCode === "DRAFT" || item.StatusCode === "REJECTED") && item.TotalRecipients >=1 &&item.IsFromScratchDocument === 0 && item.OwnedBy === "You" ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{history.push(`documentselect/${item.ADocumentId}`)}}><a className="dropdown-item"> Fill and Send</a></li>:""}
                                                    { (item.OwnedBy === "You" && item.IsOwner === 1 &&  item.StatusCode === "SENT" && item.ViewerStatusCode === "SENT")?
                                                    <li style={{cursor:"pointer"}}><a className="dropdown-item" onClick={()=>openApprovePage(item)}>{item.RecipientStatus}</a></li>:""}
                                                    {/* <li onClick={()=>openApprovePage(item)}><a className="dropdown-item" href="javascript:void(0);" >Approve</a></li> */}

                                                     {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsGoogleDriveEnabled === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttodrive(item.ADocumentId)}} ><a className="dropdown-item">Export to drive</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsOneDriveEnabled === 1 ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttoonedrivedrive(item.ADocumentId)}} ><a className="dropdown-item" >Export to one drive</a></li>:""}
                                                    {item.IsOwner === 1 && item.StatusCode === "APPROVED" && item.IsDropBoxEnabled === 1 ?
                                                    <li  style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),Exporttodropboxdrive(item.ADocumentId)}} > <a className="dropdown-item">Export to drop box</a></li>:""}
                                                    {item.IsOwner === 1 && item.WorkflowCode === "STANDARD" && item.OwnedBy === "You" ?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item), Createtemplate(item.ADocumentId)}} ><a className="dropdown-item">Create Template</a></li>:""}
                                                      {item.IsOwner === 1 && item.WorkflowCode === "STANDARD"?
                                                    <li style={{cursor:"pointer"}}  onClick={()=>{getaudittrailinfo(item.ADocumentId)}}><a className="dropdown-item">Audit Trail</a></li>:""}
                                                    {(item.IsOwner === 1 && item.StatusCode !== "SENT") ?item.StatusCode !== "CAMPAIGNSTARTED"?
                                                    <li style={{cursor:"pointer"}} onClick={()=>{setSelecteddoc(item),setshowdeletedocumentmodule(true)}}><a className="dropdown-item">Delete</a></li>:"":""}
                                                     {item.WorkflowCode === "STANDARD"?
                                                     <>
                                                     <li style={{cursor:"pointer"}} ><a className="dropdown-item" onClick={()=>viewApprovePage(item)}>View</a></li>
                                                     <li style={{cursor:"pointer"}} onClick={()=>{DownloadDocument(item)}}><a className="dropdown-item">Download</a></li>
                                                     </>
                                                    :""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "DRAFT"?
                                                    <li><a className="dropdown-item" href="#">AStart Campaign</a></li>:""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "CAMPAIGNSTARTED"?
                                                    <>
                                                    <li><a className="dropdown-item" href="#">Stop Campaign</a></li>
                                                    <li><a className="dropdown-item" href="#">Download Petition</a></li>
                                                    <li><a className="dropdown-item" href="#">Request Signature</a></li>
                                                    <li><a className="dropdown-item" href="#">Signatory Count</a></li>
                                                    </>
                                                     :""}
                                                    {item.WorkflowCode === 'SIGNCAMP' && item.StatusCode === "CAMPAIGNSTOPPED"?
                                                    <>
                                                    <li><a className="dropdown-item" href="#">Download Petition + 10 pages</a></li>
                                                    <li><a className="dropdown-item" href="#">Download</a></li>
                                                    </>
                                                    :""}
                                                    { item.IsOwner === 1 && item.StatusCode === "REJECTED"?
                                                     <li style={{cursor:"pointer"}} onClick={()=>{ViewRejectedreason(item.ADocumentId)}}><a className="dropdown-item">View Reason</a></li>:""
                                                    }
                                                </ul>
                                               {/* </div> */}
                                                </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item.RecipientNames!== null&&item.RecipientNames.length>10?
                                             <Tooltip placement="top" title={item.RecipientNames} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        position:"relative",
                                                        textAlign:"start",
                                                        width: 80,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.RecipientNames}
                                                    </div>
                                                </Tooltip>:item.RecipientNames !== null?item.RecipientNames:"-"}
                                                
                                             </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>
                                                <><div>{moment.utc(item?.CreatedOn).local().format('MM/DD/YYYY')}</div>
                                                <div>{moment.utc(item?.CreatedOn).local().format('h:mm A')}</div></>
                                                
                                            </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>
                                               {item?.UpdatedOn !== null ? <><div>{(moment.utc(item?.UpdatedOn).local().format('MM/DD/YYYY'))}</div>
                                                                            <div>{(moment.utc(item?.UpdatedOn).local().format('h:mm A'))}</div></>:
                                                                             <><div>{moment.utc(item?.CreatedOn).local().format('MM/DD/YYYY')}</div>
                                                                             <div>{moment.utc(item?.CreatedOn).local().format('h:mm A')}</div></>}
                                                </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item.OwnedBy!== null&&item.OwnedBy.length>15?
                                             <Tooltip placement="top" title={item.OwnedBy} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        width: 100,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        textAlign:"start",
                                                        position:"relative"
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.OwnedBy}
                                                    </div>
                                                </Tooltip>:item.OwnedBy !== null?item.OwnedBy:"-"}
                                                </td>
                                             <td    ><div className='table_btn' style={{width:"max-content"}}>
                                                {/* <button  style={{cursor:"context-menu",margin:0, border:"0.5px solid black",background:"none"}}
                                                // className={`btn ${item.StatusCode ==="DRAFT"?"draft":
                                                // item.StatusCode ==="WAITFOROTHERS"?"pending":
                                                // item.StatusCode ==="ACTIONREQUIRED"?"sign":
                                                // item.StatusCode ==="APPROVED" && item.OwnedBy==="You"?"invitation":
                                                // item.StatusCode ==="APPROVED"?"completed":
                                                // item.StatusCode ==="SENT"?"pending":
                                                // item.StatusCode ==="REJECTED"?"rejected":
                                                // item.StatusCode ==="CANCELLED"?"cancelled"
                                                // :""          
                                                // }-btn`} 
                                               >{item.StatusCode ==="DRAFT"?<img src= 'src/images/draft.svg' title="Draft" style={{width:30,height:30,padding:3}}/>:item.StatusCode ==="SENT"?<img src= 'src/images/send.svg' title="Sent" style={{width:30,height:25,padding:4}}/>:item.StatusCode ==="REJECTED"?<img src= 'src/images/Rejected.svg' title="Rejected" style={{width:30,height:30,padding:3}}/>:item.StatusCode ==="CANCELLED"?<img src= 'src/images/Cancelled.svg' title="Cancelled" style={{width:30,height:30,padding:4}}/>:item.StatusCode ==="APPROVED"?<img src= 'src/images/Approved.svg' title="Approved" style={{width:30,height:30,padding:4}}/>:""}</button> */}
                                               {Statuswidth(item.StatusCode,item.TotalRecipients,item.RecipientApprovedCount)}
                                                </div>
                                                {item.StatusCode ==="DRAFT"?<span style={{color:'blue' , fontSize:'14px'}}>Document is in Draft</span>:item.StatusCode ==="SENT"?<span style={{color:'blue' , fontSize:'14px'}}>Waiting for others</span>:item.StatusCode ==="REJECTED"?<span style={{color:'red' , fontSize:'14px'}}>Document is Rejected</span>:item.StatusCode ==="CANCELLED"?<span style={{color:'red' , fontSize:'14px'}}>Document is Cancelled</span>:""}
                                                </td>
                                         </tr>)
                                        }):
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td
                                             className={"flex justify-content-center mt-4"}> No documents are found.</td>
                                        </tr>}
                                        <tr style={{height:perpage.length<=4?230:0,height:perpage.length<=7?200:0}}>
                                        </tr>
                                    </tbody>
                                    
                                </table>
                                
                            </div>
                        </div>
                    </div>
                    {PageCount>1&&
                    <div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
                                        {/* Searchstring.length>0?searchpagecount: */}
											<Pagination
                                             {...bootstrap5PaginationPreset}
												current={pageNumber}
												total={PageCount}
												onPageChange={(newPage) =>
                                                    handlePagination(newPage)}
                                                maxWidth={"80px"}
											/>
										</nav>
									</div>
								</div>
							</div>}
                 
                </div>
            </div>
            {ShowDelertalert &&<SweetAlert success onConfirm={setShowDelertalert(false)} />}
            
            <Modal show={showcreatetemplatemodule} 
                    //size="sm"
                    width="800px"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header calssName="flex" >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#949494f0"}} > Create Template</p>
                        </Modal.Title>
                        <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setshowcreatetemplatemodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowcreatetemplatemodule(false)}}/> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                         <div>
                            <label >Title<span style={{color:"red"}}>*</span></label>
                            <input id="title" onChange={(event) => {setTitle(event.target.value)}} className="form-control mt-1" type="text"  aria-label="default input example"/>
                            </div>
                            {userdata?.IsOwner===1&&<div className="form-check form-check-inline mt-4 ml-2">
                            <input onChange={(event) => {setIspublic(event.target.value==="true"?true:false)}}   className="form-check-input" type="checkbox" id="Ispublic" value="true"/>
                            <label className="form-check-label " for="inlineCheckbox1">Is Public</label>
                            </div>}
                            <div className="form-check form-check-inline mt-4 ml-2">
                            <input onChange={(event) => {setprefill(event.target.value==="true"?true:false)}} className="form-check-input" type="checkbox" id="prefill" value="true"/>
                            <label className="form-check-label " for="inlineCheckbox2">Pre-fill</label>
                             </div>
                        {/* <div className='mt-3 mb-3'>
                        <label className='mb-1' >Category<span style={{color:"red"}}>*</span></label>
                        <select style={{fontSize:16, fontWeight:300}} onChange={(event) => {setSelectedtemplatecategory(event.target.value)}} className="form-select form-select-sm form-select-border"  id='select-box'>
                            <option  selected>Select </option>
                            {templatecategoryslist.length>0&&templatecategoryslist.map(item=>{
                               return(
                               <option style={{fontSize:16, fontWeight:300}} value={item.TemplateCategoryId}>
                               {item.TemplateCategoryName}
                             </option>)
                            })}
                        </select>
                         </div>
                            <div>
                            <label >Tags<span style={{color:"red"}}>*</span></label>
                            <input className="form-control mt-1 mb-3" type="text" 
                             placeholder="Add a tag and click on enter..."
                             aria-label="default input example"
                             onKeyPress={event => {
                                if (event.key === "Enter") {
                                  setTags([...tags, event.target.value]);
                                  event.target.value = "";
                                }
                              }}
                              autofocus
                             />
                            </div> */}
                            {/* <ul className="TagListelement">
                                {tags.map(tag => (
                                    <li className="Tagelement">
                                    {tag}
                                    <XCircle
                                        className="TagIconelement"
                                        size="16"
                                        onClick={() => {
                                        setTags([...tags.filter(word => word !== tag)]);
                                        }}
                                    />
                                    </li>
                                ))}
                            </ul> */}
                        
                    </Modal.Body>
                    <Modal.Footer>
                        
                        <Button disabled={Title!==""?false:true} variant="primary"  onClick={() => {
                           Savetemplate()
                        }}>CREATE</Button> 
                         <Button variant="secondary" onClick={() => {
                            setshowcreatetemplatemodule(false)
                            setSelectedtemplatecategory("")
                            setIspublic(false)
                            setTitle("")
                            setprefill(0)
                            setTags([])
                           
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
            <Modal show={showsharedocument} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p>Share Document</p>
                            
                        </Modal.Title>
                        <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setshowsharedocument(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowsharedocument(false)}}/> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <div className='flex'>
                        <p className='-mt-1 mb-4' style={{fontSize:13,color:"#696969"}}>ADD RECIPIENT ( Selected recipients will receive a copy of the final approved document.)</p>
                        <div> <div  type="button" onClick={addrecipients}><img className='-mt-2 mb-4'  src="src/images/plus_icon.svg"   /></div></div>
                        </div>
                        
                       
                        {Inputfields.map((inputfield,index) =>{
                             return(
                            <div className='flex justify-content-center'>
                            <div className='mb-3'>
                            <input id="id" type="text" SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="EmailAddress" value={inputfield.EmailAddress} onChange={e=>handilechangeinput(index,e)} className='docinput' placeholder=' Email*'  />
                            </div>
                            <div className='mb-3'>
                                <input id="id" type="text" SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="FirstName"  value={inputfield.FirstName} onChange={e=>handilechangeinput(index,e)}  className='docinput' placeholder=' First Name*'  />
                            </div>
                            <div className='mb-3'>
                            <input id="id" type="text"  SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="LastName" value={inputfield.LastName} onChange={e=>handilechangeinput(index,e)}  className='docinput' placeholder=' Last Name*'  />
                            </div>
                            {Inputfields.length>1 && <div type="button" onClick={()=>{removeRecipients(index)}}>
                            <img className='ml-2 mt-1'
								src='src/images/cancel_close_cross_delete_remove_icon.svg'
								width={"25px"} alt=''/>
                             
                                </div> }
                             </div>)})}
                        
                        
                        
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button  variant="primary" onClick={() => {
                            let allinputs = true
                            Inputfields.map(item=>{
                                if(item.EmailAddress==="" ||item.FirstName==="" ||item.LastName===""){
                                    allinputs=false;
                                   // Swal.fire("Please fill all the details","","info")
                                    Swal.fire({
                                        icon: 'info',
                                        title: 'Please fill all the details',
                                        showConfirmButton: true,
                                        confirmButtonText:"OKAY"
                                        //timer: 2000
                                      })
                                 }
                            })
                            if(allinputs === true){
                                Sharedocument();
                             }
                        }}>SHARE</Button> 
                         <Button variant="secondary" onClick={() => {
                           setshowsharedocument(false)
                           setinputfield([])
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
                {/* Delete the DocumentModule  */}
                <Modal show={showdeletedocumentmodule} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}} > Delete document</p>
                            
                        </Modal.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        <CloseIcon className="close_add_doc_pop" onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}/>
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <label for="comment">Reason for deletion<span style={{color:"red"}}>*</span></label>
                        <div className="form-group">
                        <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="5" id="comment"  value={deletedocreason} onChange={(e)=>docreasondelete(e)}/>
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        </div>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        
                        <Button disabled={deletedocreason.length>0 && deletedocreason !== null?false:true} variant="primary"  onClick={() => {
                           deletedocument()
                           setshowdeletedocumentmodule(false)
                           setdeletedocreason("")
                        }}>DELETE</Button> 
                         <Button variant="secondary" onClick={() => {
                            setshowdeletedocumentmodule(false)
                            setSelecteddoc(null)
                            setdeletedocreason("")
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={showrevokedocumentpopup} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}} >Reason for revoking document</p>
                            
                        </Modal.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        <CloseIcon className="close_add_doc_pop" onClick={()=>{setdeletedocreason(""),setshowrevokedocumentpopup(false)}}/>
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <label for="comment">By revoking this document, you are cancelling all remaining signing activities. Recipients who have finished and not finished signing will receive an email notification that includes your reason for Revoke. Recipients who have not yet signed will not be able to view or sign the enclosed documents."<span style={{color:"red"}}>*</span></label>
                        <div className="form-group mt-2">
                        <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="3" id="comment" maxLength={200} value={revokedocreasion} onChange={(e)=>setrevokedocreasion(e.target.value)}/>
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        <span>Characters remaining:<p>{200-revokedocreasion.length}</p></span>
                        </div>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        
                        <Button disabled={revokedocreasion.length>0 && revokedocreasion !== null?false:true} variant="primary"  onClick={() => {
                           Revokethedocment()
                           setshowrevokedocumentpopup(false)
                           setrevokedocreasion("")
                        }}>REVOKE</Button> 
                         <Button variant="secondary" onClick={() => {
                            setshowrevokedocumentpopup(false)
                            //setSelecteddoc(null)
                            setrevokedocreasion("")
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={showmanagevaliditydocumentmodule} 
                    size=""
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}}> Manage document validity</p>
                            
                        </Modal.Title>
                        <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setshowmanagevaliditydocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowmanagevaliditydocumentmodule(false)}}/> */}
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        {Selecteddoc!== null &&Selecteddoc.ExpirationDate === null?<><div style={{color:"#736a6a width: 295px; overflow-wrap: break-word;"}}>
                            The validity date for this transaction has not been set by the sender.
                            </div><div style={{color:"#736a6a width: 295px; overflow-wrap: break-word;"}}>
                            Please contact the sender of the document.</div></>:
                        <div className='flex '>
                        <div >
                        <div>Current Validity Date</div>
                        <input type="text" style={{height:"50px",fontSize:"15px",color:"grey"}} className='docinput' value={Selecteddoc!== null &&Selecteddoc.ExpirationDate !== null?moment(Selecteddoc.ExpirationDate).format('MM/DD/yyyy'):""} />
                        </div> 
                        <div >
                        <div>New Validity Date <span style={{color:"red"}}>*</span></div>
                        <div className='App'>
                        {/* <DatePicker 
                            selected={updatedManagevalidityDate=== ""?null:updatedManagevalidityDate} 
                            onChange={(date) => setupdatedManagevalidityDate(date)}
                            minDate={moment().toDate()}
                            placeholderText={'  Please select a date'}
                       /> */}
                       <Calendar 
                       placeholder='Select'
                       minDate={new Date()} 
                       readOnlyInput 
                       value={updatedManagevalidityDate}
                       onChange={(e) => setupdatedManagevalidityDate(e.value)} dateFormat="mm/dd/yy" showIcon />
                        </div>
                        
                        </div> 
                        </div>}
                    
                        
                    </Modal.Body>
                    <Modal.Footer>
                        {Selecteddoc!== null &&Selecteddoc.ExpirationDate !== null&& <Button disabled ={updatedManagevalidityDate !== null? false:true} variant="primary" onClick={() => {
                           updatemanagevalidity()
                           setshowmanagevaliditydocumentmodule(false)
                           setupdatedManagevalidityDate(null)
                        }}>UPDATE</Button> }

                    </Modal.Footer>
                </Modal>
                <Modal show={showAuditmodule} 
                    size="xl"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    >
                    {/* <Modal.Header  > */}
                       
                        {/* <button type="button" className="close" style={{backgroundColor:"#ffff"}} onClick={()=>{setshowAuditmodule(false)}} 
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowAuditmodule(false)}}/>
                    </Modal.Header> */}
                    <Modal.Body className='error-modal-content'>
                    <div style={{maxHeight:300,overflowY:"auto"}}>
                        <div ref={printRef}>
                        <Modal.Header  >   
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}}> Audit Trail</p>
                            
                        </Modal.Title>
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowAuditmodule(false)}}/> */}
                        </Modal.Header> 
                        <div className="table__row__conts table-responsive">
                        <table className="table">
                        <thead style={{visibility:"collapse"}} className='first_head'>
                                        <tr>
                                            <th></th>
                                            <th style={{width:150}}></th>
                                            <th></th>
                                            <th style={{display:"none"}}>
                                                {/* 1-8 on <span>8</span> */}
                                                </th>
                                                
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"#515762"}}>Date</th>
                                            <th className='px-5' style={{position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"#515762"}}>Details</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"#515762"}}>Change </th> 
                                           <th style={{display:"none"}}></th> {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead>
                        {/* <thead className="thead-light">
                            <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Details</th>
                            <th scope="col">Change</th>
                            </tr>
                        </thead> */}
                        <tbody >
                            {Audittraildata.length>0?Audittraildata.map(item=>{
                                return(
                            <tr>
                            {/* <td>{item.Date.length>25?<Tooltip placement='top' title={item.Date} arrow={true}>
                            <div
                                style={{
                                  width: 220,
                                  overflow: "hidden",
                                  whiteSpace: "nowrap",
                                  textOverflow: "ellipsis",
                                   marginBottom:"-4px"
                                // color: "#4E2C90",
                                    }}
                                 >
                               {item.Date}
                           </div>
                            </Tooltip>:item.Date}</td> */}
                            <td style={{fontSize:14,fontWeight:300,color:"#404A5A",width:295,wordWrap:"break-word"}}>{item.Date}</td>
                            <td className='px-5' style={{fontSize:14,fontWeight:300,color:"#404A5A",maxWidth:200}}>{item.IPAddress}</td>
                            {/* <td>{item.Description.length>30?<Tooltip placement="top" title={item.Description} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        width: 220,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        marginBottom:"-4px"
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.Description}
                                                    </div>
                                                </Tooltip>:item.Description}
                            </td> */}
                            <td className='px-5' style={{fontSize:14,fontWeight:300,color:"#404A5A",width:310,wordWrap:"break-word"}}>{item.Description}</td>
                            </tr>
                                )
                            }):""}
                            
                        
                        </tbody> 
                        </table>
                        </div>
                        </div>
                        </div>
                       
                        
                        
                    </Modal.Body>
                  
                    <Modal.Footer>
                        <ReactToPrint trigger={()=>{
                             return <Button variant="primary">PRINT</Button>
                         }} 
                         content={reactToPrintContent}
                         documentTitle="AuditTrail Document"
                         pageStyle="print"
                         removeAfterPrint
                         />
                        <Button variant="secondary" onClick={()=>{setshowAuditmodule(false)}}>CLOSE</Button>
                         
                    </Modal.Footer>
                </Modal>
                <Modal show={showdrivemodel} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    scrollable={true}
                    >
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p> {Exporttitle}</p>
                            
                        </Modal.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setshowdrivemodel(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                         <CloseIcon className="close_add_doc_pop" onClick={()=>{setshowdrivemodel(false)}}/> 
                    </Modal.Header>
                        <Modal.Body className='error-modal-content'>
                         <div>
                         <ul className="list-group">
                            {Goodrivedata.length>0?Goodrivedata.map(item=>{
                                 return(
                                    item.IsFolder === true?
                                        <li className="list-group-item" style={{backgroundColor:"#e2dbdb",cursor:"pointer"}} onClick={()=>Exporttitle==="Google Drive"?getchilddata(item):Exporttitle==="Drop Box"?getdropboxchilddata(item):Exporttitle==="One Drive"?getonedrivechilddata(item): ""}>
                                          <>
                                          <i className="fa fa-folder mr-1" style={{color:"#2F7FED"}}></i>
                                           <span>{item.roleName}</span>
                                           {item.children.length>0&&<ul className="list-group" style={{display:"block"}}>
                                              { item.children.map(item1=>{
                                                return(
                                                    <li className="list-group-item" style={{cursor:"default"}} >{item1.roleName}</li>
                                                )
                                              })}
                                              </ul>}
                                           </> </li>
                                           :
                                         <li className="list-group-item" onClick={()=>Exporttitle==="Google Drive"?getchilddata(item):Exporttitle==="Drop Box"?getdropboxchilddata(item):Exporttitle==="One Drive"?getonedrivechilddata(item):""}><span>{item.roleName}</span></li>
                                         
                                 )
                            })
                        
                        :"Drive is Empty"}
                        
                        </ul>
                        </div>
                        </Modal.Body>
                    
                    
                    <Modal.Footer>
                    <Button variant="primary" onClick={()=>{ExporttoCloud()}}>EXPORT</Button>
                        
                        <Button variant="secondary" onClick={()=>{setshowdrivemodel(false)}}>CANCEL</Button>
                         
                    </Modal.Footer>
                </Modal>


                <Modal show={showImportDrivemodel}
                onHide={() => { setShowImportDrivemodel(false);setChildArr([]) }}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                scrollable={true}>
                <Modal.Header  >
                    <Modal.Title id="example-modal-sizes-title-lg">
                        <p> {Exporttitle}</p>
                    </Modal.Title>
                    {/* <button type="button" className="clobtn btn-secondary" style={{ backgroundColor: "#ffff" }} onClick={() => { setShowImportDrivemodel(false) }}>
                        <CloseIcon />
                    </button> */}
                    <CloseIcon className="close_add_doc_pop" onClick={()=>{setShowImportDrivemodel(false)}}/> 
                </Modal.Header>
                <Modal.Body className='error-modal-content'>
                    <div>
                          <ul className="list-group">
                            {Goodrivedata.length>0?Goodrivedata.map(item=>{
                                 return(
                                    item.IsFolder === true?
                                    <div>
                                        <li key={item.id} className="list-group-item" style={{backgroundColor:"#e2dbdb",cursor:"pointer", }} onClick={()=>Exporttitle==="Google Drive"?getchilddata(item):Exporttitle==="Drop Box"?getdropboxchilddata(item):Exporttitle==="One Drive"?getonedrivechilddata(item): ""}>
                                          <>
                                          <i className="fa fa-folder mr-1" style={{color:"#2F7FED"}}></i>
                                           <span>{item.roleName}</span>
                                           
                                           </> </li>
                                           {item.children.length > 0 && <ul className="list-group" style={{borderRadius: '0', display:"block", display: 'block', padding: '0.2rem', background: 'rgb(226 219 219)'}}>
                                           { item.children.map(item1=>{                                                
                                             return(
                                                 <>
                                                  
                                                 <li key={item1.id} className="list-group-item" style={{cursor:"default" }} >
                                                 <input type="checkbox" style={{marginRight:"30px", height:"15px",width:"15px"}}  onClick={(event)=>{checkBoxHandler(event.target.checked,item1)}}></input>
                                                     {item1.roleName}</li>
                                              </>
                                             )
                                           })}
                                           </ul>}
                                           </div>
                                           :
                                         <li key={item.id} className="list-group-item" >
                                            <input type="checkbox"  style={{marginRight:"30px", height:"15px",width:"15px"}}  onClick={(event)=>{checkBoxHandler(event.target.checked,item)}}></input>
                                            <span>{item.roleName}</span>
                                            </li>
                                             ) })
                        :"Drive is Empty"}
                        </ul>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => { setShowImportDrivemodel(false) }}>CANCEL</Button>
                    <Button variant="primary"onClick={insertHandler}>INSERT</Button>
                </Modal.Footer>
            </Modal>
        </div>

    )
}
export { Manage }; 