import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { addDocStep2Options, addDocStep3Options } from '../_services/model.service';
import { addDocumentService } from '../_services/adddocument.service';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useEffect, useState } from "react";
import { commonService } from '../_services/common.service';
import { Document, Page, pdfjs } from "react-pdf";
import config from 'config';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
import Draggable, { DraggableCore } from 'react-draggable';
import { Calendar } from 'primereact/calendar';
import * as moment from 'moment'
import Spinner from 'react-bootstrap/Spinner';
import '../css/controls.css'
import '../css/documentReview.css'
import Signature from './Controls/Signature';
import Initial from './Controls/Initial';
import DigitalSignature from './Controls/DigitalSignature';
import Attachment from './Controls/Attachment';
import CompanyAddress from './Controls/CompanyAddress';
import UserAddress from './Controls/UserAddress';
import Aadhaar from './Controls/Aadhaar';
import ImageSeal from './Controls/ImageSeal';
import DateSigned from './Controls/DateSigned';
import Email from './Controls/Email';
import Name from './Controls/Name';
import Company from './Controls/Company';
import Payment from "./Controls/Payment";
import Checkbox from './Controls/CheckBox';
import NotarySeal from './Controls/NotarySeal';
import TextBox from './Controls/TextBox';
import Dob from './Controls/Dob'
import { Button, Modal } from 'react-bootstrap';
import Pagination from "react-responsive-pagination";
import ReactTooltip from 'react-tooltip';
let pagesArray = []
let imageArray = []
let enableAutoApprove = false
function DocumentReview() {
    //Vertical Tabs
    const usehistory = useHistory();
    const [count, setCount] = React.useState(3);
    const [step4Data, setStep4Data] = useState({});
    const [subNumPages, setSubNumPages] = React.useState(1);
    const [documentFile, setdocument] = useState('');
    const [selectedPageNumber, setselectedPageNumber] = useState(1);
    const options2 = addDocStep3Options
    const options3 = addDocStep2Options
    const [previewMailObj, setPreviewMailObj] = useState({});
    const [submitEnable, setSubmitEnable] = useState(true);
    const [DraggableControls, setDraggableControls] = useState([]);
    const [submitPopShow, setSubmitPopShow] = useState(false);
    const [CurrentPdfPageNumber, setCurrentPdfPageNumber] = React.useState(1);
    const [SubmitDocNumPages, setSubmitDocNumPages] = React.useState(1);
    const [submitSuccessPopShow, setSubmitSuccessPopShow] = useState(false);
    const [documentLimitPopShow, setDocumentLimitPopShow] = useState(false);
    const [showAutoApprovePopup, setShowAutoApprovePopup] = useState(false)
    const [canvasImagePages, setCanvasImagePages] = useState(imageArray)
    const [autoFillDoc, setautoFillDoc] = useState(0)
    const [IsanyoneenableDsc,setIsanyoneenableDsc] = useState(false)
    let { id } = useParams();

    // get functions to build form with useForm() hook

    function getDocumentInfoData() {
        document.body.classList.add('loading-indicator');
        addDocumentService.getDocumentInfo(id).then(data => {
            pagesArray = []
            if (data.Entity && data.Entity.Data) {
                setStep4Data(data.Entity.Data)
                let dscenabled=''
                dscenabled = data.Entity.Data.Recipients.find(item => item.IsDSC == 1)
                if(dscenabled){
                    setIsanyoneenableDsc(true)
                 }
                setdocument(config.serverUrl + 'adocuments/download/' + data.Entity.Data.FileName)
                if (data.Entity.Data.Controls) {
                    var controls = Object.keys(data.Entity.Data.Controls);
                    controls.forEach((ctrlId) => {
                        const obj = data.Entity.Data.Controls[ctrlId];
                        obj.Top = obj.TopByPage
                        obj.Left = obj.LeftByPage
                        obj.CtrlId = ctrlId;
                        DraggableControls.push(obj)
                    })
                    setDraggableControls([...DraggableControls])

                }
                checkManualApprove(data.Entity.Data)
            }
        })
    }
    function checkManualApprove(data) {
        let intiatorFilter = []
        let CCFilter = []
        intiatorFilter = data.Recipients.filter(Item => Item.IsInPerson)
        let intiatorControlMiss = false
        for (let index = 0; index < DraggableControls.length; index++) {
            if (DraggableControls[index].IsOwner && DraggableControls[index].IsMandatory) {
                if (DraggableControls[index].Code == "signature" && !DraggableControls[index].SignatureUpload && !DraggableControls[index].SignatureFont) {
                    intiatorControlMiss = true
                } else if (DraggableControls[index].Code == "initial" && !DraggableControls[index].InitialUpload && !DraggableControls[index].SignatureFont) {
                    intiatorControlMiss = true
                } else if (DraggableControls[index].Code != "initial" && DraggableControls[index].Code != "signature" && !DraggableControls[index].Value) {
                    intiatorControlMiss = true
                }
            }
        }
        if(data.Recipients && data.Recipients.length == 2){
            CCFilter = data.Recipients.filter(Item => Item.IsCCEnabled)

        }
        console.log(data)
        console.log(data.IsParallelSigning)
        console.log(intiatorControlMiss, !intiatorControlMiss)
        console.log(data.Recipients)
        console.log(data.Recipients.length > 1)
        console.log(intiatorFilter.length == 0)
        console.log(CCFilter.length ==0)
        console.log(data && data.IsParallelSigning && !intiatorControlMiss && data.Recipients && data.Recipients.length > 1 && intiatorFilter.length == 0 && CCFilter.length ==0)
    if(data && data.IsParallelSigning && !intiatorControlMiss && data.Recipients && data.Recipients.length > 1 && intiatorFilter.length == 0 && CCFilter.length ==0){
        enableAutoApprove = true
    }
    }
    useEffect(() => {
        setdocument(commonService.documentValue)
        getDocumentInfoData()
    }, [])

    function getDocumentFile(fileName) {
        addDocumentService.getDocumentFile(fileName).then(data => {
        })
    }
    function onDocumentLoadSuccess(e) {
        selectedPageNumber ? setselectedPageNumber(selectedPageNumber) : setselectedPageNumber(1)
    }
    function subDocumentsLoadSuccess(e) {
        setSubNumPages(e.numPages)
        setSubmitDocNumPages(e.numPages)
    }
    function submitDocumentsLoadSuccess(e) {
        setSubmitDocNumPages(e.numPages)
    }
    function setOnMainDoc(pagenumber) {
        setselectedPageNumber(pagenumber)

    }
    const loadSpinner = () => {
        return <div style={{ display: "flex", alignContent: 'center', alignItems: "center", justifyContent: "center", marginTop: '30%' }}>
            <Spinner variant="primary" animation="border" /></div>
    }
    /****** To Go to Back dashboard Page  */
    function back() {
        usehistory.push('/account/documentprepare/' + step4Data.ADocumentId)
    }


    function getNext() {
        // usehistory.push('/account/documentreview/' + step2Data.ADocumentId)
    }
    //Share Document Hide Show
    function shareDocOpen() {
        OnSubmit()

        // var srcElement = document.getElementById('sharedoc');
        // if (srcElement != null) {
        //     if (srcElement.style.display == "block") {
        //         // srcElement.style.display = "none";
        //     } else {
        //         getPreviewPopupDetails ()

        //         // srcElement.style.display = "block";
        //     }
        //     return false;
        // }
    }
    function shareDocClose() {

        var srcElement = document.getElementById('sharedoc');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            }
            return false;
        }
    }
function OnSubmit(){
    if(enableAutoApprove){
        setShowAutoApprovePopup(true)
    } else {
        getPreviewPopupDetails()
    }
}

    function getPreviewPopupDetails() {
        document.body.classList.add('loading-indicator');
        addDocumentService.getDocumentPreviewDetaild(id).then(data => {
            data.Entity.Data.Title = step4Data.Title ? step4Data.Title : step4Data.OriginalFileName.split('.')[0]
            data.Entity.Data.Subject = data.Entity.Data.Subject
            data.Entity.Data.DocumentReminder = data.Entity.Data.DocumentReminder ? data.Entity.Data.DocumentReminder : 0
            data.Entity.Data.Body = step4Data.Recipients[0].IsOwner ? data.Entity.Data.SentBy + ' sent you a document titled ' + data.Entity.Data.Title + ' to review and signing electronically.' : ' sent you a document titled ' + data.Entity.Data.Title + ' to review and signing electronically.'
            data.Entity.Data.ExpirationDate = data.Entity.Data.ExpirationDate ? data.Entity.Data.ExpirationDate : ''
            setPreviewMailObj(data.Entity.Data)
            // var srcElement = document.getElementById('sharedoc');
            // srcElement.style.display = "block";
                setSubmitPopShow(true)
        })
    }
function AutoApproveSubmit() {
    setShowAutoApprovePopup(false)
    getPreviewPopupDetails()
}

    function GenerateOTP() {

        const digits = '0123456789';

        const otpLength = 4;

        let otp = '';

        for (let i = 1; i <= otpLength; i++) {

            const index = Math.floor(Math.random() * (digits.length));

            otp = otp + digits[index];

        }
        if (otp.length < 4) { GenerateOTP(); }
        return otp;

    }
    const reviewSubmitHandler = () => {
        finalSubmit()
    }
    function finalSubmit() {
        // console.log(previewMailObj, 'previewMailObj')
        if (submitEnable == true) {
            setSubmitEnable(false)
            let d = new Date();
            let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
            let timeSplit = time.split(' ');
            let timeZone = timeSplit[1];
            let submitData = {}
            var title = step4Data.Title ? step4Data.Title : step4Data.OriginalFileName.split('.')[0]
            submitData = {
                ADocumentId: step4Data.ADocumentId,
                AutoFill: autoFillDoc,
                Content: previewMailObj.Body,
                DocumentReminder: previewMailObj.DocumentReminder,
                Initial: [],
                IsParallelSigning: step4Data.IsParallelSigning,
                IsSecured: step4Data.IsSecured,
                DocumentOtp: step4Data.IsSecured ? GenerateOTP() : null,
                Signature: [],
                Subject: previewMailObj.Subject,
                Recipients: step4Data.Recipients,
                Title: title,
                IsReact: 1,
                ToRecipients: Array.prototype.map
                    .call(step4Data.Recipients, (item) => item.EmailAddress)
                    .join(','),
                DateTime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                TimeZone: timeZone,
            };
            previewMailObj.ExpirationDate ? submitData.ExpirationDate = moment(previewMailObj.ExpirationDate).format('YYYY-MM-DD') : null;
            addDocumentService.finalSubmit(step4Data.ADocumentId, submitData).then(data => {
                if(!data.Entity.DocumentLimitReached) {
                shareDocClose()
                setSubmitPopShow(false)
                setSubmitSuccessPopShow(true)
                } else {
               setSubmitPopShow(false)
               setDocumentLimitPopShow(true)
            }
                setSubmitEnable(true)
            }, error => {
                setSubmitEnable(true)
            })
        }
    }
    function successfulOkHandler() {
        setSubmitSuccessPopShow(false)
        usehistory.push('/account/manage')

    }
    function getOnRenderSuccess(e, pageIndex, page) {
        pagesArray.push(page)
        if(subNumPages == pagesArray.length){
            DocumentControlsSetUp()
            loadPagesData()
        }
       }
       function loadPagesData(){
       let canvasPagesArray = $('body').find('canvas')
        if (canvasPagesArray && canvasPagesArray.length > 0) {
            for (let idx = 0; idx <= canvasPagesArray.length; idx++) {
                const DocPageDom = canvasPagesArray[idx].toDataURL('image/jpeg',0.95)
                if(DocPageDom){
                    imageArray.push(DocPageDom)
                }
            }
            setCanvasImagePages([...imageArray])
        }
       }
    function DocumentControlsSetUp() {
        for (let index = 0; index < DraggableControls.length; index++) {
            DraggableControls[index].Left = DraggableControls[index].LeftByPage - 10
            let topPosition = 0
            if (DraggableControls[index].PageNumber == 1) {
                DraggableControls[index].Top = DraggableControls[index].TopByPage
            } else {
                for (var idx = 0; idx < DraggableControls[index].PageNumber; idx++) {
                if ((DraggableControls[index].PageNumber - 1) == idx) {
                  topPosition = (topPosition + DraggableControls[index].TopByPage)
                  DraggableControls[index].Top = topPosition 
                } else {
                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[idx].firstChild
                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                }
              }
            }
        }
        setDraggableControls([...DraggableControls])
    } 

    return (
        <>
            <div className="dashboard_home wrapper_other" id="wrapper_other">
                <Sidebar />

                <div className="main other_main" id="main">
                    <Topbar />

                    <div className="main-content wizard">
                        <div className='row'>
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div id="wizard_step">
                                    <div className="step-wizard" role="navigation">
                                        <ul>
                                            <li className={count === 0 || count < 4 ? 'active done' : ''}>
                                                <button>
                                                    <div className="step">1</div>
                                                    <div className="title">Add</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 1 || (count < 4 && count != 0) ? 'active done' : ''}>
                                                <button id="step2">
                                                    <div className="step">2</div>
                                                    <div className="title">Select</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 2 || (count < 4 && count != 1 && count != 0) ? 'active done' : ''}>
                                                <button id="step3">
                                                    <div className="step">3</div>
                                                    <div className="title">Prepare</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 3 ? 'active done' : ''}>
                                                <button id="step4">
                                                    <div className="step">4</div>
                                                    <div className="title">Review</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className='wizard-panel' id="wizard-pane-4">
                                        <div className='content'>
                                            <div className='prepare_document document_other'>
                                                <div className='row'>
                                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                        <h2 className='document-select-overflowdots'>Document Title: <span data-tip data-for='title'>{step4Data?.Title}</span></h2>
                                                        <ReactTooltip id="title" place="top" effect="solid">
                                                           {step4Data?.Title}</ReactTooltip>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-xl-9 col-lg-7 col-md-7 col-sm-12'>
                                                        <div className='prepare-slide_pln overflow-documents' style={{overflowX:'hidden'}}>
                                                            <Document className="row box"
                                                                file={documentFile}
                                                                loading={loadSpinner}
                                                                noData={''}
                                                                onLoadSuccess={subDocumentsLoadSuccess}>
                                                                {DraggableControls.map(control => {
                                                                    return (
                                                                        <>
                                                                            {(control.Code == "signature") ? <div key={control.code + control?.CtrlId}><Signature IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "date") ? <div key={control.code + control?.CtrlId}><DateSigned IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "payment") ? <div key={control.code + control?.CtrlId}><Payment IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "seal") ? <div key={control.code + control?.CtrlId}><NotarySeal IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "text") ? <div key={control.code + control?.CtrlId}><TextBox IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "email") ? <div key={control.code + control?.CtrlId}><Email IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "username") ? <div key={control.code + control?.CtrlId}><Name IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "companyname") ? <div key={control.code + control?.CtrlId}><Company IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "image") ? <div key={control.code + control?.CtrlId}><ImageSeal IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "aadhaar") ? <div key={control.code + control?.CtrlId}><Aadhaar IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "digitalsignature") ? <div key={control.code + control?.CtrlId}><DigitalSignature IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == 'dob') ? <div key={control.code + control?.CtrlId}><Dob IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "attachment") ? <div key={control.code + control?.CtrlId}><Attachment IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "companyaddress") ? <div key={control.code + control?.CtrlId}><CompanyAddress IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "useraddress") ? <div key={control.code + control?.CtrlId}><UserAddress IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "initial") ? <div key={control.code + control?.CtrlId}><Initial IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                            {(control.Code == "checkbox") ? <div key={control.code + control?.CtrlId}><Checkbox IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} /></div> : null}
                                                                        </>
                                                                    )
                                                                })}
                                                                {subNumPages ? <>
                                                                    {Array.apply(null, Array(subNumPages))
                                                                        .map((x, i) => i + 1).sort((a, b) => a - b)
                                                                        .map((page, index) => {
                                                                            return <Page key={page+'canvaspage'} scale={IsanyoneenableDsc === true? 1:1.5} className="document_prepare" onRenderSuccess={(e) => {getOnRenderSuccess(e,index, page)}} noData={''} pageNumber={page} />
                                                                        })}
                                                                </>
                                                                    : null}
                                                            </Document>
                                                             </div>
                                                    </div>
                                                    <div className='col-xl-3 col-lg-5 col-md-5 col-sm-12'>
                                                        <div className='prepare-be-document'>
                                                            <h2>Recipients</h2>
                                                            <ul className='documet_review_recipients_ul'>
                                                                {(step4Data && step4Data.Recipients) ? [...step4Data?.Recipients, ...step4Data?.CCRecipients].map((obj, index) => {
                                                                    return <li key={obj?.FullName}>
                                                                       <div> <span>{obj.Position ? obj.Position : index + 1}</span> </div>
                                                                        <div className="text_artle" >
                                                                            {/* <div style={{display: 'flex'}}>  */}
                                                                             <h6 style={{ display:"flex"}}>
                                                                                <div data-tip data-for={"tool"+obj.RecipientId} style={{overflow:"hidden",textOverflow:"ellipsis",whiteSpace:"nowrap",width:"105px"}}>{obj?.FullName}</div>
                                                                                {obj.RecipientRole == "Signatory" ? <img className={'recipient_image_zoom'} src={'src/images/signulu_small_black_logo.svg'}
                                                                                onError={(e) => {
                                                                                    e.target.src = 'src/images/signulu_small_black_logo.svg'
                                                                                }} alt="" /> : null }
                                                                                {obj.RecipientRole == "Reviewer" ? <img className={'recipient_image_zoom'} src={'src/images/reviewericon.svg'}
                                                                                onError={(e) => {
                                                                                    e.target.src = 'src/images/reviewericon.svg'
                                                                                }} alt="" /> : null }
                                                                                {obj.RecipientRole == "Approver" ? <img className={'recipient_image_zoom'} src={'src/images/reviewericon.svg'}
                                                                                onError={(e) => {
                                                                                    e.target.src = 'src/images/reviewericon.svg'
                                                                                }} alt="" /> : null }
                                                                                {obj.RecipientRole == "CC" ? <img  src={'src/images/cc_recipient_icon.svg'} className={'cc_image_zoom'}
                                                                                onError={(e) => {
                                                                                    e.target.src = 'src/images/reviewericon.svg'
                                                                                }} alt="" /> : null }
                                                                            {/* </div> */}
                                                                            </h6>
                                                                            <p>{obj?.EmailAddress}</p>
                                                                        </div>
                                                                        <ReactTooltip id={"tool"+obj.RecipientId} place="top" effect="solid">
                                                                        {obj?.FullName}
                                                                           </ReactTooltip>
                                                                    </li>
                                                                }) : null}
                                                            </ul>
                                                        </div>
                                                        <button type="submit" onClick={IsanyoneenableDsc == true ?getPreviewPopupDetails:shareDocOpen} className="subBtn">Submit
                                                        {/* <button type="submit" onClick={()=>{IsanyoneenableDsc == true ?getPreviewPopupDetails():shareDocOpen}} className="subBtn">Submit */}
                                                            <div className="icon">
                                                                <span><img src="src/images/right_arrow.svg" /></span>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                        <div className="slide-tab-btn">
                                                            <button className="btn prev-btn" onClick={() => { back() }}>BACK</button>
                                                            {/* <button className="btn cncl-btn">cancel</button> */}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={submitPopShow} onHide={() => { setSubmitPopShow(false) }}
                size="lg"
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                        Submit Document
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: '350px', overflow: "hidden auto" }} >
                    <div>
                        <div id="sharedoc" className='prepare_document submit_popup share_document' >
                            <div className='d-flex'>
                                <div style={{width: '40%', height: '50%', minWidth: '250px'}}>
                                    <div style={{width: '70%', height: '50%', backgroundColor: 'darkgray'}}>
                                        {canvasImagePages && canvasImagePages.length>0 ?<img src={canvasImagePages[CurrentPdfPageNumber-1]} style={{border: '1px solid #bebabeee', width: '200px' , height: '250px'}}></img>
                                         : <Document className="box submit_popdcodument"
                                         file={documentFile}
                                         loading={loadSpinner}
                                         noData={''}
                                         onLoadSuccess={submitDocumentsLoadSuccess}>
                                         <Page key={CurrentPdfPageNumber} scale={1.5} className="submit_popdcodument_page" noData={''} pageNumber={CurrentPdfPageNumber} />
                                     </Document>}
                                    </div>
                                    {
                                        <div className="submit_document-nagin">
                                            <nav aria-label="Page navigation">
                                                <Pagination
                                                    current={CurrentPdfPageNumber}
                                                    total={SubmitDocNumPages}
                                                    onPageChange={(newPage) =>
                                                        setCurrentPdfPageNumber(newPage)
                                                    }
                                                    maxWidth={'80px'}
                                                />
                                            </nav>
                                        </div>

                                    }
                                </div>
                                <div className="slide_text_artcl" style={{marginBottom:'1rem'}}>
                                    <div className="wap">
                                        <h6 className='text-break'>Name:  {step4Data.Title ? step4Data.Title : step4Data.OriginalFileName ? step4Data.OriginalFileName.split('.')[0] : ''}</h6>
                                        <h5>Recipients : {step4Data.Recipients ? step4Data.Recipients.length : 0}</h5>
                                        <p>No of Pages: {subNumPages}</p>
                                    </div>
                                </div>
                            </div>
                            {/* </div> */}
                            <div className='row'>
                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                    <div className='share_document_form'>
                                        <form>
                                            <div className='row'>
                                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                    <div className='form-group'>
                                                        <label>Subject</label>
                                                        <input type="text" value={(previewMailObj?.Subject) ? previewMailObj?.Subject : ''}  maxLength="100" style={{marginBottom:'1rem'}}
                                                            onChange={(event) => { previewMailObj.Subject = event.target.value, setPreviewMailObj({ ...previewMailObj })} }></input>
                                                        {/* <textarea value={(previewMailObj?.Subject) ? previewMailObj?.Subject : ''}
                                                            onChange={(event) => { previewMailObj.Subject = event.target.value, setPreviewMailObj({ ...previewMailObj }) }} /> */}
                                                        {/* <input type="text" value={(previewMailObj?.Subject) ? previewMailObj?.Subject : ''}
                                                            onChange={(event) => { previewMailObj.Subject = event.target.value, setPreviewMailObj({ ...previewMailObj }) }} /> */}
                                                    </div>
                                                </div>
                                                <div className='col-xl-6 col-lg-6 col-md-6 col-sm-6'>
                                                    <div className='form-group'>
                                                        <label>Reminder Frequency</label>
                                                        <select style={{marginBottom:'1rem'}} className='hlfinput' value={previewMailObj?.DocumentReminder} onChange={(event) => {
                                                            previewMailObj.DocumentReminder = event.target.value;
                                                            setPreviewMailObj({ ...previewMailObj })
                                                        }}>
                                                            <option value="0">None</option>
                                                            <option value="1">Daily</option>
                                                            <option value="7">Weekly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            {/* </div>
                                            <div className='row'> */}
                                                <div className='col-xl-6 col-lg-6 col-md-6 col-sm-6'>
                                                    <div className='form-group'>
                                                        <label>Document validity date</label>
                                                        <Calendar id="icon"
                                                            minDate={new Date()} 
                                                            readOnlyInput 
                                                            value={(previewMailObj && previewMailObj.ExpirationDate) ? previewMailObj.ExpirationDate : ''}
                                                            onChange={(e) => { previewMailObj.ExpirationDate = e.target.value, setPreviewMailObj({ ...previewMailObj }) }} dateFormat="mm/dd/yy" showIcon />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='row'>
                                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                    <div className='form-group'>
                                                        <label>Body of the mail</label>
                                                        <textarea maxLength="300" value={(previewMailObj?.Body) ? previewMailObj?.Body : ''} style={{marginBottom:'1rem'}}
                                                            onChange={(event) => { previewMailObj.Body = event.target.value, setPreviewMailObj({ ...previewMailObj }) }} />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{ justifyContent: 'space-between'}}>
                    <Button variant="secondary" style={{width: '7rem'}} onClick={() => { setSubmitPopShow(false) }}
                        className='submit_cancel_btn'>CANCEL</Button>
                    {/* <p>I understand this is legal representation of my initials</p> */}
                    <Button style={{width: '7rem'}} onClick={() => { reviewSubmitHandler() }} className='submit_send_btn'
                    >SEND</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={submitSuccessPopShow}  onHide={() => { setSubmitSuccessPopShow(false) }}
                size=""
                backdrop="static"
                keyboard={false}
                dialogClassName=""
                aria-labelledby="contained-modal-title-vcente"
                centered>
                <Modal.Header style={{justifyContent:"center"}}>
                    <Modal.Title id="contained-modal-title-vcente">
                        <div class="success-animation">
                            <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" /></svg>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                      <div>
                      <p style={{lineHeight: '1.5'}}>Document  <b>{step4Data.Title ? step4Data.Title : step4Data.OriginalFileName ? step4Data.OriginalFileName.split('.')[0] : ''}</b> is successfully sent for approvals/signatures.</p>
                      </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center'>
                    {/* <Button variant="primary" onClick={() => { setDocmodalShow(false) }}>Submit</Button> */}
                    {/* <p>I understand this is legal representation of my initials</p> */}
                    <Button variant="secondary" style={{background: '#F06B30', border: 'none'}} onClick={() => {successfulOkHandler() }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={documentLimitPopShow} onHide={() => { setDocumentLimitPopShow(false) }}
                size=""
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton style={{ }}>
                    <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                        Document Limit Reached
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card container">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div" style={{marginBottom:'10px'}}>
                                        <p style={{ fontSize: '16px',lineHeight: '1.5'}}>Need to add more documents? Upgrade to add more documents and access adavanced features.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}>
                    <Button variant="secondary" style={{}} onClick={() => { }}
                        className='submit_cancel_btn'>Upgrade</Button>
                    <Button style={{ }} onClick={() => { setDocumentLimitPopShow(false)}} className='submit_send_btn'
                    >No Thanks</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showAutoApprovePopup} onHide={() => { setShowAutoApprovePopup(false) }} 
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    Approvals
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{marginTop: '10px'}}>
                <input type="radio" id="0" name="approval" value={0} defaultChecked  onChange={(e) => {setautoFillDoc(e.target.value) }}/>  <b style={{fontSize:'19px',marginLeft: '14px'}}>Manually review and approve</b>
                <p style={{fontSize:'14px', marginLeft: '32px',lineHeight: '20px'}}>   After sending the document,you need to open the document and approve
                    it,if you are an authorized signatory.</p><br></br> 
                <input type="radio" id="1" name="approval" value={1} onChange={(e) => {setautoFillDoc(e.target.value)}}/>  <b style={{fontSize:'19px',marginLeft: '14px'}}>Approve instantly when all recipients approve</b>
                <p style={{fontSize:'14px', marginLeft: '32px',lineHeight: '20px'}}>   Once all the recipients review and approve the
                  document,the same will be approved automatically,by the
                  system,using your registered signature pattern.</p> 
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    {/* <Button variant="secondary" onClick={() => { setModalShow(false), setDeleteRecipient({}) }}>Cancel</Button> */}
                    <Button className="" style={{letterSpacing: '0.12em',backgroundColor: '#F06B30', borderColor: '#f8f8f8!important', height: '41px', border:'none'}} onClick={() => { AutoApproveSubmit() }}>SUBMIT</Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}

export { DocumentReview }; 