import React, {useEffect , useState} from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import {Range} from "react-range";
import {usersService} from "../_services/users.service";
import { addDocumentService } from '../_services/adddocument.service';
function AccountProfile({ history, location }) {
    const [values, setValues] = useState(10)
    const [resultsUser,setResultsUser]=useState()
    const [defaultResultsUsersendingnotifications,setdefaultResultsUsersendingnotifications]=useState(false)
    const [defaultResultsUserRecipientnotifications,setdefaultResultsUserRecipientnotifications]=useState(false)
    const [resultsList,setResultsList]=useState()
    const [fullResults,setFullResults]=useState()
    const [count,setcount]=useState(0)
    const [defaultdatechecked,setdefaultdatechecked]=useState(false)
    const [SIGNATURE_SETTINsdefaultselect,setSIGNATURE_SETTINsdefaultselect]=useState(false)
    const [companyPack,setcompanyPack]=useState("")
    const [savedata,setsavedata]=useState(false)
    const [sendNotificationArr,setsendNotificationArr]=useState([
        {
            No:1,
            Name:"SEND_NOTIFY_SELECT_ALL",
            Text:"Select All",
            Value:true
        },
        {
            No:2,
            Name:"SEND_ENVELOPE_APPROVED",
            Text:"When the document is completed",
            Value:true
        },
        {
            No:3,
            Name:"SEND_ENVELOPE_REJECT",
            Text:"A signer declines to sign",
            Value:true
        },
        {
            No:4,
            Name:"SEND_ENVELOPE_DELETE",
            Text:"Documents will be purged from the system",
            Value:true
        }
    ])
    const [reviceNotificationArr,setreviceNotificationArr]=useState([
        {
            No:1,
            Name:"RECIPIENT_NOTIFY_SELECT_ALL",
            Text:"Select All",
            Value:true
        },
        {
            No:2,
            Name:"RECIPIENT_ENVELOPE_SENT",
            Text:"I have a document to sign",
            Value:true
        },
        {
            No:3,
            Name:"RECIPIENT_ENVELOPE_APPROVED",
            Text:"When the document is completed",
            Value:true
        },
        {
            No:4,
            Name:"RECIPIENT_ENVELOPE_REJECT",
            Text:"Another signer declines to sign",
            Value:true
        },
        {
            No:5,
            Name:"RECIPIENT_ENVELOPE_CORRECT",
            Text:"The sender corrects a document",
            Value:true
        },
        {
            No:6,
            Name:"RECIPIENT_ENVELOPE_DELETE",
            Text:"Documents will be purged from the system",
            Value:true
        }
    ])





    //Tabs
    function openTab(event, divID) {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
        setcount(0)
    }

    useEffect(() => {
        // getdSettingUser()
        getdSettingList()
        getdCompanyPack()
    }, []);
    useEffect(() => {
        // getdSettingUser()
        getdSettingList()
        getdCompanyPack()
    }, [savedata]);

    useEffect(()=>{
//    
        setValues(values)
    },[values])



    function getdSettingList(){
        usersService.getSettingList()
            .then((result) => {
                setResultsList(result.Entity)
                //console.log('list praveen',result)
                getdSettingUser(result.Entity)
            })
            .catch(error => {

            });
    }
    function getdCompanyPack(){
        usersService.getCompanyPackageCode()
            .then((result) => {
                setcompanyPack(result.Entity.PackageCode)
            })
            .catch(error => {

            });
    }

    function getdSettingUser(result1){
        //  
        usersService.getSettingUser()
            .then((result) => {
                setResultsUser(result.Entity)
                if(result.Entity.SENDING_NOTIFICATIONS === undefined){
                setdefaultResultsUsersendingnotifications(true)
                }
                if(result.Entity.RECIPIENT_NOTIFICATIONS === undefined){
                    setdefaultResultsUserRecipientnotifications(true)
                }
                console.log('user',result)
                compareListAndUser(result1,result.Entity)

            })
            .catch(error => {

            });
    }

    function compareListAndUser(result1,result2){
        let temp_list=result1
        let temp_user=result2

        let tempfontsize=result2.SIGNATURE_FONT ? result2.SIGNATURE_FONT[0].IntValue : 10
        setValues(tempfontsize)
        temp_list=temp_user
        setFullResults({...temp_list,...temp_user})
        setcount(0)
        setcount(0)
        setcount(0)
       
        if (result2.SIGNATURE_FONT === undefined){
            let SIGNATURE_FONTduplicte=result1.SIGNATURE_FONT
            SIGNATURE_FONTduplicte[0].IntValue=10
            let tepm_fullresult=temp_list
               
            tepm_fullresult["SIGNATURE_FONT"]=SIGNATURE_FONTduplicte
            setFullResults(tepm_fullresult)
        }
        else {
            let SIGNATURE_FONTduplicte=result2.SIGNATURE_FONT

            let tepm_fullresult=temp_list
               
            tepm_fullresult["SIGNATURE_FONT"]=SIGNATURE_FONTduplicte
            setFullResults(tepm_fullresult)
        }


        if (result2.WHATSAPP === undefined){
            if(result1.WHATSAPP !== undefined){
            let whatsduplicte=result1.WHATSAPP
            whatsduplicte.BitValue = false
            let tepm_fullresult=temp_list
               
            tepm_fullresult["WHATSAPP"]=whatsduplicte
            setFullResults(tepm_fullresult)}
        }
        else {
            let whatsduplicte=result2.WHATSAPP

            let tepm_fullresult=temp_list
               
            tepm_fullresult["WHATSAPP"]=whatsduplicte
            setFullResults(tepm_fullresult)
        }

        if (result2.SENDER_SEQ_POSITION === undefined){
            let SENDER_SEQ_POSITIONduplicte=result1.SENDER_SEQ_POSITION
            let tepm_fullresult=temp_list
               
            tepm_fullresult["SENDER_SEQ_POSITION"]=SENDER_SEQ_POSITIONduplicte
            setFullResults(tepm_fullresult)
        }
        else {
            let SENDER_SEQ_POSITIONduplicte=result2.SENDER_SEQ_POSITION
            let tepm_fullresult=temp_list
               
            tepm_fullresult["SENDER_SEQ_POSITION"]=SENDER_SEQ_POSITIONduplicte
            setFullResults(tepm_fullresult)
        }
        if (result2.GENERAL_PREFERENCES === undefined){
            let GENERAL_PREFERENCESduplicte=result1.GENERAL_PREFERENCES
            GENERAL_PREFERENCESduplicte[0].BitValue = true

            let tepm_fullresult=temp_list
               
            tepm_fullresult["GENERAL_PREFERENCES"]=GENERAL_PREFERENCESduplicte
            setFullResults(tepm_fullresult)
        }
        else {
            let GENERAL_PREFERENCESduplicte=result2.GENERAL_PREFERENCES
            let tepm_fullresult=temp_list
               
            tepm_fullresult["GENERAL_PREFERENCES"]=GENERAL_PREFERENCESduplicte
            setFullResults(tepm_fullresult)
        }

        if(result2.DATE_FORMAT === undefined){
          //   
            let DATE_FORMATduplicte=result1.DATE_FORMAT
            DATE_FORMATduplicte[1].BitValue = false
            DATE_FORMATduplicte[0].BitValue = true
            let tepm_fullresult=temp_list
               
            tepm_fullresult["DATE_FORMAT"]=DATE_FORMATduplicte
            setFullResults(tepm_fullresult)
            setdefaultdatechecked(true)
          
        }else{
             
            if(result2.DATE_FORMAT.length<2){
                let DATE_FORMATduplicte=result1.DATE_FORMAT
                DATE_FORMATduplicte[0]=result2.DATE_FORMAT[0]
                let tepm_fullresult=temp_list
                   
                tepm_fullresult["DATE_FORMAT"]=DATE_FORMATduplicte
                setFullResults(tepm_fullresult)
            }
            else{
            let DATE_FORMATduplicte=result2.DATE_FORMAT
            let tepm_fullresult=temp_list
               
            tepm_fullresult["DATE_FORMAT"]=DATE_FORMATduplicte
            setFullResults(tepm_fullresult)}
        }
        if(result2.SIGNATURE_SETTINGS === undefined){
            // 
            let SIGNATURE_SETTINGSduplicte=result1.SIGNATURE_SETTINGS
            SIGNATURE_SETTINGSduplicte[0].BitValue = true
            SIGNATURE_SETTINGSduplicte[1].BitValue = false
            SIGNATURE_SETTINGSduplicte[2].BitValue = false
            SIGNATURE_SETTINGSduplicte[3].BitValue = false
            SIGNATURE_SETTINGSduplicte[4].BitValue = false
            SIGNATURE_SETTINGSduplicte[5].BitValue = false
            let tepm_fullresult=temp_list
            tepm_fullresult["SIGNATURE_SETTINGS"]=SIGNATURE_SETTINGSduplicte
            setFullResults(tepm_fullresult)
            setSIGNATURE_SETTINsdefaultselect(true)
            //handleChanger("SIGNATURE_SETTINGS",0)
          
        }else{
                let SIGNATURE_SETTINGSduplicte=result2.SIGNATURE_SETTINGS
                let tepm_fullresult=temp_list
                tepm_fullresult["SIGNATURE_SETTINGS"]=SIGNATURE_SETTINGSduplicte
                setFullResults(tepm_fullresult)
            }
        //if(result2.SENDING_NOTIFICATIONS === undefined)
        if(result2.SENDING_NOTIFICATIONS !== undefined){
        for (let y=0 ; y< sendNotificationArr.length;y++){

            for (let x=0;x < result2.SENDING_NOTIFICATIONS.length;x++){
                if(sendNotificationArr[y].Name===result2.SENDING_NOTIFICATIONS[x].Code){

                    let temp=sendNotificationArr
                    temp[y].Value=(result2.SENDING_NOTIFICATIONS[x].BitValue == 0 || result2.SENDING_NOTIFICATIONS[x].BitValue == false)?false:true
                    setsendNotificationArr(temp)
                }
            }
        }}
        setcount(count+1)
        setcount(count+1)
        //if(result2.RECIPIENT_NOTIFICATIONS === undefined)
        if(result2.RECIPIENT_NOTIFICATIONS !== undefined){
        for (let y=0 ; y< reviceNotificationArr.length;y++){
            
            for (let x=0;x < result2.RECIPIENT_NOTIFICATIONS.length;x++){
                if(reviceNotificationArr[y].Name===result2.RECIPIENT_NOTIFICATIONS[x].Code){
                    let temp=reviceNotificationArr
                    temp[y].Value=(result2.RECIPIENT_NOTIFICATIONS[x].BitValue == 0 || result2.RECIPIENT_NOTIFICATIONS[x].BitValue == false)?false:true
                    setreviceNotificationArr(temp)
                    setcount(count+1)
                }
            }
        }}
// setTimeout(()=>setcount(count+1),2000)

        setcount(count+1)


    }

    const handleChanger=(field,e)=>{
       //  
        let tempp=fullResults
        console.log(e)
        switch (field){
            case "GENERALPREFERENCES":
                   
                console.log("false",!e)
                tempp.GENERAL_PREFERENCES[0].BitValue=(!e)
                setFullResults(tempp)
                setcount(count+1)
                break;
            case "WHATSAPP":
                   
                console.log("false 111111",!e)
                tempp.WHATSAPP[0].BitValue=(!e)
                setFullResults(tempp)
                setcount(count+1)
                break;
            case "SENDER_SEQ_POSITION":
                   
                console.log("false",!e)
                tempp.SENDER_SEQ_POSITION[0].BitValue=(!e)
                setFullResults(tempp)
                setcount(count+1)
                break;
            case "DATE_FORMAT":
                setdefaultdatechecked(false)
                // let templist=temp.DATE_FORMAT
                 
                if(tempp.DATE_FORMAT.length>1){
                    if (e===0){
                        tempp.DATE_FORMAT[1].BitValue=false;
                        tempp.DATE_FORMAT[0].BitValue=true;

                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    }else if (e===1){
                        tempp.DATE_FORMAT[0].BitValue=false;
                        tempp.DATE_FORMAT[1].BitValue=true;

                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    }
                }else {
                    if (e===0){
                        tempp.DATE_FORMAT[0].BitValue=true;
                        tempp.DATE_FORMAT[0].TextValue="MM-DD-YYYY";
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    }else {
                        tempp.DATE_FORMAT[1].BitValue=true;
                        tempp.DATE_FORMAT[1].TextValue="DD-MM-YYYY";
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    }
                }
            case "SIGNATURE_SETTINGS":
                 
                // let tempSignSet=temp.SIGNATURE_SETTINGS
                setSIGNATURE_SETTINsdefaultselect(false)
                switch (e){
                    case 0:
                        tempp.SIGNATURE_SETTINGS[1].BitValue=false
                        tempp.SIGNATURE_SETTINGS[2].BitValue=false
                        tempp.SIGNATURE_SETTINGS[3].BitValue=false
                        tempp.SIGNATURE_SETTINGS[4].BitValue=false
                        tempp.SIGNATURE_SETTINGS[5].BitValue=false
                        tempp.SIGNATURE_SETTINGS[0].BitValue=true
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    case 1:
                        tempp.SIGNATURE_SETTINGS[0].BitValue=false
                        tempp.SIGNATURE_SETTINGS[2].BitValue=false
                        tempp.SIGNATURE_SETTINGS[3].BitValue=false
                        tempp.SIGNATURE_SETTINGS[4].BitValue=false
                        tempp.SIGNATURE_SETTINGS[5].BitValue=false
                        tempp.SIGNATURE_SETTINGS[1].BitValue=true
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    case 2:
                        tempp.SIGNATURE_SETTINGS[1].BitValue=false
                        tempp.SIGNATURE_SETTINGS[0].BitValue=false
                        tempp.SIGNATURE_SETTINGS[3].BitValue=false
                        tempp.SIGNATURE_SETTINGS[4].BitValue=false
                        tempp.SIGNATURE_SETTINGS[5].BitValue=false
                        tempp.SIGNATURE_SETTINGS[2].BitValue=true
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    case 3:
                        tempp.SIGNATURE_SETTINGS[1].BitValue=false
                        tempp.SIGNATURE_SETTINGS[2].BitValue=false
                        tempp.SIGNATURE_SETTINGS[0].BitValue=false
                        tempp.SIGNATURE_SETTINGS[4].BitValue=false
                        tempp.SIGNATURE_SETTINGS[5].BitValue=false
                        tempp.SIGNATURE_SETTINGS[3].BitValue=true
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    case 4:
                        tempp.SIGNATURE_SETTINGS[1].BitValue=false
                        tempp.SIGNATURE_SETTINGS[2].BitValue=false
                        tempp.SIGNATURE_SETTINGS[3].BitValue=false
                        tempp.SIGNATURE_SETTINGS[0].BitValue=false
                        tempp.SIGNATURE_SETTINGS[5].BitValue=false
                        tempp.SIGNATURE_SETTINGS[4].BitValue=true
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                    case 5:
                        tempp.SIGNATURE_SETTINGS[1].BitValue=false
                        tempp.SIGNATURE_SETTINGS[2].BitValue=false
                        tempp.SIGNATURE_SETTINGS[3].BitValue=false
                        tempp.SIGNATURE_SETTINGS[4].BitValue=false
                        tempp.SIGNATURE_SETTINGS[0].BitValue=false
                        tempp.SIGNATURE_SETTINGS[5].BitValue=true
                        setFullResults(tempp)
                        setcount(count+1)
                        break;
                }
                break
            case "SENDING_NOTIFICATIONS":
              //   
                let temp=sendNotificationArr
                
                   
                if (e===1){
                    temp[0].Value=(!(temp[e-1].Value))
                    temp[1].Value=((temp[e-1].Value))
                    temp[2].Value=((temp[e-1].Value))
                    temp[3].Value=((temp[e-1].Value))
                    setsendNotificationArr(temp)
                    setcount(count + 1)
                }
                else if (e===2){
                    temp[e-1].Value=(!(temp[e-1].Value))
                    setsendNotificationArr(temp)
                    if ( temp[1].Value && temp[2].Value && temp[3].Value ){
                        temp[0].Value=true
                        setsendNotificationArr(temp)
                    }else {
                        temp[0].Value=false
                        setsendNotificationArr(temp)
                    }
                    setcount(count + 1)

                }
                else if (e===3){
                    temp[e-1].Value=(!(temp[e-1].Value))
                    setsendNotificationArr(temp)
                    if ( temp[1].Value && temp[2].Value && temp[3].Value ){
                        temp[0].Value=true
                        setsendNotificationArr(temp)
                    }else {
                        temp[0].Value=false
                        setsendNotificationArr(temp)
                    }
                    setcount(count + 1)
                }
                else if (e===4){
                    temp[e-1].Value=(!(temp[e-1].Value))
                    setsendNotificationArr(temp)
                    if ( temp[1].Value && temp[2].Value && temp[3].Value ){
                        temp[0].Value=true
                        setsendNotificationArr(temp)
                    }else {
                        temp[0].Value=false
                        setsendNotificationArr(temp)
                    }
                    setcount(count + 1)
                }
                break
            case "RECIPIENT_NOTIFICATIONS":
                let temp1=reviceNotificationArr
                if (e===1){
                    temp1[0].Value=(!(temp1[e-1].Value))
                    temp1[1].Value=((temp1[e-1].Value))
                    temp1[2].Value=((temp1[e-1].Value))
                    temp1[3].Value=((temp1[e-1].Value))
                    temp1[4].Value=((temp1[e-1].Value))
                    temp1[5].Value=((temp1[e-1].Value))
                    setreviceNotificationArr(temp1)
                    setcount(count + 1)
                }
                else if (e===2){
                    temp1[e-1].Value=(!(temp1[e-1].Value))
                    setreviceNotificationArr(temp1)
                    if ( temp1[1].Value && temp1[2].Value && temp1[3].Value && temp1[4].Value && temp1[5].Value ){
                        temp1[0].Value=true
                        setreviceNotificationArr(temp1)
                    }else {
                        temp1[0].Value=false
                        setreviceNotificationArr(temp1)
                    }
                    setcount(count + 1)
                }
                else if (e===3){
                    temp1[e-1].Value=(!(temp1[e-1].Value))
                    setreviceNotificationArr(temp1)
                    if ( temp1[1].Value && temp1[2].Value && temp1[3].Value && temp1[4].Value && temp1[5].Value ){
                        temp1[0].Value=true
                        setreviceNotificationArr(temp1)
                    }else {
                        temp1[0].Value=false
                        setreviceNotificationArr(temp1)
                    }
                    setcount(count + 1)
                }
                else if (e===4){
                    temp1[e-1].Value=(!(temp1[e-1].Value))
                    setreviceNotificationArr(temp1)
                    if ( temp1[1].Value && temp1[2].Value && temp1[3].Value && temp1[4].Value && temp1[5].Value ) {
                        temp1[0].Value = true
                        setreviceNotificationArr(temp1)
                    }
                    else {
                            temp1[0].Value=false
                            setreviceNotificationArr(temp1)
                        }
                    setcount(count + 1)
                }else if(e===5){
                    temp1[e-1].Value=(!(temp1[e-1].Value))
                    setreviceNotificationArr(temp1)
                    if ( temp1[1].Value && temp1[2].Value && temp1[3].Value && temp1[4].Value && temp1[5].Value ){
                        temp1[0].Value=true
                        setreviceNotificationArr(temp1)
                    }else {
                        temp1[0].Value=false
                        setreviceNotificationArr(temp1)
                    }
                    setcount(count + 1)
                }else if(e===6){
                    temp1[e-1].Value=(!(temp1[e-1].Value))
                    setreviceNotificationArr(temp1)
                    if ( temp1[1].Value && temp1[2].Value && temp1[3].Value && temp1[4].Value && temp1[5].Value ){
                        temp1[0].Value=true
                        setreviceNotificationArr(temp1)
                    }else {
                        temp1[0].Value=false
                        setreviceNotificationArr(temp1)
                    }
                    setcount(count + 1)
                }
                break
        }
    }

    const sendData=()=>{
        let finaldata=fullResults
        let listdate=resultsList
        //finaldata.SENDING_NOTIFICATIONS=finaldata?.SENDING_NOTIFICATIONS?.length < 4?listdate.SENDING_NOTIFICATIONS:finaldata?.SENDING_NOTIFICATIONS
        // if(finaldata.SENDING_NOTIFICATIONS){
        //     if(finaldata?.SENDING_NOTIFICATIONS?.length<4){
        //         finaldata.SENDING_NOTIFICATIONS =listdate.SENDING_NOTIFICATIONS
        //     }
        // }
        // else{
        //     finaldata.SENDING_NOTIFICATIONS =listdate.SENDING_NOTIFICATIONS
        // }
        if(finaldata.SENDING_NOTIFICATIONS !== undefined){
            for (let a=0;a<listdate?.SENDING_NOTIFICATIONS?.length;a++){
                listdate.SENDING_NOTIFICATIONS[a].BitValue=  sendNotificationArr[a].Value
            }

        let newarr = listdate.SENDING_NOTIFICATIONS
      
        for(let i=0;i<finaldata.SENDING_NOTIFICATIONS.length;i++){
            const retail = newarr.filter(iterator => iterator.Code !== finaldata.SENDING_NOTIFICATIONS[i].Code && iterator.BitValue == false )
           newarr = retail;
        }
        finaldata.SENDING_NOTIFICATIONS = [...finaldata.SENDING_NOTIFICATIONS , ...newarr]
        }
      else{
        finaldata.SENDING_NOTIFICATIONS =listdate.SENDING_NOTIFICATIONS
      }
   

        // if(finaldata.RECIPIENT_NOTIFICATIONS){
        //     if(finaldata?.RECIPIENT_NOTIFICATIONS?.length<6){
        //         finaldata.RECIPIENT_NOTIFICATIONS =listdate.RECIPIENT_NOTIFICATIONS
        //     }
        // }
        // else{
        //     finaldata.RECIPIENT_NOTIFICATIONS =listdate.RECIPIENT_NOTIFICATIONS
        // }
        //let newarr1 = listdate.RECIPIENT_NOTIFICATIONS
        if(finaldata.RECIPIENT_NOTIFICATIONS !== undefined){
            for (let a=0;a<listdate?.RECIPIENT_NOTIFICATIONS?.length;a++){
                listdate.RECIPIENT_NOTIFICATIONS[a].BitValue=  reviceNotificationArr[a].Value
            }

        let newarr1 = listdate.RECIPIENT_NOTIFICATIONS
        for(let i=0;i<finaldata.RECIPIENT_NOTIFICATIONS.length;i++){
             
            const retail = newarr1.filter(iterator => iterator.Code !== finaldata.RECIPIENT_NOTIFICATIONS[i].Code && iterator.BitValue == false )
           newarr1 = retail;
        }
         finaldata.RECIPIENT_NOTIFICATIONS = [...finaldata.RECIPIENT_NOTIFICATIONS , ...newarr1]
        }
      else{
        finaldata.RECIPIENT_NOTIFICATIONS =listdate.RECIPIENT_NOTIFICATIONS
      }
        //finaldata.RECIPIENT_NOTIFICATIONS =listdate.RECIPIENT_NOTIFICATIONS
         //finaldata.SENDING_NOTIFICATIONS=listdate.SENDING_NOTIFICATIONS
        //finaldata.RECIPIENT_NOTIFICATIONS=listdate.RECIPIENT_NOTIFICATIONS
        if (finaldata.SENDER_SEQ_POSITION[0]["UserSettingId"] === undefined){
            finaldata.SENDER_SEQ_POSITION[0]["Type"]=null
            finaldata.SENDER_SEQ_POSITION[0]["UserId"]=finaldata.DATE_FORMAT[0].UserId
            finaldata.SENDER_SEQ_POSITION[0]["UserSettingId"]=-1
        }
        if (finaldata.WHATSAPP && finaldata.WHATSAPP[0]["UserSettingId"] === undefined){
            finaldata.WHATSAPP[0]["Type"]=null
            finaldata.WHATSAPP[0]["UserId"]=finaldata.DATE_FORMAT[0].UserId
            finaldata.WHATSAPP[0]["UserSettingId"]=-1
        }
        if (finaldata.GENERAL_PREFERENCES[0]["UserSettingId"] === undefined){
            finaldata.GENERAL_PREFERENCES[0]["Type"]=null
            finaldata.GENERAL_PREFERENCES[0]["UserId"]=finaldata.DATE_FORMAT[0].UserId
            finaldata.GENERAL_PREFERENCES[0]["UserSettingId"]=-1
        }

        for (let a=0;a<finaldata?.SENDING_NOTIFICATIONS?.length;a++){
            let temp_arr_send=finaldata.SENDING_NOTIFICATIONS[a]

            if (temp_arr_send.Code==="SEND_NOTIFY_SELECT_ALL" && !(temp_arr_send.UserSettingId) ){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="SEND_ENVELOPE_APPROVED" &&  !(temp_arr_send.UserSettingId)){
                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="SEND_ENVELOPE_REJECT" &&  !(temp_arr_send.UserSettingId)){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="SEND_ENVELOPE_DELETE" &&  !(temp_arr_send.UserSettingId)){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
   
            if (a < 4){
                const senddata = sendNotificationArr.filter((item) => item.Name === finaldata.SENDING_NOTIFICATIONS[a].Code);
                finaldata.SENDING_NOTIFICATIONS[a].BitValue=  senddata[0].Value
                if(finaldata.SENDING_NOTIFICATIONS[a].BitValue == true && finaldata.SENDING_NOTIFICATIONS[a].UserSettingId > 0){
                    finaldata.SENDING_NOTIFICATIONS[a].resetToDefault =1
                }
            }





        }

        for (let a=0;a<finaldata?.RECIPIENT_NOTIFICATIONS?.length;a++){

            let temp_arr_rec=finaldata.RECIPIENT_NOTIFICATIONS[a]
            if (temp_arr_rec.Code==="RECIPIENT_NOTIFY_SELECT_ALL" && !(temp_arr_rec.UserSettingId)){

                temp_arr_rec["Type"]=null
                temp_arr_rec["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_rec["UserSettingId"]=-1

            }
            if (temp_arr_rec.Code==="RECIPIENT_ENVELOPE_SENT" && !(temp_arr_rec.UserSettingId)){

                temp_arr_rec["Type"]=null
                temp_arr_rec["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_rec["UserSettingId"]=-1

            }
            if (temp_arr_rec.Code==="RECIPIENT_ENVELOPE_APPROVED" && !(temp_arr_rec.UserSettingId)){

                temp_arr_rec["Type"]=null
                temp_arr_rec["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_rec["UserSettingId"]=-1

            }
            if (temp_arr_rec.Code==="RECIPIENT_ENVELOPE_REJECT" && !(temp_arr_rec.UserSettingId)){

                temp_arr_rec["Type"]=null
                temp_arr_rec["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_rec["UserSettingId"]=-1

            }if (temp_arr_rec.Code==="RECIPIENT_ENVELOPE_CORRECT" && !(temp_arr_rec.UserSettingId)){
                temp_arr_rec["Type"]=null
                temp_arr_rec["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_rec["UserSettingId"]=-1

            }if (temp_arr_rec.Code==="RECIPIENT_ENVELOPE_DELETE" && !(temp_arr_rec.UserSettingId)){

                temp_arr_rec["Type"]=null
                temp_arr_rec["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_rec["UserSettingId"]=-1

            }


        if (a < 6){
            const senddata1 = reviceNotificationArr.filter((item) => item.Name === finaldata.RECIPIENT_NOTIFICATIONS[a].Code);
            finaldata.RECIPIENT_NOTIFICATIONS[a].BitValue=   senddata1[0].Value
            if(finaldata.RECIPIENT_NOTIFICATIONS[a].BitValue == true && finaldata.RECIPIENT_NOTIFICATIONS[a].UserSettingId > 0){
                finaldata.RECIPIENT_NOTIFICATIONS[a].resetToDefault =1
            }
            setcount(0)
        }

            // if (finaldata.RECIPIENT_NOTIFICATIONS[a].BitValue){
            //     // finaldata.splice(x, 1);
            //     delete finaldata.RECIPIENT_NOTIFICATIONS[a]
            // }
        }

        for (let a=0;a<finaldata?.SIGNATURE_SETTINGS?.length;a++){
            let temp_arr_send=finaldata.SIGNATURE_SETTINGS[a]

            if (temp_arr_send.Code==="INC_FIRST_LAST_NAME" && !(temp_arr_send.UserSettingId) ){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="INC_FIRST_NAME" &&  !(temp_arr_send.UserSettingId)){
                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="INC_LAST_NAME" &&  !(temp_arr_send.UserSettingId)){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="INC_DESIGNATION" &&  !(temp_arr_send.UserSettingId)){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="INC_PHONE_NUMBER" &&  !(temp_arr_send.UserSettingId)){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="INC_LOCATION" &&  !(temp_arr_send.UserSettingId)){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }

        }

        for (let a=0;a<finaldata?.DATE_FORMAT?.length;a++){
            let temp_arr_send=finaldata.DATE_FORMAT[a]

            if (temp_arr_send.Code==="US_DATE_FORMAT" && !(temp_arr_send.UserSettingId) ){

                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
            if (temp_arr_send.Code==="IND_DATE_FORMAT" &&  !(temp_arr_send.UserSettingId)){
                temp_arr_send["Type"]=null
                temp_arr_send["UserId"]=finaldata.DATE_FORMAT[0].UserId
                temp_arr_send["UserSettingId"]=-1
            }
           
        }
        // debugger
        if(defaultResultsUsersendingnotifications === true){
            const updateddata = finaldata?.SENDING_NOTIFICATIONS.filter((item) => item.BitValue === false || item.BitValue== 0);
            finaldata.SENDING_NOTIFICATIONS = updateddata
        }
        if(defaultResultsUserRecipientnotifications === true){
            const updateddata1 = finaldata?.RECIPIENT_NOTIFICATIONS.filter((item) => item.BitValue === false || item.BitValue== 0 );
            finaldata.RECIPIENT_NOTIFICATIONS = updateddata1
        }
         let tempsetting
        let newObj=finaldata
       
        //console.log(newObj,"newObj")
          //let newObj
        if (fullResults.SIGNATURE_FONT){
             tempsetting= {...fullResults.SIGNATURE_FONT[0]}
            if (tempsetting["UserSettingId"] === undefined){
                tempsetting["Type"]=null
                tempsetting["UserId"]=finaldata.DATE_FORMAT[0].UserId
                tempsetting["UserSettingId"]=-1
            }
            let tempobject={...listdate}
            tempobject.SIGNATURE_FONT=[tempsetting]
             newObj = { ...finaldata, "SIGNATURE_FONT":tempobject.SIGNATURE_FONT }
            console.log("newObj",newObj)
            newObj.SIGNATURE_FONT[0].IntValue=values
            delete newObj.SIGNATURE
        }

        console.log(newObj,"newObj last")
    
        if(newObj.RECIPIENT_NOTIFICATIONS !== undefined && newObj.RECIPIENT_NOTIFICATIONS.length == 0){
            delete newObj.RECIPIENT_NOTIFICATIONS
        
        }
        if(newObj.SENDING_NOTIFICATIONS !== undefined && newObj.SENDING_NOTIFICATIONS.length == 0){
            delete newObj.SENDING_NOTIFICATIONS
        }
        console.log(newObj,"newObj last one")
        // delete newObj.SENDING_NOTIFICATIONS
           
           
           
        usersService.updateUserSetting(newObj)
            .then((result) => {
                console.log('!!!!!!!!!!!!!!!!!!', result)
             if(result.HttpStatusCode === 200){
                setsavedata(!savedata)
                let userData = localStorage.getItem('UserObj') ? JSON.parse(localStorage.getItem('UserObj')) : ''
                if (userData.user_id) {
                addDocumentService.getUserPreferences(userData.user_id)
                }
             }
                

            })
            .catch(error => {

            });

        console.log(finaldata)
    }

    const reset=()=>{
        setFullResults(resultsList)
        setcount(0)
    }


    function handelrange(e) {
        let myRange = document.querySelector('#myRange');
        let myValue = document.querySelector('#myValue');
        let myUnits = 'Font Size:';
        let off = myRange.offsetWidth / (parseInt(myRange.max) - parseInt(myRange.min));
        let px =  ((myRange.valueAsNumber - parseInt(myRange.min)) * off) - (myValue.offsetParent.offsetWidth / 2);

        myValue.parentElement.style.left = (px + 40) + 'px';
        myValue.parentElement.style.top = myRange.offsetHeight + 'px';
        // myValue.parentElement.style.top =  + 'px';
        // myValue.innerHTML = myRange.value + ' ' + myUnits;


            let pxx = ((myRange.valueAsNumber - parseInt(myRange.min)) * off) - (myValue.offsetWidth / 2);
               
            if (myRange.value === "10"){
                myValue.innerHTML =   + myRange.value + "(Default)" ;
            }else {
                myValue.innerHTML =    + myRange.value
            }

            myValue.parentElement.style.left = pxx + 'px';
            setValues(e)

    }
    
    return (
        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content userpreferences">
                    <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                {/* <h3>User Preferences</h3> */}
                                <h3 style={{fontSize:35,fontWeight:300,marginTop:10}}>User Preferences</h3>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
						 <div className="recipient-btn"> 
                         <button onClick={()=>history.push('/account/dashboard')} className="btn back_btn" style={{ padding:"10px 10px",backgroundColor:"white",border:" 1px solid black",width:"39px",height:"40px"}}><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}}/></button>
                            </div>
							</div>
                        </div>
                    </div>

                    <div className='row align-items-center'>
                        <div className='references'>
                            <div className='tab'>
                                <ul className='navbar-tabs'>
                                    <li className="tablinks active" onClick={(e) => openTab(e, 'firstTab')}><button>General Preferences</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'secondTab')}><button>Email Preferences</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}><button>Date Format</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button>Signature Settings</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'fifthTab')}><button>Signature Font Size</button></li>
                                </ul>
                            </div>
                            <div id="firstTab" className='tab-content'>
                                {/*<div className='heading'>*/}
                                {/*    <h4>GENERAL PREFERENCES</h4>*/}
                                {/*</div>*/}
                                <div className='check-section'>
                                    <h5>GENERAL PREFERENCES</h5>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check" >
                                                <input class="form-check-input" onClick={(e)=>handleChanger('GENERALPREFERENCES',(fullResults && fullResults.GENERAL_PREFERENCES && fullResults.GENERAL_PREFERENCES[0]?.BitValue))} type="checkbox" checked={(fullResults &&fullResults.GENERAL_PREFERENCES&& fullResults.GENERAL_PREFERENCES[0]?.BitValue)} id="GENERALPREFERENCES" />
                                                <label class="form-check-label" for="GENERALPREFERENCES">
                                                    Include audit trail in pdf
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                {
                                    companyPack==="COMPANY" &&
                                    <div className='check-section'>
                                        <h5>WHATSAPP NOTIFICATIONS</h5>
                                        <ul className='checkList'>
                                            <li>
                                                <div className="form-check">
                                                    <input className="form-check-input" type="checkbox"
                                                           onClick={(e) => handleChanger('WHATSAPP', (fullResults && fullResults.WHATSAPP[0]?.BitValue))}
                                                           checked={(fullResults && fullResults.WHATSAPP && fullResults.WHATSAPP[0]?.BitValue)}
                                                           id="WHATSAPPNOTIFICATIONS"/>
                                                    <label className="form-check-label" htmlFor="WHATSAPPNOTIFICATIONS">
                                                        Enable
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                }

                                <div className='check-section'>
                                    <h5>DOCUMENT INITIATOR POSITION(DEFAULT POSITION IS NO.1)</h5>
                                    <ul className='checkList'>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="checkbox" onClick={(e)=>handleChanger('SENDER_SEQ_POSITION',(fullResults && fullResults.SENDER_SEQ_POSITION && fullResults.SENDER_SEQ_POSITION[0]?.BitValue))} checked={(fullResults && fullResults.SENDER_SEQ_POSITION && fullResults.SENDER_SEQ_POSITION[0]?.BitValue)}
                                                       id="DOCUMENTINITIATORPOSITION"/>
                                                <label className="form-check-label" htmlFor="DOCUMENTINITIATORPOSITION">
                                                    Move the default position of the sender to the last in the workflow
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div id="secondTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        {sendNotificationArr.map((item,index)=>{

                                            return(
                                                <>
                                                    <li>
                                                        <div className="form-check" onClick={()=>handleChanger('SENDING_NOTIFICATIONS',(item.No))}>
                                                            <input className="form-check-input" type="checkbox" value=""
                                                                   id={`flexCheckDefault ${index}`} checked={item.Value} />
                                                            <label className="form-check-label"
                                                                   htmlFor={`flexCheckDefault${index}`}>
                                                                {item.Text}
                                                            </label>
                                                        </div>
                                                    </li>
                                                </>
                                            )
                                        })
                                        }
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        {reviceNotificationArr.map((item,index)=>{
                                            console.log(item.Value)
                                            return(
                                                <>
                                                    <li>
                                                        <div className="form-check" onClick={()=>handleChanger('RECIPIENT_NOTIFICATIONS',(item.No))}>
                                                            <input className="form-check-input" type="checkbox" value=""
                                                                   id={`flexCheckDefault ${index}`} checked={item.Value} />
                                                            <label className="form-check-label"
                                                                   htmlFor={`flexCheckDefault${index}`}>
                                                                {item.Text}
                                                            </label>
                                                        </div>
                                                    </li>
                                                </>
                                            )
                                        })
                                        }
                                        {/*<li>*/}
                                        {/*    <div class="form-check">*/}
                                        {/*        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />*/}
                                        {/*        <label class="form-check-label" for="flexCheckDefault">*/}
                                        {/*            Select All*/}
                                        {/*        </label>*/}
                                        {/*    </div>*/}
                                        {/*</li>*/}
                                        {/*<li>*/}
                                        {/*    <div class="form-check">*/}
                                        {/*        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"  />*/}
                                        {/*        <label class="form-check-label" for="flexCheckDefault">*/}
                                        {/*            I have a document to sign*/}
                                        {/*        </label>*/}
                                        {/*    </div>*/}
                                        {/*</li>*/}
                                        {/*<li>*/}
                                        {/*    <div class="form-check">*/}
                                        {/*        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />*/}
                                        {/*        <label class="form-check-label" for="flexCheckDefault">*/}
                                        {/*            When the document is completed*/}
                                        {/*        </label>*/}
                                        {/*    </div>*/}
                                        {/*</li>*/}
                                        {/*<li>*/}
                                        {/*    <div class="form-check">*/}
                                        {/*        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"  />*/}
                                        {/*        <label class="form-check-label" for="flexCheckDefault">*/}
                                        {/*            Another signer declines to sign*/}
                                        {/*        </label>*/}
                                        {/*    </div>*/}
                                        {/*</li>*/}
                                        {/*<li>*/}
                                        {/*    <div class="form-check">*/}
                                        {/*        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />*/}
                                        {/*        <label class="form-check-label" for="flexCheckDefault">*/}
                                        {/*            The sender corrects a document*/}
                                        {/*        </label>*/}
                                        {/*    </div>*/}
                                        {/*</li>*/}
                                        {/*<li>*/}
                                        {/*    <div class="form-check">*/}
                                        {/*        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />*/}
                                        {/*        <label class="form-check-label" for="flexCheckDefault">*/}
                                        {/*            Documents will be purged from the systern*/}
                                        {/*        </label>*/}
                                        {/*    </div>*/}
                                        {/*</li>*/}
                                    </ul>
                                </div>
                            </div>
                            <div id="thirdTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>DATE FORMAT</h4>
                                    <p>Apply the following formats to the 'Date Signed' field.</p>
                                </div>
                                <div className='check-section'>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check" >
                                                <input class="form-check-input" onClick={()=>handleChanger("DATE_FORMAT",0)} type="radio"
                                            //    checked ={fullResults?.DATE_FORMAT?.DATE_FORMAT[0]?.BitValue}
                                            //checked ={defaultdatechecked === true ?defaultdatechecked: ((fullResults && fullResults.DATE_FORMAT &&fullResults.DATE_FORMAT.length>1&&fullResults.DATE_FORMAT[0].BitValue) === true) ?true:false}
                                            checked={defaultdatechecked ===true?defaultdatechecked: (fullResults && fullResults.DATE_FORMAT && fullResults.DATE_FORMAT[0]?.Code==="US_DATE_FORMAT" && fullResults.DATE_FORMAT[0]?.BitValue===true)}

                                            //checked={(fullResults&&fullResults.DATE_FORMAT&&fullResults.DATE_FORMAT.length>0&&fullResults.DATE_FORMAT[0].BitValue === true)?true : false } 
                                                 id="DATEFORMAT1" name={"DATEFORMAT"} />
                                                <label class="form-check-label" for="DATEFORMAT1">
                                                    MM-DD-YYYY
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" onClick={()=>handleChanger("DATE_FORMAT",1)} type="radio"  checked={(fullResults && fullResults.DATE_FORMAT && fullResults.DATE_FORMAT[1] && fullResults.DATE_FORMAT[1].Code==="IND_DATE_FORMAT" &&  fullResults.DATE_FORMAT[1].BitValue===true)}
 id="DATEFORMAT2" name={"DATEFORMAT"}  />
                                                <label class="form-check-label" for="DATEFORMAT2">
                                                    DD-MM-YYYY
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>SIGNATURE SETTINGS</h4>
                                    <p>Customize your signature as per your requirement by using the below options.</p>
                                </div>
                                <div className='check-section'>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" id="SIGNATURESETTINGS1" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",0)}
                                                       checked={fullResults && fullResults.SIGNATURE_SETTINGS && fullResults.SIGNATURE_SETTINGS[0]?.Code==="INC_FIRST_LAST_NAME" && fullResults.SIGNATURE_SETTINGS[0]?.BitValue===true}
                                                />
                                                <label class="form-check-label" for="SIGNATURESETTINGS1">
                                                    Include First and Last Name
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS2" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",1)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS && fullResults.SIGNATURE_SETTINGS[1] && fullResults.SIGNATURE_SETTINGS[1].Code==="INC_FIRST_NAME" &&  fullResults.SIGNATURE_SETTINGS[1].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS2">
                                                    Include First Name only
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS3" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",2)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS && fullResults.SIGNATURE_SETTINGS[2] && fullResults.SIGNATURE_SETTINGS[2].Code==="INC_LAST_NAME" && fullResults.SIGNATURE_SETTINGS[2].BitValue===true)}

                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS3">
                                                    Include Last Name only
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS4" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",3)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS && fullResults.SIGNATURE_SETTINGS[3] && fullResults.SIGNATURE_SETTINGS[3].Code==="INC_DESIGNATION" && fullResults.SIGNATURE_SETTINGS[3].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS4">
                                                    Include Designation
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS5" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",4)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS && fullResults.SIGNATURE_SETTINGS[4] && fullResults.SIGNATURE_SETTINGS[4].Code==="INC_PHONE_NUMBER" && fullResults.SIGNATURE_SETTINGS[4].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS5">
                                                    Include Phone Number
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS6" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",5)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS && fullResults.SIGNATURE_SETTINGS[5] && fullResults.SIGNATURE_SETTINGS[5].Code==="INC_LOCATION" && fullResults.SIGNATURE_SETTINGS[5].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS6">
                                                    Include Location (City)
                                                </label>
                                            </div>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                            <div id="fifthTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>SIGNATURE FONT SIZE</h4>
                                    <p>Customize your signature font size as per your requirement by using the below scale.</p>
                                </div>
                                <div>
                                    <p>Input slider:</p>
                                    <div className={'customer-range'}>
                                        {/*<input type="range" min="7" max="13" className="custome-range" value="1"/>*/}
                                        {/*<span style={{position:"absolute",top:"-20px"}} >*/}
                                        {/*<span  id={"myValue"}></span>*/}
                                        {/*</span>*/}
                                        <input type={"range"} id={"myRange"} min={7} max={13} className={"custome-range"} value={values} onChange={(e)=>setValues(e.target.value)} />



                                        {/*<Range*/}
                                        {/*    step={1}*/}
                                        {/*    min={7}*/}
                                        {/*    max={13}*/}
                                        {/*    values={values}*/}
                                        {/*    onChange={(values) => {*/}
                                        {/*        setValues(values)*/}
                                        {/*    }}*/}
                                        {/*    renderTrack={({ props, children }) => (*/}
                                        {/*        <div*/}
                                        {/*            {...props}*/}
                                        {/*            className="w-full slider-line-heigth h-3 pr-2 my-4 bg-gray-200 rounded-md"*/}
                                        {/*        >*/}
                                        {/*            {children}*/}
                                        {/*        </div>*/}
                                        {/*    )}*/}
                                        {/*    renderThumb={({ props }) => (*/}
                                        {/*        <div*/}
                                        {/*            {...props}*/}
                                        {/*            className="w-5 h-5  custome-bg-color transform slider-line-pointer translate-x-10 bg-indigo-500 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"*/}
                                        {/*        >*/}
                                        {/*            <div className={'slider-line-valuse'}>{values[0]}{values[0] === 10 && ' (Default)'}</div>*/}
                                        {/*        </div>*/}
                                        {/*    )}*/}
                                        {/*/>*/}
                                        <div className={'d-flex justify-content-between mt--18'}>
                                            <div>7</div>
                                            <div>13</div>
                                        </div>
                                        {/*{*/}
                                        {/*    values==="10"?*/}
                                        {/*}*/}
                                        <div className={"text-center mt-4"}>  {values === "10" ?`${values} (Default)`:values}   </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                    <div>
                            <div className="subf_pagn_btn text-right">
                                {/*<button className="btn" onClick={()=>reset()} >Reset</button>*/}
                                {/* <button style={{backgroundColor:"blue",border:"1px solid blue"}} className="btn" onClick={()=>sendData()} >SAVE</button> */}
                                <button type="button" className="btn save-btn" style={{width: 110, backgroundColor: "2F7FED", border: "1px solid 2F7FED", color: "white"}}
                                    onClick={() => sendData()}>SAVE </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export { AccountProfile };
