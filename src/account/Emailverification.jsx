import React, { useState, useEffect } from 'react';
import { Link,useHistory ,useLocation} from 'react-router-dom';
import queryString from 'query-string';
import { Formik, Field, Form, ErrorMessage, yupToFormErrors } from 'formik';
import * as Yup from 'yup';
import { TextField } from "@material-ui/core";
import Reaptcha from 'reaptcha';
import { accountService, alertService } from '@/_services';
import { result } from 'lodash';
import * as CryptoJS from 'crypto-js';
import Swal from 'sweetalert2';

function EmailVerification( ){

    let email=''
    let code=''
    const usehistory = useHistory();  
    const search = useLocation().search;
    const searchParams = new URLSearchParams(search);
    const URLPARAM = window.atob(searchParams.get("q"))
    
    if (URLPARAM) {
        const splitData = URLPARAM.split('&')
        if (splitData && splitData[0] && splitData[1]) {
            email = splitData[0].split('=') ? (splitData[0].split('=')[1]) : '';
            code = splitData[1].split('=') ? (splitData[1].split('=')[1]) : '';
        }
    }
    const verifytheregistration=()=>{
          if(email !==""&& code !=="")
                 accountService.emailverification(email,code).then((result)=>{
                    if(result?.Error==="" ){
                        if(result?.Entity?.isRegistrationVerified == false){
                            Swal.fire({
                                position: 'center',
                                icon: 'info',
                                title: 'Link expired / broken',
                                showConfirmButton: true,
                                confirmButtonText:"OKAY",
                              }).then((result) => {
                                if (result.isConfirmed) {
                                    usehistory.push('/account/login');
                                }
                            })
                        }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Successfully verifyed email',
                            showConfirmButton: true,
                            confirmButtonText:"OKAY",
                          }).then((result) => {
                            if (result.isConfirmed) {
                                usehistory.push('/account/login');
                            }
                        })
                    }
                    }
                    else{
                        setErrormessage("somthing wrong")
                    }
                  })
     }
     return (
        <div>
            <div className="card-body">{verifytheregistration()}</div>
        </div>
    )
     }

     
export { EmailVerification }; 