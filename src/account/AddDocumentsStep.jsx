import React from 'react';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { Document, Page, pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
import { commonService } from '../_services/common.service';
import { addDocumentService } from '../_services/adddocument.service';
import { Link, useHistory, useParams, useLocation } from 'react-router-dom';
import { useEffect, useState, useRef } from "react";
import "../css/adddocumentstep1.css";
import ReactTooltip from 'react-tooltip';
import Tooltip from "@material-ui/core/Tooltip";
import { addDocStep2Options, getSessionItem, removeSessionItem, setSessionItem } from '../_services/model.service';
import config from 'config';
import { useTranslation } from 'react-i18next';
import { Button, Modal, ProgressBar } from 'react-bootstrap';
import { DocumentCard } from './Content/DocumentCard';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { alertService } from '@/_services';
import CloseIcon from '@mui/icons-material/Close';
import Spinner from 'react-bootstrap/Spinner';
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import { manageservice } from '../_services/manageservice';
import DoneIcon from '@mui/icons-material/Done';
import moment from 'moment';
let docsData = []
let nonDeleteDocsData = []
let CreateDocumentId = ''
function AddDocumentsStep() {
    const handle = useFullScreenHandle();
    const usehistory = useHistory();
    const [count, setCount] = React.useState(0);
    const [errorCount, setErrorCount] = React.useState(0);
    const [selectDocumentFile, setSelectDocumentFile] = useState('');
    const [selectedPageNumber, setselectedPageNumber] = useState(1);
    const [step2Data, setStep2Data] = useState({});
    const [documentName, setDocumentName] = useState();
    const [editEnable, setEditEnable] = useState();
    const [docmodalShow, setDocmodalShow] = useState(false);
    const [documentRename, setdocumentRename] = useState(false);
    const { t, i18n } = useTranslation();
    const [popDocPages, setPopDocPages] = React.useState(1);
    const [uploadOption, setUploadOption] = React.useState(false);
    const [viewPdf, setviewPdf] = React.useState(false);
    const wrapperRef = useRef(null);
    const wrapperRef1 = useRef(null);
    const [selectDocumentIndex, setSelectDocumentIndex] = useState('');
    const [selectDocumentName, setSelectDocumentName] = useState('');
    const [uploadPercentage, setuploadPercentage] = useState(0)
    const [showErrorPopUp, setShowErrorPopUp] = useState(false);
    const [documentFileArray, setDocumentFileArray] = useState([]);
    const [documentUpdateData, setdocumentUpdateData] = useState({});
    const dndRef = useRef(null)
    const location = useLocation();
    const [isUpdateChangesHappen, setIsUpdateChangesHappen] = useState(false);
    const [showdrivemodel, setshowdrivemodel] = useState(false);
    const [Goodrivedata, setGoodrivedata] = useState([]);
    const [Exporttitle, setExporttitle] = useState(null)
    const [userInfo, setUserInfo] = useState({})
    const [childArr, setChildArr] = useState([])
    const [istempdoc, setistempdoc] = useState(false)
    const [templaterecipients, settemplaterecipients] = useState([])
    const [IsFromScratchDocument, setIsFromScratchDocument] = useState(false)
    const button = useRef(true)
    const Ccount = useRef(0)
    if (location.state) {
        var { DocumentID } = location.state;
    }


    const useOutsideAlerter = (ref) => {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    moreOptionDetail(-1)
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }

    //   useOutsideAlerter(wrapperRef);

    const useOutsideAlerter1 = (ref1) => {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref1.current && !ref1.current.contains(event.target)) {
                    setUploadOption(false)
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref1]);
    }

    // useOutsideAlerter1(wrapperRef1);
    useEffect(async () => {
        docsData = []
        alertService.clear()
        CreateDocumentId = ''
        getUserInfo();
        getTags();


        if (commonService.documentValue && !DocumentID) {
            for (let index = 0; index < commonService.documentValue.length; index++) {
                if (commonService.documentValue[index]) {
                    
                    await getInitialFile(commonService.documentValue[index])
                }
            }
        } else if (commonService.cloudDocuments && commonService.cloudDocuments.DocsData && !DocumentID) {
            let DocumentData = commonService.cloudDocuments
            setChildArr(DocumentData.DocsData),
                setExporttitle(DocumentData.type)
            commonService.setCloudDocuments({})
            insertHandler(DocumentData.type, DocumentData.DocsData)
        }
        if (commonService.templatedocumentValue && !DocumentID) {
            let data = commonService.templatedocumentValue
            setistempdoc(true)
            settemplaterecipients(data.Recipients)
            let TemplateDocumentObj = {}
            TemplateDocumentObj.document = config.serverUrl + 'adocuments/download/' + data.FileName
            TemplateDocumentObj.documentName = data.OriginalFileName
            TemplateDocumentObj.isMoreOption = false
            TemplateDocumentObj.IsDelete = false;
            TemplateDocumentObj.Position = 1
            TemplateDocumentObj.DocumentSource = 'DocumentExist'
            TemplateDocumentObj.process = true
            TemplateDocumentObj.ADocumentSplitId = -1
            TemplateDocumentObj.Height = data.Height
            TemplateDocumentObj.Width = data.Width
            TemplateDocumentObj.TemplateId = data.TemplateId
            TemplateDocumentObj.FileName = data.FileName
            //  let array=[]
            //data.map(item=>{
            // array.push(data)
            // })
            setDocumentName(data.OriginalFileName)
            //  for(let i=0; i<array.length; i++){
            //     array[i].document = config.serverUrl + 'adocuments/download/' +array[i].FileName
            //     array[i].documentName=array[i].OriginalFileName
            //     array[i].isMoreOption=false
            //     array[i].IsDelete=false
            //     array[i].DocumentSource = 'DocumentExist'
            //     array[i].process=true
            // }
            setDocumentFileArray([...[TemplateDocumentObj]])
            nonDeleteDocsData = [TemplateDocumentObj]
            commonService.settemplateDocument(null)
        }
        if (commonService.docauthdocumentValue && !DocumentID) {
            CreateDocumentId = commonService.docauthdocumentValue.ADocumentId
            let data = commonService.docauthdocumentValue
            if (commonService.docauthdocumentValue && commonService.docauthdocumentValue.IsFromScratchDocument) {
                setIsFromScratchDocument(true)
            }
            setdocumentUpdateData({ ...commonService.docauthdocumentValue })
            let array = []
            array.push(data.UploadDocuments[0]);
            setDocumentName(array[0].OriginalFileName)
            for (let i = 0; i < array.length; i++) {
                array[i].document = config.serverUrl + 'adocuments/download/' + array[i].FileName
                array[i].documentName = array[i].OriginalFileName
                array[i].isMoreOption = false
                array[i].IsDelete = false
                array[i].DocumentSource = 'DocumentExist'
                array[i].process = true
                array[i].Position = 1

            }
            setDocumentFileArray([...array])
            nonDeleteDocsData = array
            commonService.setDocauthDocument(null)
        }
        else if (DocumentID) {
            getDocumentInfoData()
        }
        commonService.setDocument('')
    }, [])
    useEffect(() => {
        docsData = documentFileArray
        nonDeleteDocsData = docsData.filter(e => e.IsDelete == false)
    }, [documentFileArray])
    function getDocumentInfoData() {
        addDocumentService.getDocumentInfo(DocumentID ,true).then(data => {
            if (data.Entity && data.Entity.Data) {
                setDocumentName(data.Entity.Data.Title)
                setdocumentUpdateData({ ...data.Entity.Data })
                for (let i = 0; i < data.Entity.Data.UploadDocuments.length; i++) {
                    data.Entity.Data.UploadDocuments[i].document = config.serverUrl + 'adocuments/download/' + data.Entity.Data.UploadDocuments[i].FileName
                    data.Entity.Data.UploadDocuments[i].documentName = data.Entity.Data.UploadDocuments[i].OriginalFileName
                    data.Entity.Data.UploadDocuments[i].isMoreOption = false
                    data.Entity.Data.UploadDocuments[i].IsDelete = false
                    data.Entity.Data.UploadDocuments[i].DocumentSource = 'DocumentExist'
                    data.Entity.Data.UploadDocuments[i].process = true

                }
                setDocumentFileArray([...data.Entity.Data.UploadDocuments])
                nonDeleteDocsData = data.Entity.Data.UploadDocuments
                //  setSubDocumentFile(config.serverUrl + 'adocuments/download/' + data.Entity.Data.FileName)
            } else {

            }
        })
    }
    const getInitialFile = (file) => {
        if(button.current){
            document.body.classList.add('loading-indicator');
           button.current = !button.current
        }
        
        return new Promise(async (resolve) => {
            if (file) {
                // \.ppt|\.pptx|
                // .ppt, .pptx,

                var allowedExtensions = /(\.doc|\.docx|\.png|\.jpg|\.jpeg|\.xls|\.xlsx|\.pdf)$/i;
                if (!allowedExtensions.exec(file.name)) {
                    alertService.warn('Only the following file formats are acceptable: .doc, .docx, .png, .jpg, .jpeg, .xls, .xlsx, .pdf')
                    // setShowErrorPopUp(true)
                    // setErrorCount(0)
                } else {
                    
                    let fileExtExtract = file.name.split('.');
                    let fileExtName = ('.' + fileExtExtract[fileExtExtract.length - 1]).toLowerCase();
                    const nonDeleteDocs = docsData.filter(e => e.IsDelete == false)
                    let position = nonDeleteDocs.length + 1
                    let replaceIndex = JSON.parse(getSessionItem('replaceIndex'))
                    if (fileExtName == '.pdf') {
                        let docObj = {
                            "ADocumentSplitId": -1,
                            "Height": 1188,
                            "Width": 918,
                            "Pages": 0,
                            "OriginalFileName": file.name,
                            "FileName": '',
                            "Position": position,
                            "DocumentSource": 'upload',
                            "IsDelete": false,
                            document: file,
                            documentName: file.name,
                            "UploadedOn": new Date,
                            isMoreOption: false,
                            "Recipients": [],
                            process: true

                        }
                        if (!documentName && docsData && docsData.length == 0) {
                            setDocumentName(file.name.split('.')[0])
                        }
                        if (replaceIndex || replaceIndex == 0) {
                            docObj.Position = replaceIndex + 1
                            const existingData = { ...docsData[replaceIndex] }
                            if (existingData.ADocumentSplitId !== -1 || !existingData.ADocumentSplitId) {
                                existingData.IsDelete = true;
                                docsData.push(existingData)
                            }
                            docsData[replaceIndex] = docObj
                            setDocumentFileArray([...docsData])
                            getProperAddDocumentPosition()
                        } else {
                            setDocumentFileArray(oldArray => [...oldArray, docObj])
                        }
                        removeSessionItem('replaceIndex')
                    } else {
                        var documentInfo = {
                            "error": false,
                            "UploadedOn": new Date,
                            "IsSelected": true,
                            "Position": position,
                            "Height": 1262.835,
                            "Width": 892.92,
                            "isFileConvertedToPdf": true,
                            "DocumentSource": "upload",
                            "Recipients": [],
                            "IsDelete": false,
                            "OriginalFileName": file.name,
                            "FileName": file.name,
                            "ADocumentSplitId": -1,
                            "Pages": 0,
                            process: true
                        }
                        let apiResult = await commonService.convertFileToPDF(documentInfo, file);
                        documentInfo.FileName = apiResult.Entity.FileName;
                        if (!documentName && docsData.length == 0) {
                            setDocumentName(apiResult.Entity.OriginalFileName.split('.')[0])
                        }
                        documentInfo.OriginalFileName = apiResult.Entity.OriginalFileName;
                        documentInfo.DocumentSource = 'upload-nonpdf';
                        documentInfo.document = config.serverUrl + 'adocuments/download/' + apiResult.Entity.FileName,
                            documentInfo.documentName = apiResult.Entity.OriginalFileName
                        documentInfo.isMoreOption = false;
                        if (replaceIndex || replaceIndex == 0) {
                            documentInfo.Position = replaceIndex + 1
                            const existingData = { ...docsData[replaceIndex] }
                            if (existingData.ADocumentSplitId !== -1 || !existingData.ADocumentSplitId) {
                                existingData.IsDelete = true;
                                docsData.push(existingData)
                            }
                            docsData[replaceIndex] = documentInfo
                            await setDocumentFileArray([...docsData])
                            await getProperAddDocumentPosition()
                        } else {
                            await setDocumentFileArray(oldArray => [...oldArray, documentInfo])
                            // documentFileArray.push(documentInfo)
                        }
                        removeSessionItem('replaceIndex')

                    }
                    nonDeleteDocsData = docsData.filter(e => e.IsDelete == false)
                }
                setIsUpdateChangesHappen(true)
                resolve(true)
                
            }
        })

    }

    function getProperAddDocumentPosition() {
        setIsUpdateChangesHappen(true)
        if (docsData && docsData.length > 0) {
            const nonDeleteDoc = []
            const deleteDoc = []
            for (let index = 0; index < docsData.length; index++) {
                if (docsData[index].IsDelete == 1 || docsData[index].IsDelete) {
                    deleteDoc.push(docsData[index])
                } else {
                    nonDeleteDoc.push(docsData[index])
                    nonDeleteDoc[nonDeleteDoc.length - 1].Position = nonDeleteDoc.length
                }
            }
            for (let index = 0; index < nonDeleteDoc.length; index++) {
                nonDeleteDoc[index].Position = index + 1
            }
            nonDeleteDocsData = nonDeleteDoc;
            const totaldocsData = [...nonDeleteDoc, ...deleteDoc]
            setDocumentFileArray([...totaldocsData])
        }
    }
    /**************** STEP ONE DOCUMENT CODE ***********/
    //Documents Dropdown
    function setp1Dropdown(event) {
        var srcElement = document.getElementById(event);
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    const options = addDocStep2Options
    /****** on Document load it will be atomatically loads */
    function onDocumentLoadSuccess(e) {
        selectedPageNumber ? setselectedPageNumber(selectedPageNumber) : setselectedPageNumber(1)
    }
    function subDocumentsLoadSuccess(e, index) {
        documentFileArray[index].Pages = e.numPages
        setDocumentFileArray([...documentFileArray])
        Ccount.current = Ccount.current + 1;
        console.log(Ccount , "Ccount" , index,"ev" ,e);
        let length = documentFileArray.length
        
        if (length == Ccount.current){
            document.body.classList.remove('loading-indicator');
        }
    }

    function documentPageLoad(e, index) {
        // documentFileArray[index].Pages= e.numPages
        // console.log(documentFileArray, documentFileArray[index])
        // setDocumentFileArray([...documentFileArray])
    }
    function popDocLoad(e) {
        setPopDocPages(e.numPages)
    }
    function setOnMainDoc(pagenumber) {
        setselectedPageNumber(pagenumber)

    }
    /****** To Go to Back dashboard Page  */
    function backToDashboard() {
        usehistory.push('dashboard')
    }
    function addDocumentFile(event) {
        for (let file of event.target.files) {
            getInitialFile(file)
        }

        // setdocument(event.target.files[0])
        // commonService.setDocument(event.target.files[0]);
    }
    function redirectToManage() {
        usehistory.push('/account/manage')

    }
    function firstNext() {
        if (DocumentID || CreateDocumentId) {
            if (isUpdateChangesHappen || IsFromScratchDocument) {
                documentUpadteSubmit()
            } else {
                usehistory.push('/account/documentselect/' + documentUpdateData.ADocumentId)
            }
        } else {
            if (!documentFileArray || documentFileArray.length == 0) {
                alertService.warn('At least one document is required.')
            }
            else if (!documentName || documentName == '') {
                alertService.warn('Document name is required.')
            } else {
                let docName = documentName
                let docTitle = ''
                if (docName.indexOf('.') !== -1) {
                    docTitle = docName.substr(0, docName.lastIndexOf(".")).replace(".", "_");
                } else {
                    docTitle = docName;
                }
                let userObj = commonService.userInfoObj
                let TotalPage = 0
                let finalDocuments = []
                let FinalDataArry = [...documentFileArray]
                // FinalDataArry.sort(data=>data.FileName == '')
                let fileNameDocs = []
                let nonfileNameDocs = []
                for (let index = 0; index < FinalDataArry.length; index++) {
                    if (FinalDataArry[index].FileName) {
                        fileNameDocs.push(FinalDataArry[index])
                    } else {
                        nonfileNameDocs.push(FinalDataArry[index])
                    }
                }

                FinalDataArry = [...nonfileNameDocs, ...fileNameDocs]
                for (let index = 0; index < FinalDataArry.length; index++) {
                    FinalDataArry[index].OriginalFileName = FinalDataArry[index].documentName
                    FinalDataArry[index].UploadedOn = new Date()
                    TotalPage = TotalPage + FinalDataArry[index].Pages
                    if (FinalDataArry[index].DocumentSource == 'upload') {
                        finalDocuments.push(FinalDataArry[index].document)
                    }
                    delete FinalDataArry[index].Document
                    if (FinalDataArry[index].canvasImageUrl) {
                        delete FinalDataArry[index].canvasImageUrl
                    }

                }

                let array = []
                for (let i = 1; i <= TotalPage; i++) {
                    array.push(i)
                }
                userObj["IsSignatory"] = true;
                userObj["SignType"] = "Sign";
                userObj["Initial"] = "TS";
                userObj["Color"] = "att-ad-recipient-pure-blue";
                userObj["RecipientPageNumbers"] = array;
                userObj["StatusCode"] = "DRAFT";
                userObj["IsInPerson"] = 0;
                userObj["Position"] = 1;
                userObj["IsOwner"] = 1;
                let Recipientslist = []
                if (istempdoc === true) {
                    templaterecipients.forEach(data => { data.RecipientPageNumbers = array })
                    Recipientslist = [...templaterecipients]
                }
                else {
                    Recipientslist = [userObj]
                }
                let d = new Date();
                let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
                let timeSplit = time.split(' ');
                let timeZone = timeSplit[1];
                let documentInfo = {
                    "ADocumentId": -1,
                    "Title": docTitle,
                    "IsParallelSigning": true,
                    "Height": 1262.835,
                    "Width": 892.92,
                    "UserId": userObj.user_id,
                    "CompanyId": userObj.company_id,
                    "UploadDocuments": FinalDataArry,
                    "Document": finalDocuments,
                    "Recipients": Recipientslist,
                    "DateTime": moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                    "TimeZone": timeZone,
                    "WorkflowTypeId": 1,
                    "WorkflowCode": "STANDARD"
                }
                addDocumentService.addDocument(documentInfo, finalDocuments).then((apiResult) => {
                    if (apiResult.Status) {
                        setStep2Data(apiResult.Entity.Data)
                        usehistory.push('/account/documentselect/' + apiResult.Entity.Data.ADocumentId)
                    } else {
                        alertService.warn('This PDF document is encrypted and cannot be processed with FPDI. Please convert that file to PDF (like print and save as PDF) and continue.')
                    }

                })
            }
        }
    }

    function documentUpadteSubmit() {
        const nonDeleteDocs = documentFileArray.filter(e => e.IsDelete == false)
        if (!nonDeleteDocs || nonDeleteDocs.length == 0) {
            alertService.warn('At least one document is required.')
        } else if (!documentName || documentName == '') {
            alertService.warn('Document name is required.')
        }
        else {
            let docName = documentName
            let docTitle = ''
            if (docName.indexOf('.') !== -1) {
                docTitle = docName.substr(0, docName.lastIndexOf(".")).replace(".", "_");
            } else {
                docTitle = docName;
            }
            documentUpdateData.Title = docTitle
            let finalDocuments = []
            let finalDocumentObjects = []
            let Documents = []
            let TotalPage = 0
            let FinalDataArry = [...documentFileArray]
            let newDataArray = []
            let ExistArray = []
            for (let index = 0; index < FinalDataArry.length; index++) {
                FinalDataArry[index].OriginalFileName = FinalDataArry[index].documentName
                FinalDataArry[index].UploadedOn = new Date()
                // TotalPage = TotalPage + FinalDataArry[index].Pages
                if (FinalDataArry[index].DocumentSource == 'upload') {
                    finalDocuments.push(FinalDataArry[index].document)
                    FinalDataArry[index].UploadedOn = new Date()
                    Documents.push(FinalDataArry[index])
                }
                if (FinalDataArry[index].DocumentSource == 'upload' || (FinalDataArry[index].ADocumentId && !FinalDataArry[index].IsDelete)) {
                    finalDocumentObjects.push(FinalDataArry[index])
                }
                delete FinalDataArry[index].Document
                if (FinalDataArry[index].canvasImageUrl) {
                    delete FinalDataArry[index].canvasImageUrl
                }
                if (FinalDataArry[index].ADocumentSplitId == -1) {
                    newDataArray.push(FinalDataArry[index])
                } else {
                    ExistArray.push(FinalDataArry[index])
                }

            }
            FinalDataArry = [...newDataArray, ...ExistArray]
            // console.log(finalDocumentObjects, 'finalDocumentsfinalDocumentsfinalDocuments', FinalDataArry)
            for (let index = 0; index < finalDocumentObjects.length; index++) {
                TotalPage = TotalPage + finalDocumentObjects[index].Pages
            }
            let array = []
            let pagesData = ''
            for (let i = 1; i <= TotalPage; i++) {
                if (pagesData) {
                    pagesData = pagesData + ', ' + i
                } else {
                    pagesData = i
                }
                array.push(i)
            }
            // for (let index = 0; index < documentUpdateData.Recipients.length; index++) {
            //     documentUpdateData.Recipients[index].RecipientPageNumbers = array;
            // }
            // for (let index = 0; index < documentUpdateData.CCRecipients.length; index++) {
            //     documentUpdateData.CCRecipients[index].RecipientPageNumbers = array;
            // }
            let d = new Date();
            let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
            let timeSplit = time.split(' ');
            let timeZone = timeSplit[1];
            documentUpdateData.Document = Documents;
            // documentUpdateData.RecipientPageNumbers = pagesData;
            documentUpdateData.RecipientPages = pagesData;
            documentUpdateData.UploadDocuments = FinalDataArry;
            documentUpdateData.DateTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                documentUpdateData.TimeZone = timeZone
            //console.log(documentUpdateData, 'documentUpdateDatadocumentUpdateData')
            // console.log(finalDocuments, 'finalDocuments')
            addDocumentService.updateDocument(documentUpdateData.ADocumentId, documentUpdateData, finalDocuments).then(data => {
                if (data) {
                    usehistory.push('/account/documentselect/' + documentUpdateData.ADocumentId)
                }

            })

        }
    }


    /******************************* STEP ONE CLODE CODE *************************************/
    const options2 = {
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            599: {
                items: 1,
                margin: 0
            },
            1000: {
                items: 1,
                margin: 0
            },
            1280: {
                items: 1,
                margin: 0
            },
        },
    };


    //Share Document Hide Show
    function shareDoc() {
        var srcElement = document.getElementById('sharedoc');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }


    const options3 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };
    function docNameChange(event) {
        setIsUpdateChangesHappen(true)
        setSelectDocumentName(event.target.value)

    }
    function docNameEditClick() {
        editEnable ? setEditEnable(false) : setEditEnable(true)
    }
    function uploadDiv() {
        button.current = true
        var srcElement = document.getElementById('upload');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }


    function getTags() {
        document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
            const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
            // console.log(dropZoneElement.childNodes[0].div, 'dropZoneElementdropZoneElement')

            dropZoneElement.addEventListener("click", (e) => {
                if (e.target.className != 'file-add-text') {
                    inputElement.click();
                }
            });

            inputElement.addEventListener("change", async (e) => {
                if (inputElement.files.length) {
                    for (let inputFile of inputElement.files) {
                        await updateThumbnail(dropZoneElement, inputFile, false);
                    }
                    var inputVal = inputElement.value;
                }
            });
            dropZoneElement.addEventListener("dragover", (e) => {
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.add("drop-zone--over");
            });
            dropZoneElement.addEventListener('dragleave', function (e) {
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.remove("drop-zone--over");
            });

            ["dragleave", "dragend"].forEach((type) => {
                dropZoneElement.addEventListener(type, (e) => {
                });
            });

            dropZoneElement.addEventListener("drop", (e) => {
                if (e.dataTransfer.files.length) {
                    inputElement.files = e.dataTransfer.files;
                    let exist = false
                    if (e.target.className == 'file-add-text' || e.target.className == 'file-add use_templete drop-zone') {
                        exist = true
                    }
                    console.log({ e }, e.dataTransfer.files)
                    for (let inputFile of e.dataTransfer.files) {
                        updateThumbnail(dropZoneElement, inputFile, exist);
                    }
                }
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.remove("drop-zone--over");
            });
        });

    }
    /**
 * Updates the thumbnail on a drop zone element.
 *
 * @param {HTMLElement} dropZoneElement
 * @param {File} file
 * 
 */
    function updateThumbnail(dropZoneElement, file, exist) {
        return new Promise(async (resolve) => {
            let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

            // First time - remove the prompt
            if (dropZoneElement.querySelector(".drop-zone__prompt")) {
                //   dropZoneElement.querySelector(".drop-zone__prompt").remove();
            }

            // First time - there is no thumbnail element, so lets create it
            if (!thumbnailElement) {
                thumbnailElement = document.createElement("div");
                //   thumbnailElement.classList.add("drop-zone__thumb");
                // dropZoneElement.appendChild(thumbnailElement);
            }
            // thumbnailElement.dataset.label = file.name;
            // Show thumbnail for image files
            if (file.type.startsWith("image/")) {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                    thumbnailElement.style.backgroundImage = `url('${reader.result}')`;

                };
            } else {
                thumbnailElement.style.backgroundImage = null;
            }
            await getInitialFile(file)
            // if(!exist) {
            uploaddivClose()
            resolve(true)
        })

        // }
    }
    function uploaddivClose() {
        var srcElement = document.getElementById('upload');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            }
            return false;
        }
    }
    function onDocumentSourceErrorLoad(e) {
        // console.log(e)
    }
    function onDocumentErrorLoad(e, index, data) {
        if (e.code == 1 || e.name == 'PasswordException') {
            alertService.warn('Error uploading ' + '<b>' + data.documentName + '</b>' + ': Unable to load Document.Document is Password protected, please remove password protection before using the document')
        } else {
            alertService.error('Error uploading ' + '<b>' + data.documentName + '</b>' + ': the vaidation of the PDF file failed')
        }
        documentFileArray.splice(index, 1)
        getProperDocumentPosition()
        // setShowErrorPopUp(true)
        // setErrorCount(1)
    }

    const model = (index) => {
        Ccount.current = Ccount.current - 1
        const nonDeleteDocs = documentFileArray.filter(e => e.IsDelete == false)
        if (!DocumentID) {
            setSelectDocumentIndex(index)
            setDocmodalShow(true)
        } else if (DocumentID && nonDeleteDocs.length > 1) {
            setSelectDocumentIndex(index)
            setDocmodalShow(true)
        } else {
            alertService.warn('At least one document is required!')
        }
    }
    const moreUploadOption = (e) => {
        e.preventDefault();
        return (
            setUploadOption(!uploadOption)
        )
    }
    const onPdfView = (data) => {
        setviewPdf(true)
        setSelectDocumentFile(data.document)
        setSelectDocumentName(data.documentName)
        moreOptionDetail(-1)
    }
    const ondocumentRename = (e, data, index) => {
        e.preventDefault();
        setSelectDocumentIndex(index)
        setSelectDocumentName(data.documentName)
        setdocumentRename(true);
        moreOptionDetail(-1)
    }
    function deleteDocument() {
        documentFileArray[selectDocumentIndex].IsDelete = true
        if (documentFileArray[selectDocumentIndex].DocumentSource != 'DocumentExist') {
            documentFileArray.splice(selectDocumentIndex, 1)
        }
        getProperDocumentPosition()
        // documentFileArray.forEach((item, i) => item.Position = i + 1)
        // setDocumentFileArray([...documentFileArray])
        setSelectDocumentIndex('')
        nonDeleteDocsData = docsData.filter(e => e.IsDelete == false)
    }

    function getReplaceIndex(e, index) {
        e.preventDefault();
        setSessionItem('replaceIndex', index)
        uploadDiv()
        moreOptionDetail(index)
    }

    function getProperDocumentPosition() {
        setIsUpdateChangesHappen(true)
        if (documentFileArray && documentFileArray.length > 0) {
            const nonDeleteDoc = []
            const deleteDoc = []
            for (let index = 0; index < documentFileArray.length; index++) {
                if (documentFileArray[index].IsDelete == 1 || documentFileArray[index].IsDelete) {
                    deleteDoc.push(documentFileArray[index])
                } else {
                    nonDeleteDoc.push(documentFileArray[index])
                    nonDeleteDoc[nonDeleteDoc.length - 1].Position = nonDeleteDoc.length
                }
            }
            for (let index = 0; index < nonDeleteDoc.length; index++) {
                nonDeleteDoc[index].Position = index + 1
            }
            nonDeleteDocsData = nonDeleteDoc;
            const totalDocumentFileArray = [...nonDeleteDoc, ...deleteDoc]
            setDocumentFileArray([...totalDocumentFileArray])
        } else {
            setDocumentFileArray([...documentFileArray])
        }
    }
    function documentTitleChange(event) {
        setIsUpdateChangesHappen(true)
        setDocumentName(event.target.value)
    }
    const sortDocuments = (dragIndex, hoverIndex) => {
        if (dragIndex && hoverIndex || (dragIndex == 0 || hoverIndex == 0)) {
            const items = [...documentFileArray];
            const [reorderedItem] = items.splice(dragIndex, 1);
            items.splice(hoverIndex, 0, reorderedItem);
            items.forEach((data, index) => {
                data.IsDocPositionChanged = 1
                data.Position = index + 1
            })
            setDocumentFileArray([...items])
            setIsUpdateChangesHappen(true)
        }
    }

    function downloadDocument(event, data, index) {
        if (data.DocumentSource == 'upload-nonpdf' || data.DocumentSource == 'DocumentExist') {
            downloadNonPDFDocument(data)
        } else {
            const name = data.documentName.split('.pdf')[0] + '.pdf'
            var blob = new Blob([data.document]);
            const element = document.createElement('a');
            element.href = URL.createObjectURL(blob);
            element.download = name;
            document.body.appendChild(element);
            element.click();
        }
        moreOptionDetail(index)

    }
    function downloadNonPDFDocument(data) {
        let FileName = data.FileName
        if (data.OriginalFileName) {
            FileName = data.OriginalFileName.split('.pdf')[0] + '.pdf'
        }
        addDocumentService.getDocumentFile(data.FileName).then((apiResult) => {
            const blob = new Blob([apiResult], { type: 'application/octet-stream' });
            const element = document.createElement('a');
            element.href = URL.createObjectURL(blob);
            element.download = FileName;
            document.body.appendChild(element);
            element.click();

        })

    }
    function applyTemplate(event, data, index) {
        console.log(data, 'applyTemplate')

    }
    const moreOptionDetail = (i) => {
        for (let index = 0; index < documentFileArray.length; index++) {
            if (index == i) {
                documentFileArray[index].isMoreOption = documentFileArray[index].isMoreOption ? false : true
            } else {
                documentFileArray[index].isMoreOption = false
            }
            // if (documentFileArray[index].IsDelete) {
            //     documentFileArray.splice(index, 1)
            // }

        }

        setDocumentFileArray([...documentFileArray])
    }

    const getOnPageLoadSuccess = (event, index, data, pageimage) => {
        if (!documentFileArray[index].canvasImageUrl && pageimage) {
            documentFileArray[index].canvasImageUrl = pageimage
        }

    }
    function loadPassWord(callback, index, data) {
        console.log({ callback, index, data })

    }
    const Exporttodrive = () => {
        setChildArr([])
        setGoodrivedata([])
        addDocumentService.getgoogledrivedata().then((result) => {
            if (result && result.Entity) {
                console.log(result, 'googleData')
                result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
                setGoodrivedata(result?.Entity)
            }
        }).catch({
        })
        setExporttitle("Google Drive")
        setshowdrivemodel(true)
    }
    const Exporttodropboxdrive = () => {
        setGoodrivedata([])
        setChildArr([])
        addDocumentService.getdropboxdrivedata().then((result) => {
            if (result && result.Entity) {
                result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
                setGoodrivedata(result?.Entity)
            }
        }).catch({
        })
        setExporttitle("Drop Box")
        setshowdrivemodel(true)
    }
    const Exporttoonedrivedrive = () => {
        setGoodrivedata([])
        setChildArr([])
        addDocumentService.getonedrivedata().then((result) => {
            if (result && result.Entity) {
                result.Entity.sort((firstData, secondData) => secondData.IsFolder - firstData.IsFolder)
                setGoodrivedata(result?.Entity)
            }
        }).catch({
        })
        setExporttitle("One Drive")
        setshowdrivemodel(true)
    }
    function getUserInfo() {
        return commonService.userInfo(true).then((userData) => {
            if (userData && userData.Data) {
                setUserInfo(userData.Data)
            }
        });
    }

    const getchilddata = (item) => {
        if (item.IsFolder && item.children && item.children.length == 0) {
            addDocumentService.getgoogledrivechilddata(item.Id).then((result) => {
                if (result?.Entity?.length > 0) {
                    item.children = result.Entity;
                    item.open = item.open ? false : true
                    setGoodrivedata([...Goodrivedata])
                }
            })
        } else {
            item.open = item.open ? false : true
            setGoodrivedata([...Goodrivedata])
           
        }
    }
    const getdropboxchilddata = (item) => {
        if (item.IsFolder && item.children && item.children.length == 0) {
            addDocumentService.getdropboxdrivechilddata(item.Id).then((result) => {
                if (result?.Entity?.length > 0) {
                    item.children = result.Entity;
                    item.open = item.open ? false : true
                    setGoodrivedata([...Goodrivedata])
                }
            })
        } else {
            item.open = item.open ? false : true
            setGoodrivedata([...Goodrivedata])
        }
    }
    const checkBoxHandler = (value, item) => {
        if (value) {
            setChildArr([...childArr, item])
        }
        else {
            let index = childArr.indexOf(e => e.Id == item.Id)
            console.log(index)
            childArr.splice(index, 1);
            setChildArr([...childArr])
        }
    }
    const getonedrivechilddata = (item) => {
        if (item.IsFolder && item.children && item.children.length == 0) {
            addDocumentService.getonedrivechilddata(item.Id).then((result) => {
                if (result?.Entity?.length > 0) {
                    item.children = result.Entity;
                    item.open = item.open ? false : true
                    setGoodrivedata([...Goodrivedata])
                }
            })
        } else {
            item.open = item.open ? false : true
            setGoodrivedata([...Goodrivedata])
        }
    }

    const getDataById = (roleName, documentInfo, result) => {
        let replaceIndex = JSON.parse(getSessionItem('replaceIndex'));

        documentInfo.FileName = result.Entity;
        if (!documentName && docsData.length == 0) {
            setDocumentName(roleName.split('.')[0])
        }
        documentInfo.OriginalFileName = roleName;
        documentInfo.DocumentSource = 'upload-nonpdf';
        documentInfo.document = config.serverUrl + 'adocuments/download/' + result.Entity,
            documentInfo.documentName = roleName
        documentInfo.isMoreOption = false;
        if (replaceIndex || replaceIndex == 0) {
            documentInfo.Position = replaceIndex + 1
            const existingData = { ...docsData[replaceIndex] }
            if (existingData.ADocumentSplitId !== -1 || !existingData.ADocumentSplitId) {
                existingData.IsDelete = true;
                docsData.push(existingData)
            }
            docsData[replaceIndex] = documentInfo
            setDocumentFileArray([...docsData])
            removeSessionItem('replaceIndex')
        } else {
            setDocumentFileArray(oldArray => [...oldArray, documentInfo])
            // documentFileArray.push(documentInfo)
        }
        removeSessionItem('replaceIndex')
        getProperAddDocumentPosition()

    }

    const getGoogleDocById = (Id, roleName, documentInfo,) => {
        addDocumentService.getGoogleDocDataById(Id, roleName).then((result) => {
            console.log(result)
            getDataById(roleName, documentInfo, result)
        })
        nonDeleteDocsData = docsData.filter(e => e.IsDelete == false)
    }

    const getDropBoxDocById = (Id, roleName, documentInfo) => {
        addDocumentService.getDropBoxDocDataById(Id, roleName).then((result) => {
            console.log(result)
            getDataById(roleName, documentInfo, result)
        });
        nonDeleteDocsData = docsData.filter(e => e.IsDelete == false)
    }
    const getOneDriveDocById = (Id, roleName, documentInfo) => {
        addDocumentService.getOneDriveDocDataById(Id, roleName).then((result) => {
            console.log(result)
            getDataById(roleName, documentInfo, result)
        });
        nonDeleteDocsData = docsData.filter(e => e.IsDelete == false)
    }

    const insertHandler = (type, data) => {
        let cloudDocsData = [];
        let cloudType = '';
        if (data) {
            cloudDocsData = [...data];
            cloudType = type;
        } else {
            cloudDocsData = [...childArr];
            cloudType = Exporttitle;
        }
        cloudDocsData.forEach(e => {
            const nonDeleteDocs = docsData.filter(e => e.IsDelete == false)
            let position = nonDeleteDocs.length + 1
            var documentInfo = {
                "error": false,
                "UploadedOn": new Date,
                "IsSelected": true,
                "Position": position,
                "Height": 1262.835,
                "Width": 892.92,
                "isFileConvertedToPdf": true,
                "DocumentSource": "upload",
                "Recipients": [],
                "IsDelete": false,
                "OriginalFileName": e.roleName,
                "FileName": e.roleName,
                "ADocumentSplitId": -1,
                "Pages": 0,
                process: true
            }
            if (cloudType == "Google Drive") {
                getGoogleDocById(e.Id, e.roleName, documentInfo)
            }
            if (cloudType == "Drop Box") {
                getDropBoxDocById(e.Id, e.roleName, documentInfo)
            }
            if (cloudType == "One Drive") {
                getOneDriveDocById(e.Id, e.roleName, documentInfo)
            }
        })
        setIsUpdateChangesHappen(true)
        setshowdrivemodel(false)
        uploaddivClose()

    }
    const loadSpinner = () => {
        return <div style={{ display: "flex", alignContent: 'center', alignItems: "center", justifyContent: "center", marginTop: '30%' }}>
            <Spinner variant="primary" animation="border" /></div>
    }
    const components = [
        <div className='wizard-panel active' id="wizard-pane-1" >
            <div className='content'>
                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                        <div id="firstTab" className="">
                            <div className='title-flex'>
                                {(!documentFileArray || documentFileArray.length <= 0) ? <h3 >Please Add Your Document
                                    {/* <span className='tooltip-info'><img src="src/images/info.svg" alt='' />
                                    <div className="tooltip-des">
                                        <div className="tooltip-details">
                                            A chief executive officer (CEO) or just chief executive (CE), is the most senior corporate, executive, or administrative officer in charge of managing an organization – especially
                                            an independent legal entity such as a company.
                                        </div>
                                    </div>
                                </span> */}
                                </h3> : null}
                                {(documentFileArray && documentFileArray.length > 0) ? <div className="edit-tooltip-adddoc" style={{ display: 'flex', width: '500px', alignItems: "baseline" }}>

                                    {!editEnable ?
                                        <div>
                                            <h3 className='overflowtestdots adjust-title' data-tip={documentName} data-for="nameOfFile">{documentName}</h3>
                                            <ReactTooltip id="nameOfFile" place="top" effect="solid">
                                                {documentName}
                                            </ReactTooltip>

                                        </div> :
                                        <input type="text" id="docname" className='docinput docinput-title' name="docName" value={documentName} onChange={documentTitleChange} />
                                    }
                                    <div className='editicon' style={{ display: 'flex' }}>
                                        <>
                                            <img src="src/images/edit_icon.svg" data-tip="Edit-Icon" data-for="Edit-Icon" alt="" onClick={(event) => { docNameEditClick() }} />
                                            <ReactTooltip id="Edit-Icon" place="top" effect="solid">
                                                Edit
                                            </ReactTooltip>
                                        </>
                                        {(!DocumentID && documentFileArray && documentFileArray.length > 0) ?
                                            <>
                                                {/* <img style={{marginLeft: '2rem'}} src="src/images/Cleaner.svg" data-tip="Cleaner-Icon" data-for="Cleaner-Icon"  alt="" onClick={()=>{setDocumentFileArray([]);nonDeleteDocsData = []; setChildArr([])}} />
                                      <ReactTooltip id="Cleaner-Icon" place="top" effect="solid">
                                            Clear All
                                       </ReactTooltip> */}
                                            </>
                                            //  <button className="btn next-btn " style={{marginRight: '1rem', background: 'rgb(36, 99, 209)'}} onClick={()=>{setDocumentFileArray([]);nonDeleteDocsData = []
                                            // }}>Clear All</button>
                                            : null}
                                    </div>
                                </div> : null}
                                <h3 className='doc-total-adjust' >Total Documents: {(nonDeleteDocsData && nonDeleteDocsData.length > 0) ? nonDeleteDocsData.length : 0}
                                </h3>
                            </div>
                            <div style={{ border: '1px solid #babec2' }} className='card-height'>
                                <div className='row wizard-form adjust-row' style={{ margin: 'auto' }}>
                                    <div className="col-3" >
                                        <div className='file-add file-upload use_templete drop-zone' onClick={moreOptionDetail}>
                                            <div className="drop-zone__prompt">
                                                <input type="file" onClick={(event) => { event.preventDefault(); event.target.value = null }} multiple accept=".doc,.docx,image/png,image/jpg,image/jpeg,.xls,.xlsx,.pdf" name="myFile" className="drop-zone__input dragUpload" />
                                            </div>
                                            <div className="file-add-img-wrapper">
                                                <img src="./src/images/file-add.svg" alt="file-add" className='file-add-img' />
                                            </div>
                                            <p className='file-add-text'>Drop your files here or</p>
                                            <div className="file-add-btn-container">
                                                <button className='file-add-btn' onClick={(e) => { uploadDiv() }}>upload
                                                    {/* <input type="file" className='file-add-input' /> */}
                                                    <span className='file-add-btn-arr'><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                                                        <path strokeLinecap="round" strokeLinejoin="round" d="M5 10l7-7m0 0l7 7m-7-7v18" />
                                                    </svg>
                                                    </span>
                                                </button>
                                                {
                                                    uploadOption ?
                                                        <ul className='more-upload-options' ref={wrapperRef1}>
                                                            <li className='more-upload-item'><a href="" onClick={(e) => { e.preventDefault(); uploadDiv() }} className='more-upload-link'><span className="link-svg"><svg className="" fill="#868686" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" focusable="false">
                                                                <path d="M2 2v9h12V2H2zm8 11l.92 2.08c.23.51-.05.92-.62.92H5.7c-.57 0-.84-.42-.62-.92L6 13H1.6A1.6 1.6 0 010 11.38V1.62C0 .72.72 0 1.6 0h12.8c.89 0 1.6.73 1.6 1.62v9.76c0 .9-.72 1.62-1.6 1.62H10z"></path>
                                                            </svg></span> Desktop</a>
                                                            </li>
                                                            <li className='more-upload-item'><a href="/" className='more-upload-link'><span className="link-svg"><img src="./src/images/Box-gray.png" alt="" /></span> Box</a></li>
                                                            <li className='more-upload-item'><a href="/" className='more-upload-link'><span className="link-svg"><img src="./src/images/Dropbox-gray.png" alt="" /></span> Dropbox</a></li>
                                                            <li className='more-upload-item'><a href="/" className='more-upload-link'><span className="link-svg"><img src="./src/images/GoogleDrive-gray.png" alt="" /></span> Google Drive</a></li>
                                                            <li className='more-upload-item'><a href="/" className='more-upload-link'><span className="link-svg"><img src="./src/images/OneDrive-gray.png" alt="" /></span> OneDrive</a></li>
                                                            <li className='more-upload-item'><a href="/" className='more-upload-link'><span className="link-svg"><svg className="" fill="#868686" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" focusable="false">
                                                                <path d="M1 1.994A2 2 0 013.006 0h9.988C14.102 0 15 .895 15 1.994v12.012A2 2 0 0112.994 16H3.006A2.001 2.001 0 011 14.006V1.994zM3 2v3h10V2H3zm0 5v7h3V7H3zm5 0v7h5V7H8z"></path>
                                                            </svg></span> Use a template</a>
                                                            </li>
                                                        </ul>
                                                        : null}
                                            </div>
                                        </div>
                                    </div>
                                    <DndProvider backend={HTML5Backend}>

                                        {
                                            documentFileArray && documentFileArray.length > 0 ?
                                                documentFileArray.map((data, index) => {
                                                    return <>
                                                        {!data.IsDelete ? <DocumentCard data={data} index={index} id={index + data?.documentName} model={model}
                                                            subDocumentsLoadSuccess={subDocumentsLoadSuccess}
                                                            onDocumentErrorLoad={onDocumentErrorLoad}
                                                            onDocumentSourceErrorLoad={onDocumentSourceErrorLoad}
                                                            documentPageLoad={documentPageLoad}
                                                            onPdfView={onPdfView}
                                                            moreOptionDetail={moreOptionDetail}
                                                            getReplaceIndex={getReplaceIndex}
                                                            downloadDocument={downloadDocument}
                                                            applyTemplate={applyTemplate}
                                                            ondocumentRename={ondocumentRename}
                                                            moveCard={sortDocuments}
                                                            //    handleBlurEvent={handleBlurEvent}
                                                            onPageLoadSuccess={getOnPageLoadSuccess}
                                                            onPassword={loadPassWord}
                                                            key={index + data?.documentName}
                                                        ></DocumentCard> : null}

                                                    </>
                                                })
                                                : null}
                                    </DndProvider>


                                    {/* <div><ExamplePDFViewer /></div> */}

                                </div>
                            </div>

                        </div>

                        {/* <div id="secondTab" className="tabcontent"></div>
                        <div id="thirdTab" className="tabcontent"></div>
                        <div id="fourthTab" className="tabcontent"></div>
                        <div id="fifthTab" className="tabcontent"></div> */}
                    </div>
                </div>
            </div>
        </div>

    ]
    const errorComponents = [
        <div>
            <p>Only the following file formats are acceptable: .doc, .docx, .png, .jpg, .jpeg, .ppt, .pptx, .xls, .xlsx, .pdf</p>
        </div>,
        <div>
            <p>Invalid pdf structure.</p>
        </div>,

        <div>
            <p>This PDF document is encrypted and cannot be processed with FPDI. Please convert that file to PDF (like print and save as PDF) and continue.</p>
        </div>
    ]

    //Add New Recipient Modal
    function recipient() {
        var srcElement = document.getElementById('new_recipient');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }

    function openTab(event, divID) {
        // if (divID == 'firstTab') {
        // inputFileRef.current.click();
        // }
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    return (
        <>

            <div className="dashboard_home wrapper_other document_add" style={{ overflow: 'hidden' }} id="wrapper_other">
                <Sidebar />
                <div className="main other_main scroll" id="main">
                    <Topbar />
                    <div className="main-content wizard">
                        <div className='row'>
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div id="wizard_step">
                                    <div className="step-wizard" role="navigation">
                                        <ul>
                                            <li className={count === 0 || count < 4 ? 'active done' : ''}>
                                                <button>
                                                    <div className="step">1</div>
                                                    <div className="title">Add</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 1 || (count < 4 && count != 0) ? 'active done' : ''}>
                                                <button id="step2">
                                                    <div className="step">2</div>
                                                    <div className="title">Select</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 2 || (count < 4 && count != 1 && count != 0) ? 'active done' : ''}>
                                                <button id="step3">
                                                    <div className="step">3</div>
                                                    <div className="title">Prepare</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 3 ? 'active done' : ''}>
                                                <button id="step4">
                                                    <div className="step">4</div>
                                                    <div className="title">Review</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className='wizard-form'>
                                        {components[count]}
                                        <div className="upload_sec popup_ht" id="upload" style={{ display: "none" }}>
                                            <div className="content">
                                                <div className='pop_close'>
                                                    <h4>{t('UPLOAD_YOUR_FILES')}
                                                        <Tooltip placement='top' title='Supported File Formats: .PDF,.DOC,.DOCX,.XLS,.XLSX,.JPG,.PNG,.JPEG'>
                                                            <img className='ml-1' style={{ width: 10, height: 10, marginBottom: 2, backgroundcolor: "blue" }} src="src/images/info-circle.svg" alt="info icon" />
                                                        </Tooltip>
                                                    </h4>
                                                    <CloseIcon className="close_add_doc_pop" onClick={() => { uploadDiv(), removeSessionItem('replaceIndex') }} />
                                                </div>
                                                <h5>{t('USE_TEMPLATE_THIRD_PARTY')}</h5>
                                                <div className="upload_block">
                                                    {/* <div className="use_templete" onClick={()=>{usehistory.push('/account/template')}}>
                                                        <img src="src/images/folder.svg" alt='' />
                                                        <h3>{t('USE_TEMPLATE')}</h3>
                                                    </div> */}
                                                    <div className="use_templete" onClick={() => { if (userInfo.IsGoogleDriveEnabled) { Exporttodrive() } }}>
                                                        <div style={{ display: "flex", justifyContent: "space-between" }}>
                                                            <img src="src/images/google_drive.svg" alt='' />
                                                            <div data-tip data-for='google_drive'>
                                                                {userInfo.IsGoogleDriveEnabled ?
                                                                    <DoneIcon style={{ color: "green" }} onClick={(event) => { event.stopPropagation(); }} />
                                                                    : <CloseIcon style={{ color: "red" }} />
                                                                }
                                                                <ReactTooltip id="google_drive" place="top" effect="solid">
                                                                    {userInfo.IsGoogleDriveEnabled ? <>Connected</> : <>Disconnected</>}</ReactTooltip>
                                                            </div>
                                                        </div>
                                                        <h3>{t('GOOGLE_DRIVE')}</h3>
                                                        {/* <h4>1,357</h4> */}
                                                    </div>
                                                    <div className="use_templete" onClick={() => { if (userInfo.IsOneDriveEnabled) { Exporttoonedrivedrive() } }}>
                                                        <div style={{ display: "flex", justifyContent: "space-between" }}>
                                                            <img src="src/images/one_drive.svg" alt='' />
                                                            <div data-tip data-for='one_drive'>
                                                                {userInfo.IsOneDriveEnabled ? <DoneIcon onClick={(event) => { event.stopPropagation(); }} style={{ color: "green" }} /> : <CloseIcon style={{ color: "red" }} />}
                                                                <ReactTooltip id="one_drive" place="top" effect="solid">
                                                                    {userInfo.IsOneDriveEnabled ? <>Connected</> : <>Disconnected</>}</ReactTooltip>
                                                            </div>
                                                        </div>
                                                        <h3>{t('ONE_DRIVE')}</h3>
                                                        {/* <h4>2,14777 Files</h4> */}
                                                    </div>
                                                    <div className="use_templete" onClick={() => { if (userInfo.IsDropBoxEnabled) { Exporttodropboxdrive() } }}>
                                                        <div style={{ display: "flex", justifyContent: "space-between" }}>
                                                            <img src="src/images/drop_box.svg" alt='' />
                                                            <div data-tip data-for='drop_box'>
                                                                {userInfo.IsDropBoxEnabled ? <DoneIcon onClick={(event) => { event.stopPropagation(); }} style={{ color: "green" }} /> : <CloseIcon style={{ color: "red" }} />}
                                                                <ReactTooltip id="drop_box" place="top" effect="solid">
                                                                    {userInfo.IsDropBoxEnabled ? <>Connected</> : <>Disconnected</>}</ReactTooltip>
                                                            </div>
                                                        </div>
                                                        <h3>{t('DROP_BOX')}</h3>
                                                        {/* <h4>1,457 Files</h4> */}
                                                    </div>

                                                    <div className="use_templete drop-zone card_design">
                                                        <div style={{ display: "flex", justifyContent: "space-between" }}>
                                                            <img src="src/images/folder_1.svg" alt='' />
                                                            <div>
                                                                <input type="file" onClick={(event) => { event.target.value = null }} multiple accept=".doc,.docx,image/png,image/jpg,image/jpeg,.xls,.xlsx,.pdf" name="myFile" className="drop-zone__input" />
                                                            </div>
                                                        </div>
                                                        <h3 className="drop-zone__prompt font">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h3>
                                                    </div>
                                                    {/* <div className="use_templete drop-zone">
                                                        <img src="src/images/folder_1.svg" alt='' className="img-fluid mx-auto d-block" />
                                                        <h4 className="drop-zone__prompt">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h4>
                                                        <input type="file" onClick={(event) => { event.target.value=null }} multiple name="myFile" accept=".doc,.docx,image/png,image/jpg,image/jpeg,.ppt,.pptx,.xls,.xlsx,.pdf" className="drop-zone__input" />
                                                    </div> */}
                                                </div>
                                                {/* <div className="back" onClick={() => { uploadDiv(), removeSessionItem('replaceIndex') }}>
                                                    <h6 className="fileback"><span ><img src="src/images/back.svg" alt='' /></span>{t('BACK')}</h6>
                                                </div> */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className={DocumentID ? 'slide-tab-btn content_end lg:bottom-0 scroll-btn' : 'slide-tab-btn lg:bottom-0 scroll-btn'} style={{ backgroundColor: 'white' }}>
                                            {!DocumentID ? <button className="btn prev-btn" onClick={function (event) { backToDashboard(); event.preventDefault() }}>BACK</button> : null}
                                            {(!DocumentID && documentFileArray && documentFileArray.length > 0) ? <button className="btn next-btn " style={{ marginRight: '1rem', background: 'rgb(36, 99, 209)' }} onClick={() => {
                                                setDocumentFileArray([]);Ccount.current=0; nonDeleteDocsData  = []; setChildArr([])
                                            }}>CLEAR ALL</button> : null}
                                            {/* <button className="btn prev-btn" onClick={() => { redirectToManage() }}>Cancel</button> */}
                                            <button className="btn next-btn" onClick={() => { firstNext() }}>NEXT</button>
                                            {/* <div className='draft_btn'> */}


                                            {/* <button className="btn draf-btn">Draft</button> */}
                                            {/* <button className="btn cancel-btn" onClick={() => { redirectToManage() }}>Cancel</button> */}
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {/* -------- Contacts PopUp Start--------------*/}

                <Modal show={showErrorPopUp} onHide={() => { setShowErrorPopUp(false) }}
                    size=""
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header className='modal-header-border-none' closeButton>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        {errorComponents[errorCount]}
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-border-none justify-content-center'>
                        <Button className="button_modal_ok" onClick={() => { setShowErrorPopUp(false) }}>OKAY</Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={documentRename} onHide={() => {
                    setdocumentRename(false); setSelectDocumentIndex('');
                    setSelectDocumentName('')
                }}
                    size=""
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p>Rename</p>
                        </Modal.Title>
                        <button type="button" className="close" onClick={() => {
                            setdocumentRename(false), setSelectDocumentIndex(''),
                                setSelectDocumentName('')
                        }} >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <label htmlFor="docName">Name <span style={{ color: 'red' }}>*</span> </label>
                        <input type="text" id="docName" className='docinput' name="docName" value={selectDocumentName} onChange={docNameChange} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => {
                            documentFileArray[selectDocumentIndex].documentName = selectDocumentName;
                            documentFileArray[selectDocumentIndex].OriginalFileName = selectDocumentName;
                            // documentFileArray[selectDocumentIndex].document.name = selectDocumentName;
                            setSelectDocumentIndex('');
                            setSelectDocumentName('');
                            setdocumentRename(false);
                        }}>RENAME</Button>
                        <Button variant="secondary" onClick={() => {
                            setdocumentRename(false), setSelectDocumentIndex(''),
                                setSelectDocumentName('')
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={viewPdf} onHide={() => { setviewPdf(false) }}
                    size="lg"
                    dialogClassName="my-docpop"
                    aria-labelledby="example-modal-sizes-title-lg"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                            {selectDocumentName}
                        </Modal.Title>
                        <FullscreenIcon onClick={() => { handle.enter() }} style={{ cursor: "pointer", marginLeft: '20px' }} />
                    </Modal.Header>
                    <Modal.Body style={{ height: '430px', overflow: 'hidden', paddingBottom: '8rem' }} >
                        <FullScreen handle={handle} >
                            <div className='pop-doc' style={{ maxHeight: handle.active ? "100vh" : null }}>
                                {/* {handle.active?<button className="fullscreen_btn" onClick={()=>{handle.exit()}}>Exit</button>:null} */}
                                <Document className="row"
                                    file={selectDocumentFile}
                                    noData={''}
                                    loading={loadSpinner}
                                    onLoadSuccess={popDocLoad}>
                                    {Array.apply(null, Array(popDocPages))
                                        .map((x, i) => i + 1)
                                        .map(page =>
                                            <>
                                                <Page noData={''} scale={1.5} className="subdocument_intial mx-auto d-block" pageNumber={page} />
                                                <span className='pagenumber-flex'>page {page} of {popDocPages}</span>
                                            </>
                                        )}
                                </Document>
                            </div>
                        </FullScreen>
                    </Modal.Body>
                    {/* <Modal.Footer> */}
                    {/* <Button variant="primary" onClick={() => { setDocmodalShow(false) }}>Submit</Button> */}
                    {/* <Button variant="secondary" onClick={() => { setviewPdf(false) }}>Close</Button>
                    </Modal.Footer> */}
                </Modal>
            </div>
            <Modal show={docmodalShow} onHide={() => { setDocmodalShow(false) }}
                size="lg"
                aria-labelledby="example-modal-sizes-title-lg"
                dialogClassName="delete-docpop"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                        <p>Delete Document</p>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <p className='model-text'>Are you sure you want to delete this document?</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => {
                        setDocmodalShow(false)
                        deleteDocument()
                    }}>YES</Button>
                    <Button variant="secondary" onClick={() => { setDocmodalShow(false) }}>NO</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showdrivemodel}
                onHide={() => { setshowdrivemodel(false); setChildArr([]) }}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                scrollable={true}>
                <Modal.Header  >
                    <Modal.Title id="example-modal-sizes-title-lg">
                        <p> {Exporttitle}</p>
                    </Modal.Title>
                    <button type="button" className="close" style={{ backgroundColor: "#ffff" }} onClick={() => { setshowdrivemodel(false) }}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Modal.Header>
                <Modal.Body className='error-modal-content'>
                    <div>
                        <ul className="list-group">
                            {Goodrivedata.length > 0 ? Goodrivedata.map(item => {
                                return (
                                    item.IsFolder === true ?
                                        <div>
                                            <li key={item.id} className="list-group-item" style={{ backgroundColor: "#e2dbdb", cursor: "pointer", }} onClick={() => Exporttitle === "Google Drive" ? getchilddata(item) : Exporttitle === "Drop Box" ? getdropboxchilddata(item) : Exporttitle === "one Drive" ? getonedrivechilddata(item) : ""}>
                                                <i className="fa fa-folder mr-1" style={{ color: "#2F7FED" }}></i>
                                                <span>{item.roleName}</span> </li>
                                            {item.children.length > 0 && <ul className="list-group" style={{ borderRadius: '0', display: "block", display: 'block', padding: '0.2rem', background: 'rgb(226 219 219)' }}>
                                                {item.children.map(item1 => {
                                                    return (
                                                        <li key={item1.id} className="list-group-item" style={{ cursor: "default" }} >
                                                            <input type="checkbox" style={{ marginRight: "30px", height: "15px", width: "15px" }} onChange={(event) => { checkBoxHandler(event.target.checked, item1) }}></input>
                                                            {item1.roleName}</li>
                                                    )
                                                })}
                                            </ul>}
                                        </div>
                                        :
                                        <li key={item.id} className="list-group-item" >
                                            <input type="checkbox" style={{ marginRight: "30px", height: "15px", width: "15px" }} onChange={(event) => { checkBoxHandler(event.target.checked, item) }}></input>
                                            <span>{item.roleName}</span>
                                        </li>
                                )
                            })
                                : "Drive is Empty"}
                        </ul>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => { setshowdrivemodel(false) }}>CANCEL</Button>
                    <Button variant="primary" onClick={insertHandler}>INSERT</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export { AddDocumentsStep }; 