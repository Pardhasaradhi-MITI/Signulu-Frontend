import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { accountService } from '@/_services';
import { Login } from './Login';
import { Register } from './Register';
import { VerifyEmail } from './VerifyEmail';
import { ForgotPassword } from './ForgotPassword';
import { ResetPassword } from './ResetPassword';
import { UpdatePassword } from './UpdatePassword';
import { Dashboard } from './Dashboard';
import { Template } from './Template';
import { TemplateOther } from './TemplateOther';
import { Manage } from './Manage';
import { ActivateMail } from './ActivationMail';
import { Thanks } from './thanks';
import { MyTemplates } from './MyTemplates';
import { Reports } from './Reports';
import { AddDocumentsStep } from './AddDocumentsStep';
import { StandardPopup } from './StandardPopup';
import { BillingandUsage } from './BillingandUsage';
import { Signatures } from './Signatures';
import { CloudStorage } from './CloudStorage';
import { Users } from './Users';
import { Contacts } from './Contacts';
import { DocumentSelect } from './DocumentSelect';
import { DocumentPrepare } from './DocumentPrepare';
import { DocumentReview } from './DocumentReview';
import { CreateDocument } from './CreateDocument';
import { PersonalInformations } from './Personalinformation';
import { MyAccount } from './MyAccount';
import { TrainingVideos } from './TrainingVideos';
import { AccountProfile } from './AccountProfile';
import { User } from './TrainingVideos';
import { Pricings } from './Pricings';
import { DocumentAgreement } from './DocumentAgreement';
import {DocumentAgreementview}from './DocumentAgreementview';
import { DocumentUnauthApprove } from './DocumentUnauthApprove';
import { DocumentUnauthApprovePageError } from './UnauthApprovePageError';
import Checksum from './Checksum';
import { FacialandPhotoVerification } from './FacialandPhotoVerification';
import { AadhaarInfo } from './AadhaarInfo';
import { Feedback } from './Feedback';
import { Reassignsigner } from './Reassigner';
import { EmailVerification } from './Emailverification';
function Account({ history, match }) {
    const { path } = match;

    useEffect(() => {
        // redirect to home if already logged in
        if (accountService.userValue) {
            history.push('/');
        }
    }, []);

    return (
        <Switch>
        <Route path={`${path}/login`} component={Login} />
        <Route path={`${path}/register`} component={Register} />
        <Route path={`${path}/thanks`} component={Thanks} />
        <Route path={`${path}/aadhaarInfo`} component={AadhaarInfo} />
        <Route path={`${path}/activatemail`} component={ActivateMail} />
        <Route path={`${path}/verify-email`} component={VerifyEmail} />
        <Route path={`${path}/forgot-password`} component={ForgotPassword} />
        <Route path={`${path}/resetpassword`} component={ResetPassword} />
        <Route path={`${path}/UpdatePassword`} component={UpdatePassword} />
        <Route path={`${path}/dashboard`} component={Dashboard} />
        <Route path={`${path}/template`} component={Template} />
        <Route path={`${path}/templateother`} component={TemplateOther} />
        <Route path={`${path}/manage`} component={Manage} />
        <Route path={`${path}/mytemplates`} component={MyTemplates} />
        <Route path={`${path}/reports`} component={Reports} />
        <Route path={`${path}/adddocumentsstep`} component={AddDocumentsStep} />
        <Route path={`${path}/adddocumentsstep/:id`} component={AddDocumentsStep} />
        <Route path={`${path}/documentselect/:id`} component={DocumentSelect} />
        <Route path={`${path}/documentprepare/:id`} component={DocumentPrepare} />
        <Route path={`${path}/documentreview/:id`} component={DocumentReview} />
        <Route path={`${path}/standardpopup`} component={StandardPopup} />
        <Route path={`${path}/createdocument`} component={CreateDocument} />
        <Route path={`${path}/billingandusage`} component={BillingandUsage} />
        <Route path={`${path}/signatures`} component={Signatures} />
        <Route path={`${path}/cloudstorage`} component={CloudStorage} />
        <Route path={`${path}/users`} component={Users} />
        <Route path={`${path}/personalinformation`} component={PersonalInformations} />
        <Route path={`${path}/accountprofile`} component={MyAccount} />
        <Route path={`${path}/contacts`} component={Contacts} />
        <Route path={`${path}/pricing`} component={Pricings} />
        <Route path={`${path}/trainingvideos`} component={TrainingVideos} />
        <Route path={`${path}/userpreferences`} component={AccountProfile} />
        <Route path={`${path}/documentagreement/:id`} component={DocumentAgreement} />
        <Route path={`${path}/documentagreementview/:id`} component={DocumentAgreementview} />
        <Route path={`${path}/unauthapprove`} component={DocumentUnauthApprove} />
        <Route path={`${path}/unautherror`} component={DocumentUnauthApprovePageError} />
        <Route path={`${path}/verify`} component={Checksum} />
        <Route path={`${path}/facialandphotoverification`} component={FacialandPhotoVerification} />
        <Route path={`${path}/feedback`} component={Feedback} />
        <Route path={`${path}/reassignsigner`} component={Reassignsigner} />
        <Route path={`${path}/emailverification`} component={EmailVerification} />
        {/*<Route path={`${path}/newPersonalInformationsss`} component={PersonalInformationsss} />*/}
        {/* <Route path={`${path}/newUserPreferencesss`} component={UserPreferencesss} /> */}

    </Switch>
    );
}

export { Account };