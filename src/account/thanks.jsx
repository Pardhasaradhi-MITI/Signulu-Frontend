


import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import "../css/thanks.css";

function Thanks({ history }) {

    const getLogin = (e) => {
        e.preventDefault();
        history.push("/login");
    }
    useEffect(() => {
        setTimeout(() => {
            history.push("/login");

        },10000);
    }, [])

    return (
        <>
            <div style={{overflowX:"hidden"}}>

                <div className="row coloradd">
                    <div className="column image">
                        <img src="src/images/signulu_black_logo2.svg" alt="" className="img-fluid "/>
                    </div>
                    {/* <div className="column textCenter" >
                        <h3>Signulu</h3>
                    </div> */}
                </div>
                <div className='align'>
                    <div><center>
                        <div>
                            <h5 style={{fontWeight:400}} >You will be redirected to login page in 10 sec.</h5>
                            <br />
                            <div className='card col-md-4 card-align'>
                                <h4>You have been successfully registered</h4>
                            </div>
                            <br />
                            <a className='redirect' onClick={getLogin} href="#"  >Redirect to Home Page</a>
                        </div>
                    </center>
                    </div>
                </div>
            </div>
        </>

    )
}
export { Thanks }; 

