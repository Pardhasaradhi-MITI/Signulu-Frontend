import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { Document, Page, pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
import { commonService } from '../_services/common.service';
import { addDocumentService } from '../_services/adddocument.service';
import { Link, useHistory, useParams} from 'react-router-dom';
import { useEffect, useState} from "react";
import "../css/adddocumentstep1.css";
import { addDocStep2Options } from '../_services/model.service';
import config from 'config';
import { useTranslation } from 'react-i18next';
import { Button, Modal } from 'react-bootstrap';
import Tooltip from "@material-ui/core/Tooltip";
import moment from 'moment';
function AddDocumentsStep({ history, location }) {
    const usehistory = useHistory();
    const [count, setCount] = React.useState(0);
    const [errorCount, setErrorCount] = React.useState(0);
    const [subNumPages, setSubNumPages] = React.useState(0);
    const [documentFile, setdocument] = useState('');
    const [selectedPageNumber, setselectedPageNumber] = useState(1);
    const [step2Data, setStep2Data] = useState({});
    const [uploadDocuments, setUploadDocuments] = useState([]);
    const [documentName, setDocumentName] = useState();
    const [editEnable, setEditEnable] = useState();  
    const [docmodalShow, setDocmodalShow] = useState(false); 
    const [showErrorPopUp, setShowErrorPopUp] = useState(false); 
    const { t, i18n } = useTranslation();
    const [popDocPages, setPopDocPages] = React.useState(1);

    

function handlepopup() {
    setp1Dropdown('dropdown' + 1)
}
useEffect(() => {
    // setdocument(commonService.documentValue)
    getTags()
    getInitialFile(commonService.documentValue)
 }, [])

   /* step2variable */
   const [workflowTypes, setWorkflowTypes] = useState([]);
   const [selectedWorkFlowType, setSelectedWorkFlowType] = useState();
 function getInitialFile(file) {

     if (file) {
        var allowedExtensions =   /(\.doc|\.docx|\.png|\.jpg|\.jpeg|\.ppt|\.pptx|\.xls|\.xlsx|\.pdf)$/i;

        if (!allowedExtensions.exec(file.name)) {
            setShowErrorPopUp(true)
            setErrorCount(0)
        } else {
   let fileExtExtract = file.name.split('.');
   let	fileExtName = ('.' + fileExtExtract[fileExtExtract.length-1]).toLowerCase();
   if (fileExtName == '.pdf') {
           setdocument(file)
           let docObj = [{
            "ADocumentSplitId": -1,
            "Height": 1188,
            "Width": 918,
            "Pages": 0,
            "OriginalFileName": file.name,
            "FileName": '',
            "Position": 1,
            "DocumentSource": 'upload',
            "IsDelete": false
            }]
            setDocumentName(file.name.split('.')[0])
           setUploadDocuments(docObj)
   } else {
    var documentInfo = {
        "Document" : [{
            "error": false,
            "UploadedOn": new Date,
            "IsSelected": true,
            "Position": 1,
            "Height": 1262.835,
            "Width": 892.92,
            "isFileConvertedToPdf": true,
            "DocumentSource": "upload",
            "Recipients": [],
            "IsDelete": false,
            "OriginalFileName": file.name,
            "FileName": file.name,
            "ADocumentSplitId": -1,
            "Pages": 0,
          }]
      }
     commonService.convertFileToPDF(documentInfo, file).then((apiResult) => {
       documentInfo.Document[0].FileName = apiResult.Entity.FileName;
       setDocumentName(apiResult.Entity.OriginalFileName.split('.')[0])
       documentInfo.Document[0].OriginalFileName = apiResult.Entity.OriginalFileName;
       documentInfo.Document[0].DocumentSource = 'upload-nonpdf';
       setdocument(config.serverUrl + 'adocuments/download/' + apiResult.Entity.FileName)
          setUploadDocuments(documentInfo.Document)
               });
   }
}
     }
}
     /**************** STEP ONE DOCUMENT CODE ***********/
    //Documents Dropdown
    function setp1Dropdown(event) {
        var srcElement = document.getElementById(event);
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    const options = addDocStep2Options
/****** on Document load it will be atomatically loads */
    function onDocumentLoadSuccess(e) {
        console.log(selectedPageNumber)
     selectedPageNumber ? setselectedPageNumber(selectedPageNumber) : setselectedPageNumber(1)
    }
    function subDocumentsLoadSuccess(e) {
        console.log(e.numPages)

        setSubNumPages(e.numPages)
    }
    function popDocLoad(e) {
        setPopDocPages(e.numPages)
    }
    function setOnMainDoc(pagenumber) {
        console.log(selectedPageNumber)
        setselectedPageNumber(pagenumber)

    }
    /****** To Go to Back dashboard Page  */
    function backToDashboard() {
        usehistory.push('dashboard')
    }
    function addDocumentFile(event){
        getInitialFile(event.target.files[0])
        // setdocument(event.target.files[0])
        // commonService.setDocument(event.target.files[0]);
    }
  function redirectToManage(){
    usehistory.push('/account/manage')

    }
   function firstNext(count) {
       console.log(uploadDocuments, 'uploadDocumentsuploadDocuments')
let docName = documentName
let docTitle = ''
if (docName.indexOf('.') !== -1) {
    docTitle = docName.substr(0, docName.lastIndexOf(".")).replace(".", "_");
} else {
    docTitle = docName;
}
let userObj = commonService.userInfoObj

let array = []
for (let i = 1; i <= subNumPages; i++) {
    array.push(i)
}
uploadDocuments[0].Pages = subNumPages;
    userObj["IsSignatory"] = true;
    userObj["SignType"] = "Sign";
    userObj["Initial"]= "TS";
    userObj["Color"]= "att-ad-recipient-aqua";
    userObj["RecipientPageNumbers"]= array;
    userObj["StatusCode"]= "DRAFT";
    userObj["IsInPerson"] = 0;
    userObj["Position"] = 1;
    let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
    let documentInfo = {
        "ADocumentId": -1,
        "Title": docTitle,
        "IsParallelSigning": true,
        "Height": 1262.835,
        "Width": 892.92,
        "UserId": userObj.user_id,
        "CompanyId": userObj.company_id,
        "UploadDocuments": uploadDocuments,
        "Document": [
          {
            "error": false,
            "UploadedOn": new Date,
            "IsSelected": true,
            "Position": 1,
            "isFileConvertedToPdf": false,
            "DocumentSource": "upload",
            "Recipients": [],
            "OriginalFileName": docName,
            "Document": {},
            "DocumentProperties": {
              "PageCount": subNumPages,
              "DimPages": subNumPages,
            //   "PagesDimension": [
            //     {
            //       "PageNum": 1,
            //       "Height": 1262.835,
            //       "Width": 892.92
            //     },
            //     {
            //       "PageNum": 2,
            //       "Height": 1262.835,
            //       "Width": 892.92
            //     },
            //     {
            //       "PageNum": 3,
            //       "Height": 1262.835,
            //       "Width": 892.92
            //     },
            //     {
            //       "PageNum": 4,
            //       "Height": 1262.835,
            //       "Width": 892.92
            //     },
            //     {
            //       "PageNum": 5,
            //       "Height": 1262.835,
            //       "Width": 892.92
            //     }
            //   ],
              "PageOffset": 0
            }
          }
        ],
        "Recipients": [
           userObj
        ],
        "DateTime": moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        "TimeZone": timeZone,
        "WorkflowTypeId": 1,
        "WorkflowCode": "STANDARD"
      }
      setSelectedWorkFlowType('STANDARD')
      let fileData = ''
if (uploadDocuments.DocumentSource == 'upload') {
    fileData = documentFile
}
    addDocumentService.addDocument(documentInfo, documentFile).then((apiResult) => {
        if (apiResult.Status) {
            console.log(apiResult.Entity.Data, 'apiResultapiResult')
            setStep2Data(apiResult.Entity.Data)
            usehistory.push('documentselect/'+ apiResult.Entity.Data.ADocumentId)
            secondStepOpen()
        } else {
            setShowErrorPopUp(true);
            setErrorCount(2)
        }

    })
    }

/******************************* STEP ONE CLODE CODE *************************************/
    const options2 = {
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            599: {
                items: 1,
                margin: 0
            },
            1000: {
                items: 1,
                margin: 0
            },
            1280: {
                items: 1,
                margin: 0
            },
        },
    };
    function secondStepOpen(){
        getWorkflowData()
    }

    function getWorkflowData(){
        addDocumentService.getWorkFlowTypes().then((types) => {
            console.log(types.Entity, 'types')
setWorkflowTypes(types.Entity)
        })
    }

  //Share Document Hide Show
  function shareDoc() {
    var srcElement = document.getElementById('sharedoc');
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = "none";
        } else {
            srcElement.style.display = "block";
        }
        return false;
    }
}


    const options3 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };
   function docNameChange(event){
       if (event.target.value)
       setDocumentName(event.target.value)

    }
    function docNameEditClick(){
        editEnable ? setEditEnable(false) : setEditEnable(true)
    } 
    function uploadDiv() {
        var srcElement = document.getElementById('upload');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    

    function  getTags() {
        document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
            const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
         
            dropZoneElement.addEventListener("click", (e) => {
              inputElement.click();
            });
          
            inputElement.addEventListener("change", (e) => {
              if (inputElement.files.length) {
                updateThumbnail(dropZoneElement, inputElement.files[0]);
              }
            });
          
            dropZoneElement.addEventListener("dragover", (e) => {
              e.preventDefault();
              dropZoneElement.classList.add("drop-zone--over");
            });
          
            ["dragleave", "dragend"].forEach((type) => {
              dropZoneElement.addEventListener(type, (e) => {
                dropZoneElement.classList.remove("drop-zone--over");
              });
            });
          
            dropZoneElement.addEventListener("drop", (e) => {
              e.preventDefault();
          
              if (e.dataTransfer.files.length) {
                inputElement.files = e.dataTransfer.files;
                updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
              }
          
              dropZoneElement.classList.remove("drop-zone--over");
            });
          });
        
        }
          /**
           * Updates the thumbnail on a drop zone element.
           *
           * @param {HTMLElement} dropZoneElement
           * @param {File} file
           */
          function updateThumbnail(dropZoneElement, file) {
            let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
          
            // First time - remove the prompt
            if (dropZoneElement.querySelector(".drop-zone__prompt")) {
            //   dropZoneElement.querySelector(".drop-zone__prompt").remove();
            }
          
            // First time - there is no thumbnail element, so lets create it
            if (!thumbnailElement) {
              thumbnailElement = document.createElement("div");
            //   thumbnailElement.classList.add("drop-zone__thumb");
              dropZoneElement.appendChild(thumbnailElement);
            }
            // thumbnailElement.dataset.label = file.name;
            // Show thumbnail for image files
            if (file.type.startsWith("image/")) {
              const reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = () => {
                thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
              };
            } else {
              thumbnailElement.style.backgroundImage = null;
            }
            getInitialFile(file)
            uploadDiv()
          }
         function onDocumentSourceErrorLoad(e) {
console.log(e)
          }
        function onDocumentErrorLoad(e) {
            setShowErrorPopUp(true)
            setErrorCount(1)
            console.log(e, 'onDocumentErrorLoad')

          }

    const components = [
        <div className='wizard-panel active' id="wizard-pane-1" >
            <div className='content'>
                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                        <div className="tab">
                            <button className="tablinks" onClick={uploadDiv} >Add Document

                            {/* <input style={{display:"none"}} id="upload"  type="file"  ref={inputFileRef}
                             onChange={(event)=> { addDocumentFile(event)}}/> */}
                            </button>
                            {/* <button className="tablinks" >
                                <ul className="checkList">
                                    <li>
                                        <div className="form-check">
                                        <div >
                                        <input className="form-check-input" type="checkbox" id="flexCheckDefault" value="" />
                                        </div>
                                            <label className="form-check-label docNameoverflowtestdots doc-name-dispaly" htmlFor="flexCheckDefault">Document 1</label>
                                            <div className="dots" ><img src="src/images/dots_2.svg" alt=""/></div>
                                        </div>
                                    </li>
                                </ul>
                            </button> */}
                            {/* <button className="tablinks" onClick={(e) => openTab(e, 'secondTab')}>
                                <span className="folder"><img src="src/images/folder_small.svg" alt='' /></span>Use Template
                            </button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}>
                                <span className="google_drive"><img src="src/images/google_drive_small.svg" alt='' /></span>Google Drive
                            </button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}>
                                <span className="one_drive"><img src="src/images/one_drive_small.svg" alt='' /></span>One Drive
                            </button>
                            <button className="tablinks" onClick={(e) => openTab(e, 'fifthTab')}>
                                <span className="drop_box"><img src="src/images/drop_box_small.svg" alt='' /></span>Drop Box
                            </button> */}
                            
                        </div>

                        <div id="firstTab" className="tabcontent">
                        {!documentName ? <h3 data-tip="Edit">Please Add Your Document <span className='tooltip-info'><img src="src/images/info.svg" alt='' />
                                <div className="tooltip-des">
                                    <div className="tooltip-details">
                                        A chief executive officer (CEO) or just chief executive (CE), is the most senior corporate, executive, or administrative officer in charge of managing an organization – especially
                                        an independent legal entity such as a company.
                                    </div>
                                </div>
                            </span>
                            </h3> : null}
                            {documentName ? <div  style={{display:'flex'}}> 
                            {/* <h3>Review and select pages for</h3> */}
                               {!editEnable ? <h3 className='overflowtestdots adjust-title' data-tip={documentName}>{documentName}</h3> : 
                               <input type="text" id="docname" className='docinput adjust-title' name="docName" value={documentName} onChange={docNameChange} />
                                     } 
                                    <img src="src/images/edit_icon.svg" data-tip="Edit" className='editicon' alt="" onClick={(event) => {docNameEditClick()}} /></div>
                                : null}
                            <div className='row'>
                                <div className='col-xl-9 col-lg-9 col-md-9 col-sm-12'>
                                        <div className="preview" style={{ padding: "10px" }}>
                                            <div className="dots" onClick={function (event) {
                                                if (documentFile) {
                                                    setDocmodalShow(true)
                                                }
                                            }}>
                                                <img src="src/images/dots.svg" alt='' />
                                            </div>
                                            <div className="content">
                                                <Document 
                                                    file={documentFile}
                                                    onLoadSuccess={onDocumentLoadSuccess}
                                                    onLoadError={onDocumentErrorLoad}
                                                        onSourceError={onDocumentSourceErrorLoad}>
                                                        
                                                    <Page className="mx-auto d-block document_intial" scale={2} noData={'Please select a File'}  pageNumber={selectedPageNumber} />
                                            </Document>
                                            {/* <p>Page {pageNumber} of {numPages}</p> */}
                                            {/* <img src="src/images/larg_doc.svg" alt='' className='mx-auto d-block' /> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                                    {subNumPages ? <span className='total-page-count'>Total Pages: {subNumPages}</span> : null }
                                    <div className="other_docs other_docs1">
                                        <div className="row">
                                        <Document  className="row" 
                                                file={documentFile}
                                                onLoadSuccess={subDocumentsLoadSuccess}>
                                                    {Array.apply(null, Array(subNumPages))
                                                    .map((x, i) => i + 1)
                                                    .map(page => 
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12" >
                                                <div className="block" style={{padding: "0px"}}>
                                                    <div className="dots" onClick={function(event){ setp1Dropdown('dropdown' + page); event.preventDefault()}}>
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div id={'dropdown' + page} className="dropdown" style={{ display: 'none' }}>
                                                        <ul>
                                                            <li>
                                                                <a href="#">
                                                                    Delect <span><img src="src/images/delete_icon.svg" /></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Edit <span><img src="src/images/edit_icon.svg" /></span>
                                                                </a>
                                                            </li>
                                                            <li className="active">
                                                                <a href="#">
                                                                    Replace <span><img src="src/images/replace_icon.svg" /></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <ul className="other">
                                                            <li className="folder">
                                                                <a href="#"><img src="src/images/folder_small.svg" /></a>
                                                            </li>
                                                            <li className="google_drive">
                                                                <a href="#"><img src="src/images/google_drive_small.svg" /></a>
                                                            </li>
                                                            <li className="one_drive">
                                                                <a href="#"><img src="src/images/one_drive_small.svg" /></a>
                                                            </li>
                                                            <li className="drop_box">
                                                                <a href="#"><img src="src/images/drop_box_small.svg" /></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="content" onClick={function(event){ setOnMainDoc(page); event.preventDefault()}}>
                                                    <Page scale={2} className="subdocument_intial mx-auto d-block"  pageNumber={page}  />
                                                        {/* <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" /> */}
                                                    </div>
                                                </div>
                                            </div>
                                            )}
                                            </Document>
                                            {/* <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots">
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots">
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                <div className="block">
                                                    <div className="dots">
                                                        <img src="src/images/dots_2.svg" />
                                                    </div>
                                                    <div className="content">
                                                        <img src="src/images/small_doc.svg" alt="" className="mx-auto d-block" />
                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="slide-tab-btn">
                                    <button className="btn prev-btn" onClick={function(event){ backToDashboard(); event.preventDefault()}}>Back</button>
                                    {count <= 3 && <button className="btn next-btn" onClick={() => {firstNext(count)}}>Next</button>}
                                    <div className='draft_btn'>
                                        <button className="btn draf-btn">Draft</button>
                                        <button className="btn cancel-btn" onClick={() => {redirectToManage()}}>Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div id="secondTab" className="tabcontent"></div>
                        <div id="thirdTab" className="tabcontent"></div>
                        <div id="fourthTab" className="tabcontent"></div>
                        <div id="fifthTab" className="tabcontent"></div> */}
                    </div>
                </div>
            </div>
        </div>,


        <div className='wizard-panel' id="wizard-pane-2" >
            <div className='content'>
                <div className='recipients_content'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <h2>Select recipients to <span>For Tom Hanks</span></h2>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12'>
                            <div className='recipients_slider_cont'>
                                <div className='main_doc'>
                                <Document 
                                                file={documentFile}
                                                onLoadSuccess={onDocumentLoadSuccess}>
                                                <Page className="mx-auto d-block document_intial"  pageNumber={selectedPageNumber} />
                                            </Document>
                                            {/* <p>Page {pageNumber} of {numPages}</p> */}
                                    {/* <img src="src/images/l_document_icon_4.jpg" alt="" className='img-fluid' /> */}
                                </div>
                                <div className='page_num'>Pages: 3/12</div>
                                <div className='carousel-item_mg'>
                                    <Document  className="row" 
                                                file={documentFile}
                                                onLoadSuccess={subDocumentsLoadSuccess}>
                                 <OwlCarousel className='recipients-carousel owl-carousel owl-theme'  responsive={options.responsive} margin={10}  dots={true} nav={true} items={4} responsiveClass={true} navText={["<img src='src/images/left-arrw.svg'>","<img src='src/images/right-arrw.svg'>"]}>

                                                    {Array.apply(null, Array(subNumPages))
                                                    .map((x, i) => i + 1)
                                                    .map(page =>{
                                                          return <div className='item'>
                                                      <Page  className="document_intial"  pageNumber={page}  />
                                                              {/* <img src="src/images/document_icon_4.svg" alt="" /> */}
                                                              </div> } )}
                                                              </OwlCarousel>
                                                              </Document>                                            
                                        {/* <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div> */}
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12'>
                            <div className='recipients_scroll'>
                                <div className='top_sec'>
                                    <div className='workflow'>
                                        <label>Workflow :</label>
                                         <select  onChange={(event) => {setSelectedWorkFlowType(event.target.value)}}  className='hlfinput'>
                                        {
                                           workflowTypes.map(el => <option value={el.WorkflowCode} key={el}> {el.WorkflowName} </option>)
                                          }
                                        </select>
                                    </div>
                                    <div className='page_number'>
                                        <label>Add Page Number :</label>
                                        <label className="switch">
                                            <input type="checkbox" checked={step2Data?.addpagenumber} onChange={(event) => {step2Data.addpagenumber = event.target.checked}}   />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div className='middle_sec'>
                                    <h2>Recipients</h2>
                                    <div className="btn-lg-scet">
                                        <button className="btn">ADD FROM CONTACTS</button>
                                        <button className="btn" onClick={recipient}>Add New Recipient</button>
                                    </div>
                                    <div id="new_recipient" className='new_recipient' style={{ display: 'none' }}>
                                        <div className='recipient_content'>
                                            <div className='top_bar'>
                                                <h2>Add New Recipient </h2>
                                                <button type='button' className='add_btn'>Add</button>
                                            </div>
                                            <div className='main-form'>
                                                <div className='row'>
                                                    <div className='col-lg-6 col-md-6 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="First Name" />
                                                        </div>
                                                    </div>
                                                    <div className='col-lg-6 col-md-6 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="Last Name" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="Email Address" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="Designation" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                        <div className="paging-count d-flex">
                                                            <span className="text">Position: <input type="text" placeholder="2" /></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='content_bottom'>
                                            <div className="frt-tgl">
                                                <div className="item active">
                                                    <div className="form-group">
                                                        {/* <input type="checkbox" id="signatory" checked /> */}
                                                        <label htmlFor="signatory"></label>
                                                    </div>
                                                    <h6>Signatory</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" onChange={(event) => {console.log(event.target.checked)}} checked={false} />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                                <div className="item">
                                                    <h6>Facial Recognition</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" onChange={(event) => {console.log(event.target.checked)}} checked={false} />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                                <div className="item">
                                                    <h6>Photo Verification</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" onChange={(event) => {console.log(event.target.checked)}} checked={false} />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="toggle_scroll">
                                        <div className="sect-btn-item">
                                            <button className="tog-ttl active">NORMAL</button>
                                            <button className="tog-ttl">Secured</button>
                                        </div>
                                        <div className="sect-btn-item">
                                            <button className="tog-ttl">Sequential</button>
                                            <button className="tog-ttl active">PARALLEL</button>
                                        </div>
                                    </div>
                                </div>
                                <div className='bottom_sec'>
                                    {step2Data?.Recipients?.map((data) =>{ console.log(data, 'data')
                                    return (!data.isAdded ? (<div className="srll-item first-srll-item">
                                        <div className="top_wrrp">
                                            <div className="headl">
                                                <h5>{data?.FullName}</h5>
                                                <p className="mail_txt">{data?.EmailAddress}</p>
                                            </div>
                                            <div className="headr">
                                                <div>
                                                    <h6>Signatory</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" checked={data?.IsSignatory} />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="pageing-re d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: {data?.Position}
                                            </div>
                                        </div>
                                    </div>) : (<div className="srll-item first-srll-item">
                                        <div className="top_wrrp top_wrrp_othr">
                                            <div className="headl">
                                                <h5>{data?.FullName}</h5>
                                                <p className="mail_txt">{data?.EmailAddress}</p>
                                            </div>
                                            <div className="asd">
                                                <select className="hlfinput">
                                                    <option>Select</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>

                                                <div className="chos-icon">
                                                    <img src="src/images/delete_icon.svg" alt="" />
                                                    <img src="src/images/edit_icon.svg" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="pageing-re pageing-re_othr d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: {data?.Position}
                                            </div>
                                        </div>
                                        <div className="frt-tgl">
                                            <div className="item">
                                                <h6>Signatory</h6>
                                                <label className="switch">
                                                    <input type="checkbox" checked={data?.IsSignatory} />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Facial Recognition</h6>
                                                <label className="switch">
                                                    <input type="checkbox" checked={data?.IsSignatory}/>
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Photo Verification</h6>
                                                <label className="switch">
                                                    <input type="checkbox"checked={data?.IsSignatory} />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>))})}
                                    {/* <div className="srll-item first-srll-item">
                                        <div className="top_wrrp top_wrrp_othr">
                                            <div className="headl">
                                                <h5>Daniel Farrell</h5>
                                                <p className="mail_txt">info.farrell@gmail.com</p>
                                            </div>
                                            <div className="asd">
                                                <select className="hlfinput">
                                                    <option>Select</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>

                                                <div className="chos-icon">
                                                    <img src="src/images/delete_icon.svg" alt="" />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="pageing-re pageing-re_othr d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: 1
                                            </div>
                                        </div>
                                        <div className="frt-tgl">
                                            <div className="item">
                                                <h6>Signatory</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Facial Recognition</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Photo Verification</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="srll-item first-srll-item">
                                        <div className="top_wrrp top_wrrp_othr">
                                            <div className="headl">
                                                <h5>Channing Tatum</h5>
                                                <p className="mail_txt">hello.tatum@gmail.com</p>
                                            </div>
                                            <div className="asd">
                                                <select className="hlfinput">
                                                    <option>Select</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>

                                                <div className="chos-icon">
                                                    <img src="src/images/delete_icon.svg" alt="" />
                                                    <img src="src/images/edit_icon.svg" alt="" />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="pageing-re pageing-re_othr d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: 1
                                            </div>
                                        </div>
                                        <div className="frt-tgl">
                                            <div className="item">
                                                <h6>Signatory</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Facial Recognition</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Photo Verification</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="slide-tab-btn">
                                {count > 0 && <button className="btn prev-btn" onClick={() => {setCount(count - 1),localStorage.setItem("addDocumentPage", count - 1)}}>Back</button>}
                                {count <= 3 && <button className="btn next-btn" onClick={() => {setCount(count + 1), localStorage.setItem("addDocumentPage", count + 1)}}>Next</button>}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>,


        <div className='wizard-panel' id="wizard-pane-3" >
            <div className='content'>
                <div className='prepare_document'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <h2>Prepare document <span>For Tom Hanks</span></h2>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-3 col-lg-5 col-md-5 col-sm-12'>
                            <div className='top_sec'>
                                <select className='hlfinput'>
                                    <option>James Franco</option>
                                </select>
                            </div>
                            <div className='list_item'>
                                <h2>Select input</h2>
                                <ul>
                                    <li >
                                        <button className="btn"><img src="src/images/signulu_small_black_logo.svg" alt="" /> Signature</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/initial.svg" alt="" /> Initial</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/digital-signature.svg" alt="" /> Date Signed</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/text-box.svg" alt="" /> Textbox</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/email.svg" alt="" /> Email</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/checkbox.svg" alt="" /> Checkbox</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/name.svg" alt="" /> Name</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/company.svg" alt="" /> Company</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/image-seal.svg" alt="" /> Image/Seal</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/digital-signature.svg" alt="" /> Digital Signature (Dsc)</button>
                                    </li>
                                    <li className='active'>
                                        <button className="btn"><img src="src/images/attachment.svg" alt="" /> Attachment</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/user-address.svg" alt="" /> User Address</button>
                                    </li>
                                    <li>
                                        <button className="btn"><img src="src/images/company-address.svg" alt="" /> Company Address</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className='col-xl-9 col-lg-7 col-md-7 col-sm-12'>
                            <div className='prepare-slide_pln prepare-slide_pln2'>
                                {/* <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options2.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true}  navText={["<img src='src/images/left-arrw.svg'>","<img src='src/images/right-arrw.svg'>"]}>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                </OwlCarousel> */}

                                <Document  className="row" 
                                                file={documentFile}
                                                onLoadSuccess={subDocumentsLoadSuccess}>
                                <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options2.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true}>
                                                    {Array.apply(null, Array(subNumPages))
                                                    .map((x, i) => i + 1)
                                                    .map(page =>{
                                                          return <div className='item'>
                                                      <Page  className="document_intial img-fluid mx-auto d-block"  pageNumber={page}  />
                                                              {/* <img src="src/images/document_icon_4.svg" alt="" /> */}
                                                              </div> } )}
                                                              </OwlCarousel>
                                                              </Document>  

                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="slide-tab-btn">
                                {count > 0 && <button className="btn prev-btn" onClick={() => {setCount(count - 1), localStorage.setItem("addDocumentPage", count - 1)}}>Back</button>}
                                {count <= 3 && <button className="btn next-btn" onClick={() => {setCount(count + 1),localStorage.setItem("addDocumentPage", count + 1)}}>Next</button>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>,

        <div className='wizard-panel' id="wizard-pane-4">
            <div className='content'>
                <div className='prepare_document'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <h2>Prepare document <span>For Tom Hanks</span></h2>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-9 col-lg-7 col-md-7 col-sm-12'>
                            <div className='prepare-slide_pln'>
                                <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options2.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true} navText={["<img src='src/images/left-arrw.svg'>","<img src='src/images/right-arrw.svg'>"]}>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                </OwlCarousel>
                                <div id="sharedoc" className='share_document' style={{display: 'none'}}>
                                    <h2>Share Document</h2>
                                    <div className='row'>
                                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                            <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options3.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true} navText={["<img src='src/images/left-arrw.svg'>","<img src='src/images/right-arrw.svg'>"]}>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                            </OwlCarousel>
                                        </div>
                                        <div className='col-xl-6 col-lg-12 col-md-6 col-sm-12'>
                                            <div className="slide_text_artcl">
                                                <div className="wap">
                                                    <h6>3 pages</h6>
                                                    <h5>Pages : 2 Recipients : 1</h5>
                                                    <p>Name: Daniel Farrell</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                            <div className='share_document_form'>
                                                <form>
                                                    <div className='row'>
                                                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Subject</label>
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Remind Frequency</label>
                                                                <select className='hlfinput'>
                                                                    <option>Daily</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Keywords</label>
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Keywords</label>
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <textarea />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <button type="submit" className="subBtn">Send</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-3 col-lg-5 col-md-5 col-sm-12'>
                            <div className='prepare-be-document'>
                                <h2>Recipients</h2>
                                <ul>
                                    <li>
                                        <span>1</span>
                                        <div className="text_artle">
                                            <h6>Liam Crowe</h6>
                                            <p>hello.Liam@gmail.com</p>
                                        </div>
                                    </li>
                                    <li>
                                        <span className="num_cotn">2</span>
                                        <div className="text_artle">
                                            <h6>Daniel Farrell</h6>
                                            <p>hello.farrell@gmail.com</p>
                                        </div>
                                    </li>
                                    <li>
                                        <span className="num_cotn">3</span>
                                        <div className="text_artle">
                                            <h6>Chris Pine</h6>
                                            <p>info.pine@gmail.com</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" className="subBtn"  onClick={shareDoc}>Submit
                                <div className="icon">
                                    <span><img src="src/images/right_arrow.svg" /></span>
                                </div>
                            </button>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="slide-tab-btn">
                                <button className="btn prev-btn">BACK</button>
                                <button className="btn cncl-btn">cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    ]
   const errorComponents = [
       <div>
           <p>Only the following file formats are acceptable: .doc, .docx, .png, .jpg, .jpeg, .ppt, .pptx, .xls, .xlsx, .pdf</p>
       </div>,
 <div>
 <p>Invalid pdf structure.</p>
</div>,

<div>
<p>This PDF document is encrypted and cannot be processed with FPDI. Please convert that file to PDF (like print and save as PDF) and continue.</p>
</div>
   ]


    //Owl

    //Documents Dropdown

    //Add New Recipient Modal
    function recipient() {
        var srcElement = document.getElementById('new_recipient');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }

    function openTab(event, divID) {
        // if (divID == 'firstTab') {
        // inputFileRef.current.click();
        // }
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }

    return (
<>
        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content wizard">
                    <div className='row'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div id="wizard_step">
                                <div className="step-wizard" role="navigation">
                                    <ul>
                                        <li className={count === 0 || count < 4 ? 'active done' : ''}>
                                            <button>
                                                <div className="step">1</div>
                                                <div className="title">Add</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 1 || (count < 4 && count != 0) ? 'active done' : ''}>
                                            <button id="step2">
                                                <div className="step">2</div>
                                                <div className="title">Select</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 2 || (count < 4 && count != 1 && count != 0) ? 'active done' : ''}>
                                            <button id="step3">
                                                <div className="step">3</div>
                                                <div className="title">Prepare</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 3 ? 'active done' : ''}>
                                            <button id="step4">
                                                <div className="step">4</div>
                                                <div className="title">Review</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div className='wizard-form'>
                                    {components[count]}
                                    <div className="upload_sec" id="upload" style={{display: "none"}}>
                <div className="content">
                    <h4>{t('UPLOAD_YOUR_FILES')}</h4>
                    <h5>{t('USE_TEMPLATE_FROM_THIRD_PARTY')}</h5>
                    <div className="upload_block">
                        <div className="use_templete">
                            <img src="src/images/folder.svg" alt='' />
                            <h3>{t('USE_TEMPLATE')}</h3>
                            <h4>1,235 Files </h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/google_drive.svg" alt='' />
                            <h3>{t('GOOGLE_DRIVE')}</h3>
                            <h4>1,357</h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/one_drive.svg" alt='' />
                            <h3>{t('ONE_DRIVE')}</h3>
                            <h4>2,143 Files</h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/drop_box.svg" alt='' />
                            <h3>{t('DROP_BOX')}</h3>
                            <h4>1,457 Files</h4>
                        </div>
                        <div className="use_templete drop-zone">
                            <img src="src/images/folder_1.svg" alt='' className="img-fluid mx-auto d-block" />
                            <h4 className="drop-zone__prompt">{t('CLICK_TO_UPLOAD_OR_DRAP_AND_DROP')}</h4>
                            <input type="file" name="myFile" className="drop-zone__input" />
                        </div>
                    </div>
                    <div className="back"onClick={uploadDiv}>
                        <h6 className="fileback"><span ><img src="src/images/back.svg" alt='' /></span>{t('BACK')}</h6>
                    </div>
                </div>
            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             {/* -------- Contacts PopUp Start--------------*/}
            
            
            <Modal show={showErrorPopUp} onHide={() => { setShowErrorPopUp(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='error-modal-content'>
                    {errorComponents[errorCount]}
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    {/* <Button variant="secondary" onClick={() => { setModalShow(false), setDeleteRecipient({}) }}>Cancel</Button> */}
                    <Button className="button_modal_ok" onClick={() => { setShowErrorPopUp(false) }}>Ok</Button>
                </Modal.Footer>
            </Modal>

        </div>
         <Modal show={docmodalShow} onHide={() => { setDocmodalShow(false) }}
         size="lg"
         dialogClassName="my-docpop"
         aria-labelledby="example-modal-sizes-title-lg"
          centered>
          <Modal.Header closeButton>
              <Modal.Title id="example-modal-sizes-title-lg">
                  {documentName}
              </Modal.Title>
          </Modal.Header>
          <Modal.Body >
              <div className='pop-doc'>
              <Document className="row"
                  file={documentFile}
                  onLoadSuccess={popDocLoad}>
                  {Array.apply(null, Array(popDocPages))
                      .map((x, i) => i + 1)
                      .map(page =>
                          <>
                          <Page scale={2} className="subdocument_intial mx-auto d-block" pageNumber={page} />
                          <span className='pagenumber-flex'>page {page} of {popDocPages}</span>
                          </>
                      )}
              </Document>
              </div>
          </Modal.Body>
          <Modal.Footer>
              {/* <Button variant="primary" onClick={() => { setDocmodalShow(false) }}>Submit</Button> */}
              <Button variant="secondary" onClick={() => { setDocmodalShow(false) }}>Close</Button>
          </Modal.Footer>
      </Modal>
      </>
    )
}

export { AddDocumentsStep }; 