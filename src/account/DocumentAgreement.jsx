import React, { useState, useEffect } from 'react';
import { Link, useHistory, useLocation, useParams } from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';
import { useRef } from 'react';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import config from 'config';
import Spinner from 'react-bootstrap/Spinner';
import { Document, Page, pdfjs} from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
import '../css/controls.css'
import Signature from './Controls/Signature';
import Initial from './Controls/Initial';
import DigitalSignature from './Controls/DigitalSignature';
import Attachment from './Controls/Attachment';
import CompanyAddress from './Controls/CompanyAddress';
import UserAddress from './Controls/UserAddress';
import Aadhaar from './Controls/Aadhaar';
import ImageSeal from './Controls/ImageSeal';
import DateSigned from './Controls/DateSigned';
import Email from './Controls/Email';
import Name from './Controls/Name';
import Company from './Controls/Company';
import Payment from "./Controls/Payment";
import Checkbox from './Controls/CheckBox';
import NotarySeal from './Controls/NotarySeal';
import TextBox from './Controls/TextBox';
import Dob from './Controls/Dob';
import SignatureControlEditPopup from './ControlEditPopups/SignaturePopup';
import { addDocumentService } from '../_services/adddocument.service';
import ReactTooltip from 'react-tooltip';
import { TermsConditions } from './TermsConditions';
import { useSearchParams } from 'react-router-dom';
import moment from "moment/moment";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { manageservice } from './../_services/manageservice';
import { commonService } from '../_services/common.service';
import OTPInput from "otp-input-react";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import Comment from './Controls/Comment';
import { Avatar } from 'primereact/avatar';
import { generateUQID } from '../_services/model.service';

let PIDUSEROBJ = {}
let isControlImage = false;
let loginUser = {};
let DocId = '';
let PID = '';
let pagesArray = [];
let selectedControlDataObj = {}
let commentCreateObj = {}
let editCommentObj = false
function DocumentAgreement({ history, location }) {
    const handle = useFullScreenHandle();
    let { id } = useParams();
    const DocId = id;
    const [documentData, setDocumentData] = useState({});
    const [documentFile, setDocument] = useState('');
    const [selectedPageNumber, setselectedPageNumber] = useState(1);
    const [subNumPages, setSubNumPages] = useState();
    const [docRecipients, setDocRecipients] = useState([]);
    const [showWorkflow, setShowWorkflow] = useState(false);
    const [submitSaveDocument, setsubmitSaveDocument] = useState(false);
    const [docControls, setDocControls] = useState([]);
    const [allControls, setAllControls] = useState([]);
    const [isUser_Owner, setUser_Owner] = useState(false);
    const [User_Reviewer, setUser_Reviewer] = useState(false);
    const [User_Approver, setUser_Approver] = useState(false);
    const [controlTypes, setControlTypes] = useState([]);
    const [selectedControl, setSelectedControl] = useState({});
    const [signaturePopShow, setSignaturePopShow] = useState(false);
    const [selectedSignatureControlEditData, setSelectedSignatureControlEditData] = useState({});
    const [selection, setSelection] = useState({ draw: true, choose: false, upload: false, setting: false });
    const [errorStatus, setErrorStatus] = useState(false);
    const [isUser_hasSignCntrl, setisUser_hasSignCntrl] = useState(false);
    const [isuser_hasDscSignCntrl,setisuser_hasDscSignCntrl] = useState(false);
    const [isuser_hasAadharEsign,setisuser_hasAadharEsign]=useState(false)

    const [errorExist, setErrorExist] = useState(false);
    const [errorStatueCode, setErrorStatueCode] = useState(false);
    const [documentTitle, setdocumentTitle] = useState('');
    const [loginUserControls, setLoginUserControls] = useState([]);
    const [expirationDate, setExpirationDate] = useState('');
    const [rejectPopShow, setRejectPopShow] = useState(false);
    const [rejectReason, setRejectReason] = useState('');
    const [IsTermsConditionsEnabled, setIsTermsConditionsEnabled] = useState(false);
    const [rejectedSuccessPopShow, setRejectedSuccessPopShow] = useState(false);
    const [validPopShow, setValidPopShow] = useState(false);
    const [confirmPopShow, setConfirmPopShow] = useState(false);
    const [approvePopShow, setApprovePopShow] = useState(false);
    const [rejectError, setRejectError] = useState(false);
    const [LoginRecipientUserObj, setLoginRecipientUserObj] = useState(false);
    const [confirmWithControlsPopShow, setConfirmWithControlsPopShow] = useState(false);
    const [alreadySign, setAlreadySign] = useState(false);
    const [confirmWithoutControlsPopShow, setConfirmWithoutControlsPopShow] = useState(false);
    const [ErrorRejectReason, setErrorRejectReason] = useState();
    const [ErrorRejectResp, setErrorRejectResp] = useState();

    const usehistory = useHistory();
    const childCompRef = useRef()
    const documentScrollRef = useRef()
    const [uploadPopShow, setUploadPopShow] = useState(false);
    const [IsFileUploaded, setIsFileUploaded] = useState(false);
    const [className, setclassName] = useState('');
    const [failedMessage, setfailedMessage] = useState(false);
    const [attachImageFile, setAttachImageFile] = useState(false);
    const [controlPopDocPages, setControlPopDocPages] = useState();
    const [fileName, setFileName] = useState();
    const [isImageFile, setIsImageFile] = useState(true);
    const [acceptedFiles, setAcceptedFiles] = useState(false);

    const [upBtn, setUpBtn] = useState(true);
    const [downBtn, setDownBtn] = useState(true);
    const [progress,setProgress]=useState(0);
    const [CCAndNonccRecipients,setCCAndNonccRecipients]=useState([]);
    const [scrollPageNumber,setScrollPageNumber]=useState(1);
    const [canvasesArray,setcanvasesArray]=useState([]); 
    const [TotalPagesHeight,setTotalPagesHeight]=useState(0);
    const [submitOtpDocument, setsubmitOtpDocument] = useState(false);
    const [otpError, setOtpError] = useState('');
    const [otpValue, setOtpValue] = useState();
    const [OTPValueLength, setOTPValueLength] = useState(0);
    const [FacialPopup,setFacialPopup] =useState(false)
    const [dscconformpopup,setdscconformpopup]=useState(false)
    const [IsanyoneenableDsc,setIsanyoneenableDsc] = useState(false)
    const [IsanyoneenableAadharEsign,setIsanyoneenableAadharEsign] = useState(false)
    const [addComments, setAddComments] = useState([]);
    const [showAddCommentPopup, setShowAddCommentPopup] = useState(false);
    const [commentError, setCommentError] = useState(false);
    const [comment, setComment] = useState(false);
    const [selectedCommentRecipient, setselectedCommentRecipient] = useState({});
    const [commentedRecpList, setcommentedRecpList] = useState([]);
    const [isCommentDisable, setIsCommentDisable] = useState(false);
    const [isControlTableClose, setIsControlTableClose] = useState(false);
    const [isCommentsTableClose, setIsCommentsTableClose] = useState(false);
    const [Aadharconformpopup,setAadharconformpopup]=useState(false)
    const [allContolFillSuccessPopupOpen, setAllContolFillSuccessPopupOpen] = useState(false);
    const [disableDcoumentComments, setDisableDcoumentComments] = useState(false)
    const [selectedCommentObj, setSelectedCommentObj] = useState({});
    const [showdonthavepermissionpopup,setshowdonthavepermissionpopup]=useState(false);
    const [totalRecipientPageNumberslength,settotalRecipientPageNumberslength]=useState(null)
    //Dropdown Menu User
    function userDropdown() {

        toast.warn('This feature is under development', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: false,
            progress: undefined,
            });

        // var srcElement = document.getElementById('dropdown_menu');
        // if (srcElement != null) {
        //     if (srcElement.style.display == "block") {
        //         srcElement.style.display = "none";
        //     } else {
        //         srcElement.style.display = "block";
        //     }
        //     return false;
        // }
    }
    useEffect(() => {
        if (commonService.userInfoObj) {
            loginUser = commonService.userInfoObj
            // documentVerify()
            getDocumentData()
        } else {
            commonService.userInfo().then((userData) => {
                loginUser = userData.Data
                // documentVerify()
                getDocumentData()
            });
        }
    }, [])

    function documentVerify() {
        addDocumentService.unauthGetVerify(DocId, PID)
            .then(res => {
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0');
                var yyyy = today.getFullYear();
                var currentDate = yyyy + '-' + mm + '-' + dd ;
                if ((res.Entity && !res.Entity.ExpirationDate) || ( res.Entity && currentDate <= res.Entity.ExpirationDate)) {
                    setdocumentTitle(res.Entity.Title)
                        if(!res.Status && res.Entity.StatusCode == 'APPROVED'){
                            setErrorStatueCode(res.Entity.StatusCode)
                            setErrorExist(true) 
                        } else if(!res.Status && res.Entity.StatusCode == 'REJECTED' ) {
                            setErrorStatueCode(res.Entity.StatusCode);
                            setErrorRejectReason(res.Entity.Reason);
                            setErrorRejectResp(res.Entity.FullName)
                            setErrorExist(true) 
                        } else if(!res.Status && res.Entity.StatusCode == 'CANCELLED'){
                            setErrorStatueCode(res.Entity.StatusCode)
                            setErrorExist(true) }
                        else if ("undefined" !== typeof res.Entity.TOS && res.Entity.TOS != null) {
                            setErrorExist(false)
                        addDocumentService.unauthSubscribtion(DocId).then((userData) => {
                            loginUser = userData.Entity
                            getDocumentData('onPageLoad');
                        });
                        } else {
                            setIsTermsConditionsEnabled(true)
                            setErrorExist(false)
                           //terms an condition popup 
                        }
                    } else {
                        setdocumentTitle(res.Entity.Title)
                        if ((res.Entity.ExpirationDate < currentDate)) {
                            var expDate = new Date(res.Entity.ExpirationDate);
                            var expdd = String(expDate.getDate()).padStart(2, '0');
                            var expmm = String(expDate.getMonth() + 1).padStart(2, '0');
                            var expyyyy = expDate.getFullYear();
                            var expirationDate = null;
                            expirationDate = expmm + '/' + expdd + '/' + expyyyy;
                            setExpirationDate(expirationDate)
                            setErrorStatueCode('EXPIRED')
                            setErrorExist(true)    
                        } else if (res.Entity.StatusCode) {
                            setErrorStatueCode(res.Entity.StatusCode)
                            setErrorExist(true) 
                        }
   
                    }
            })
    }
    const getDocumentData = (arg1) => {
        manageservice.getDocumentData(DocId)
            .then(res => {
                pagesArray = []
                if (res.Entity && res.Entity.Data) {
                    setDocumentData(res.Entity.Data);
                    PIDUSEROBJ = res.Entity.Data.Recipients.find(data => data.EmailAddress == loginUser.EmailAddress)
                    settotalRecipientPageNumberslength(PIDUSEROBJ?.RecipientPageNumbers.length)
                    PID = PIDUSEROBJ?.RecipientId
                    if(res.Entity.Data.IsParallelSigning !==1 && PIDUSEROBJ.position !==1 ){
                     let previoususer=  res.Entity.Data.Recipients.find(data => data.position = PIDUSEROBJ.SortPosition-1)
                      if(previoususer?.StatusCode == "SENT"){
                         setshowdonthavepermissionpopup(true)
                      }
                    }
                   let PIDUSEROBJForCheckAadhar = res.Entity.Data.Recipients.find(data => data.IsAadhaarEsign == 1 || data.IsAadhaarEsign == true)
                    if(PIDUSEROBJForCheckAadhar){
                        setIsanyoneenableAadharEsign(true)
                    }
                    if(PIDUSEROBJ?.IsDSC == 1){
                        setIsanyoneenableDsc(true)
                     }
                    if((PIDUSEROBJ?.IsFacialRecognition || PIDUSEROBJ?.IsPhotoRecognition) && !PIDUSEROBJ?.IsFacialVerified){
                        setFacialPopup(true)
                    } else {
                    if(res.Entity.Data.IsSecured && PIDUSEROBJ && !PIDUSEROBJ?.IsOwner && PIDUSEROBJ?.DocumentOtp){
                        setsubmitOtpDocument(true)
                    } else {
                        prepareDocumentData(res.Entity.Data)
                    }
                }
                }      

            })

    }
    function prepareDocumentData(data){
        let documentDataObj = {}
        if(data) {
            documentDataObj = data
        } else {
            documentDataObj = documentData
        }
        setDocument(config.serverUrl + 'adocuments/download/' + documentDataObj.FileName);
        setDocRecipients(documentDataObj.Recipients);
        documentDataObj.Recipients && documentDataObj.CCRecipients ? setCCAndNonccRecipients([...documentDataObj.Recipients, ...documentDataObj.CCRecipients]): setCCAndNonccRecipients(documentDataObj.Recipients)
        const PIDOBJ = documentDataObj.Recipients.find(data => data.EmailAddress == loginUser.EmailAddress)
        PID = PIDOBJ.RecipientId
        setLoginRecipientUserObj(PIDOBJ)
        if (PIDOBJ.IsOwner) {
            setUser_Owner(true)
        }
        if (PIDOBJ.RecipientRole == 'Reviewer') {
            setUser_Reviewer(true)
        }
        if(PIDOBJ.RecipientRole == 'Approver') {
            setUser_Approver(true)
        }
        if(PIDOBJ.IsSignatory) {
            setisUser_hasSignCntrl(true)
        }
        if(PIDOBJ.IsDSC == 1|| PIDOBJ.IsDSC == true){
            setisuser_hasDscSignCntrl(true)
       }
       if(PIDOBJ.IsAadhaarEsign == 1|| PIDOBJ.IsAadhaarEsign == true){
        setisuser_hasAadharEsign(true)
       }
       let CommentedRecipients = []
       if(documentDataObj.AddComments && documentDataObj.Recipients
            && documentDataObj.Recipients.length > 0 && documentDataObj.AddComments.length > 0){
           for (let l = 0; l < documentDataObj.Recipients.length; l++) {
               for (let k = 0; k < documentDataObj.AddComments.length; k++) {
                  if(documentDataObj.Recipients[l].RecipientId == documentDataObj.AddComments[k].RecipientId
                    && documentDataObj.Recipients[l].RecipientId != PIDUSEROBJ?.RecipientId ) {
                   CommentedRecipients.push(documentDataObj.Recipients[l])
                   break;
                  } 
               }
           }
       }
       for (let l = 0; l < documentDataObj.Recipients.length; l++) {
        if(documentDataObj.Recipients[l].IsDSC || documentDataObj.Recipients[l].IsAadhaarKYC || documentDataObj.Recipients[l].IsAadhaarEsign) {
            setDisableDcoumentComments(true)
        }
    }
       // if(!documentDataObj.IsParallelSigning) {
       CommentedRecipients.push(PIDUSEROBJ)
       setcommentedRecpList(CommentedRecipients)
       setselectedCommentRecipient(CommentedRecipients[0])
       // }
        if (documentDataObj.Controls) {
            var controls = Object.keys(documentDataObj.Controls);
            controls.forEach((ctrlId) => {
                let FinalObj = {}
                const obj = documentDataObj.Controls[ctrlId];

                obj.Top = obj.TopByPage
                obj.Left = obj.LeftByPage
                obj.CtrlId = ctrlId;
                const RespData = documentDataObj.Recipients.find(data => data.RecipientId == obj.RecipientId)
                FinalObj = obj
                if (RespData) {
                    if(RespData.IsInPerson) {
                        // RespData.SignatureFont = null
                        // RespData.InitialUpload  = null;
                        // RespData.SignatureUpload  = null
                        // RespData.SignType = null
                        FinalObj = { ...obj}
                    } else if(RespData && RespData.RecipientId == PIDOBJ.RecipientId) {
                        FinalObj = { ...obj, ...RespData}
                    } else {
                        FinalObj = { ...obj}
                    }
                }
                if (FinalObj.RecipientId == PID || (PIDUSEROBJ?.IsOwner && FinalObj.IsInPerson)) {
                    FinalObj.enableControl = true;
                    loginUserControls.push(FinalObj)
                }
                allControls.push(FinalObj)   
            })
            if(loginUserControls.length<=1){
                setUpBtn(true);
                setDownBtn(true);
            }
            if(loginUserControls.length>1) {
                setDownBtn(false);
            } else {
                setDownBtn(true);
                setUpBtn(true);
            }
            changeProgressBarValue()
            
            setLoginUserControls([...loginUserControls])
            setAllControls([...allControls])
            for (let index = 0; index < documentDataObj.AddComments.length; index++) {
                documentDataObj.AddComments[index].Left = documentDataObj.AddComments[index].CommentLeft
                documentDataObj.AddComments[index].Top = documentDataObj.AddComments[index].CommentTop
            }
            setAddComments(documentDataObj.AddComments)
            if(loginUserControls && loginUserControls.length>0) {
                selectedControlDataObj = loginUserControls[0]
            setSelectedControl({...loginUserControls[0]})
            }
            
        }
    }

function changeProgressBarValue(){
   let controlsFilledCount = 0
   let personalControlFilledCount = 0
   let personalControl = []
    for (let k = 0; k < allControls.length; k++) {
        if (allControls[k].Code == "signature" && allControls[k].SignatureUpload){
            controlsFilledCount++
            if(allControls[k].RecipientId == PID || (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson)) {
                personalControlFilledCount++
            }
        } else if(allControls[k].Code == "initial" && allControls[k].InitialUpload) {
            controlsFilledCount++
            if(allControls[k].RecipientId == PID|| (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson)) {
                personalControlFilledCount++
            }
        } else if ((allControls[k].Code == "initial" || allControls[k].Code == "signature") && allControls[k].SignatureFont) {
            controlsFilledCount++
            if(allControls[k].RecipientId == PID|| (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson)) {
                personalControlFilledCount++
            }
        } else if (allControls[k].Code == "date" ||  allControls[k].Code == "seal" || allControls[k].Code == "digitalsignature") {
            controlsFilledCount++
            if(allControls[k].RecipientId == PID|| (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson)) {
                personalControlFilledCount++
            }
        } else if (allControls[k].Value) {
            controlsFilledCount++
            if(allControls[k].RecipientId == PID|| (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson)) {
                personalControlFilledCount++
             }
        }  
        if(allControls[k].RecipientId == PID|| (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson)) {
            personalControl.push(allControls[k])
        } 
    }
    if(Math.round(personalControlFilledCount/(personalControl.length)*100) == 100){
        if(!allContolFillSuccessPopupOpen) {
        let content = contentfunction()
        toast.success(content, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: false,
            progress: undefined,
            });
            setAllContolFillSuccessPopupOpen(true)
        }
        } else {
            setAllContolFillSuccessPopupOpen(false)
        }
    setProgress(Math.round(controlsFilledCount/(allControls.length)*100)); 
}
function contentfunction() {
    return <> 
    {/* <b>Ready to {isUser_hasSignCntrl ? 'SIGN' : 'APPROVE'}?</b> */}
    <p style={{lineHeight:'1.2'}}>The document is complete from your end and is ready to {(PIDUSEROBJ.IsSignatory || PIDUSEROBJ.IsDSC) ? 'Sign' : 'Approve'}. Click on {(PIDUSEROBJ.IsSignatory || PIDUSEROBJ.IsDSC) ? 'Sign' : 'Approve'} to complete the process</p>
    </>
}
    function onDocumentLoadSuccess(e) {
        selectedPageNumber ? setselectedPageNumber(selectedPageNumber) : setselectedPageNumber('')
        setSubNumPages(e.numPages);
    }

    function getOnRenderSuccess(e, pageIndex, page) {
        pagesArray.push(page)
        if(subNumPages == pagesArray.length){
            DocumentControlsSetUp()
            DocumentCommentsSetUp('')
            setcanvasesArray($('body').find('canvas'))
        }
       }
       function DocumentCommentsSetUp(data) {
        let comments = addComments
        if(data){
            comments = data
        }
        for (let index = 0; index < comments.length; index++) {
            comments[index].Left = comments[index].LeftByPage
            let topPosition = 0
            if (comments[index].PageNumber == 1) {
                comments[index].Top = comments[index].TopByPage
            } else {
                for (var idx = 0; idx < comments[index].PageNumber; idx++) {
                if ((comments[index].PageNumber - 1) == idx) {
                  topPosition = (topPosition + comments[index].TopByPage)
                  comments[index].Top = topPosition
                } else if(PIDUSEROBJ.RecipientPageNumbers.includes(idx+1)) {
                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[idx].firstChild
                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                }
              }
            }
            if(!PIDUSEROBJ.RecipientPageNumbers.includes(comments[index].PageNumber)){
                comments[index].HideCommentFalse = true
            }
        }
        setAddComments(comments)

       }
    function DocumentControlsSetUp() {
        let loginControls = []
        for (let index = 0; index < allControls.length; index++) {
            allControls[index].Left = allControls[index].LeftByPage - 10
            let topPosition = 0
            if (allControls[index].PageNumber == 1) {
                allControls[index].Top = allControls[index].TopByPage
            } else {
                for (var idx = 0; idx < allControls[index].PageNumber; idx++) {
                if ((allControls[index].PageNumber - 1) == idx) {
                  topPosition = (topPosition + allControls[index].TopByPage)
                  allControls[index].Top = topPosition
                } else {
                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[idx].firstChild
                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                }
              }
            }
            if (allControls[index].RecipientId == PID || (PIDUSEROBJ?.IsOwner && allControls[index].IsInPerson)) {
                allControls[index].enableControl = true;
                loginControls.push(allControls[index])
            }
        }
        setAllControls([...allControls])
        // const Data = loginControls.sort(function(a, b){
        //     let x = a
        //     let y = b
        //     if (x.Top < y.Top) {return -1;}
        //     if (x.Top > y.Top) {return 1;}
        //     return 0;
        //   });
        const Data =loginControls
        setLoginUserControls([...Data])
        if(Data && Data.length > 0) {
            selectedControlDataObj = Data[0]
        setSelectedControl({...Data[0]})
        }
    }
    function WorkflowInfo() {
        setShowWorkflow(true);
    }

    function downloadDocument() {
        let FileName = documentData.Title
        if(documentData.Title) {
            FileName = documentData.Title.split('.pdf')[0]  + '.pdf'
        }
        manageservice.downloadUnauthDocument(DocId, PID).then(response => {
            const blob = new Blob([response], { type: 'application/octet-stream' });
            const element = document.createElement('a');
            element.href = URL.createObjectURL(blob);
            element.download = FileName;
            document.body.appendChild(element);
            element.click();
        })
    }

    const loadSpinner = () => {
        return <div style={{ display: "flex", alignContent: 'center', alignItems: "center", justifyContent: "center", marginTop: '30%' }}>
            <Spinner variant="primary" animation="border" /></div>
    }

    function controlOnChangeValue(control, value) {
        for (let index = 0; index < allControls.length; index++) {
            if (allControls[index].CtrlId == control.CtrlId) {
                allControls[index].Value = value;
                allControls[index].MandatoryErrorExist = false;
                break
            }
        }
          setAllControls([...allControls])
          changeProgressBarValue()
    }

    function navigationControlChange(){
        for (let index = 0; index < loginUserControls.length; index++) {
        if (loginUserControls[index].CtrlId == selectedControlDataObj.CtrlId) {
            if (loginUserControls.length == 1) {
                setDownBtn(true);
                setUpBtn(true);
                break
            } else if (index == loginUserControls.length - 1) {
                setDownBtn(true);
                setUpBtn(false);
                setselectedPageNumber(selectedControlDataObj.PageNumber);
                break
            } else if (index == 0) {
                setselectedPageNumber(selectedControlDataObj.PageNumber);
                setUpBtn(true);
                setDownBtn(false);
                break
            } else {
                setUpBtn(false);
                setDownBtn(false);
                // break
            }
        }
    }
    }

    const signSelectionHandler = () => {
        if (validpopupclose() || selection.setting) {
            allControls.forEach((e, index) => {

                if ((e.Code == "signature" || e.Code == "initial") && e.RecipientId == selectedControl.RecipientId) {
                    e.SignatureUpload = '';
                    e.SignatureFont = '';
                    e.InitialUpload = ""
                    if (selectedSignatureControlEditData.signType == "Font") {
                        e.SignatureUpload = '';
                        e.SignatureFont = selectedSignatureControlEditData.SignatureFont;
                        e.InitialUpload = "";
                        e.MandatoryErrorExist = false;
                    } else if (selectedSignatureControlEditData.signType == "Upload") {
                        e.SignatureUpload = selectedSignatureControlEditData.SignUpload;
                        e.SignatureFont = '';
                        e.InitialUpload = selectedSignatureControlEditData.InitialUpload;
                        e.MandatoryErrorExist = ((e.Code == "signature" && selectedSignatureControlEditData.SignUpload) ||  (e.Code == "initial" && selectedSignatureControlEditData.InitialUpload)) ? false : e.MandatoryErrorExist;
                    } else if (selectedSignatureControlEditData.signType == "Sign") {
                        e.SignatureUpload = selectedSignatureControlEditData.SignData;
                        e.SignatureFont = '';
                        e.InitialUpload = selectedSignatureControlEditData.SignInitial;
                        e.MandatoryErrorExist = ((e.Code == "signature" && selectedSignatureControlEditData.SignData) || (e.Code == "initial" && selectedSignatureControlEditData.SignInitial)) ? false : e.MandatoryErrorExist;
                    }
                    e.SignType = selectedSignatureControlEditData.signType;
                    e.SignatureInitial = selectedSignatureControlEditData.SignatureInitial;
                    e.SignatureName = selectedSignatureControlEditData.SignatureName;
                }
            })

            for (let k = 0; k < docRecipients.length; k++) {
                if (docRecipients[k].RecipientId == selectedControl.RecipientId) {
                    docRecipients[k].SignatureUpload = '';
                    docRecipients[k].SignatureFont = '';
                    docRecipients[k].InitialUpload = ''
                    if (selectedSignatureControlEditData.signType == "Font") {
                        docRecipients[k].SignatureUpload = '';
                        docRecipients[k].SignatureFont = selectedSignatureControlEditData.SignatureFont;
                        docRecipients[k].InitialUpload = ''
                    } else if (selectedSignatureControlEditData.signType == "Upload") {
                        docRecipients[k].SignatureUpload = selectedSignatureControlEditData.SignUpload
                        docRecipients[k].SignatureFont = ''
                        docRecipients[k].InitialUpload = selectedSignatureControlEditData.InitialUpload;
                    } else if (selectedSignatureControlEditData.signType == "Sign") {
                        docRecipients[k].SignatureUpload = selectedSignatureControlEditData.SignData
                        docRecipients[k].SignatureFont = ''
                        docRecipients[k].InitialUpload = selectedSignatureControlEditData.SignInitial;
                    }
                    docRecipients[k].SignType = selectedSignatureControlEditData.signType;
                    docRecipients[k].SignatureInitial = selectedSignatureControlEditData.SignatureInitial
                    docRecipients[k].SignatureName = selectedSignatureControlEditData.SignatureName
                    break
                }
            }
            setSignaturePopShow(false)
            setDocRecipients([...docRecipients]);
            setAllControls([...allControls])
            changeProgressBarValue()
        } else {
            setErrorStatus(true)
        }
    }

    function validpopupclose() {
        // if (selection.draw) {
        //     if (selectedSignatureControlEditData.SignData || selectedSignatureControlEditData.SignInitial) {
        //         return true
        //     } else {
        //         return false
        //     }
        // } else if (selection.choose) {
        //     if (selectedSignatureControlEditData.SignatureFont) {
        //         return true
        //     } else {
        //         return false
        //     }
        // } else if (selection.upload) {
        //     if (selectedSignatureControlEditData.SignUpload || selectedSignatureControlEditData.InitialUpload) {
        //         return true
        //     } else {
        //         return false
        //     }
        // }
        return true
    }

    function GetScrollValue(event) {
        let pageHeight = 0
        let PagesArray = canvasesArray
        if (PagesArray.length == 0) {
            PagesArray = $('body').find('canvas')
        }
        if (PagesArray && PagesArray.length > 0) {
            for (let idx = 0; idx <= PagesArray.length; idx++) {
                let DocPageDom = PagesArray[idx]
                if (PagesArray[idx]) {
                    pageHeight = pageHeight + Number(DocPageDom.style.height.replace('px', ''))
                    if (pageHeight > event.target.scrollTop + (event.target.offsetHeight / PagesArray.length)) {
                        setScrollPageNumber(idx + 1)
                        break
                    } else if (pageHeight < event.target.scrollTop + (event.target.offsetHeight / PagesArray.length)) {
                        setScrollPageNumber(idx)
                    }
                }
            }
        } else {
            setcanvasesArray($('body').find('canvas'))
        }
    }
    function rejectPopOpen() {
        setRejectPopShow(true)
     }
     function redirectToManagePage() {
        usehistory.push('/account/manage');
     }
    function approveDocument(type) {
        if (type == 'Sign') {
            let mandatoryValidation = false
            for (let k = 0; k < allControls.length; k++) {
                if ((allControls[k].RecipientId == PID || (PIDUSEROBJ?.IsOwner && allControls[k].IsInPerson)) && allControls[k].IsMandatory) {
                    if (((allControls[k].Code == "signature" && !allControls[k].SignatureUpload) || (allControls[k].Code == "initial" && !allControls[k].InitialUpload))
                     && ((allControls[k].Code == "initial" || allControls[k].Code == "signature") && !allControls[k].SignatureFont)) {
                        mandatoryValidation = true;
                        allControls[k].MandatoryErrorExist = true;
                    } else if (!allControls[k].Value && allControls[k].Code != 'date' && allControls[k].Code != "signature" && allControls[k].Code != "initial") {
                        mandatoryValidation = true
                        allControls[k].MandatoryErrorExist = true
                    } 
                    if(allControls[k].StatusCode == "APPROVED"){
                        mandatoryValidation = true;
                            // allControls[k].MandatoryErrorExist = true;
                            // setAlreadySign(true)
                    }
                }
                if(allControls[k].Code != "signature" && allControls[k].Code != "initial") {
                    delete allControls[k].InitialUpload
                    delete allControls[k].SignatureUpload
                    delete allControls[k].SignatureFont
                }
            }
            if (!mandatoryValidation) {
                setConfirmPopShow(true)
            } else if(mandatoryValidation && LoginRecipientUserObj.StatusCode =="APPROVED"){
                setAlreadySign(true)
            } else {
                if( mandatoryValidation && LoginRecipientUserObj.RecipientRole == 'Signatory'){
                    setValidPopShow(true)
                } else if(mandatoryValidation){
                    setValidPopShow(true)
                } else if(loginUserControls && loginUserControls.length > 0) {
                    setConfirmWithControlsPopShow(true)
                } else {
                    setConfirmWithoutControlsPopShow(true)
                }
                
            }
        } else if (type == 'Approve') {
            let mandatoryValidation = false
            for (let k = 0; k < allControls.length; k++) {
                if ((allControls[k].RecipientId == PID || (PIDUSEROBJ?.IsOwner && allControls[k].IsInPerson)) && allControls[k].IsMandatory) {
                    if (((allControls[k].Code == "signature" && !allControls[k].SignatureUpload) || (allControls[k].Code == "initial" && !allControls[k].InitialUpload))
                     && ((allControls[k].Code == "initial" || allControls[k].Code == "signature") && !allControls[k].SignatureFont)) {
                        mandatoryValidation = true;
                        allControls[k].MandatoryErrorExist = true;
                    } else if (!allControls[k].Value && allControls[k].Code != 'date' && allControls[k].Code != "signature" && allControls[k].Code != "initial") {
                        mandatoryValidation = true
                        allControls[k].MandatoryErrorExist = true
                    }
                }
                if(allControls[k].Code != "signature" && allControls[k].Code != "initial") {
                    delete allControls[k].InitialUpload
                    delete allControls[k].SignatureUpload
                    delete allControls[k].SignatureFont
                }
            }
            if (!mandatoryValidation && loginUserControls && loginUserControls.length > 0) {
                setConfirmPopShow(true)
            } else {
                if(mandatoryValidation &&LoginRecipientUserObj.RecipientRole == 'Signatory'){
                    setValidPopShow(true)
                } else if(mandatoryValidation){
                    setValidPopShow(true)
                } else if(loginUserControls && loginUserControls.length > 0) {
                    setConfirmWithControlsPopShow(true)
                } else {
                    setConfirmWithoutControlsPopShow(true)
                }
                
            }
        }
        else if (type == 'Aadhar'){
            let mandatoryValidation = false
            for (let k = 0; k < allControls.length; k++) {
                if ((allControls[k].RecipientId == PID || (PIDUSEROBJ?.IsOwner && allControls[k].IsInPerson)) && allControls[k].IsMandatory) {
                    if (((allControls[k].Code == "signature" && !allControls[k].SignatureUpload) || (allControls[k].Code == "initial" && !allControls[k].InitialUpload))
                     && ((allControls[k].Code == "initial" || allControls[k].Code == "signature") && !allControls[k].SignatureFont)) {
                        mandatoryValidation = true;
                        allControls[k].MandatoryErrorExist = true;
                    } else if (!allControls[k].Value && allControls[k].Code != 'date' && allControls[k].Code != 'seal' && allControls[k].Code != "signature" && allControls[k].Code != "initial") {
                        mandatoryValidation = true
                        allControls[k].MandatoryErrorExist = true
                    }
                }
                if(allControls[k].Code != "signature" && allControls[k].Code != "initial") {
                    delete allControls[k].InitialUpload
                    delete allControls[k].SignatureUpload
                    delete allControls[k].SignatureFont
                }
            }
                if(mandatoryValidation &&LoginRecipientUserObj.RecipientRole == 'Signatory'){
                    setValidPopShow(true)
                } else if(mandatoryValidation){
                    setValidPopShow(true)
                } else {
                    let d = new Date();
                    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
                    let timeSplit = time.split(' ');
                    let timeZone = timeSplit[1];
                    documentData.URL = `${window.location.origin+`/account/dashboard`}`,
                    documentData.DateTime= moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                    documentData.TimeZone = timeZone,
                    documentData.IsReact = 1,
                    addDocumentService.getAadharsignresponse(documentData,DocId, PID).then(result=>{
                      let  redirectUrl = 'https://esign.egov-nsdl.com/nsdl-esp/authenticate/esign-doc/';
                        $('#addAadharContent').html(''); 
                                              const form = $('<form action="' + redirectUrl + '" method="POST" id="URL" name="URL" enctype="multipart/form-data">' +
                                              '<input type="hidden" id="aadhaarCtrlId" name="msg" value="" ng-show="false">' +
                                              '<input type="hidden" id="documentDataForAadhaar" name="DocumentData" value="" ng-show="false">' +
                                              '<div class="text-align-center"><input type="submit" value="Sign" class="signbutton"></div>'+
                                             '</form>');
                                             setAadharconformpopup(true)
                                                $('#addAadharContent').append(form); 
                                                $('#aadhaarCtrlId').val(result.data.Entity.RequestXML);
                                               // $(form).submit();
                    })
                }
        }
        else if (type== 'Dsc'){
            let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
            documentData.URL = `${window.location.origin+`/account/dashboard`}`,
            documentData.DateTime= moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
            documentData.TimeZone = timeZone,
            documentData.IsReact = 1,
            addDocumentService.getdscsignresponse(documentData,DocId, PID).then(result=>{
                $('#addDSCContent').html(''); 
                const dscInfo = JSON.parse(result.Entity);
                  					const parameter1 = dscInfo.parameters[1].value
              		                const parameter2 = dscInfo.parameters[2].value
              		                const parameter3 = dscInfo.parameters[3].value
              		                const dscUrl = dscInfo.DSCBaseURL+'/eMsecure/V3_0/Index'
            	  		            const form = $('<form style="margin:10px;" action="'+dscUrl+'" method="POST" id="URL" name="URL" enctype="multipart/form-data">' +
                                         '<input id="Parameter1" type="hidden" name="Parameter1" value="'+ parameter1 +'">'+
                                         '<input id="Parameter2" type="hidden" name="Parameter2" value="'+ parameter2 +'">'+
                                         '<input id="Parameter3" type="hidden" name="Parameter3" value="'+ parameter3 +'">'+
                                         '<div class="text-align-center"><input type="submit"  value="Sign" class="signbutton"></div>'+
                                        '</form>');
                                        setdscconformpopup(true)
                                        $('#addDSCContent').append(form); 
            })
        }
    }

    function submitSign() {
        let controls = {};
        let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
        for (let k = 0; k < allControls.length; k++) {
            if(allControls[k].Code == 'date' && (allControls[k].RecipientId == PID  || (PIDUSEROBJ?.IsOwner && allControls[k].IsInPerson))) {
                const NewDate = new Date()
                let date = moment(NewDate).format(allControls[k].DateFormat);
                allControls[k].Value = date
            }
            controls[allControls[k].CtrlId] = allControls[k];
        }
            let submitApiPayload = {
                Signature: [],
                Initial: [],
                ADocumentId: DocId,
                Recipients: docRecipients,
                Controls: controls,
                AddComments: addComments, //comment feature 
                AutoFill: null,
                DateTime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                TimeZone: timeZone,
                IsReact: 1
            }
            if(IsanyoneenableAadharEsign == true){
                manageservice.approveDocumentAadhar(DocId, PID, submitApiPayload)
                .then(response => {
                    setConfirmPopShow(false)
                    setConfirmWithControlsPopShow(false)
                    setConfirmWithoutControlsPopShow(false)
                    // setAlreadySign(false)
                    setApprovePopShow(true)
                    // usehistory.push('/account/manage');
                })
            }
            else{
            manageservice.approveDocument(DocId, PID, submitApiPayload)
                .then(response => {
                    setConfirmPopShow(false)
                    setConfirmWithControlsPopShow(false)
                    setConfirmWithoutControlsPopShow(false)
                    // setAlreadySign(false)
                    setApprovePopShow(true)
                    // usehistory.push('/account/manage');
                })
            }
        }
    function submitApprove() {
        let controls = {};
        let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
        for (let k = 0; k < allControls.length; k++) {
            if(allControls[k].Code == 'date' && (allControls[k].RecipientId == PID || (PIDUSEROBJ?.IsOwner && allControls[k].IsInPerson))) {
                const NewDate = new Date()
                let date = moment(NewDate).format(allControls[k].DateFormat);
                allControls[k].Value = date
            }
            controls[allControls[k].CtrlId] = allControls[k];
        }
        setAllControls([...allControls])
            let approvePayload = {
                Signature: [],
                Initial: [],
                ADocumentId: DocId,
                Recipients: docRecipients,
                Controls: controls,
                AddComments: addComments, //comment feature 
                AutoFill: null,
                DateTime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                TimeZone: timeZone
            }
            manageservice.approveDocument(DocId, PID, approvePayload)
                .then(response => {
                    setConfirmPopShow(false)
                    setConfirmWithControlsPopShow(false)
                    setConfirmWithoutControlsPopShow(false)
                    // setAlreadySign(false)
                    setApprovePopShow(true)
                    // usehistory.push('/account/manage');
                })
    }

    function saveDocument() {
        let controls = {};
        let d = new Date();
        let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
        let timeSplit = time.split(' ');
        let timeZone = timeSplit[1];
        for (let k = 0; k < allControls.length; k++) {
            // if(allControls[k].Code == 'date' && (allControls[k].RecipientId == PID || (PIDUSEROBJ.IsOwner && allControls[k].IsInPerson))) {
            //     const NewDate = new Date()
            // let date = moment(NewDate).format(allControls[k].DateFormat);
            //     allControls[k].Value = date
            // }
            controls[allControls[k].CtrlId] = allControls[k];
        }
        let obj = {
            Signature: [],
            Initial: [],
            ADocumentId: DocId,
            Recipients: docRecipients,
            Controls: controls,
            AddComments: addComments,
            AutoFill: null,
            DateTime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
            TimeZone: timeZone
        }
        addDocumentService.unauthSaveOnly(DocId, PID, obj)
            .then(res => {
                DocumentCommentsSetUp(res.Data.AddComments)
                setsubmitSaveDocument(true);

            })
    }
    function changeSelectedControl(controlData) {
        selectedControlDataObj = controlData
        setSelectedControl(controlData)
        navigationControlChange()
    }
    function changeTableSelectedControl(controlData) {
        selectedControlDataObj = controlData
        setSelectedControl(controlData)
    }
    function rejectSubmitHandler() {
        saveDocument()
        if (rejectReason) {
            let obj = {
                Reason: rejectReason
            }
            addDocumentService.unauthRejectDocument(DocId, PID, obj)
                .then(res => {
                    setRejectError('')
                    setRejectPopShow(false)
                    setRejectedSuccessPopShow(true)
                })
        } else {
            setRejectError('Reason for rejection is required')
        }
    }
    function onRejectChange(event) {
        setRejectReason(event.target.value)
        setRejectError('')
    }

    function submitTermsAndCondition() {
        addDocumentService.unauthTermsConditions(DocId, PID).then(res => {
            setIsTermsConditionsEnabled(false)
            getDocumentData('onPageLoad');
        })
    }
    function rejectSuccessfulOkHandler() {
        setRejectedSuccessPopShow(false)
        usehistory.push('/account/manage');
    }
    function OkHandler() {
        setApprovePopShow(false)
        usehistory.push('/account/manage');
    }

// IMAGESEAL/ATTACHMENTS
function changeAttachControlEdit(type) {
        setAttachImageFile('')
        setIsFileUploaded(false)
        setfailedMessage(false);
        setFileName('')
        if (type == 'IMAGE') {
            isControlImage =true
            setAcceptedFiles('image/png,image/jpg,image/jpeg')
        } else {
            isControlImage =false
            setAcceptedFiles('image/png,image/jpg,image/jpeg,.pdf')
        }
        if(selectedControl && selectedControl.Value){
            setAttachImageFile(selectedControl.FileBase64); 
            setFileName(selectedControl.Value)
            setIsFileUploaded(true)
    
        }
        setUploadPopShow(true)
        selectedControl.Value ? extentionFile(selectedControl.Value) : null;
        setTimeout(() => {
            getTags()
        }, 500);
    
    }
    
    // ImageSeal/Attachment
    function getTags() {
        document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
            const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
    
            dropZoneElement.addEventListener("click", (e) => {
                if (e.target.className != 'file-add-text') {
                    inputElement.click();
                }
            });
    
            inputElement.addEventListener("change", (e) => {
                if (inputElement.files.length) {
                    for (let inputFile of inputElement.files) {
                        saveFiles(inputFile)                }
                    var inputVal = inputElement.value;
                }
            });
            dropZoneElement.addEventListener("dragover", (e) => {
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.add("drop-zone--over");
            });
            dropZoneElement.addEventListener('dragleave', function (e) {
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.remove("drop-zone--over");
            });
    
            ["dragleave", "dragend"].forEach((type) => {
                dropZoneElement.addEventListener(type, (e) => {
                });
            });
    
            dropZoneElement.addEventListener("drop", (e) => {
                if (e.dataTransfer.files.length) {
                    inputElement.files = e.dataTransfer.files;
                    for (let inputFile of e.dataTransfer.files) {
                        saveFiles(inputFile)
                    }
                }
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.remove("drop-zone--over");
            });
        });
    
    }
    
    function clearFileInfo() {
        setAttachImageFile('')
        setIsFileUploaded(false)
        setfailedMessage(false);
        setFileName('')
        setTimeout(() => {
            getTags()
        }, 500);
        }
    
        function saveFiles(file) {
            if (isControlImage) {
                var allowedWithOutPDFExtensions = /(\.png|\.jpg|\.jpeg)$/i;
                if (allowedWithOutPDFExtensions.exec(file.name) && 1054464 >= file.size ) {
                    process(file)
                } else {
                    setfailedMessage(true);
                }
            } else {
                var allowedWithPDFExtensions = /(\.png|\.jpg|\.jpeg|\.pdf)$/i;
                if (allowedWithPDFExtensions.exec(file.name) && 2097152 >= file.size) {
                    process(file)
                } else {
                    setfailedMessage(true);
                }
            }
        }
    function getBase64(file) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            setAttachImageFile(reader.result); 
            setIsFileUploaded(true)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    function process(file) {
        setFileName(file.name)
        extentionFile(file.name)
        getBase64(file);
        setIsFileUploaded(true);
        setfailedMessage(false);
    }
    function extentionFile(name) {
        var ext = name.split('.');
        setclassName('')
        switch (ext[1]) {
              case 'pdf':
                setclassName('fa fa-file-pdf');
                setIsImageFile(false)
              break;
            case 'png':
            case 'jpg':
            case 'jpeg':
                setclassName('fa fa-picture');
                setIsImageFile(true)
                break; default:
                setclassName('fa fa-file');
                break;
        }
    }
    function ControlpopDocLoad(e) {
        setControlPopDocPages(e.numPages)
    }
    
    function handleControlErrorClick() {
        setfailedMessage(false);
    }
    function UploadImageAttachment() {
            for (let index = 0; index < allControls.length; index++) {
                if(allControls[index].CtrlId == selectedControl.CtrlId) {
                    allControls[index].Value = fileName ? fileName : null
                    allControls[index].FileBase64 = attachImageFile ? attachImageFile : null
                    allControls[index].MandatoryErrorExist = fileName ? false : allControls[index].MandatoryErrorExist
                    setSelectedControl({...allControls[index]})
                    selectedControlDataObj = allControls[index]
                    break
                }
                
            }
              setAllControls([...allControls])
              setUploadPopShow(false)
              setAttachImageFile('')
              setIsFileUploaded(false)
              setfailedMessage(false);
              setFileName('')
              changeProgressBarValue()
    }
    // IMAGESEALANDATTACHMENY END
    // UP AND DOWN BUTTONS
    function navigateComments(item) {
        documentScrollRef.current.scrollTo(item.Left - (item.Left/2), item.Top -100)
    }
    function navigateControls(movement) {
        for (let index = 0; index < loginUserControls.length; index++) {
            if (movement === 'down') {
                if (loginUserControls[index].CtrlId == selectedControl.CtrlId) {
                    const control = allControls.find(data => loginUserControls[index + 1].CtrlId == data.CtrlId)
                    setUpBtn(false);
                    if (control) {
                        setSelectedControl(control);
                        selectedControlDataObj = control
                        documentScrollRef.current.scrollTo(control.Left - (control.Left/2), control.Top -100)
                        setselectedPageNumber(control.PageNumber);
                        if (index + 2 == loginUserControls.length) {
                            setDownBtn(true);
                            setUpBtn(false);
                        }
                    } else {
                        setUpBtn(true);
                    }
                    break;
                }
            }
            else if (movement === 'up') {
                if (loginUserControls[index].CtrlId == selectedControl.CtrlId) {
                    const control = allControls.find(data => loginUserControls[index - 1].CtrlId == data.CtrlId)
                    setDownBtn(false);
                    if (control) {
                        selectedControlDataObj = control
                        setSelectedControl(control);
                        setselectedPageNumber(control.PageNumber);
                        documentScrollRef.current.scrollTo(control.Left - (control.Left/2), control.Top -100)
                        if (index == loginUserControls.length || index == 1) {
                            setUpBtn(true);
                            setDownBtn(false);
                        }
                    } else {
                        setDownBtn(true);
                    }
                    break;
                }
            } else if (loginUserControls[index].CtrlId == selectedControlDataObj.CtrlId) {
                if (loginUserControls.length == 1) {
                    setDownBtn(true);
                    setUpBtn(true);
                    documentScrollRef.current.scrollTo(selectedControlDataObj.Left - (selectedControlDataObj.Left/2), selectedControlDataObj.Top -100)
                    break
                } else if (index == loginUserControls.length - 1) {
                    setDownBtn(true);
                    setUpBtn(false);
                    setselectedPageNumber(selectedControlDataObj.PageNumber);
                    documentScrollRef.current.scrollTo(selectedControlDataObj.Left - (selectedControlDataObj.Left/2), selectedControlDataObj.Top -100)
                    break
                } else if (index == 0) {
                    setselectedPageNumber(selectedControlDataObj.PageNumber);
                    setUpBtn(true);
                    setDownBtn(false);
                    documentScrollRef.current.scrollTo(selectedControlDataObj.Left - (selectedControlDataObj.Left/2), selectedControlDataObj.Top -100)
                    break
                } else {
                    setUpBtn(false);
                    setDownBtn(false);
                    documentScrollRef.current.scrollTo(selectedControlDataObj.Left - (selectedControlDataObj.Left/2), selectedControlDataObj.Top -100)
                    // break
                }
            }
        }
    }
    function controlResizeEnd(sizeValue, e, direction, ref,position, control) {
        for (let index = 0; index < allControls.length; index++) {
            if (allControls[index].CtrlId == control.CtrlId) {
                ref.style.width ? allControls[index].Width = Number(ref.style.width.replace('px', '')): null;
                ref.style.height ? allControls[index].Height = Number(ref.style.height.replace('px', '')) : null;
                const Pagereference = document.getElementsByClassName('react-pdf__Page')[allControls[index].PageNumber-1].firstChild
                if(Number(Pagereference.style.width.replace('px', '')) < (allControls[index].LeftByPage + allControls[index].Width + 3)) {
                    allControls[index].LeftByPage = (Number(Pagereference.style.width.replace('px', '')) + 10) -(10 + allControls[index].Width)
                    allControls[index].Left = Number(Pagereference.style.width.replace('px', ''))-(10 + allControls[index].Width)
                }
                let TotalHeight = TotalPagesHeight
                if (!TotalPagesHeight) {
                    for (let index = 0; index < subNumPages; index++) {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index].firstChild
                        if (subNumPages == (index + 1)) {
                            TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                            setTotalPagesHeight(TotalHeight)
                            break
                        } else {
                            TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                }
                if (TotalHeight < (allControls[index].Top + 10 + allControls[index].Height)) {
                    allControls[index].TopByPage = allControls[index].TopByPage -(10 + allControls[index].Height)
                    allControls[index].Top = TotalHeight-(10 + allControls[index].Height)
                }
                break
            }
        }
        setAllControls([...allControls])
    }
    function verifyOTP() {
        if (otpValue && otpValue.length == 4) {
            if(otpValue == PIDUSEROBJ?.DocumentOtp){
                setsubmitOtpDocument(false)
                prepareDocumentData()
            } else {
                setOtpError('OTP entered was incorrect, Please enter valid OTP')
            }
    } else {
        setOtpError('Please enter 4 digits otp')

    }
    }
    function OTPChange (e) {
        setOtpError('')
        setOTPValueLength(e.length)
        setOtpValue(e)
    }
    function goToFacialPage() {
        let URL = '/account/facialandphotoverification' + '?docId='+ DocId + '&recId=' + PID + "&redirect_url=" + window.location.pathname + window.location.search + '&fileName='+ PIDUSEROBJ?.ProfilePicture + '&guid='+PIDUSEROBJ?.PartyGUID
        setFacialPopup(true)
        usehistory.push(URL);
    
      }
      function onGetTextSuccessLoaded(data) {
      }
      function onRenderTextLayer(data) {
      }
      function handledocumentPageClickEvent(e) {
        if(!disableDcoumentComments) {
            setComment('')
            setIsCommentDisable(false)
            editCommentObj = ''
            let commentTop = 0
            let commentLeft = 0
            let PageNumber = 1
            let left = 0
            let top = 0
            let TotalHeight = 0
            if (e.target.classList && e.target.classList.contains('react-pdf__Page__canvas')) {
                PageNumber = e.target.offsetParent.dataset.pageNumber
                left = e.nativeEvent.layerX
                top = e.nativeEvent.layerY
            } else if (e.target.offsetParent && e.target.offsetParent.offsetParent && e.target.offsetParent.offsetParent.classList.contains('react-pdf__Page__canvas')) {
                PageNumber = e.target.offsetParent.offsetParent.dataset.pageNumber
            }
            const pagerefrenceDocu = document.getElementsByClassName('react-pdf__Page')[PageNumber-1].firstChild
            if(e.nativeEvent.layerX > 22 && e.nativeEvent.layerX < Number(pagerefrenceDocu.style.width.replace('px', '')) && e.nativeEvent.layerY < Number(pagerefrenceDocu.style.height.replace('px', ''))-6 && e.nativeEvent.layerY > 10) {
            if (PageNumber != 1) {
                for (let index = 0; index < PageNumber; index++) {
                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index].firstChild
                    if (PageNumber == (index + 1)) {
                        // TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                        break
                    } else if(PIDUSEROBJ.RecipientPageNumbers.includes(index+1)) {
                        TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                    }
                }
                top = TotalHeight + e.nativeEvent.layerY
            }
                setShowAddCommentPopup(true)
            const referId = generateUQID('', 2, "int");
            commentCreateObj = {
                AddCommentId: -1,
                CommentLeft: e.nativeEvent.layerX,
                CommentTop: top,
                CommentTopByPage: top,
                Top:top,
                Left:e.nativeEvent.layerX,
                TopByPage: e.nativeEvent.layerY,
                LeftByPage: e.nativeEvent.layerX,
                PageNumber: PageNumber,
                EmailAddress: PIDUSEROBJ?.EmailAddress,
                IsDelete: false,
                ADocumentId: documentData.ADocumentId,
                RecipientId: PID,
                Height: 0,
                Width: 0,
                referId: referId,
                Comment: ''
            }
        }
        }
    }
    function handledocumentPageMouseDownEvent(e) {
    }
    function SubmitComment() {
        if (!comment) {
            setCommentError('Comment is required')
        } else {
            commentCreateObj.Comment = comment
            setSelectedCommentObj(commentCreateObj)
            addComments.push(commentCreateObj)
            setAddComments([...addComments])
            // if (commentedRecpList.findIndex(data => data.RecipientId == PID) == -1) {
            //     commentedRecpList.push(PIDUSEROBJ)
            //     setcommentedRecpList(commentedRecpList)
            //     if(commentedRecpList.length == 1) {
                    selectedRecipientAvatar(PIDUSEROBJ)
            //     }
            // }
            setShowAddCommentPopup(false)
        }
    }
    function EditComment(index) {
        if (!comment) {
            setCommentError('Comment is required')
        } else {
            for (let index = 0; index < addComments.length; index++) {
                if (addComments[index].AddCommentId != -1 && editCommentObj.AddCommentId && addComments[index].AddCommentId == editCommentObj.AddCommentId) {
                    addComments[index].Comment = comment
                    break
                } else if (addComments[index].referId && editCommentObj.referId && addComments[index].referId == editCommentObj.referId) {
                    addComments[index].Comment = comment
                    break
                }
            }
            setAddComments([...addComments])
            setShowAddCommentPopup(false)
        }
    }

    function removeComment() {
        for (let index = 0; index < addComments.length; index++) {
            if (addComments[index].AddCommentId == -1 && editCommentObj?.referId && addComments[index].referId == editCommentObj.referId) {
                addComments.splice(index, 1)
                break
            } else if (addComments[index].AddCommentId != -1 && editCommentObj.AddCommentId && addComments[index].AddCommentId == editCommentObj.AddCommentId) {
                addComments[index].IsDelete = true
                break
            }
        }
        // if(addComments.findIndex(data=>data.RecipientId == PID && !data.IsDelete) == -1) {
        //    const data =commentedRecpList.filter(data=> data.RecipientId != PID)
        //     setcommentedRecpList(data)
        //     console.log(data, '')
        // }
        setAddComments([...addComments])
        setShowAddCommentPopup(false)

    }
    function CancelCommentPopup() {
        setShowAddCommentPopup(false)
    }
    function onCommentChange(event) {
        setComment(event.target.value)
    }
    function onCommentIconClick(commentObj) {
        editCommentObj = commentObj
        setSelectedCommentObj(commentObj)
        if (editCommentObj.RecipientId != PID) {
            setIsCommentDisable(true)
        } else {
            setIsCommentDisable(false)
        }
        setComment(commentObj.Comment)
        setShowAddCommentPopup(true)
    }
    function selectedRecipientAvatar(item) {
        setselectedCommentRecipient(item)
    }
    function controlTableToggleChange(){
        setIsControlTableClose(isControlTableClose?false: true)
    
      }
      function CommentsTableToggleChange(){
        setIsCommentsTableClose(isCommentsTableClose?false:true)
      }
      const Handelsidebar = () => {
        var element = document.getElementById("sidebar");
            element.classList.toggle("shrink");

            var MainElement = document.getElementById("mainrecipients");
            MainElement.classList.toggle("grow");
            document.getElementById('toggle').classList.remove('display-block')
            document.getElementById('toggle').classList.add('display-none')
            document.getElementById('preview_btn').classList.remove('display-none')
            document.getElementById('preview_btn').classList.add('display-block')
    }
    const Handelsidebar1 = () => {
        var element = document.getElementById("sidebar");
            element.classList.toggle("shrink");

            var MainElement = document.getElementById("mainrecipients");
            MainElement.classList.toggle("grow");
            document.getElementById('toggle').classList.remove('display-none')
            document.getElementById('toggle').classList.add('display-block')
            document.getElementById('preview_btn').classList.remove('display-block')
            document.getElementById('preview_btn').classList.add('display-none')
    }
    return (
        <>
        {IsTermsConditionsEnabled ? <TermsConditions getSubmitAction={submitTermsAndCondition} isChild={true}/>:
                <>
               {!errorExist ? <div className="main-content main-from-content documentagreement document_unauthapprove" style={{overflowX: 'hidden'}}>
                    {/* <div className='container-fluid'>
                        <div className='row'>
                            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                <div className='logo_sec mt-4 mb-4'>
                                    <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block'></img>
                                </div>
                            </div>
                        </div>
                    </div> */}
                   <div className='documentagreement-bg-color'>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='btn-group'>
                                <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block image_zoom'></img>
                                <h2 style={{fontSize: '20px', paddingLeft: '30px'}}>Document Name: {documentData.Title}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    <div className='action_part'>
                        <div className='container-fluid'>
                            <div className='row align-items-center '>
                                <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                    <ul className='info left_part'>
                                        <li><strong>Sender Name: </strong>{documentData.SenderName}</li>
                                        <li><strong>Sent Date: </strong>{documentData.SentDate}</li>
                                        <li><strong>Expiry Date: </strong>{documentData.ExpiryDate ? documentData.ExpiryDate : 'NA'}</li>
                                    </ul>
                                </div>
                                <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                    <ul className='info right_part'>
                                        {/* <li><strong>Pages:</strong> {documentData.TotalPages}</li> */}
                                       <li>
                                            <button type='button' disabled={isUser_Owner} className={(isUser_Owner) ? 'btn_reject button_disabled' : 'btn_reject'} style={{color:'black'}} onClick={rejectPopOpen}>REJECT</button>
                                        </li>
                                        <li className=""  data-tip data-for='button_tip'>
                                            <button type='button' disabled={isUser_Owner} onClick={userDropdown} className={(isUser_Owner) ? 'btn_r-assign button_disabled' : 'btn_r-assign'} style={{color:'black'}}>RE-ASSIGN</button>
                                            <ReactTooltip id="button_tip" place="top" effect="solid">
                                            This feature is under Development</ReactTooltip>
                                            <div id="dropdown_menu" className="dropdown-menu">
                                                <div className="setup_list">
                                                    <h3>Enter All Required Information Below</h3>
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                            <div className="form-group">
                                                                <div className="input-wrapper">
                                                                    <input type="text" autoComplete="off" placeholder="Chris Stallone" />
                                                                    <label htmlFor="">First Name<span>*</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                            <div className="form-group">
                                                                <div className="input-wrapper">
                                                                    <input type="text" autoComplete="off" placeholder="Email Address*" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                            <div className="form-group">
                                                                <div className="input-wrapper">
                                                                    <input type="text" autoComplete="off" placeholder="Phone*" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                            <div className="subf_pagn_btn">
                                                                <button className="btn">SEND</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <button onClick={() => redirectToManagePage()} type='button' className='btn_reject'>Close</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='document_area docAgreement'>
                    <button type="button" onClick={Handelsidebar1} id="preview_btn" className="Preview display-none" style={{marginRight: '-15px', marginTop: '15px'}}>Actions</button>
                        <div className='container-fluid'>
                            <div className='row recipientsmain' id="mainrecipients">
                                <div className='col-xl-7 col-lg-7 col-md-7 col-sm-12  recipients-left-part'>
                                    <div className='main_docs'>
                                        <h5><strong>Pages:</strong>  {scrollPageNumber}/{totalRecipientPageNumberslength}</h5>
                                        <div className='docs'>
                                            <div ref={documentScrollRef} className='prepare-slide_pln prepare-slide_pln2 overflow-documents' id='scroll_area' style={{ backgroundColor: 'darkgrey', minHeight: '500px', scrollBehavior: 'smooth'}} onScroll={GetScrollValue}>
                                                <Document className="row box"  id="mainDoc"
                                                    file={documentFile}
                                                    noData={''}
                                                    loading={loadSpinner}
                                                    onLoadSuccess={onDocumentLoadSuccess}>
                                                    {allControls.map(control => {
                                                        return (
                                                            <>
                                                                {(control.Code == "signature") ? <div key={control.code + control?.CtrlId} >
                                                                <Signature IsDragDisabled={true} onChangeValue={controlOnChangeValue} ControlObj={control} IsResizeDisabled={true}
                                                                selectedControlObj={selectedControl} setSignaturePopShow={setSignaturePopShow} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj} /></div> : null}
                                                                {(control.Code == "date") ? <div key={control.code + control?.CtrlId}><DateSigned selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj} /></div> : null}
                                                                {(control.Code == "payment") ? <div key={control.code + control?.CtrlId}><Payment selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "seal") ? <div key={control.code + control?.CtrlId}><NotarySeal selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "text") ? <div key={control.code + control?.CtrlId}><TextBox selectedControlObj={selectedControl} OnResizeEnd={controlResizeEnd} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "email") ? <div key={control.code + control?.CtrlId}><Email selectedControlObj={selectedControl} OnResizeEnd={controlResizeEnd} onChangeValue={controlOnChangeValue}IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "username") ? <div key={control.code + control?.CtrlId}><Name selectedControlObj={selectedControl} OnResizeEnd={controlResizeEnd} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "companyname") ? <div key={control.code + control?.CtrlId}><Company selectedControlObj={selectedControl} OnResizeEnd={controlResizeEnd} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "image") ? <div key={control.code + control?.CtrlId}><ImageSeal selectedControlObj={selectedControl} setAttachImageControlSetting={changeAttachControlEdit} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "aadhaar") ? <div key={control.code + control?.CtrlId}><Aadhaar selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "digitalsignature") && control.IsDSCVerified==0? <div key={control.code + control?.CtrlId}><DigitalSignature selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == 'dob') ? <div key={control.code + control?.CtrlId}><Dob selectedControlObj={selectedControl} IsDragDisabled={true} onChangeValue={controlOnChangeValue} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "attachment") ? <div key={control.code + control?.CtrlId}><Attachment selectedControlObj={selectedControl} setAttachImageControlSetting={changeAttachControlEdit} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "companyaddress") ? <div key={control.code + control?.CtrlId}><CompanyAddress selectedControlObj={selectedControl} OnResizeEnd={controlResizeEnd} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "useraddress") ? <div key={control.code + control?.CtrlId}><UserAddress selectedControlObj={selectedControl} OnResizeEnd={controlResizeEnd} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "initial") ? <div key={control.code + control?.CtrlId}><Initial selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} setSignaturePopShow={setSignaturePopShow} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                                {(control.Code == "checkbox") ? <div key={control.code + control?.CtrlId}><Checkbox selectedControlObj={selectedControl} onChangeValue={controlOnChangeValue} IsDragDisabled={true} ControlObj={control} IsResizeDisabled={true} getSelectedControl={changeSelectedControl} pageType={true} selectedRecipientObj={LoginRecipientUserObj}/></div> : null}
                                                            </>
                                                        )
                                                    })}
                                         { addComments && addComments.length > 0 ? 
                                            addComments.map(comment => {
                                                return (
                                                    <>
                                                     {!comment.IsDelete && !comment.HideCommentFalse  && selectedCommentRecipient && selectedCommentRecipient.RecipientId == comment.RecipientId ? <div onClick={(e) => { onCommentIconClick(comment)}}  key={comment.referId + comment.AddCommentId }>
                                                         <Comment  commentObj={comment} selectedComment={selectedCommentObj}></Comment>
                                                   </div> : null }
                                                    </>
                                                )
                                                }) : null 
                                            }
                                                    {Array.apply(null, Array(subNumPages))
                                                        .map((x, i) => i + 1).sort((a, b) => a - b)
                                                        .map((page, index) =>
                                                            <>
                                                                <Page onGetTextSuccess={onGetTextSuccessLoaded} onRenderTextLayerSuccess={onRenderTextLayer} noData={''} scale={IsanyoneenableDsc === true?1:1.5} className="doc_agreement_unauth" onRenderSuccess={(e) => {getOnRenderSuccess(e,index, page)}} pageNumber={page} onClick={handledocumentPageClickEvent} onMouseDown={ handledocumentPageMouseDownEvent } />
                                                                {/* <span className='pagenumber-flex'>page {page} of {subNumPages}</span> */}
                                                            </>
                                                        )}
                                                </Document>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-xl-5 col-lg-5 col-md-5 col-sm-12 second_bg recipients-right-part' id="sidebar">
                            <button type="button" onClick={Handelsidebar} id="toggle" className="side-arrow"><img src="src/images/right-arrw.svg" /></button>
                             <div className='side_panel recipients_scroll'>
                                        <ul>
                                            <li data-tip data-for='unauth_save'><button onClick={() => saveDocument()} type='button' className='btn_save'>SAVE</button>
                                            <ReactTooltip id="unauth_save" place="top" effect="solid">
                                            Click Save button to save the values ​​of the controls and you can resume filling other controls upon reopening the document.</ReactTooltip>
                                           </li>
                                            <li><button onClick={() => approveDocument(isuser_hasAadharEsign?'Aadhar' :isuser_hasDscSignCntrl ? 'Dsc': isUser_hasSignCntrl ? 'Sign' : 'Approve')} type='button' className='btn_approve'>{isUser_hasSignCntrl ? 'SIGN' : 'APPROVE'}</button></li>
                                            <li><button onClick={() => WorkflowInfo()} type='button' className='btn_download'>WORKFLOW</button></li>
                                            {/* {isUser_Owner ?  */}
                                            <li><button onClick={() => downloadDocument()} type='button' className='btn_signatory'>DOWNLOAD</button></li>
                                            {/* : ''} */}
                                            {/* <li><button onClick={()=>downloadDocument()} type='button' className='btn_signatory'>Download</button></li> */}
                                        </ul>
                                        <div className='naviga'>
                                            <h6>Navigation</h6>
                                            <ul className='navig'>
                                                <li><button type='button' disabled={upBtn} onClick={()=> {navigateControls('up')}} className={ upBtn ? 'btn_Up button_disabled' :'btn_Up' }>UP</button></li>
                                                <li><button type='button' disabled={downBtn} onClick={()=>{navigateControls('down')}}  className={downBtn ? 'btn_down button_disabled' :'btn_down' }>DOWN</button></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className='bottom_part'>
                                        <div className="progress">
                                            <div className={(progress < 35) ? 'adjust_value_bar progress-bar bar-width-md': "progress-bar bar-width-md"} role="progressbar" aria-valuenow="{progress}" aria-valuemin="0" aria-valuemax="100" style={{width: progress+'%'}}>
                                                {/* <span className="value__bar_sm">25% Complete</span>
                                                <span className="value__bar_md">30% Complete</span>
                                                <span className="value__bar_lg">75% Complete</span>
                                                <span className="value__bar_xl">100% Complete</span> */}
                                                <span className="value__bar_md">{progress}% Complete</span>
                                            </div>
                                        </div>
                                        <div className='table-responsive'>
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th>Page</th>
                                                        <th>Field</th>
                                                        <th>Description</th>
                                                        {!isControlTableClose?<div onClick={() => {controlTableToggleChange()}}><ExpandLessIcon/></div>:
                                                <div onClick={() => {controlTableToggleChange()}}><ExpandMoreIcon/></div>}
                                            </tr>
                                        </thead>
                                      {!isControlTableClose ?  <tbody>
                                            {(allControls && allControls.length > 0) ? allControls.map((item, index) => {
                                                return (
                                                    <>
                                                    {(item.RecipientId == PID || (PIDUSEROBJ?.IsOwner && item.IsInPerson)) ? <tr key={index.toString()}  onClick={()=>{changeTableSelectedControl(item),navigateControls('')}}  className='unauth_table_align'>
                                                        <td className={item.MandatoryErrorExist ? 'error-recipient-border' : ''}>{item.PageNumber}</td>
                                                        <td>{item.DisplayName}</td>
                                                        <td>NA</td>
                                                    </tr> : null}
                                                    </>
                                                )
                                            }) : "No Data available"}
                                        </tbody> : null}        
                                            </table>
                                        </div>
                                        <div style={{marginTop:'10px'}}>
                                {(commentedRecpList && commentedRecpList.length > 0) ? commentedRecpList.map((item, index) => {
                                      return (
                                        <>
                                  <Avatar data-tip data-for={'image_avatar' + item.RecipientId}
                                   onClick={()=>{selectedRecipientAvatar(item)}}
                                    label={item?.FirstName?.charAt(0)?.toUpperCase() + item?.LastName?.charAt(0)?.toUpperCase()}
                                   shape="circle" className={'resp-' +item.Color}
                                    style={{backgroundColor: item.Color, color: 'white', marginLeft: '5px',marginTop: '5px',border:  (selectedCommentRecipient && item.RecipientId == selectedCommentRecipient.RecipientId) ? '2px solid #ffdd00': 'none'}}/>
                                  <ReactTooltip id={"image_avatar" + item.RecipientId} place="top" effect="solid">
                                    {item?.FirstName} {item?.LastName}</ReactTooltip>
                                  </>)}) : null}
                                </div>
                               {/* {!documentData.IsParallelSigning ?
                               (addComments && addComments.length > 0) ?  */}
                               <div className='table-responsive' style={{marginTop:'10px'}}>
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>Page</th>
                                                <th>Comment</th>
                                                { !isCommentsTableClose?<div onClick={() => {CommentsTableToggleChange()}}><ExpandLessIcon/></div>:
                                                <div onClick={() => {CommentsTableToggleChange()}}><ExpandMoreIcon/></div>}
                                            </tr>
                                        </thead>
                                       {!isCommentsTableClose ? <tbody>
                                            {(addComments && addComments.length > 0) ? addComments.map((item, index) => {
                                                return (
                                                    <>
                                                    {( selectedCommentRecipient && (item?.RecipientId == selectedCommentRecipient?.RecipientId) && !item.IsDelete && !item.HideCommentFalse) ? <tr key={index.toString()}  onClick={()=>{onCommentIconClick(item),navigateComments(item)}}  className='unauth_table_align'>
                                                        <td className={item.MandatoryErrorExist ? 'error-recipient-border' : ''}>{item.PageNumber}</td>
                                                        <td data-tip data-for={'comment' + item?.AddCommentId + item?.referId}><p  className='common-overflowdots'>{item.Comment}</p>
                                                        <ReactTooltip id={"comment" + item?.AddCommentId + item?.referId} place="top" effect="solid">
                                                         {item.Comment}</ReactTooltip></td>
                                                    </tr> : null}
                                                    </>
                                                )
                                            }) : "No Data available"}
                                        </tbody> : null }

                                    </table>
                              </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        
                    <Modal show={showWorkflow}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered>
                        <div>
                            <Modal.Header  >
                                <Modal.Title id="example-modal-sizes-title-lg">
                                    <p> Workflow</p>
        
                                </Modal.Title>
                                <button type="button" className="close" onClick={() => { setShowWorkflow(false) }}
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </Modal.Header>
                            <Modal.Body style={{overflow:'auto'}} className='error-modal-content'>
                                <table className="table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">S No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope='col'>Role</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {CCAndNonccRecipients.length > 0 ? CCAndNonccRecipients.map((item, index) => {
                                            return (
                                                <tr key={index + 1}>
                                                    <td>{index + 1}</td>
                                                    <td>{item.FullName}</td>
                                                    <td>{item.EmailAddress}</td>
                                                    <td>{item.ReassignStatus==1?item.ReasonRecipientRole: item.RecipientRole}</td>
                                                    <td className='flex justify-content-start'>{item.ReassignStatus==1?"Reassigned":(documentData.StatusCode== "CANCELLED"&&documentData.IsParallelSigning==0&&item.StatusCode=='SENT')||(documentData.StatusCode== "REJECTED"&&documentData.IsParallelSigning==0&&item.StatusCode=='SENT')?'NA':item.StatusCode}
                                                    {item.ReassignStatus==2?<><div> <img data-tip data-for={`${index}`} src="src/images/info.svg" alt='' /></div>
                                                                    <ReactTooltip id={`${index}`} place="left" effect="solid">
                                                                        <div>Reassigned by : {item.ReassignMember}</div>
                                                                    </ReactTooltip></>:""}
                                                    </td>
                                                </tr>
                                            )
                                        }) : "No Data available"}
                                    </tbody>
                                </table>
                            </Modal.Body></div>
                        <Modal.Footer>
                            <Button className="btn btn-secondary" style={{backgroundColor:'rgb(240, 107, 48)',border:'rgb(240, 107, 48)'}} onClick={() => { setShowWorkflow(false) }}>OKAY</Button>
                        </Modal.Footer>
                    </Modal>
        
                    <Modal show={signaturePopShow} onHide={() => { setSignaturePopShow(false) }}
                        size="lg"
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton>
                            <Modal.Title id="example-modal-sizes-title-lg">
                                Signature
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '350px', overflow: "hidden auto" }} >
                            <SignatureControlEditPopup ref={childCompRef} setSelectedSignatureControlEditData={setSelectedSignatureControlEditData}
                                selectedcontrolObj={selectedControl} selection={selection} setSelection={setSelection} errorStatus={errorStatus}
                                 setErrorStatus={setErrorStatus} disableManage={true} pageType={'unauth'} />
                        </Modal.Body>
                        <Modal.Footer style={{ justifyContent: 'center' }}>
                            {/* <p>I understand this is legal representation of my initials</p> */}
                            {/* <Button variant="secondary" style={{background: 'rgb(36, 99, 209)', borderColor: 'none'}} onClick={() => {  signSelectionHandler()}}>Apply</Button> */}
                            {/* <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#d10d03', border: 'none', lineHeight: '2'}} onClick={() => {  deleteHandler() }}>Delete</Button>
                            <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#F06B30', border: 'none', lineHeight: '2'}} onClick={() => {  childCompRef.current.clear() }}>Reset</Button> */}
                            <Button variant="secondary" style={{ background: 'rgb(36, 99, 209)', border: 'none', lineHeight: '2' }} onClick={() => { signSelectionHandler() }}>APPLY</Button>
        
                        </Modal.Footer>
                    </Modal>
        
                    <Modal show={uploadPopShow} onHide={() => { setUploadPopShow(false) }}
                        size="lg"
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{}}>
                        {isControlImage ? <Modal.Title id="example-modal-sizes-title-lg" style={{}}><div>
                        Upload the image <img data-tip data-for="addPageNumber" src="src/images/info.svg" alt='' /></div>
                        <ReactTooltip id="addPageNumber" place="top" effect="solid">
                        <div>Supported Files: .JPG,.JPEG,.PNG</div>  
                         </ReactTooltip>
                            </Modal.Title>: <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                            <div>
                    Upload the attachment <img data-tip data-for="addPageNumber" src="src/images/info.svg" alt='' /></div> 
                    <ReactTooltip id="addPageNumber" place="top" effect="solid">
                        <div>Supported Files: .JPG,.JPEG,.PNG,.PDF.</div>  
                         </ReactTooltip>
                            </Modal.Title>}
                            {!isImageFile && attachImageFile ? <FullscreenIcon onClick={()=>{handle.enter()}} style={{cursor:"pointer",marginLeft:'20px'}}/> : null}
                        </Modal.Header>
                        <Modal.Body style={{ height: '350px', overflow: "hidden auto" }}>
                        {failedMessage ?
                                <div className='card' style={{ marginBottom: '10px', padding: '10px', color: 'red' }}>
                                    <div>
                                        <button type="button" className="btn-close" aria-label="Close" style={{ float: 'right', zoom: '0.6' }} onClick={() => { handleControlErrorClick() }}></button>
                                    </div>
                                    {isControlImage ? <div >
                                        <p style={{ lineHeight: '1.5', fontSize: '14px' }}>The maximum size of file is 1MB</p>
                                        <p style={{ lineHeight: '1.5', fontSize: '14px' }}>Only the following file formats are acceptable: .jpeg, .jpeg, .png</p>
                                    </div> : null
                                    }
                                    {!isControlImage ? <div >
                                        <p style={{ lineHeight: '1.5', fontSize: '14px' }}>The maximum size of file is 2MB</p>
                                        <p style={{ lineHeight: '1.5', fontSize: '14px' }}>Only the following file formats are acceptable: .jpeg, .jpeg, .png, .pdf</p>
                                    </div> : null
                                    }
                                </div> : null}   <div className='card' >
                                <div className='chucksum' style={{ justifyContent: 'center', display: 'flex', minHeight: '300px'}}>
                                    {!IsFileUploaded ? <div className='file-add file-upload use_templete drop-zone' style={{ outline: 'none', width: '300px', height: '300px' }}>
                                        <div className="drop-zone__prompt">
                                            <input type="file" onClick={(event) => { event.target.value = null }} accept={acceptedFiles} name="myFile" className="drop-zone__input dragUpload" />
                                        </div>
                                        <div className="file-add-img-wrapper">
                                            <img src="./src/images/file-add.svg" alt="file-add" className='file-add-img' style={{ height: '150%' }} />
                                        </div>
                                        <p className='file-add-text'>Drop your files here or</p>
                                        <div style={{ cursor: 'pointer', position: 'relative' }}>
                                            <button className='file-upload-btn' onClick={(e) => {}}>UPLOAD
                                                <span className='file-add-btn-arr'><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                                                    <path strokeLinecap="round" strokeLinejoin="round" d="M5 10l7-7m0 0l7 7m-7-7v18" />
                                                </svg>
                                                </span>
                                            </button>
                                        </div>
                                    </div> : null}
                                    {IsFileUploaded ?
                                        <div>
                                            {(attachImageFile && !failedMessage && isImageFile) ?
                                                <div style={{ justifyContent: 'center' }}>
                                                    {/* <div>
                                                        <button type="button" className="btn-close" aria-label="Close" style={{ float: 'right', zoom: '0.6' }} onClick={() => { clearFileInfo() }}></button>
                                                    </div> */}
                                                    <p style={{ marginRight: '10px', padding: '0.5rem' }}> {fileName}</p>
                                                    {/* <div className='pop-doc' > */}
                                                    <FullScreen handle={handle} >
                                            <div className='pop-doc'  style={{maxHeight:handle.active?"100vh":null}}>
                                                <img style={{ padding: '0.5rem' }} src={attachImageFile}></img>
                                                </div>
                                            </FullScreen>
                                                    {/* </div> */}
                                                </div> : null}
                                            {!isImageFile && attachImageFile && !failedMessage ?
                                                <div style={{ justifyContent: 'center' }}>
                                                    {/* <div>
                                                        <button type="button" className="btn-close" aria-label="Close" style={{ float: 'right', zoom: '0.6' }} onClick={() => { clearFileInfo() }}></button>
                                                    </div> */}
                                                    <p style={{ marginRight: '10px', padding: '0.5rem' }}> {fileName}</p>
                                                    <FullScreen handle={handle} >
                                            <div className='pop-doc'  style={{maxHeight:handle.active?"100vh":null}}>
                                                        <Document className="row"
                                                            file={attachImageFile}
                                                            noData={''}
                                                            loading={loadSpinner}
                                                            onLoadSuccess={ControlpopDocLoad}>
                                                            {Array.apply(null, Array(controlPopDocPages))
                                                                .map((x, i) => i + 1)
                                                                .map(page =>
                                                                    <>
                                                                        <Page noData={''} scale={1.5} className="subdocument_intial mx-auto d-block" pageNumber={page} />
                                                                        <span className='pagenumber-flex'>page {page} of {controlPopDocPages}</span>
                                                                    </>
                                                                )}
                                                        </Document>
                                                    </div>
                                                    </FullScreen>

                                                </div> : null}
                                        </div> : null}
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}>
                           {/* <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#d10d03', border: 'none', lineHeight: '2'}} onClick={() => {  deleteHandler() }}>Delete</Button> */}
                            <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#F06B30', border: 'none', lineHeight: '2'}} onClick={() => {clearFileInfo() }}>CLEAR</Button>
                            <Button variant="secondary" style={{background: 'rgb(36, 99, 209)', border: 'none', lineHeight: '2'}} onClick={() => { UploadImageAttachment()}}>APPLY</Button>
        
                        </Modal.Footer>
                    </Modal>
        
                    <Modal show={rejectPopShow} onHide={() => { setRejectPopShow(false);setRejectError('') }}
                        size=""
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{  }} className='p-speeddial-direction-up'>
                            <Modal.Title id="example-modal-sizes-title-lg" >
                            Reject Document
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '200px', overflow: "hidden" }} >
                            <div>
                                <div id="sharedoc" className='prepare_document submit_popup share_document' style={{padding: '1rem'}}>
        
                                    {/* </div> */}
                                    <div className='row'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                            <div className='share_document_form'>
                                                <form>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label style={{ fontSize: '18px' }}>Reason for rejection</label>
                                                                <textarea maxLength="300" onChange={onRejectChange} placeholder='' />
                                                               {rejectError ? <span className='error-message' >{rejectError}</span> : null}
                                                            </div>
                                                        </div>
                                                    </div>
        
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer style={{ justifyContent: 'center' }}>
                            <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { setRejectPopShow(false); setRejectError('') }}
                                className='submit_cancel_btn'>CANCEL</Button>
                            <Button disabled={!rejectReason} style={{ width: '7rem', backgroundColor: 'rgb(240, 107, 48)', borderColor: 'rgb(240, 107, 48)' }} onClick={() => { rejectSubmitHandler() }} className='submit_send_btn'
                            >REJECT</Button>
                        </Modal.Footer>
                    </Modal>
        
                    <Modal show={rejectedSuccessPopShow} onHide={() => {  rejectSuccessfulOkHandler() }}
                        size=""
                        dialogClassName=""
                        backdrop="static"
                        keyboard={false}
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{}}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Success
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div">
                                                <p style={{ fontSize: '16px' }}>Successfully rejected the document</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}>
                            <Button variant="secondary" style={{ background: '#F06B30', border: 'none', }} onClick={() => { rejectSuccessfulOkHandler() }}>OKAY</Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal show={validPopShow} onHide={() => { setValidPopShow(false) }}
                        size=""
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{}}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Validations
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div">
                                                <p style={{ fontSize: '16px' }}>Some of the mandatory field(s) are left empty</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}> 
                            <Button variant="primary" style={{ background: '#F06B30', width: '7rem', border: 'none', }} onClick={() => { setValidPopShow(false) }}>OKAY</Button>
                        </Modal.Footer>
                    </Modal>

                    
                    <Modal show={alreadySign} onHide={() => { setAlreadySign(false)}}
                        size=""
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{}}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Validations
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div">
                                                <p style={{ fontSize: '16px' }}>You have already signed this document</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}> 
                            <Button variant="secondary" style={{ background: '#F06B30', width: '7rem', border: 'none', }} onClick={() => { setAlreadySign(false) }}>OKAY</Button>
                        </Modal.Footer>
                    </Modal>

                    <Modal show={confirmPopShow} onHide={() => { setConfirmPopShow(false) }}
                        size=""
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{ }}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Confirm
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div" style={{marginBottom:'10px'}}>
                                                <p style={{ fontSize: '16px',lineHeight: '1.5'}}>
                                                You have successfully filled information in all the fields. Click on {!isUser_hasSignCntrl ? 'Approve' : 'Sign'} to execute the document</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}>
                            <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { setConfirmPopShow(false) }}
                                className='submit_cancel_btn'>CANCEL</Button>
                            <Button style={{ width: '7rem'}} onClick={() => { submitSign() }} className='submit_send_btn'
                            >{!isUser_hasSignCntrl ? 'APPROVE' : 'SIGN'}</Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal show={approvePopShow} onHide={() => { OkHandler()  }}
                        size=""
                        dialogClassName=""
                        backdrop="static"
                        keyboard={false}
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header style={{}}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Document Approved
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '160px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card model-card-alignment container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div">
                                                <p style={{ fontSize: '16px',lineHeight: '1.5' }}>You have successfully approved the document. After all the recipients approve the document, the final pdf will be shared.</p>
                                                <p style={{ fontSize: '16px',lineHeight: '1.5',justifyContent:'center', display:'flex', marginTop:'30px' }}>Click the "Download" link to download the document.</p>
                                                <div className='download-btn-align'>
                                                <p style={{cursor:'pointer', color: '#F06B30'}} onClick={() => { downloadDocument() }}><i className="fa fa-download" aria-hidden="true" style={{marginRight:'10px'}}></i>Download</p>
                                                <p className='redirect-align'  style={{cursor:'pointer', color: '#F06B30'}} onClick={() => { OkHandler() }}>Redirect to home page </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}>
                           <Button variant="primary" style={{ width: '7rem'}} onClick={() => { OkHandler() }}
                                className='submit_cancel_btn'>OKAY</Button>
                        </Modal.Footer>
                    </Modal>
        
                    <Modal show={confirmWithoutControlsPopShow} onHide={() => { setConfirmWithoutControlsPopShow(false)}}
                        size=""
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{ }}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Confirm
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div" style={{marginBottom:'10px'}}>
                                                <p style={{ fontSize: '16px',lineHeight: '1.5'}}>Are you sure you want approve this document?</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}>
                            <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { setConfirmWithoutControlsPopShow(false) }}
                                className='submit_cancel_btn'>CANCEL</Button>
                            <Button style={{ width: '7rem'}} onClick={() => { submitSign() }} className='submit_send_btn'
                            >APPROVE</Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal show={confirmWithControlsPopShow} onHide={() => {  setConfirmWithControlsPopShow(false) }}
                        size=""
                        dialogClassName=""
                        aria-labelledby="example-modal-sizes-title-lg"
                        centered>
                        <Modal.Header closeButton style={{ }}>
                            <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                                Confirm
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                            <div className="container-fluid">
                                <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                                    <div className="example-card container">
                                        <div className="row">
                                            <div className="col-md-12 add-recipients-div" style={{marginBottom:'10px'}}>
                                                <p style={{ fontSize: '16px',lineHeight: '1.5'}}>You have successfully filled information in all the fields. Click on Approve to save and approve or Click on Cancel to review.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className='justify-content-center' style={{}}>
                            <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { setConfirmWithControlsPopShow(false) }}
                                className='submit_cancel_btn'>CANCEL</Button>
                            <Button style={{ width: '7rem'}} onClick={() => { submitSign() }} className='submit_send_btn'
                            >APPROVE</Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal show={submitSaveDocument}  onHide={() => { setsubmitSaveDocument(false) }}
                size=""
                // backdrop="static"
                // keyboard={false}
                dialogClassName=""
                // aria-labelledby="contained-modal-title-vcente"
                centered>
                <Modal.Header style={{justifyContent:"center"}}>
                    <Modal.Title id="contained-modal-title-vcente">
                        <div className="success-animation">
                            <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" /></svg>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                      <div>
                      <p style={{lineHeight: '1.5',textAlign: 'center'}}>Data saved successfully.</p>
                      </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center'>
                    {/* <Button variant="primary" onClick={() => { setDocmodalShow(false) }}>Submit</Button> */}
                    {/* <p>I understand this is legal representation of my initials</p> */}
                    <Button variant="secondary" style={{background: '#F06B30', border: 'none'}} onClick={() => {setsubmitSaveDocument(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={submitOtpDocument}  onHide={() => { setsubmitOtpDocument(false) }} style={{width: '70%'}}
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName="medium_size_dailog"
                // aria-labelledby="contained-modal-title-vcente"
                centered>
                <Modal.Header style={{justifyContent:"center"}}>
                    <Modal.Title id="contained-modal-title-vcente">
                        <div>
                            <p>Validate OTP</p>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                <div style={{ justifyContent: 'center', display: 'flex'}}>
                <OTPInput className="row"  inputClassName="otp_input" value={otpValue} onChange={OTPChange}
                     autoFocus OTPLength={4} otpType="number"
                    disabled={false} inputStyles={{width: "50px", height: "50px",
                     textAlign: "center", marginRight: "10px",fontSize: "32px", padding: '13px', color:"#2F7FED"}} />
                </div>
                    {otpError ? <span className='error-message' style={{display: 'flex',justifyContent: 'center',marginTop: '10px'}}>
                           {otpError}</span> : null}
                </Modal.Body>
                <Modal.Footer className='justify-content-center'>
                    <Button variant="secondary" type="button" disabled={(OTPValueLength == 4) ? false : true} style={{background: '#F06B30', border: 'none'}}  onClick={verifyOTP}>{('VERIFY')}</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={FacialPopup} 
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Body style={{ height: '210px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card container">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div" style={{ marginBottom: '10px' }}>
                                        {(!PIDUSEROBJ?.ProfilePicture) ? <><p style={{fontSize: '16px',lineHeight: '1.5'}}>To sign this document, the sender has enabled Facial Recognition and Photo Verification conditions to validate the identity of the signatory.</p><br></br>
                                        <p style={{fontSize: '16px',lineHeight: '1.5'}}>Since the system does not have the reference image of the signatory, the actual image shall be printed in the audit trail for future reference.</p><br></br>
                                        <p style={{fontSize: '16px',lineHeight: '1.5'}}>Please click on Verify button to proceed further.</p></> :
                                        <>{(PIDUSEROBJ?.IsPhotoRecognition && PIDUSEROBJ?.IsFacialRecognition) ? <p style={{fontSize: '16px',lineHeight: '1.5'}}>To sign this document, you need to undergo Photo Verification and Facial Recognition process. Your photo shall be displayed in the Audit Trail for future reference.</p>
                                        : PIDUSEROBJ?.IsPhotoRecognition ? <p style={{fontSize: '16px',lineHeight: '1.5'}}>To sign this document, you need to undergo Photo Verification process. Your photo shall be displayed in the Audit Trail for future reference.</p> : 
                                        <p style={{fontSize: '16px',lineHeight: '1.5'}}>To sign this document, you need to undergo Facial Recognition process. Your photo shall be displayed in the Audit Trail for future reference.</p>}
                                        <br></br>
                                       <p style={{fontSize: '16px',lineHeight: '1.5'}}>Please click on Verify button to proceed further.</p></>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{border:'none'}}>
                    {/* <Button variant="secondary" style={{}} onClick={() => { }}
                        className='submit_cancel_btn'>Upgrade</Button> */}
                    <Button style={{}} onClick={() => { goToFacialPage() }} className='submit_send_btn'
                    >VERIFY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={Aadharconformpopup} 
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                      <Modal.Header style={{}}>
                    <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    CONFIRM
                    </Modal.Title>
                    <button type="button" className="close" onClick={() => { setAadharconformpopup(false) }}
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                </Modal.Header>
                <Modal.Body style={{ height: '210px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card container">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div" style={{ marginBottom: '10px' }}>
                                    <p style={{fontSize: '16px',lineHeight: '1.5'}}>This document requires you to sign using Aadhaar E-Sign issued by NSDL.</p>
    					          	        <p style={{fontSize: '16px',lineHeight: '1.5'}}>Please click on the 'Sign' button for redirecting to NSDL site for signing the document.</p> 
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}>
                   {/* <Button variant="secondary" style={{ width: '7rem',background: '#F06B30', border: 'none'}} onClick={() => { setApprovePopShow(false), setErrorExist(true), setErrorStatueCode('APPROVED') }}
                        className='submit_cancel_btn'>OKAY</Button> */}
                        <div id="addAadharContent"></div>
                </Modal.Footer>
                
            </Modal>
            <Modal show={dscconformpopup} 
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                      <Modal.Header style={{}}>
                    <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    CONFIRM
                    </Modal.Title>
                    <button type="button" className="close" onClick={() => { setdscconformpopup(false) }}
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                </Modal.Header>
                <Modal.Body style={{ height: '210px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card container">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div" style={{ marginBottom: '10px' }}>
                                    <p style={{fontSize: '16px',lineHeight: '1.5'}}>This document requires you to sign using DSC issued by eMudhra.</p>
    					          	        <p style={{fontSize: '16px',lineHeight: '1.5'}}>Please click on the 'Sign' button for redirecting to eMudhra site for signing the document.</p> 
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}>
                   {/* <Button variant="secondary" style={{ width: '7rem',background: '#F06B30', border: 'none'}} onClick={() => { setApprovePopShow(false), setErrorExist(true), setErrorStatueCode('APPROVED') }}
                        className='submit_cancel_btn'>OKAY</Button> */}
                        <div id="addDSCContent"></div>
                </Modal.Footer>
                
            </Modal>
            <Modal show={showAddCommentPopup} onHide={() => { CancelCommentPopup() }}
                size=""
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton style={{  }} className='p-speeddial-direction-up'>
                    <Modal.Title id="example-modal-sizes-title-lg" >
                   {!editCommentObj? 'Add comment' : isCommentDisable ? 'View comment' : 'Edit comment' } 
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: '200px', overflow: "hidden" }} >
                    <div>
                        <div id="sharedoc" className='prepare_document submit_popup share_document' style={{padding: '1rem'}}>

                            {/* </div> */}
                            <div className='row'>
                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                    <div className='share_document_form'>
                                        <form>
                                            <div className='row'>
                                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                    <div className='form-group'>
                                                        {!isCommentDisable ? <label style={{ fontSize: '18px' }}>Please add your comment</label> : null}
                                                        <textarea disabled={isCommentDisable} value={comment}  maxLength="300" onChange={onCommentChange} placeholder='' />
                                                       {commentError ? <span className='error-message' >{commentError}</span> : null}
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{ justifyContent: 'center' }}>
               {editCommentObj && editCommentObj.RecipientId == PID ?  <Button style={{ width: '7rem', backgroundColor: '#d10d03', border: 'none)' }}
                     onClick={() => { removeComment() }} className='submit_send_btn'
                    >DELETE</Button> : null} 
                    <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { CancelCommentPopup() }}
                        className='submit_cancel_btn'>CANCEL</Button>
                   { !isCommentDisable ? <Button disabled={!comment} style={{ width: '7rem', backgroundColor: 'rgb(240, 107, 48)', borderColor: 'rgb(240, 107, 48)' }}
                     onClick={() => { editCommentObj ? EditComment() : SubmitComment() }} className='submit_send_btn'
                    >INSERT</Button> : null}
                </Modal.Footer>
            </Modal>
            <Modal show={showdonthavepermissionpopup} 
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Body style={{ height: '210px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card container">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div" style={{ marginBottom: '10px' }}>
                                    <p style={{fontSize: '16px',lineHeight: '1.5'}}>This document is in Sequential Flow,  You dont have access till your turn comes. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}>
                   <Button variant="secondary" style={{ width: '7rem',background: '#F06B30', border: 'none'}} onClick={() => {redirectToManagePage() }}
                        className='submit_cancel_btn'>OKAY</Button>
                </Modal.Footer>
                
            </Modal>
                    <ToastContainer />
        
                </div> :
        
                  <div className="main-content main-from-content documentagreement">
        <div className='documentagreement-bg-color'>
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                        <div className='btn-group' style={{ padding: '10px' }}>
                            <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block' style={{ zoom: '0.5' }}></img>
                            {/* <h2 style={{fontSize: '20px', paddingLeft: '30px'}}>View Document Master Agreement</h2> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container-fluid">
            <div className="justify-content-center align-items-center min-height card-img-overlay" style={{ position: 'relative' }}>
                <div className="example-card card-align" style={{ borderRadius: '10px' }}>
                    <div className="row">
                        <div className="col-md-12 add-recipients-div">
                           {errorStatueCode == "APPROVED" ? <p style={{wordBreak: 'break-all'}}>Your document <b>{documentTitle}</b> is approved.</p> : null}
                           {errorStatueCode == "EXPIRED" ? <p style={{wordBreak: 'break-all'}}>Your document <b>{documentTitle}</b> has been expired on {expirationDate}.</p> : null}
                           {errorStatueCode == "REJECTED" ? <div> <p style={{wordBreak: 'break-all'}}>This document has been rejected by <b>{ErrorRejectResp}</b> for the reason {ErrorRejectReason}. You are not required to sign this document, please reach out to the Sender for further action.</p> </div>: null}
                           {errorStatueCode == "CANCELLED" ?<div> <p style={{wordBreak: 'break-all'}}>This document has been Cancelled.</p> </div> : null}
                        </div>
                    </div>
                </div>
            </div>
                            </div>
                        </div>
                    }
                    </>
                    }
                     </>
        
            )
}

export { DocumentAgreement }; 