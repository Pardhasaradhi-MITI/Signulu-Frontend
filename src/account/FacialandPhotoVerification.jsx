import React, { useEffect, useState, useRef } from 'react';
import { Link, useLocation } from 'react-router-dom';
import '../css/facialandphotoverification.css'
import { accountService, alertService } from '@/_services';
import { commonService } from '../_services/common.service';
import { useHistory, useParams } from 'react-router-dom';
import { dataURItoBlob } from '../_services/model.service';
import { Modal, Button } from 'react-bootstrap';
import config from 'config';
let DocId = '';
let PID = '';
let fileName = null;
let redirectURL = '';
let GUID = '';
let domain = '';
let captureImage = ''
function FacialandPhotoVerification({ history, location }) {
    let { id } = useParams();
    const search = useLocation().search;
    const searchParams = new URLSearchParams(search);
    if (searchParams.get("docId")) {
        DocId = searchParams.get("docId")
    }
    if (searchParams.get("recId")) {
        PID = searchParams.get("recId")
    }
    if (searchParams.get("fileName")) {
        fileName = searchParams.get("fileName")
    }
    if (searchParams.get("redirect_url")) {
        redirectURL = searchParams.get("redirect_url")
        domain = redirectURL
        domain = new URL(config.fileServerUrl).origin
        console.log(domain.origin, 'domaindomain')
    }
    if (searchParams.get("guid")) {
        GUID = searchParams.get("guid")
    }
    domain = new URL(config.fileServerUrl).origin
    console.log(domain.origin, '@domaindomain')
    const [captures, setCaptures] = useState([]);
    const [error, setError] = useState('');
    const [isCaptured, setIsCaptured] = useState();
    const [captureResults, setCaptureResults] = useState([]);
    const webcamRef = useRef(null)
    const videoRef = useRef(null)
    const canvasRef = useRef(null)
    const [showFacialSuccessPopUP,setShowFacialSuccessPopUP]= useState(false)
    const [showFacialFailurePopUP,setShowFacialFailurePopUP]= useState(false)
    const usehistory = useHistory();
    const [stopVideotag,setStopVideo]= useState(false)

    useEffect(() => {
        getVideo();
    }, [videoRef]);

    const getVideo = () => {
        navigator.mediaDevices.getUserMedia({ video: true })
            .then(stream => {
                let video = videoRef.current;
                video.srcObject = stream;
                video.play();
            })
            .catch(err => {
                console.error("error:", err);
            });
    };

    function getCaptureImage() {
        setError('')
        const DataImage = canvasRef.current.getContext("2d").drawImage(videoRef.current, 0, 0,700, 700);
        const captureImage = canvasRef.current.toDataURL("image/png")
        verifyImages(captureImage)
    }
    function verifyImages(captureImage) {
        const imageBlob = dataURItoBlob(captureImage)
        var obj = {
            UserImage: captureImage,
            MatchScore: 0
        }
        commonService.getFacedoubleComapreMatch({FaceContent:captureImage}).then(reponse => {
            console.log(reponse, 'datadata')
            if(reponse.Entity && reponse.Entity.FaceCount == 1 ) {
        commonService.getFaceComapreMatch(imageBlob, fileName, domain).then(data => {
            console.log(data, 'data')
            if ((data.successful && data.matchScore > 0) || !fileName || fileName == null || fileName == 'null') {
                var obj = {
                    UserImage: captureImage,
                    MatchScore: data.matchScore
                }
                let capturesArray = [...captures]
                capturesArray.push(obj)
                setCaptures(capturesArray)
                if (capturesArray && capturesArray.length > 2) {
                    stopVideo()
                }
            } else {
                setError('The captured image does not conform to standards. Please take another picture and ensure your face is clearly visible without any distortions')
            }
        }).catch(error => {

        });
    } else {
        setError('The captured image does not conform to standards. Please take another picture and ensure your face is clearly visible without any distortions')
    }
    }).catch(error => {

    });
    }

    function stopVideo() {
        videoRef.current.srcObject.getTracks().forEach(track => {
            if (track.readyState == 'live' && track.kind == 'video') {
                track.stop();
            }
        });
        setStopVideo(true)
    }
    function removeCaptureImage(index) {
        const capImages = [...captures]
        if (capImages.length == 3) {
            setStopVideo(false)
            getVideo()
        }
        capImages.splice(index, 1)
        setCaptures(capImages)
        setError('')

    }
    function verified() {
        var srcElement = document.getElementById('verify');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    function submitData() {

        let slu_obj = {
            "RecipientId": PID,
            "ADocumentId": DocId,
            "UserGUID": GUID,
            "UserImages": []
        }
        const capImages = [...captures]
        let havingAccuratePic = false
        capImages.forEach((element) => {
            if (element.MatchScore > 0.75 || !fileName || fileName == 'null' || fileName == null) {
                havingAccuratePic = true
            }
        });
        slu_obj.UserImages = [...capImages]

        if (havingAccuratePic) {
            commonService.getFaceSubmit(slu_obj).then(data => {
                setShowFacialSuccessPopUP(true)
            }).catch(error => {
                setShowFacialFailurePopUP(true)
                setError('Your identity does not match with our records. Please entry again')
                console.log(error, 'submitData submitData')
            });
        } else {
            setShowFacialFailurePopUP(true)
            setError('Your identity does not match with our records. Please entry again')
        }

    }

    function goToDocument() {
        console.log('&&&&&&', redirectURL)
        usehistory.push(redirectURL);
    }

    return (
        <div className="dashboard_home wrapper_other main-content main-from-content documentagreement document_unauthapprove" id="wrapper_other">
             <div className='documentagreement-bg-color'>
                <div className='container-fluid' style={{padding:'15px'}}>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='btn-group'>
                                <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block image_zoom'></img>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <Sidebar /> */}
            <div className="other_main" id="main">
                {/* <Topbar /> */}

                <div className="photoverification">
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <h3 className="sec-title">Facial Recognition</h3>
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <p className="sec-title text-center" style={{marginBottom:"10px",  color: '#307fed'}}>Please ensure your face is clearly visible in the captured image. Else, the signature will be considered invalid</p>
                    </div>
                    <div className="row">
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div className={(captures && captures.length == 3) ? "top-panel top-panel-with-images": "top-panel"}>
                                <div className="picture">
                                    <img src="src/images/cameranew.svg" />
                                </div>
                                <p>You are required to submit 3 images for the system to verify your identity.</p>
                                <div className="capture_btn"><button onClick={() => { getCaptureImage() }} className="btn" disabled={captures && captures.length == 3  ? true : false}>{captures && captures.length > 0 ? 'Take Another' : 'Take a capture'}</button></div>
                                <div className="count-capture">Images <span>{captures ? captures.length : 0}/3</span></div>
                            </div>
                            <div className="bottom-panel">
                                <div className={captures && captures.length == 3 ? "picture-thumb": 'picture-thumb'}>
                                    {captures && captures.length > 0 ? captures.map((imageObj, index) => {
                                        return (<div className="screenshot">
                                            <img src={imageObj?.UserImage} style={{width:'150px'}} />
                                            <div className={(captures && captures.length == 3) ? "delete-icon delete-icon-with-images" : "delete-icon" }>
                                                <img src="src/images/delete_icon.svg" onClick={() => removeCaptureImage(index)} />
                                            </div>
                                        </div>)
                                    }) : null}
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                            <div className="video-record">
                            {captures && captures.length == 3 ?
                            <div className="align" style={{position: "relative"}}><> <h5>{(!fileName || fileName == null || fileName == 'null') ? '':'We have captured 3 images for verification.'} <span>Click on Submit</span> to confirm.</h5>
                            <div className="submit_btn"><button type="button" disabled={(captures && captures.length == 3) ? false : true} className="btn" onClick={submitData}>Submit</button></div>
                            </>
                            </div>: null }
                            {!stopVideotag ? <video ref={videoRef} style={{width: '100%'}} />: null }
                                <canvas width={700} height={700} style={{ visibility: 'hidden' }} ref={canvasRef} />
                            </div>
                            <div  style={{marginTop:"10px"}}>
                            {error ?
                             <p className="text-center" style={{color:'red'}}>The captured image does not match with the identify of the signatory</p> : null}
                           </div>
                        </div>
                        
                    </div>

                    {/* <div className= "row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12" style={{marginTop:"10px"}}>
                            {error ? <p className="text-center" style={{color:'red'}}>The captured image does not match with the identify of the signatory</p>: null}
                           
                        </div>
                    </div> */}
                    <div id="verify" className="verified-msg" style={{ display: 'none' }}>
                        <div className="content">
                            <div className='top-heading'>
                                <span>
                                    <img src="src/images/verify.png" />
                                </span>
                                <h3>Verified</h3>
                            </div>
                            <p>Your identity is sucessfully verified and you can proceed with the signature of the document.</p>
                            <div className="submit_btn"><button className="btn">Go to Document</button></div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={showFacialSuccessPopUP} 
                size=""
                dialogClassName=""
                backdrop="static"
                keyboard={false}
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header style={{border: 'none'}}>
                    <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    <img style={{height: '35px', marginRight: '20px'}} src="src/images/verify.png" />
                    {(!fileName || fileName == null || fileName == 'null') ? 'Submitted':'Verified'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: '90px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card ">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div">
                                    {(!fileName || fileName == null || fileName == 'null') ? <p style={{lineHeight: '1.5' }}>The images captured have been submitted sucessfully. please contact the sender for any clarifications.</p>
                                    :<p style={{lineHeight: '1.5' }}>Your identity is successfully verified and you can proceed with the signature of the document</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{border: 'none'}}>
                    <Button variant="secondary" style={{ background: '#F06B30', border: 'none'}} onClick={() => { goToDocument() }}>GO TO DOCUMENT</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showFacialFailurePopUP} onHide={() => {  setShowFacialFailurePopUP(false) }} 
                size=""
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton style={{border: 'none'}}>
                    <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    <img style={{height: '35px', marginRight: '20px'}} src="src/images/failed.png" />
                    Failed
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: '90px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card ">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div">
                                        <p style={{lineHeight: '1.5' }}>Your identity does not match with our records. Please entry again</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{border: 'none'}}>
                    <Button variant="secondary" style={{ background: '#F06B30', border: 'none'}} onClick={() => { setShowFacialFailurePopUP(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
        </div>


    )
}

export { FacialandPhotoVerification }; 