import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import {authService} from "../_services/auth.service";
import {usersService} from "../_services/users.service";



function CloudStorage({ history, location }) {

    const [getUserInfo,setUserInfo]=useState({})

    useEffect(() => {
        getUser()
    },[]);




    function googleConect() {
        var now = new Date();
        now.setTime(now.getTime() + 1 * 300 * 1000);
        document.cookie = "IsReact=1" + ";expires=" + now.toUTCString() + ";path=/";
        return authService.get('google/authurl/1').then(res => {
            window.location.replace(res.Entity.AuthUrl);
            return res.Entity.AuthUrl;
        });
    }

    function googleDisconect() {
        return authService.get('google/delink').then(res => {
            getUser()
        });
    }

    function onedriveConect() {
        var now = new Date();
        now.setTime(now.getTime() + 1 * 300 * 1000);
        document.cookie = "IsReact=1" + ";expires=" + now.toUTCString() + ";path=/";
        return authService.get('onedrive/authurl/1').then(res => {
            window.location.replace(res.Entity.AuthUrl);
            return res.Entity.AuthUrl;
        });
    }

    function onedriveDisconect() {
        return authService.get('onedrive/delink').then(res => {
            // debugger
            getUser()
        });
    }
    function dropboxConect() {
        var now = new Date();
        now.setTime(now.getTime() + 1 * 300 * 1000);
        document.cookie = "IsReact=1" + ";expires=" + now.toUTCString() + ";path=/";
        return authService.get('dropbox/authurl/1').then(res => {
            // debugger
            window.location.replace(res.Entity.AuthUrl);
            return res.Entity.AuthUrl;
        });
    }
    function dropboxDisconect() {
        return authService.get('dropbox/delink').then(res => {
            // debugger
            getUser()
        });
    }

    const getUser = () => {
        usersService.getCurrentUserclodu()
            .then((result) => {
                if(result){
                    setUserInfo(result.Entity)
                }
            })
            .catch(error => {
            });
    }

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content cloudstorage">
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            {/* <h3>Cloud Storage</h3> */}
                            <h3 style={{fontSize:35,fontWeight:300,marginBottom:5}}>Cloud Storage</h3>
                            <p>Access your documents anytime by connecting your cloud storage.</p>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className="recipient-btn">
                            <button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}}/></button>
                            </div>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="upload_block">
                                <div className='row'>
                                    {/*<div className='col-xl-4 col-lg-4 col-md-6 col-sm-12'>*/}
                                    {/*    <div class="use_templete">*/}
                                    {/*        <img src="src/images/folder.svg" alt="" />*/}
                                    {/*        <div className='content'>*/}
                                    {/*            <h3>Use Template</h3>*/}
                                    {/*            <h4>Disconnect</h4>*/}
                                    {/*        </div>*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}
                                    <div className='col-xl-4 col-lg-4 col-md-6 col-sm-12'>
                                        <div class="use_templete">
                                            <img src="src/images/google_drive.svg" alt="" />
                                            <div className='content'>
                                                <h3>Google Drive</h3>
                                                <h4 className={getUserInfo && getUserInfo[0] && getUserInfo[0].IsEnable ?'':'green'} onClick={()=>{getUserInfo && getUserInfo[0] && getUserInfo[0].IsEnable === 1 ?  googleDisconect() : googleConect()}}>
                                                    {getUserInfo && getUserInfo[0] && getUserInfo[0].IsEnable !== 1 ? "CONNECT":"DISCONNECT"}
                                                    </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-xl-4 col-lg-4 col-md-6 col-sm-12 mb-4'>
                                        <div class="use_templete">
                                            <img src="src/images/one_drive.svg" alt="" />
                                            <div className='content'>
                                                <h3>One Drive</h3>
                                                <h4 onClick={()=>{getUserInfo && getUserInfo[1] && getUserInfo[1].IsEnable ?  onedriveDisconect() : onedriveConect()}} className={getUserInfo && getUserInfo[1] && getUserInfo[1].IsEnable ?'':'green'}>{getUserInfo && getUserInfo[1] && getUserInfo[1].IsEnable !==1 ? "CONNECT":"DISCONNECT"}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-xl-4 col-lg-4 col-md-6 col-sm-12'>
                                        <div class="use_templete">
                                            <img src="src/images/google_dropbox.svg" alt="" />
                                            <div className='content'>
                                                <h3>Drop Box</h3>
                                                <h4 onClick={()=>{getUserInfo && getUserInfo[2] && getUserInfo[2].IsEnable ?  dropboxDisconect() : dropboxConect()}} className={getUserInfo && getUserInfo[2] && getUserInfo[2].IsEnable  ?'':'green'}>{getUserInfo && getUserInfo[2] && getUserInfo[2].IsEnable !==1 ? "CONNECT ":"DISCONNECT"}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export { CloudStorage }; 