import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useEffect, useState } from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import {usersService} from "./../../_services/users.service"
import { useTranslation } from 'react-i18next';
// function Sidebar({ history, location }) {
//import {useRoute} from '@react-navigation/native';
//const location = useLocation();
//const route = useRoute();
//console.log(route.name); test  


function Sidebar() {
    const [loginuserdata,setloginuserdata] = useState({})
    const location = useLocation();
    const locationname = location.pathname;

    useEffect(()=>{
        getCurrentUserData();
    },[])
    const getCurrentUserData = ()=>{
        usersService.getCurrentUserData()
        .then((result) => {
            if(result){
                setloginuserdata(result.Data)
               
            }
        })
    }
    //var adminmenu=0;
    //Filter Dropdown
    const { t, i18n } = useTranslation();

    function dropdown9() {
        var srcElement = document.getElementById('dropdown9');
        var subelement = document.getElementById('sub_admin'); 
        var ul = document.getElementById("main_nav");
        var li= ul.childNodes;
        console.log(li.length)
         for (var i = 0; i < li.length; i++) {
               li[i].classList.remove("active");
         }
         subelement.classList.add("active");

        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }

    $('#sub_admin').click(function (e) {
        if ($(this).hasClass("drop")) {
            $(this).removeClass("drop");
        }
        else {
            $(this).addClass("drop");
        }
    });
    

   
    return (
        <div className="sidebar" id="sidebar-nav">
            <div className="sidebar-scroll">
                <div className='side'>
                    <div className="logo">
                        <div>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i className="fas fa-align-left text-warning"></i>
                        </button>
                        <img src="src/images/signulu_black_logo.svg" alt="" className="logo1" />
                        <img src="src/images/signulu_small_black_logo.svg" alt="" className="logo2" />
                        </div>
                
                    </div>
                    <div className="quise_btn">
                        <button className="toggle_btn" onClick={slideToggle}><img src="src/images/toggle-menu.svg" /></button>
                    </div>
                </div>
                <nav className="navbar navbar-expand-lg">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav nav" id='main_nav'>
                            <li className={locationname == '/account/dashboard' ? 'item active' : 'item'}>
                                <Link to="/account/dashboard" ><img src="src/images/home_icon.svg" alt="" /><span>{t('HOME')}</span></Link>
                            </li >
                            <li className={locationname == '/account/manage' ? 'item active' : 'item'}>
                                <Link to="/account/manage" ><img src="src/images/manage_icon.svg" alt="" /><span>{t('MANAGE')}</span></Link>
                            </li>
                            <li className={locationname == '/account/template' ? 'item active' : 'item'}>
                                <Link to="/account/template" ><img src="src/images/templates_icon.svg" alt="" /><span>{t('TEMPLATES')}</span></Link>
                            </li>
                            <li className={locationname == '/account/reports' ? 'item active' : 'item'}>
                            <Link to="/account/reports" ><img src="src/images/reports_icon.svg" alt="" /><span>{t('REPORTS')}</span></Link>
                            </li>
                            {/* <li className={locationname == '/account/knowledge' ? 'item active' : 'item'}>
                                <a><img src="src/images/knowledge_icon.svg" alt="" /><span>{t('KNOWLEDGE')}</span></a>
                            </li> */}
                            {loginuserdata?.role_id == 5||loginuserdata?.role_id == 2 ||loginuserdata?.IsSuperAdmin===1? <li id="sub_admin" className={(locationname == '/account/billingandusage' || locationname == '/account/personalinformation' ||locationname == '/account/accountprofile' || locationname == '/account/signatures'|| locationname==='/account/userpreferences' || locationname == '/account/cloudstorage'  || locationname == '/account/users'  || locationname == '/account/contacts') ? 'item sub_drop-list active' : 'item sub_drop-list'}>
                                <a href="#;" className="sub_toggle_btn arrow_down" onClick={function(event){ dropdown9(); event.preventDefault()}}><img src="src/images/admin_icon.svg" alt="" /><span>{t('ADMIN')}</span></a>
                                <div className="sub_toggle_list active" id="dropdown9" style={{ display: 'none' }}>
                                    <div className="card card-body">
                                        <ul className="m-0 p-0">
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/billingandusage" >{t('BILLING_AND_USAGE')}</Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/signatures" >{t('SIGNATURES')} </Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/personalinformation">{t('PERSONAL_INFORMATION')}</Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/accountprofile">{t('ACCOUNT_PROFILE')} </Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/cloudstorage" >{t('CLOUD_STORAGE')} </Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/users" >{t('USERS')} </Link> </li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/contacts" >{t('CONTACTS')} </Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/userpreferences">{t('USER_PREFERENCES')} </Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>: <li id="sub_admin" className={(locationname == '/account/billingandusage' || locationname == '/account/personalinformation' ||locationname == '/account/accountprofile' || locationname == '/account/signatures'|| locationname==='/account/userpreferences' || locationname == '/account/cloudstorage'  || locationname == '/account/users'  || locationname == '/account/contacts') ? 'item sub_drop-list active' : 'item sub_drop-list'}>
                                <a href="#;" className="sub_toggle_btn arrow_down" onClick={function(event){ dropdown9(); event.preventDefault()}}><img src="src/images/admin_icon.svg" alt="" /><span>{t('ADMIN')}</span></a>
                                <div className="sub_toggle_list active" id="dropdown9" style={{ display: 'none' }}>
                                    <div className="card card-body">
                                        <ul className="m-0 p-0">
                                            {/* <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/billingandusage" >{t('BILLING_AND_USAGE')}</Link></li> */}
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/signatures" >{t('SIGNATURES')} </Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/personalinformation">{t('PERSONAL_INFORMATION')}</Link></li>
                                            {/* <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/accountprofile">{t('ACCOUNT_PROFILE')} </Link></li> */}
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/cloudstorage" >{t('CLOUD_STORAGE')} </Link></li>
                                            {/* <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/users" >{t('USERS')} </Link> </li> */}
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/contacts" >{t('CONTACTS')} </Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/account/userpreferences">{t('USER_PREFERENCES')} </Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>}
                           {loginuserdata?.IsSuperAdmin===1&&<li className={locationname == '/supperadmin/dashboard' ? 'item active' : 'item'}>
                            <Link to="/Supperadmin/dashboard" ><img src="src/images/productadmin.svg" alt="" /><span>{t('Product Admin')}</span></Link>
                            </li>}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export { Sidebar };