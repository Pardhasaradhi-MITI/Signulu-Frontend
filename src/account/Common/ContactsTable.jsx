import React, { useEffect, useState } from 'react';
import Pagination from 'react-responsive-pagination';
import * as EmailValidator from 'email-validator';
import EllipsisText from "react-ellipsis-text";

import { contactsService } from '../../_services/contacts.service';
import '../../css/ContactsTablePopup.css'

function ContactsPopTable(props) {
	const [contacts, setContacts] = useState([]);
	const [searchcontacts,setsearchcontacts]=useState([])
	const { selectedContacts, setSelectedContacts } = props
	const [searchText, setSearchText] = useState("");
	const [PageCount, setPageCount] = useState(0);
	const [pageNumber, setPageNumber] = useState(1);
	const [perpage,setperpage]=useState([]);
	const [searchperpage,setsearchperpage]=useState([]);
	const [searchpagecount,setsearchpagecount]=useState(1);
	const keys=["EmailAddress","FirstName","LastName"]
	//let searchValue = searchText
	// useEffect(() => {
	// 	searchValue = searchText
	// }, [searchText])
	useEffect(()=>{
		setsearchcontacts([])
		if(searchText !== ""){
			search(contacts)
		}
	   },[searchText])

	useEffect(() => {
		getContactList();
	}, []);

	// const getContactTypes = () => {
	// 	let input = {}
	// 	contactsService.getContactTypes(input)
	// 		.then((result) => {
	// 			setContactTypes(result);
	// 		})
	// 		.catch(error => {

	// 		});
	// }

	const getContactList = () => {
		let input = {
			"ContactTypeId": null,
			"PageNumber": 1,
			"PageSize": 1000000,
			"SearchByText": ""
		}
		contactsService.contactList(input)
			.then((result) => {
				let resdata =result.data.map(e => { return { ...e, isChecked: false } })
				// setContacts(result.data.map(e => { return { ...e, isChecked: false } }));
				//setPageCount(result.PageCount);
				// setPageNumber(result.PageNumber);
				// checkSelectedContacts(result.data)
				setperpage(resdata.slice(0,10));
				setContacts(resdata)
				let pages = 0;
				for(let i=1;i<Math.ceil(resdata.length/10)+1;i++){
					pages = i
				}
				setPageCount(pages)
				checkSelectedContacts(resdata)
			})
			.catch(error => {

			});
	}


	function checkSelectedContacts(data) {
		if (selectedContacts && selectedContacts.length > 0) {
			for (let index = 0; index < data.length; index++) {
				for (let j = 0; j < selectedContacts.length; j++) {
					if (selectedContacts[j].CompanyContactId == data[index].CompanyContactId) {
						data[index].isChecked = true
					}
				}
			}
			setContacts([...data])
		}
	}
	const handleSearchText = (e) => {
		setSearchText(e.target.value);
		if (e.target.value === "") {
			// getContactList();
		}

	}

	// const searchContact = () => {
	//       setPageNumber(1);
	// 	let input = {
	// 		"ContactTypeId": null,
	// 		"PageNumber": 1,
	// 		"PageSize": 5,
	// 		"SearchByText": searchValue
	// 	}
	// 	contactsService.contactList(input)
	// 		.then((result) => {
	// 			setContacts(result.data.map(e => { return { ...e, isChecked: false } }));
	// 			setPageCount(result.PageCount);
	// 			// setPageNumber(result.PageNumber);
	// 			checkSelectedContacts(result.data)
	// 		})
	// 		.catch(error => {

	// 		});
	// }

	 const search=(contacts)=>{
		if(contacts) {
        setsearchcontacts([])
       const list= contacts.length>0 && contacts.filter((item)=>keys.some((key)=>_.includes(_.lowerCase(item[key]),_.lowerCase(searchText))))
   setsearchcontacts(list);
        setsearchperpage(list.length>10?list.slice(0,10):list);
        let pages = 0;
        for(let i=1;i<Math.ceil(list.length/10)+1;i++){
            pages = i
        }
        setsearchpagecount(pages)
	}
    }


	const clearSearch = () => {
		
		searchValue = ''
		setSearchText('');
		//setPageNumber(1);
		//getContactList();
	}

	
	const handlePagination = (newPage) => {
		setPageNumber(newPage);
		if(searchcontacts.length>0 && searchText !== ""){
			setsearchperpage([])
			setsearchperpage(searchcontacts.slice((newPage*10)-10,newPage*10));
		}
		else{
			setperpage([])
			setperpage(contacts.slice((newPage*10)-10,newPage*10));
		}
	   
	   }


	function checkedContact(event, data, index) {
		let contactindex=contacts.findIndex(function(i){
			return i.CompanyContactId === data.CompanyContactId;
	   })
		contacts[contactindex].isChecked = event,
			setContacts([...contacts])
		if (event == false) {
			if (selectedContacts && selectedContacts.length > 0) {
				for (var j = selectedContacts.length - 1; j >= 0; j--) {
					if (selectedContacts[j].CompanyContactId == data.CompanyContactId) {
						selectedContacts.splice(j, 1)
					}
				}
			}
		} else if (event == true) {
			selectedContacts.push(contacts[contactindex])
			//setSelectedContacts(current => [contacts[index],...current] )
		}
		setSelectedContacts(selectedContacts)

	}

	return (

		<div className="main other_main contacts-table-popup" id="main">
			<div className=" wrapper_other main-content main-from-content contacts" >
				<div className="row">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div className="recipient-btn">
							<div className={"search-box"}>
								<div className="input-group" style={{ flex: 'none' }}>
									<input type="text" className="form-control"
										placeholder="Search"
										value={searchText}
										onChange={(e) => handleSearchText(e)}
										// onKeyPress={(event) => {
										// 	if (event.key === "Enter") {
										// 		event.preventDefault();
										// 		searchContact();
										// 	}
										// }}
									/>
									{
										searchText === "" ?
											<button type="button" className=" bg-transparent search-btn"
												style={{ zIndex: 100 }}
												// onClick={searchContact}
												>
												<img src='src/images/search_icon_blue.svg' alt='' />
											</button> :
											<button type="button" className=" bg-transparent search-btn"
												style={{ zIndex: 100 }}
												onClick={()=>setSearchText("")}>
												<img src='src/images/exit.png' alt='' />
											</button>
									}
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row align-items-center">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						{
							searchText === ""?perpage.map((item, index) => {
								return (
									<div className='document_block' key={item.CompanyContactId} >
										<ul className='content_area' onChange={(event) => {
											// let isChecked = (item?.isChecked) ? false : true
											// checkedContact(isChecked, item, index)
										}} >

											<li className={'checkbox_li'} >
												<h6></h6>
												<input className='selectCheckbox'
													type="checkbox"
													onChange={(event) => { checkedContact(event.target.checked, item, index) }}
													checked={item?.isChecked}></input>
											</li>
											<li className={"w-20"} title={item.ContactName}><h6>Name</h6>
												<h5>
													<EllipsisText text={item.ContactName} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Email</h6><h5>
												<EllipsisText text={item.EmailAddress} length={20} />
											</h5></li>
											<li className={"w-20"} title={item.Designation ? item.Designation : "NA"}><h6>Designation</h6>
												<h5>
													<EllipsisText text={item.Designation ? item.Designation : 'NA'} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Contact Type</h6>
												<h5>
													<EllipsisText text={item.ContactType ? item.ContactType : 'NA'} length={20} />
												</h5>
											</li>
										</ul>
									</div>
								)
							}):
							searchperpage && searchperpage.map((item, index) => {
								return (
									<div className='document_block' key={item.CompanyContactId} >
										<ul className='content_area' onChange={(event) => {
											// let isChecked = (item?.isChecked) ? false : true
											// checkedContact(isChecked, item, index)
										}} >

											<li className={'checkbox_li'} >
												<h6></h6>
												<input className='selectCheckbox'
													type="checkbox"
													onChange={(event) => { checkedContact(event.target.checked, item, index) }}
													checked={item?.isChecked}></input>
											</li>
											<li className={"w-20"} title={item.ContactName}><h6>Name</h6>
												<h5>
													<EllipsisText text={item.ContactName} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Email</h6><h5>
												<EllipsisText text={item.EmailAddress} length={20} />
											</h5></li>
											<li className={"w-20"} title={item.Designation ? item.Designation : "NA"}><h6>Designation</h6>
												<h5>
													<EllipsisText text={item.Designation ? item.Designation : 'NA'} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Contact Type</h6>
												<h5>
													<EllipsisText text={item.ContactType ? item.ContactType : 'NA'} length={20} />
												</h5>
											</li>
										</ul>
									</div>
								)
							})
							
						}
						{
							searchText === ""?contacts.length === 0 &&
							<div className={"text-center mt-4"}> No records are found.</div>
                            :searchcontacts.length === 0 &&
							<div className={"text-center mt-4"}> No records are found.</div>
						}

					</div>
				</div>
				<div className="row">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div className="document-nagin">
							<nav aria-label="Page navigation">
								<Pagination
									current={pageNumber}
									total={searchText.length>0?searchpagecount:PageCount}
									onPageChange={(newPage) => setTimeout(() => {
                                        handlePagination(newPage)
                                    }, 500)}
								/>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>


	)
}

export { ContactsPopTable };




