import React from 'react';

import { Link, useHistory } from 'react-router-dom';
import { accountService, alertService } from '@/_services';
import { loginService } from '../../_services/login.service';
import { commonService } from '../../_services/common.service';
import  '../../css/topbar.css'
import { Avatar } from '@mui/material';
import { useEffect, useState,useRef} from "react";
import { useTranslation } from 'react-i18next';
 import Form from 'react-bootstrap/Form';
import moment from 'moment';
import { FLAG_SELECTOR_OPTION_LIST } from '../../_services/model.service';
import {CountCard } from '../Content/Card/CountCard';
import { manageservice } from '../../_services/manageservice';
import { ResetPassword } from '../../account/ResetPassword';
import { Button, ButtonToolbar, Modal,ListGroup,ListGroupItem } from 'react-bootstrap';
import { ModelTraining } from '@mui/icons-material';

import {UpdatePassword} from "../../account/UpdatePassword"
import { lowerCase } from 'lodash';

function Topbar({ history, location ,profilePic12}) {
    const wrapperRef1 = useRef(null);
    const { t, i18n } = useTranslation();
    const usehistory = useHistory();  
    const [userObj, setUserObj] = useState();
    const [userDaysLeft, setUserDaysLeft] = useState(null);
    const [END_URL, setEndUrl] = useState(commonService.getImageUrl())
    const [selectedLanguage, setselectedLanguage] = useState(null);
    const [tosigncount,settosigncount]=useState();
    const [ResetPasswordmodelshow,setResetPasswordmodelshow]=useState(false)
    const [isintrailstringdata,setisintrailstringdata] = useState(false)
    const [numberofdaysintrail,setnumberofdaysintrail] = useState()
    const [profilePic1,setProfilePic1] =useState(null)
     
    
    useEffect(() => {
        if (localStorage.getItem('lang')) {
            setselectedLanguage(localStorage.getItem('lang'))
        } else {
            setselectedLanguage(FLAG_SELECTOR_OPTION_LIST[0].locale)
            localStorage.setItem('lang', FLAG_SELECTOR_OPTION_LIST[0].locale)
        }
     userInfo();
     userTrailDays();
    //  getcardsinfo();
    },[]);

    function redirectToFeedBack() {
        usehistory.push("/account/feedback");
    }
    function cancelButton(){
        setResetPasswordmodelshow(false);
    }
    function signOut() {
        loginService.logout( userObj.UserId, userObj["FirstName"], userObj["LastName"],)
        .then((result) => {
            usehistory.push("/account/login");
          });
    }
    function userInfo() {
       
        commonService.userInfo().then((userData) => {
            if("undefined" !== typeof userData.Data.LastLoginAttempt  && moment(userData.Data.LastLoginAttempt).isValid() && userData.Data.LastLoginAttempt != null && userData.Data.LastLoginAttempt != '') {
                var gmtDateTime = moment.utc(userData.Data.LastLoginAttempt);
              var local = gmtDateTime.local().format('MM/DD/YYYY hh:mm A');
              userData.Data.lastTimeLogin = local
              //var local = gmtDateTime.local().format('DD/MM/YYYY hh:mm A');
            }
            let profilepic=userData.Data.ProfilePicture;
            if(profilepic !== null){
                 setProfilePic1(END_URL+profilepic)}
             
            setUserObj(userData.Data)
          });
    }
    function userTrailDays() {
 commonService.userTrailDays().then((days) => {
   
           // console.log(days,"73")
           let type = typeof (days?.Entity?.Days)
           if(type === "string"){
            setisintrailstringdata(true)
             let days1 =parseInt(days?.Entity?.Days.split('')[0])
              //days?.Entity?.Days = days1
              setnumberofdaysintrail(days1)
             //setUserDaysLeft(days)
           }else{
            setUserDaysLeft(days)
           }
           
          });
    }
    function stringAvatar() {
        let name = userObj ? userObj.FullName : 'N A'
        return {
          sx: {
            bgcolor: "#2F7FED",
            width:'47px',
            height:'47px'
          },
          children: (name.split(' ') && name.split(' ')[0] && name.split(' ')[1]) ? `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`: 'NA',
        };
      }
    //Dropdown Menu User
    function userDropdown() {
        var srcElement = document.getElementById('dropdown_menu');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    function userDropdownClose() {
        var srcElement = document.getElementById('dropdown_menu');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } 
            return false;
        }
    }
   function changeLanguage(e) {
     setselectedLanguage(e.target.value)
     i18n.changeLanguage(e.target.value)
     localStorage.setItem('lang', e.target.value)
    }
    function applyfilter() {
        usehistory.push('/account/manage', { state: "TOSIGN",loginuseremail:userObj?.EmailAddress });
    }
    // const getcardsinfo=()=>{
    //     manageservice.getdoctotalcount().then((result)=>{
    //                  if(result){
    //                     result?.Entity?.map(item=>{
    //                       item.Name==="To Sign"?
    //                         settosigncount(item.RecordCount):""
    //                     })
    //                  }
    //     }).catch({
    
    //     })
    //   }

      function alerPop(){
        alertService.info('This feature is under development. We will notify to you once it is ready. Thank you for your support.',2000)

    }
     const useOutsideAlerter1 = (ref1) => {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref1.current && !ref1.current.contains(event.target) && event.target.id != 'user_profile_id') {
                    userDropdownClose()
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref1]);
        
    }
    useOutsideAlerter1(wrapperRef1);
    

    return (
        
        <div className="topbar">
            <div className="top_info">
                <div className="d-flex align-items-center">
                    {/* <button className="toggle_btn" onClick={slideToggle}><i className="fas fa-align-left"></i></button> */}
                    <div className="payment_info">
                        <button className="buy_btn" onClick={()=>alerPop()}>{t('BUY_NOW')}</button>
                        <ul>
                            <li style={{cursor:"default"}}>{isintrailstringdata === true ?numberofdaysintrail:userDaysLeft?.Entity?.Days} {t('Days Left')}</li>
                            <li style={{cursor:"default"}}>
                                 <span style={{padding:"0px 8px",marginLeft:"-8px"}}><img src="src/images/document_icon.svg" /></span>  {userObj?.CompanyUsedDocCount>0?userObj?.CompanyUsedDocCount:userObj?.UserUsedDocCount}/{userObj?.DocumentLimit}
                             </li>
                        </ul>
                    </div>
                </div>
                <div className="others">
                    {/* <div className="topdrop position-relative">
                        <Form.Select value={selectedLanguage ? selectedLanguage : '' }
                          onChange={changeLanguage} aria-label="Default select example">
                        {FLAG_SELECTOR_OPTION_LIST.map((data, index) => <option value={data.locale} key={data.locale}>{data.displayText}</option>)}
                        </Form.Select>
                    </div> */}
                    <div className="notification position-relative">
                        {/* <a href="#">
                            <img src="src/images/bell_icon.svg" />
                            <span>2</span>
                        </a> */}
                    </div>
                    <div className="search" style={{cursor:"default"}}>
                        <input style={{cursor:"default"}} type="text" name="" placeholder={t('SEARCH_BY_DATE_OR_NAME')}/>
                        <span style={{cursor:"default"}}><img src="src/images/search_icon.svg" /></span>
                    </div>
                    {/* <div className="sign" >
                    <a href="#" onClick={function(event){ applyfilter(); event.preventDefault()}} className="sign_btn">{t('TO_SIGN')} <span>{tosigncount}</span></a>
                    </div> */}
                    <a style={{color:"#1A2434", cursor:"default",fontWeight:300,fontSize:16,paddingLeft:8,paddingRight:8 }}>

                    Last logged in: {moment.utc(userObj?.LastLoginAttempt !== null ?userObj?.LastLoginAttempt:"-").local().format('MM/DD/YYYY h:mm A')}
                    </a>
                    <div className="users users-neo">
                        <div className="nav-item dropdown">
                            <a href="#" className="dropdown-toggle nav-link user"  onClick={function(event){ userDropdown(); event.preventDefault()}}>
                                {profilePic12 == null?userObj?.ProfilePicture ?<img id="user_profile_id" src={profilePic1} alt="" className="user-picture" /> : <Avatar {...stringAvatar()} />:<img id="user_profile_id" src={profilePic12} alt="" className="user-picture" />}
                                <span className="active"></span>
                            </a>
                            <div id="dropdown_menu" className="dropdown-menu" ref={wrapperRef1}>
                                <div className="setup_list">
                                    <ul>
                                    <li>
                                            <a ><span>{userObj?.EmailAddress}</span></a>
                                        </li>
                                        {/* <li> */}
                                       
                                            {/* <a ><span>{userObj?.LastLoginAttempt}</span></a> */}
                                            {/* <a >{t('Last logged in')}: <span>{userObj?.lastTimeLogin}</span></a>
                                        </li> */}
                                        <li>
                                            <a >{t('ACCOUNT')}: <span> #{userObj?.AccountId}</span></a>
                                        </li>
                                        {userObj?.company_name ? <li><a >{userObj?.company_name}</a></li>: null}
                                        <li>
                                            <a >{t('TOTAL_DOCUMENTS')}: <span>{userObj?.CompanyUsedDocCount>0?userObj?.CompanyUsedDocCount:userObj?.UserUsedDocCount}/{userObj?.DocumentLimit}</span></a>
                                        </li>
                                        <li>
                                            <a >{t('Subscription Plan')}: <span >{userObj?.PackageName ? <li><a >{userObj?.PackageName}{`${"("} ${userObj?.DurationMode} ${")"}`}</a></li>: "-"}</span></a>
                                        </li>
                                        
                                    </ul>
                                </div>
                                <div className="setup_list">
                                    <ul>
                                        <li><a  >{t('MY_DOCUMENTS')}: {userObj?.UserUsedDocCount}</a></li>
                                        <li> <Link to="personalinformation">{t('MANAGE_PROFILE')}</Link></li>
                                        {/* <li><a href="#" onClick={function(event){event.preventDefault()}}>{t('FEEDBACK')}</a></li>
                                        <li><a href="#" onClick={function(event){event.preventDefault()}}>{t('MANAGE_PROFILE')}</a></li> */}
                                        <li><a href="#" onClick={function(event){event.preventDefault(), redirectToFeedBack()}}>{t('FEEDBACK')}</a></li>
                                        <li style={{cursor:"pointer"}}><a onClick={function(event){event.preventDefault(),setResetPasswordmodelshow(true)}}>{t('UPDATE_PASSWORD')}</a></li>
                                        <li><a href="#" onClick={function(event){ event.preventDefault()}}>{t('CANCEL_ACCOUNT')}</a></li>
                                        <li><a href="#" onClick={function(event){event.preventDefault(); signOut()}}>{t('LOG_OUT')}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div>{moment(userObj?.LastLoginAttempt !== null ?userObj.LastLoginAttempt:"-").format('MMM/DD/YYYY h:mm:ss a')}</div> */}
                </div>
            </div>
            <Modal show={ResetPasswordmodelshow}>
            <UpdatePassword UserId={userObj?.UserId} close={cancelButton}/>
            {/* <Modal.Header >
               <Modal.Title>
                   <p>RESET PASSWORD</p>
                   <button type="button" className="close" style={{ backgroundColor: "#ffff" }} onClick={() => { setShowImportDrivemodel(false) }}>
                        <span aria-hidden="true">&times;</span>
                    </button>
               </Modal.Title>

            </Modal.Header>
            <Modal.Body className='error-modal-content'>
                  <UpdatePassword/>
                </Modal.Body> */}
            </Modal>
        </div>
    )
}

export { Topbar }; 