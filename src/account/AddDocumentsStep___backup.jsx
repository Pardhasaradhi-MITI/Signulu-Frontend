import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { AddDocumentsStep1 } from './AddDocumentsStep1';
import { AddDocumentsStep2 } from './AddDocumentsStep2';
import { AddDocumentsStep3 } from './AddDocumentsStep3';
import { AddDocumentsStep4 } from './AddDocumentsStep4';



function AddDocumentsStep({ history, location }) {   


    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content wizard">
                    <div className='row'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div id="wizard_step">
                                <div className="step-wizard" role="navigation">
                                    <ul>
                                        <li className="active done">
                                            <button id="step1">
                                                <div className="step">1</div>
                                                <div className="title">Add</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li id="wizard-tab-li-2">
                                            <button id="step2">
                                                <div className="step">2</div>
                                                <div className="title">Select</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li id="wizard-tab-li-3">
                                            <button id="step3">
                                                <div className="step">3</div>
                                                <div className="title">Prepare</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li id="wizard-tab-li-4">
                                            <button id="step4">
                                                <div className="step">4</div>
                                                <div className="title">Review</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div className='wizard-form'>
                                    <AddDocumentsStep1 />

                                    <AddDocumentsStep2 />

                                    <AddDocumentsStep3 />

                                    <AddDocumentsStep4 />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { AddDocumentsStep }; 