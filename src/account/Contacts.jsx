import React, {useEffect, useState} from 'react';
import {Sidebar} from './Common/Sidebar';
import {Topbar} from './Common/Topbar';
import Pagination from 'react-responsive-pagination';
import * as EmailValidator from 'email-validator';
import EllipsisText from "react-ellipsis-text";
import CloseIcon from '@mui/icons-material/Close';
import {contactsService} from "../_services/contacts.service";
import {Box,Modal} from "@material-ui/core";
import { Button, ButtonToolbar, Modal as Model,ListGroup,ListGroupItem } from 'react-bootstrap';
// import { Button, ButtonToolbar, ListGroup,ListGroupItem } from 'react-bootstrap';
import Backdrop from '@mui/material/Backdrop';
import Dropzone from "react-dropzone";
import { read, utils, writeFile } from 'xlsx';
import ReactTooltip from 'react-tooltip';

const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	bgcolor: 'background.paper',
	boxShadow: 24,
	width: "85%"
};

function Contacts({history, location}) {
	const [contacts, setContacts] = useState([]);
	const [searchText, setSearchText] = useState("");
	const [pageCount, setPageCount] = useState(0);
	const [pageNumber, setPageNumber] = useState(1);
	const [pageSize, setPageSize] = useState(5);
	const [activePage, setActivePage] = useState('list');
	const [count,setcount]=useState(0)
	const [contactTypes, setContactTypes] = useState([]);
	const [contactType, setContactType] = useState("");
	const [Company, setCompany] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [emailAddress, setEmailAddress] = useState("");
	const [zipCode, setZipCode] = useState("");
	const [cellPhone, setCellPhone] = useState("");
	const [addressLine1, setAddressLine1] = useState("");
	const [addressLine2, setAddressLine2] = useState("");
	const [city, setCity] = useState("");
	const [state, setState] = useState("");
	const [workPhone, setWorkPhone] = useState("");
	const [designation, setDesignation] = useState("");
	const [palceholderEmail,setPLaceHolderEmail]=useState('');
	const [isDomain, setIsDomain] = useState(false);
	const [error, setError] = useState({
		firstName: '',
		lastName: '',
		emailAddress: '',
		designation: '',
		zipCode: '',
		addressLine1: '',
		cellPhone:"",
		addressLine2: '',
		city: '',
		state: ''
	});

	const [selectedContact, setSelectedContact] = useState({});
	const [open, setOpen] = React.useState(false);
	const [uploadedFile, setUploadedFile] = useState(null);
	const [isValidFile, setIsValidFile] = useState(false);
	const [openSuccessModal, setOpenSuccessModal] = React.useState(false);
	const [openConfirmModal, setOpenConfirmModal] = React.useState(false);
	const [openDuplicateModal, setOpenDuplicateModal] = React.useState(false);
	const [duplicatedContactList, setDuplicatedContactList] = useState([]);
	const [userInfo, setUserInfo] = useState({});
	// const [count, setcount] = useState([]);
	// const [,]=useState(0)

	useEffect(() => {
		getContactTypes();
	}, [])
	useEffect(() => {
		getContactList();
	}, [pageNumber]);
	useEffect(() => {
		if (openSuccessModal===false){
			resetForm()
		}
	}, [openSuccessModal]);

	// useEffect(() => {
	// 	if(activePage==="add"){
	// 		let errors = error;
	// 		const timeoutId = setTimeout(() =>{
	// 			checEmail(emailAddress).then(function (data) {
					  
	// 				if (data === true) {
	// 					errors.emailAddress = 'Email already exist with another contact';
	// 				} else {
	// 					errors.emailAddress = '';
	// 				}
	// 				// setEmailAddress(value);
	// 				setError(errors);
	// 				setcount(count+1)
	// 				// setError(errors);
	// 			});
	// 		} , 1000);
	// 		return () => clearTimeout(timeoutId);
	// 	}

	// }, [emailAddress]);

	useEffect(() => {
		if (activePage==="add"){
			 
			let tempEmail=(userInfo.email_address).split("@")
			let tempemailer=tempEmail[1]
			let removeemailer=emailAddress.split("@")
			let completeemail= "@"+tempemailer
			// setEmailAddress(completeemail)
			setPLaceHolderEmail(completeemail)
		}else {
			setPLaceHolderEmail("")
		}
	}, [isDomain]);

	const emailValidater=()=>{
		 
		if (activePage==="add" && emailAddress) {
			let errors = error;

			if (isDomain === true){
				 let tempEmail=(userInfo.email_address).split("@")
				let tempemailer=tempEmail[1]
				let removeemailer=emailAddress.split("@")
				let completeemail= removeemailer[0] + "@"+tempemailer
				// setEmailAddress(completeemail)
				checEmail(completeemail).then(function (data) {
					let removeemailer=emailAddress.split("@")
					setEmailAddress(removeemailer[0])
					 
					if (data === true) {
						errors.emailAddress = 'Email already exist.';
					} else {
						errors.emailAddress = '';
					}
					// setEmailAddress(value);
					setError(errors);
					setcount(count + 1)
					// setError(errors);
				});
				// setEmailAddress(completeemail)
			}else {
				checEmail(emailAddress).then(function (data) {
					// let removeemailer=emailAddress.split("@")
					// setEmailAddress(removeemailer[0])
					 
					if (data === true) {
						errors.emailAddress = 'Email already exist.';
					} else {
						errors.emailAddress = '';
					}
					// setEmailAddress(value);
					setError(errors);
					setcount(count + 1)
					// setError(errors);
				});
			}


		}
	}

	const getContactTypes = () => {
		let input = {}
		contactsService.getContactTypes(input)
			.then((result) => {
				//setUserInfo(result.Data)
				setContactTypes(result);
				// setContactType(result[0]?result[0].ContactTypeId:'');
			})
			.catch(error => {

			});
	}

	const getContactList = () => {
		let input = {
			"ContactTypeId": null,
			"PageNumber": pageNumber,
			"PageSize": pageSize,
			"SearchByText": ""
		}
		contactsService.contactList(input)
			.then((result) => {
				setContacts(result.data);
				setPageCount(result.PageCount);
				setPageNumber(result.PageNumber);
				setPageSize(result.PageSize);
			})
			.catch(error => {

			});
	}

	const handleSearchText = (e) => {
		setSearchText(e.target.value);
		if (e.target.value === "") {
			getContactList();
		}

	}

	const searchContact = () => {
		let input = {
			"ContactTypeId": null,
			"PageNumber": 1,
			"PageSize": 5,
			"SearchByText": searchText
		}
		contactsService.contactList(input)
			.then((result) => {
				setContacts(result.data);
				setPageCount(result.PageCount);
				setPageNumber(result.PageNumber);
				setPageSize(result.PageSize);
			})
			.catch(error => {

			});
	}

	const clearSearch = () => {
		setSearchText("");
		getContactList();
	}

	const handlePagination = (newPage) => {
		setPageNumber(newPage);
	}

	const resetForm = () => {
		  
		setActivePage('list');
		setContactType("");
		setFirstName("");
		setLastName("");
		setEmailAddress("");
		setZipCode("");
		setCellPhone("");
		setAddressLine1("");
		setAddressLine2("");
		setCity("");
		setState("");
		setWorkPhone("");
		setDesignation("");
		setSelectedContact({});

		setError({
			firstName: '',
			lastName: '',
			emailAddress: '',
			designation: '',
			zipCode: '',
			addressLine1: '',
			cellPhone:"",
			addressLine2: '',
			city: '',
			state: ''
		})
	}

	const addContact = () => {
	//	let tempEmail=(userInfo.email_address).split("@")
		//let tempemailer=tempEmail[1]
		//let removeemailer=emailAddress.split("@")
	//	let completeemail= removeemailer[0] + "@"+tempemailer
		let input = {
			"IsDomain": isDomain,
			"ContactType": contactType,
			"Company": Company,
			"FirstName": firstName,
			"LastName": lastName,
			"EmailAddress": emailAddress,
			"WorkPhone": workPhone,
			"CellPhone": cellPhone,
			"Address1": addressLine1,
			"Address2": addressLine2,
			"City": city,
			"State": state,
			"PostalCode": zipCode,
			"Designation":designation,
		}
		if (validateForm()) {
			contactsService.addContact(input)
				.then((result) => {
					if (result.HttpStatusCode === 200) {
						setOpenSuccessModal(true);
						getContactList();
						// setTimeout(() => { resetForm();  }, 1000);

					}
				})
				.catch(error => {

				});
		}

	}

	const validateForm = () => {
		let count = 0;
		let tmp_errors = {...error};
		if (firstName === "") {
			tmp_errors.firstName = 'First Name is required.';
			count = count + 1;
		} else if (firstName.length < 3) {
			tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
			count = count + 1;
		}
		if (lastName === "") {
			tmp_errors.lastName = 'Last Name is required.';
			count = count + 1;
		}
		if (emailAddress === "") {
			tmp_errors.emailAddress = 'Email Address is required.';
			count = count + 1;
		} else if (EmailValidator.validate(emailAddress) === false) {
			tmp_errors.emailAddress = 'Enter correct email address.';
			count = count + 1;
		}
		if (designation === "") {
			tmp_errors.designation = 'Designation is required.';
			count = count + 1;
		}
		if (count <= 0) {
			tmp_errors = {
				firstName: '',
				lastName: '',
				emailAddress: '',
				designation: '',
				zipCode: '',
				addressLine1: '',
				cellPhone:"",
				addressLine2: '',
				city: '',
				state: ''
			}
		}
		setError(tmp_errors);

		return count <= 0;
	}

	const selectContact = (contact) => {
		let selectedContactType = contactTypes.filter(function (type) {
			return type.ContactType === contact.ContactType
		});
		setActivePage('edit');
		setContactType(selectedContactType.length > 0 ? selectedContactType[0].ContactTypeId : "");
		setFirstName(contact.FirstName);
		setLastName(contact.LastName);
		setEmailAddress(contact.EmailAddress);
		setZipCode(contact.PostalCode);
		setCellPhone(contact.CellPhone);
		setAddressLine1(contact.Address1);
		setAddressLine2(contact.Address2);
		setCity(contact.City);
		setState(contact.State);
		setCompany(contact.Company)
		setWorkPhone(contact.WorkPhone);
		setDesignation(contact.Designation);
		setSelectedContact(contact);
	}

	const editContact = () => {
		let input = {
			IsDomain: isDomain,
			"CompanyContactId": selectedContact.CompanyContactId,
			"CompanyId": 437,
			"ContactType": contactType,
			"Company":Company,
			"FirstName": firstName,
			"LastName": lastName,
			"EmailAddress": emailAddress,
			"WorkPhone": workPhone,
			"CellPhone": cellPhone,
			"Address1": addressLine1,
			"Address2": addressLine2,
			"City": city,
			"State": state,
			"PostalCode": zipCode,
			"Designation":designation,
		}
		if (validateForm()) {
			contactsService.editContact(selectedContact.CompanyContactId, input)
				.then((result) => {
					if (result.HttpStatusCode === 200) {
						setOpenSuccessModal(true);
						getContactList();
						// resetForm();
					}
				})
				.catch(error => {

				});
		}
	}

	const deleteContact = () => {
		contactsService.deleteContact(selectedContact.CompanyContactId)
			.then((result) => {
				if (result.HttpStatusCode === 200) {
					setOpenConfirmModal(false);
					setSelectedContact({});
					getContactList();
				}
			})
			.catch(error => {

			});
	}

	const handleOpen = () => setOpen(true);
	const handleClose = () =>{
		setUploadedFile(null)
		setOpen(false)
		resetForm()
	}
	const handleCloseUploadedfile =() =>{
		setUploadedFile(null)
		setIsValidFile(false)
	}

	const onDrop = (accepted, rejected) => {
		let meta = accepted[0];
		let filename = meta.name;
		if ((filename.substring((filename.length - 4))).toLowerCase() !== ".csv") {
			//   
			setIsValidFile(false);
			 setUploadedFile(null)
		} else {
			setIsValidFile(true);

			// setUploadedFile(null)
		}
		setUploadedFile(accepted[0]);

	}

	const importContact = () => {

		contactsService.uploadContacts(uploadedFile)
			.then((result) => {
				setUploadedFile(null);
				if (result.HttpStatusCode === 200) {
					setOpen(false);
					getContactList();
					console.log(result.Entity.DuplicateContacts)
					if(result.Entity.DuplicateContacts.length > 0){
						setOpenDuplicateModal(true)
						setDuplicatedContactList(result.Entity.DuplicateContacts)

					}
				}
			})
			.catch(error => {

			});
	}

	const handleInputChange = (feild, e) => {
		let value = e.target.value;
		let errors = error;
		switch (feild) {
			case 'firstName':
				if (value === "") {
					errors.firstName = 'First Name is required.';
				} else if (value.length < 3) {
					errors.firstName = 'First Name must be atleast 3 characters long.';
				} else {
					errors.firstName = ''
				}
				setFirstName(value);
				setError(errors);
				break;
			case 'lastName':
				if (value === "") {
					errors.lastName = 'Last Name is required.';
				} else {
					errors.lastName = ''
				}
				setLastName(value);
				setError(errors);
				break;
			// case 'emailAddress':
			// 	if (value === "") {
			// 		errors.emailAddress = 'Email Address is required.';
			// 		setEmailAddress(value);
			// 		setError(errors);
			// 	} else if (EmailValidator.validate(value) === false) {
			// 		errors.emailAddress = 'Enter correct email address.';
			// 		setEmailAddress(value);
			// 		setError(errors);
			// 	} else if (EmailValidator.validate(value) === true) {
			// 		errors.emailAddress = '';
			// 		setEmailAddress(value);
			// 		setError(errors);
			// 	}

				case 'emailAddress':
				if (isDomain === true){
					if (value === "") {
						errors.emailAddress = 'Email Address is required.';
						setEmailAddress(value);
						setError(errors);
					}else {
						errors.emailAddress = '';
						setEmailAddress(value);
						setError(errors);
					}
				}else {
					if (value === "") {
						errors.emailAddress = 'Email Address is required.';
						setEmailAddress(value);
						setError(errors);
					} else if (EmailValidator.validate(value) === false) {
						errors.emailAddress = 'Enter correct email address.';
						setEmailAddress(value);
						setError(errors);
					} else if (EmailValidator.validate(value) === true) {
						errors.emailAddress = '';
						setEmailAddress(value);
						setError(errors);
					}
				}
             break;
			case 'zipCode':
				errors.zipCode = value.length < 5 ? 'ZipCode must be atleast 5 digits long.' : '';
				setZipCode(value);
				setError(errors);
				break;
			case 'addressLine1':
				if (value.length < 3) {
					errors.addressLine1 = 'Address must be atleast 3 characters long.';
				} else {
					errors.addressLine1 = ''
				}
				setAddressLine1(value);
				setError(errors);
				break;
				case 'cell':
					if (e.target.value.length === 16) return false;   //limits to 10 digit entry
					setCellPhone(e?.target.value);       //saving input to state
				if (value.length < 10) {
					errors.cellPhone = 'Mobile number must be atleast 10 characters long.';
				} else {
					errors.cellPhone = ''
				}
				setCellPhone(value);
				setError(errors);
				break;
			case 'addressLine2':
				if (value.length < 3) {
					errors.addressLine2 = 'Address Line #2 must be atleast 3 characters long.';
				} else {
					errors.addressLine2 = ''
				}
				setAddressLine2(value);
				setError(errors);
				break;
			case 'city':
				if (value.length < 3) {
					errors.city = 'City must be atleast 2 characters.';
				} else {
					errors.city = ''
				}
				setCity(value);
				setError(errors);
				break;
			case 'state':
				if (value.length < 2) {
					errors.state = 'State must be atleast 2 characters.';
				} else {
					errors.state = ''
				}
				setState(value);
				setError(errors);
				break;
			case 'designation':
				errors.designation = value === "" ? 'Designation is required.' : '';
				setDesignation(value);
				setError(errors);
				break;
			default:
				break;
		}

	}

	const checEmail = (email) => {
		return new Promise(function (resolve, reject) {
			contactsService.checkEmailExist(email)
				.then((result) => {
					let value;
					value = !!result.ContactId;
					resolve(value);
				})
				.catch(error => {

				});
		});
	}

	const handleOpenSuccessModal = () => setOpenSuccessModal(true);
	const handleCloseSuccessModal = () => {
		setOpenSuccessModal(false);
	}

	const handleCloseConfirmationModal = () => {
		setOpenConfirmModal(false);
		setSelectedContact({});
	}

	const handleCloseDuplicateModal = () => {
		setOpenDuplicateModal(false);
		setDuplicatedContactList([])
		setSelectedContact({});
	}



	const selectContactForDelete = (contact) => {
		setSelectedContact(contact);
		setOpenConfirmModal(true);
	}
	const filterexceldata=()=>{
		let exceldata = []
			let input = {
				"ContactTypeId": null,
				"PageNumber": 1,
				"PageSize": 100000,
				"SearchByText": ""
			}
			contactsService.contactList(input)
				.then((result) => {
				if(result?.data){
					result?.data.map(item=>{
					let newitem = {}
					newitem.ContactName = item.ContactName
					newitem.EmailAddress =item.EmailAddress
					newitem.Designation = item.Designation
					newitem.ContactType = item.ContactType
					newitem.Company = item.Company
					exceldata.push(newitem)
				})}
		//setexceldata(exceldata)
		console.log(exceldata,"exceldata")
					const headings = [[
						'Name',
						'Email',
						'Designation',
						'Contact Type',
					    'Company'
					]]
		const wb = utils.book_new();
		const ws = utils.json_to_sheet([]);
		utils.sheet_add_aoa(ws, headings);
		utils.sheet_add_json(ws, exceldata, { origin: 'A2', skipHeader: true });
		utils.book_append_sheet(wb, ws, 'Report');
		writeFile(wb, 'Contacts Report.xlsx');
	})
	.catch(error => {

	});
	}

	return (

		<div className="dashboard_home wrapper_other" id="wrapper_other">
			<Sidebar/>

			<div className="main other_main" id="main">
				<Topbar/>

				<div className="main-content main-from-content contacts">
				<div className="main-content main-from-content signatures" style={{padding:"0px 30px 30px 44px"}}>
					<div className="row">
					{activePage === 'add' || activePage === 'edit' ?
					
					<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
						<h3 style={{fontSize:35,fontWeight:300}}>{activePage === 'add' ? "Add Contact" : "Edit Contact"}</h3>
					</div>
				:
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
						<h3 style={{fontSize:35,fontWeight:300}}>Contacts</h3>
						</div>}
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
							<div className="recipient-btn">
							<button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}} /></button>
								
							</div>
						</div>
					</div>
					{
						activePage === "list" &&
						<>
							<div className="row">
								<div className="col-xl-2 col-lg-2 col-md-2 col-sm-12">
									{/* <h3>CONTACTS</h3> */}
								</div>
								<div className="col-xl-10 col-lg-10 col-md-10 col-sm-12">
									<div className="recipient-btn">
										<div className={"search-box"}>
											<div className="input-group" style={{flex: 'none'}}>
												<input type="text" className="form-control"
												       placeholder="Search"
												       value={searchText}
												       onChange={(e) => handleSearchText(e)}
												       onKeyPress={(event) => {
													       if (event.key === "Enter") {
														       event.preventDefault();
														       searchContact();
													       }
												       }}
												/>
												{
													searchText === "" ?
														<button type="button" className=" bg-transparent search-btn"
														        style={{zIndex: 100}}
														        onClick={searchContact}
														>
															<img src='src/images/search_icon_blue.svg' alt=''/>
														</button> :
														<button type="button" className=" bg-transparent search-btn"
														        style={{zIndex: 100}}
														        onClick={clearSearch}
														>
															<img src='src/images/exit.png' alt=''/>
														</button>
												}

											</div>
										</div>
										<div className='import'>
											<button className="btn" onClick={handleOpen}>IMPORT</button>
										</div>
										<div className='add_user'>
											<button className="btn" onClick={() => setActivePage('add')}>
												<img src="src/images/plus_icon.svg"/>Add Contact
											</button>
										</div>
										<div className="action_area">
											<ul>
												<li data-tip data-for="XLSX"><img src="src/images/XLSX.png" alt="" onClick={()=>{filterexceldata()}} style={{width:35,height:35,cursor:"pointer"}} /></li>
												<ReactTooltip id="XLSX" place="top" effect="solid">Export to Excel</ReactTooltip>
												{/* <li><img src="src/images/pdf.svg" alt=""/></li>
												<li><img src="src/images/csv.svg" alt=""/></li> */}
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div className="row align-items-center">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									{
										contacts.map((item, index) => {
											return (
												<div className='document_block' key={index}>
													<ul className='content_area'>
														<li className={"w-25"} title={item.ContactName}><h6>Name</h6>
															<h5>
																<EllipsisText text={item.ContactName} length={"20"}/>
															</h5>

														</li>
														<li className={"w-20"}><h6>Email</h6><h5>
															<EllipsisText text={item.EmailAddress} length={"20"} />
														</h5></li>
														<li className={"max-width-80"} title={item.Designation ? item.Designation : "NA" }><h6>Designation</h6>
															<h5>
																<EllipsisText text={item.Designation ? item.Designation : 'NA'} length={"20"} />
															</h5>
														</li>
														<li className={"max-width-80"}><h6>Contact Type</h6>
															<h5>
																<EllipsisText text={item.ContactType ? item.ContactType : 'NA'} length={"20"} />


															</h5>
														</li>
														<li className={"max-width-80"} title={item.Company ? item.Company : 'NA'}>
															<h6>Company</h6>
															<h5>{item.Company ? item.Company : 'NA'}</h5></li>
													</ul>
													<ul className='action_area'>
														<li>
															<a href='javascript:void(0)'
															   onClick={() => selectContact(item)}
															   className={'hover-tool'}>
																<img className={'tooltip-image'}
																     src='src/images/edit1.svg' alt=''/>
																<span className={'tooltiptext'}>Edit</span>

															</a>
														</li>
														{/* <li><a className={'hover-tool'} href='#'><img
															className={'tooltip-image'} src='src/images/lock.svg'
															alt=''/> <span className={'tooltiptext'}>Lock</span></a>
														</li> */}
														<li><a href='javascript:void(0)'
														       onClick={() => selectContactForDelete(item)}
														       className={'hover-tool'}>
															<img className={'tooltip-image'}
															     src='src/images/delete_icon.svg' alt=''/>
															<span className={'tooltiptext'}>Delete</span>
														</a>
														</li>
													</ul>
												</div>
											)
										})
									}
									{
										contacts.length === 0 &&
										<div className={"text-center mt-4"}> No records are found.</div>
									}

								</div>
							</div>
							<div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
											<Pagination
												current={pageNumber}
												total={pageCount}
												onPageChange={(newPage) => handlePagination(newPage)}
											/>
										</nav>
									</div>
								</div>
							</div>
						</>
					}
					{
						activePage === 'add' || activePage === 'edit' ?
							<div className={"contact-form"}>
								
								<div className={"contact-form-section mt-3"}>
									<div className={"row"}>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<select className="form-select" aria-label="Default select example"
												style={{display: "block", width: "100%",padding: ".375rem .75rem",fontSize: "1rem",fontWeight: "400",lineHeight: "1.5",color: "#212529",backgroundColor: "#fff",backgroundClip: "padding-box"}}
												        value={contactType}
												        onChange={(e) => setContactType(e.target.value)}
												>
													<option value={""} selected disabled={true} >Choose</option>

													{
														contactTypes.map((item, index) => {
															return (
																<option value={item.ContactTypeId}
																        key={index}>{item.ContactType}</option>
															)
														})
													}
												</select>
												<p className={"error-msg"}/>
											</div>
										</div>
										<div className={"col-sm-6"}>
											{/* <div className={"mb-3"}>
												<input type="text" className="form-control"
												       placeholder="First Name"
												       value={firstName}
												       onChange={(e) => handleInputChange('firstName', e)}
												       title="please fill out this feild"
												/>
												<p className={"error-msg"}>{error.firstName}</p>
											</div> */}
										</div>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input type="text" className="form-control"
												       placeholder="First Name"
												       value={firstName}
												       onChange={(e) => handleInputChange('firstName', e)}
												       title="please fill out this feild"
												/>
												<p className={"error-msg"}>{error.firstName}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input type="text" className="form-control"
												       placeholder="Last Name"
												       value={lastName}
												       onChange={(e) => handleInputChange('lastName', e)}
												       title="please fill out this feild"
												/>
												<p className={"error-msg"}>{error.lastName}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input type="text" className="form-control"
												       placeholder="Email Address"
												       value={emailAddress}
													readOnly={activePage!=='add'}
													onBlur={()=>emailValidater()}
												       onChange={(e) => handleInputChange('emailAddress', e)}
												       title="please fill out this feild"
												/>
												<p className={"custome-email-input-tag"}>{palceholderEmail}</p>
												<p className={"error-msg"}>{error.emailAddress}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input style={{marginLeft:'-0.5px'}}  type="text" className="form-control"
												       placeholder="Mobile"
												       onChange={(e) => {
														handleInputChange('cell', e)
													     }
												       }
												       value={cellPhone}
												/>
												<p className={"error-msg"}>{error.cellPhone}</p>
											</div>
										</div>
										 <div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="Address"
												       value={addressLine1}
												       onChange={(e) => handleInputChange('addressLine1', e)}
												/>
												<p className={"error-msg"}>{error.addressLine1}</p>
											</div>
										</div> 
										 <div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="City"
												       value={city}
												       onChange={(e) => handleInputChange('city', e)}
												/>
												<p className={"error-msg"}>{error.city}</p>
											</div>
										</div>
										
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="State"
												       value={state}
												       onChange={(e) => handleInputChange('state', e)}
												/>
												<p className={"error-msg"}>{error.state}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="Zip Code"
												       onChange={(event) => {
													       //   
													       if (event.target.value.length === 7) return false;   //limits to 10 digit entry
													       // setZipCode(event?.target.value);
													       handleInputChange('zipCode', event)//saving input to state
												       }
												       }
												       value={zipCode}
												/>
												<p className={"error-msg"}>{error.zipCode}</p>
											</div>
										</div>
										{/* <div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="number" className="form-control"
												       placeholder="Work Phone with extension"
												       onChange={(event) => {
													       if (event.target.value.length === 11) return false;   //limits to 10 digit entry
													       setWorkPhone(event?.target.value);       //saving input to state
												       }
												       }
												       value={workPhone}
												/>
												<p className={"error-msg"}/>
											</div>
										</div> */}
										
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="Designation"
												       value={designation}
												       onChange={(e) => handleInputChange('designation', e)}
												       title="please fill out this feild"
												/>
												<p className={"error-msg"}>{error.designation}</p>
											</div>
										</div>
									</div>
								</div>
								<div className={"form-action mt-3"}>
                                <button type="button" className="btn btn-secondary" style={{width:105}}
                                            onClick={resetForm}>CANCEL</button>
                                    <button type="button" className="btn save-btn" style={{width:110,backgroundColor:"#2F7FED",border:"1px solid #2F7FED"}}
                                            onClick={activePage === "add" ? addContact : editContact} > SAVE </button>
                                </div>
							</div> : null
					}
					<Modal
						open={open}
						onClose={handleClose}
						// aria-labelledby="transition-modal-title"
						// aria-describedby="transition-modal-description"
						aria-labelledby="contained-modal-title-vcenter"
						closeAfterTransition
						BackdropComponent={Backdrop}
						BackdropProps={{
							timeout: 1000,
						}}
						disableBackdropClick={true}
						id={"contacts-import-modal"}
					>
						<Box sx={style}>
							<div className={"modal-heading"}>
								<p className={"title"}>CONTACTS - IMPORT</p>
								<a href='javascript:void(0)'
								   onClick={handleClose}
								>
									<img src='src/images/close_icon.svg' alt=''/>

								</a>
							</div>
							<div className={"modal-body-content"}>
								<div className={"container-fluid py-3"}>
									<div className={"row"}>
										<div className={"col-sm-7 left-column"}>
											<ul>
												<li style={{fontSize:17}}>
													Click on the right panel to choose your contacts data (csv) file.
													The following table gives a sample of the csv file's structure.
													<ul>
														<li style={{fontSize:17,marginLeft:"-16px"}}>The first row in the csv file must be a header row
															containing the 5 column names given in the table.
														</li>
														<li style={{fontSize:17,marginLeft:"-16px"}}>The second row onwards, only the email address is
															mandatory.
														</li>
													</ul>
												</li>
											</ul>
											<div className="table-responsive">
												<table className="table table-bordered">
													<thead>
													<tr>
														<th scope="col" style={{textAlign:"left",fontSize:16}}>Email Address</th>
														<th scope="col" style={{textAlign:"left",minWidth:"105px",fontSize:16}}>First Name</th>
														<th scope="col" style={{ textAlign:"left",minWidth:"105px",fontSize:16}}>Last Name</th>
														<th scope="col"style={{textAlign:"left", fontSize:16}}>Company</th>
														<th scope="col" style={{textAlign:"left",fontSize:16}}>Mobile</th>
														<th scope="col" style={{textAlign:"left",fontSize:16}}>Designation</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td style={{fontSize:17}}>example@gmail.com</td>
														<td style={{fontSize:17}}>John</td>
														<td style={{fontSize:17}}>Jacob</td>
														<td style={{fontSize:17}}>Google</td>
														<td style={{fontSize:17}}>1234567890</td>
														<td></td>
													</tr>
													<tr>
														<td style={{fontSize:17}}>example@yahoo.com</td>
														<td style={{fontSize:17}}>Alexander</td>
														<td style={{fontSize:17}}>Noah</td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td style={{fontSize:17}}>example@hotmail.com</td>
														<td style={{fontSize:17}}>Ava</td>
														<td style={{fontSize:17}}>Daniel</td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div  className={"col-sm-5 right-column "}>
											<Dropzone noClick={!!uploadedFile}
											          onDrop={(accepted, rejected) => onDrop(accepted, rejected)}
											          multiple={false}
											          accept=".csv"
											>
												{({getRootProps, getInputProps}) => (
													<section className={"w-100"}>
														<div style={{cursor:"pointer"}} {...getRootProps()} className={"dropzone-upload-section"}>
															{
																uploadedFile ?
																	<div>
																		<div className={"uploaded-content"}>
																			{
																				isValidFile &&
																				<p>{uploadedFile.name}</p>
																			}

																			<a href='javascript:void(0)'
																			   onClick={() => handleCloseUploadedfile()}>
																				<img
																					src='src/images/cancel_close_cross_delete_remove_icon.svg'
																					width={"25px"} alt=''/>
																			</a>
																		</div>
																		{
																			isValidFile === false &&
																			<p className={"invalid-file-content my-3 text-center"}
																			   style={{color: "red"}}>Please upload the
																				proper data.</p>
																		}
																	</div>
																	:
																	<div >
																		<input {...getInputProps()}  />
																		<p className={"upload-content"}>Click here to
																			upload the file</p>
																	</div>
															}
														</div>

													</section>
												)}
											</Dropzone>
										</div>
									</div>
								</div>
							</div>
							<div className={"modal-footer-section"}>
								<div
									className={"modal-action-section offset-lg-7 col-lg-5 offset-md-5 col-md-7 col-12"}>
									<Button type={"button"}  variant="secondary"
							        onClick={handleClose}
							>
								CANCEL</Button>
									<button type="button" className="btn import-btn"
									        onClick={importContact}
									        disabled={!isValidFile}
									>
										IMPORT
									</button>
								</div>
							</div>
						</Box>
					</Modal>
					{/* DUPLICATE MEMEBER MODAL*/}
					<Modal
						open={duplicatedContactList.length>0}
						onClose={handleCloseDuplicateModal}
						aria-labelledby="transition-modal-title"
						aria-describedby="transition-modal-description"
						closeAfterTransition
						BackdropComponent={Backdrop}
						BackdropProps={{
							timeout: 1000,
						}}
						disableBackdropClick={true}
						id={"contacts-import-modal"}
					>
						<Box sx={style}>
							<div className={"modal-heading"}>
								<p className={"title"}>DUPLICATE MEMBERS</p>
								<a href='javascript:void(0)'
								   onClick={handleCloseDuplicateModal}
								>
									<img src='src/images/close_icon.svg' alt=''/>

								</a>
							</div>
							<div className={"modal-body-content"}>
								<div className={"container-fluid py-3"}>
									<div className="table-responsive">
										<table className="table table-bordered">
											<thead>
											<tr>
												<th style={{fontSize:17}} scope="col">Email Address</th>
												<th style={{fontSize:17}} scope="col">First Name</th>
												<th style={{fontSize:17}} scope="col">Last Name</th>
												<th style={{fontSize:17}} scope="col">Company</th>
												<th style={{fontSize:17}} scope="col">Mobile</th>
												<th style={{fontSize:17}} scope="col">Designation</th>
											</tr>
											</thead>
											<tbody>
											{
												duplicatedContactList.map((item,index)=>{
													return <tr key={index}>
														<td style={{fontSize:16}}>{item.EmailAddress}</td>
														<td style={{fontSize:16}}>{item.FirstName}</td>
														<td style={{fontSize:16}}>{item.LastName}</td>
														<td style={{fontSize:16}}>{item.Company}</td>
														<td style={{fontSize:16}}>{item.CellPhone}</td>
														<td style={{fontSize:16}}>{item.Designation}</td>
													</tr>
												})
											}
											</tbody>
										</table>
									</div>
								</div>
							</div>

						</Box>
					</Modal>

					<SuccessModal open={openSuccessModal} handleClose={handleCloseSuccessModal}
					              activePage={activePage} setActivePage={setActivePage}/>
					<ConfirmationModal open={openConfirmModal}
					                   handleClose={handleCloseConfirmationModal}
					                   submitConfirm={deleteContact}
					                   contact={selectedContact}
					/>

				</div>
			</div>
			</div>
		</div>
	)
}

export {Contacts};

const style1 = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 600,
	backgroundColor: '#363795',
	// border: '2px solid #000',
	boxShadow: '0px 3px 6px #00000029',
	p: 2,
};

const SuccessModal = (props) => {
	const {open, handleClose, activePage,setActivePage} = props;
	// const [tempActivePage,setTempActivePage]=useState(activePage)
	// console.log("list")
	//
	//
	// console.log("true",activePage)
	return (
		<div className={'successModal-modal-color'}>
			{/* <Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
				className={"custom-success-modal"}
				disableBackdropClick={true}

			>
				<Box sx={style1}>
					<div className={"modal-heading d-flex justify-content-end"}>
						<a href='javascript:void(0)'
						   onClick={handleClose}
						>
							<img src='src/images/close_icon.svg' alt=''/>

						</a>
					</div>
					<div className={"modal-content-section text-center"}>
						<p className={"success-content"}>Successfullly {activePage === "add" ? "added" : "updated"} contact
							information</p>
						<button type={"button"} className={"btn ok-btn mt-5 mb-3"}
						        onClick={handleClose}
						>
							OKAY
						</button>
					</div>
				</Box>
			</Modal> */}
			<Model show={open} 
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                     <Model.Header  >
                    </Model.Header>
                    <Model.Body className='error-modal-content'>
                       
					<p className={"success-content"}>Successfullly {activePage === "add" ? "added" : "updated"} contact
							information</p>
                    </Model.Body>
                    <Model.Footer>
                        
					<Button type={"button"}  variant="primary" style={{marginRight:210}}
						        onClick={handleClose}
						>
							OKAY</Button>
					
                    </Model.Footer>
                </Model>
		</div>
	)
}

const style2 = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 600,
	backgroundColor: '#363795',
	// border: '2px solid #000',
	boxShadow: '0px 3px 6px #00000029',
	p: 2,
};

const ConfirmationModal = (props) => {
	const {open, handleClose, contact, submitConfirm} = props;
	return (
		<div className={'successModal-modal-color'}>
			{/* <Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
				className={"custom-confirm-modal"}
				disableBackdropClick={true}

			>
				<Box sx={style1}>
					<div className={"modal-heading d-flex justify-content-between"}>
						<p className={"heading"}>Confirm</p>
						<a href='javascript:void(0)'
						   onClick={handleClose}
						>
							<img src='src/images/close_icon.svg' alt=''/>

						</a>
					</div>
					<div className={"modal-content-section mt-3"}>
						<p className={"success-content text-left"}>Would you like to delete the selected
							contact({contact.ContactName}) record?</p>
						<div className={"text-center mt-5 mb-3"}>
							<button type={"button"} className={"btn ok-btn mr-3"}
							        onClick={handleClose}
							>
								CANCEL
							</button>
							<button type={"button"} className={"btn ok-btn"}
							        onClick={submitConfirm}
							>
								OKAY
							</button>
						</div>

					</div>
				</Box>
			</Modal> */}
			<Model show={open} 
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Model.Header  >
                        <Model.Title id="example-modal-sizes-title-lg">
                            <p style={{color:"#212529"}} >Confirm</p>
                            
                        </Model.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setOpen(false)}}/> */}
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Model.Header>
                    <Model.Body className='error-modal-content'>
                        <label for="comment">Would you like to delete the selected
							contact({contact.ContactName}) record?</label>
                        <div className="form-group">
                        {/* <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="5" id="comment" /> */}
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        </div>
                        
                    </Model.Body>
                    <Model.Footer>
                        
                        {/* <Button disabled={deletedocreason.length>0 && deletedocreason !== null?false:true} variant="primary"  onClick={() => {
                           deletedocument()
                           setshowdeletedocumentmodule(false)
                           setdeletedocreason("")
                        }}>DELETE</Button>  */}
						<Button type={"button"}  variant="secondary"
							        onClick={handleClose}
							>
								CANCEL</Button>
                         {/* <Button variant="secondary" onClick={() => {
                            setshowdeletedocumentmodule(false)
                            setSelecteddoc(null)
                            setdeletedocreason("")
                        }}>OKAY</Button> */}
						<Button type={"button"} variant="primary"
							        onClick={submitConfirm}
							>
								OKAY</Button>
                    </Model.Footer>
                </Model>
		</div>
	)
}


