import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {Formik, Field, Form, ErrorMessage} from 'formik';
import * as Yup from 'yup';

import {accountService, alertService} from '@/_services';
import {Sidebar} from './Common/Sidebar';
import {Topbar} from './Common/Topbar';
import {contactsService} from "../_services/contacts.service";
import {usersService} from "../_services/users.service";
import Pagination from "react-responsive-pagination";
import * as EmailValidator from "email-validator";
import {Box, Modal} from "@material-ui/core";

import { Button, ButtonToolbar, Modal as Model,ListGroup,ListGroupItem } from 'react-bootstrap';
import EllipsisText from "react-ellipsis-text";
import {commonService} from "../_services/common.service";
import {Avatar} from "@mui/material";
import camera from "@/images/camera - Copy.svg";
import axios from "axios";
import { read, utils, writeFile } from 'xlsx';
import ReactTooltip from 'react-tooltip';
import config from 'config';
const API_END_POINT = config.serverUrl

function Users({history, location}) {
	const [users, setUsers] = useState([]);
	const [searchText, setSearchText] = useState("");
	const [pageCount, setPageCount] = useState(0);
	const [pageNumber, setPageNumber] = useState(1);
	const [pageSize, setPageSize] = useState(5);
	const [activePage, setActivePage] = useState('list');
	const [count,setcount]=useState(0)
	const [userLimit,setUserLimit]=useState(0)

	const [rolesList, setRolesList] = useState([]);
	const [roleType, setRoleType] = useState("0");

	const [firstName, setFirstName] = useState("");
	// const [profilePic,SetProfilePic]=useState('')
	const [lastName, setLastName] = useState("");
	const [emailAddress, setEmailAddress] = useState("");
	const [cellPhone, setCellPhone] = useState("");
	const [designation, setDesignation] = useState("");
	const [addressLine1, setAddressLine1] = useState("");
	const [addressLine2, setAddressLine2] = useState("");
	const [city, setCity] = useState("");
	const [state, setState] = useState("");
	const [zipCode, setZipCode] = useState("");
	const [businessUnit,setBusinessUnit] = useState("");
	const [businessrole,setBusinessrole] = useState("");
	const [locations,setLocation] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [isDomain, setIsDomain] = useState(false);
	const [profilePic, setProfilePic] = useState(null);
	const [userId, setUserId] = useState("");
	const [passwordConfirmationModal, setPasswordConfirmationModal] = useState(false);
	const [resetPasswordSuccessModal, setResetPasswordSuccessModal] = useState(false);
	const [userLimitExceedModal, setUserLimitExceedModal] = useState(false);
	const [END_URL, setEndUrl] = useState(commonService.getImageUrl())
	const [selectedUser, setSelectedUser] = useState({});
	const [userInfo, setUserInfo] = useState({});
	const [openSuccessModal, setOpenSuccessModal] = React.useState(false);
	const [profilePic2, setProfilePic2] = useState(null);
	const [profilePic1, setProfilePic1] = useState(null);
	const [profilepicerror, setprofilepicerror] = useState('');
	const [palceholderEmail,setPLaceHolderEmail]=useState('');
	const [allUsers,setallUsers]=useState([]);
	const [allactiveUserslength,setallactiveUserslength]=useState()
	const [previousIsActive,setpreviousIsActive] = useState(false)
   console.log(allactiveUserslength,userLimit,"22222222")
	const [error, setError] = useState({
		firstName: '',
		lastName: '',
		emailAddress: '',
		designation: '',
		zipCode: '',
		cellPhone:"",
		addressLine1: '',
		addressLine2: '',
		city: '',
		state: '',
		roleType: '',
		businessUnit:"",
		businessrole:"",
		locations:""
	});
	// const [isDomain,setIsDomain]=useState(false)

	useEffect(() => {
		getRoles();
		getUser()
		getallUsersList()
	}, [])
	useEffect(() => {
		getUsersList();
	}, [pageNumber]);
	useEffect(() => {
		if (isDomain===true){
			 
			let tempEmail=(userInfo.email_address).split("@")
			let tempemailer=tempEmail[1]
			let removeemailer=emailAddress.split("@")
			let completeemail= "@"+tempemailer
			// setEmailAddress(completeemail)
			setPLaceHolderEmail(completeemail)
		}else {
			setPLaceHolderEmail("")
		}
	}, [isDomain]);


	const getUser = () => {
		
		usersService.getCurrentUserData()
			.then((result) => {
				//  
				if(result){
					setUserInfo(result.Data)
					getlimit(result.Data.company_id)

				}
			})
			.catch(error => {
			});
	}
	const getlimit = (id) => {
		
		usersService.getUserLimit(id)
			.then((result) => {
				//  
				if(result){
					setUserLimit(result.Entity.NumberOfUsers)
					setcount(0)
					setcount(0)
					// console.log()
				}
			})
			.catch(error => {
			});
	}



	useEffect(() => {
		if (openSuccessModal===false){
			resetForm()
		}
	}, [openSuccessModal]);



	const emailValidater=()=>{
		 
		if (activePage==="add" && emailAddress) {
			let errors = error;

			if (isDomain === true){
				 let tempEmail=(userInfo.email_address).split("@")
				let tempemailer=tempEmail[1]
				let removeemailer=emailAddress.split("@")
				let completeemail= removeemailer[0] + "@"+tempemailer
				// setEmailAddress(completeemail)
				checEmail(completeemail).then(function (data) {
					let removeemailer=emailAddress.split("@")
					setEmailAddress(removeemailer[0])
					 
					if (data === true) {
						errors.emailAddress = 'Email already exist.';
					} else {
						errors.emailAddress = '';
					}
					// setEmailAddress(value);
					setError(errors);
					setcount(count + 1)
					// setError(errors);
				});
				// setEmailAddress(completeemail)
			}else {
				checEmail(emailAddress).then(function (data) {
					// let removeemailer=emailAddress.split("@")
					// setEmailAddress(removeemailer[0])
					 
					if (data === true) {
						errors.emailAddress = 'Email already exist.';
					} else {
						errors.emailAddress = '';
					}
					// setEmailAddress(value);
					setError(errors);
					setcount(count + 1)
					// setError(errors);
				});
			}


		}
	}


	const getRoles = () => {
		let input = {}
		usersService.roleList(input)
			.then((result) => {
				setRolesList(result);
				console.log(result)
				//removed by ds - to make "choose user" as default
				// setRoleType(result[0] ? result[0].RoleId : '0');
			})
			.catch(error => {

			});
	}

	const getUsersList = () => {
		let input = {
			"IsActive": "1",
			"PageNumber": pageNumber,
			"PageSize": pageSize,
			"UserId": null,
			"SearchByText": "",
			"RoleId": null
		}
		usersService.userList(input)
			.then((result) => {
				setUsers(result.data);
				setPageCount(result.PageCount);
				setPageNumber(result.PageNumber);
				setPageSize(result.PageSize);
			})
			.catch(error => {

			});
	}
	const getallUsersList = () => {
		let input = {
			"IsActive": "1",
			"PageNumber": 1,
			"PageSize": 1000000,
			"UserId": null,
			"SearchByText": "",
			"RoleId": null,
			"SortDir": "d",
            "SortKey": "date",
		}
		usersService.userList(input)
			.then((result) => {
				setallUsers(result.data);
				const allactiveUsers = result.data.filter(item => item.is_active == 1 || item.is_active == true)
				setallactiveUserslength(allactiveUsers.length)
			})
			.catch(error => {

			});
	}

	const handleSearchText = (e) => {
		setSearchText(e.target.value);
		if (e.target.value === "") {
			getUsersList();
		}
	}

	const searchUser = () => {
		let input = {
			"IsActive": "1",
			"PageNumber": pageNumber,
			"PageSize": pageSize,
			"UserId": null,
			"SearchByText": searchText,
			"RoleId": null
		}
		usersService.userList(input)
			.then((result) => {
				setUsers(result.data);
				setPageCount(result.PageCount);
				setPageNumber(result.PageNumber);
				setPageSize(result.PageSize);
			})
			.catch(error => {

			});
	}

	const handlePagination = (newPage) => {
		setPageNumber(newPage);
	}

	const resetForm = () => {

		setRoleType("0");
		setFirstName("");
		setLastName("");
		setEmailAddress("");
		setZipCode("");
		setCellPhone("");
		setAddressLine1("");
		setAddressLine2("");
		setCity("");
		setState("");
		setDesignation("");
		setBusinessUnit("");
		setBusinessrole("");
		setLocation("");
		setIsActive(false);
		setIsDomain(false);
		setProfilePic(null);
		setProfilePic2(null);
		setProfilePic1(null);
		setSelectedUser({});
		setActivePage('list');
		setError({
			firstName: '',
			lastName: '',
			emailAddress: '',
			designation: '',
			zipCode: '',
			addressLine1: '',
			cellPhone:"",
			addressLine2: '',
			city: '',
			state: '',
			roleType: '',
			businessrole:"",
			businessUnit:"",
			locations:""
		})
	}

	const addUser = () => {
		let tempEmail=(userInfo.email_address).split("@")
		let tempemailer=tempEmail[1]
		let removeemailer=emailAddress.split("@")
		let completeemail= removeemailer[0] + "@"+tempemailer
		let input = {
			"IsDomain": isDomain,
			"IsActive": isActive,
			"Email": isDomain == true ?completeemail:emailAddress,
			"Address1": addressLine1,
			"FirstName": firstName,
			"Address2": addressLine2,
			"LastName": lastName,
			"Phone": cellPhone,
			"RoleId": roleType,
			"Designation": designation,
			"City": city,
			"State": state,
			"ZipCode": zipCode,
			"Role":businessrole,
			"Department":businessUnit,
			"Location":locations,
			"UserAddProfilePic": {},
			"ProfilePic": [{}],
			"DurationMode": "Annual",
			"PackageId": "1",
			"TrialInDays": "14",
			"CompanyId": "352",
			"IsReact":"1"
		}
		if (validateForm()) {


			document.body.classList.add('loading-indicator');
			let formData = new FormData();
			// if (validation()){
			// 	return ""
			// }
			formData.append("model",JSON.stringify(input) );
			formData.append("ProfilePic[]", profilePic2);
			let token = localStorage.getItem('LoginToken');
			let config;
			if (token !== null && token !== undefined && token !== "") {
				config = {
					headers: {
						'Accept': 'application/json',
						'userauthtoken': token
					}
				};
			} else {
				config = {
					headers: {
						'Accept': 'application/json'
					}
				};
			}

				axios.post(`${API_END_POINT}users/add`, formData, config)
				.then(function (response) {
					
					// result.Logo=response.data.Logo
					setOpenSuccessModal(true);
							getUsersList();
							getallUsersList();
								// setTimeout(() => { resetForm();  }, 1000);
					 			// resetForm();
					document.body.classList.remove('loading-indicator');

				})
				.catch(error => {

				});

		}




			// usersService.addUser(profilePic, input)
			// 	.then((result) => {
			// 		if (result.HttpStatusCode === 200) {
			// 			setOpenSuccessModal(true);
			// 			getUsersList();
			// 			// setTimeout(() => { resetForm();  }, 1000);
			// 			// resetForm();
			// 		}
			// 	})
			// 	.catch(error => {
			//
			// 	});
		}



	const validateForm = () => {
		let count = 0;
		let tmp_errors = {...error};
		if (firstName === "") {
			tmp_errors.firstName = 'First Name is required.';
			count = count + 1;
		} else if (firstName.length < 3) {
			tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
			count = count + 1;
		}
		if (lastName === "") {
			tmp_errors.lastName = 'Last Name is required.';
			count = count + 1;
		}
		if (emailAddress === "") {
			tmp_errors.emailAddress = 'Email Address is required.';
			count = count + 1;
		}
		else if (isDomain=== false && EmailValidator.validate(emailAddress) === false) {
			tmp_errors.emailAddress = 'Enter correct email address.';
			count = count + 1;
		}
		
		if (designation === "") {
			tmp_errors.designation = 'Designation is required.';
			count = count + 1;
		}
		if (businessrole === "") {
			tmp_errors.businessrole = 'Business Role is required.';
			count = count + 1;
		}
		if (businessUnit === "") {
			tmp_errors.businessUnit = 'Department/Business Unit is required.';
			count = count + 1;
		}
		if (locations === "") {
			tmp_errors.locations = 'Location is required.';
			count = count + 1;
		}
		if (roleType === "" || roleType === "0" ) {
			tmp_errors.roleType = 'Access Level is required.';
			count = count + 1;
		}
		if(tmp_errors.emailAddress === 'Email already exist.'){
			count = count + 1;
		}
		if (count <= 0) {
			tmp_errors = {
				firstName: '',
				lastName: '',
				emailAddress: '',
				designation: '',
				zipCode: '',
				addressLine1: '',
				cellPhone:"",
				addressLine2: '',
				city: '',
				state: '',
				roleType: '',
				businessrole:"",
				businessUnit:"",
				locations:""
			}
		}
		setError(tmp_errors);

		return count <= 0;
	}

	const selectUser = (user) => {
		//  

		usersService.getUserById(user.UserId)
			.then((result) => {

				if (result) {

					setActivePage('edit');

					setProfilePic(END_URL + result?.ProfilePicture)
					setRoleType(result.RoleId);
					setFirstName(result.FirstName);
					setLastName(result.LastName);
					setEmailAddress(result.Email);
					setZipCode(result.ZipCode);
					setCellPhone(result.Phone);
					setAddressLine1(result.Address1);
					setAddressLine2(result.Address2);
					setCity(result.City);
					setState(result.State);
					setBusinessrole(result.Role);
					setBusinessUnit(result.Department);
					setLocation(result.Location);
					setDesignation(result.Designation);
					setIsActive(user.is_active === 1);
					setSelectedUser(result);
				}
			})
			.catch(error => {

			});

	}

	const resetPasswordConfirmation = (user) => {

		setPasswordConfirmationModal(true)
		setUserId(user.UserId)

	}

	const resetPasswordAction = () => {
		setPasswordConfirmationModal(false)
		console.log(userId)
		usersService.resetPassword(userId)
			.then((result) => {
				if (result) {
					setResetPasswordSuccessModal(true)
				}
			})
	}

	const editUser = () => {
		
		// let selectedRole = rolesList.filter(function (role) {
		// 	return role.RoleId === roleType
		// });
		let array = [];
		// selectedRole.map((item, index) => {
		// 	array[index] = item.RoleName
		// })
		let input = {
			Address1: addressLine1,
			Address2: addressLine2,
			AddressType: selectedUser.AddressType,
			City: city,
			CountryCode: selectedUser.CountryCode,
			Designation: designation,
			Email: emailAddress,
			FirstName: firstName,
			IsActive: isActive,
			IsAddressPrimary: selectedUser.IsAddressPrimary,
			IsDomain: isDomain,
			IsOwner: selectedUser.IsOwner,
			IsPhonePrimary: selectedUser.IsPhonePrimary,
			LastName: lastName,
			OriginalFilename: selectedUser.OriginalFilename,
			Phone: cellPhone,
			PhoneExt: selectedUser.PhoneExt,
			ProfilePicture: selectedUser.ProfilePicture,
			RegisteredOn: selectedUser.RegisteredOn,
			RoleId: roleType,
			RoleNames: array[0],
			Roles: array,
			State: state,
			TrialInDays: selectedUser.TrialInDays,
			UserAddressId: selectedUser.UserAddressId,
			UserContactId: selectedUser.UserContactId,
			UserId: selectedUser.UserId,
			UserSubscriptionId: selectedUser.UserSubscriptionId,
			ZipCode: zipCode,
			"Role":businessrole,
			"Department":businessUnit,
			"Location":locations,
				"IsReact":"1"
		}

		if (validateForm()) {
			usersService.editUser(selectedUser.UserId, input)
				.then((result) => {
					if (result.HttpStatusCode === 200) {
						setOpenSuccessModal(true);
						getUsersList();
						getallUsersList();
						// setTimeout(() => { resetForm();  }, 1000);
						// resetForm();
					}
				})
				.catch(error => {

				});
		}
	}

	const handleInputChange = (feild, e) => {
		let value = e.target.value;
		let errors = error;
		if(feild == 'isActive'){
			setpreviousIsActive(isActive)
		}
		switch (feild) {
			case 'isActive':
					setIsActive(!isActive)
				break;
			case 'firstName':
				if (value === "") {
					errors.firstName = 'First Name is required.';
				} else if (value.length < 3) {
					errors.firstName = 'First Name must be atleast 3 characters long.';
				} else {
					errors.firstName = ''
				}
				setFirstName(value);
				setError(errors);
				break;
			case 'lastName':
				if (value === "") {
					errors.lastName = 'Last Name is required.';
				} else {
					errors.lastName = ''
				}
				setLastName(value);
				setError(errors);
				break;
			case 'emailAddress':
				if (isDomain === true){
					if (value === "") {
						errors.emailAddress = 'Email Address is required.';
						setEmailAddress(value);
						setError(errors);
					}else {
						errors.emailAddress = '';
						setEmailAddress(value);
						setError(errors);
					}
				}else {
					if (value === "") {
						errors.emailAddress = 'Email Address is required.';
						setEmailAddress(value);
						setError(errors);
					} else if (EmailValidator.validate(value) === false) {
						errors.emailAddress = 'Enter correct email address.';
						setEmailAddress(value);
						setError(errors);
					} else if (EmailValidator.validate(value) === true) {
						errors.emailAddress = '';
						setEmailAddress(value);
						setError(errors);
					}
				}



				break;
			case 'zipCode':
				errors.zipCode = value.length < 5 ? 'ZipCode must be atleast 5 digits long.' : '';
				if (value.length <= 10){
					setZipCode(value);
				}

				setError(errors);
				break;
				
			case 'addressLine1':
				if (value.length < 3) {
					errors.addressLine1 = 'Address must be atleast 3 characters long.';
				} else {
					errors.addressLine1 = ''
				}
				setAddressLine1(value);
				setError(errors);
				break;
				case 'cellPhone':
					if (e.target.value.length === 16) return false;   //limits to 10 digit entry
					setCellPhone(e?.target.value);       //saving input to state
				if (value.length < 10) {
					errors.cellPhone = 'Mobile number must be atleast 10 characters long.';
				} else {
					errors.cellPhone = ''
				}
				setCellPhone(value);
				setError(errors);
				break;
			case 'addressLine2':
				if (value.length < 3) {
					errors.addressLine2 = 'Address Line #2 must be atleast 3 characters long.';
				} else {
					errors.addressLine2 = ''
				}
				setAddressLine2(value);
				setError(errors);
				break;
			case 'city':
				if (value.length < 2) {
					errors.city = 'City must be atleast 2 characters.';
				} else {
					errors.city = ''
				}
				setCity(value);
				setError(errors);
				break;
			case 'state':
				if (value.length < 2) {
					errors.state = 'State must be atleast 2 characters.';
				} else {
					errors.state = ''
				}
				setState(value);
				setError(errors);
				break;
			case 'designation':
				errors.designation = value === "" ? 'Designation is required.' : '';
				setDesignation(value);
				setError(errors);
				break;
			case 'roleType':
				errors.roleType = value === "" || value === "0" ? 'Access Level is required.' : '';
				setRoleType(value);
				setError(errors);
				break;
				case 'businessUnit':
				errors.businessUnit = value === "" ? 'Department/Business Unit is required.' : '';
				setBusinessUnit(value);
				setError(errors);
				break;
			case 'businessrole':
				errors.businessrole = value === "" ? 'Business Role is required.' : '';
				setBusinessrole(value);
				setError(errors);
				break;
			// case 'designation':
			// 	errors.designation = value === "" ? 'Designation is required.' : '';
			// 	setDesignation(value);
			// 	setError(errors);
			// 	break;
			case 'locations':
				errors.locations= value === "" ? 'Location is required.' : '';
				setLocation(value);
				setError(errors);
				break;
			default:
				break;

		}

	}

	const checEmail = (email) => {
		return new Promise(function (resolve, reject) {
			usersService.checkEmailExist(email)
				.then((result) => {
					let value;
					value = !!result.Data.Email;
					resolve(value);
				})
				.catch(error => {

				});
		});
	}
	const handleCloseSuccessModal = () => {
		setOpenSuccessModal(false);
	}
	const clearSearch = () => {
		setSearchText("");
		getUsersList();
	}

	const addUserHandler = (id) => {
		const allactiveUsers = allUsers.filter(item => item.is_active == 1)
		setallactiveUserslength(allactiveUsers.length)
		if(allactiveUsers.length >= id ){
			setUserLimitExceedModal(true)
		}else{
			setActivePage('add')
		}

	}

	// const handleImageChange = (e) => {
	// 	let tmp_profile_data =''
	// 		// {...profileDetail};
	// 	let reader = new FileReader();
	// 	let file = e.target.files[0];
	// 	reader.onloadend = () => {
	// 		tmp_profile_data['profile'] = {
	// 			...tmp_profile_data['profile'],
	// 			profilePic: reader.result
	// 		}
	// 		// setSelectedProfilePic(file);
	// 		// setProfileDetail(tmp_profile_data);
	// 	}
	// 	reader.readAsDataURL(file);
	// }

	function stringAvatar() {
		let name = firstName ?firstName : 'U'
		return {
			sx: {
				bgcolor: "#2F7FED",
				width:'47px',
				height:'47px'
			},
			children: ( name.charAt(0)? `${name.charAt(0)}`: 'NA')
		};
	}

	const handleProfilepic=(e)=> {
		let reader = new FileReader();
		let file = e.target.files[0];
		if (file.size > 1000000){
		setprofilepicerror("The fill should be upto 1 MB")
			setcount(0)
			// setError(errors)
			return ""
		}
		setprofilepicerror("")
	 	reader.onloadend = () => {
			setProfilePic2(file);
		setProfilePic1(reader.result);
		// temp.Logo=reader.result



	 	}

		reader.readAsDataURL(file);

	 }
	 const filterexceldata=()=>{
		let exceldata = []
					allUsers.map(item=>{
					let newitem = {}
					newitem.Name = `${item.FirstName} ${item.LastName}`
					newitem.EmailAddress =item.EmailAddress
					newitem.UsedDocuments = item.UsedDocumentCount+"/"+item.DocumentLimit
					newitem.Active = item.is_active
					newitem.Admin = item.RoleNames
					exceldata.push(newitem)
				})
					const headings = [[
						'Name',
						'Email',
						'Used Documents',
						'Is Active',
						'Is Admin',
					]]
		const wb = utils.book_new();
		const ws = utils.json_to_sheet([]);
		utils.sheet_add_aoa(ws, headings);
		utils.sheet_add_json(ws, exceldata, { origin: 'A2', skipHeader: true });
		utils.book_append_sheet(wb, ws, 'Report');
		writeFile(wb, 'Users Report.xlsx');

	}	




	return (

		<div className="dashboard_home wrapper_other" id="wrapper_other">
			<Sidebar/>

			<div className="main other_main" id="main">
				<Topbar/>

				<div className="main-content main-from-content users">
				<div className="main-content main-from-content signatures" style={{padding:"0px 30px 30px 44px"}}>
					<div className="row">
					{ activePage === 'add' || activePage === 'edit' ?
				
					<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
					   <h3 style={{fontSize:35,fontWeight:300}}>{activePage === 'add' ? "Add User" : "Edit User"}</h3>
				   </div> 
				   
			   :
					<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
									{/* <h3>Users</h3> */}
									<h3 style={{fontSize:35,fontWeight:300}}>Users</h3>
								</div>
							// 	<div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
							// 	{/* <h3>Users</h3> */}
							// 	<h3 style={{fontSize:35,fontWeight:300}}>Granite Factory</h3>
							// </div>
								}
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
							<div className="recipient-btn">
							<button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}} /></button>
								
							</div>
						</div>
					</div>
					{
						activePage === "list" &&
						<>
							<div className="row">
							<div style={{visibility:"hidden"}} className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
									{/* <h3>Users</h3> */}
									<h3 style={{fontSize:35,fontWeight:300}}>Users</h3>
								</div>
								<div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
									<div className="recipient-btn">
										<div className={"search-box"}>
											<div className="input-group">
												<input type="text" className="form-control"
												       placeholder="Search"
												       value={searchText}
												       onChange={(e) => handleSearchText(e)}
												       onKeyPress={(event) => {
													       if (event.key === "Enter") {
														       event.preventDefault();
														       searchUser();
													       }
												       }}
												/>
												{
													searchText === "" ?
														<button type="button" className="bg-transparent search-btn"
														        style={{zIndex: 100}}
														        onClick={searchUser}
														>
															<img src='src/images/search_icon_blue.svg' alt=''/>
														</button> :
														<button type="button" className="bg-transparent search-btn"
														        style={{zIndex: 100}}
														        onClick={clearSearch}
														>
															<img src='src/images/exit.png' alt=''/>
														</button>
												}
											</div>
										</div>
										<div className='add_user white-space-no-wrap'>
											<button className="btn" onClick={() => addUserHandler(userLimit)}>
												<img src="src/images/plus_icon.svg"/>Add User
											</button>
										</div>
										<div className="action_area">
											<ul>
											<li  data-tip data-for="XLSX"><img src="src/images/XLSX.png" alt="" onClick={()=>{filterexceldata()}} style={{width:35,height:35,cursor:"pointer"}} /></li>
											<ReactTooltip id="XLSX" place="top" effect="solid">Export to Excel</ReactTooltip>
												{/* <li><img src="src/images/pdf.svg" alt=""/></li>
												<li><img src="src/images/csv.svg" alt=""/></li> */}
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div className="row align-items-center">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									{
										users.length === 0 ?
											<p className="e-num d-name text-center">No records are found.</p>
											:
											users.map((item, index) => {
												return (
													<div className='document_block' key={index}>
														<ul className='content_area change '>
															<li className={"w-25"} title={`${item.FirstName} ${item.LastName}`}><h6>Name</h6>
																<h5><EllipsisText text={`${item.FirstName} ${item.LastName}`} length={"25"}/></h5>
															</li>
															<li className={"w-25"} title={item.EmailAddress}><h6>Email</h6><h5>
																<EllipsisText text={item.EmailAddress} length={"25"}/>
															</h5></li>
															<li className={"max-width-105"}><h6>Used Documents</h6>
																<h5>{item.UsedDocumentCount ? item.UsedDocumentCount : 0}/{item.DocumentLimit}</h5>
															</li>
															<li className={"max-width-80"}><h6>Is Active</h6> <label className="switch">
																<input type="checkbox" checked={item.is_active === 1}/>
																<span className="slider round"/>
															</label></li>
															<li className={"max-width-80"}><h6>Is Admin</h6> <label className="switch">
																<input type="checkbox" disabled={true} checked={item.RoleNames==="Administrator"}/>
																<span className="slider round"/>
															</label></li>
														</ul>


														<ul className='action_area max-width-105'>

															{index  !== 0  || item.RoleNames!=="Administrator" ?
																<li>
																	<a href='javascript:void(0)'
																	   title={"Edit user"}
																	   onClick={() => selectUser(item)}>
																		<img src='src/images/edit1.svg' alt=''/>
																	</a>
																</li>
																:<></>
															}

															<li>
																<a href='javascript:void(0)'
																   title={"Reset password"}
																   onClick={() => resetPasswordConfirmation(item)}>
																	<img src='src/images/lock.svg' alt=''/>
																</a>
															</li>
															{/*<li><a href='#'><img src='src/images/lock.svg' alt='' /></a></li>*/}
														</ul>
													</div>
												)
											})
									}

								</div>
							</div>
							<div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
											<Pagination
												current={pageNumber}
												total={pageCount}
												onPageChange={(newPage) => handlePagination(newPage)}
											/>
										</nav>
									</div>
								</div>
							</div>
						</>
					}
					{
						activePage === 'add' || activePage === 'edit' ?
							<div className={"user-form"}>
								{/* <div className="row">
									 <div className="col-xl-12">
										<h3 style={{fontSize:35,fontWeight:300}}>{activePage === 'add' ? "Add User" : "Edit User"}</h3>
									</div> 
								</div> */}
								<div className={"user-form-section mt-3"}>
									<div className={"row"}>
										<div className={"col-sm-6 profile-pic-is_active-btn"}>
											<div className={'parent-image text-center  align-items-center position-relative parent-profile-pic'}>
												<label className={'pb-2'}>Profile Pic</label>
												{
													activePage === "add"?
														<>
															{ profilePic1 !== null  ?<img src={profilePic1} alt="" className="user-picture" />: <Avatar {...stringAvatar()} />}
														</>
														:
														<>
															{ selectedUser.ProfilePicture !== null && activePage !== "add"  ?<img src={profilePic} alt="" className="user-picture" />: <Avatar {...stringAvatar()} />}
														</>
												}


												{/*<img alt={"user profile pic"} className={"users-profile-for-edit-user"} src={profilePic}/>*/}

												{
													activePage === "add" &&
													<>
														<input className={"code uploadprofileimage"} id="file-input" type="file"
															   onChange={(e) => handleProfilepic(e)}/>
														<div className={'w-60 img_profile'}>
															<label htmlFor="file-input" className={"parentcameracamer-2 mb-2"}>
																<img src={camera} className={"cameracamera"}/>
																{/*Change Photo*/}
															</label>
															{/* <lable className={"mt-2"} >Add</lable> */}
															{
																profilepicerror !== "" &&
																<p className={"error-msg"}>{profilepicerror}</p>
															}
														</div>
													</>
												}
											</div>
											<div className={'mb-3 custome-toggle-btn'}>
												<div>Is Active</div>
												<label className="switch">
													<input  checked={isActive} disabled={(isActive === false && previousIsActive === false && allactiveUserslength>=userLimit)?true:false}   onChange={(e) => handleInputChange('isActive', e)} type="checkbox"/>
													<span style={{marginLeft:-2}} className="slider round"></span>
												</label>
											</div>
											{
												activePage === "add" &&
												<div className={'mb-3 custome-toggle-btn'}>
													<div>Is Domain</div>
													<label className="switch">
														<input   onInput={()=>setIsDomain(!isDomain)} type="checkbox" />
														<span style={{marginLeft:-2}} className="slider round"></span>
													</label>
												</div>
											}

										</div>

										<div className={"col-sm-6"}>
											<div className={'mb-3'}>
											</div>
										</div>

										<div className={"col-sm-6"}>
											<div className={"mb-3"}>

												{/*<div className={'w-60 img_profile'}>*/}
												{/*	<label htmlFor="file-input">*/}
												{/*		<img*/}
												{/*			src={profilePic}*/}
												{/*			className={'profile_photo'} alt={'profile_photo'}/>*/}


												{/*			<div className={'change_button'}>*/}
												{/*				/!*<img src={white_edit} alt={'white_edit'} className={'white_edit'}/>*!/*/}
												{/*			</div>*/}

												{/*	</label>*/}


												{/*		<input className={"code uploadprofileimage"} id="file-input" type="file"*/}
												{/*			   onChange={(e) => handleImageChange(e)}/>*/}



												{/*</div>*/}


												<select className="form-select" aria-label="Default select example"
												        value={roleType}
														style={{display: "block", width: "100%",padding: ".375rem .75rem",fontSize: "1rem",fontWeight: "400",lineHeight: "1.5",color: "#212529",backgroundColor: "#fff",backgroundClip: "padding-box"}}
												        onChange={(e) => handleInputChange('roleType', e)}
												>
													<option value={"0"} defaultValue={true}>Choose Access level
													</option>

													{
														rolesList.map((item, index) => {
															return (
																<option disabled={item.RoleId>2?true:false} value={item.RoleId}
																        key={index}>{item.RoleName}</option>
															)
														})
													}
												</select>
												<p className={"error-msg"}>{error.roleType}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											 <div className="mb-3">
											{/*<select className="form-select" aria-label="Default select example"
											//value={}
											style={{display: "block", width: "100%",padding: ".375rem .75rem",fontSize: "1rem",fontWeight: "400",lineHeight: "1.5",color: "#6c757d",backgroundColor: "#fff",backgroundClip: "padding-box"}}
                                              >
													<option>Organization Details</option>
													<option>Department/Business Unit</option>
													<option>Role</option>
													<option>Designation</option>
													<option>Location</option>
												 </select>
												<p className={"error-msg"}>{error.addressLine2}</p>*/}
											</div> 
										</div> 
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input type="text" className="form-control"
												       placeholder="First Name"
												       value={firstName}
												       onChange={(e) => handleInputChange('firstName', e)}
												       title="please fill out this feild"
												/>
												<p className={"error-msg"}>{error.firstName}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input type="text" className="form-control"
												       placeholder="Last Name"
												       value={lastName}
												       onChange={(e) => handleInputChange('lastName', e)}
												       title="please fill out this feild"
												/>
												<p className={"error-msg"}>{error.lastName}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input type="text" className="form-control"
												    placeholder="Email Address"
													value={emailAddress}
													disabled={activePage === "edit"}
													onChange={(e) => handleInputChange('emailAddress', e)}
													onBlur={()=>emailValidater()}
												       title="please fill out this feild"
												/>
												<p className={"custome-email-input-tag"}  style={{position:"relative",left:"363px",top:"-39px",color:"#ced4da"}}>{palceholderEmail}</p>
												<p className={"error-msg"}>{error.emailAddress}</p>
											</div>
										</div>
										{/* <div className={"col-sm-6"}>
											<div className={"mb-3 parent-custome-email-input-tag"}>
												<input type="email" className="form-control "
												       placeholder="Email Address"
												       disabled={activePage === "edit"}
												       value={emailAddress}
												       onChange={(e) => handleInputChange('emailAddress', e)}
													   onBlur={()=>emailValidater()}
												       title="please fill out this feild"
												/>
												<p className={"custome-email-input-tag"}>{palceholderEmail}</p>
												<p className={"error-msg"}>{error.emailAddress}</p>
											</div>
										</div> */}
										<div className={"col-sm-6"}>
											<div className={"mb-3"}>
												<input  type="number" className="form-control"
												       placeholder="Mobile"
												       onChange={(e) => {handleInputChange('cellPhone', e)
													      
												       }
												       }
												       value={cellPhone}
												/>
												<p className={"error-msg"}>{error.cellPhone}</p>
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className="mb-3 ">
												<input type="text" className="form-control "
												       placeholder="Address"
												       value={addressLine1}
												       onChange={(e) => handleInputChange('addressLine1', e)}
												/>

												<p className={"error-msg"}>{error.addressLine1}</p>
											</div>
										</div>
										
										
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="City"
												       value={city}
												       onChange={(e) => handleInputChange('city', e)}
												/>
												<p className={"error-msg"}>{error.city}</p>
											</div>
										</div>
									
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
												       placeholder="State"
												       value={state}
												       onChange={(e) => handleInputChange('state', e)}
												/>
												<p className={"error-msg"}>{error.state}</p>
											</div>
										</div>

									
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
													   placeholder="Zip Code"
													   onChange={(event) => {
														   if (event.target.value.length === 11) return false;   //limits to 10 digit entry
														   // setZipCode(event?.target.value);
														   handleInputChange('zipCode', event)//saving input to state
													   }
													   }
													   value={zipCode}
												/>
												<p className={"error-msg"}>{error.zipCode}</p>
											</div>
										</div>
										<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h6 style={{marginTop:-13,marginBottom:20}}>Organization Details :<span style={{color:"red"}}>*</span></h6>
                                                </div>
												<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
													   placeholder="Department/Business Unit"
													   onChange={(event) => {
														  // if (event.target.value.length === 11) return false;   //limits to 10 digit entry
														   // setZipCode(event?.target.value);
														   handleInputChange('businessUnit', event)//saving input to state
													   }
													   }
													   value={businessUnit}
												/>
												 <p className={"error-msg"}>{error.businessUnit}</p> 
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
													   placeholder="Business Role"
													   onChange={(event) => {
														  // if (event.target.value.length === 11) return false;   //limits to 10 digit entry
														   // setZipCode(event?.target.value);
														   handleInputChange('businessrole', event)//saving input to state
													   }
													   }
													   value={businessrole}
												/>
												<p className={"error-msg"}>{error.businessrole}</p> 
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
													   placeholder="Designation"
													   onChange={(event) => {
														  // if (event.target.value.length === 11) return false;   //limits to 10 digit entry
														   // setZipCode(event?.target.value);
														   handleInputChange('designation', event)//saving input to state
													   }
													   }
													   value={designation}
												/>
												 <p className={"error-msg"}>{error.designation}</p> 
											</div>
										</div>
										<div className={"col-sm-6"}>
											<div className="mb-3">
												<input type="text" className="form-control"
													   placeholder="Location"
													   onChange={(event) => {
														  // if (event.target.value.length === 11) return false;   //limits to 10 digit entry
														   // setZipCode(event?.target.value);
														  handleInputChange('locations', event)//saving input to state
													   }
													   }
													 value={locations}
												/>
												 <p className={"error-msg"}>{error.locations}</p> 
											</div>
										</div>
									 </div>
								</div>
									<div className={"form-action mt-3"}>
										<button type="button" className="btn btn-secondary" style={{ width: 105 }}
											onClick={resetForm}>CANCEL
										</button>
										<button type="button" className="btn save-btn" style={{ width: 110, backgroundColor: "#2F7FED", border: "1px solid #2F7FED" }}
											onClick={activePage === "add" ? addUser : editUser}>SAVE
										</button>
									</div>
							</div> : null
					}
					<SuccessModal open={openSuccessModal}
					              handleClose={handleCloseSuccessModal}
					              activePage={activePage}
					/>

				</div>
			</div>
			</div>
			<PasswordConfiramtionModal passwordConfirmationModal={passwordConfirmationModal}
			                           resetPasswordAction={(args) => resetPasswordAction(args)}
			                           setPasswordConfirmationModal={setPasswordConfirmationModal}/>

			<ResetPasswordSuccess resetPasswordSuccessModal={resetPasswordSuccessModal}
			                      setResetPasswordSuccessModal={setResetPasswordSuccessModal}/>

			<UserLimitExceed userLimitExceedModal={userLimitExceedModal}
			                 setUserLimitExceedModal={setUserLimitExceedModal}/>
		</div>
	)
}

export {Users};

const style1 = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 600,
	backgroundColor: '#363795',
	// border: '2px solid #000',
	boxShadow: '0px 3px 6px #00000029',
	p: 2,
};


const SuccessModal = (props) => {
	const {open, handleClose, activePage} = props;
	return (
		<>
			{/* <Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
				className={"custom-success-modal"}
				disableBackdropClick={true}
			

			>
				<Box sx={style1}>
					<div className={"modal-heading d-flex justify-content-end"}>
						<a href='javascript:void(0)'
						   onClick={handleClose}
						>
							<img src='src/images/close_icon.svg' alt=''/>

						</a>
					</div>
					<div className={"modal-content-section text-center"}>
						<p className={"success-content"}>Successfullly {activePage === "add" ? "added" : "updated"} user
							information</p>
						<button type={"button"} className={"btn ok-btn mt-5 mb-3"}
						        onClick={handleClose}
						>
							OKAY
						</button>
					</div>
				</Box>
			</Modal> */}
			<Model show={open} 
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Model.Header  >
                        <Model.Title id="example-modal-sizes-title-lg">
                           {/* // <p style={{color:"#212529"}} >Confirm</p> */}
						  
                        </Model.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setOpen(false)}}/> */}
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Model.Header>
                    <Model.Body className='error-modal-content'>
					<p className={"success-content"}>Successfullly {activePage === "add" ? "added" : "updated"} user
							information.</p>
							<div>
                        {/* <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="5" id="comment" /> */}
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        </div>
                        
                    </Model.Body>
                    <Model.Footer>
					<Button style={{marginRight:210}} type={"button"} variant="primary"
						        onClick={handleClose}
						>
							OKAY
							</Button>
				   </Model.Footer>
                </Model>
		</>
	)
}

const PasswordConfiramtionModal = (props) => {
	const {passwordConfirmationModal, setPasswordConfirmationModal, resetPasswordAction} = props;
	return (
		<>
			{/* <Modal
				open={passwordConfirmationModal}
				onClose={() => setPasswordConfirmationModal(false)}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
				className={"custom-confirm-modal"}
				disableBackdropClick={true}

			>
				<Box sx={style1}>
					<div className={"modal-heading d-flex justify-content-between"}>
						<p className={"heading"}>Confirm</p>
						<a href='javascript:void(0)'
						   onClick={() => setPasswordConfirmationModal(false)}
						>
							<img src='src/images/close_icon.svg' alt=''/>

						</a>
					</div>
					<div className={"modal-content-section mt-3"}
					>
						<p className={"success-content"}>Are you sure you want to update password?</p>
						<div className={"text-center mt-5 mb-3"}>
							<button type={"button"} variant="secondary" className={"btn ok-btn mr-3"}
							        onClick={() => setPasswordConfirmationModal(false)}
							>
								CANCEL
							</button>
							<button type={"button"} className={"btn ok-btn"}
							        onClick={() => resetPasswordAction()}
							>
								OKAY
							</button>
						</div>

					</div>
				</Box>
			</Modal> */}
			<Model show={passwordConfirmationModal} 
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Model.Header  >
                        <Model.Title id="example-modal-sizes-title-lg">
                           {/* // <p style={{color:"#212529"}} >Confirm</p> */}
						   <p className={"heading"}>Confirm</p>
                        </Model.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setOpen(false)}}/> */}
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Model.Header>
                    <Model.Body className='error-modal-content'>
					<p className={"success-content"}>Are you sure you want to update password?</p>
                        <div className="form-group">
                        {/* <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="5" id="comment" /> */}
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        </div>
                        
                    </Model.Body>
                    <Model.Footer>
					<Button type={"button"} variant="secondary"
							        onClick={() => setPasswordConfirmationModal(false)}
							>
								CANCEL
								</Button>
							<Button type={"button"} variant="primary"
							        onClick={() => resetPasswordAction()}
							>
								OKAY
								</Button>
                      
						
                    </Model.Footer>
                </Model>
		</>
	)
}

const ResetPasswordSuccess = (props) => {
	const {resetPasswordSuccessModal, setResetPasswordSuccessModal} = props;
	return (
		<>
			{/* <Modal
				open={resetPasswordSuccessModal}
				onClose={() => setResetPasswordSuccessModal(false)}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
				className={"custom-confirm-modal"}
				disableBackdropClick={true}

			>
				<Box sx={style1}>
					<div className={"modal-heading d-flex justify-content-between"}>
						<p className={"heading"}>Success!</p>
						<a href='javascript:void(0)'
						   onClick={() => setResetPasswordSuccessModal(false)}
						>
							<img src='src/images/close_icon.svg' alt=''/>

						</a>
					</div>
					<div className={"modal-content-section mt-3"}
					>
						<p className={"success-content"}>Password has been reset and sent him/her an email with the new
							password.</p>
						<div className={"text-center mt-5 mb-3 "}>
							<button type={"button"} className={"btn ok-btn mr-3 width:100px font-size:1.5rem"}
							        onClick={() => setResetPasswordSuccessModal(false)}
							>
								OKAY
							</button>

						</div>

					</div>
				</Box>
			</Modal> */}
			<Model show={resetPasswordSuccessModal} 
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Model.Header  >
                        <Model.Title id="example-modal-sizes-title-lg">
                           {/* // <p style={{color:"#212529"}} >Confirm</p> */}
                           <p className={"heading"}>Success!</p>
                        </Model.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setOpen(false)}}/> */}
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Model.Header>
                    <Model.Body className='error-modal-content'>
					<p className={"success-content"}>Password has been reset and sent him/her an email with the new
							password.</p>
                        <div className="form-group">
                        {/* <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="5" id="comment" /> */}
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        </div>
                        
                    </Model.Body>
                    <Model.Footer>
                        
                        {/* <Button disabled={deletedocreason.length>0 && deletedocreason !== null?false:true} variant="primary"  onClick={() => {
                           deletedocument()
                           setshowdeletedocumentmodule(false)
                           setdeletedocreason("")
                        }}>DELETE</Button>  */}
						 <Button type={"button"} variant="primary" style={{marginRight:210}}
							        onClick={() => setResetPasswordSuccessModal(false)}
							>
								OKAY
								</Button>
                    </Model.Footer>
                </Model>
		</>
	)
}

const UserLimitExceed = (props) => {
	const {userLimitExceedModal, setUserLimitExceedModal} = props;
	return (
		<>
			{/* <Modal
				open={userLimitExceedModal}
				onClose={() => setUserLimitExceedModal(false)}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
				className={"custom-confirm-modal"}
				disableBackdropClick={true}

			>
				<Box sx={style1}>
					<div className={"modal-heading d-flex justify-content-between"}>
						<p className={"heading"}>Users Limit Reached</p>
						<a href='javascript:void(0)'
						   onClick={() => setUserLimitExceedModal(false)}
						>
							<img src='src/images/close_icon.svg' alt=''/>

						</a>
					</div>
					<div className={"modal-content-section mt-3"}
					>
						<p className={"success-content"}>Need to add more users? Upgrade to add more users and access
							advanced features..</p>
						<div className={"text-center mt-5 mb-3"}>
							<button type={"button"} className={"btn ok-btn mr-3"}
							        onClick={() => setUserLimitExceedModal(false)}
							>
								UPGRADE
							</button>
							<button type={"button"} className={"btn ok-btn mr-3"}
							        onClick={() => setUserLimitExceedModal(false)}
							>
								NO THANKS
							</button>

						</div>

					</div>
				</Box>
			</Modal> */}
			<Model show={userLimitExceedModal} 
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Model.Header  >
                        <Model.Title id="example-modal-sizes-title-lg">
                           {/* // <p style={{color:"#212529"}} >Confirm</p> */}
                            <p className={"heading"}>Users Limit Reached</p>
                        </Model.Title>
                        {/* <button type="button" className="close_icon" style={{backgroundColor:"#ffffff",color:"#6E7278"}} onClick={()=>{setdeletedocreason(""),setshowdeletedocumentmodule(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                        {/* <CloseIcon className="close_add_doc_pop" onClick={()=>{setOpen(false)}}/> */}
                        {/* <button type="button" calssName="close" onClick={()=>{setshowdeletedocumentmodule(false)}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button> */}
                    </Model.Header>
                    <Model.Body className='error-modal-content'>
					<p className={"success-content"}>Need to add more users? Upgrade to add more users and access
							advanced features.</p>
                        <div className="form-group">
                        {/* <textarea style={{width:"100%"}} type="text" calssName="form-control" rows="5" id="comment" /> */}
                        {/* {deletedocreason.length<=0? <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>Reason is Required</p>:""} */}
                        </div>
                        
                    </Model.Body>
                    <Model.Footer>
                        
                        {/* <Button disabled={deletedocreason.length>0 && deletedocreason !== null?false:true} variant="primary"  onClick={() => {
                           deletedocument()
                           setshowdeletedocumentmodule(false)
                           setdeletedocreason("")
                        }}>DELETE</Button>  */}
						<Button type={"button"} 
							        onClick={() => setUserLimitExceedModal(false)}
							>
								UPGRADE
								</Button>
								<Button type={"button"} 
								 variant="secondary"
							        onClick={() => setUserLimitExceedModal(false)}
							>
								NO THANKS
								</Button>
						
                    </Model.Footer>
                </Model>
		</>
	)
}