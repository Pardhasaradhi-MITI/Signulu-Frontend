import React from "react";
import { useState, useEffect, useRef, forwardRef, useImperativeHandle } from "react";
import KeyboardOutlinedIcon from '@mui/icons-material/KeyboardOutlined';
import BorderColorOutlinedIcon from '@mui/icons-material/BorderColorOutlined';
import { usersService } from "../../_services/users.service";
import upload from "../../../src/images/upload.svg"
import { SignatureService } from "../../_services/signature.service";
import close_icon from "../../images/cancel_close_cross_delete_remove_icon.svg";
import SignaturePad from 'react-signature-canvas';
import "../../css/SignaturePopup.css"
import ReactTooltip from 'react-tooltip';
import UploadIcon from '@mui/icons-material/Upload';
import SettingsIcon from '@mui/icons-material/Settings';
let EditObj = {}
const SignatureControlEditPopup = forwardRef(({
    setSelectedSignatureControlEditData,
    selectedcontrolObj,
    deleteHandler,
    selection,
    setSelection,
    errorStatus,
    setErrorStatus,
    disableManage,
    getSelectedPage, 
    isPageChange,
    pageType }, ref) => {
    const [getUserInfo, setUserInfo] = useState({})
    const [trimmedDataURL1, settrimmedDataURL1] = useState('')
    const [trimmedDataURL2, settrimmedDataURL2] = useState('')
    const [checkbox, setCheckbox] = useState(0)
    const [signValue1, setSignValue1] = useState('')
    const [signValue2, setSignValue2] = useState('')
    const [signValue3, setSignValue3] = useState(null)
    const [signValue4, setSignValue4] = useState(null)
    const [TypeCode, setTypeCode] = useState('')
    const [activeTage, setActiveTag] = useState("firstTab")
    const [fileerror, setfileerror] = useState("")
    const [MandatoryCheck, setMandatoryCheck] = useState(false)
    const [SelectedPage, setSelectedPage] = useState('')
    let signaturepad1 = useRef(null)
    let signaturepad2 = useRef(null)

    let sigPad1 = useRef(null)
    let sigPad2 = useRef(null)
    useEffect(() => {
        if(!pageType) {
            isPageChange(false)
        }
        // getUser()
        getSelectedDataManage()
        if (selection.draw == true || selectedcontrolObj.IsInPerson) {
            setActiveTag("firstTab")
            selection.draw = true
            selectionHandler('draw')
        }
        if (selection.upload == true) {
            setActiveTag('secondTab')
        }
        if (selection.choose == true) {
            setActiveTag('thirdTab')
        }
        //if(pageType) {
            setUserInfo(selectedcontrolObj)
        //}
        if(selectedcontrolObj.SelectPage){
            if(!pageType) {
                getSelectedPage(selectedcontrolObj.SelectPage)
            }
            setSelectedPage(selectedcontrolObj.SelectPage)
            EditObj.SelectPage = selectedcontrolObj.SelectPage 
        } else {
            EditObj.SelectPage= ''
        };

    }, []);

    useEffect(() => {
        resizeCanvas();
        window.addEventListener("resize", resizeCanvas);
    },[signaturepad1,signaturepad2])
    function resizeCanvas() {
        const canvas1 = document.getElementsByClassName('sigCanvas1')[0]
        const canvas2 = document.getElementsByClassName('sigCanvas2')[0]
        // const ratio =  Math.max(window.devicePixelRatio || 1, 1);
        if (canvas2) {
        canvas2.width = canvas2.offsetWidth;
        canvas2.height = canvas2.offsetHeight;
        canvas2.getContext("2d").scale(1, 1);
        }
        if (canvas1) {
            canvas1.width = canvas1.offsetWidth;
            canvas1.height = canvas1.offsetHeight;
            canvas1.getContext("2d").scale(1, 1);
        }
    }
    useImperativeHandle(ref, () => ({
        clear
    }))
    const getSelectedDataManage = () => {
        if (selectedcontrolObj && selectedcontrolObj.IsMandatory) {
            EditObj.IsMandatory = true
            setMandatoryCheck(true)
        } else {
            EditObj.IsMandatory = false
            setMandatoryCheck(false)
        }
        if (selectedcontrolObj && selectedcontrolObj.SignType == 'Sign') {
            EditObj.SignData = selectedcontrolObj.SignatureUpload ? selectedcontrolObj.SignatureUpload : '';
            EditObj.SignInitial = selectedcontrolObj.InitialUpload ? selectedcontrolObj.InitialUpload : '';
            EditObj.SignUpload = '';
            EditObj.InitialUpload = '';
            EditObj.SignatureFont = ''
            selectedcontrolObj.SignatureUpload ? settrimmedDataURL1(selectedcontrolObj.SignatureUpload) : '';
            selectedcontrolObj.InitialUpload ? settrimmedDataURL2(selectedcontrolObj.InitialUpload) : '';
            setSignValue1('');
            setSignValue2('');
            setCheckbox(0);
        } else if (selectedcontrolObj && selectedcontrolObj.SignType == 'Upload') {
            EditObj.SignData = ''
            EditObj.SignInitial = ''
            EditObj.SignatureFont = ''
            EditObj.SignUpload = selectedcontrolObj.SignatureUpload;
            EditObj.InitialUpload = selectedcontrolObj.InitialUpload;
            selectedcontrolObj.SignatureUpload ? setSignValue1(selectedcontrolObj.SignatureUpload) : '';
            selectedcontrolObj.InitialUpload ? setSignValue2(selectedcontrolObj.InitialUpload) : '';
            setCheckbox(0);
            settrimmedDataURL1('');
            settrimmedDataURL2('');
        } else if (selectedcontrolObj && selectedcontrolObj.SignType == 'Font') {
            EditObj.SignatureFont = selectedcontrolObj.SignatureFont
            EditObj.SignUpload = '';
            EditObj.InitialUpload = '';
            EditObj.SignInitial = ''
            selectedcontrolObj.SignatureFont ? setCheckboxVlaue(selectedcontrolObj.SignatureFont) : '';
            setSignValue1('')
            setSignValue2('')
            settrimmedDataURL1('')
            settrimmedDataURL2('')
        }
        EditObj.signType = selectedcontrolObj.SignType;
        EditObj.SignatureInitial = selectedcontrolObj.SignatureInitial
        EditObj.SignatureName = selectedcontrolObj.SignatureName
        setSelectedSignatureControlEditData(EditObj)
        if (selection.draw) {
            sigPad1.clear()
            sigPad2.clear()
        }
        console.log(sigPad1, sigPad2, '&&&&&&')
    }

    const getMandatory = (event) => {
        EditObj.IsMandatory = event.target.checked
        setMandatoryCheck(event.target.checked)
        setSelectedSignatureControlEditData(EditObj)
    }
    function clear() {
        if (selection.draw) {
            sigPad1.clear()
            sigPad2.clear()
        }
        settrimmedDataURL1("")
        settrimmedDataURL2("")
        setSignValue1("")
        setSignValue2("")
        setSignValue3(null)
        setSignValue4(null)
        setCheckbox(0)
        EditObj.SignUpload = '';
        EditObj.InitialUpload = '';
        EditObj.SignatureFont = ''
        EditObj.SignData = ''
        EditObj.SignInitial = ''
        EditObj.IsMandatory = true
        setMandatoryCheck(true)
        selectedcontrolObj.SelectPage ? setSelectedPage(selectedcontrolObj.SelectPage) : setSelectedPage('')
        selectedcontrolObj.SelectPage ? getSelectedPage(selectedcontrolObj.SelectPage) : getSelectedPage('')
        isPageChange(false)
        setSelectedSignatureControlEditData(EditObj)
    }
    const getSelectPage=(e)=> {
        isPageChange(true)
        setSelectedPage(e.target.value)
        getSelectedPage(e.target.value)
    }
    function setCheckboxVlaue(value) {
        switch (value) {
            case "att-ad-brush-script":
                setCheckbox(1)
                break;
            case "att-ad-forte":
                setCheckbox(2)
                break;
            case "att-ad-freestyle-script":
                setCheckbox(3)
                break;
            case "att-ad-harlow-solid-italic":
                setCheckbox(4)
                break;
            case "att-ad-blackadder-itc":
                setCheckbox(5)
                break;
            case "att-ad-vivaldi":
                setCheckbox(6)
                break;

        }
    }
    //Vertical Tabs
    const getUser = () => {

        usersService.getCurrentUserData()
            .then((result) => {
                if (result && !pageType) {
                    // setUserInfo(result.Data)
                }
            })
            .catch(error => {

            });
    }
    function readImgUrlAndPreview1(input) {
        let reader
        if (input.target && input.target.files[0]) {
            let file = input.target.files[0]
            if (file.size > 102400) {
                setfileerror("The file should be upto 100KB.")

                return ""
            }
            setfileerror('')
            reader = new FileReader();
            reader.onload = function (e) {
                setSignValue1(e.target.result)
                setSignValue3(null)
                setTypeCode("SIGN_UPLOAD_FULL")
                EditObj.SignUpload = e.target.result,
                    EditObj.signType = 'Upload'
                setSelectedSignatureControlEditData(EditObj)
            }
        };
        reader.readAsDataURL(input.target.files[0]);
    }

    function readImgUrlAndPreview2(input) {
        let reader
        if (input.target && input.target.files[0]) {
            let file = input.target.files[0]
            if (file.size > 102400) {
                setfileerror("The file should be upto 100KB.")
                return ""
            }
            setfileerror('')
            reader = new FileReader();
            reader.onload = function (e) {
                setSignValue2(e.target.result)
                setSignValue4(null)
                setTypeCode("SIGN_UPLOAD_INITIAL")
                EditObj.InitialUpload = e.target.result,
                    EditObj.signType = 'Upload'
                setSelectedSignatureControlEditData(EditObj)
            }
        }
        ;
        reader.readAsDataURL(input.target.files[0]);
    }

    const chageCheckFun = (index, value) => {
        setCheckbox(index)
        EditObj.SignatureFont = value,
            EditObj.signType = 'Font',
            EditObj.SignatureInitial = shortName(),
            EditObj.SignatureName = getUserInfo.FirstName + ' ' + getUserInfo.LastName,
            setSelectedSignatureControlEditData(EditObj)
    }

    const sigpadEnd = (e) => {
        if (e === 1) {
            EditObj.SignData = sigPad1.getTrimmedCanvas().toDataURL('image/png'),
                EditObj.signType = 'Sign'
            settrimmedDataURL1(sigPad1.getTrimmedCanvas().toDataURL('image/png'))
        }
        if (e === 2) {
            EditObj.SignInitial = sigPad2.getTrimmedCanvas().toDataURL('image/png');
            EditObj.signType = 'Sign'
            settrimmedDataURL2(sigPad2.getTrimmedCanvas().toDataURL('image/png'))
        }
        setSelectedSignatureControlEditData(EditObj)
    }
    const delete_sigpadEnd = (e) => {
        if (e === 1) {
            if (activeTage !== "secondTab") {
                settrimmedDataURL1('')
                sigPad1? sigPad1.clear() : '';
                EditObj.SignData = '';
            } else {
                setSignValue1('')
                EditObj.SignUpload = '';

            }
        }
        if (e === 2) {
            if (activeTage !== "secondTab") {
                settrimmedDataURL2('')
                sigPad2 ? sigPad2.clear() : '';
                EditObj.SignInitial = '';

            } else {
                setSignValue2('')
                EditObj.InitialUpload = '';            }
        }
        if (e === 3) {
            setSignValue3(null)

        }
        if (e === 4) {
            setSignValue4(null)
        }
        setSelectedSignatureControlEditData(EditObj)
    }
    const shortName = () => {
        let tempFirstName = getUserInfo.FirstName
        let tempLastName = getUserInfo.LastName
        let shortname1 = tempFirstName && tempFirstName.slice(0, 1)
        let shortname2 = tempLastName && tempLastName.slice(0, 1)
        let stortName = shortname1 + shortname2
        return stortName
    }

    const selectionHandler = (selected) => {
        if (selected == "draw") {
            setActiveTag('firstTab')
        }
        if (selected == "upload") {
            setActiveTag('secondTab')
        }
        if (selected == "choose") {
            setActiveTag('thirdTab')
        }
        for (const key in selection) {

            if (key == selected) {
                selection[key] = true;
            }
            else {
                selection[key] = false;
            }
        }
        setErrorStatus(false)
        setSelection({ ...selection })
    }
    return (
        <div className='signPopup_Prepare'>
            <div className="selectIcons_Prepare">
                <div className="signSideBar_Prepare">
                    <div style={{ cursor: "pointer", borderLeft: selection.draw ? "5px solid #4673d2" : "none" }}>
                        <BorderColorOutlinedIcon className='item_Prepare' data-tip data-for="draw" style={{ cursor: "pointer" }} onClick={() => { selectionHandler("draw") }} />
                        <ReactTooltip id="draw" place="top" effect="solid" >
                            Draw
                        </ReactTooltip>
                    </div>
                   {!selectedcontrolObj.IsInPerson ? <div style={{ cursor: "pointer", borderLeft: selection.upload ? "5px solid #4673d2" : "none" }}>
                        <UploadIcon className='item_Prepare' data-tip data-for="upload" style={{ cursor: "pointer" }} onClick={() => { selectionHandler("upload") }} />
                        {/* <CameraAltOutlinedIcon className='item_Prepare' data-tip data-for="upload" style={{ cursor: "pointer"}} onClick={() => { selectionHandler("upload") }} /> */}
                        <ReactTooltip id="upload" place="top" effect="solid" >
                            Upload
                        </ReactTooltip>
                    </div> : null}
                    {!selectedcontrolObj.IsInPerson ? <div style={{ cursor: "pointer", borderLeft: selection.choose ? "5px solid #4673d2" : "none" }}>
                        <KeyboardOutlinedIcon className='item_Prepare' data-tip data-for="choose" style={{ cursor: "pointer" }} onClick={() => { selectionHandler("choose") }} />
                        <ReactTooltip id="choose" place="top" effect="solid" >
                            Select
                        </ReactTooltip>
                    </div > : null
                    }
                   {!disableManage ? <div style={{ cursor: "pointer", borderLeft: selection.setting ? "5px solid #4673d2" : "none" }}>
                        {/* <AccessTimeIcon className='item_Prepare' data-tip data-for="setting" style={{ cursor: "pointer"}} onClick={() => { selectionHandler("setting") }} /> */}
                        <SettingsIcon className='item_Prepare' data-tip data-for="setting" style={{ cursor: "pointer" }} onClick={() => { selectionHandler("setting") }} />
                        <ReactTooltip id="setting" place="top" effect="solid" >
                            Manage
                        </ReactTooltip>
                    </div>: null}
                </div>
            </div>
            {selection.draw ?
                <div className="signaturePopup " style={{ width: '100%' }}>
                    {/* <p>Draw in it</p> */}
                    <h3 style={{ marginBottom: '10px' }} >Draw in it</h3>
                    {errorStatus ? <p style={{ color: "red" }}>Please sign the signature/Initial.</p> : null}
                    <div className="sign_draw_card" >
                    <div style={trimmedDataURL1 ? { height: 380, borderRadius: '5px',marginBottom:'20px' } : { height: 'auto', borderRadius: '5px', marginBottom:'20px' }} className={'parent-tag-value-name'}>
                    <div className={'tag-value-Name'}>Name</div>
                                            <SignaturePad id='nameCanvas'
                                                canvasProps={{width:'360', height:'140', className: 'sigCanvas1', }}
                                                ref={(ref) => {
                                                    sigPad1 = ref
                                                    signaturepad1 = ref
                                                }}
                                                onEnd={() => sigpadEnd(1)}
                                            />
                                            {trimmedDataURL1 !== ""
                                                ? <div >
                                                 <img style={{position:'relative'}} alt={'close'} src={close_icon} onClick={() => delete_sigpadEnd(1)} className={"sign-close-icon-delete"} />
                                                    <img alt='signature' style={{ display: 'flex', justifyContent: 'center'}}
                                                        src={trimmedDataURL1} />
                                                </div>
                                                : null}
                                        </div>

                                        <div style={trimmedDataURL2 ? { height: 380, borderRadius: '5px' } : { height: 'auto', borderRadius: '5px' }} className={'parent-tag-value-name'}>
                                            <div className={'tag-value-Name'}>Initial</div>
                                            <SignaturePad id='intialCanvas'
                                                canvasProps={{ width:'250', height:'140', className: 'sigCanvas2' }}
                                                ref={(ref) => {
                                                    sigPad2 = ref
                                                    signaturepad2 = ref

                                                }}
                                                onEnd={() => sigpadEnd(2)}
                                            />
                                            {trimmedDataURL2 !== ""
                                                ? <div >
                                                   <img alt={'close'} style={{position:'relative'}} src={close_icon} onClick={() => delete_sigpadEnd(2)} className={"sign-close-icon-delete"} />
                                                    <img alt='signature'
                                                        src={trimmedDataURL2} style={{ display: 'flex', justifyContent: 'center'}} />
                                                </div>
                                                : null}
                                        </div>
                                   

                    </div>


                    {/* <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="tabcontent_Sign" style={{ display: "block" }}>
                                <div className={'row'}>
                                    <div className={'parent-canva col-7 d-table'}>
                                        <div style={trimmedDataURL1 ? { height: 380, borderRadius: '5px' } : { height: 'auto', borderRadius: '5px' }} className={'parent-tag-value-name'}>
                                            <div className={'tag-value-Name'}>Name</div>
                                            <SignaturePad
                                                canvasProps={{ className: 'sigCanvas1', }}
                                                ref={(ref) => {
                                                    sigPad1 = ref
                                                }}
                                                onEnd={() => sigpadEnd(1)}
                                            />
                                            {trimmedDataURL1 !== ""
                                                ? <div style={{ display: 'flex', justifyContent: 'center', padding: '2rem' }}>
                                                    <img alt='signature'
                                                        src={trimmedDataURL1} />
                                                    <img alt={'close'} src={close_icon} onClick={() => delete_sigpadEnd(1)} className={"sign-close-icon-delete"} />
                                                </div>
                                                : null}
                                        </div>
                                    </div>
                                    <div className={'parent-canva col-5 d-table'}>
                                        <div style={trimmedDataURL2 ? { height: 380, borderRadius: '5px' } : { height: 'auto', borderRadius: '5px' }} className={'parent-tag-value-name'}>
                                            <div className={'tag-value-Name'}>Initial</div>
                                            <SignaturePad
                                                canvasProps={{ className: 'sigCanvas2' }}
                                                ref={(ref) => {
                                                    sigPad2 = ref
                                                }}
                                                onEnd={() => sigpadEnd(2)}
                                            />
                                            {trimmedDataURL2 !== ""
                                                ? <div style={{ display: 'flex', justifyContent: 'center', padding: '2rem' }}>
                                                    <img alt='signature'
                                                        src={trimmedDataURL2} />
                                                    <img alt={'close'} src={close_icon} onClick={() => delete_sigpadEnd(2)} className={"sign-close-icon-delete"} />
                                                </div>
                                                : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                </div> : null}
            {selection.upload ?
                <div style={{ width: '100%', position: 'relative' }}>
                    <div className="signaturePopup">
                        <div className="upload_content">
                            <h3 style={{ margin: '0px 0px 10px 0px' }} >Upload</h3>
                            {errorStatus ? <p style={{ color: "red" }}>Please upload the signature/Initial.</p> : null}
                            <p style={{ lineHeight: "1.3", fontSize: '18px'}}>Recommended Settings:</p>
                            <p style={{ fontSize: "14px", lineHeight: "1.3" }}>Image dimensions: 670 X 280px</p>
                            <p style={{ fontSize: "14px", lineHeight: "1.3" }}>File size: 100KB (Max).</p>
                            <p style={{ fontSize: "14px", lineHeight: "1.3" }}>File format Types: .jpg, .png,.jpeg.</p>
                            <p style={{ fontSize: "14px", lineHeight: "1.3" }}>Note: Use transparent image for better result.</p></div>
                        <div className='row'>
                            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                {fileerror ? <p className={"error-msg mt-2"}>{fileerror}</p> : null}
                                <div className="tabcontent_Sign" style={{ display: "block" }}>
                                    <div className={'row secondTab-parent-class-sign'}>
                                        <div className="col-6 row_to_colm upload-select-file">
                                            <p>Name</p>
                                            <div className="file_input_wrap parent-tag-value-name upload-align" style={{ borderRadius: '5px' }}>
                                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                                    <input type="file" name="imageUpload" id="imageUpload" accept="image/png, image/jpg, image/jpeg" onClick={(event) => { event.target.value = null }}
                                                        onChange={(e) => readImgUrlAndPreview1(e)} className="hide" />
                                                    <label htmlFor="imageUpload" className="btn btn-large btn-align">
                                                        <img src={upload} className={'sign_upload_icon'} />
                                                        Select file
                                                    </label>

                                                </div>
                                                {(signValue1 !== "" || signValue3 !== null)
                                                    ? <div className={'mt-3'} style={{ display: 'flex', justifyContent: 'center' }}>
                                                        <img className='resizeimgNameUpload choose_initial' alt={`signature `}
                                                            src={TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !== null ? signValue3 : signValue1} />
                                                        <img alt={'close'} src={close_icon} onClick={() => delete_sigpadEnd(TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !== null ? 3 : 1)} className={"sign-close-icon-delete"} />
                                                    </div>
                                                    : null}
                                            </div>
                                        </div>
                                        <div className='col-6 row_to_colm upload-select-file'  >
                                            <p> Initial</p>
                                            <div className="file_input_wrap parent-tag-value-name upload-align" style={{ borderRadius: '5px' }}>
                                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                                    <input type="file" name="imageUpload" id="imageUpload1" accept="image/png, image/jpg, image/jpeg"
                                                        onChange={(e) => readImgUrlAndPreview2(e)} className="hide" />
                                                    <label htmlFor="imageUpload1" className="btn btn-large btn-align">
                                                        <img src={upload} className={'sign_upload_icon'} />
                                                        Select file
                                                    </label>
                                                </div>
                                                {(signValue2 !== "" || signValue4 !== null)
                                                    ? <div className={'mt-3'} style={{ display: 'flex', justifyContent: 'center' }}>
                                                        <img className='resizeimgInitialUpload choose_initial' alt={`signature ${signValue2}`}
                                                            src={TypeCode === "SIGN_UPLOAD_INITIAL" && signValue4 !== null ? signValue4 : signValue2} />
                                                        <img alt={'close'} src={close_icon} onClick={() => delete_sigpadEnd(TypeCode === "SIGN_UPLOAD_INITIAL" && signValue4 !== null ? 4 : 2)} className={"sign-close-icon-delete"} />
                                                    </div>
                                                    : null}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : null}
            {selection.choose ?
                <div className="signaturePopup" style={{ width: "100%" }}>
                    <h3 style={{ margin: '0' }} >Choose</h3>
                    {errorStatus ? <p style={{ color: "red" }}>Please select any font style.</p> : null}
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 choose-align'>
                            <div className="tabcontent_Sign">
                                <div >
                                    <div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" onClick={() => chageCheckFun(1, 'att-ad-brush-script')}>
                                        <input className="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault1" checked={checkbox === 1}
                                        />
                                        <label className="form-check-label att-ad-brush-script radio_box"
                                            htmlFor="flexRadioDefault1"
                                        >
                                            {getUserInfo.FirstName}{" "}{getUserInfo.LastName}
                                        </label>
                                        <label className="form-check-label att-ad-brush-script radio_box"
                                            htmlFor="flexRadioDefault1"
                                        >
                                            {shortName()}
                                        </label>
                                    </div>
                                    <div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" onClick={() => chageCheckFun(2, 'att-ad-forte')} >
                                        <input className="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault6" checked={checkbox === 2}
                                        />
                                        <label className="form-check-label att-ad-forte radio_box" htmlFor="flexRadioDefault6"
                                        >
                                            {getUserInfo.FirstName}{" "}{getUserInfo.LastName}
                                        </label>
                                        <label className="form-check-label att-ad-forte radio_box" htmlFor="flexRadioDefault6"
                                        >
                                            {shortName()}
                                        </label>
                                    </div>
                                    <div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" onClick={() => chageCheckFun(3, 'att-ad-freestyle-script')}>
                                        <input className="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault2" checked={checkbox === 3}
                                        />
                                        <label className="form-check-label att-ad-freestyle-script radio_box"
                                            htmlFor="flexRadioDefault2"
                                        >
                                            {getUserInfo.FirstName}{" "}{getUserInfo.LastName}
                                        </label>
                                        <label className="form-check-label att-ad-freestyle-script radio_box"
                                            htmlFor="flexRadioDefault2"
                                        >
                                            {shortName()}
                                        </label>
                                    </div>
                                    <div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" onClick={() => chageCheckFun(4, 'att-ad-harlow-solid-italic')}>
                                        <input className="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault3" checked={checkbox === 4}
                                        />
                                        <label className="form-check-label att-ad-harlow-solid-italic radio_box"
                                            htmlFor="flexRadioDefault3"
                                        >
                                            {getUserInfo.FirstName}{" "}{getUserInfo.LastName}
                                        </label>
                                        <label className="form-check-label att-ad-harlow-solid-italic radio_box"
                                            htmlFor="flexRadioDefault3"
                                        >
                                            {shortName()}
                                        </label>
                                    </div>
                                    <div className="form-check d-flex  custome-d-flex justify-content-around p-2 mb-1" onClick={() => chageCheckFun(5, 'att-ad-blackadder-itc')} >
                                        <input className="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault4" checked={checkbox === 5}
                                        />
                                        <label className="form-check-label att-ad-blackadder-itc radio_box"
                                            htmlFor="flexRadioDefault4"
                                        >
                                            {getUserInfo.FirstName}{" "}{getUserInfo.LastName}
                                        </label>
                                        <label className="form-check-label att-ad-blackadder-itc radio_box"
                                            htmlFor="flexRadioDefault4"
                                        >
                                            {shortName()}
                                        </label>
                                    </div>
                                    <div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" onClick={() => chageCheckFun(6, 'att-ad-vivaldi')} >
                                        <input className="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault5" checked={checkbox === 6} />
                                        <label className="form-check-label att-ad-vivaldi radio_box" htmlFor="flexRadioDefault5">
                                            {getUserInfo.FirstName}{" "}{getUserInfo.LastName}
                                        </label>
                                        <label className="form-check-label att-ad-vivaldi radio_box" htmlFor="flexRadioDefault5">
                                            {shortName()}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                : null}
            {selection.setting ?
                <div className="popup_setting">
                    <div 
                    // style={{ display: "flex", flexDirection: "column", alignContent: "center" }}
                    >
                        <div className="form-group top_wrrp" style={{ marginBottom: "30px" }}>
                            <label style={{marginBottom: '0.5rem'}} htmlFor="selectPage">Select Pages</label>
                            <div className="input-wrapper" style={{maxWidth: '250px'}}>
                                <select name="selectPage" id="selectPage" value={SelectedPage} onChange={getSelectPage}
                                    className="hlfinput">
                                    <option value={''} >Select</option>
                                    <option value={'FIRST'}>First page</option>
                                    <option value={'LAST'}>Last page</option>
                                    <option value={'ODD'}>Odd pages</option>
                                    <option value={'EVEN'}>Even pages</option>
                                    <option value={'ALL'}>All pages</option>
                                </select>
                            </div>
                        </div>
                        {/* <button className="btn delete_control" style={{background: 'rgb(36, 99, 209)'}}><b>Apply</b></button> */}
                    </div>
                    <div 
                    style={{ display: "flex", flexDirection: "column", alignContent: "center", }}
                    >
                        <div  style={{margin: '3px'}}
                        // style={{ marginBottom: "40px", marginTop: "10px", display: "flex", justifyContent: "center" }}
                        >
                        <input type="checkbox" checked={MandatoryCheck} onChange={getMandatory}></input>
                        <label style={{ marginLeft: "6px" }}> Mandatory</label></div>
                        {/* <button className="btn delete_control" style={{ fontWeight: '300' }} onClick={deleteHandler}><b>Delete Control</b></button> */}
                    </div>
                </div> : null}
        </div>
    )
}
)
export default SignatureControlEditPopup;