import React from "react";
import { useState, useEffect, useRef, forwardRef, useImperativeHandle } from "react";
export default function ControlEditPopup({deleteHandler,selectedcontrolObj,setSelectedControlObj, getSelectedPage, isPageChange}){
  const [SelectedPage, setSelectedPage] = useState('')
    const getMandatory=(event)=> {
        selectedcontrolObj.IsMandatory=event.target.checked;
        setSelectedControlObj({...selectedcontrolObj})
      }
      useEffect(() => {
        selectedcontrolObj.SelectPage ? setSelectedPage(selectedcontrolObj.SelectPage): null;
        isPageChange(false)
    }, [])
      const getSelectPage=(e)=> {
        isPageChange(true)
        setSelectedPage(e.target.value)
        getSelectedPage(e.target.value)
    }
    return(
        <>
          <div className="control_popup_setting" >
             <div
              // style={{display:"flex",flexDirection:"column", alignContent:"center"}}
              >
             <div className="form-group top_wrrp" style={{  marginBottom:"30px"}}>
             <label style={{marginBottom: '0.5rem'}} htmlFor="selectPage">Select Pages</label>
                   <div className="input-wrapper">
                <select name="CountryId" id="CountryId" className="hlfinput" value={SelectedPage} onChange={getSelectPage}>
                  <option value={''} >Select</option>
                  <option value={'FIRST'}>First page</option>
                  <option value={'LAST'}>Last page</option>
                  <option value={'ODD'}>Odd pages</option>
                  <option value={'EVEN'}>Even pages</option>
                  <option value={'ALL'}>All pages</option>
                </select>
              </div>
            </div>
               {/* <button className="btn delete_control" style={{background: 'rgb(36, 99, 209)'}}>Apply</button> */}
             </div>
             <div
              // style={{display:"flex",flexDirection:"column",alignContent:"center",}}
              >
             <div   style={{margin: '3px'}}
            //  style={{  marginBottom:"30px", display: 'flex', justifyContent: 'center'}}
             >
             <input type="checkbox" disabled={selectedcontrolObj?.Code == 'seal'} defaultChecked={selectedcontrolObj?.IsMandatory} onChange={getMandatory}></input>
               <label style={{marginLeft:"6px"}}> Mandatory</label></div>
                 {/* <button className="btn delete_control" onClick={deleteHandler}>Delete Control</button> */}
                  </div>
        </div>
        </>
    )
}