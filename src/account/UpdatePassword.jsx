import React, { useState, useEffect } from 'react';
import { Link,useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { Formik, Field, Form, ErrorMessage, yupToFormErrors } from 'formik';
import * as Yup from 'yup';
import { TextField } from "@material-ui/core";
import Reaptcha from 'reaptcha';
import { accountService, alertService } from '@/_services';
import { result } from 'lodash';
import * as CryptoJS from 'crypto-js';
import Swal from 'sweetalert2';

function UpdatePassword( props){

     
    const usehistory = useHistory();  
    const userId = props?.UserId
    const TokenStatus = {
        Validating: 'Validating',
        Valid: 'Valid',
        Invalid: 'Invalid'
    }
    const [errormessage,setErrormessage]=useState("")
  
    const [recaptchaerrormessage,setrecaptchaerrormessage] = useState("")
    const [token, setToken] = useState(null);
    const [tokenStatus, setTokenStatus] = useState(TokenStatus.Validating);
    const [validCaptcha,setvalidCaptcha]=useState()
   // const [ inputValue, setInputValue ] = useState("Value from onchanges");

    // const [buttonClose,setbuttonClose]=useState(false)
   

    // useEffect(() => {
    //     const { token } = queryString.parse(location.search);

    //     // remove token from url to prevent http referer leakage
    //     history.replace(location.pathname);

    //     accountService.validateResetToken(token)
    //         .then(() => {
    //             setToken(token);
    //             setTokenStatus(TokenStatus.Valid);
    //         })
    //         .catch(() => {
    //             setTokenStatus(TokenStatus.Invalid);
    //         });
    // }, []);
    function cryptedString(string) {
        let ckey = CryptoJS.enc.Utf8.parse('1234567890123456');
        let encrypt = CryptoJS.AES.encrypt(string, ckey, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        })
        return encrypt.ciphertext.toString()
    
    }
   
    function getForm() {
        const initialValues = {
            CurrentPassword:'',
            password: '',
            confirmPassword: '',
            recaptcha:''
        };

        const validationSchema = Yup.object().shape({
            CurrentPassword:Yup.string()
            .required('Current Password is required'),
            password: Yup.string()
            .matches(
                /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{12,}$/,
                'One upper case, One lower case, One number, One symbol, Minimum 12 characters.')
                .required('New Password is required'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                 .required('Confirm New Password is required'),
                // recaptcha: Yup.string().required('recaptcha is required'),
        });
       
        const handleCancel = () => {
            props.close()
        };
        function onSubmit({CurrentPassword, password, confirmPassword }, { setSubmitting }) {
            
            setErrormessage("")
            alertService.clear();
            const passwordconform=cryptedString(confirmPassword)
            const passwordNew=cryptedString(password)
            const passwordcurrent=cryptedString(CurrentPassword)
            let data={"UserId":userId,"Password":passwordconform}
            let data1={"CurrentPassword":passwordcurrent,"NewPassword":passwordNew,"ConfirmPassword":passwordconform}
        
            validCaptcha=== true && accountService.checkpassword(data).then((result)=>{
                 console.log(result,"check")
                 accountService.updatepassword(data1).then((result)=>{
                    console.log(result,"Update")
                    if(result?.Error===""){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Password updated successfully',
                            showConfirmButton: true,
                            confirmButtonText:"OKAY",
                        
                            
                          }).then((result) => {
                            if (result.isConfirmed) {
                                usehistory.push('/account/login');
                            }
                        })
                    }
                    
                    else{
                        setErrormessage("Please check your current password")
                    }
                  })
              })
              
            // accountService.resetPassword({ token, password, confirmPassword })
            //     .then(() => {
            //         alertService.success('Password reset successful, you can now login', { keepAfterRouteChange: true });
            //         // history.push('login');
            //     })
            //     .catch(error => {
            //         setSubmitting(false);
            //         alertService.error(error);
            //     });
        }
      
        function onCaptchaVerify(recaptchaResponse) {
            //validCaptcha = true
            setvalidCaptcha(true)
           
        };
        function onCaptchaExpire(recaptchaResponse) {
            //validCaptcha = false
            setvalidCaptcha(false)
        }  
        return (
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit} >
                {({ errors, touched, isSubmitting }) => (
                    <Form>
                          <p style={{ color: "red", textAlign: "left" }}>{errormessage}</p>
                        <div className="form-group my-2">
                            {/* <label>Current Password</label> */}
                            <Field placeholder="Current Password" name="CurrentPassword" type="password" className={'form-control' +  (errormessage!==""||errors.CurrentPassword && touched.CurrentPassword ? ' is-invalid' : '')} />
                            <ErrorMessage name="CurrentPassword" component="div" className="invalid-feedback" />
                            {/* <ErrorMessage component="div" className="invalid-feedback">{errormessage}</ErrorMessage> */}
                        </div>
                            
                        <div className="form-group my-2">
                            {/* <label>Password</label> */}
                            <Field placeholder="New Password" name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                            <ErrorMessage name="password" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group my-2">
                            {/* <label>Confirm Password</label> */}
                            <Field placeholder="Confirm New Password" name="confirmPassword" type="password" className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
                            <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
                        </div>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group mt-4">
                                            <div className="captcha">
                                            <Reaptcha id="recaptcha" name="recaptcha"
                                                    sitekey="6LcljLYUAAAAANiWkP0pM3hMy-wakNZ0T0pYakKO"
                                                    onVerify={onCaptchaVerify}
                                                    onExpire={onCaptchaExpire} />
                                         {validCaptcha ===false  && <span className='error-message'>Captcha is required</span>}
                                                {/* {state && state.errors && state.errors.recaptcha ?
                                             <span className='error-message'>{state.errors.recaptcha}</span>
                                              : null} */}
                                        </div>
                                    </div>
                                </div>
                        <div className="form-row mt-4">
                            <div className="form-group col flex">
                            <button type="submit" className="btn btn-secondary " onClick={handleCancel}>CANCEL</button>  
                                <button type="submit"  className="btn btn-primary" onClick={()=>{validCaptcha !== true?setvalidCaptcha(false):""}}>
                                    {/* {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>} */}
                                    UPDATE
                                </button>
                               </div>
                        </div>
                        
                    </Form>
                )}
            </Formik>
        );
    }

    function getBody() {
         return getForm();
        // switch (tokenStatus) {
        //     case TokenStatus.Valid:
        //         return getForm();
        //     case TokenStatus.Invalid:
        //         return <div>Token validation failed, if the token has expired you can get a new one at the <Link to="forgot-password">forgot password</Link> page.</div>;
        //     case TokenStatus.Validating:
        //         return <div>Validating token...</div>;
        // }
    }

    return (
        <div>
            <h3 className="card-header">Update Password</h3>
            <div className="card-body">{getBody()}</div>
        </div>
    )
}

export { UpdatePassword }; 