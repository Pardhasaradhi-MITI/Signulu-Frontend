import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {Formik, Field, Form, ErrorMessage} from 'formik';
import * as Yup from 'yup';

import {accountService, alertService} from '@/_services';
import {Sidebar} from './Common/Sidebar';
import {Topbar} from './Common/Topbar';
import {billingService} from "../_services/billing.service";
import config from 'config';
import {usersService} from "../_services/users.service";
import {contactsService} from "@/_services/contacts.service";
import Pagination from "react-responsive-pagination";
import "../css/BillingandUsage.css"
const API_END_POINT = config.serverUrl;

function BillingandUsage({history, location}) {
    const [billingList, setBillingList] = useState([]);
    const [activePage, setActivePage] = useState('list');
    const [selectedBill, setSelectedBill] = useState({});
    const [billingHistoryList, setBillingHistoryList] = useState([]);
    const [companyStatistics, setCompanyStatistics] = useState({});
    const [searchText, setSearchText] = useState("");
    const [pageCount, setPageCount] = useState(0);
    const [pageNumber, setPageNumber] = useState(1);
    const [pageSize, setPageSize] = useState(5);

   
    useEffect(() => {

        getBillingList();
    }, [pageNumber]);
    useEffect(() => {

        getBillingHistoryList();
        getUser()
    }, []);

    const getUser = () => {
       //    
       usersService.getCurrentUserData()
			.then((result) => {
                if(result){
                    // setCompanyID(result.Data.company_id)
                    getBillingstatistics()
                  //  console.log(result.Data.company_id)
                }
            })
            .catch(error => {

            });

    }




    const getBillingList = () => {
        let input = {
            "PageNumber": pageNumber,
            "PageSize": pageSize,
        }
        billingService.billingList(input)
            .then((result) => {
                setBillingList(result.data);
                setPageCount(result.PageCount);
                setPageNumber(result.PageNumber);
                setPageSize(result.PageSize);
            })
            .catch(error => {

            });
    }

    const getBillingstatistics = () => {
        //    
        billingService.getBillStatistics()
            .then((result) => {
             //   console.log(result,"billing and usage")
                //    
                setCompanyStatistics(result.Entity);
                //console.log(companyStatistics.TotalActiveUsers)
                //console.log("Hareesh2020",result.Entity)
            })
            .catch(error => {
            });
    }

    const getBillingHistoryList = () => {
        billingService.billingHistory()
            .then((result) => {
                setBillingHistoryList(result);
            })
            .catch(error => {

            });
    }

    const getUserData = () => {
        
        const userData = localStorage.getItem('UserObj');
        if (userData) {
            return JSON.parse(userData);
        }
        return {};
    }

    const downloadInvoice = (invoiceId) => {
        let userData = getUserData();
        window.open(API_END_POINT + 'billing/' + invoiceId + '/pdf/' + userData.user_id, '_blank').focus();
    }

    const selectBill = (billData) => {
        setActivePage('view');
        billingService.billById(billData.BillId)
            .then((result) => {

                setSelectedBill(result);
                setSelectedBill(result);

            })
            .catch(error => {

            });
    }

    const closeEditSection = () => {
        setActivePage('list');
        setSelectedBill({});
    }
    const funbillamount=e=>{

        let x=e.toString();
        let lastThree = x.substring(x.length-3);
        let otherNumbers = x.substring(0,x.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree;
        let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
        return res
    }

    const handlePagination = (newPage) => {
        setPageNumber(newPage);
    }
    const currency_symbols = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'CRC': '₡', // Costa Rican Colón
        'GBP': '£', // British Pound Sterling
        'ILS': '₪', // Israeli New Sheqel
        'INR': '₹', // Indian Rupee
        'JPY': '¥', // Japanese Yen
        'KRW': '₩', // South Korean Won
        'NGN': '₦', // Nigerian Naira
        'PHP': '₱', // Philippine Peso
        'PLN': 'zł', // Polish Zloty
        'PYG': '₲', // Paraguayan Guarani
        'THB': '฿', // Thai Baht
        'UAH': '₴', // Ukrainian Hryvnia
        'VND': '₫', // Vietnamese Dong
    };

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar/>

            <div className="main other_main" id="main">
                <Topbar/>

                <div className="main-content main-from-content billingandusage">
                    {
                        activePage === 'list' &&
                        <>
                            <div className="row">
                                <div className="col-xl-6  col-lg-6 col-md-6 col-sm-12">
                                    {/* <h3>Billing And Usage Statistics</h3> */}
                                    <h3 style={{fontSize:35,fontWeight:300}}>Billing And Usage Statistics</h3>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <div className="recipient-btn">
                                    <button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}}   /></button>
                                    </div>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                    <div className='dashboard_item'>
                                        <div className="item">
                                            <div style={{cursor: "default",userSelect:"none"}}className='content'><img src="src/images/document-9_icon.svg"
                                                                          alt=""/><h5>{companyStatistics.TotalUsedDocument} <span>/{companyStatistics.TotalDocuments}</span></h5>
                                                <p>Current Consumption Level</p></div>
                                        </div>
                                        <div className="item">
                                            <div style={{cursor: "default",userSelect:"none"}} className='content'><img src="src/images/document-10_icon.svg"
                                                                          alt=""/><h5>{companyStatistics.TotalAmountConsumed}</h5><p>Consumption in
                                                Dollar</p></div>
                                        </div>
                                        <div className="item">
                                            <div style={{cursor: "default",userSelect:"none"}}className='content'><img src="src/images/document-11_icon.svg"
                                                                          alt=""/><h5>{ companyStatistics.TotalActiveUsers+companyStatistics.TotalInActiveUsers} <span>No of Users</span>
                                            </h5></div>
                                            <ul>
                                                <li>
                                                    <h6><span className="circle active"></span>{companyStatistics.TotalActiveUsers}</h6>
                                                    <span>Active Users</span>
                                                </li>
                                                <li>
                                                    <h6><span className="circle inactive"></span>{companyStatistics.TotalInActiveUsers}</h6>
                                                    <span>Inactive Users</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div style={{cursor:"default",userSelect:"none"}} className="item">
                                            <div className='content'><img src="src/images/document-12_icon.svg"
                                                                          alt=""/><h5>{companyStatistics.TotalActiveUsers}</h5><p>Licenses Consumed
                                                </p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xl-6  d-flex align-items-center col-lg-6 col-md-6 col-sm-12">
                                    {/* <h3>Billing And Usage</h3> */}
                                    <h3 style={{fontSize:"25px" ,fontWeight:300}}>Billing And Usage</h3>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <div className="pay-btn ml-auto max-w-450 " style={{maxWidth:435}}>
                                        <p>Account will get expired on <strong>28 Feb 22</strong></p>
                                        <button className="btn pay_btn">PAY NOW</button>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    {
                                        billingList && billingList.map((item, index) => {
                                            return (
                                                <div className='document_block' key={index}>
                                                    <ul className='content_area'>
                                                        <li><h6>Invoice Number</h6><h5>{item.ReferenceNum}</h5></li>
                                                        <li><h6>Duration</h6>
                                                            <h5>{item.BillFrom} - {item.BillTo}</h5></li>
                                                        <li><h6>Invoice Amount</h6>
                                                            <h5>  {currency_symbols[item.CurrencyCode]} {funbillamount(item.BillAmount)}</h5></li>
                                                        <li><h6>Status</h6>
                                                            <h5>{item.IsPaid === 1 ? "Paid" : "Not Paid"}</h5></li>
                                                    </ul>
                                                    <ul className='action_area'>
                                                        <li title={'View bill'}>
                                                            <a href='javascript:void(0)'
                                                               onClick={() => selectBill(item)}>
                                                                <img src='src/images/eye.svg' alt=''/>
                                                            </a>
                                                        </li>
                                                        <li title={'Download bill'}>
                                                            <a href='javascript:void(0)'
                                                               onClick={() => downloadInvoice(item.BillId)}>
                                                                <img src='src/images/download.svg' alt=''/>
                                                            </a>
                                                        </li>
                                                        {/*<li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>*/}
                                                    </ul>
                                                </div>

                                            )
                                        })
                                    }
                                    {
                                        billingList.length === 0 &&
                                        <div className={"text-center mt-4"}> No records are found.</div>
                                    }
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="document-nagin">
                                        <nav aria-label="Page navigation">
                                            <Pagination
                                                current={pageNumber}
                                                total={pageCount}
                                                onPageChange={(newPage) => handlePagination(newPage)}
                                            />
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            {/*<div className="row">*/}
                            {/*    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">*/}
                            {/*        <div className="document-nagin">*/}
                            {/*            <nav aria-label="Page navigation">*/}
                            {/*                <ul className="pagination">*/}
                            {/*                    <li className="page-item"><a className="page-link active" href="#">1</a></li>*/}
                            {/*                    <li className="page-item"><a className="page-link" href="#">2</a></li>*/}
                            {/*                    <li className="page-item"><a className="page-link" href="#">3</a></li>*/}
                            {/*                    <li className="page-item"><a className="page-link" href="#">4</a></li>*/}
                            {/*                    <li className="page-item">*/}
                            {/*                        <a className="page-link" href="#" aria-label="Next">*/}
                            {/*                            <span aria-hidden="true">•••</span>*/}
                            {/*                        </a>*/}
                            {/*                    </li>*/}
                            {/*                </ul>*/}
                            {/*            </nav>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className='row'>*/}
                            {/*    <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">*/}
                            {/*        <h3>Billing History</h3>*/}
                            {/*    </div>*/}
                            {/*    <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>*/}
                            {/*        <div className='action_area'>*/}
                            {/*            <ul>*/}
                            {/*                <li><img src='src/images/xls.svg' alt=''/></li>*/}
                            {/*                <li><img src='src/images/pdf.svg' alt=''/></li>*/}
                            {/*                <li><img src='src/images/csv.svg' alt=''/></li>*/}
                            {/*            </ul>*/}
                            {/*            <select className='hlfinput'>*/}
                            {/*                <option>December</option>*/}
                            {/*            </select>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className='row'>*/}
                            {/*    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>*/}
                            {/*        <div className="table-pay-select">*/}
                            {/*            <form action="#">*/}
                            {/*                <div className="date_fil">*/}
                            {/*                    <div className='from-group'>*/}
                            {/*                        <input type="text" placeholder='18-01-22'/>*/}
                            {/*                        <span className='icon'><img src='src/images/date.svg'*/}
                            {/*                                                    alt=''/></span>*/}
                            {/*                    </div>*/}
                            {/*                    <span>to</span>*/}
                            {/*                    <div className='from-group'>*/}
                            {/*                        <input type="text" placeholder='18-01-22'/>*/}
                            {/*                        <span className='icon'><img src='src/images/date.svg'*/}
                            {/*                                                    alt=''/></span>*/}
                            {/*                    </div>*/}
                            {/*                </div>*/}
                            {/*                <div className="text-fil">*/}
                            {/*                    <input type="text" name id placeholder="Invoice Number"/>*/}
                            {/*                    <select className="hlfinput">*/}
                            {/*                        <option selected>More Filters</option>*/}
                            {/*                        <option value={1}>One</option>*/}
                            {/*                        <option value={2}>Two</option>*/}
                            {/*                        <option value={3}>Three</option>*/}
                            {/*                    </select>*/}
                            {/*                    <div className="search">*/}
                            {/*                        <input type="text" placeholder="Search"/>*/}
                            {/*                        <button type="submit" className="btn"/>*/}
                            {/*                    </div>*/}
                            {/*                    <button className="more-btn"><span>...</span></button>*/}
                            {/*                </div>*/}
                            {/*            </form>*/}
                            {/*        </div>*/}
                            {/*        <div className="table-pay table-responsive">*/}
                            {/*            <table className="table">*/}
                            {/*                <thead>*/}
                            {/*                <tr>*/}
                            {/*                    <th>*/}
                            {/*                        <div className="head-ttl">Date</div>*/}
                            {/*                    </th>*/}
                            {/*                    <th>*/}
                            {/*                        <div className="head-ttl">Invoice Number</div>*/}
                            {/*                    </th>*/}
                            {/*                    <th>*/}
                            {/*                        <div className="head-ttl">Payment Method</div>*/}
                            {/*                    </th>*/}
                            {/*                    <th>*/}
                            {/*                        <div className="head-ttl">Status</div>*/}
                            {/*                    </th>*/}
                            {/*                    <th>*/}
                            {/*                        <div className="head-ttl">Amount</div>*/}
                            {/*                    </th>*/}
                            {/*                </tr>*/}
                            {/*                </thead>*/}
                            {/*                <tbody>*/}
                            {/*                {*/}
                            {/*                    billingHistoryList.map((item,index)=>{*/}
                            {/*                        return(*/}
                            {/*                            <tr key={index}>*/}
                            {/*                                <td>*/}
                            {/*                                    <div className="text">*/}
                            {/*                                        <p>{item.PaymentDate}</p>*/}
                            {/*                                    </div>*/}
                            {/*                                </td>*/}
                            {/*                                <td>*/}
                            {/*                                    <div className="text">*/}
                            {/*                                        <p>{item.ReferenceNum}</p>*/}
                            {/*                                    </div>*/}
                            {/*                                </td>*/}
                            {/*                                <td>*/}
                            {/*                                    <div className="text hizl-item">*/}
                            {/*                                        <img src="src/images/visa_table.svg" alt="img"/>*/}
                            {/*                                        <p>******6605</p>*/}
                            {/*                                    </div>*/}
                            {/*                                </td>*/}
                            {/*                                <td>*/}
                            {/*                                    <div className="text">*/}
                            {/*                                        {*/}
                            {/*                                            item.PaymentStatus === "Paid"?*/}
                            {/*                                                <button className="btn btn-paid">Paid</button>*/}
                            {/*                                                :*/}
                            {/*                                                <button className="btn btn-due">Due</button>*/}
                            {/*                                        }*/}
                            {/*                                    </div>*/}
                            {/*                                </td>*/}
                            {/*                                <td>*/}
                            {/*                                    <div className="text hizl-item">*/}
                            {/*                                        <p>₹{item.BillAmount}</p>*/}

                            {/*                                        <a href='javascript:void(0)'*/}
                            {/*                                           onClick={() => downloadInvoice(item.BillId)}>*/}
                            {/*                                            <img src='src/images/download.svg' alt=''/>*/}
                            {/*                                        </a>                                                                </div>*/}
                            {/*                                </td>*/}
                            {/*                            </tr>*/}
                            {/*                        );*/}
                            {/*                    })*/}
                            {/*                }*/}
                            {/*                /!*<tr>*!/*/}
                            {/*                /!*    <td>*!/*/}
                            {/*                /!*        <div className="text">*!/*/}
                            {/*                /!*            <p>28th Jan 22</p>*!/*/}
                            {/*                /!*        </div>*!/*/}
                            {/*                /!*    </td>*!/*/}
                            {/*                /!*    <td>*!/*/}
                            {/*                /!*        <div className="text">*!/*/}
                            {/*                /!*            <p>C9E6C569913</p>*!/*/}
                            {/*                /!*        </div>*!/*/}
                            {/*                /!*    </td>*!/*/}
                            {/*                /!*    <td>*!/*/}
                            {/*                /!*        <div className="text hizl-item">*!/*/}
                            {/*                /!*            <img src="src/images/visa_table.svg" alt="img"/>*!/*/}
                            {/*                /!*            <p>******6605</p>*!/*/}
                            {/*                /!*        </div>*!/*/}
                            {/*                /!*    </td>*!/*/}
                            {/*                /!*    <td>*!/*/}
                            {/*                /!*        <div className="text">*!/*/}
                            {/*                /!*            <button className="btn btn-paid">Paid</button>*!/*/}
                            {/*                /!*        </div>*!/*/}
                            {/*                /!*    </td>*!/*/}
                            {/*                /!*    <td>*!/*/}
                            {/*                /!*        <div className="text hizl-item">*!/*/}
                            {/*                /!*            <p>$250</p>*!/*/}
                            {/*                /!*            <img src="src/images/download.svg" alt="img"/>*!/*/}
                            {/*                /!*        </div>*!/*/}
                            {/*                /!*    </td>*!/*/}
                            {/*                /!*</tr>*!/*/}
                            {/*                </tbody>*/}
                            {/*            </table>*/}
                            {/*        </div>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </>
                    }
                    {
                        activePage === 'view' &&
                        <>
                            <div className={"bill-detail-container"}>
                                <div className={"container"}>
                                    <div className="card">
                                        <div className="card-body p-0">
                                            <div className={"head-section"}>
                                                <p>Billing Details</p>
                                            </div>
                                            <div className={"content-section"}>
                                                <div className={"invoice-card"}>
                                                    <div className={"row"}>
                                                        <div className={"col-sm-6"}>
                                                            <p className={"mb-3"}>Invoice Number: {selectedBill.ReferenceNum}</p>
                                                            <p className={"mb-3"}>Invoice Amount: {currency_symbols[selectedBill.CurrencyCode] }  {selectedBill.BillAmount && funbillamount(selectedBill.BillAmount)}</p>
                                                        </div>
                                                        <div className={"col-sm-6"}>
                                                            <p className={"mb-3"}>Due Date: {selectedBill.BillDueOn}</p>
                                                        </div>
                                                    </div>
                                                    <table >
                                                        <thead >
                                                            <tr>
                                                                <td scope="col">Description</td>
                                                                <td scope="col">Quantity</td>
                                                                <td scope="col">Rate</td>
                                                                <td scope="col">Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            selectedBill.BillLineItem && selectedBill.BillLineItem.map((item,index)=>{
                                                                return(
                                                                    <tr>
                                                                       <td >{item.Description}</td>
                                                                        <td  >{item.Quantity}</td>
                                                                        <td  > {currency_symbols[selectedBill.CurrencyCode] } {funbillamount(item.Rate)}</td>
                                                                        <td  >{currency_symbols[selectedBill.CurrencyCode] } {funbillamount(item.Amount)}</td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div className={"button-section text-center pb-3 "}>
                                            <button type="submit" className="btn btn-primary"
                                                        onClick={closeEditSection}
                                                >
                                                    CLOSE
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    }

                </div>
            </div>
        </div>
    )
}

export {BillingandUsage};
