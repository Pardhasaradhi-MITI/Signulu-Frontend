
import React, { Component } from 'react';
import { loginService } from '../_services/login.service';
import { commonService } from '../_services/common.service';
import moment from 'moment';
import { Avatar } from "@mui/material";
import { accountService, alertService } from '@/_services';
//import "../css/thanks.css";
import "../css/activatemail.css";
import { Button, ButtonToolbar, Modal } from 'react-bootstrap';

class ActivateMail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalShow: false,
            userObj: { email_address: "", account: "", lastloggedin: "", userDaysLeft: null },

        }
        this.userObj = localStorage.getItem('UserObj') ? JSON.parse(localStorage.getItem('UserObj')) : {}
        console.log(this.userObj, 'state')
        this.openActivateEmailPopup = this.openActivateEmailPopup.bind(this);
        this.stringAvatar = this.stringAvatar.bind(this)
        // this.logout = this.logout.bind(this)
    }

    componentDidMount() {

        commonService.activatemailuserInfo().then((userData) => {
            if ("undefined" !== typeof userData.Data.LastLoginAttempt && moment(userData.Data.LastLoginAttempt).isValid() && userData.Data.LastLoginAttempt != null && userData.Data.LastLoginAttempt != '') {
                var gmtDateTime = moment.utc(userData.Data.LastLoginAttempt);
                var local = gmtDateTime.local().format('MM/DD/YYYY hh:mm A');
                userData.Data.lastTimeLogin = local
                //var local = gmtDateTime.local().format('DD/MM/YYYY hh:mm A');
                //   this.setState(userObj => {
                //     userObj.lastloggedin = local,
                //     userObj.email_address = userData?.Data?.EmailAddress,
                //     userObj.account = userData?.Data?.AccountId


                //   })
                this.setState(prevState => ({
                    userObj: {
                        ...prevState.userObj,
                        email_address: userData?.Data?.EmailAddress,
                        account: userData?.Data?.AccountId,
                        lastloggedin: local
                    }
                }))
                console.log(this.state.userObj, "2021")
            }



            //setUserObj(userData.Data)
        });
        commonService.activatemailuserTrailDays().then((userData) => {
            this.setState(prevState => ({
                userObj: {
                    ...prevState.userObj,
                    userDaysLeft: userData?.Entity?.Days,

                }
            }))
            //  setUserDaysLeft(days)
            // this.setState({ userDaysLeft: this.state.userDaysLeft })
        });
        // loginService.getMessage().subscribe(message => {
        //     this.userObj = message

        // })
        //     let data = new FormData();
        // data.append('IsReact', 1);
        //     loginService.resendVerficationEmail(data)
        //         .then((result) => {
        //         })
        //     console.log(this.userObj)
    }

    //  userInfo =()=> {

    //     commonService.userInfo().then((userData) => {
    //         if("undefined" !== typeof userData.Data.LastLoginAttempt  && moment(userData.Data.LastLoginAttempt).isValid() && userData.Data.LastLoginAttempt != null && userData.Data.LastLoginAttempt != '') {
    //             var gmtDateTime = moment.utc(userData.Data.LastLoginAttempt);
    //           var local = gmtDateTime.local().format('MM/DD/YYYY hh:mm A');
    //           userData.Data.lastTimeLogin = local
    //           //var local = gmtDateTime.local().format('DD/MM/YYYY hh:mm A');
    //         }

    //         this.setState({ userObj: this.state.userObj })
    //     });
    // }
    //  userTrailDays() {
    //     commonService.userTrailDays().then((days) => {
    //       //  setUserDaysLeft(days)
    //         this.setState({ userDaysLeft: this.state.userDaysLeft })
    //       });
    // }

    // logout() {
    //     console.log(this.userObj, 'this.userObj')
    //     loginService.logout(this.userObj)
    //         .then((result) => {
    //             localStorage.clear()
    //             this.back();
    //         }, error => {

    //         });
    // }

    // back = () => {
    //     const { history } = this.props;
    //     console.log(history, 'historyhistoryhistory')
    //     history.push("/login");
    // };

    sendActivateEmail = (e) => {
        e.preventDefault();
        let data = new FormData();
        data.append('IsReact', 1);
        loginService.resendVerficationEmail(data)
            .then((result) => {
                this.openActivateEmailPopup()
            }, error => {
            });
    }

    openActivateEmailPopup() {
        this.setState({ modalShow: !this.state.modalShow })
        var srcElement = document.getElementById('activateMail');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    ActivateEmailPopClose() {
        var srcElement = document.getElementById('activateMail');
        srcElement.style.display = "none";

    }

    signOut() {
        const { history } = this.props;
        loginService.logout(this.state.userObj.UserId, userObj["FirstName"], userObj["LastName"],)
            .then((result) => {
                console.log(result, 'resultresultresult')
                history.push("/account/login");
            });
    }

    // userDropdown() {
    //     var srcElement = document.getElementById('dropdown_menu');
    //     if (srcElement != null) {
    //         if (srcElement.style.display == "block") {
    //             srcElement.style.display = "none";
    //         } else {
    //             srcElement.style.display = "block";
    //         }
    //         return false;
    //     }
    // }
    stringAvatar() {
        let name = this.userObj.uname
        // let name = firstName ?fullname : 'N A'
        return {
            sx: {
                bgcolor: "#2F7FED",
                width: '47px',
                height: '47px'
            },
            children: (name.split(' ') && name.split(' ')[0] && name.split(' ')[1]) ? `${name.split(' ')[0][0]}${name.split(' ')[1][0]}` : 'U',
        };
    }

    render() {
        console.log(this.state, "@@@@@")
        return (
            <>
                <div style={{ overflowX: "hidden", overflowY: "hidden" }}>
                    <header className='main-header' >
                        <div className='row he'>
                            <div className="col-md-5" style={{ padding: "10px" }} >
                                <img src="src/images/signulu_black_logo2.svg" className='img_fluid1' alt="" />
                                {/* <h1>Signulu</h1> */}

                            </div>
                            <div className="col-md-4" style={{ color: "#FFFFFF", fontWeight: 300, fontSize: 17, paddingLeft: 137, paddingRight: 19, paddingTop: "23px" }}>
                                <a  >
                                    Last logged in: {moment.utc(this.state.userObj?.lastloggedin !== null ? this.state.userObj?.LastLoginAttempt : "-").local().format('MM/DD/YYYY h:mm A')}


                                    {/* Last logged in: {moment.utc(this.state.userObj?.lastloggedin).local().format('MM/DD/YYYY h:mm A')} */}
                                </a>
                            </div>
                            <div className="col-md-1 " >

                                <div className="users" >
                                    <div className="nav-item dropdown" >
                                        <a href="#" style={{ padding: "0.5rem 5rem", marginTop: -4 }} className="dropdown-toggle nav-link user" type="button" id="dropdownMenuButton1"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            {/* <img src="src/images/user.png" alt="" className="user-img" /> */}
                                            <div style={{ marginTop: 7, marginBottom: -20 }}><Avatar {...this.stringAvatar()} /></div>
                                            <span className="active"></span>
                                        </a>

                                        <div className="dropdown-menu" style={{ minWidth: "14rem", marginLeft: -30 }} aria-labelledby="dropdownMenuButton1">
                                            <ul style={{ marginBottom: "0rem" }} >
                                                <li>

                                                    <a ><span>{this.state.userObj?.email_address}</span></a>

                                                </li>
                                                <li>
                                                    <a >Account: <span> #{this.state.userObj?.account}</span></a>
                                                </li>
                                                <li>
                                                    <a href="#" onClick={this.signOut}>Log Out</a>
                                                </li>
                                            </ul>                                        </div>



                                    </div>

                                </div>

                            </div>
                            <div className="col-md-2" style={{ color: "#1A2434", fontWeight: 300, fontSize: 17, paddingLeft: 18, paddingRight: 19, paddingTop: "23px" }}>
                                <a style={{ marginLeft: 44, color: "white" }}>{this.state.userObj?.userDaysLeft} Days Left</a>
                            </div>
                        </div>
                    </header>
                    <div className="card carditem"  >
                        <div className='header-item' >
                            <h4 className='headding'>ACCOUNT VERIFICATION</h4>
                        </div>
                        <div style={{ marginLeft: "10px" }}>
                            <div className='userName'>
                                <p>Hi {this.userObj.uname},</p>
                            </div>
                            <div className='description'>
                                <p>Welcome to the Signulu!</p>
                            </div>
                            <div className='description'>
                                <p>Your registered email address is not yet verified. We sent you an email with a link to verify your email
                                    address. Please check your email account</p> <p style={{ marginTop: 10 }}>and click on the link to verify the same.</p>
                            </div>
                            <div className='description'>
                                <p>If you have not recieved the email please <a onClick={this.sendActivateEmail} href="#"  >click here </a> to resend the
                                    email.</p>
                            </div>
                        </div>
                    </div>
                    <MyVerticallyCenteredModal
                        user={this.userObj}
                        show={this.state.modalShow}
                        onHide={() => this.setState({ modalShow: !this.state.modalShow })}
                    />
                    {/* 
                    <div id="activateMail" className="modal">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h2>Success</h2>
                                <span className="close" onClick={this.ActivateEmailPopClose}>&times;</span>
                            </div>
                            <div className="modal-body">
                                <p>
                                    We have successfully re-sent the Email verification mail to {this.userObj.email_address}</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button"
                                    className=" signup-btn mx-auto my-3" onClick={this.ActivateEmailPopClose}>Ok</button>
                            </div>
                        </div>

                    </div> */}
                </div>

            </>
        )
    }
}
export { ActivateMail };
function MyVerticallyCenteredModal(props) {


    return (

        <Modal

            {...props}
            size="lg"
            // width="65%"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            dialogClassName="activation-dialog-classname"
            ClassName="activemail-modal"
        >
            <Modal.Header className="activemail-modal-header" closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Success
                </Modal.Title>
            </Modal.Header>
            <Modal.Body >
                <p>
                    We have successfully re-sent the Email verification mail to  </p>
                <p style={{ marginTop: 10 }}> {props?.user?.email_address}</p>

            </Modal.Body>
            <Modal.Footer className="activemail-modal-footer justify-content-center" >
                <Button data-dismiss="modal " onClick={props.onHide}>OKAY</Button>
            </Modal.Footer>
        </Modal>
    );
}