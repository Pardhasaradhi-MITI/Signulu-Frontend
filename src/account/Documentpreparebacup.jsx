import React from 'react';
import {useRef} from 'react';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { addDocStep3Options, generateUQID } from '../_services/model.service';
import { addDocumentService } from '../_services/adddocument.service';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useEffect, useState } from "react";
import { commonService } from '../_services/common.service';
import { Document, Page, pdfjs } from "react-pdf";
import config from 'config';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
import { Button, Modal, ProgressBar } from 'react-bootstrap';
import '../css/controls.css'
import Signature from './Controls/Signature';
import Initial from './Controls/Initial';
import DigitalSignature from './Controls/DigitalSignature';
import Attachment from './Controls/Attachment';
import CompanyAddress from './Controls/CompanyAddress';
import UserAddress from './Controls/UserAddress';
import Aadhaar from './Controls/Aadhaar';
import ImageSeal from './Controls/ImageSeal';
import DateSigned from './Controls/DateSigned';
import Email from './Controls/Email';
import Name from './Controls/Name';
import Company from './Controls/Company';
import Payment from "./Controls/Payment";
import Checkbox from './Controls/CheckBox';
import NotarySeal from './Controls/NotarySeal';
import TextBox from './Controls/TextBox';
import Dob from './Controls/Dob'
import Spinner from 'react-bootstrap/Spinner';
import SignatureControlEditPopup from './ControlEditPopups/SignaturePopup';
import ControlEditPopup from "./ControlEditPopups/ControlEditPopup"
let contronotExistRespArray = []
let docsData = {}
import { alertService } from '@/_services';
import ReactTooltip from 'react-tooltip';
import UploadIcon from '@mui/icons-material/Upload';
import SettingsIcon from '@mui/icons-material/Settings';
let isControlImage = false;
let pagesArray = []
let selectControlOBJ = {}
let nonPermitPagesArray=[]
function DocumentPrepare() {
    //Vertical Tabs
    const usehistory = useHistory();
    const [count, setCount] = React.useState(2);
    const [step3Data, setStep3Data] = useState({});
    const [subNumPages, setSubNumPages] = useState();
    const [documentFile, setdocument] = useState('');
    const [controlTypes, setControlTypes] = useState([]);
    const [DraggableControls, setDraggableControls] = useState([]);
    const [finalControls, setFinalControls] = useState({});
    const [selectedRecipient, setSelectedRecipient] = useState();
    const [signaturePopShow, setSignaturePopShow] = useState(false);
    const [controlPopupShow,setControlPopupShow]=useState(false);
    const [selectedSignatureControlEditData, setSelectedSignatureControlEditData] = useState({});
    const [dropped, setDropped] = useState([]);
    const [mandatoryNotExist, setMandatoryNotExist] = useState(false);
    let { id } = useParams();
    const [selectedControl, setSelectedControl] = useState({});
    const [selection,setSelection]=useState({draw:true,choose:false,upload:false,setting:false})
    const [errorStatus,setErrorStatus]=useState(false);
    const [TotalPagesHeight,setTotalPagesHeight]=useState(0);
    const childCompRef = useRef()
    const [uploadPopShow, setUploadPopShow] = useState(false);
    const [IsFileUploaded, setIsFileUploaded] = useState(false);
    const [className, setclassName] = useState('');
    const [failedMessage, setfailedMessage] = useState(false);
    const [attachImageFile, setAttachImageFile] = useState(false);
    const [controlPopDocPages, setControlPopDocPages] = useState();
    const [fileName, setFileName] = useState();
    const [isImageFile, setIsImageFile] = useState(true);
    const [attachmentSelection, setAttachmentSelection] = useState({upload:true, manage:false});
    const [SelectedPage, setSelectedPage] = useState(0);
    const [MandatoryCheck, setMandatoryCheck] = useState(false);
    const [acceptedFiles, setAcceptedFiles] = useState(false);
    const pageRef = useRef(null);
    const [isSelectChanged, setIsSelectChanged] = useState(false)
    const [PopUpSelectedPage, setPopUpSelectedPage] = useState()
    const [showControlAddedbySelectPage, setshowControlAddedbySelectPage] = useState(false)
    const [IsanyoneenableDsc,setIsanyoneenableDsc] = useState(false)
    const [Selectpagelist,setSelectpagelist]=useState([{}])
    const [dscsignPositionlist,setdscsignPositionlist]=useState([{}])
    const [Dscselectedpage,setDscselectedpage]=useState({})
    const [DscselectedSignatureposition,setDscselectedSignatureposition]=useState({})
    const [Dsccontroldetails,setDsccontroldetails]=useState({})
    const [DscLevelofAccess,setDscLevelofAccess]=useState()
    const [showDscLevelofAccess,setshowDscLevelofAccess]=useState(false)
    const [controldraginfo,setcontroldraginfo] = useState()
    
    console.log(selectedRecipient,"selectedRecipient")
    function getDocumentInfoData() {
        addDocumentService.getDocumentInfo(id).then(data => {
            pagesArray = []
            if (data.Entity && data.Entity.Data) {
                setStep3Data(data.Entity.Data)
                setSelectedRecipient(data.Entity.Data.Recipients[0])
                setdocument(config.serverUrl + 'adocuments/download/' + data.Entity.Data.FileName)
                let dscenabled=''
                dscenabled = data.Entity.Data.Recipients.find(item => item.IsDSC == 1)
                if(dscenabled){
                    setIsanyoneenableDsc(true)
                 }
                if (data.Entity.Data.Controls) {
                    var controls = Object.keys(data.Entity.Data.Controls);
                    controls.forEach((ctrlId) => {
                        let FinalObj= {}
                        const obj = data.Entity.Data.Controls[ctrlId];
                        obj.Top = obj.TopByPage
                        obj.Left = obj.LeftByPage
                        obj.CtrlId = ctrlId;
                       const RespData = data.Entity.Data.Recipients.find(data=>data.RecipientId == obj.RecipientId)
                       FinalObj = obj
                       if(RespData) {
                        FinalObj = {...obj, ...RespData}
                       }
                        DraggableControls.push(FinalObj)
                        checkValidControlsForResp(FinalObj)
                    })
                    setDraggableControls([...DraggableControls])
                    setSelectedControl({...DraggableControls[0]})
                    selectControlOBJ = DraggableControls[0]
                }
            }
        })
    }
    useEffect(() => {
        docsData = step3Data
    }, [step3Data])
    function getControlTypesInfoData() {
        addDocumentService.getControlsTypes().then(data => {
            
            var controlKeys = Object.keys(data.ControlTypes);
            var controls = []
            controlKeys.forEach((e) => {
                controls = [...controls, ...data.ControlTypes[e]]
            })
          let dsccontroll = controls.find(item => item.Code == "digitalsignature")
          setDsccontroldetails(dsccontroll)
            console.log(controls,"controls")
            setControlTypes(controls)
        })
    }

    useEffect(() => {
        alertService.clear()
        setdocument(commonService.documentValue)
        getControlTypesInfoData()
        getDocumentInfoData()
        getselectpagelist()
        getsignaturepositionslist()
    }, [])
    useEffect(()=>{
        DSCcontrolDragEnd()
    },[controldraginfo])

    function subDocumentsLoadSuccess(e) {
        setSubNumPages(e.numPages)
    }
    function getOnRenderSuccess(e, pageIndex, page) {
        pagesArray.push(page)
        if(subNumPages == pagesArray.length){
            DocumentControlsSetUp()
        }
       }
    function DocumentControlsSetUp() {
        for (let index = 0; index < DraggableControls.length; index++) {
            DraggableControls[index].Left = DraggableControls[index].LeftByPage - 10
            let topPosition = 0
            if (DraggableControls[index].PageNumber == 1) {
                DraggableControls[index].Top = DraggableControls[index].TopByPage
            } else {
                for (var idx = 0; idx < DraggableControls[index].PageNumber; idx++) {
                if ((DraggableControls[index].PageNumber - 1) == idx) {
                  topPosition = (topPosition + DraggableControls[index].TopByPage)
                  DraggableControls[index].Top = topPosition
                } else {
                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[idx].firstChild
                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                }
              }
            }
        }
        setDraggableControls([...DraggableControls])
    } 
    function getselectpagelist(){
        addDocumentService.getSelectpagelist().then(result=>{
            if(result){
                setSelectpagelist(result?.Entity)
            }
            
        })
    }
    function getsignaturepositionslist(){
        addDocumentService.getSignaturepositionlist().then(result=>{
            if(result){
                setdscsignPositionlist(result?.Entity)
            }
        })

    }
    function addNewRecipControlRecord(event, controlObj) {

    }
    /****** To Go to Back dashboard Page  */
    function back() {
        makeUpadteApiCall(false)
    }
    function getNext() {
        makeUpadteApiCall(true)
    }
    function allowDrop(ev) {
        ev.preventDefault();
        ev.dataTransfer.dropEffect = "move";
    }
    function drag(ev, item) {
        if (item) {
            var ghost = document.getElementById("drag-ghost");
            if (ghost && ghost.parentNode) {
                ghost.parentNode.removeChild(ghost);
            }
            const ItemData = { ...selectedRecipient, ...item }
            // const img = new Image();
            // var elem = document.createElement("div");
            // elem.id = "drag-ghost";
            // elem.classList.add('grabbing')
            // img.src = item.ImagePreview;
            // img.id = 'drag_image';
            // img.classList.add('grabbing')
            // elem.appendChild(img)
            // document.body.appendChild(elem);
            let neEle = document.getElementById(item.IconClass + '-drag')
            ev.dataTransfer.effectAllowed = "move";
            ev.dataTransfer.setData("controlObj", JSON.stringify(ItemData));
            ev.dataTransfer.setDragImage(neEle, 0, 0);
        }
    }
    function drop(ev, item) {
        var ghost = document.getElementById("drag-ghost");
        if (ghost && ghost.parentNode) {
          ghost.parentNode.removeChild(ghost);
        }
        if(ev.dataTransfer.getData("controlObj")) {
        // const MainDoc =   document.querySelector('#scrollArea');
        ev.preventDefault();
        if(ev.target.offsetParent.dataset.pageNumber) {
        let height = 0;
        let topPosition = 0;
        const controlObj = JSON.parse(ev.dataTransfer.getData("controlObj"));     
        if(controlObj.RecipientPageNumbers.includes(parseInt(ev.target.offsetParent.dataset.pageNumber))){
            topPosition = ev.nativeEvent.layerY
            const newId = generateUQID(controlObj.Code, 8, 'string');
            controlObj.PageNumber = ev.target.offsetParent.dataset.pageNumber;
          for (let index = 0; index < controlObj.PageNumber; index++) {
          const pagerefrence =  document.getElementsByClassName('react-pdf__Page')[index].firstChild
          if(controlObj.PageNumber == (index  + 1)) {
            height = height + ev.nativeEvent.layerY
          } else {
            height = height + Number(pagerefrence.style.height.replace('px', ''))
          }
           }
            const pageDom =  document.getElementsByClassName('react-pdf__Page')[controlObj.PageNumber-1].firstChild
            if((Number(pageDom.style.width.replace('px', '')))  < ev.nativeEvent.layerX ) {
                controlObj.LeftByPage = (Number(pageDom.style.width.replace('px', '')) +10) - 160;
                controlObj.Left = Number(pageDom.style.width.replace('px', '')) - 160;
            } else {
                controlObj.LeftByPage = ev.nativeEvent.layerX + 10
                controlObj.Left = ev.nativeEvent.layerX;
            }
            let TotalHeight = TotalPagesHeight
            if (!TotalPagesHeight) {
                for (let index = 0; index < subNumPages; index++) {
                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index].firstChild
                    if (subNumPages == (index + 1)) {
                        TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                        setTotalPagesHeight(TotalHeight)
                        break
                    } else {
                        TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                    }
                    }
                }
                if (TotalHeight < (height + 60)) {
                    height = (TotalHeight - 60)
                    topPosition = (ev.nativeEvent.layerY - 60)
                }
                  
                   
                
            controlObj.id = newId;
            controlObj.IsMandatory = true;
            // controlObj.HasSignatoryField = false
            // controlObj.HasNotarizerField = false
            controlObj.AdControlId = -1,
            controlObj.ADocumentId = step3Data.ADocumentId,
            controlObj.CtrlId = newId;
            controlObj.TopByPage = topPosition
            controlObj.FontFamily = 'Arial'
            controlObj.FontSize = 15
            controlObj.FontColor = ' rgb(0,0,0)'
            controlObj.Justification = 'left'
            controlObj.BackgroundColor = 'none'
            controlObj.Top = height;
            if(controlObj.Code == 'checkbox') {
                controlObj.Width = 30;
                controlObj.Height = 30;
            } else {
                controlObj.Width = 155;
                controlObj.Height = 30;
            }
            controlObj.isNewControl = true
            DraggableControls.push(controlObj)
            setDraggableControls([...DraggableControls])
            setSelectedControl({...controlObj})
            selectControlOBJ = controlObj
            checkValidControlsForResp(controlObj)
        }
        else{
            const message = controlObj.FullName + ' has no permissions for this page.'
            alertService.warn(message )
        }

     
    
    } else {
        alertService.warn('Please drop control on pdf page only')
    }
}
    }
    function checkValidControlsForResp(controlObj) {
        for (let k = 0; k < docsData.Recipients.length; k++) {
            if (controlObj.RecipientId == docsData.Recipients[k].RecipientId && docsData.Recipients[k].IsSignatory && controlObj.Code == 'signature') {
                const controExist = DraggableControls.find(data => { if (data.Code == 'signature' && !data.IsDelete && data.RecipientId ==docsData.Recipients[k].RecipientId ) { return true } })
                if (controExist) { docsData.Recipients[k].HasSignatoryField = true } else {
                    docsData.Recipients[k].HasSignatoryField = false
                }
            }
            if (controlObj.RecipientId == docsData.Recipients[k].RecipientId && docsData.Recipients[k].IsNotarizer && controlObj.Code == 'seal' ) {
                const controExist = DraggableControls.find(data => { if (data.Code == 'seal' && !data.IsDelete&& !data.IsDelete && data.RecipientId ==docsData.Recipients[k].RecipientId) { return true } })
                if (controExist) {
                    docsData.Recipients[k].HasNotarizerField = true
                } else {
                    docsData.Recipients[k].HasNotarizerField = false
                }
            }
        }
        setStep3Data({ ...docsData })
    }

    function controlOnChangeValue(control, value) {
        setDraggableControls(prevState => {
            const newState = prevState.map(obj => {
              if (obj.CtrlId == control.CtrlId) {
                return {...obj, Value: value ? value : null};
              }
                    return obj;
            });
      
            return newState;
          });
        // for (let index = 0; index < DraggableControls.length; index++) {
        //     if (DraggableControls[index].CtrlId == control.CtrlId) {
        //         DraggableControls[index].Value = value;
        //         break
        //     }
        // }
        // setDraggableControls([...DraggableControls])
    }
    function controlResizeEnd(sizeValue, e, direction, ref,position, control) {
        setSelectedControl({...control})
        selectControlOBJ = control
        for (let index = 0; index < DraggableControls.length; index++) {
            if (DraggableControls[index].CtrlId == control.CtrlId) {
                ref.style.width ? DraggableControls[index].Width = Number(ref.style.width.replace('px', '')): null;
                ref.style.height ? DraggableControls[index].Height = Number(ref.style.height.replace('px', '')) : null;
                const Pagereference = document.getElementsByClassName('react-pdf__Page')[DraggableControls[index].PageNumber-1].firstChild
                if(Number(Pagereference.style.width.replace('px', '')) < (DraggableControls[index].LeftByPage + DraggableControls[index].Width + 3)) {
                    DraggableControls[index].LeftByPage = Pagereference.width -(10 + DraggableControls[index].Width)
                    DraggableControls[index].Left = Pagereference.width-(10 + DraggableControls[index].Width)
                }
                let TotalHeight = TotalPagesHeight
                if (!TotalPagesHeight) {
                    for (let index = 0; index < subNumPages; index++) {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index].firstChild
                        if (subNumPages == (index + 1)) {
                            TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                            setTotalPagesHeight(TotalHeight)
                            break
                        } else {
                            TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                }
                if (TotalHeight < (DraggableControls[index].Top + 10 + DraggableControls[index].Height)) {
                    DraggableControls[index].TopByPage = DraggableControls[index].TopByPage -(10 + DraggableControls[index].Height)
                    DraggableControls[index].Top = TotalHeight-(10 + DraggableControls[index].Height)
                }
                break
            }
        }
        setDraggableControls([...DraggableControls])
    }
    function controlDragEnd(mouseEvent, dragPositionObj, control) {
        setSelectedControl({...control})
        selectControlOBJ = control
        let pageNumber = 0;
        let height = 0
        let previousPagesHeight = 0
        let pageTopPosition = 0
        for (let index = 0; index < DraggableControls.length; index++) {
            if (DraggableControls[index].CtrlId == control.CtrlId) {
                let TotalHeight = TotalPagesHeight
                if (!TotalPagesHeight) {
                    for (let index = 0; index < subNumPages; index++) {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index].firstChild
                        if (subNumPages == (index + 1)) {
                            TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                            setTotalPagesHeight(TotalHeight)
                            break
                        } else {
                            TotalHeight = TotalHeight + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                }
                if(dragPositionObj.y < 7) {
                    height = 9
                    pageTopPosition = 9
                    pageNumber = 1
                 } else {
                let dropedPage = document.getElementsByClassName('react-pdf__Page')[0];
                for (var idx = 0; idx <= $('body').find('canvas').length; idx++) {
                     let DocPageDom = document.getElementsByClassName('react-pdf__Page')[0].firstChild
                    if(dragPositionObj.y < Number(DocPageDom.style.height.replace('px', ''))) {
                        pageNumber = 1
                        height = dragPositionObj.y
                        pageTopPosition = dragPositionObj.y
                        break
                    } else if (height >= (dragPositionObj.y)) {
                    dropedPage = document.getElementsByClassName('react-pdf__Page')[idx -1].firstChild
                    pageNumber = idx
                    height = dragPositionObj.y
                    break
                  } else {
                    const dPage = document.getElementsByClassName('react-pdf__Page')[idx].firstChild
                    previousPagesHeight = height
                    height = height + Number(dPage.style.height.replace('px', ''))
                  }
                }
            }
            previousPagesHeight ? pageTopPosition = dragPositionObj.y - previousPagesHeight : ''
            if(selectedControl.RecipientPageNumbers.includes(parseInt(pageNumber))){
                const Pagereference = document.getElementsByClassName('react-pdf__Page')[pageNumber-1].firstChild
                if(Number(Pagereference.style.width.replace('px', '')) < (dragPositionObj.x + DraggableControls[index].Width + 3)) {
                    DraggableControls[index].LeftByPage = (Number(Pagereference.style.width.replace('px', '')) + 10) - (10 + DraggableControls[index].Width)
                    DraggableControls[index].Left = Number(Pagereference.style.width.replace('px', '')) - (10 + DraggableControls[index].Width)
                } else if (dragPositionObj.x < 10) {
                    DraggableControls[index].LeftByPage = 11 + 10
                    DraggableControls[index].Left = 11
                }  else {
                    DraggableControls[index].LeftByPage = dragPositionObj.x + 10
                    DraggableControls[index].Left = dragPositionObj.x
                }
                DraggableControls[index].PageNumber = pageNumber
                if (TotalHeight < (height + 10 + DraggableControls[index].Height)) {
                    height = TotalHeight - (10 + DraggableControls[index].Height)
                    pageTopPosition = pageTopPosition - (10 + DraggableControls[index].Height)
                }
                DraggableControls[index].TopByPage = pageTopPosition
                DraggableControls[index].Top = height 
                DraggableControls[index].DSCCoordinates =`${DraggableControls[index].Left},${(Number(Pagereference.style.width.replace('px', ''))-DraggableControls[index].TopByPage)},${DraggableControls[index].Left+DraggableControls[index].Width},${(Number(Pagereference.style.width.replace('px', ''))-(DraggableControls[index].TopByPage+DraggableControls[index].Height))}`
                
                break 
          
                
            } else{
                const message = selectedControl.FullName + ' has no permissions for this page.'
                alertService.warn(message )
                break
            }
             
             }
            
        }
        let info = DraggableControls.find(item=>control.CtrlId === item.CtrlId)
        setcontroldraginfo(info)
        setDraggableControls([...DraggableControls])
        // SetAll(control)
          
        
    }
    function DSCcontrolDragEnd() {
        for (var i = DraggableControls.length - 1; i >= 0; i--) {
            if (DraggableControls[i].RecipientId == controldraginfo.RecipientId || DraggableControls[i].CtrlId == control.CtrlId) {
                if (DraggableControls[i].AdControlId) {
                    DraggableControls[i].IsDelete = 1
                } else {
                    DraggableControls.splice(i, 1)
                }
            }
        }
        for (let page = 1; page <= subNumPages; page++) {
            if (selectedRecipient.RecipientPageNumbers.includes(page)) {
                let topPosition = 0
                let allselectedControl = { ...Dsccontroldetails,...selectedRecipient }
                let newId = generateUQID(allselectedControl.Code, 8, 'string');
                allselectedControl.PageNumber = page
                for (let index = 1; index <= page; index++) {
                    if (page == index) {
                        allselectedControl.AdControlId = -1
                        allselectedControl.CtrlId = newId
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                        allselectedControl.Left = (Number(pagerefrence.style.width.replace('px', ''))-controldraginfo.LeftByPage)-122
                          allselectedControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', ''))-controldraginfo.LeftByPage)-122
                            allselectedControl.Top=(Number(pagerefrence.style.height.replace('px', '')) + controldraginfo.Top)-65
                            allselectedControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) + controldraginfo.TopByPage)-65
                            
                        allselectedControl.SelectPage = Dscselectedpage
                        DraggableControls.push(allselectedControl)
                        break;
                    } else {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                        topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                    }
                }
            } else {
                nonPermitPagesArray.push(page)
            }
           
             setshowControlAddedbySelectPage(true)
            setDraggableControls([...DraggableControls])

        }
        
    }
    // function SetAll(control){
    //   let  maincontrol= DraggableControls.find(item=>item.CtrlId===control.CtrlId)
    //   console.log(maincontrol,"maincontrol")
    //   let dragcontrol =[]
    //   DraggableControls.map(item=>{
    //      const Pagereference2 =document.getElementsByClassName('react-pdf__Page')[item.PageNumber-1].firstChild
    //      item.LeftByPage=maincontrol.LeftByPage
    //      item.Left=maincontrol.Left
    //      item.TopByPage=(Number(pagerefrence2.style.height.replace('px', '')) - 65)+maincontrol.TopByPage
    //      item.Top=(Number(pagerefrence2.style.height.replace('px', '')) - 65)+maincontrol.Top
    //      //item.Width=Number(Pagereference2.style.width.replace('px', ''))
    //      //item.Height=Number(Pagereference2.style.height.replace('px', ''))
    //      item.DSCCoordinates =`${maincontrol.Left},${(Number(Pagereference2.style.width.replace('px', ''))-maincontrol.TopByPage)},${maincontrol.Left+maincontrol.Width},${(Number(Pagereference2.style.width.replace('px', ''))-(maincontrol.TopByPage+maincontrol.Height))}`
    //      dragcontrol.push(item)
    //   })
    //   console.log(dragcontrol,"dragcontrol")
    //   setDraggableControls(dragcontrol)
       
    // }
    function getDropRecipient(event) {
        const selectRespObj = step3Data.Recipients.find(data => event.target.value == data.RecipientId)
        setSelectedRecipient(selectRespObj)
    }
    function validControls(control) {
        if (control.Code == 'signature' || control.Code == 'initial' || control.Code == 'date' ||
            control.ControlTypeId == 5 || control.ControlTypeId == 3 || control.ControlTypeId == 6) {
            return true
        }
        else false
    }


    function makeUpadteApiCall(next) {
        let FinalData = { ...step3Data }
        let ErrorExist = false
        if(IsanyoneenableDsc === true){
         contronotExistRespArray = []
        for (let k = 0; k < FinalData.Recipients.length; k++) {
            if (FinalData.Recipients[k].IsNotarizer && FinalData.Recipients[k].IsSignatory
                && (!FinalData.Recipients[k].HasNotarizerField || !FinalData.Recipients[k].HasSignatoryField)) {
                contronotExistRespArray.push(FinalData.Recipients[k])
            } else if (FinalData.Recipients[k].IsNotarizer && !FinalData.Recipients[k].HasNotarizerField) {
                contronotExistRespArray.push(FinalData.Recipients[k])

            } else if (FinalData.Recipients[k].IsSignatory && !FinalData.Recipients[k].HasSignatoryField) {
                contronotExistRespArray.push(FinalData.Recipients[k])
            }
        }
        if(contronotExistRespArray && contronotExistRespArray.length > 0) {
            ErrorExist = true
            if(next) {
            setMandatoryNotExist(true)
            }
        }
    }
        // const unique = [...new Map(contronotExistRespArray.map((m) => [m.RecipientId, m])).values()];
if(!ErrorExist || !next) {
        let d = new Date();
        let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
        let timeSplit = time.split(' ');
        let timeZone = timeSplit[1];
        let controls = {}
        delete FinalData.UploadDocuments;
        FinalData.DateTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
        FinalData.TimeZone = timeZone;
        for (let index = 0; index < DraggableControls.length; index++) {
            controls[DraggableControls[index].CtrlId] = DraggableControls[index]
            FinalData.CtrlIds.push(DraggableControls[index].CtrlId);
            for (let k = 0; k < FinalData.Recipients.length; k++) {
                if (!FinalData.Recipients[k].Controls) {
                    FinalData.Recipients[k].Controls = [];
                }
                if (FinalData.Recipients[k].RecipientId == DraggableControls[index].RecipientId) {
                    delete DraggableControls[index].CtrlId.Controls
                    delete DraggableControls[index].CtrlId.RecipientPageNumbers
                    FinalData.Recipients[k].Controls.push(DraggableControls[index].CtrlId)

                }
            }
        }
        FinalData.Controls = controls
        addDocumentService.updateDocumentInfo(FinalData.ADocumentId, FinalData).then(data => {
            if (data) {
                if (next) {
                    usehistory.push('/account/documentreview/' + step3Data.ADocumentId)
                } else {
                    usehistory.push('/account/documentselect/' + step3Data.ADocumentId)
                }
            }
        })
    }
    }
    
    const handleOnDrag = (event, item) => {
        // event.dataTransfer.dropEffect = "move";
        event.preventDefault();
    }
 function validpopupclose() {
    if(selection.draw) {
        if(selectedSignatureControlEditData.SignData  || selectedSignatureControlEditData.SignInitial) {
            return true
        } else {
         return   false
        }
    } else if (selection.choose) {
    if(selectedSignatureControlEditData.SignatureFont) {
        return true
    } else {
      return  false
    }} else if (selection.upload) {
        if(selectedSignatureControlEditData.SignUpload  || selectedSignatureControlEditData.InitialUpload) {
            return true
        } else {
         return   false
    }
}
 }
    const signSelectionHandler=()=>{
        if(validpopupclose() || selection.setting) {
        DraggableControls.forEach(e=>{ 
                if((e.Code=="signature" || e.Code=="initial") && e.RecipientId==selectedControl.RecipientId ){
                        e.SignatureUpload = '';
                        e.SignatureFont = '';
                        e.InitialUpload=""
                    if(selectedSignatureControlEditData.signType=="Font"){
                        e.SignatureUpload = '';
                        e.SignatureFont = selectedSignatureControlEditData.SignatureFont;
                        e.InitialUpload=""
                    }
                    else if (selectedSignatureControlEditData.signType=="Upload"){
                        e.SignatureUpload=selectedSignatureControlEditData.SignUpload
                        e.SignatureFont = ''
                        e.InitialUpload=selectedSignatureControlEditData.InitialUpload;
                    } else if (selectedSignatureControlEditData.signType=="Sign") {
                        e.SignatureUpload=selectedSignatureControlEditData.SignData
                        e.SignatureFont = ''
                        e.InitialUpload = selectedSignatureControlEditData.SignInitial;
                    }
                    e.IsMandatory = selectedSignatureControlEditData.IsMandatory;   
                    e.SignType = selectedSignatureControlEditData.signType;
                    e.SignatureInitial = selectedSignatureControlEditData.SignatureInitial
                    e.SignatureName = selectedSignatureControlEditData.SignatureName

                }  

        })

        for (let k = 0; k < docsData.Recipients.length; k++) {
            if(docsData.Recipients[k].RecipientId === selectedControl.RecipientId) {
                docsData.Recipients[k].SignatureUpload = '';
                docsData.Recipients[k].SignatureFont = '';
                docsData.Recipients[k].InitialUpload = ''
                if(selectedSignatureControlEditData.signType == "Font") {
                    docsData.Recipients[k].SignatureUpload = '';
                    docsData.Recipients[k].SignatureFont = selectedSignatureControlEditData.SignatureFont;
                    docsData.Recipients[k].InitialUpload = ''
                } else if(selectedSignatureControlEditData.signType == "Upload") {
                    docsData.Recipients[k].SignatureUpload = selectedSignatureControlEditData.SignUpload
                    docsData.Recipients[k].SignatureFont = ''
                    docsData.Recipients[k].InitialUpload=selectedSignatureControlEditData.InitialUpload;
                }  else if(selectedSignatureControlEditData.signType == "Sign") {
                    docsData.Recipients[k].SignatureUpload = selectedSignatureControlEditData.SignData
                    docsData.Recipients[k].SignatureFont = ''
                    docsData.Recipients[k].InitialUpload = selectedSignatureControlEditData.SignInitial;
                }
                // docsData.Recipients[k].Value = selectedSignatureControlEditData.signData
                docsData.Recipients[k].SignType = selectedSignatureControlEditData.signType;
                docsData.Recipients[k].SignatureInitial = selectedSignatureControlEditData.SignatureInitial
                docsData.Recipients[k].SignatureName = selectedSignatureControlEditData.SignatureName
                break
            }      
            
            }
            for (let index = 0; index < DraggableControls.length; index++) {
                if(DraggableControls[index].CtrlId == selectedControl.CtrlId) {
                    setSelectedControl({...DraggableControls[index]})
                    selectControlOBJ = DraggableControls[index]
                    break
                }
            }
        setStep3Data({...docsData})
        setDraggableControls([...DraggableControls])
        if(isSelectChanged && PopUpSelectedPage) {
            addOtherControlsBySelectPage()
        } else {
            setSignaturePopShow(false)
            setControlPopupShow(false)
        }

        } else {
            setErrorStatus(true)
        }
    }
      const deleteHandler=()=>{
        for (let index = 0; index < DraggableControls.length; index++) {
            if(DraggableControls[index].CtrlId == selectedControl.CtrlId && DraggableControls[index].isNewControl == true) {
                DraggableControls.splice(index,1);
                break
            } else if(DraggableControls[index].CtrlId == selectedControl.CtrlId && !DraggableControls[index].isNewControl) {
                DraggableControls[index].IsDelete = 1
                break
            }
            
        }
          setDraggableControls([...DraggableControls])
          checkValidControlsForResp(selectedControl)
          setControlPopupShow(false)
          setSelectedControl({})
          selectControlOBJ = {}
          setUploadPopShow(false)
          setSignaturePopShow(false)
        }
        
        const handleKeyDown = event => {
            if(event.key=="Delete"){
                if(selectedControl.CtrlId){
                    deleteHandler()
                }
            }
          }
    const loadSpinner = () => {
        return <div style={{ display: "flex", alignContent: 'center', alignItems: "center", justifyContent: "center", marginTop: '30%' }}>
            <Spinner variant="primary"  animation="border" /></div>
    }

    const otherSelectionHandler=()=>{
        if(isSelectChanged && PopUpSelectedPage) {
          addOtherControlsBySelectPage()
        } else {
            DraggableControls.forEach(e=>{
                if(e.Code==selectedControl.Code  && e.RecipientId==selectedControl.RecipientId){
                 e.IsMandatory=selectedControl.IsMandatory;
                }      
              })
            setControlPopupShow(false)
            setDraggableControls([...DraggableControls])
        }
    }
function changeAttachControlEdit(type) {
    setIsSelectChanged(false)
if((selectedRecipient.RecipientId == selectedControl.RecipientId && !selectedRecipient.IsOwner) || (selectedRecipient.RecipientId != selectedControl.RecipientId && selectedRecipient.IsOwner) ) {
    setControlPopupShow(true)
} else {
    setMandatoryCheck(selectedControl.IsMandatory ? selectedControl.IsMandatory: false)
    selectedControl.SelectPage ? setSelectedPage(selectedControl.SelectPage) : setSelectedPage('')
    setAttachmentSelection({upload:true,manage:false})
    setAttachImageFile('')
    setIsFileUploaded(false)
    setfailedMessage(false);
    setFileName('')
    if (type == 'IMAGE') {
        setAcceptedFiles('image/png,image/jpg,image/jpeg')
        isControlImage =true
    } else {
        isControlImage =false
        setAcceptedFiles('image/png,image/jpg,image/jpeg,.pdf')
    }
    if(selectedControl && selectedControl.Value){
        setAttachImageFile(selectedControl.FileBase64); 
        setFileName(selectedControl.Value)
        setIsFileUploaded(true)

    }
    setUploadPopShow(true)
    selectedControl.Value ? extentionFile(selectedControl.Value) : null;
    setTimeout(() => {
        getTags()
    }, 500);
}

}

// ImageSeal/Attachment
function getTags() {
    document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
        const dropZoneElement = inputElement.closest(".use_templete.drop-zone");

        dropZoneElement.addEventListener("click", (e) => {
            if (e.target.className != 'file-add-text') {
                inputElement.click();
            }
        });

        inputElement.addEventListener("change", (e) => {
            if (inputElement.files.length) {
                for (let inputFile of inputElement.files) {
                    saveFiles(inputFile)                }
                var inputVal = inputElement.value;
            }
        });
        dropZoneElement.addEventListener("dragover", (e) => {
            e.preventDefault();
            e.stopPropagation();
            dropZoneElement.classList.add("drop-zone--over");
        });
        dropZoneElement.addEventListener('dragleave', function (e) {
            e.preventDefault();
            e.stopPropagation();
            dropZoneElement.classList.remove("drop-zone--over");
        });

        ["dragleave", "dragend"].forEach((type) => {
            dropZoneElement.addEventListener(type, (e) => {
            });
        });

        dropZoneElement.addEventListener("drop", (e) => {
            if (e.dataTransfer.files.length) {
                inputElement.files = e.dataTransfer.files;
                for (let inputFile of e.dataTransfer.files) {
                    saveFiles(inputFile)
                }
            }
            e.preventDefault();
            e.stopPropagation();
            dropZoneElement.classList.remove("drop-zone--over");
        });
    });

}

function clearFileInfo() {
    setAttachImageFile('')
    setIsFileUploaded(false)
    setfailedMessage(false);
    setFileName('')
    setMandatoryCheck(selectedControl.IsMandatory ? selectedControl.IsMandatory: false)
    setPopUpSelectedPage(selectedControl.SelectPage ? selectedControl.SelectPage : '')
    setIsSelectChanged(false)
    setTimeout(() => {
        getTags()
    }, 500);
    }

    function saveFiles(file) {
        if (isControlImage) {
            var allowedWithOutPDFExtensions = /(\.png|\.jpg|\.jpeg)$/i;
            if (allowedWithOutPDFExtensions.exec(file.name) && 1054464 >= file.size ) {
                process(file)
            } else {
                setfailedMessage(true);
            }
        } else {
            var allowedWithPDFExtensions = /(\.png|\.jpg|\.jpeg|\.pdf)$/i;
            if (allowedWithPDFExtensions.exec(file.name) && 2097152 >= file.size) {
                process(file)
            } else {
                setfailedMessage(true);
            }
        }
    }
function getBase64(file) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        setAttachImageFile(reader.result); 
        setIsFileUploaded(true)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}
function process(file) {
    setFileName(file.name)
    extentionFile(file.name)
    getBase64(file);
    setIsFileUploaded(true);
    setfailedMessage(false);
}


function extentionFile(name) {
    var ext = name.split('.');
    setclassName('')
    switch (ext[1]) {
          case 'pdf':
            setclassName('fa fa-file-pdf');
            setIsImageFile(false)
          break;
        case 'png':
        case 'jpg':
        case 'jpeg':
            setclassName('fa fa-picture');
            setIsImageFile(true)
            break; default:
            setclassName('fa fa-file');
            break;
    }
}
function ControlpopDocLoad(e) {
    setControlPopDocPages(e.numPages)
}

function handleControlErrorClick() {
    setfailedMessage(false);
}
function UploadImageAttachment() {
        for (let index = 0; index < DraggableControls.length; index++) {
            if(DraggableControls[index].CtrlId == selectedControl.CtrlId) {
                DraggableControls[index].Value = fileName ? fileName : null
                DraggableControls[index].FileBase64 = attachImageFile ? attachImageFile : null
                DraggableControls[index].IsMandatory = MandatoryCheck
                setSelectedControl({...DraggableControls[index]})
                selectControlOBJ = DraggableControls[index]
                selectControlOBJ.Value = fileName ? fileName : null
                selectControlOBJ.FileBase64 = attachImageFile ? attachImageFile : null
                selectControlOBJ.IsMandatory = MandatoryCheck
                break
            }
            
        }
          setDraggableControls([...DraggableControls])
          if(isSelectChanged && PopUpSelectedPage) {
            addOtherControlsBySelectPage()
          } else {
            setUploadPopShow(false)
            setAttachImageFile('')
            setIsFileUploaded(false)
            setfailedMessage(false);
            setFileName('')
          }

}
    const getMandatory = (event) => {
        setMandatoryCheck(event.target.checked)
    }
    const getSelectPage = (e) => {
        setSelectedPage(e.target.value)
        setPopUpSelectedPage(e.target.value)
        setIsSelectChanged(true)
    }

    function addOtherControlsBySelectPage() {
        console.log({...selectControlOBJ},"...selectControlOBJ")
        nonPermitPagesArray = []
        const newId = generateUQID(selectControlOBJ.Code, 8, 'string');
        const groupId = generateUQID(null, 4, 'int');
        let selectPageSelectControl = { ...selectControlOBJ }
        if (PopUpSelectedPage == 'FIRST') {
            if (selectPageSelectControl.RecipientPageNumbers.includes(1)) {
                let newId = generateUQID(selectPageSelectControl.Code, 8, 'string');
                if (!selectPageSelectControl.GroupId) {
                    selectPageSelectControl.GroupId = groupId;
                }
                for (var i = DraggableControls.length - 1; i >= 0; i--) {
                    if (DraggableControls[i].GroupId == selectPageSelectControl.GroupId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                        if (DraggableControls[i].AdControlId) {
                            DraggableControls[i].IsDelete = 1
                        } else {
                            DraggableControls.splice(i, 1)
                        }
                    }
                }
                const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectPageSelectControl.SelectPage = PopUpSelectedPage
                DraggableControls.push(selectPageSelectControl)
                setControlPopupShow(false)
                setSignaturePopShow(false)
                setControlPopupShow(false)
                setDraggableControls([...DraggableControls])
                setUploadPopShow(false)
                setUploadPopShow(false)
                setAttachImageFile('')
                setIsFileUploaded(false)
                setfailedMessage(false);
                setFileName('')
                setshowControlAddedbySelectPage(true)
            } else {
                nonPermitPagesArray=[1]
                setControlPopupShow(false)
                setSignaturePopShow(false)
                setControlPopupShow(false)
                setUploadPopShow(false)
                setUploadPopShow(false)
                setAttachImageFile('')
                setIsFileUploaded(false)
                setfailedMessage(false);
                setFileName('')

            }
        } else if (PopUpSelectedPage == 'LAST') {
            if (selectPageSelectControl.RecipientPageNumbers.includes(1)) {
                let newId = generateUQID(selectPageSelectControl.Code, 8, 'string');
                if (!selectPageSelectControl.GroupId) {
                    selectPageSelectControl.GroupId = groupId;
                }
                for (var i = DraggableControls.length - 1; i >= 0; i--) {
                    if (DraggableControls[i].GroupId == selectPageSelectControl.GroupId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                        if (DraggableControls[i].AdControlId) {
                            DraggableControls[i].IsDelete = 1
                        } else {
                            DraggableControls.splice(i, 1)
                        }
                    }
                }
                let topPosition = 0
                for (let index = 1; index <= subNumPages; index++) {
                    if (subNumPages == index) {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                        if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + selectPageSelectControl.TopByPage)) {
                            selectPageSelectControl.Top = topPosition - 15
                            selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                        } else {
                            selectPageSelectControl.Top = topPosition + selectPageSelectControl.TopByPage
                        }
                        if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                            selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                            selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                        }
                        break;
                    } else {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                        topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                    }
                }
                selectPageSelectControl.SelectPage = PopUpSelectedPage
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = subNumPages
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setControlPopupShow(false)
                setSignaturePopShow(false)
                setControlPopupShow(false)
                setUploadPopShow(false)
                setUploadPopShow(false)
                setAttachImageFile('')
                setIsFileUploaded(false)
                setfailedMessage(false);
                setFileName('')
                setshowControlAddedbySelectPage(true)

            } else {
                setControlPopupShow(false)
                setSignaturePopShow(false)
                setControlPopupShow(false)
                setUploadPopShow(false)
                setUploadPopShow(false)
                setAttachImageFile('')
                setIsFileUploaded(false)
                setfailedMessage(false);
                setFileName('')
                nonPermitPagesArray=[subNumPages]

            }
        } else if (PopUpSelectedPage == 'ALL') {
            if (!selectPageSelectControl.GroupId) {
                selectPageSelectControl.GroupId = groupId;
            }
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].GroupId == selectPageSelectControl.GroupId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }
            for (let page = 1; page <= subNumPages; page++) {
                if (selectPageSelectControl.RecipientPageNumbers.includes(page)) {
                    let topPosition = 0
                    let allselectedControl = { ...selectPageSelectControl }
                    let newId = generateUQID(allselectedControl.Code, 8, 'string');
                    allselectedControl.PageNumber = page
                    for (let index = 1; index <= page; index++) {
                        if (page == index) {
                            allselectedControl.AdControlId = -1
                            allselectedControl.CtrlId = newId
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + allselectedControl.TopByPage)) {
                                allselectedControl.Top = topPosition - 15
                                allselectedControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                            } else {
                                allselectedControl.Top = topPosition + allselectedControl.TopByPage
                            }
                            if (Number(pagerefrence.style.width.replace('px', '')) < allselectedControl.LeftByPage) {
                                allselectedControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                                allselectedControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                            }
                            allselectedControl.SelectPage = PopUpSelectedPage
                            DraggableControls.push(allselectedControl)
                            break;
                        } else {
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                } else {
                    nonPermitPagesArray.push(page)
                }
                setControlPopupShow(false)
                setSignaturePopShow(false)
                 setControlPopupShow(false)
                 setUploadPopShow(false)
                 setUploadPopShow(false)
                 setAttachImageFile('')
                 setIsFileUploaded(false)
                 setfailedMessage(false);
                 setFileName('')
                 setshowControlAddedbySelectPage(true)
                setDraggableControls([...DraggableControls])

            }
        } else if (PopUpSelectedPage == 'EVEN') {
            if (!selectPageSelectControl.GroupId) {
                selectPageSelectControl.GroupId = groupId;
            }
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].GroupId == selectPageSelectControl.GroupId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }
            for (let page = 1; page <= subNumPages; page++) {
                if (page % 2 == 0) {
                    if (selectPageSelectControl.RecipientPageNumbers.includes(page)) {
                        let topPosition = 0
                        let allselectedControl = { ...selectPageSelectControl }
                        let newId = generateUQID(allselectedControl.Code, 8, 'string');
                        allselectedControl.PageNumber = page
                        for (let index = 1; index <= page; index++) {
                            if (page == index) {
                                allselectedControl.AdControlId = -1
                                allselectedControl.CtrlId = newId
                                const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + allselectedControl.TopByPage)) {
                                    allselectedControl.Top = topPosition - 15
                                    allselectedControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                                } else {
                                    allselectedControl.Top = topPosition + allselectedControl.TopByPage
                                }
                                if (Number(pagerefrence.style.width.replace('px', '')) < allselectedControl.LeftByPage) {
                                    allselectedControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                                    allselectedControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                                }
                                allselectedControl.SelectPage = PopUpSelectedPage
                                DraggableControls.push(allselectedControl)
                                break;
                            } else {
                                const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                            }
                        }
                    } else {
                        nonPermitPagesArray.push(page)
                    }
                }
                setControlPopupShow(false)
                setSignaturePopShow(false)
                 setControlPopupShow(false)
                 setUploadPopShow(false)
                 setUploadPopShow(false)
                 setAttachImageFile('')
                 setIsFileUploaded(false)
                 setfailedMessage(false);
                 setFileName('')
                setDraggableControls([...DraggableControls])
            }
        } else if (PopUpSelectedPage == 'ODD') {
            if (!selectPageSelectControl.GroupId) {
                selectPageSelectControl.GroupId = groupId;
            }
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].GroupId == selectPageSelectControl.GroupId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }
            for (let page = 1; page <= subNumPages; page++) {
                if (page % 2 != 0) {
                    if (selectPageSelectControl.RecipientPageNumbers.includes(page)) {
                        let topPosition = 0
                        let allselectedControl = { ...selectPageSelectControl }
                        let newId = generateUQID(allselectedControl.Code, 8, 'string');
                        allselectedControl.PageNumber = page
                        for (let index = 1; index <= page; index++) {
                            if (page == index) {
                                allselectedControl.AdControlId = -1
                                allselectedControl.CtrlId = newId
                                const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + allselectedControl.TopByPage)) {
                                    allselectedControl.Top = topPosition - 15
                                    allselectedControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                                } else {
                                    allselectedControl.Top = topPosition + allselectedControl.TopByPage
                                }
                                if (Number(pagerefrence.style.width.replace('px', '')) < allselectedControl.LeftByPage) {
                                    allselectedControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                                    allselectedControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                                }
                                allselectedControl.SelectPage = PopUpSelectedPage
                                DraggableControls.push(allselectedControl)
                                break;
                            } else {
                                const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                            }
                        }
                    } else {
                        nonPermitPagesArray.push(page)
                        // setControlPopupShow(false)

                    }
                }
                setUploadPopShow(false)
                setControlPopupShow(false)
                setSignaturePopShow(false)
                setControlPopupShow(false)
                setUploadPopShow(false)
                setAttachImageFile('')
                setIsFileUploaded(false)
                setfailedMessage(false);
                setFileName('')
                setDraggableControls([...DraggableControls])
                setshowControlAddedbySelectPage(true)


            }
        }
    }
    function addDSCControltodocument(){
        const newId = generateUQID(Dsccontroldetails.Code, 8, 'string');
        const groupId = generateUQID(null, 4, 'int');
        let selectPageSelectControl = { ...Dsccontroldetails,...selectedRecipient }
        selectPageSelectControl.Height=65;
        selectPageSelectControl.Width=122;
        if(Dscselectedpage=== 'FIRST'){
        if (selectPageSelectControl.RecipientPageNumbers.includes(1)) {
            let newId = generateUQID(selectPageSelectControl.Code, 8, 'string');
            if (!selectPageSelectControl.GroupId) {
                selectPageSelectControl.GroupId = groupId;
            }
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].RecipientId  == selectPageSelectControl.RecipientId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }

        //selectPageSelectControl.AdControlId = -1;
        if(DscselectedSignatureposition === 'Top-Left'){
        selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=20
        selectPageSelectControl.LeftByPage=20
        selectPageSelectControl.Top=20
        selectPageSelectControl.TopByPage=20
        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            }
        if(DscselectedSignatureposition === 'Top-Right'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
            selectPageSelectControl.isNewControl=true;
            selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', ''))-30)-122;
            selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', ''))-30)-122;
            selectPageSelectControl.Top=20
            selectPageSelectControl.TopByPage=20
                    if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                        selectPageSelectControl.Top = topPosition - 15
                        selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                    } else {
                        selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                    }
                    if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                        selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                        selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                    }
                    selectPageSelectControl.AdControlId = -1
                    selectPageSelectControl.CtrlId = newId
                    selectPageSelectControl.PageNumber = 1
                    selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                    selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                    DraggableControls.push(selectPageSelectControl)
                    setDraggableControls([...DraggableControls])
                    setAttachImageFile('')
                    setFileName('')
                    setshowControlAddedbySelectPage(true)
                }
        
        if(DscselectedSignatureposition === 'Top-Center'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', ''))-122)/2
        selectPageSelectControl.LeftByPage=Number(pagerefrence.style.width.replace('px', ''))-122/2
        selectPageSelectControl.Top=20
        selectPageSelectControl.TopByPage=20
        
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            
        }
        if(DscselectedSignatureposition === 'Bottom-Left'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=20
        selectPageSelectControl.LeftByPage=20
        selectPageSelectControl.Top=(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
        selectPageSelectControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
       
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
        }
        if(DscselectedSignatureposition === 'Bottom-Right'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', ''))-30)-122;
        selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', ''))-30)-122;
        selectPageSelectControl.Top=(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
        selectPageSelectControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
       
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            
        }
        if(DscselectedSignatureposition === 'Bottom-Center'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', ''))-20)/2;
        selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', ''))-20)/2;
        selectPageSelectControl.Top=(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
        selectPageSelectControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
       
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            
        }
        if(DscselectedSignatureposition === 'Middle-Left'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=20
        selectPageSelectControl.LeftByPage=20
        
        selectPageSelectControl.Top=(Number(pagerefrence.style.height.replace('px', '')) - 122)/2
        selectPageSelectControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) - 122)/2
       
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            
        }
        if(DscselectedSignatureposition === 'Middle-Right'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', '')) - 20)-122
        selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', '')) - 20)-122
        selectPageSelectControl.Top=(Number(pagerefrence.style.height.replace('px', '')) - 20)/2
        selectPageSelectControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) - 20)/2
       
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            
            
        }
        if(DscselectedSignatureposition === 'Middle-Center'){
            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[0].firstChild
            selectPageSelectControl.id=newId;
        selectPageSelectControl.isNewControl=true;
        selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', '')) - 122)/2
        selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', '')) - 122)/2
        selectPageSelectControl.Top=(Number(pagerefrence.style.height.replace('px', '')) - 122)/2
        selectPageSelectControl.TopByPage=(Number(pagerefrence.style.height.replace('px', '')) - 122)/2
       
                if ((Number(pagerefrence.style.height.replace('px', ''))) < (selectPageSelectControl.TopByPage)) {
                    selectPageSelectControl.Top = topPosition - 15
                    selectPageSelectControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                } else {
                    selectPageSelectControl.Top = selectPageSelectControl.TopByPage
                }
                if (Number(pagerefrence.style.width.replace('px', '')) < selectPageSelectControl.LeftByPage) {
                    selectPageSelectControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                    selectPageSelectControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                }
                selectPageSelectControl.AdControlId = -1
                selectPageSelectControl.CtrlId = newId
                selectPageSelectControl.PageNumber = 1
                selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                DraggableControls.push(selectPageSelectControl)
                setDraggableControls([...DraggableControls])
                setAttachImageFile('')
                setFileName('')
                setshowControlAddedbySelectPage(true)
            
        }
       }
        }
        else if(Dscselectedpage === "LAST"){
            if (selectPageSelectControl.RecipientPageNumbers.includes(1)) {
                let newId = generateUQID(selectPageSelectControl.Code, 8, 'string');
                if (!selectPageSelectControl.GroupId) {
                    selectPageSelectControl.GroupId = groupId;
                }
                for (var i = DraggableControls.length - 1; i >= 0; i--) {
                    if (DraggableControls[i].RecipientId  == selectPageSelectControl.RecipientId  || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                        if (DraggableControls[i].AdControlId) {
                            DraggableControls[i].IsDelete = 1
                        } else {
                            DraggableControls.splice(i, 1)
                        }
                    }
                }
                        if(DscselectedSignatureposition === 'Top-Left'){
                    let topPosition = 0
                for (let index = 1; index <= subNumPages; index++) {
                    if (subNumPages == index) {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                        selectPageSelectControl.Left=20
                        selectPageSelectControl.LeftByPage=20
                        selectPageSelectControl.Top=topPosition+20
                        selectPageSelectControl.TopByPage=topPosition+20
                        break;
                        
                    } else {
                        const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                        topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                    }
                }
                    selectPageSelectControl.id=newId;
                    selectPageSelectControl.isNewControl=true;
                            selectPageSelectControl.AdControlId = -1
                            selectPageSelectControl.CtrlId = newId
                            selectedRecipient.SelectPage=Dscselectedpage
                selectedRecipient.SignaturePosition=DscselectedSignatureposition
                            selectPageSelectControl.PageNumber = subNumPages
                            selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                            DraggableControls.push(selectPageSelectControl)
                            setDraggableControls([...DraggableControls])
                            setAttachImageFile('')
                            setFileName('')
                            setshowControlAddedbySelectPage(true)
                        }
                        if(DscselectedSignatureposition === 'Top-Right'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', ''))-30)-122;
                                    selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', ''))-30)-122;
                                    selectPageSelectControl.Top=topPosition+20
                                    selectPageSelectControl.TopByPage=topPosition+20
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)

                        }
                        if(DscselectedSignatureposition === 'Top-Center'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', ''))-122)/2;
                                    selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', ''))-122)/2;
                                    selectPageSelectControl.Top=topPosition+20
                                    selectPageSelectControl.TopByPage=topPosition+20
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)
                        }
                        if(DscselectedSignatureposition === 'Bottom-Left'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=20
                                    selectPageSelectControl.LeftByPage=20
                                    selectPageSelectControl.Top=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
                                    selectPageSelectControl.TopByPage=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)
                        }
                        if(DscselectedSignatureposition === 'Bottom-Right'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', '')) - 30)-122
                                    selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', '')) - 30)-122
                                    selectPageSelectControl.Top=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
                                    selectPageSelectControl.TopByPage=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)

                            
                        }
                        if(DscselectedSignatureposition === 'Bottom-Center'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', '')) - 122)/2
                                    selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', '')) - 122)/2
                                    selectPageSelectControl.Top=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
                                    selectPageSelectControl.TopByPage=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 20)-65
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)

                        }
                        if(DscselectedSignatureposition === 'Middle-Left'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=20
                                    selectPageSelectControl.LeftByPage=20
                                    selectPageSelectControl.Top=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 65)/2
                                    selectPageSelectControl.TopByPage=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 65)/2
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)
                        }
                        if(DscselectedSignatureposition === 'Middle-Right'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', '')) - 30)-122
                                    selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', '')) - 30)-122
                                    selectPageSelectControl.Top=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 65)/2
                                    selectPageSelectControl.TopByPage=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 65)/2
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)
                        }
                        if(DscselectedSignatureposition === 'Middle-Center'){
                            let topPosition = 0
                            for (let index = 1; index <= subNumPages; index++) {
                                if (subNumPages == index) {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    selectPageSelectControl.Left=(Number(pagerefrence.style.width.replace('px', '')) - 122)/2
                                    selectPageSelectControl.LeftByPage=(Number(pagerefrence.style.width.replace('px', '')) - 122)/2
                                    selectPageSelectControl.Top=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 65)/2
                                    selectPageSelectControl.TopByPage=topPosition+(Number(pagerefrence.style.height.replace('px', '')) - 65)/2
                                    break;
                                    
                                } else {
                                    const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                                    topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                                }
                            }
                                selectPageSelectControl.id=newId;
                                selectPageSelectControl.isNewControl=true;
                                        selectPageSelectControl.AdControlId = -1
                                        selectPageSelectControl.CtrlId = newId
                                        selectPageSelectControl.PageNumber = subNumPages
                                        selectedRecipient.SelectPage=Dscselectedpage
                                        selectedRecipient.SignaturePosition=DscselectedSignatureposition
                                        selectPageSelectControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                                        DraggableControls.push(selectPageSelectControl)
                                        setDraggableControls([...DraggableControls])
                                        setAttachImageFile('')
                                        setFileName('')
                                        setshowControlAddedbySelectPage(true)

                        }
                       
            }
        }
        else if(Dscselectedpage === "ALL"){
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].RecipientId  == selectPageSelectControl.RecipientId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }
            for (let page = 1; page <= subNumPages; page++) {
                if (selectPageSelectControl.RecipientPageNumbers.includes(page)) {
                if (!selectPageSelectControl.GroupId) {
                    selectPageSelectControl.GroupId = groupId;
                }
               
                    let topPosition = 0
                    let allselectedControl = { ...selectPageSelectControl }
                    let newId = generateUQID(allselectedControl.Code, 8, 'string');
                    allselectedControl.PageNumber = page
                    for (let index = 1; index <= page; index++) {
                        if (page == index) {
                             //pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            allselectedControl.AdControlId = -1
                            allselectedControl.CtrlId = newId
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            allselectedControl.Left=DscselectedSignatureposition == "Top-Left"?20:
                            DscselectedSignatureposition == "Top-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Top-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Bottom-Left"?20:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Middle-Left"?20:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:""

                            allselectedControl.LeftByPage=DscselectedSignatureposition == "Top-Left"?20:
                            DscselectedSignatureposition == "Top-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Top-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Bottom-Left"?20:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Middle-Left"?20:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:""
                            allselectedControl.Top=DscselectedSignatureposition == "Top-Left"||DscselectedSignatureposition == "Top-Right"||DscselectedSignatureposition == "Top-Center" ?20:
                            DscselectedSignatureposition == "Bottom-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Middle-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)/2:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:""
                            allselectedControl.TopByPage=DscselectedSignatureposition == "Top-Left"||DscselectedSignatureposition == "Top-Right"||DscselectedSignatureposition == "Top-Center" ?20:
                            DscselectedSignatureposition == "Bottom-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Middle-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 65)/2:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:""
                            //const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + allselectedControl.TopByPage)) {
                                allselectedControl.Top = topPosition - 15
                                allselectedControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                            } else {
                                allselectedControl.Top = topPosition + allselectedControl.TopByPage
                            }
                            if (Number(pagerefrence.style.width.replace('px', '')) < allselectedControl.LeftByPage) {
                                allselectedControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                                allselectedControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                            }
                            selectedRecipient.SelectPage=Dscselectedpage
                            selectedRecipient.SignaturePosition=DscselectedSignatureposition
                            allselectedControl.DSCCoordinates=`${allselectedControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-allselectedControl.TopByPage)},${allselectedControl.Left+allselectedControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(allselectedControl.TopByPage+allselectedControl.Height))}`
                            DraggableControls.push(allselectedControl)
                            break;
                        } else {
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                    setDraggableControls([...DraggableControls])
                    setAttachImageFile('')
                    setFileName('')
                    setshowControlAddedbySelectPage(true)
                }
                

            }
        }
        else if(Dscselectedpage === "EVEN"){
           
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].RecipientId  == selectPageSelectControl.RecipientId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }
            
            for (let page = 1; page <= subNumPages; page++) {
                if (page % 2 == 0) {
                if (selectPageSelectControl.RecipientPageNumbers.includes(page)) {
                if (!selectPageSelectControl.GroupId) {
                    selectPageSelectControl.GroupId = groupId;
                }
               
                    let topPosition = 0
                    let allselectedControl = { ...selectPageSelectControl }
                    let newId = generateUQID(allselectedControl.Code, 8, 'string');
                    allselectedControl.PageNumber = page
                    for (let index = 1; index <= page; index++) {
                        if (page == index) {
                             //pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            allselectedControl.AdControlId = -1
                            allselectedControl.CtrlId = newId
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            allselectedControl.Left=DscselectedSignatureposition == "Top-Left"?20:
                            DscselectedSignatureposition == "Top-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Top-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Bottom-Left"?20:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Middle-Left"?20:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:""

                            allselectedControl.LeftByPage=DscselectedSignatureposition == "Top-Left"?20:
                            DscselectedSignatureposition == "Top-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Top-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Bottom-Left"?20:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Middle-Left"?20:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:""
                            allselectedControl.Top=DscselectedSignatureposition == "Top-Left"||DscselectedSignatureposition == "Top-Right"||DscselectedSignatureposition == "Top-Center" ?20:
                            DscselectedSignatureposition == "Bottom-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Middle-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)/2:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:""
                            allselectedControl.TopByPage=DscselectedSignatureposition == "Top-Left"||DscselectedSignatureposition == "Top-Right"||DscselectedSignatureposition == "Top-Center" ?20:
                            DscselectedSignatureposition == "Bottom-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Middle-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 65)/2:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:""
                            //const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + allselectedControl.TopByPage)) {
                                allselectedControl.Top = topPosition - 15
                                allselectedControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                            } else {
                                allselectedControl.Top = topPosition + allselectedControl.TopByPage
                            }
                            if (Number(pagerefrence.style.width.replace('px', '')) < allselectedControl.LeftByPage) {
                                allselectedControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                                allselectedControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                            }
                            selectedRecipient.SelectPage=Dscselectedpage
                            selectedRecipient.SignaturePosition=DscselectedSignatureposition
                            allselectedControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                            DraggableControls.push(allselectedControl)
                            break;
                        } else {
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                    setDraggableControls([...DraggableControls])
                    setAttachImageFile('')
                    setFileName('')
                    setshowControlAddedbySelectPage(true)
                }
               }

            }
            
        }
        else if(Dscselectedpage === "ODD"){
            for (var i = DraggableControls.length - 1; i >= 0; i--) {
                if (DraggableControls[i].RecipientId  == selectPageSelectControl.RecipientId || DraggableControls[i].CtrlId == selectPageSelectControl.CtrlId) {
                    if (DraggableControls[i].AdControlId) {
                        DraggableControls[i].IsDelete = 1
                    } else {
                        DraggableControls.splice(i, 1)
                    }
                }
            }
           
            for (let page = 1; page <= subNumPages; page++) {
                if (page % 2 !== 0) {
                if (selectPageSelectControl.RecipientPageNumbers.includes(page)) {
                if (!selectPageSelectControl.GroupId) {
                    selectPageSelectControl.GroupId = groupId;
                }
               
                    let topPosition = 0
                    let allselectedControl = { ...selectPageSelectControl }
                    let newId = generateUQID(allselectedControl.Code, 8, 'string');
                    allselectedControl.PageNumber = page
                    for (let index = 1; index <= page; index++) {
                        if (page == index) {
                             //pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            allselectedControl.AdControlId = -1
                            allselectedControl.CtrlId = newId
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            allselectedControl.Left=DscselectedSignatureposition == "Top-Left"?20:
                            DscselectedSignatureposition == "Top-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Top-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Bottom-Left"?20:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Middle-Left"?20:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:""

                            allselectedControl.LeftByPage=DscselectedSignatureposition == "Top-Left"?20:
                            DscselectedSignatureposition == "Top-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Top-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Bottom-Left"?20:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:
                            DscselectedSignatureposition == "Middle-Left"?20:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.width.replace('px', ''))-30)-122:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.width.replace('px', ''))-122)/2:""
                            allselectedControl.Top=DscselectedSignatureposition == "Top-Left"||DscselectedSignatureposition == "Top-Right"||DscselectedSignatureposition == "Top-Center" ?20:
                            DscselectedSignatureposition == "Bottom-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Middle-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)/2:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:""
                            allselectedControl.TopByPage=DscselectedSignatureposition == "Top-Left"||DscselectedSignatureposition == "Top-Right"||DscselectedSignatureposition == "Top-Center" ?20:
                            DscselectedSignatureposition == "Bottom-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Bottom-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 20)-65:
                            DscselectedSignatureposition == "Middle-Left"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:
                            DscselectedSignatureposition == "Middle-Right"?(Number(pagerefrence.style.height.replace('px', '')) - 65)/2:
                            DscselectedSignatureposition == "Middle-Center"?(Number(pagerefrence.style.height.replace('px', '')) - 122)/2:""
                            //const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            if ((Number(pagerefrence.style.height.replace('px', '')) + topPosition) < (topPosition + allselectedControl.TopByPage)) {
                                allselectedControl.Top = topPosition - 15
                                allselectedControl.TopByPage = Number(pagerefrence.style.height.replace('px', '')) - 15
                            } else {
                                allselectedControl.Top = topPosition + allselectedControl.TopByPage
                            }
                            if (Number(pagerefrence.style.width.replace('px', '')) < allselectedControl.LeftByPage) {
                                allselectedControl.left = Number(pagerefrence.style.width.replace('px', '')) - 15
                                allselectedControl.LeftByPage = Number(pagerefrence.style.width.replace('px', '')) - 15
                            }
                            selectedRecipient.SelectPage=Dscselectedpage
                            selectedRecipient.SignaturePosition=DscselectedSignatureposition
                            allselectedControl.DSCCoordinates=`${selectPageSelectControl.Left},${(Number(pagerefrence.style.width.replace('px', ''))-selectPageSelectControl.TopByPage)},${selectPageSelectControl.Left+selectPageSelectControl.Width},${(Number(pagerefrence.style.width.replace('px', ''))-(selectPageSelectControl.TopByPage+selectPageSelectControl.Height))}`
                            DraggableControls.push(allselectedControl)
                            break;
                        } else {
                            const pagerefrence = document.getElementsByClassName('react-pdf__Page')[index - 1].firstChild
                            topPosition = topPosition + Number(pagerefrence.style.height.replace('px', ''))
                        }
                    }
                    setDraggableControls([...DraggableControls])
                    setAttachImageFile('')
                    setFileName('')
                    setshowControlAddedbySelectPage(true)
                }
                
            }
            }
           
        }
    }
    function handleDscLevelChange(type,selectedRecipient){
       if(type==="DOCLEVEL"){
        setDscLevelofAccess("DOCLEVEL")
        //selectedRecipient?.TypeLevelAccess="DOCLEVEL"
       }
       if(type==="PAGELEVEL"){
        setDscLevelofAccess("PAGELEVEL")
        //selectedRecipient?.TypeLevelAccess="PAGELEVEL"
       }
    }
    function getMessage() {
        return <p> <b>{selectedControl.FullName}</b> has no permissions for these pages
            {nonPermitPagesArray?.map((pagenumber, index) => {
                if (index == (nonPermitPagesArray.length - 1)) {
                    return <> {pagenumber}</>
                }
                else {
                    return <> {pagenumber}, </>
                }
            })}
        </p>
    }


    return (
        <>
            <div className="dashboard_home wrapper_other" id="wrapper_other" tabIndex={0} onKeyDown={handleKeyDown}>
                <Sidebar />

                <div className="main other_main" id="main">
                    <Topbar />
                    <div className="main-content wizard">
                        <div className='row'>
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div id="wizard_step">
                                    <div className="step-wizard" role="navigation">
                                        <ul>
                                            <li className={count === 0 || count < 4 ? 'active done' : ''}>
                                                <button>
                                                    <div className="step">1</div>
                                                    <div className="title">Add</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 1 || (count < 4 && count != 0) ? 'active done' : ''}>
                                                <button id="step2">
                                                    <div className="step">2</div>
                                                    <div className="title">Select</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 2 || (count < 4 && count != 1 && count != 0) ? 'active done' : ''}>
                                                <button id="step3">
                                                    <div className="step">3</div>
                                                    <div className="title">Prepare</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                            <li className={count === 3 ? 'active done' : ''}>
                                                <button id="step4">
                                                    <div className="step">4</div>
                                                    <div className="title">Review</div>
                                                </button>
                                                <div className="progressbar"></div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className='wizard-panel' id="wizard-pane-3" >
                                        <div className='content'>
                                            <div className='prepare_document'>
                                                <div className='row'>
                                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                        <h2 className='document-select-overflowdots'>Document Title: <span data-tip data-for='title'>{step3Data?.Title}</span></h2>
                                                        <ReactTooltip id="title" place="top" effect="solid">
                                                           {step3Data?.Title}</ReactTooltip>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-xl-3 col-lg-5 col-md-5 col-sm-12'>
                                                        <div className='top_sec'>
                                                            <select className='hlfinput' style={{ marginBottom: '20px' }} onChange={(event) => { getDropRecipient(event) }}>
                                                                {(step3Data && step3Data.Recipients) ? step3Data?.Recipients.map((obj, index) => { return <option value={obj.RecipientId} key={obj?.FullName + index + obj?.EmailAddress}>{obj.FullName}</option> }) : null}
                                                            </select>
                                                        </div>
                                                        {IsanyoneenableDsc !== true?<div className='list_item'>
                                                            <h2>Select input</h2>
                                                            <ul>
                                                                {/* className='active' */}
                                                                {controlTypes ? controlTypes.map((item, index) => {
                                                                    item.ImagePreview ='src/assets/controlimages/' + item.Code + '.svg'
                                                                    return <>
                                                                        {(selectedRecipient?.IsOwner && (item.Code == 'seal')) || (item.Code == 'digitalsignature' || item.Code == 'aadhaar' || item.Code == 'payment' || item.Code == 'seal' || (!selectedRecipient?.IsNotarizer && item.Code == 'seal'))  ? null :
                                                                            <li key={item.CtrlId} className={(selectedRecipient && !selectedRecipient.IsSignatory && validControls(item)) ? 'control_draggable control_disable' : 'control_draggable'} onDragStart={(event) => { drag(event, item) }}
                                                                                onDragEnd={(event) => { addNewRecipControlRecord(event, item) }} onDrag={(event) => { handleOnDrag(event, item) }} >
                                                                                <button id={item.CtrlId} draggable={(selectedRecipient && !selectedRecipient.IsSignatory && validControls(item)) ? false : true}
                                                                                    className={(selectedRecipient && !selectedRecipient.IsSignatory && validControls(item)) ? 'btn control_draggable control_disable' : 'btn control_draggable'}>
                                                                                        <span className={item.IconClass+ '-drag'} id={item.IconClass+'-drag'}></span>
                                                                                    <img id={item.CtrlId}  src={item.IconClass ? ('src/assets/controlimages/' + item.Code + '.svg') : 'src/images/signulu_small_black_logo.svg'}
                                                                                        onError={(e) => {
                                                                                            e.target.src = 'src/images/signulu_small_black_logo.svg'
                                                                                        }} alt="" />
                                                                                    {item.DisplayName}</button>
                                                                            </li>}
                                                                    </>
                                                                }) : null}
                                                            </ul>
                                                        </div>:
                                                        <div 
                                                        // style={{ display: "flex", flexDirection: "column", alignContent: "center" }}
                                                        >
                                                            <div className="form-group top_wrrp" style={{ marginBottom: "30px" }}>
                                                                <label style={{marginBottom: '0.5rem'}} htmlFor="selectPage">Select Pages</label>
                                                                <div className="input-wrapper" style={{maxWidth: '250px'}}>
                                                                    <select name="selectPage" id="selectPage" value={selectedRecipient?.SelectPage !==null?selectedRecipient?.SelectPage:Dscselectedpage} onChange={(event) => {setDscselectedpage(event.target.value)}} 
                                                                        className="hlfinput">
                                                                        <option value={''} >Select</option>
                                                                        {Selectpagelist.map(item=>{
                                                                            return(
                                                                                <option selected={Dscselectedpage===item.SelectPage?true:false} value={item.SelectPage}>{item.SelectPageName}</option>
                                                                            )
                                                                        })}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="form-group top_wrrp" style={{ marginBottom: "30px" }}>
                                                                <label style={{marginBottom: '0.5rem'}} htmlFor="selectPage">Signature Position</label>
                                                                <div className="input-wrapper" style={{maxWidth: '250px'}}>
                                                                    <select name="selectPage" id="selectPage" value={selectedRecipient?.SignaturePosition !==null?selectedRecipient?.SignaturePosition: DscselectedSignatureposition} onChange={(event) => {setDscselectedSignatureposition(event.target.value)}} 
                                                                        className="hlfinput">
                                                                        <option value={''} >Select</option>
                                                                        {dscsignPositionlist.length>0&&dscsignPositionlist.map(item=>{
                                                                            return(
                                                                                <option value={item.SignaturePosition}>{item.SignaturePostionName}</option>
                                                                            )
                                                                        })}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            {/* <div className="form-group top_wrrp" style={{ marginBottom: "30px" }}>
                                                                <label style={{marginBottom: '0.5rem'}} htmlFor="selectPage">Co-ordinates</label>
                                                                <div className="input-wrapper" style={{maxWidth: '250px'}}>
                                                                <input class="form-control" type="text" placeholder={`${DraggableControls.map(item=>{if(item.id== selectedRecipient.Controls[0]){
                                                                    return item.DSCCoordinates
                                                                }})}`}readonly></input>
                                                                </div>
                                                            </div> */}
                                                           {(showDscLevelofAccess === true || selectedRecipient?.TypeLevelAccess !==null) ? <div>
                                                            <div class="form-check" style={{ marginBottom: "20px" }}>
                                                <input class="form-check-input" type="radio" id="SIGNATURESETTINGS1" name={"SIGNATURESETTINGS"}
                                                onClick={()=>handleDscLevelChange("DOCLEVEL",selectedRecipient)}
                                                checked={DscLevelofAccess === "DOCLEVEL"|| selectedRecipient?.TypeLevelAccess ==="DOCLEVEL"?true:false}
                                                />
                                                <label class="form-check-label" for="SIGNATURESETTINGS1">
                                                Document Level
                                                </label>
                                            </div>
                                            <div class="form-check" style={{ marginBottom: "30px" }}>
                                                <input class="form-check-input" type="radio" id="SIGNATURESETTINGS1" name={"SIGNATURESETTINGS"}
                                                 onClick={()=>handleDscLevelChange("PAGELEVEL",selectedRecipient)}
                                                 checked={DscLevelofAccess === "PAGELEVEL"|| selectedRecipient?.TypeLevelAccess ==="PAGELEVEL"?true:false}
                                                />
                                                <label class="form-check-label" for="SIGNATURESETTINGS1">
                                                Page Level
                                                </label>
                                            </div>
                                            </div>:""}
                                             <button className="btn delete_control" onClick={() => {setshowDscLevelofAccess(true), addDSCControltodocument(),setDscLevelofAccess("DOCLEVEL")}}><b>Apply</b></button>
                                               </div>
                                                }
                                                    </div>
                                                    <div className='col-xl-9 col-lg-7 col-md-7 col-sm-12'>
                                                        <div className='prepare-slide_pln prepare-slide_pln2 overflow-documents'  id="scrollArea"
                                                            onDrop={(event, item) => { drop(event, item) }} onDragOver={(event) => { allowDrop(event) }}>
                                                            <Document className="row box"
                                                                loading={loadSpinner}
                                                                noData={''}
                                                                file={documentFile}
                                                                onLoadSuccess={subDocumentsLoadSuccess}>
                                                                {DraggableControls.map(control => {
                                                                    return (
                                                                        <>
                                                                            {(control.Code == "signature" && !control.IsDelete) ? 
                                                                                <div key={control.code + control?.CtrlId}><Signature IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd}
                                                                                    OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} setSignaturePopShow={setSignaturePopShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "date" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <DateSigned IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "payment" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Payment IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "seal" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <NotarySeal IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "text" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <TextBox IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient} /></div> : null}
                                                                            {(control.Code == "email" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Email IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "username" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Name IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "companyname" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Company IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "image" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <ImageSeal IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setAttachImageControlSetting={changeAttachControlEdit} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "aadhaar" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Aadhaar IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "digitalsignature" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <DigitalSignature IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={true} onChangeValue={controlOnChangeValue}  selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == 'dob' && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Dob IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient} /></div> : null}
                                                                            {(control.Code == "attachment" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Attachment IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setAttachImageControlSetting={changeAttachControlEdit} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "companyaddress" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <CompanyAddress IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "useraddress" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <UserAddress IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "initial" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Initial IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} setSignaturePopShow={setSignaturePopShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                            {(control.Code == "checkbox" && !control.IsDelete) ? <div key={control.code + control?.CtrlId}>
                                                                                <Checkbox IsDragDisabled={false} ControlObj={control} OnDragEnd={controlDragEnd} OnResizeEnd={controlResizeEnd} IsResizeDisabled={false} onChangeValue={controlOnChangeValue} setControlPopupShow={setControlPopupShow} selectedControlObj={selectedControl} deleteHandler={deleteHandler} selectedRecipientObj={selectedRecipient}/></div> : null}
                                                                        </>
                                                                    )
                                                                })}

                                                                {subNumPages ? <>
                                                                    {Array.apply(null, Array(subNumPages))
                                                                        .map((x, i) => i + 1).sort((a, b) => a - b)
                                                                        .map((page, index) => {
                                                                            return <Page key={page} scale={IsanyoneenableDsc === true?1:1.5} id={page} noData={''}  onRenderSuccess={(e) => {getOnRenderSuccess(e,index, page)}} ref={pageRef} className="document_prepare" pageNumber={page} />
                                                                        })}
                                                                </>
                                                                    : null}
                                                            </Document>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                        <div className="slide-tab-btn">
                                                            {count > 0 && <button className="btn prev-btn" onClick={() => { back(), localStorage.setItem("addDocumentPage", count - 1) }}>BACK</button>}
                                                            {count <= 3 && <button className="btn next-btn" onClick={() => { getNext(), localStorage.setItem("addDocumentPage", count + 1) }}>NEXT</button>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={signaturePopShow} onHide={() => { setSignaturePopShow(false)}}
                size="lg"
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                    Add Signature
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{height:'355px',overflow: "hidden auto"}} >
                <SignatureControlEditPopup ref={childCompRef} setSelectedSignatureControlEditData={setSelectedSignatureControlEditData}  isPageChange={setIsSelectChanged} getSelectedPage={setPopUpSelectedPage}
                 selectedcontrolObj={selectedControl} deleteHandler={deleteHandler} selection={selection} setSelection={setSelection} errorStatus={errorStatus} setErrorStatus={setErrorStatus} />
                </Modal.Body>
                <Modal.Footer style={{justifyContent: 'center'}}>
                <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#d10d03', border: 'none', lineHeight: '2'}} onClick={() => {  deleteHandler() }}>DELETE</Button>
                    <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#F06B30', border: 'none', lineHeight: '2'}} onClick={() => {  childCompRef.current.clear() }}>RESET</Button>
                    {/* <p>Legal representation of my initials</p> */}
                    <Button variant="secondary" style={{background: 'rgb(36, 99, 209)', border: 'none', lineHeight: '2'}} onClick={() => {  signSelectionHandler()}}>APPLY</Button>
                </Modal.Footer>
            </Modal>

            <Modal show={controlPopupShow}  onHide={() => { setControlPopupShow(false) }}
                size=""
                dialogClassName="modal_custom_control_setting"
                aria-labelledby="contained-modal-title-vcente"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcente">
                        {selectedControl.DisplayName}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{overflow: "hidden auto"}} >
                       <ControlEditPopup setControlPopupShow={setControlPopupShow}
                        selectedcontrolObj={selectedControl} isPageChange={setIsSelectChanged} getSelectedPage={setPopUpSelectedPage}
                        deleteHandler={deleteHandler}
                        setSelectedControlObj={setSelectedControl}  
                        />
                </Modal.Body>
                <Modal.Footer style={{justifyContent: 'center'}}>
                    {/* <Button variant="primary" onClick={() => { setDocmodalShow(false) }}>Submit</Button> */}
                    {/* <p>I understand this is legal representation of my initials</p> */}
                    <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#d10d03', border: 'none', lineHeight: '2'}} onClick={() => {  deleteHandler() }}>DELETE</Button>
                    <Button variant="secondary" style={{background: 'rgb(36, 99, 209)', border: 'none', lineHeight: '2'}} onClick={() => {otherSelectionHandler() }}>APPLY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={mandatoryNotExist} onHide={() => { setMandatoryNotExist(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body style={{ textAlign: 'center' }}>
                    <h3 >Error Log</h3>
                    <br />
                    <div id="prepare_error_table-wrapper">
                     <div id="prepare_error_table-scroll">
                        <table style={{ border: '1px solid black' }} className='table table-bordered'>
                           
                        <thead> <tr> 
                                <th>Error</th>
                                <th>Recipient</th>
                            </tr>
                            </thead>
                            <tbody>
                            {contronotExistRespArray.map((obj, index) => {
                                 return <tr index={index} key={obj?.RecipientId} style={{overflow:'auto'}}>
                                <td style={{ borderWidth: '1px' }}>
                                {(!obj.HasSignatoryField && obj.IsSignatory) ? 
                                "The recipient is set as a Signatory. Hence, adding the Signature control is mandatory. Please add Signature control to proceed further." : null}
                                {(!obj.HasNotarizerField && obj.IsNotarizer) ?  
                                'Notary controls (username , seal ) are missing'
                                : null}
                                </td>
                              
                                <td>{obj?.FullName}</td>

                            </tr>
                                 })}
                                 </tbody>
                        </table>
                    </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    {/* <Button variant="secondary" onClick={() => { setModalShow(false), setDeleteRecipient({}) }}>Cancel</Button> */}
                    <Button className="button_modal_ok" onClick={() => { setMandatoryNotExist(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={uploadPopShow} onHide={() => { setUploadPopShow(false) }}
                size="lg"
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton style={{}}>
                    {isControlImage ? <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                        Upload the image
                    </Modal.Title>: <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    Upload the attachment
                    </Modal.Title>}
                </Modal.Header>
                <Modal.Body style={{ height: '350px', overflow: "hidden auto" }}>
                {failedMessage ?
                        <div className='card' style={{ marginBottom: '10px', padding: '10px', color: 'red' }}>
                            <div>
                                <button type="button" className="btn-close" aria-label="Close" style={{ float: 'right', zoom: '0.6' }} onClick={() => { handleControlErrorClick() }}></button>
                            </div>
                            {isControlImage ? <div >
                                <p style={{ lineHeight: '1.5', fontSize: '14px' }}>The maximum size of file is 1MB</p>
                                <p style={{ lineHeight: '1.5', fontSize: '14px' }}>Only the following file formats are acceptable: .jpeg, .jpeg, .png</p>
                            </div> : null
                            }
                            {!isControlImage ? <div >
                                <p style={{ lineHeight: '1.5', fontSize: '14px' }}>The maximum size of file is 2MB</p>
                                <p style={{ lineHeight: '1.5', fontSize: '14px' }}>Only the following file formats are acceptable: .jpeg, .jpeg, .png, .pdf</p>
                            </div> : null
                            }
                        </div> : null}
                    <div style={{ display:'flex'}}>
                    <div className="selectIcons_Prepare">
                <div className="signSideBar_Prepare">
                    <div style={{ cursor: "pointer", borderLeft: attachmentSelection.upload ? "5px solid #4673d2" : "none" }}>
                        <UploadIcon className='item_Prepare' data-tip data-for="upload" style={{ cursor: "pointer" }} onClick={() => {
                             setAttachmentSelection({upload:true,manage:false}), setTimeout(() => { getTags() }, 500); }} />
                        <ReactTooltip id="upload" place="top" effect="solid" >
                            Upload
                        </ReactTooltip>
                    </div>
                   <div style={{ cursor: "pointer", borderLeft: attachmentSelection.manage ? "5px solid #4673d2" : "none" }}>
                        <SettingsIcon className='item_Prepare' data-tip data-for="setting" style={{ cursor: "pointer" }} onClick={() => { setAttachmentSelection({upload:false,manage:true})}} />
                        <ReactTooltip id="setting" place="top" effect="solid" >
                            Manage
                        </ReactTooltip>
                    </div>
                </div>
            </div>
                   { attachmentSelection.upload ? <div className='card' style={{ width: '100%'}}>
                        <div className='chucksum' style={{ justifyContent: 'center', display: 'flex', minHeight: '300px', width: '100%' }}>
                            {!IsFileUploaded ? <div className='file-add file-upload use_templete drop-zone' style={{ outline: 'none', width: '300px', height: '300px' }}>
                                <div className="drop-zone__prompt">
                                    <input type="file" onClick={(event) => { event.target.value = null }} accept={acceptedFiles} name="myFile" className="drop-zone__input dragUpload" />
                                </div>
                                <div className="file-add-img-wrapper">
                                    <img src="./src/images/file-add.svg" alt="file-add" className='file-add-img' style={{ height: '150%' }} />
                                </div>
                                <p className='file-add-text'>Drop your files here or</p>
                                <div style={{ cursor: 'pointer', position: 'relative' }}>
                                    <button className='file-upload-btn' onClick={(e) => {}}>upload
                                        <span className='file-add-btn-arr'><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M5 10l7-7m0 0l7 7m-7-7v18" />
                                        </svg>
                                        </span>
                                    </button>
                                </div>
                            </div> : null}
                            {IsFileUploaded ?
                                <div>
                                    {(attachImageFile && !failedMessage && isImageFile) ?
                                        <div style={{ justifyContent: 'center' }}>
                                            {/* <div>
                                                <button type="button" className="btn-close" aria-label="Close" style={{ float: 'right', zoom: '0.6' }} onClick={() => { clearFileInfo() }}></button>
                                            </div> */}
                                            <p style={{ marginRight: '10px', padding: '0.5rem' }}> {fileName}</p>
                                            {/* <div className='pop-doc' > */}
                                                <img style={{ padding: '0.5rem' }} src={attachImageFile}></img>
                                            {/* </div> */}
                                        </div> : null}
                                    {!isImageFile && attachImageFile && !failedMessage ?
                                        <div style={{ justifyContent: 'center' }}>
                                            {/* <div>
                                                <button type="button" className="btn-close" aria-label="Close" style={{ float: 'right', zoom: '0.6' }} onClick={() => { clearFileInfo() }}></button>
                                            </div> */}
                                            <p style={{ marginRight: '10px', padding: '0.5rem' }}> {fileName}</p>
                                            <div className='pop-doc' >
                                                <Document className="row"
                                                    file={attachImageFile}
                                                    noData={''}
                                                    loading={loadSpinner}
                                                    onLoadSuccess={ControlpopDocLoad}>
                                                    {Array.apply(null, Array(controlPopDocPages))
                                                        .map((x, i) => i + 1)
                                                        .map(page =>
                                                            <>
                                                                <Page noData={''} scale={1.5} className="subdocument_intial mx-auto d-block" pageNumber={page} />
                                                                <span className='pagenumber-flex'>page {page} of {controlPopDocPages}</span>
                                                            </>
                                                        )}
                                                </Document>
                                            </div>

                                        </div> : null}
                                </div> : null}
                        </div>
                    </div> : null}
                    {attachmentSelection.manage ?
                <div className="popup_setting">
                    <div 
                    // style={{ display: "flex", flexDirection: "column", alignContent: "center" }}
                    >
                        <div className="form-group top_wrrp" style={{ marginBottom: "30px" }}>
                            <label style={{marginBottom: '0.5rem'}} htmlFor="selectPage">Select Pages</label>
                            <div className="input-wrapper" style={{maxWidth: '250px'}}>
                                <select name="selectPage" id="selectPage" value={SelectedPage} onChange={getSelectPage}
                                    className="hlfinput">
                                    <option value={''} >Select</option>
                                    <option value={'FIRST'}>First page</option>
                                    <option value={'LAST'}>Last page</option>
                                    <option value={'ODD'}>Odd pages</option>
                                    <option value={'EVEN'}>Even pages</option>
                                    <option value={'ALL'}>All pages</option>
                                </select>
                            </div>
                        </div>
                        {/* <button className="btn delete_control" style={{background: 'rgb(36, 99, 209)'}}><b>Apply</b></button> */}
                    </div>
                    <div 
                    style={{ display: "flex", flexDirection: "column", alignContent: "center", }}
                    >
                        <div  style={{margin: '3px'}}
                        // style={{ marginBottom: "40px", marginTop: "10px", display: "flex", justifyContent: "center" }}
                        >
                        <input type="checkbox" checked={MandatoryCheck} onChange={getMandatory}></input>
                        <label style={{ marginLeft: "6px" }}> Mandatory</label></div>
                        {/* <button className="btn delete_control" style={{ fontWeight: '300' }} onClick={deleteHandler}><b>Delete Control</b></button> */}
                    </div>
                </div> : null}
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}>
<Button variant="primary" className="btn next-btn sign_clear"style={{background: '#d10d03', border: 'none', lineHeight: '2'}} onClick={() => {  deleteHandler() }}>DELETE</Button>
                    <Button variant="primary" className="btn next-btn sign_clear"style={{background: '#F06B30', border: 'none', lineHeight: '2'}} onClick={() => {clearFileInfo() }}>CLEAR</Button>
                    <Button variant="secondary" style={{background: 'rgb(36, 99, 209)', border: 'none', lineHeight: '2'}} onClick={() => { UploadImageAttachment()}}>APPLY</Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showControlAddedbySelectPage} onHide={() => {  setshowControlAddedbySelectPage(false) }}
                size=""
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header closeButton style={{ }}>
                    <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                    Success
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: '200px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card ">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div" style={{marginBottom:'10px'}}>
                                        <p style={{ fontSize: '16px',lineHeight: '1.5'}}>All the controls are placed successfully based on selection.
                                         You can change the position of the controls by dragging them.</p>
                                         <br/>
                                         {(nonPermitPagesArray && nonPermitPagesArray.length>0) ?  getMessage() : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}>
                    <Button style={{ width: '7rem'}} onClick={() => { setshowControlAddedbySelectPage(false) }} className='submit_send_btn'
                    >OKAY</Button>
                </Modal.Footer>
            </Modal>

        </>
    )
}

export { DocumentPrepare }; 