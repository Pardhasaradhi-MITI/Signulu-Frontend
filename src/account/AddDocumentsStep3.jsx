import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';




function AddDocumentsStep3() {
    //Owl
    const options2 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };
    const options3 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };

    return (
        <div className='step wizard-panel' style={{ display: "none" }}>
            <div className='content'>
                <div className='prepare_document'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <h2>Prepare document <span>For Tom Hanks</span></h2>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-3 col-lg-5 col-md-5 col-sm-12'>
                            <div className='top_sec'>
                                <select className='hlfinput'>
                                    <option>James Franco</option>
                                </select>
                            </div>
                            <div className='list_item'>
                                <h2>Select input</h2>
                                <ul>
                                    <li className='active'>
                                        <button class="btn"><img src="src/images/signulu_small_black_logo.svg" alt="" /> Signature</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/initial.svg" alt="" /> Initial</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/digital-signature.svg" alt="" /> Date Signed</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/text-box.svg" alt="" /> Textbox</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/email.svg" alt="" /> Email</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/checkbox.svg" alt="" /> Checkbox</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/name.svg" alt="" /> Name</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/company.svg" alt="" /> Company</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/image-seal.svg" alt="" /> Image/Seal</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/digital-signature.svg" alt="" /> Digital Signature (Dsc)</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/attachment.svg" alt="" /> Attachment</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/user-address.svg" alt="" /> User Address</button>
                                    </li>
                                    <li>
                                        <button class="btn"><img src="src/images/company-address.svg" alt="" /> Company Address</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className='col-xl-9 col-lg-7 col-md-7 col-sm-12'>
                            <div className='prepare-slide_pln'>
                                <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options2.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true}>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                </OwlCarousel>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="slide-tab-btn">
                                <button className="btn prev-btn">BACK</button>
                                <button className="btn cncl-btn">cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { AddDocumentsStep3 }; 