import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function StandardPopup({ history, location }) {
    return (
        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content">
                    <div className="common_popup">
                        <div className='content'>
                            <h4>Common Popup</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id porta quam. Phasellus lacinia, justo ac sollicitudin lobortis.</p>
                            <button type="button" class="primry_btn">Get Started</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { StandardPopup }; 