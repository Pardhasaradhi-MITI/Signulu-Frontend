import React from 'react';
import { Link, useHistory} from 'react-router-dom';
import { useEffect, useState} from "react";
import { loginService } from '../_services/login.service';
import Reaptcha from 'reaptcha';
import { useCookies } from 'react-cookie';
import OTPInput from "otp-input-react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useTranslation } from 'react-i18next';


import TermsConditions from "../account/TermsConditions"
import { Button, ButtonToolbar, Modal,ListGroup,ListGroupItem } from 'react-bootstrap';
import { ModelTraining } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import { addDocumentService } from '../_services/adddocument.service';
let prodenv = process.env.NODE_ENV == 'production'?true:false
let validCaptcha = prodenv?'':true
let timer = 0;
let timeState = { time: {}, seconds: 119 }
function Login(){
    const history = useHistory();
    const [state, setState] = useState({ fields: {}, errors: {} });
    const [errorMessage, seterrorMessage] = useState('');
    const [rememberMe, setrememberMe] = useState('');
    const [otpValue, setOtpValue] = useState();
    const [timerOtp,settimerOtp]= useState({time: {}, seconds: 119})
    const [otpError, setOtpError] = useState('');
    const [cookies, setCookie, removeCookie] = useCookies(['Signulu']);
    const { t, i18n } = useTranslation();
    const[showPrivacymodel,setshowPrivacymodel]=useState(false);
    const[showTermsnadconditionsmodel,setshowTermsnadconditionsmodel]=useState(false);
    const[scheduleDemo,setscheduleDemo]=useState(false)
    const[watchDemo,setwatchDemo]=useState(false)
    const[fingerPop,setfingerPop]=useState(false)
    const[disabled,setDisabled]=useState(false)
useEffect(() => {
    if(document && document.body && document.body.classList) {
        document.body.classList.remove('loading-indicator')
    }
    const token = localStorage.getItem('LoginToken');
    if(token){
        history.push("dashboard");
    }
         if (cookies['rememberme']){
        let params = state.fields
        params['username'] = loginService.deCryptedString(cookies['un'])
        params['password'] = loginService.deCryptedString(cookies['pw'])
        setState({
            fields:params,
            errors:{}
        });
       }
       if (localStorage.getItem('lang')) {
        i18n.changeLanguage(localStorage.getItem('lang'))
       }else if (navigator && (navigator.language ||navigator.userLanguage)){
        var userLang = navigator.language.split('-')[0] || navigator.userLanguage.split('-')[0]; 
        i18n.changeLanguage(userLang)
        localStorage.setItem('lang', userLang)
       }
    }, [])
function getRememberMe(event) {
    setrememberMe(event.target.checked)
}

    function setCredentials() {
    if (rememberMe) {
        setCookie('rememberme', rememberMe, {});
        setCookie('un', loginService.credentialCryptedString(state.fields["username"]), {});
        setCookie('pw', loginService.credentialCryptedString(state.fields["password"]), {});
     }
    }

    function handleChange(e) {
        let fields = state.fields
        fields[e.target.name] = e.target.value;
        setState({
            fields
        });
    }
    function sendOTP(e, event) {
        seterrorMessage('');
        setDisabled(true);

        if (e) {
            e.preventDefault();
        }
        loginService.sendOPT(loginService.userValue.Data.user_id).then((data) => {
            if (event == true) {
            
             timeState = { time:  secondsToTime(timeState.seconds), seconds: 119 }
            // console.log(timeState)
              settimerOtp(timeState)
                startTimer();
                toast.success('OTP Resent successfully', {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: false,
                    progress: undefined,
                    });
            } else {
                otpDiv()
            }
        })
        .catch(error => {
        });

    }
    function verifyOTP() {
        if (otpValue && otpValue.length == 6) {
        loginService.verifyOTP(loginService.userValue.Data.user_id, otpValue).then((data) => {
            if(data.Status){
             var srcElement = document.getElementById('otp');
             srcElement.style.display = "none";
             setUserValues()
            } else {
            setOtpError('Entered OTP is invalid')
            }
        })
        .catch(error => {
        });
    } else {
        setOtpError('Please enter six digit valid OTP')
    }
    }
    function setUserValues(){
        loginService.update_currentUser(loginService.userValue.Data);
        loginService.storeLoginTokenKey(loginService.userValue.Token);
        setCredentials()
        let fields = {};
        fields["username"] = "";
        fields["password"] = "";
       prodenv== true? validCaptcha = false:""
        setState({ fields: fields, errors: {} });
        seterrorMessage('');
        var now = new Date();
        now.setTime(now.getTime() + 1 * 3600 * 1000);
        document.cookie = "Token="+ loginService.userValue.Token + ";expires=" + now.toUTCString() + ";path=/";
        // console.log(loginService.userValue.Token , 'loginService.userValue.Token ')
        localStorage.setItem('Logintime', now);
        addDocumentService.getUserPreferences(loginService.userValue.Data.user_id)
        history.push("dashboard");
    }
    function OTPChange (e) {
        setOtpError('')
        setOtpValue(e)
    }
    function submitAuthForm(e) {
        e.preventDefault();
        if (validateForm()) {
         loginService.login(state.fields["username"], state.fields["password"])
                .then((result) => {
                    validDations(result)
                })
                .catch(error => {
                });
        }
    }
    function validateForm() {
        let fields = state.fields
        let errors =  {}
        let formIsValid = true;
        if (!fields["username"]) {
            formIsValid = false;
            errors["username"] = "Email / Contact No is required";
        }
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Password is required";
        }
        if (!validCaptcha && prodenv== true) {
            formIsValid = false;
            errors["recaptcha"] = "Recaptcha is required";
        }
        else{
            errors["recaptcha"] = "";
        }
        setState({
            fields: fields,
            errors: errors
        });
        return formIsValid;
    }

    function otpDiv() {
        timeState = { time:  secondsToTime(timeState.seconds), seconds: 119 }
        settimerOtp(timeState)
        startTimer()
        var srcElement = document.getElementById('otp');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }

    }
    function multiLogin() {
        setDisabled(true);

        var srcElement = document.getElementById('multilogin');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    function multiLoginVerify() {
        loginService.sessionLoginToSignulu(state.fields["username"], state.fields["password"])
        .then((result) => {
            setCredentials()
            var srcElement = document.getElementById('multilogin');
            srcElement.style.display = "none";
            validDations(result)
          });
    }
    function validDations(result) {
        if (result.Status && (result.Entity.Data.IsActive == 1) && result.Entity.Token) {
            loginService.update_subject_user(result.Entity)
            console.log(process.env.NODE_ENV, 'process.env.NODE_ENV')
            // if(process.env.NODE_ENV == 'production') {
            //     sendOTP('', false)
            // } else {
            //     setUserValues()
            // }
            setUserValues()
        } else if (result.Entity.Data.loginCounter == 1) {
            seterrorMessage('You have entered wrong password, only 2 attempt(s) remaining.');
        } else if (result.Entity.Data.loginCounter == 2) {
           // setInterval(seterrorMessage('You have entered wrong password, only 1 attempt(s) remaining.'), 4000)
         seterrorMessage('You have entered wrong password, only 1 attempt(s) remaining.')
        } else if (result.Entity.Data.UserAccountLocked) {
            seterrorMessage('User Account Locked, Please check your email to reset your password');
        
        } else if (!result.Entity.Data.isUserAuthenticated) {
            seterrorMessage('Email/Password is invalid');
        } else {
            if (result.Entity.Data.IsActive == 0 && result.Entity.Token) {
                loginService.storeVerifyMailTokenKey(result.Entity.Token);
                loginService.update_currentUser(result.Entity.Data);
                localStorage.setItem('showInactiveUserMessage', "true");
                setCredentials()
                seterrorMessage('')
                history.push("activatemail");
            } else {
                loginService.storeVerifyMailTokenKey(result.Entity.Token);
                seterrorMessage('')
                multiLogin()
            }
        }
    }
    function SCHEDULE_DEMO(event) {
        event.preventDefault();
          setscheduleDemo(true) 
    }
    function WATCH_DEMO(event) {
        event.preventDefault();
          setwatchDemo(true) 
    }

    

    // function showPrivacymodel(event) {
    //     event.preventDefault();
    //       setshowPrivacymodel(true) 
    // }


    function multiLoginPopClose() {
        setDisabled(false);

        var srcElement = document.getElementById('multilogin');
        srcElement.style.display = "none";
           
    }
    function onCaptchaVerify(recaptchaResponse) {
        validCaptcha = true
        const fields = state.fields
        const errors =  state.errors
            errors["recaptcha"] = "";
        setState({
            fields: fields,
            errors: errors
        });
    };
    function onCaptchaExpire(recaptchaResponse) {
        validCaptcha = false
    }   
  function secondsToTime(secs){
        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);
        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);
        let obj = {
          "m": minutes,
          "s": seconds
        };
        return obj;
      }
    
      function startTimer() {
          if (timer) {
            clearInterval(timer);
            timer = 0
        }
        if (timer == 0 && timeState.seconds > 0) {
          timer =  setInterval(() => {
            countDown() }, 1000);
        }
      }
      function countDown() {
        if ((timeState.seconds - 1) == 0){
            clearInterval(timer);
        }
        timeState = {  time: secondsToTime(timeState.seconds - 1),
            seconds: timeState.seconds - 1 }
            settimerOtp(timeState)
        }

    function alerPop(){
        alertService.info('This feature is under development. We will notify to you once it is ready. Thank you for your support.',2000)

    }

    return (
        <>
        <div>
            <div className="signulu_regi_sec login_sec ">
                <div className="container-fluid ">
                    <div className="row space ">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 left_part">
                            <div className="logo">
                                <img src={"src/images/signulu_logo.svg"} alt="" className="img-fluid mx-auto d-block"  />
                            </div>
                            <h1 className="text-center">{t('PLEASE_LOGIN_TO_YOUR_ACCOUNT')}</h1>
                          
                            <div className="picture">
                                <ul>
                             <li className="img-fluid mx-auto d-block"  style={{cursor:"pointer"}} onClick={()=>setfingerPop(true)} >   <img src="src/images/fingerprint.svg" alt="" /></li>
                             </ul>
                            </div>
                          
                            <h3 className="text-center">{t('LOGIN_USING_FINGER_PRINT')}</h3>
                            <h4 className="text-center">{t('PLACE_USE_YOUR_FINGER_TO_LOGIN')}</h4>
                            <div className="orenq">{t('OR')}</div>
                            {errorMessage ? <span className='error-message'>{errorMessage}</span> : null}
                            <div className="login_form">
                                <h5> {t('LOGIN_USING_USERID_AND_PASSWORD')}</h5>
                               <form method="post"  name="userAuthForm"  onSubmit={submitAuthForm}>
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group">
                                        <div className="input-wrapper">
                                            {/* <input type="text" id="username" name="username" value={(state.fields && state.fields.username) ? state.fields.username : ''} onChange={handleChange} />
                                            <label htmlFor="user">{t('Email')}</label> */}
                                            <input type="text" id="username" name="username"  value={(state.fields && state.fields.username) ? state.fields.username : ''} onChange={handleChange} placeholder='Email Address' />
                                            {state && state.errors && state.errors.username ? <span className='error-message'>{state.errors.username}</span> : null}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group">
                                        <div className="input-wrapper">
                                            {/* <input type="password" id="password" name="password" value={(state.fields && state.fields.password) ? state.fields.password : ''} onChange={handleChange}/> */}
                                            <input type="password" id="password" name="password" value={(state.fields && state.fields.password) ? state.fields.password : ''}  onChange={handleChange} placeholder='Password'/>
                                              {state && state.errors && state.errors.password ?
                                             <span className='error-message'>{state.errors.password}</span> : null}

                                        </div>
                                    </div>
                                </div>
                                {prodenv== true? <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group">
                                            <div className="captcha">
                                            <Reaptcha id="recaptcha" name="recaptcha"
                                                    sitekey="6LcljLYUAAAAANiWkP0pM3hMy-wakNZ0T0pYakKO"
                                                    onVerify={onCaptchaVerify}
                                                    onExpire={onCaptchaExpire}
                                                      />
                                                {state && state.errors && state.errors.recaptcha ?
                                             <span className='error-message'>{state.errors.recaptcha}</span> : null}
                                        </div>
                                    </div>
                                </div>:""}
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <p> {t("After clicking the login button, a verification code will be sent to your registered email ID.")}</p>
                                </div>
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group">
                                        <button type="submit" className="primry_btn" disabled={disabled}>
                                        {t('LOGIN')} <span><img src="src/images/right_arrow.svg" /></span>
                                        </button>
                                    </div>
                                </div>
                                </form>
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <ul>
                                        <li>
                                            <input className="remeberme " id="remeberme" type="checkbox" onChange={getRememberMe} />
                                            <label htmlFor="remeberme">{t('Remember Me')}</label>
                                        </li>
                                        <li><Link to="forgot-password"  className="">{t('FORGOT_PASSWORD')}</Link> <a href="#"> </a></li>  
                                    </ul>
                                    <p>
                                    {t("Don't have an account?")} <span><Link to="register"  className="">{t('SIGN_UP')}</Link></span> {t('now')}
                                    </p>
                                    <p>{t('PLEASE_CLICK_TO_VIEW')} <a style={{cursor:"pointer"}} disabled={disabled} onClick={()=>setshowPrivacymodel(true)}
                                    //  href="https://www.signulu.com/index.php/policy"
                                      >{t('PRIVACY_POLICY')}</a> {t('AND')} <a style={{cursor:"pointer"}} disabled={disabled} onClick={()=>setshowTermsnadconditionsmodel(true)}
                                    //    href="https://www.signulu.com/index.php/terms"
                                       >{t('TERMS_AND_CONDITIONS')}</a>.</p>
                                </div>

                                <div id="otp" className="opt_block" style={{display: 'none'}}>
                                    <div className="content">
                                        <h4>{t('A verification code is sent to your registered email ID. Please enter the code to login. ')}</h4>
                                        <div className="verification-code" style={{marginTop:'25px'}}>
                                            <div className="verification-code--inputs" style={{margin:'0px'}}>
                                                <div className='text-center' style={{marginLeft:'40px'}}>
                                                <OTPInput className="row"  inputClassName="otp_input" value={otpValue} onChange={OTPChange}
                                                 autoFocus OTPLength={6} otpType="number"
                                                  disabled={false} inputStyles={{width: "50px", height: "50px",
                                                  textAlign: "center", marginRight: "10px",fontSize: "32px", padding: '13px', color:"#2F7FED"}} />
                                                  </div>
                                                  {otpError ? <span className='error-message'>{otpError}</span> : null}
                                            </div>
                                            <input type="hidden" id="verificationCode" />
                                        </div>
                                        <p>{t('IF_YOU_DONOT_RECEIVE_CODE')} {(timerOtp.time.s|| timerOtp.time.m) ? <>{t('RESEND')}</> : <a href="#" onClick={(e) =>sendOTP(e,true)}>{t('RESEND')}</a>}</p>
                                        <button type="button" className="primry_btn" onClick={verifyOTP}>{t('VERIFY')}</button>

                                        <h6>{t('TIME_REMAINING')} {timerOtp.time.m}{timerOtp.time.s ? <span>:{timerOtp.time.s}</span>  : null} {t('SECONDS')}</h6>
                                    </div>
                                </div>
                                <div id="multilogin" className="opt_block" style={{display: 'none', height:"auto",backgroundColor:"white"}}>
                                <button className="btn-close close-btn "  onClick={multiLoginPopClose}></button>
                                    <div className="content">
                                        <div style={{color:"black"}}>
                                        {t('NOTE')}: {t('CONCURENT_LOGIN_VALID_MESSAGE')}
                                        </div>
                                    </div>
                                    <div className="text-center divbtn">
                                        <button type="button" style={{ height:"50px", width:"auto",marginRight: "30px",borderRadius: "5px"}}  className="primry_btn" onClick={multiLoginVerify}>{t('OKAY')}</button>
                                        <button type="button"  style={ {height:"50px", width:"auto", backgroundColor:"#808285",borderRadius: "5px"}}   className="primry_btn" onClick={multiLoginPopClose}>{t('CANCEL')}</button>
                                        </div>
                                        
                                </div>
                               
                            </div>
                            <Modal show={fingerPop}
                            size="md"
                           // style={{ width: "360px",display:"flex",justifyContent:"center"}}
                            centered
                            >
                                <Modal.Body className='p-0' >
                                    <div className="main-content main-from-content termsconditions p-0">
                                                <div className='container p-0  -mt-1' >
                                    <div className='termsconditions_content'style={{margin:"-0.5px",maxHeight:"600pxpx",width:"500px"}}>
                                                        
                                                            <h3 className='flex'><p className='py-3' style={{fontSize:"27px"}}>SORRY</p><p><CloseIcon style={{color:"#ffffff"}}  className="close_add_doc_pop" onClick={()=>{setfingerPop(false)}}/></p></h3>
                                                            {/* <h3><CloseIcon  className="close_add_doc_pop " onClick={()=>{setshowPrivacymodel(false)}}/>
                                                            </h3> */}
                                                        <br/>
                                                           
                                                            <div className='content' style={{maxHeight:"328px",overflow:"hidden",fontSize:"50px", fontColor:"blue"}}>
 <p style={{    width: "450px",height: "100px",fontSize: "large"}}>This feature is under development. We will notify to you once it is ready. Thank you for your support.</p>


                                                               
                                                            </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                </Modal.Body>
                            </Modal>
                            <Modal show={watchDemo}
                            size="md"
                            
                           // style={{ width: "360px",display:"flex",justifyContent:"center"}}
                            centered
                            >
                                <Modal.Body className='p-0' >
                                    <div className="main-content main-from-content termsconditions p-0">
                                                <div className='container p-0  -mt-1' >
                                    <div className='termsconditions_content'style={{margin:"-0.5px",maxHeight:"600pxpx",width:"500px"}}>
                                                        
                                                            <h3 className='flex'><p className='py-3' style={{fontSize:"27px"}}>Watch Demo</p><p><CloseIcon style={{color:"#ffffff"}}  className="close_add_doc_pop" onClick={()=>{setwatchDemo(false)}}/></p></h3>
                                                            {/* <h3><CloseIcon  className="close_add_doc_pop " onClick={()=>{setshowPrivacymodel(false)}}/>
                                                            </h3> */}
                                                        <br/>
                                                           
                                                            <div className='content' style={{maxHeight:"328px",overflow:"hidden",fontSize:"50px", fontColor:"blue"}}>
 <p style={{    width: "450px",height: "200px",fontSize: "large"}}> We are getting this ready for you in some time. We will let you know once the demo video is uploaded. 
Meanwhile, please contact Mr. Benjamin Joseph at benjamin@signulu.com or Mr. Anish at anishjoel@signulu.com for scheduling the demo and know more about Signulu.</p>


                                                               
                                                            </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                </Modal.Body>
                            </Modal>

                            <Modal show={scheduleDemo}
                            size="md"
                            centered
                            >
                                <Modal.Body className='p-0' >
                                    <div className="main-content main-from-content termsconditions p-0">
                                                <div className='container p-0  -mt-1' >
                                    <div className='termsconditions_content'style={{margin:"-0.5px"}}>
                                                        
                                                            <h3 className='flex'><p className='py-3' style={{fontSize:"27px"}}>Schedule Demo</p><p><CloseIcon style={{color:"#ffffff"}}  className="close_add_doc_pop" onClick={()=>{setscheduleDemo(false)}}/></p></h3>
                                                            {/* <h3><CloseIcon  className="close_add_doc_pop " onClick={()=>{setshowPrivacymodel(false)}}/>
                                                            </h3> */}
                                                        
                                                          <br/> 
                                                        <div className='content' style={{maxHeight:"328px",overflow:"hidden"}}>
<p style={{    width: "450px",height: "200px",fontSize: "large"}}>Thank you for your interest in Signulu eSignature Platform. Please contact Mr. Benjamin Joseph at benjamin@signulu.com or Mr. Anish at anishjoel@signulu.com for scheduling the demo and know more about Signulu.</p>
</div>
                                                        </div>
                                                        </div>
                                                        </div>                                                      
                         </Modal.Body>
                            </Modal>
                            <Modal show={showPrivacymodel}
                            size="xl"
                            centered
                            >
                                <Modal.Body className='p-0' >
                                    <div className="main-content main-from-content termsconditions p-0">
                                                <div className='container p-0  -mt-1' >
                                    <div className='termsconditions_content'style={{margin:"-0.5px",maxHeight:"485px"}}>
                                                        
                                                            <h3 className='flex'><p className='py-3'>Privacy Policy</p><p>
                                                            {/* <button type="button" className="close_icon" style={{backgroundColor:"#2f7fed",color:"#ffffff"}} onClick={()=>{setshowPrivacymodel(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                                                                <CloseIcon style={{color:"#ffffff"}}  className="close_add_doc_pop" onClick={()=>{setshowPrivacymodel(false)}}/>
                                                                </p></h3>
                                                            {/* <h3><CloseIcon  className="close_add_doc_pop " onClick={()=>{setshowPrivacymodel(false)}}/>
                                                            </h3> */}
                                                        
                                                            <h2>Signulu.com’s Privacy Policy</h2>
                                                            <div className='content' style={{maxHeight:"328px"}}>
                                                                <p>This is the first version of Signulu.com Privacy Policy and is effective as of Oct 2022. Signulu.com (“Signulu.com”, “we” or “us”) is committed to protecting your privacy. This Privacy Policy explains our practices regarding the collection, use, disclosure, and protection of information that is collected through our web based application and/or our mobile application and/or our website (collectively, our “Service”), as well as your choices regarding the collection and use of information.</p>
                                                                <p>This Privacy Policy describes how and when Signulu.com collects, uses and shares your information when you use our Services. When using any of our Services, you consent to the collection, transfer, manipulation, storage, disclosure and other uses of your information as described in this Privacy Policy. Irrespective of which country you reside in or supply information from, you authorize Signulu.com to use your information in the United States and any other country where Signulu.com operates.</p>
                                                                <p>If you have any questions or comments about this Privacy Policy, please contact us at support@signulu.com.</p>
                                                                <h4 class="mt-2">Here’s some of the information we collect:</h4>
                                                                {/* <p>The following key points of the Terms of Use are brought for your convenience only. These key points are not in lieu of the full Terms of Use.</p> */}
                                                                <ul>
                                                                    <li><h6>Information Collected Upon Registration.</h6>When you create or reconfigure an account with Signulu.com, you provide some personal information, including, but not limited to social media information such as but not limited to your Twitter (username, password, email address) and Facebook account information (username, password). Further, residential and/or business information including address, state, zip code, etc. may be collected. Some of this information, for example, your name and username, may be listed publicly on our Services, including on your profile page and in search results. Some Services, such as search, public user profiles and viewing lists, may not require registration. You are also agreeing that you are of the legal age in your country to provide such data and be of the legal age to work and be employed as a model or artist.</li>
                                                                    <li><h6>Account Information.</h6>This includes at least your user name, and e-mail address;</li>
                                                                    <li><h6>Network Performance and Usage Information.</h6>This tells us how you use our network, our products and our services, and how well our equipment and network is performing;</li>
                                                                    <li><h6>Web Browsing & Wireless Application Information.</h6>This tells us about the websites you visit and the mobile applications you use whilst logged in to our network;</li>
                                                                    <li><h6>Location Information. </h6>This tells us where your wireless device is located, as well as your zip code and street address;</li>
                                                                   <li><h6>Geolocation.</h6>We may collect your location through GPS, Wi-Fi, or wireless network triangulation in order to obtain your location for the purposes of providing our Service. We may use geolocation data to build a history of occurrences of observed events that are the intent of Signulu.com, in order to bring the most relevant information to you.</li>
                                                                <li><h6>Cookies.</h6>Like many websites, we use cookies and similar technologies to collect additional website usage data and to improve our Services, but we do not require cookies for many parts of our Services such as searching and looking at public user profiles or lists. A cookie is a small data file that is transferred to your computer's hard disk. Signulu.com may use both session cookies and persistent cookies to better understand how you interact with our Services, to monitor aggregate usage by our users and web traffic routing on our Services, and to customize and improve our Services. Most Internet browsers automatically accept cookies. You can instruct your browser, by changing its settings, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. However, some Services may not function properly if you disable cookies.</li>
                                                                <li><h6>Log Data.</h6>Our servers may automatically record information ("Log Data") created by your use of the Services. Log Data may include information such as your IP address, browser type, operating system, the referring web page, pages visited, location, your mobile carrier, device and application IDs, search terms, and cookie information. We may receive Log Data when you interact with our Services, for example, when you visit our website(s) or use our apps or otherwise sign into our Services, interact with our email notifications, use your Signulu.com account to authenticate to a third-party website or application, or visit a third-party website that includes a Signulu.com button or widget. Signulu.com uses Log Data to provide our Services and to measure, customize, and improve them. If not already done earlier, we will either delete Log Data or remove any common account identifiers, such as your username, full IP address, or email address, after 18 months.</li>
                                                                <li><h6>Third-Parties.</h6>Signulu.com may use a variety of third-party services to help provide our Services, such as hosting our blog(s), and to help us understand the use of our Services, such as Google Analytics. These third-party service providers may collect information sent by your browser as part of a web page request, such as cookies or your IP address. Third-party ad partners may share information with us, like a browser cookie ID or cryptographic hash of a common account identifier (such as an email address), to help us measure ad quality and tailor ads. For example, this allows us to display ads about things you may have already shown interest in. If you prefer, you can turn off tailored ads in your privacy settings so that your account is not matched to information shared by ad partners for tailoring ads.</li>
                                                                </ul>
                                                                <h4 class="mt-2">Here are the ways we collect your information:</h4>
                                                                <ul>
                                                                    <li>We get information from you when use our Service</li>
                                                                    <li>We collect it from how you use our Service.</li>
                                                                </ul>
                                                                <h4 class="mt-2">Here are some of the ways we use your information:</h4>
                                                                <ul>
                                                                    <li>Provide services and improve your customer experience;</li>
                                                                    <li>Address network integrity and security issues;</li>
                                                                    <li>Do research and analysis to maintain, protect, develop and improve our network;</li>
                                                                    <li>Deliver relevant advertising;</li>
                                                                    <li>Create external marketing & analytics reports;</li>
                                                                    <li>Assist in the prevention and investigation violations of our Terms of Use or Acceptable Use Policies. </li>
                                                                </ul>
                                                                <h4 class="mt-2">Some examples of who we share your Personal Information with:</h4>
                                                                 <ul>
                                                                    <li>With other companies that perform services on our behalf only as needed for them to perform those services. We require them to protect your information consistent with our Policy.</li>
                                                                    <li>With other companies and entities, to :
                                                                        <ul>
                                                                            <li>Comply with court orders and other legal process;</li>
                                                                            <li>Assist with identity verification, and preventing fraud and identity theft;</li>
                                                                            <li>Enforce our agreements and property rights; and</li>
                                                                            <li>Obtain payment for products and services including the transfer or sale of delinquent accounts to third parties for collection.</li>
                                                                        </ul>
                                                                    </li>
                                                                 </ul>
                                                                 <h4 class="mt-2">Details on Personal and Anonymous & Aggregate Information</h4>
                                                                 <ul>
                                                                    <li>What is Personal Information? Information that identifies or reasonably can be used to identify you.</li>
                                                                    <li>What is Anonymous? This is information that does not identify you and cannot reasonably be used to identify you specifically.</li>
                                                                    <li>What is Aggregate? We take a large number of individual’s data and combine it into anonymous groups or categories.</li>
                                                                    <li>How we use this information? We use and share this information in many ways including research, analysis, marketing and relevant advertising.</li>
                                                                 </ul>
                                                                 <h4 class="mt-2">Our privacy commitments</h4>
                                                                 <ul>
                                                                    <li>We do not sell your Personal Information to anyone for any purpose.</li>
                                                                    <li>We keep your Personal Information in our records while you are a user, or until it is no longer needed for business, tax, or legal purposes.</li>
                                                                    <li>We will keep your information safe using encryption or other appropriate security controls.</li>
                                                                 </ul>
                                                                 <h4 class="mt-2">Modifying Your Personal Information</h4>
                                                                 <ul>
                                                                    <li>If you are a registered user of our Services, we may provide you with tools and account settings to access or modify the personal information you provided to us and associated with your account.</li>
                                                                    <li>You may also permanently delete your account by deleting the application from your mobile device or affirmatively deleting your user account via our website.</li>
                                                                 </ul>
                                                                 <h4 class="mt-2">Our Online Privacy Policy for Children</h4>
                                                                 <ul>
                                                                    <li>We do not knowingly collect personally identifying information from anyone under the age of 13, unless authorized or with the knowledge of a parent or guardian . </li>
                                                                 </ul>
                                                                 <h4 class="mt-2">Your Choices & Controls</h4>
                                                                 <ul>
                                                                    <li>You may have choices about which types of advertising you get from us.</li>
                                                                    <li>You may control whether your anonymous information is used in our External Marketing & Analytics Reports.</li>
                                                                    <li>You may choose whether to receive marketing calls, e-mails, or text messages from us.</li>
                                                                    <li>You may have a choice about how we use your Customer Proprietary Network Information.</li>
                                                                    
                                                                 </ul>
                                                                 <p>All references regarding opting-in or obtaining consent are only relevant to UK and EU consumers and clients under the General Data Protection Regulation ("GDPR"). </p>
                                                                <p><h6>Data Controllers and Contracting Parties.</h6>We clarify that Signulu.com is acting as the data controller and contracting party for the data of some of our Members and Visitors. Signulu.com    will be the data controller and contracting party for those who live outside of the “Designated Countries” and will remain the data controller for those that live in the Designated Countries. We use the term “Designated Countries” to refer to countries in the European Union (EU), European Economic Area (EEA), and Switzerland.</p>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                </Modal.Body>
                            </Modal>
                            <Modal show={showTermsnadconditionsmodel}
                            size="xl"
                            centered
                           
                            >
                                <Modal.Body className='p-0' >
                                    <div className="main-content main-from-content termsconditions p-0">
                                                <div className='container p-0 -mt-1' >
                                    <div className='termsconditions_content'style={{margin:"-0.5px",maxHeight:"485px"}} >
                                                        
                                                            <h3 className='flex'><p className='py-3'>Terms and Conditions</p><p>
                                                            {/* <button type="button" className="close_icon" style={{backgroundColor:"#2f7fed",color:"#ffffff"}} onClick={()=>{setshowTermsnadconditionsmodel(false)}}>
                                                            <span aria-hidden="true">&times;</span>
                                                            </button> */}
                                                                <CloseIcon style={{color:"#ffffff"}}  className="close_add_doc_pop" onClick={()=>{setshowTermsnadconditionsmodel(false)}}/>
                                                                </p></h3>
                                                            {/* <h3><CloseIcon  className="close_add_doc_pop " onClick={()=>{setshowPrivacymodel(false)}}/>
                                                            </h3> */}
                                                        
                                                            <h2>Signulu.com’s Terms of Use</h2>
                                                            <div className='content' style={{maxHeight:"328px"}}>
                                                                <p>These Terms of Use ("Terms" or “Agreement”) govern your rights and obligations regarding the use of Signulu.com services and sites, which is owned by Millennium Info Tech Inc., a New Jersey(Inc. or Limited Liability Company) (collectively referred to as the <b>"Service" </b>or <b>“Site”</b>) on the Internet. These Terms constitute a fully binding agreement between Signulu.com (including its affiliates and subsidiaries "We") the proprietor of all rights in and to the Service, and you. It is therefore recommended that you carefully read these Terms.</p>
                                                                <p>By using Signulu.com’s Service, you signify your assent to these Terms and our Privacy Policy ("Privacy Policy") all of which are an integral part of these Terms. If you do not agree to these Terms or any of its parts, then you are prohibited from using the Service and we request that you refrain from doing so.</p>
                                                                <h4 class="mt-2">Key Points</h4>
                                                                <p>The following key points of the Terms of Use are brought for your convenience only. These key points are not in lieu of the full Terms of Use.</p>
                                                                <ul>
                                                                    <li>Signulu.com may remind a user via, email, SMS or a phone call with news “flashes” or updates.</li>
                                                                    <li>Signulu.com is not responsible for any civil, negligent, criminal or reckless conduct, damages, or behavior of its users or for any behavior and damages to its user’s that may be caused by others. Signulu.com is not responsible for you not being of the legal age in your country to work and be employed as a model or artist. Should any registered user, account holders, third parties or service providers be damaged by any act or omission by a model, artist or user of this service, Signulu will not be liable.</li>
                                                                    <li>Signulu.com is not responsible for a user’s past, current, or future conduct as it relates to their use of the Service. Signulu.com may record the time and location of where you are. However, such information will only be disclosed if discovered or subpoenaed by court order. Signulu.com may record and keep your personal identifiable information associated with your account. Such information will be retained for a limited period of time and in accordance with the Privacy Policy.</li>
                                                                    <li>Signulu.com may require you to log in by using an existing social media account or by creating a standalone account. The Service will not provide your account information to any other users, and will not make available nor be responsible for any links between your use of the service and your social media accounts.</li>
                                                                    <li>Signulu.com is not responsible for any additional SMS, email, phone, or data charges that may result from a user not shutting off the service’s reminders. Such additional charges are borne by the user.</li>
                                                                    <li>The platform is an electronic signature platform that provides reliability, consistency, scalability and performance.</li>
                                                                    <li>It is your responsibility to understandthe validity and legality of electronic signatures in your jurisdiction. When in doubt we encourage You to contact Signulu.com for further information.</li>
                                                                    <li>You agree that Signulu.com shall not be legally or financially responsible for the invalidity or non acceptance of anelectronic signature in the jurisdiction where you reside or for the specificdocument that may not be able to be signed electronically.</li>
                                                                    <li>You agree that Signulu.com or the service is not responsible or liable for any damages that may be caused by our site or service being hacked.</li>
                                                                    <li>You agree that Signulu.com or the service is not responsible or liable for any damages that may be caused by our site or service being pirated or cyber attacked.</li>
                                                                    <li>You agree that Signulu.com or the service is not responsible or liable for any damages due to issues related to cloud storage.</li>
                                                                    <li>You agree not to misuse any documentation that may be used, managed or stored on our site or service. In the event of such a misuse of any documentation by you, either directly or indirectly, you agree that Signulu.com or the service is not responsible for any damages.</li>
                                                                    <li>You agree not to amend, alter, change, modify, update a document that has already been signed or executed on our Site or Service. In the event of such an alteration of a signed document by you, either directly or indirectly, you agree that Signulu.com or the service is not responsible for any damages.</li>
                                                                    <li>You agree that any and all sales are final for the services purchased.</li>
                                                                    <li>Except as otherwise provided herein, you agree to abide by any Purchase Order Terms and Conditions that you may have signed. You agree to abide by the payment, invoice and financial terms of such Purchase Order Terms and Conditions.</li>
                                                                </ul>
                                                                <h4 class="mt-2">The Internet connection is under your responsibility and at your expense</h4>
                                <p>Transmitting and receiving real-time updates to and from the Service, require an online (Wi-Fi or 3G or 4G or 4G LTE) connection between your cellular device and the Internet. The expenses of such connection are as prescribed by the agreement between you and your communication service provider (such as your cellular company), and according to its applicable terms of payment.</p>
                                <h4 class="mt-2">Intellectual Property</h4>
                                <p>The Service, its database, Signulu.com trademarks, copyrights, and any other intellectual property owned by Signulu.com is not free and not available for use without express written consent of Signulu.com. Signulu also does not take responsibility for any user of this site who may be using another person’s image or Intellectual Property unlawfully or intentionally. Signulu will ensure that any such activity will be immediately removed from the Site and Service and use reasonable best efforts to ensure that all users are the legal and rightful owners of their Intellectual Property.</p>
                                <h4 class="mt-2">Your age</h4>
                                <p>The Service is intended for use by users at or above the age of 18 years old, unless authorized or with the knowledge of a parent or guardian.</p>
                                <h4 class="mt-2">Privacy</h4>
                                <p>Your privacy is important to us. While using the Service, personal information may be provided by you or collected by Signulu.com as detailed in our Privacy Policy at. The Privacy Policy explains our practices pertaining to the use of your personal information and we ask that you read such Privacy Policy carefully. By accepting these Terms, you hereby acknowledge and agree to the collection, storage and use of your personal information by Signulu.com, subject to this section, the Privacy Policy and any applicable laws and regulation.</p>
                                <h4 class="mt-2">Feedback</h4>
                                <p>Your feedback on the Service we provide is important to us. By accepting these Terms, you agree that any feedback you may provide is constructive and may be used by Signulu.com for whatever purpose Signulu.com deems necessary. You further agree that you claim no ownership to any feedback you may submit and to any implementation by Signulu.com of submitted feedback.</p>
                                <h4 class="mt-2">USING THE SERVICE</h4>
                                <p>You may use the Service solely for private and personal and entertainment purposes. You must not use the Service commercially. For example, you may not offer to third parties a service of your own that uses the Service; you may not resell the Service, offer it for rent or lease, offer it to the public via communication or integrate it within a service of your own, without the prior written consent of Signulu.com.</p>
                                <p>You must not copy, print, save or otherwise use the data from the Site or the Service's database. This clause does not limit the use of the database as intended by the Service and for the purposes of private and personal use of the Service.</p>
                                <p>When using the Service or the Site you may not engage in scraping, data mining, harvesting, screen scraping, data aggregating, and indexing. You agree that you will not use any robot, spider, scraper or other automated means to access the Site or the Service’s database for any purpose without the express written permission of Signulu.com. The Service may not be used in any way not expressly permitted by these Terms.</p>
                                <p>You may not sell or offer to sell any Data that is freely or otherwise available for purchase on the Service. All Data belongs to Signulu.com and may be used and/or sold by Signulu.com for any purpose whatsoever.</p>
                                <p>Except as explicitly provided herein, nothing in this Agreement shall be deemed to create a license in or under any such Intellectual Property Rights, and you agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create derivative works from any Signulu.com content. Use of Signulu.com content for any purpose not expressly permitted by this Agreement is strictly prohibited.</p>
                                <p>You may choose to or we may invite you to submit comments or ideas about the Service, including without limitation about how to improve the Service or our products (“Ideas” of “Feedback”). By submitting any Idea, you agree that your disclosure is gratuitous, unsolicited and without restriction and will not place Signulu.com under any fiduciary or other obligation, and that we are free to use the Idea without any additional compensation to you, and/or to disclose the Idea on a non-confidential basis or otherwise to anyone. You further acknowledge that, by acceptance of your submission, Signulu.com does not waive any rights to use similar or related ideas previously known to Signulu.com, or developed by its employees, or obtained from sources other than you. </p>
                                <h4 class="mt-2">USE RESTRICTIONS</h4>
                                <p>There are certain conducts which are strictly prohibited on the Service. Please read the following restrictions carefully. Your failure to comply with the provisions set forth below may result (at Signulu.com sole discretion) in the termination of your access to the Service and may also expose you to civil and/or criminal liability.</p>
                                <h4 class="mt-2">You may not, whether by yourself or anyone on your behalf:</h4>
                                <ul>
                                    <li>Copy, modify, adapt, translate, reverse engineer, decompile, or disassemble any portion of the Content at the Service and/or Site, in any way or publicly display, perform, or distribute them;</li>
                                    <li>Make any use of the Content on any other website or networked computer environment for any purpose, or replicate or copy the Content without Signulu.com prior written consent;</li>
                                    <li>Create a browser or border environment around the Content (no frames or inline linking);</li>
                                    <li>Interfere with or violate any third party or other user's right to privacy or other rights, including copyrights and any other intellectual property rights of others, or harvest or collect personal information about visitors or users of the Service and/or Site without their express consent, including using any robot, spider, site search or retrieval application, or other manual or automatic device or process to retrieve, index, or data-mine;</li>
                                    <li>Defame, abuse, harass, stalk, threaten, or otherwise violate the legal rights of others, including others’ copyrights, and other intellectual property rights;</li>
                                    <li>Transmit or otherwise make available in connection with the Service and/or Site any virus, worm, Trojan Horse, time bomb, web bug, spyware, or any other computer code, file, or program that may or is intended to damage or hijack the operation of any hardware, Software, or telecommunications equipment, or any other actually or potentially harmful, disruptive, or invasive code or component;</li>
                                    <li>Interfere with or disrupt the operation of the Service and/or Site, or the servers or networks that host the Service and/or Site or make the Service and/or Site available, or disobey any requirements, procedures, policies, or regulations of such servers or networks;</li>
                                    <li>Sell, license, or exploit for any commercial purposes any use of or access to the Content and/or the Service and/or Site;</li>
                                    <li>Frame or mirror any part of the Service and/or Site without Signulu.com prior express written authorization;</li>
                                    <li>Create a database by systematically downloading and storing all or any of the Content from the Service and/or Site;</li>
                                    <li>Forward any data generated from the Service and/or Site without the prior written consent of Signulu.com ;</li>
                                    <li>Transfer or assign your accounts' password, even temporarily, to a third party;</li>
                                    <li>Use the Service and/or Site for any illegal, immoral or unauthorized purpose;</li>
                                    <li>Use the Site, the Service, the Content and/or for non-personal or commercial purposes without Signulu.com express prior written consent; or</li>
                                    <li>Infringe or violate any of the Terms.</li>
                                </ul>
                                <h4 class="mt-2">TERMINATION OF USE OF THE SERVICE</h4>
                                <p>You may terminate your use of the Service at any time and for whatever reason. Signulu.com retains the right to block your access to the Service and discontinue your use of the Service, at any time and for any reason Signulu.com deems appropriate, at its sole and absolute discretion. To the extent possible, we will advise you of your account termination.</p>
                                <p>Signulu.com reserves the right to change, suspend, remove, discontinue, or disable access to the Service (including, but not limited to, the Application) at any time without notice. In no event will Signulu.com be liable for the removal of or disabling of access to any portion or feature of the Service (including, but not limited to, the Application).</p>
                                <p>If you breach any of the terms or conditions of the Terms or Signulu.com discontinues their Service, the Terms will automatically terminate. All of the sections of the Terms will survive any termination of the Terms except the License section and the Consent to Use of Data and Mobile Communications section.</p>
                                <h4 class="mt-2">INTELLECTUAL PROPERTY</h4>
                                <p>Copying, distributing, publicly displaying, offering to the public via communication, transferring to the public, modifying, adapting, processing, creating derivative works, selling or leasing, any part of the Service, in any manner or means without the prior written consent of Signulu.com , is strictly forbidden. "Signulu.com ", Signulu.com logo, and other trade and/or service marks are property of Signulu.com and may not be used in any of the aforementioned means.</p>
                                <p>Signulu.com may protect the Service by technological means intended to prevent unauthorized use of the Service. You undertake not to circumvent these means. Without derogating Signulu.com rights under these Terms or under any applicable law, infringement of the rights in and to the Service will, in and on itself, result in the termination of all your rights under these Terms. In such an event, you must immediately cease any and all uses of the Service, and within your obligations to Signulu.com, you undertake to do so.</p>
                                <h4 class="mt-2">MOBILE SYSTEM</h4>
                                <p>We may make available the System to access the Service via a mobile device (“Mobile System”). To use the Mobile System you must have a mobile device that is compatible with the Mobile Service. Signulu.com does not warrant that the Mobile System will be compatible with your mobile device. Signulu.com hereby grants you a non-exclusive, non-transferable, revocable license to use a compiled code copy of the Mobile System for one Signulu.com account owned or leased solely by you, for your personal use. You may not:</p>
                                <ul>
                                    <li>Modify, disassemble, decompile or reverse engineer the Mobile System, except to the extent that such restriction is expressly prohibited by law;</li>
                                    <li>Rent, lease, loan, resell, sublicense, distribute or otherwise transfer the Mobile System to any third party or use the Mobile System to provide time sharing or similar services for any third party;</li>
                                    <li>Make any copies of the Mobile System;</li>
                                    <li>Remove, circumvent, disable, damage or otherwise interfere with security-related features of the Mobile System, features that prevent or restrict use or copying of any content accessible through the Mobile System, or features that enforce limitations on use of the Mobile System; or</li>
                                    <li>Delete the copyright and other proprietary rights notices on the Mobile System. You acknowledge that Signulu.com may from time to time issue upgraded versions of the Mobile System, and may automatically electronically upgrade the version of the Mobile System that you are using on your mobile device. You consent to such automatic upgrading on your mobile device, and agree that the terms and conditions of this Agreement will apply to all such upgrades. Any third-party code that may be incorporated in the Mobile System is covered by the applicable open source licenses, free software licenses, third-party licenses, and/or End User Licenses, if any, authorizing use of such code. The foregoing license grant is not a sale of the Mobile System or any copy thereof, and Signulu.com or its third party partners or suppliers retain all right, title, and interest in the Mobile System (and any copy thereof). Any attempt by you to transfer any of the rights, duties or obligations hereunder, except as expressly provided for in this Agreement, is void.</li>
                                </ul>
                                <p>Signulu.com reserves all rights not expressly granted under this Agreement. If the Mobile System is being acquired on behalf of the United States Government, then the following provision applies. Use, duplication, or disclosure of the Mobile System by the U.S. Government is subject to restrictions set forth in this Agreement and as provided in DFARS 227.7202-1(a) and 227.7202-3(a) (1995), DFARS 252.227-7013(c)(1)(ii) (OCT 1988), FAR 12.212(a) (1995), FAR 52.227-19, or FAR 52.227-14 (ALT III), as applicable. The Mobile System originates in the United States, and is subject to United States export laws and regulations. The Mobile System may not be exported or re-exported to certain countries or those persons or entities prohibited from receiving exports from the United States. In addition, the Mobile System may be subject to the import and export laws of other countries. You agree to comply with all United States and foreign laws related to use of the Mobile System and the Signulu.com Service. (1) Mobile System from iTunes or Google Play. The following applies to any Mobile System you acquire from the iTunes Store or via Google Play Store: You acknowledge and agree that this Agreement is solely between you and Signulu.com, not Apple/Android, and that Apple/Android has no responsibility for the software or content thereof. Your use of the software must comply with the App Store Terms of Service. You acknowledge that Apple/Android has no obligation whatsoever to furnish any maintenance and support services with respect to the software. In the event of any failure of the software to conform to any applicable warranty, you may notify Apple/Android, and Apple/Android will refund the purchase price for the Software to you; to the maximum extent permitted by applicable law, Apple/Android will have no other warranty obligation whatsoever with respect to the Software, and any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be solely governed by this Agreement and any law applicable to Signulu.com as provider of the Service. You acknowledge that Apple/Android is not responsible for addressing any claims of you or any third party relating to the software or your possession and/or use of the software, including, but not limited to: (i) product liability claims; (ii) any claim that the software fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation; and all such claims are governed solely by this Agreement and any law applicable to Signulu.com as provider of the Service. You acknowledge that, in the event of any third party claim that the software or your possession and use of that software infringes that third party’s intellectual property rights, Signulu.com , not Apple/Android, will be solely responsible for the investigation, defense, settlement and discharge of any such intellectual property infringement claim to the extent required by this Agreement. You and Signulu.com acknowledge and agree that Apple/Android, and Apple/Android’s subsidiaries, are third party beneficiaries of this Agreement as relates to your license of the software, and that, upon your acceptance of the terms and conditions of this Agreement, Apple/Android will have the right (and will be deemed to have accepted the right) to enforce this Agreement as relates to your license of the software against you as a third party beneficiary thereof.</p>
                                <p>If you use the Service through an Apple/Android device, then you agree and acknowledge that:</p>
                                <p>Apple/Android, Inc. bears no duties or obligations to you under the Terms, including, but not limited to, any obligation to furnish you with Service maintenance and support;</p>
                                <p>You will have no claims, and you waive any and all rights and causes of action against Apple/Android with respect to the Service or the Terms, including, but not limited to claims related to maintenance and support, intellectual property infringement, liability, consumer protection, or regulatory or legal conformance;</p>
                                <p>Apple/Android and Apple/Android’s subsidiaries are third party beneficiaries of the Terms. Upon your acceptance of the Terms, Apple/Android will have the right (and will be deemed to have accepted the right) to enforce these Terms against you as a third party beneficiary thereof.</p>
                                <p>PLEASE READ THESE TERMS OF SERVICE CAREFULLY. BY ACCESSING OR USING SIGNULU.COM AND ANY SERVICES PROVIDED BY US, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS DESCRIBED HEREIN AND ALL TERMS INCORPORATED BY REFERENCE. IF YOU DO NOT AGREE TO ALL OF THESE TERMS AND CONDITIONS, DO NOT USE SIGNULU.COM SERVICE OR ANY SERVICES PROVIDED BY US.</p>
                                <p>We reserve the right to change or modify any of the terms and conditions contained in the Terms or any policy or guideline of the Service, at any time and in our sole discretion. Any changes or modifications will be effective immediately upon the posting of such revisions on our website located at Signulu.com (the “Site”) or when such revisions are made available via the Service, and you waive any right you may have to receive specific notice of such changes or modifications. Your continued use of the Service (or any portion thereof) following the posting of changes or modifications will confirm your acceptance of such changes or modifications. Therefore, you should frequently review the Terms and applicable policies from time-to-time to understand the terms and conditions that apply to your use of the Service. If you do not agree to the amended Terms, you must stop using the Service.</p>
                                <h4 class="mt-2">DEVICE USAGE TERMS AND CONDITIONS</h4>
                                <p>You acknowledge and agree that your use of the Service must also be in accordance with the usage rules established by your mobile device platform or service provider.</p>
                                <h4 class="mt-2">REGISTRATION DATA</h4>
                                <p>You agree to: (i) provide accurate, current and complete information about you as may be prompted by any signup, login and/or registration forms made available via the Service ("Registration Data"); (ii) maintain and promptly update the Registration Data, and any other information you provide to Signulu.com, in order to keep it accurate, current and complete; and (iii) accept all risk of unauthorized access to the Registration Data and any other information you provide to Signulu.com.</p>
                                <p>You may not use the Service if you are less than 13 years of age, whether authorized or with the knowledge of a parent or guardian. By using the Signulu.com software you verify that you are entering into this agreement with a clear mind.</p>
                                <p>Signulu.com is under no obligation to retain a record of your account or any data or information that you may have stored by means of the account or your use of the Services. You are only authorized to create and use one account for the Service and are prohibited from utilizing alter-egos or other disguised identities when utilizing the Application or Services. You are under no obligation to use or continue to use the Application or Services and may cease use of the Application or Services without notice to Signulu.com.</p>
                                <h4 class="mt-2">GRANT AND RESTRICTIONS</h4>
                                <p>Subject to the terms, conditions and limitations set forth in the Terms, Signulu.com grants you a non-exclusive, non-transferable and revocable license to use the software on any mobile device that you own or control. The terms of the license will also govern any upgrades provided by Signulu.com that replace and/or supplement the original software, unless such upgrade is accompanied by a separate license, in which case the terms of that license will govern.</p>
                                <p>You agree not to do, or authorize or permit any third-party to do, any of the following: (i) distribute or make the software available over a network where it could be used by multiple devices at the same time; (ii) rent, lease, lend, sell, redistribute or sublicense the software; (iii) copy, decompile, reverse engineer, disassemble, attempt to derive the source code of, modify, or create derivative works of the software, any updates, or any part thereof (except as and only to the extent any of the foregoing restrictions are prohibited by applicable law); or (iv) remove, alter or obscure any copyright, trademark or other proprietary rights notice on or in the software. If you violate any of the foregoing restrictions, your use of the software will immediately cease and you will have infringed the copyright and other rights of Signulu.com, which may subject you to prosecution and damages. Signulu.com reserves all rights not expressly granted to you in the Terms.</p>
                                <h4 class="mt-2">CONSENT TO USE OF DATA AND MOBILE COMMUNICATIONS</h4>
                                <p>You agree that Signulu.com may collect and use technical data, personal information and related information in connection with your use of the software including, but not limited to, contact information and technical information about your device, service and application Software, and peripherals, that are gathered periodically to facilitate the features and functionality of the software and of Service updates, product support and other services. You also consent to our communicating with you about the software or in connection with the features, functions and activities contained in the software. You also agree that any information that the Site or Service obtains from your social media accounts be used for whatever purpose Signulu.com deems necessary.</p>
                                <h4 class="mt-2">COMPATIBILITY WITH MOBILE DEVICES</h4>
                                <p>Signulu.com does not warrant that the Service will be compatible or interoperable with your mobile device or any other hardware, Software or equipment installed on or used in connection with your mobile device.</p>
                                <h4 class="mt-2">CARRIER CHARGES</h4>
                                <p>You acknowledge and understand that the Service requires and utilizes phone service, data access and text messaging capabilities. Carrier rates for phone, data and text messaging may apply and you are responsible for any such charges.</p>
                                <h4 class="mt-2">ELECTRONIC COMMUNICATIONS</h4>
                                <p>When you use the Service or send e-mails to us, you are communicating with us electronically, and you consent to receive communications from us electronically. We will communicate with you by e-mail or providing notices via the Service. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing. Any such emails may include marketing and promotional content.</p>
                                <h4 class="mt-2">THIRD-PARTY INTERACTIONS</h4>
                                <p>Your use of the software and your contact, interaction, or dealings with any third-parties arising out of your use of the Service is solely at your own risk. You acknowledge and agree that <b>Signulu.com is not responsible or liable in any manner for any loss, damage or harm of any sort incurred as the result of the Service. Signulu.com is not responsible, and shall not be held liable for, the acts, errors, omissions, representations, warranties, breaches or negligence of any such suppliers for any personal injuries, death, property damage, loss, theft or other damages or expenses resulting therefrom.</b></p>
                                <h4 class="mt-2">THIRD-PARTY CONTENT</h4>
                                <p>Signulu.com may provide third-party content via the Service and may provide links to Web pages and content of third-parties (collectively, the “Third-Party Content”) as a service to those interested in this information. Signulu.com does not control, endorse or adopt any Third-Party Content and makes no representation or warranties of any kind regarding the Third-Party Content including, but not limited to, its accuracy or completeness. You acknowledge and agree that Signulu.com is not responsible or liable in any manner for any Third-Party Content and undertakes no responsibility to update or review any Third-Party Content. Users use such Third-Party Content contained therein at their own risk.</p>
                                <h4 class="mt-2">ADVERTISEMENTS AND PROMOTIONS; THIRD-PARTY PRODUCTS AND SERVICES</h4>
                                <p>Signulu.com may run advertisements and promotions from third-parties via the Service or may otherwise provide information about or links to third-party products or services via the Service. Your business dealings or correspondence with, or participation in promotions of, such third-parties, and any terms, conditions, warranties or representations associated with such dealings or promotions, are solely between you and such third-party. Signulu.com is not responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or promotions or as the result of the presence of such non-Signulu.com advertisers or third-party information accessible via the Service.</p>
                                <h4 class="mt-2">UNLAWFUL ACTIVITY</h4>
                                <p>We reserve the right to investigate complaints or reported violations of the Terms and to take any action we deem appropriate including, but not limited to, reporting any suspected unlawful activity to law enforcement officials, regulators, or other third-parties and disclosing any information necessary or appropriate to such persons or entities relating to your Registration Data, usage history, posted materials, IP addresses, traffic information, and use of Signulu.com.</p>
                                <h4 class="mt-2">INFORMATION AND PRESS RELEASES</h4>
                                <p>The Site may contain information and press releases about us. We disclaim any duty or obligation to update this information. Information about companies other than ours contained in any press release or otherwise should not be relied upon as being provided or endorsed by us.</p>
                                <p>PLEASE READ CAREFULLY. THIS SECTION LIMITS SIGNULU.COM ’S RESPONSIBILITY TO YOU FOR USE OF THE SERVICE. WE MAKE NO REPRESENTATION OR WARRANTY TO YOU REGARDING THE SERVICE.</p>
                                <h4 class="mt-2">DISCLAIMERS</h4>
                                <p>YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT USE OF THE SERVICE (INCLUDING, BUT NOT LIMITED TO, THE APPLICATION) IS AT YOUR SOLE RISK AND THAT THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE, SAFETY, ACCURACY AND EFFORT IS WITH YOU. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE SERVICE (INCLUDING, BUT NOT LIMITED TO, THE APPLICATION) IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. SIGNULU.COM DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS (EXPRESS OR IMPLIED, ORAL OR WRITTEN) WITH RESPECT TO THE TERMS AND THE SERVICE (INCLUDING, BUT NOT LIMITED TO, THE APPLICATION) WHETHER ALLEGED TO ARISE BY OPERATION OF LAW, BY REASON OF CUSTOM OR USAGE IN THE TRADE, BY COURSE OF DEALING OR OTHERWISE, INCLUDING ANY AND ALL: (I) WARRANTIES OF MERCHANTABILITY; (II) WARRANTIES OF FITNESS OR SUITABILITY FOR ANY PURPOSE (WHETHER OR NOT SIGNULU.COM KNOWS, HAS REASON TO KNOW, HAS BEEN ADVISED OR IS OTHERWISE AWARE OF ANY SUCH PURPOSE); AND (III) WARRANTIES OF NON-INFRINGEMENT OR CONDITION OF TITLE. SIGNULU.COM DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE APPLICATION WILL BE ACCURATE OR MEET YOUR REQUIREMENTS, THAT THE OPERATION OF THE APPLICATION WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT DEFECTS IN THE APPLICATION WILL BE CORRECTED. NO ORAL OR WRITTEN INFORMATION, GUIDELINES OR ADVICE GIVEN BY SIGNULU.COM OR ITS AUTHORIZED REPRESENTATIVE WILL CREATE A WARRANTY.</p>
                                <p>You understand that the Service may be subject to downtime or otherwise unavailable for temporary periods of time due to maintenance or other reasons as we deem necessary. To the maximum extent allowed by law, we do not warrant any connection to, transmission over, or results or use of, any network connection or facilities provided (or failed to be provided) through the Service. You are responsible for assessing your own computer and transmission network needs, and the results to be obtained therefrom. You agree that we are not responsible or liable for any possible inadvertent inaccuracies in the information used by you on the Service. If you choose to include its Content in the Service, You are fully responsible for the accuracy of such listings and accuracy of the Content.</p>
                                <p>EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT, YOU EXPRESSLY AGREE THAT USE OF THE SERVICE IS AT YOUR SOLE RISK. THE SERVICE IS PROVIDED ON AN "AS IS," "AS AVAILABLE" BASIS, UNLESS SUCH WARRANTIES ARE LEGALLY INCAPABLE OF EXCLUSION. WE, ITS SUBSIDIARIES AND AFFILIATES (collectively, “SIGNULU.COM ") DISCLAIM ALL IMPLIED WARRANTIES AND CONDITIONS, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF THIRD PARTY RIGHTS, AND THOSE ARISING FROM A COURSE OF DEALING OR USAGE OF TRADE, REGARDING THE SERVICE, AND THE INTERNET. SIGNULU.COM ASSUMES NO RESPONSIBILITY FOR ANY DAMAGES SUFFERED BY YOU, INCLUDING, BUT NOT LIMITED TO, LOSS OF DATA, ITEMS OR INFORMATION, ANY LOSS OF PROFITS OR SAVINGS, LOSS OF USE, OR ANY OTHER COMMERCIAL LOSS, EVEN IF WE WAS ADVISED OF SUCH DAMAGES, FROM DELAYS, NON-DELIVERIES, ERRORS, SERVICE DOWN TIME, SERVICE UNAVAILABILITY, OR SERVICE INTERRUPTIONS CAUSED BY THE WE PARTIES OR BY YOUR OR ANY OTHER USER'S OWN ERRORS AND/OR OMISSIONS.</p>
                                <p>YOU ACKNOWLEDGE AND AGREE THAT SIGNULU.COM SHALL NOT ASSUME OR HAVE ANY LIABILITY FOR ANY ACTION BY THE OTHER USERS WITH RESPECT TO CONDUCT, COMMUNICATION OR CONTENT ON THE SERVICE. NEITHER PARTY SHALL BE LIABLE TO THE OTHER FOR ANY INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, EXEMPLARY, OR CONSEQUENTIAL DAMAGES IN CONNECTION WITH THE SERVICE OR THESE TERMS. EXCEPT FOR YOUR INDEMNIFICATION OBLIGATIONS AS OUTLINED BELOW, EACH PARTY’S LIABILITY TO THE OTHER PARTY AND YOUR EXCLUSIVE REMEDY FOR YOUR BREACH OF ANY IMPLIED OR EXPRESS WARRANTY, OR FOR BREACH OF THIS AGREEMENT IS LIMITED SOLELY TO THE TOTAL AMOUNT OF FEES PAID BY YOU TO WE FOR USE OF THE SERVICE. BECAUSE SOME STATES OR COUNTRIES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CERTAIN DAMAGES, IN SUCH STATES OR COUNTRIES EACH PARTY’S AND ITS RESPECTIVE SUBSIDIARIES' AND AFFILIATES' LIABILITY IS LIMITED TO THE EXTENT PERMITTED BY LAW.</p>
                                <p>You agree to defend, indemnify and hold harmless Signulu.com from all third-party liabilities, claims and expenses, including attorneys' fees (“Claims”) arising from any breach of these Terms by You.</p>
                                <p>You shall not be required to indemnify and hold Signulu.com harmless for such Claims arising out of Signulu.com’s willful misconduct, gross negligence or breach of these Terms. We agree to defend, indemnify and hold harmless you from any Claims that the Service infringes any patent, copyright, trade secret, or other proprietary right of a third party. A party being indemnified by the other party reserve the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by a party hereunder, and in such event, the party providing the indemnification shall have no further obligation to provide indemnification for such matter.</p>
                                <h4 class="mt-2">Legal Disputes.</h4>
                                <p>You agree that any claim or dispute at law or equity that has arisen or may arise between you and Signulu.com will be resolved in accordance with the provisions set forth in this Legal Disputes Section. Please read this Section carefully.  It affects your rights and will have a substantial impact on how claims between Signulu.com and You are resolved.</p>
                                <ul>
                                    <li><u>Applicable Law.</u>You agree that the laws of the State of New Jersey, without regard to principles of conflict of laws, will govern these Terms and any claim or dispute that has arisen or may arise between You and Signulu.com , except as otherwise stated in this Agreement.</li>
                                    <li><u>Agreement to Arbitrate.</u>You agree that any and all disputes or claims that have arisen or may arise between Signulu.com and You shall be resolved exclusively through final and binding arbitration, rather than in court, except that you may assert claims in small claims court, if the claim qualifies.  The Federal Arbitration Act governs the interpretation and enforcement of this Agreement to Arbitrate.</li>
                                    <li><u>Arbitration Procedures.</u>Arbitration is more informal than a lawsuit in court.  Arbitration uses a neutral arbitrator instead of a judge or jury, and court review of an arbitration award is very limited.  However, an arbitrator can award the same damages and relief on an individual basis that a court can award to an individual.  An arbitrator also must follow the terms of this Agreement as a court would. The arbitration will be conducted by the American Arbitration Association ("AAA") under its rules and procedures, including the AAA's Supplementary Procedures for Consumer-Related Disputes (as applicable), as modified by this Agreement to Arbitrate.  The AAA's rules are available at www.adr.org.  A form for initiating arbitration proceedings is available on the AAA's website at http://www.adr.org. </li>
                                    <p>The arbitration shall be held in the county in which Signulu.com is incorporated.  If the value of the relief sought is $10,000 or less, Signulu.com and You may elect to have the arbitration conducted by telephone or based solely on written submissions, which election shall be binding on Signulu.com and You subject to the arbitrator's discretion to require an in-person hearing, if the circumstances warrant.  Attendance at an in-person hearing may be made by telephone by Signulu.com and You unless the arbitrator requires otherwise.</p>
                                    <p>The arbitrator will decide the substance of all claims in accordance with the laws of the State of New Jersey, including recognized principles of equity, and will honor all claims of privilege recognized by law.  The arbitrator shall be bound by rulings in prior arbitrations involving you to the extent required by applicable law.  The arbitrator's award shall be final and binding and judgment on the award rendered by the arbitrator may be entered in any court having jurisdiction thereof.</p>
                                    <li><u>Costs of Arbitration.</u>Payment of all filing, administration and arbitrator fees will be governed by the AAA's rules, unless otherwise stated in this Agreement to Arbitrate.  In the event the arbitrator determines that the claim(s) you assert in the arbitration to be frivolous, you agree to reimburse Signulu.com for all fees associated with the arbitration.</li>
                                    <li><u>Judicial Forum for Legal Disputes.</u>Unless You and We agree otherwise, in the event that the Agreement to Arbitrate above is found not to apply to You or to a particular claim or dispute, as a result of a court order, You agree that any claim or dispute that has arisen or may arise between You and We must be resolved exclusively by a state or federal court located in New Jersey. Signulu.com and You agree to submit to the non-exclusive jurisdiction of the courts located within Morris County, New Jersey for the purpose of litigating all such claims or disputes.</li>
                                    <p>Nothing herein shall be construed as conveying title to the Service to You. The Service is and shall at all times remain the personal property of Signulu.com. You agree that it will never represent that the title to the Service is in anyone other than Signulu.com and shall never cooperate, whether actively or passively, with anyone claiming any right, title or interest in the Service, other than Signulu.com.</p>
                                    <p>If any provision of this Agreement is unenforceable, such unenforceability shall not make any other provision hereof unenforceable. Any reference to any party includes its agents and employees. If any provisions of this Agreement conflict with any statute or rule of law in any jurisdiction wherein it may be sought to be enforced, then said provisions shall be deemed null and void to such extent, but without invalidating the remaining provisions.</p>
                                    <p>The remedies of this Agreement provided in favor of either party shall not be deemed exclusive, but shall be cumulative, and shall be in addition to all other remedies in its favor existing at law or in equity. The failure or delay of either party in exercising any right granted it hereunder upon any occurrence of any of the contingencies set forth herein shall not constitute a waiver of any such right upon the continuation or recurrence of any such contingencies or similar contingencies and any single or partial exercise of any particular right by either party shall not exhaust the same or constitute a waiver of any other right provided herein.</p>
                                    <p>Any failure by either party to require strict performance by the other party of any term, covenants or agreements herein shall not be construed as a consent or waiver of any breach of the same or of any other term, covenant or agreement herein.</p>
                                    <p>This Agreement constitutes the complete agreement and understanding among the parties, and supersedes all prior or contemporaneous proposals, oral or written, understandings, covenants, agreements, arrangements and communications between them relating to the subject matter of this Agreement.</p>
                                    <p>You acknowledge that you have had full opportunity to review this Agreement in detail and to seek independent legal representation and advice pertaining to this Agreement and either has done or has in its own independent business judgment chosen not to do so.</p>
                                    <p>This Agreement shall not be construed against Signulu.com as the drafter of this Agreement.</p>
                                    <p>All notices given by you or required under this Agreement shall be in writing and addressed to Signulu.com, Millennium Info Tech Inc., 101 Morgan Lane, Suite # 188 Plainsboro NJ 08536</p>
                                </ul>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                </Modal.Body>
                            </Modal>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 right_part">
                            <div className="right_part_cont">
                            <h3>{t('LOGIN_TOP_MESSAGE')}</h3>
                                {/* <h3 style={{marginLeft:100}}>Signulu</h3>
                                <div>
                                    <p style={{color:"#113E8B",fontSize:"30px",marginBottom:10}}>Comprehensive eSignature</p><p style={{color:"#113E8B",fontSize:"30px",marginBottom:10}}>and Document</p> <p  style={{color:"#113E8B",fontSize:"30px",marginBottom:20}}> Workflow Solution</p>
                                </div> */}
                                <div className="btn_sec">
                                    <button type="button" disabled={disabled} className="play_btn"  style={{cursor:"pointer"}} onClick={()=>setwatchDemo(true)}>
                                    {t('WATCH_DEMO')}<span><img src="src/images/play_btn.svg" /></span>
                                    </button>
                                     <button type="button" disabled={disabled} className="schedule_btn"  style={{cursor:"pointer"}} onClick={()=>setscheduleDemo(true)}>
                                    {/* {t('SCHEDULE_DEMO')} <span><img src="src/images/schedule.svg" /></span> */}
                                    {t('SCHEDULE_DEMO')} <span><img src="src/images/schedule.svg" /></span> 
                                    </button> 
                                       {/* <div id="multilogin" className="opt_block" style={{display: 'none', height:"auto",backgroundColor:"white"}}>
                              <button className="btn-close close-btn" onClick={multiLoginPopClose}></button>
                                    <div className="content">
                                        <div style={{color:"black"}}>
                                        {t('NOTE')}: {t('SCHEDULE-DEMO')}
                                          </div>
                                    </div>
                                    <div className="text-center divbtn">
                                        <button type="button" style={{ height:"38px", width:"auto",marginRight: "30px",borderRadius: "10px"}}  className="primry_btn" onClick={multiLoginVerify}>{t('OK')}</button>
                                        <button type="button" style={ {height:"38px", width:"auto", backgroundColor:"#808285",borderRadius: "10px"}}  className="primry_btn" onClick={multiLoginPopClose}>{t('CANCEL')}</button>
                                        </div>
                                </div> */}
                                </div>
                            </div>
                            <div className="picture">
                                <img src="src/images/logi_pic.svg" alt="" className="img-fluid mx-auto d-block" />
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
          
            <ToastContainer />
        </div>
      
     </>
    )
}



export { Login }; 