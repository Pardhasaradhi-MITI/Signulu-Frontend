import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';




function AddDocumentsStep2() {
    //Vertical Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    //Owl
    const options = {
        responsive: {
            0: {
                items: 4,
            },
            599: {
                items: 4,
            },
            1000: {
                items: 4,
            },
            1280: {
                items: 4,
            },
        },
    };

    //Add New Recipient Modal
    function recipient() {
        var srcElement = document.getElementById('new_recipient');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }


    return (
        <div className='step wizard-panel' style={{ display: "none" }}>
            <div className='content'>
                <div className='recipients_content'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <h2>Select recipients to <span>For Tom Hanks</span></h2>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12'>
                            <div className='recipients_slider_cont'>
                                <div className='main_doc'>
                                    <img src="src/images/l_document_icon_4.jpg" alt="" className='img-fluid' />
                                </div>
                                <div className='page_num'>Pages: 3/12</div>
                                <div className='carousel-item_mg'>
                                    <OwlCarousel className='recipients-carousel owl-carousel owl-theme' loop responsive={options.responsive} margin={10} dots={false} nav={true} items={4} responsiveClass={true}>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                        <div className='item'><img src="src/images/document_icon_4.svg" alt="" /></div>
                                    </OwlCarousel>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12'>
                            <div className='recipients_scroll'>
                                <div className='top_sec'>
                                    <div className='workflow'>
                                        <label>Workflow :</label>
                                        <select className='hlfinput'>
                                            <option>Standard</option>
                                        </select>
                                    </div>
                                    <div className='page_number'>
                                        <label>Add Page Number :</label>
                                        <label className="switch">
                                            <input type="checkbox" />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                                <div className='middle_sec'>
                                    <h2>Recipients</h2>
                                    <div className="btn-lg-scet">
                                        <button className="btn">ADD FROM CONTACTS</button>
                                        <button className="btn" onClick={recipient}>Add New Recipient</button>
                                    </div>
                                    <div id="new_recipient" className='new_recipient' style={{ display: 'none' }}>
                                        <div className='recipient_content'>
                                            <div className='top_bar'>
                                                <h2>Add New Recipient </h2>
                                                <button type='button' className='add_btn'>Add</button>
                                            </div>
                                            <div className='main-form'>
                                                <div className='row'>
                                                    <div className='col-lg-6 col-md-6 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="First Name" />
                                                        </div>
                                                    </div>
                                                    <div className='col-lg-6 col-md-6 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="Last Name" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="Email Address" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                        <div className='form-group'>
                                                            <input type="text" placeholder="Designation" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='row'>
                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                        <div class="paging-count d-flex">
                                                            <span class="text">Position: <input type="text" placeholder="2" /></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='content_bottom'>
                                            <div className="frt-tgl">
                                                <div className="item active">
                                                    <div class="form-group">
                                                        <input type="checkbox" id="signatory" checked />
                                                        <label for="signatory"></label>
                                                    </div>
                                                    <h6>Signatory</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                                <div className="item">
                                                    <h6>Facial Recognition</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                                <div className="item">
                                                    <h6>Photo Verification</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="toggle_scroll">
                                        <div className="sect-btn-item">
                                            <button className="tog-ttl active">NORMAL</button>
                                            <button className="tog-ttl">Secured</button>
                                        </div>
                                        <div class="sect-btn-item">
                                            <button className="tog-ttl">Sequential</button>
                                            <button className="tog-ttl active">PARALLEL</button>
                                        </div>
                                    </div>
                                </div>
                                <div className='bottom_sec'>
                                    <div className="srll-item first-srll-item">
                                        <div className="top_wrrp">
                                            <div className="headl">
                                                <h5>James Franco</h5>
                                                <p className="mail_txt">info.james@gmail.com</p>
                                            </div>
                                            <div className="headr">
                                                <div>
                                                    <h6>Signatory</h6>
                                                    <label className="switch">
                                                        <input type="checkbox" />
                                                        <span className="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="pageing-re d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: 1
                                            </div>
                                        </div>
                                    </div>

                                    <div className="srll-item first-srll-item">
                                        <div className="top_wrrp top_wrrp_othr">
                                            <div className="headl">
                                                <h5>Liam Crowe</h5>
                                                <p className="mail_txt">hello.Liam@gmail.com</p>
                                            </div>
                                            <div className="asd">
                                                <select className="hlfinput">
                                                    <option>Select</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>

                                                <div className="chos-icon">
                                                    <img src="src/images/delete_icon.svg" alt="" />
                                                    <img src="src/images/edit_icon.svg" alt="" />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="pageing-re pageing-re_othr d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: 1
                                            </div>
                                        </div>
                                        <div className="frt-tgl">
                                            <div className="item">
                                                <h6>Signatory</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Facial Recognition</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Photo Verification</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="srll-item first-srll-item">
                                        <div className="top_wrrp top_wrrp_othr">
                                            <div className="headl">
                                                <h5>Daniel Farrell</h5>
                                                <p className="mail_txt">info.farrell@gmail.com</p>
                                            </div>
                                            <div className="asd">
                                                <select className="hlfinput">
                                                    <option>Select</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>

                                                <div className="chos-icon">
                                                    <img src="src/images/delete_icon.svg" alt="" />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="pageing-re pageing-re_othr d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: 1
                                            </div>
                                        </div>
                                        <div className="frt-tgl">
                                            <div className="item">
                                                <h6>Signatory</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Facial Recognition</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Photo Verification</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="srll-item first-srll-item">
                                        <div className="top_wrrp top_wrrp_othr">
                                            <div className="headl">
                                                <h5>Channing Tatum</h5>
                                                <p className="mail_txt">hello.tatum@gmail.com</p>
                                            </div>
                                            <div className="asd">
                                                <select className="hlfinput">
                                                    <option>Select</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>

                                                <div className="chos-icon">
                                                    <img src="src/images/delete_icon.svg" alt="" />
                                                    <img src="src/images/edit_icon.svg" alt="" />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="pageing-re pageing-re_othr d-flex">
                                            <div className='page_number'>Pages:1, 2, 3, 4, 5</div>
                                            <div className="page-count">
                                                Position: 1
                                            </div>
                                        </div>
                                        <div className="frt-tgl">
                                            <div className="item">
                                                <h6>Signatory</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Facial Recognition</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="item">
                                                <h6>Photo Verification</h6>
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="navigation slide-tab-btn">
                                <button className="btn prev-btn">BACK</button>
                                <button className="btn next-btn">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export { AddDocumentsStep2 }; 