import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';




function AddDocumentsStep4() {
    //Owl
    const options2 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };
    const options3 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            1000: {
                items: 1,
            },
            1280: {
                items: 1,
            },
        },
    };
    return (
        <div className='step wizard-panel' style={{ display: "none" }}>
            <div className='content'>
                <div className='prepare_document'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <h2>Prepare document <span>For Tom Hanks</span></h2>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-9 col-lg-7 col-md-7 col-sm-12'>
                            <div className='prepare-slide_pln position-relative'>
                                <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options2.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true}>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                    <div className='item'><img src="src/images/l_document_icon_4.jpg" alt="" /></div>
                                </OwlCarousel>

                                <div className='share_document'>
                                    <h2>Share Document</h2>
                                    <div className='row'>
                                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                            <OwlCarousel className='document-carousel owl-carousel owl-theme' loop responsive={options3.responsive} margin={20} dots={false} nav={true} items={4} responsiveClass={true}>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                                <div className='item'><img src="src/images/document_icon_lg.svg" alt="" /></div>
                                            </OwlCarousel>
                                        </div>
                                        <div className='col-xl-6 col-lg-12 col-md-6 col-sm-12'>
                                            <div class="slide_text_artcl">
                                                <div class="wap">
                                                    <h6>3 pages</h6>
                                                    <h5>Pages : 2 Recipients : 1</h5>
                                                    <p>Name: Daniel Farrell</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                            <div className='share_document_form'>
                                                <form>
                                                    <div className='row'>
                                                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Subject</label>
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Remind Frequency</label>
                                                                <select className='hlfinput'>
                                                                    <option>Daily</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Keywords</label>
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <label>Keywords</label>
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <div className='form-group'>
                                                                <textarea />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='row'>
                                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                            <button type="submit" class="subBtn">Send</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-3 col-lg-5 col-md-5 col-sm-12'>
                            <div className='prepare-be-document'>
                                <h2>Recipients</h2>
                                <ul>
                                    <li>
                                        <span>1</span>
                                        <div class="text_artle">
                                            <h6>Liam Crowe</h6>
                                            <p>hello.Liam@gmail.com</p>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="num_cotn">2</span>
                                        <div class="text_artle">
                                            <h6>Daniel Farrell</h6>
                                            <p>hello.farrell@gmail.com</p>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="num_cotn">3</span>
                                        <div class="text_artle">
                                            <h6>Chris Pine</h6>
                                            <p>info.pine@gmail.com</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" class="subBtn">Submit
                                <div class="icon">
                                    <span><img src="src/images/right_arrow.svg" /></span>
                                </div>
                            </button>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="navigation slide-tab-btn">
                                <button className="btn">BACK</button>
                                <button className="btn">cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { AddDocumentsStep4 }; 