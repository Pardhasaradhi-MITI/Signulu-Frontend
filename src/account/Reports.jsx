import React, { useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
 import {Tooltip as Tooltip1} from "@material-ui/core";
import { accountService, alertService } from '@/_services';
import {reportsservice} from './../_services/ReportsServices';
import {usersService} from './../_services/users.service';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import Pagination from "react-responsive-pagination";  
import * as _ from 'lodash';
import ReactTooltip from 'react-tooltip';
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    PieChart, Pie, Cell,ResponsiveContainer,
    LineChart,
    Line,
    LabelList,
  } from "recharts";
import { useState } from 'react';
import moment from 'moment';
import { useHistory} from 'react-router-dom';
import { result } from 'lodash';

function Reports({ history, location }) {
    const History = useHistory();
       const [pichartdata,setpichartdata] = useState();
       const [totalpichartdocumentcount,settotalpichartdocumentcount]=useState();
       const [DocumentYearData,setDocumentYearData]=useState();
       const [totalYeardrafts,settotalYeardrafts]=useState();
       const [totalYearpending,settotalYearpending]=useState();
       const [totalYearapproved,settotalYearapproved]=useState();
       const [totalYearrejected,settotalYearrejected]=useState();
       const [totalYearcancelled,settotalYearcancelled]=useState();
       const [Selectedyear,setSelectedyear]=useState(2022);
       const [reportslist,setreportslist]=useState([]);

       const [pageNumber,setPageNumber]=useState(1)
       const [PageCount,setPageCount]=useState(1)
       const [perpage,setperpage]=useState([])
       const [sortname,setsortname]=useState()
       const [sortOrder,setsortOrder]=useState('desc')
       const [cardsdata,setcardsdata]=useState([])
       const [loginuserdata,setloginuserdata]=useState({})
       const [documenttractchartyears,setdocumenttractchartyears]=useState([])


    useEffect(()=>{
        getpichartinfo()
        getyearlydocumentstatusdata()
        getreportslist()
        getcardsdata()
        getloginuserdata()
        gettrackchartyeardropdown()
    },[])
    useEffect(()=>{
        getyearlydocumentstatusdata()
    },[Selectedyear])
    useEffect(()=>{
        sortthedata()
    },[sortname,sortOrder])
     
    const getcardsdata=()=>{
       reportsservice.getdataforcards().then(result=>{
        if(result){
        console.log(result,"testing")
        setcardsdata(result?.Entity)
        }
       }).catch({

       })
    }
    const getloginuserdata=()=>{
        usersService.getCurrentUserData()
        .then((result) => {
            if(result){
                setloginuserdata(result.Data)
            }
        })
    }

    const sortthedata=()=>{
        //  let sortdata= _.sortBy(reportslist, sortname);
         let sortdata=_.orderBy(reportslist, sortname, sortOrder);
        // let sortdata=_.sortBy(reportslist, [function(o) { return o.sortname; }]);
         setperpage(sortdata.slice(0,10));
         setreportslist(sortdata);
    }
    const gettrackchartyeardropdown=()=>{
        reportsservice.getdocumenttractchart().then(result=>{
            if(result?.Entity){
             setdocumenttractchartyears(result?.Entity?.Years)
             let length=result?.Entity?.Years.length-1
             setSelectedyear(result?.Entity?.Years[length])
            }
        })
    }
   const getpichartinfo=()=>{
    const Subscriptiondata =[]
    reportsservice.getpichartdocumentlist().then(result=>{
      if(result?.Entity){
        if(result?.Entity?.ADocumentTrackPieChart){
            let data=result?.Entity?.ADocumentTrackPieChart;
       
      if(data.length!==0)
       for(let i=0;i<=data.length-1;i++){
        let name;
        let value;
        let type;
      
        data[i].map((item,index)=>{
            if(index===0){
                name=item
                type=item
            }
            if(index===1){
                value=item
            }
        })
      
       let obj={name:name,value:value,Packagename:type}
        Subscriptiondata.push(obj)
       }
       
    }  
    let Totaldocuments=0   
    Subscriptiondata.map(item=>{
        Totaldocuments += item.value
    })
    settotalpichartdocumentcount(Totaldocuments)
         setpichartdata(Subscriptiondata)
      }
    })
   }
    const getyearlydocumentstatusdata=()=>{
        let DocumentYearData=[];
        reportsservice.getmultibarchartdoclist(Selectedyear).then(result=>{
            if(result?.Entity){
              let monthnames=result?.Entity?.x;
              let data=result?.Entity?.ADocumentTrackChartDetails
              let a;
                let b;
                let c;
                let d;
                let e;
              for(let i=1;i<=monthnames.length-1;i++){
                
                   data.map((item,index)=>{
                    if(index ===0 ){
                     a=item[i]
                    }
                    if(index ===1 ){
                       b=item[i] 
                    }
                    if(index ===2 ){
                        c=item[i]
                    }
                    if(index ===3 ){
                       d=item[i] 
                    }
                    if(index ===4 ){
                        e=item[i]
                    }
                   })
                   let obj={name:monthnames[i],Drafts:a,Pendind:b,Approved:c,Rejected:d,Cancelled:e}
                   console.log(obj,"obj11")
                   DocumentYearData.push(obj)
              }
             let totaldrafts=0;
             let totalpending=0
             let totalapproved=0;
             let totalrejected=0;
             let totalcancelled=0;
             DocumentYearData.map(item=>{
               totaldrafts +=item.Drafts;
               totalpending +=item.Pendind;
               totalapproved +=item.Approved;
               totalrejected +=item.Rejected;
               totalcancelled += item.Cancelled;
             })
             settotalYeardrafts(totaldrafts);
             settotalYearpending(totalpending);
             settotalYearapproved(totalapproved);
             settotalYearrejected(totalrejected);
             settotalYearcancelled(totalcancelled);
             setDocumentYearData(DocumentYearData)

            }
        })
    }
    const getreportslist=()=>{
     reportsservice.getreportlist().then(result=>{
       if(result?.Entity){
        setperpage(result?.Entity.slice(0,10));
        setreportslist(result?.Entity)
        let pages = 0;
        for(let i=1;i<Math.ceil(result?.Entity?.length/10)+1;i++){
            pages = i
        }
        setPageCount(pages)
       }
     })
    }
    const handlePagination = (newPage) => {
        setPageNumber(newPage);
            setperpage([])
            setperpage(reportslist.slice((newPage*10)-10,newPage*10));
       }
   const RADIAN = Math.PI / 180;
   const renderCustomizedLabel = ({
     cx,
     cy,
     midAngle,
     innerRadius,
     outerRadius,
     percent,
     index
   }) => {
    const radius = 25+innerRadius + (outerRadius - innerRadius);
    //  const radius = innerRadius + (outerRadius - innerRadius)* 0.5;
     const x = cx + radius * Math.cos(-midAngle * RADIAN);
     const y = cy + radius * Math.sin(-midAngle * RADIAN);
   
     return (
       <text
       fontSize={"10px"}
       x={x}
       y={y}
       fill="#212529"
    //    textAnchor={x > cx ? "middle" : "middle"}
       textAnchor={x > cx ? "start" : "end"}
       dominantBaseline="central"
       >
         {`${(percent * 100).toFixed(0)}%`}
         {/* {data[index].name} ({value}) */}
       </text>
     );
   };
   const COLORS = ["rgb(93, 98, 181)","rgb(41, 195, 190)", "#FFBB28","#FF8042","#0088FE", "#00C49F", ];
// for Custom tooltip for graphs
const CustomTooltip = ({ active, payload, label }) => {
    let payload1= payload !== null ?payload.length>0&&payload[0].payload:""
    if (active && payload && payload.length) {
      return (
        <p style={{
        border:"1px solid #C9CBCE",
        backgroundColor:"white",
        padding:10,
        color:"#737B88",

        }}>
            <div className='chart_content'>
                <div className='flex mb-1 -mt-1'>
             <h6 >{payload1.name}</h6>
             <h6 >{payload1.Approved+payload1.Drafts+payload1.Pendind+payload1.Rejected+payload1.Cancelled}</h6>
             </div>
                 <ul>
                 <li><span style={{backgroundColor:`${COLORS[0]}`}} >{payload1.Drafts}</span>Draft</li>
                 <li><span style={{backgroundColor:`${COLORS[1]}`}}>{payload1.Pendind}</span>Pending</li>
                  <li><span style={{backgroundColor:`${COLORS[2]}`}}>{payload1.Approved}</span>Approved</li>
                  <li><span style={{backgroundColor:`${COLORS[3]}`}} >{payload1.Rejected}</span>Rejected</li>
                  <li><span style={{backgroundColor:`${COLORS[4]}`}}>{payload1.Cancelled}</span>Cancelled</li>
                </ul>
              </div>
            {/* <div>{payload1.name}</div>
         <div>{`Approved: ${payload1.Approved}`}</div>
         <div>{`Drafts: ${payload1.Drafts}`}</div>
         <div>{`Pendind: ${payload1.Pendind}`}</div>
         <div>{`Rejected: ${payload1.Rejected}`}</div>
         <div>{`Cancelled: ${payload1.Cancelled}`}</div> */}
        </p>
      );
    }
  
    return null;
  };
  function dropdown2(ADocumentId,index) {
    var srcElement = document.getElementById(`${"expand"+ADocumentId}`);
    var srcimageelement = document.getElementById(`${"expand"+ADocumentId+index}`);
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = "none";
        } else {
            srcElement.style.display = "block";
        }
        return false;
    }
}
// const Redirecttomanagebar=(name)=>{
   
//     History.push("manage", { state:name == "Draft"?"DRAFT":name == "Pending"?"WAITFOROTHERS":name== "Approved"?"MYDOCUMENTS":name=="Rejected"?"REJECTED":name=="Cancelled"?"CANCELLED":"" ,loginuseremail:loginuserdata.EmailAddress });
// }
const Redirecttomanage=(name)=>{
   
    History.push("manage", { state:name == "Draft"?"DRAFT":name == "Pending"?"WAITFOROTHERS":name== "Approved"?"MYDOCUMENTS":name=="Rejected"?"REJECTED":name=="Cancelled"?"CANCELLED":"" ,loginuseremail:loginuserdata.EmailAddress });
}
const defaultdata = [{ name: "Group A", value: 400 }]
const dCOLORS = ["#f0f5f5"];

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content reports pt-0">
                    <div className="main-content super_admin_dashboard p-0">
                <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className=' flex'>
                            <h3 style={{fontSize:35,fontWeight:300,marginBottom:40}}>Dashboard</h3>
                            <h3><button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"
                              style={{ padding:"10px 10px", height: "36px",border:"1px solid black",width:"37px"}}
                            ><img src="src/images/home_icon.svg" alt=""   style={{marginRight:"2px",marginBottom:"10px",width: "15px",height: "15px"}} /></button>
                            
                            </h3></div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item customer">
                                    <div className='content' style={{cursor: "default",userSelect:"none"}}>
                                    <img src="src/images/DocumentsProcessed.svg" alt="" />
                                        {/* <img src="src/images/customer.svg" alt="" /> */}
                                        <h5>{cardsdata?.MonthlyTotalDocuments}/{cardsdata?.YearlyTotalDocuments}</h5>
                                        <p>Documents processed MTD/YTD</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item user">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/userss.svg" alt="" />
                                        <h5>{cardsdata?.TotalActiveUsers+cardsdata?.TotalInActiveUsers}</h5>
                                        <p>No. of Users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item users">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/DailyActiveUsers.svg" alt="" />
                                        <h5>{cardsdata?.TotalActiveUsers}</h5>
                                        <p>No. of Active Users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item duser">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/DocumentsUser.svg" alt="" />
                                        <h5>{Math.round(cardsdata.YearlyTotalDocuments/(cardsdata?.TotalActiveUsers+cardsdata?.TotalInActiveUsers))}</h5>
                                        <p>Documents/User</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item musers">
                                    <div className='content' style={{cursor:"default",userSelect:"none",visibility:"hidden"}}>
                                        <img src="src/images/MonthlyActiveUsers.svg" alt=""  />
                                        <h5>300</h5>
                                        <p>Monthly Active Users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item documents">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/DocumentsProcessed.svg" alt="" />
                                        <h5>460</h5>
                                        <p>No. Of Documents Processed</p>
                                    </div>
                                </div>
                            </div>
                        </div> */}
                       
                    </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className='document_block'>
                                <h3 style={{fontSize:"25px"}}>Document Status - {moment(new Date()).format("MMM , YYYY")}</h3>
                                <div className='chart'>
                                    <div className='chart_picture' style={{width:300, height:260}} >
                                    <ResponsiveContainer>
                                       <PieChart className='flex' width={900} height={450}>
                                        {totalpichartdocumentcount>0?<Pie dataKey="value"
                                         data={pichartdata}
                                         innerRadius={70}
                                            outerRadius={100}
                                            labelLine={true}
                                          fill="#8884d8"
                                          paddingAngle={1}
                                           label={renderCustomizedLabel}
                                            >
                                          {pichartdata?.map((entry, index) => (
                                            console.log(entry,"entry"),
                                            <Cell style={{cursor:"pointer"}} key={`cell-${index}`} fill={COLORS[index % COLORS.length]}
                                             onClick={()=>{Redirecttomanage(entry.name)}} />
                                             ))}
                                         </Pie>:
                                         <Pie dataKey="value"
                                         data={defaultdata}
                                         innerRadius={70}
                                            outerRadius={100}
                                            labelLine={false}
                                          fill="#8884d8"
                                          paddingAngle={0}
                                           //label={renderCustomizedLabel}
                                            >
                                          {defaultdata?.map((entry, index) => (
                                            <Cell key={`cell-${index}`} fill={dCOLORS[index % dCOLORS.length]} />
                                             ))}
                                         </Pie>
                                         }
                                        </PieChart>
                                     </ResponsiveContainer>
                                        {/* <img src='src/images/chart_1.jpg' alt='' /> */}
                                    </div>
                                    <div className='chart_content' >
                                        <h5 style={{paddingLeft:"20px"}} >{totalpichartdocumentcount}</h5>
                                        <ul style={{paddingLeft:"6px"}}>
                                            {pichartdata !== undefined &&pichartdata.map((item,index)=>{
                                                return  <li><span className='active' style={{backgroundColor:`${COLORS[index]}`}}>{item.value}</span>{item.name}</li>
                                            })}
                                            
                                            {/* <li><span className='active'>48</span>Active</li>
                                            <li><span className='under_contract'>22</span>Under contract</li>
                                            <li><span className='review'>14</span>Review</li>
                                            <li><span className='under_contract'>22</span>Under contract</li>
                                            <li><span className='review'>14</span>Review</li> */}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className='document_block'>
                                <div className='flex'>
                                <h3 style={{fontSize:"25px"}}>{moment(new Date()).format("YYYY")}-Document Volume</h3>
                                         <div className='' >
                                            <select className="hlfinput" style={{cursor:"pointer"}} value={Selectedyear}
											 onChange={(e) => setSelectedyear(e.target.value)}>
                                              <option  value={0}>Select Year</option>
                                              {documenttractchartyears.map(item=>{
                                                return( <option  value={item}>{item}</option>)
                                              })}
                                              
                                             </select>
                                          </div>
                                         </div>
                                <div className='chart'>
                                    <div className='chart_picture -mr-2' style={{width:510,height:250}}>
                                    <ResponsiveContainer><BarChart
                                                        width={700}
                                                        height={250}
                                                        data={DocumentYearData}
                                                        margin={{
                                                            top: 5,
                                                            bottom: 5
                                                        }}
                                                        barSize={18}
                                                       >
                                                        <XAxis  dataKey="name" scale="point"  padding={{ left:30, right:30 }} />
                                                        <YAxis  type='number' />
                                                        <Tooltip content={CustomTooltip}  />
                                                        {/* <Legend  /> */}
                                                        {/* <Legend layout='horizontal' verticalAlign="top" height={36} align="left" /> */}
                                                        {/* <CartesianGrid strokeDasharray="1" /> */}
                                                        <Bar style={{cursor:"pointer"}} onClick={()=>{Redirecttomanage("Draft")}} stackId="a"  dataKey="Drafts" fill={`${COLORS[0]}`}/>
                                                        <Bar style={{cursor:"pointer"}} onClick={()=>{Redirecttomanage("Pending")}} stackId="a"  dataKey="Pendind" fill={`${COLORS[1]}`}/>
                                                        <Bar style={{cursor:"pointer"}} onClick={()=>{Redirecttomanage("Approved")}} stackId="a"  dataKey="Approved" fill={`${COLORS[2]}`}/>
                                                        <Bar style={{cursor:"pointer"}} onClick={()=>{Redirecttomanage("Rejected")}} stackId="a"  dataKey="Rejected" fill={`${COLORS[3]}`}/>
                                                        <Bar style={{cursor:"pointer"}} onClick={()=>{Redirecttomanage("Cancelled")}} stackId="a"  dataKey="Cancelled" fill={`${COLORS[4]}`}/>
                                                    </BarChart>
                                                    </ResponsiveContainer>
                                                    
                                    </div>
                                    
                                    {/* <div className='chart_content p-0'>
                                        <h5>{totalYeardrafts+totalYearpending}</h5>
                                        <ul>
                                            <li><span style={{backgroundColor:`${COLORS[0]}`}} className='cancelled'>{totalYeardrafts}</span>Draft</li>
                                            <li><span style={{backgroundColor:`${COLORS[1]}`}}className='rejected'>{totalYearpending}</span>Pending</li>
                                            <li><span style={{backgroundColor:`${COLORS[2]}`}}className='pending'>{totalYearapproved}</span>Approved</li>
                                            <li><span style={{backgroundColor:`${COLORS[3]}`}} className='pending'>{totalYearrejected}</span>Rejected</li>
                                            <li><span style={{backgroundColor:`${COLORS[4]}`}}className='pending'>{totalYearcancelled}</span>Cancelled</li>
                                        </ul>
                                    </div> */}
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                     {/*<div className='row'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <h2>Transactions Info</h2>
                            <select className='hlfinput'>
                                <option>January</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <h5>Transactions Closed</h5>
                            <div className='transactions_block'>
                                <div className='chart_content'>
                                    <h5>88</h5>
                                    <h6><span>+7.58</span> (50)</h6>
                                    <h3>$12,325,655</h3>
                                </div>
                                <div className='chart'>
                                    <img src='src/images/chart_3.jpg' alt='' />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <h5>New Listings</h5>
                            <div className='transactions_block'>
                                <div className='chart_content'>
                                    <h5>68</h5>
                                    <h6><span>+7.58</span> (50)</h6>
                                    <h3>$12,325,655</h3>
                                </div>
                                <div className='chart'>
                                    <img src='src/images/chart_3.jpg' alt='' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <h5>Transactions Expired</h5>
                            <div className='transactions_block'>
                                <div className='chart_content'>
                                    <h5>45</h5>
                                    <h6><span>+7.58</span> (50)</h6>
                                    <h3>$12,325,655</h3>
                                </div>
                                <div className='chart'>
                                    <img src='src/images/chart_3.jpg' alt='' />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <h5>Transactions Lost</h5>
                            <div className='transactions_block'>
                                <div className='chart_content'>
                                    <h5>65</h5>
                                    <h6><span>+7.58</span> (50)</h6>
                                    <h3>$12,325,655</h3>
                                </div>
                                <div className='chart'>
                                    <img src='src/images/chart_3.jpg' alt='' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className='document_block'>
                                <h3><strong>Users</strong></h3>
                                <div className='chart'>
                                    <div className='chart_picture'>
                                        <img src='src/images/chart_4.jpg' alt='' />
                                    </div>
                                    <div className='chart_content'>
                                        <h5>780</h5>
                                        <ul>
                                            <li><span className='active'>520</span>Active</li>
                                            <li><span className='rejected'>100</span>Inactive</li>
                                            <li><span className='cancelled'>160</span>Suspended</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className='document_block'>
                                <h3><strong>Users</strong></h3>
                                <div className='chart'>
                                    <div className='chart_picture'>
                                        <img src='src/images/chart_5.jpg' alt='' />
                                    </div>
                                    <div className='chart_content'>
                                        <h5>185</h5>
                                        <ul>
                                            <li><span className='review'>122</span>Signers</li>
                                            <li><span className='pending'>45</span>Editor</li>
                                            <li><span className='under_contract'>28</span>Admins</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className='document_block'>
                                <h3><strong>Storage </strong></h3>
                                <div className='chart'>
                                    <div className='chart_picture'>
                                        <img src='src/images/chart_6.jpg' alt='' />
                                    </div>
                                    <div className='chart_content'>
                                        <h5>1000</h5>
                                        <ul>
                                            <li><span className='rejected'>850</span>Total Uses</li>
                                            <li><span className='active'>150</span>Free Space</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className='document_block'>
                                <h3><strong>Membership Licence</strong></h3>
                                <div className='chart'>
                                    <div className='chart_picture'>
                                        <img src='src/images/chart_7.jpg' alt='' />
                                    </div>
                                    <div className='chart_content'>
                                        <h5>120</h5>
                                        <ul>
                                            <li><span className='active'>102</span>Active Licence</li>
                                            <li><span className='cancelled'>18</span>Inactive </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                    <div className='row'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <h3 style={{fontSize:"25px" ,fontWeight:300}}>Transactions Reports</h3>
                            <div className='action_area'>
                                {/* <ul>
                                    <li><img src='src/images/xls.svg' alt='' /></li>
                                    <li><img src='src/images/pdf.svg' alt='' /></li>
                                    <li><img src='src/images/csv.svg' alt='' /></li>
                                </ul> */}
                                {/* <select className='hlfinput'>
                                    <option>December</option>
                                </select> */}
                            </div>

                        </div>
                    </div>
                    <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div className="table__row__conts table-responsive">
                    <table className="table">
                        <thead style={{visibility:"collapse"}} className='first_head'>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style={{display:"none"}}>
                                                {/* 1-8 on <span>8</span> */}
                                                </th>
                                                
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th style={{cursor:"auto",position:"relative",textAlign:"start",fontSize:14,paddingLeft:47}}>Document</th>
                                            <th className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>Status</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14}}>Recipients </th> 
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14}}>Created On</th>
                                            <th className='px-5'  style={{position:"relative",textAlign:"start",fontSize:14}}>Updated On</th>
                                           
                                           <th style={{display:"none"}}></th> {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead>
                            <tbody>
                                {perpage.length>0&&perpage.map((item,index)=>{
                                   return(
                                    <>
                                    <tr>
                                     <td  style={{position:"relative",textAlign:"start",width:"28%"}}
                                    //    onClick={()=>{dropdown4(item.ADocumentId)}}
                                       >
                                               {/* <img src='src/images/down_arrow.svg' alt=""  style={{padding:5,border:"1px solid #c7a9a9 ",backgroundColor:"#ededed"}}/> */}
                                               <button style={{padding:0}} onClick={()=>{dropdown2(item.ADocumentId,index)}} id={`${"dropdownMenuButton1"+item.ADocumentId+index}`} data-bs-toggle="dropdown" aria-expanded="false" className="btn doc_btn"><img   src="src/images/plus_icon.svg" alt="" /></button>
                                                <label className='px-2' style={{cursor:"auto"}}  htmlFor="check1">
                                                {item.Title!== null&&item.Title.length>25?
                                                <Tooltip1 placement="top" title={item.Title} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        width: 180,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        marginBottom:"-4px",
                                                        color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item.Title}
                                                    </div>
                                                </Tooltip1>:item.Title}
                                                
                                                </label>
                                                </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start"}}>{item.StatusName}</td>
                                              <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item.Details?.length}
                                                
                                             </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>{
                                                <><div>{moment.utc(item?.CreatedDate).local().format('MM/DD/YYYY')}</div>
                                                <div>{moment.utc(item?.CreatedDate).local().format('h:mm A')}</div></>
                                             }</td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>{
                                                <><div>{item.CreatedDate !== null ?(moment.utc(item?.CreatedDate).local().format('MM/DD/YYYY')):(moment.utc(item?.CreatedDate).local().format('MM/DD/YYYY'))}</div>
                                                <div>{item.CreatedDate !== null ?(moment.utc(item?.CreatedDate).local().format('h:mm A')):(moment.utc(item?.CreatedDate).local().format('h:mm A'))}</div></>
                                             }</td>
                                            
                                               
                                </tr>
                                <tr  id={`${"expand"+item.ADocumentId}`} style={{display:"none"}}>
                                    <table style={{width:600}}>
                                            <thead style={{visibility:"collapse"}} className='first_head'>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style={{display:"none"}}>
                                                {/* 1-8 on <span>8</span> */}
                                                </th>
                                                
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                           <th style={{fontSize:14,paddingLeft:"46px",textAlign:"start",cursor:"auto"}}>Name</th>
                                            <th className='px-2.5' style={{textAlign:"start",fontSize:14,padding:"0px",width:"300px"}}>Email</th>
                                            <th className='px-2.5'  style={{textAlign:"start",fontSize:14,padding:"0px"}}>Status </th>
                                           <th style={{display:"none"}}></th> {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead> 
                                    <tbody>
                                {item.Details.length>0&&item.Details.map((item1,index)=>{
                                   return(
                                   <> <tr>
                                       <td style={{paddingLeft:"46px"}} >{item1.FirstName +" "+ item1.LastName}</td>
                                      <td style={{padding:"0px"}}>{item1.EmailAddress}</td>
                                      <td className='flex justify-content-start'>{item1.StatusName}
                                      {item1.ReassignStatus==2?<><div> <img data-tip data-for={`${index}`}src="src/images/info.svg" alt='' /></div>
                                                                    <ReactTooltip id={`${index}`} place="left" effect="solid">
                                                                        <div>Reassigned by : {item1.ReassignedBy}</div>
                                                                    </ReactTooltip></>:""}
                                      </td>
                                           
                                    </tr>
                                    {item1.ReassignStatus==2?
                                   <tr>
                                     <td style={{paddingLeft:"46px"}} >{item1.ReassignedBy}</td>
                                      <td style={{padding:"0px"}}>{item1.ReassignMemberEmail}</td>
                                      <td className='flex justify-content-start'>Reassigned
                                      </td>
                                    </tr>:""}</>
                                    )
                                })}
                                    </tbody>
                                            </table>
                                </tr>
                                </>

                                   )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                    </div>
                    {reportslist.length>10&&<div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="document-nagin">
                                <nav aria-label="Page navigation">
                                <Pagination
												current={pageNumber}
												total={PageCount}
												onPageChange={(newPage) => handlePagination(newPage)}
                                                maxWidth={"80px"}
											/>
                                </nav>
                            </div>
                        </div>
                    </div>}
                </div>
            </div>
        </div>
    )
}

export { Reports }; 