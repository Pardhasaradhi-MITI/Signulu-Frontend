import React, { useEffect,useState } from "react";
import "../css/AadhaarInfo.css"
import { Link, useLocation, useParams ,useHistory} from 'react-router-dom';
import { Button,Modal} from 'react-bootstrap';
import {addDocumentService} from "./../../src/_services/adddocument.service"

 let DocId=""
let PID=""
let ReqID=""

 function AadhaarInfo(){
    const [Aadhaarinfodata,setAadhaarinfodata]=useState()
    const [Image,setImage]=useState("")
    const usehistory = useHistory();
    const [successPopupEnable,setSuccessPopupEnable]=useState(false)
    const [failurePopupEnable,setFailurePopupEnable] = useState(false)
    
    let { id } = useParams();
    const search = useLocation().search;
    const searchParams = new URLSearchParams(search);
    if(searchParams.get("did")) {
        DocId = searchParams.get("did")
    }
    if(searchParams.get("rid")) {
        PID = searchParams.get("rid")
    }
    if(searchParams.get("requestId")) {
        ReqID = searchParams.get("requestId")
        console.log(ReqID,"2020")
    }
    useEffect(() => {
        addDocumentService.getekycverify(DocId,PID,ReqID).then(res=>{
            if(res?.Entity){
                setAadhaarinfodata(res?.Entity)
                setImage(res?.Entity?.photo)
            }
        })
    },[])
    function Redirecttopopupmsg(){
        let data=Aadhaarinfodata
        data.ADocumentId=parseInt(DocId)
        data.RecipientId=parseInt(PID)
       addDocumentService.ekycdobandusernameverify(data).then(result=>{
        if(result){
            if(result?.Entity?.status === "Success"){
                setSuccessPopupEnable(true)
            }else{
                setFailurePopupEnable(true)
            }
           
        }
       })
    }
    function redirectttologinpage(){
        usehistory.push(`/account/login`)
     }
    function redirecttoanauthpage() {
        let string=`PID=${DocId}&DOCID=${PID}`
        let encodedstring = btoa(string);
        usehistory.push(`/account/unauthapprove?q=${encodedstring}`);
     }
    
return(
    <div style={{overflowX:"hidden"}}>
    <div className="row coloradd1">
                    <div className="column image2">
                        <img src="src/images/signulu_black_logo2.svg" alt="" className="img-fluid3 "/>
                    </div>
                   </div>
                   <div className='align'>
                    <div style={{marginTop:"-70px"}}><center >
                        <div>
                            <h4 >Information as per Aadhaar</h4>
                           
                            </div>
                            <br/>
                            <table className="table" style={{width:800}}>
                        <thead style={{visibility:"collapse"}} className='first_head'>
                                        <tr>
                                            <th></th>
                                            <th ></th>
                                            <th></th>
                                            <th></th>
                                                
                                        </tr>
                                    </thead>
                                    <thead className='first_head'>
                                        <tr>
                                            <th style={{verticalAlign:"middle", borderBottom:"1px solid black",borderLeft:"1.5px solid black",borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"rgb(14 15 17)"}}>Full Name</th>
                                            <th className='px-5' style={{ verticalAlign:"middle", borderBottom:"1px solid black" ,borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:16,fontWeight:400,color:"#515762"}}>{Aadhaarinfodata?.fullName}</th>
                                            <th className='px-5'  style={{verticalAlign:"middle", borderBottom:"1px solid black",borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"rgb(14 15 17)"}}>Date of birth</th>
                                            <th className='px-5'  style={{verticalAlign:"middle",borderRight:"1.5px solid black"  ,borderRight:"1px solid black", borderBottom:"1px solid black",position:"relative",textAlign:"start",fontSize:16,fontWeight:400,color:"#515762"}}>{Aadhaarinfodata?.dob}</th>  
                                           
                                        </tr>
                                        <tr>
                                            <th style={{verticalAlign:"middle",borderBottom:"0.5px solid black",borderLeft:"1.5px solid black"  ,borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"rgb(14 15 17)"}}>Gender</th>
                                            <th className='px-5' style={{verticalAlign:"middle",borderBottom:"0.5px solid black" ,borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:16,fontWeight:400,color:"#515762"}}>{Aadhaarinfodata?.gender}</th>
                                            <th className='px-5'  style={{verticalAlign:"middle",borderBottom:"0.5px solid black",borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"rgb(14 15 17)"}}>Aadhaar Number</th>
                                            <th className='px-5'  style={{verticalAlign:"middle",borderRight:"1.5px solid black"  , borderBottom:"0.5px solid black",position:"relative",textAlign:"start",fontSize:16,fontWeight:400,color:"#515762"}}>{Aadhaarinfodata?.aadhaarNumber}</th>  
                                           
                                        </tr>
                                        <tr>
                                            <th style={{verticalAlign:"middle",borderBottom:"1px solid black",borderLeft:"1.5px solid black"  ,borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"rgb(14 15 17)"}}>Photograph</th>
                                            <th className='px-5' style={{verticalAlign:"middle",borderBottom:"1px solid black" ,borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:16,fontWeight:400,color:"#515762"}}>{<img src={Image}/>}</th>
                                            <th className='px-5'  style={{verticalAlign:"middle",borderBottom:"1px solid black",borderRight:"0.5px solid black",position:"relative",textAlign:"start",fontSize:14,fontWeight:400,color:"rgb(14 15 17)"}}>Address</th>
                                            <th className='px-5'  style={{verticalAlign:"middle",borderRight:"1.5px solid black"  , borderBottom:"1px solid black",position:"relative",textAlign:"start",fontSize:16,fontWeight:400,color:"#515762"}}>{Aadhaarinfodata?.address}</th>  
                                           
                                        </tr>
                                       
                                    </thead>
                          </table>
                     </center>
                    </div>
                </div> 
                <div className="text-center btn-space" >
                {/* <Button variant="primary" type="button"  style={{background: '#808285', border: 'none',margin:7}} >CANCEL</Button>  */}
                <Button variant="secondary" type="button" onClick={Redirecttopopupmsg}style={{background: '#F06B30', border: 'none',margin:7}} >CONFIRM</Button>
                                        </div>
                   <Modal show={successPopupEnable}  onHide={() => { setSuccessPopupEnable(false) }}
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName="medium_size_dailog"
                // aria-labelledby="contained-modal-title-vcente"
                centered>
                {/* <Modal.Header style={{justifyContent:"center"}}>
                    <Modal.Title id="contained-modal-title-vcente">
                         <div>
                            <p>Validate KYC</p>
                        </div> 
                    </Modal.Title>
                </Modal.Header> */}
                <Modal.Body >
                <br/>
                <div style={{ justifyContent: 'center', display: 'flex',height:90,fontSize:18,verticalAlign:"middle"}}>
                 
               <p> 
               Your Aadhaar eKYC is successfully verified and you can proceed with the Signature/Approval of the document.
               </p>
                </div>
                   
                </Modal.Body>
                <Modal.Footer className='justify-content-center'>
                    <Button onClick={redirecttoanauthpage} variant="secondary" type="button"  style={{background: '#F06B30', border: 'none'}}>OKAY</Button>
                </Modal.Footer>
            </Modal>
             <Modal show={failurePopupEnable}  onHide={() => { setFailurePopupEnable(false) }}
                size="lg"
                backdrop="static"
                keyboard={false}
                dialogClassName="medium_size_dailog"
                // aria-labelledby="contained-modal-title-vcente"
                centered>
                {/* <Modal.Header style={{justifyContent:"center"}}>
                    <Modal.Title id="contained-modal-title-vcente">
                        <div>
                            <p>Validate KYC</p>
                        </div> 
                    </Modal.Title>
                </Modal.Header> */}
                <Modal.Body >
                    <br/>
                <div style={{justifyContent: 'center', display: 'flex',height:90,fontSize:18,verticalAlign:"middle"}}>
               <p> 
               Aadhaar details are incorrect, contact the initiator to restart the transaction.
                Please click on the OKAY button to get redirected to Signulu's landing page.
               </p>
                </div>
                   
                </Modal.Body>
                <Modal.Footer className='justify-content-center'>
                    <Button onClick={redirectttologinpage} variant="secondary" type="button"  style={{background: '#F06B30', border: 'none'}}  >OKAY</Button>
                </Modal.Footer>
            </Modal> 
                                        </div>
)
} 
export{AadhaarInfo}  
                                