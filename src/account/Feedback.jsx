import React, {useEffect , useState} from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { commonService } from '../_services/common.service';
import { Modal, Button } from 'react-bootstrap';
import { accountService, alertService } from '@/_services';
import {Sidebar } from './Common/Sidebar';
import {Topbar } from './Common/Topbar';
import '../css/feedback.css';
import ReactTooltip from 'react-tooltip';

function Feedback({ history, location }) {  
    const [isActive, setIsActive] = useState(false);
    const [description, setDescription]= useState();
    const [FeedbackTypeId, setFeedbackTypeId]= useState();
    const usehistory = useHistory();
    const [feedbackPopShow, setFeedbackPopShow] = useState(false);
    const handleClick = (event,id) => {
      // 👇️ toggle isActive state on click
      setIsActive(current => id);
    };
    useEffect(() => {
        commonService.getFeedBackTypes().then((success) => {
          })
    }, [])

    
    function feedbackPost() {
     
    }
    function feedbackSent() {
      const feedBackObj = { "FeedbackDescription": description,  "UserFeedbackTypeId": isActive, 'IsReact':1}
      commonService.feedBack(feedBackObj).then((success) => {
        setFeedbackPopShow(true)
        
      })
    }

   function okHandler(){
    setFeedbackPopShow(false)
        usehistory.push('/account/dashboard')
    }
    function redirect() {
        usehistory.goBack();
    }

    return (
        <div className="dashboard_home wrapper_other main-content main-from-content documentagreement document_unauthapprove" id="wrapper_other">
            {/* <Sidebar /> */}
            <div className='documentagreement-bg-color' style={{padding: '15px'}}>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='btn-group'>
                                <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block image_zoom'></img>
                                {/* <h2 style={{fontSize: '20px', paddingLeft: '30px'}}>Document Name: {documentData.Title}</h2> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="other_main feedback-sec" id="main" style={{marginTop:'20px'}}>
                {/* <Topbar /> */}

                <div className="main-content main-from-content feedback container">
                    <div className='heading'>
                        <h2>Your feedback helps us to improve</h2> 
                        {/* <p>Thank you for taking the time to tell us about your experience with the new Add Fields page.</p> */}
                    </div>
                    <div className='content'>
                        <div className='feedback-faces' id='feedback'>
                            <lable>How satisfied are you? </lable> 
                            <div className='row'> 
                                <div className='face-wrapper'>
                                    <div data-tip="Angry-Icon" data-for="Angry-Icon" className={isActive=="1" ? 'face active1' : 'face'} onClick={(e) => handleClick(e,"1")} ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 256c0 141.4-114.6 256-256 256S0 397.4 0 256S114.6 0 256 0S512 114.6 512 256zM161.3 382.1c-5.4 12.3 8.7 21.6 21.1 16.4c22.4-9.5 47.4-14.8 73.7-14.8s51.3 5.3 73.7 14.8c12.4 5.2 26.5-4.1 21.1-16.4c-16-36.6-52.4-62.1-94.8-62.1s-78.8 25.6-94.8 62.1zM176.4 272c17.7 0 32-14.3 32-32c0-1.5-.1-3-.3-4.4l10.9 3.6c8.4 2.8 17.4-1.7 20.2-10.1s-1.7-17.4-10.1-20.2l-96-32c-8.4-2.8-17.4 1.7-20.2 10.1s1.7 17.4 10.1 20.2l30.7 10.2c-5.8 5.8-9.3 13.8-9.3 22.6c0 17.7 14.3 32 32 32zm192-32c0-8.9-3.6-17-9.5-22.8l30.2-10.1c8.4-2.8 12.9-11.9 10.1-20.2s-11.9-12.9-20.2-10.1l-96 32c-8.4 2.8-12.9 11.9-10.1 20.2s11.9 12.9 20.2 10.1l11.7-3.9c-.2 1.5-.3 3.1-.3 4.7c0 17.7 14.3 32 32 32s32-14.3 32-32z"/></svg></div>
                                    <ReactTooltip id="Angry-Icon" place="top" effect="solid">
                                    Angry
                                    </ReactTooltip>
                                </div>
                                <div className='face-wrapper'>
                                    <div data-tip="Sad-Icon" data-for="Sad-Icon" className={isActive=="2" ? 'face active2' : 'face'} onClick={(e) => handleClick(e,"2")} ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM159.3 388.7c-2.6 8.4-11.6 13.2-20 10.5s-13.2-11.6-10.5-20C145.2 326.1 196.3 288 256 288s110.8 38.1 127.3 91.3c2.6 8.4-2.1 17.4-10.5 20s-17.4-2.1-20-10.5C340.5 349.4 302.1 320 256 320s-84.5 29.4-96.7 68.7zM208.4 208c0 17.7-14.3 32-32 32s-32-14.3-32-32s14.3-32 32-32s32 14.3 32 32zm128 32c-17.7 0-32-14.3-32-32s14.3-32 32-32s32 14.3 32 32s-14.3 32-32 32z"/></svg></div>
                                    <ReactTooltip id="Sad-Icon" place="top" effect="solid">
                                    Sad
                                    </ReactTooltip>
                                </div>
                                <div className='face-wrapper'>
                                    <div data-tip="Satisfied-Icon" data-for="Satisfied-Icon" className={isActive=="3" ? 'face active3' : 'face'} onClick={(e) => handleClick(e,"3")} ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 256c0 141.4-114.6 256-256 256S0 397.4 0 256S114.6 0 256 0S512 114.6 512 256zM176 384c0 8.8 7.2 16 16 16H320c8.8 0 16-7.2 16-16s-7.2-16-16-16H192c-8.8 0-16 7.2-16 16zm-16-88c39.8 0 72-32.2 72-72s-32.2-72-72-72s-72 32.2-72 72s32.2 72 72 72zm264-72c0-39.8-32.2-72-72-72s-72 32.2-72 72s32.2 72 72 72s72-32.2 72-72zm-240 0c0 13.3-10.7 24-24 24s-24-10.7-24-24s10.7-24 24-24s24 10.7 24 24zm192 0c0 13.3-10.7 24-24 24s-24-10.7-24-24s10.7-24 24-24s24 10.7 24 24z"/></svg></div>
                                    <ReactTooltip id="Satisfied-Icon" place="top" effect="solid">
                                    Satisfied
                                    </ReactTooltip>
                                </div>
                                <div className='face-wrapper'>
                                    <div data-tip="Happy-Icon" data-for="Happy-Icon" className={isActive=="4" ? 'face active4' : 'face'} onClick={(e) => handleClick(e,"4")} ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM164.1 325.5C182 346.2 212.6 368 256 368s74-21.8 91.9-42.5c5.8-6.7 15.9-7.4 22.6-1.6s7.4 15.9 1.6 22.6C349.8 372.1 311.1 400 256 400s-93.8-27.9-116.1-53.5c-5.8-6.7-5.1-16.8 1.6-22.6s16.8-5.1 22.6 1.6zM208.4 208c0 17.7-14.3 32-32 32s-32-14.3-32-32s14.3-32 32-32s32 14.3 32 32zm128 32c-17.7 0-32-14.3-32-32s14.3-32 32-32s32 14.3 32 32s-14.3 32-32 32z"/></svg></div>
                                    <ReactTooltip id="Happy-Icon" place="top" effect="solid">
                                    Happy
                                    </ReactTooltip>
                                </div>
                                <div className='face-wrapper'>
                                    <div data-tip="Joyful-Icon" data-for="Joyful-Icon" className={isActive=="5" ? 'face active5' : 'face'} onClick={(e) => handleClick(e,"5")} ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM383.8 317.8c12.3-3.7 24.3 7 19.2 18.7c-24.5 56.9-81.1 96.7-147 96.7s-122.5-39.8-147-96.7c-5.1-11.8 6.9-22.4 19.2-18.7C166.7 329.4 210.1 336 256 336s89.3-6.6 127.8-18.2zM199.3 129.1c17.8 4.8 28.4 23.1 23.6 40.8l-17.4 65c-2.3 8.5-11.1 13.6-19.6 11.3l-65.1-17.4c-17.8-4.8-28.4-23.1-23.6-40.8s23.1-28.4 40.8-23.6l16.1 4.3 4.3-16.1c4.8-17.8 23.1-28.4 40.8-23.6zm154.3 23.6l4.3 16.1 16.1-4.3c17.8-4.8 36.1 5.8 40.8 23.6s-5.8 36.1-23.6 40.8l-65.1 17.4c-8.5 2.3-17.3-2.8-19.6-11.3l-17.4-65c-4.8-17.8 5.8-36.1 23.6-40.8s36.1 5.8 40.9 23.6z"/></svg></div>
                                    <ReactTooltip id="Joyful-Icon" place="top" effect="solid">
                                    Joyful
                                    </ReactTooltip>
                                </div>
                            </div>
                        </div>
                        <div className='feedback-details'>
                            <lable style={{'lineHeight': '1.5'}}>Please leave your feedback below (Optional)</lable>
                            <textarea rows={2} onChange={(event)=>{setDescription(event.target.value)}}></textarea>
                        </div>
                        <div className="d-flex justify-content-center">
                        <div className="submit_btn"><button disabled={!isActive} onClick={feedbackSent} className={isActive ? 'feed-btn' : 'feed-btn-disabled'}>Send</button></div>
                        <button type="button" className="btn cancel-btn" onClick={() => { redirect() }}>
										CANCEL
									</button>
                        </div>
                    </div>
                    <Modal show={feedbackPopShow} onHide={() => { setFeedbackPopShow(false) }}
                size=""
                backdrop="static"
                keyboard={false}
                dialogClassName=""
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header style={{border:"0px"}}>
                </Modal.Header>
                <Modal.Body style={{ height: '120px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="d-flex justify-content-center align-items-center min-height card-img-overlay">
                            <div className="example-card ">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div">
                                        <p style={{ fontSize: '20px' }}>Thank you for your feedback</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{}}> 
                    <Button variant="secondary" style={{ background: '#F06B30', width: '6rem', border: 'none', borderRadius:'0.25rem' }} onClick={okHandler}>OKAY</Button>
                </Modal.Footer>
            </Modal>
                </div>
            </div>
        </div>

        
    )
}

export { Feedback }; 