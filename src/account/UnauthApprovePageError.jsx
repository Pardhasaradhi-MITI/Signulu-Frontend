import React, { useState, useEffect } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
function DocumentUnauthApprovePageError() {
    return (
        <div className="main-content main-from-content documentagreement">
            <div className='documentagreement-bg-color'>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='btn-group' style={{ padding: '10px' }}>
                                <img src='src/images/signulu_black_logo2.svg' alt='' className='img-fluid mx-auto d-block' style={{ zoom: '0.5' }}></img>
                                {/* <h2 style={{fontSize: '20px', paddingLeft: '30px'}}>View Document Master Agreement</h2> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="justify-content-center align-items-center min-height card-img-overlay" style={{ position: 'relative' }}>
                    <div class="example-card card-align" style={{ borderRadius: '10px' }}>
                        <div class="row">
                            <div class="col-md-12 add-recipients-div">
                                <p>Your document <b>Document</b> is already approved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

export { DocumentUnauthApprovePageError }; 