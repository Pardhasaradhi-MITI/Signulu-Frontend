import React, { useEffect, useState ,useRef} from 'react';
import { useHistory,useLocation } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Document, Page, pdfjs } from "react-pdf";
import { accountService, alertService, } from '@/_services';
import {usersService} from "../_services/users.service"
import { TemplateServise } from '../_services/TemplateServise';
import { contactsService } from '../_services/contacts.service';
import { addDocumentService } from '../_services/adddocument.service';
import OwlCarousel from 'react-owl-carousel';
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import config from 'config';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import Tooltip from "@material-ui/core/Tooltip";
import { Button, Modal, ProgressBar } from 'react-bootstrap';
import Swal from 'sweetalert2';
import moment from 'moment';
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import EllipsisText from "react-ellipsis-text";
import _ from "lodash";
import Spinner from 'react-bootstrap/Spinner';
import { commonService } from './../_services/common.service';
import Pagination from 'react-responsive-pagination';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import ReactTooltip from 'react-tooltip';
import * as EmailValidator from "email-validator";



pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`

const options = {
    responsive: {
        0: {
            items: 1,
        },
        599: {
            items: 2,
        },
        1000: {
            items: 3,
        },
        1280: {
            items: 4,
        },
    },
};

function Template({ history, location }) {
    const wrapperRef1 = useRef(null);
    const handle = useFullScreenHandle();
    const usehistory = useHistory();
    const location1 = useLocation();
    const [templates,settemplates]=useState([]);
    const [stateupdate,setstateupdate]=useState(false);
    const [viewPdf, setviewPdf] = React.useState(false);
    const [documentRename, setdocumentRename] = useState(false);
    const [selectDocumentFile, setSelectDocumentFile] = useState('');
    const [popDocPages, setPopDocPages] = React.useState(1);
    const [userdata,setuserdata]=useState({})
    const [selectDocumentIndex, setSelectDocumentIndex] = useState('');
    const [selectDocument,setselectDocument]=useState({});
    const [TemplateDetails,setTemplateDetails]=useState({});
    const [selectDocumentName, setSelectDocumentName] = useState();
    const [ShowDetailsModule,setShowDetailsModule]=useState(false);
    const [showsharedocument,setshowsharedocument]=useState(false);
    const [Inputfields,setinputfield]=useState([])
    const [ShowContactspopup,setShowContactspopup]=useState(false)
    const [contacts, setContacts] = useState([]);
    const [searchcontacts,setsearchcontacts]=useState([])
    // const [pageCount, setPageCount] = useState(0);
    const [searchText, setSearchText] = useState("");
    const keys=["EmailAddress","FirstName","LastName"]
    const [selectedContacts,setSelectedContacts]=useState([]);
    const [SelectedTemplates,setSelectedTemplates]=useState([]);
    const [sliderrand,setsliderrand]=useState();
    const [PageCount,setPageCount]=useState(1);
    const [ContactsPageCount,setContactsPageCount]=useState(1)
    const [perpage,setperpage]=useState([])
    const [contactsperpage,setcontactsperpage]=useState([])
    const [searchperpage,setsearchperpage]=useState([])
    const [pageNumber,setPageNumber]=useState(1)
    const [contactspageNumber,setcontactspageNumber]=useState(1)
    const [PageSize,setPageSize]=useState(100)
    const [searchpagecount,setsearchpagecount]=useState(1)
    const [oldcontacts,setoldcontacts]=useState([])
    const [TemplateDetailsfull,setTemplateDetailsfull] = useState({})
    const [selectedtemplateData,setselectedtemplateData] = useState({})
    const [bulksendEmailerror,setbulksendEmailerror]=useState("")
    const [templatepagesize,settemplatepagesize] = useState(4)
    const [templatepageNumber,settemplatepageNumber] = useState()
    const [templatePageCount,settemplatePageCount]=useState(1)
    const pageRef = useRef(null);
    const loadSpinner=()=>{
        return <div style={{display:"flex",alignContent:'center',alignItems: "center",justifyContent: "center",marginTop:'30%'}}>
        <Spinner animation="border"/></div>}
   useEffect(()=>{
    getCurrentUserData()
    
   },[])
   useEffect(()=>{
    setsearchcontacts([])
    if(searchText !== ""){
        search(contacts)
    }
   },[searchText])
    // const search=(contacts)=>{
    //     setsearchcontacts([])
    //    const list= contacts.length>0 && contacts.filter((item)=>keys.some((key)=>_.includes(_.lowerCase(item[key]),_.lowerCase(searchText))))
    //     setsearchcontacts(list);
    // }
    const search=(contacts)=>{
        if(contacts)  {
        setsearchcontacts([])
       const list= contacts.length>0 && contacts.filter((item)=>keys.some((key)=>_.includes(_.lowerCase(item[key]),_.lowerCase(searchText))))
       setsearchcontacts(list);
        setsearchperpage(list.length>10?list.slice(0,10):list);
        let pages = 0;
        for(let i=1;i<Math.ceil(list.length/10)+1;i++){
            pages = i
        }
        setsearchpagecount(pages)
    } else {
        setsearchcontacts([])
    }
       
    }

   const handlesearchstring = (e)=>{
    setSearchText(e.target.value)
   }
//    useEffect(()=>{
//     getmytemplatess()
// },[templatepageNumber])
    const getCurrentUserData = ()=>{
        usersService.getCurrentUserData()
        .then((result) => {
            if(result){
                 setuserdata(result.Data)
                 console.log(result.Data)
                 getmytemplatess()
            }
        })
    }
   
    let mytemppayload= {"PageNumber":1,
    "PageSize":100000,
    "SearchByText": "",
    "SortDir": "d",
    "SortKey": "date",
    "IsReact":1,
    }
    // const getmytemplatess=()=>{
    //     settemplates([]);
    //     TemplateServise.getmytemplates(mytemppayload).then((result)=>{
    //         if(result){
    //             //settemplates(result?.Entity)
              
    //             if(result?.Entity.data.length>0){
    //                 let temps=[];
    //                 //let sorteddata=_.orderBy(result?.Entity?.data, [(obj) => new Date(obj.CreatedDate)], ['desc'])
    //                 result?.Entity.data.map(item=>{
    //                 addDocumentService.getDocumentInfo(item.ADocumentId).then(data => {
    //                 if (data.Entity && data.Entity.Data) {
    //                     setTimeout(function(){
    //                    let document1 = `${config.serverUrl + 'adocuments/download/' + item.FileName}`
    //                     item.document=document1
    //                     item.IsDelete=false;
    //                     item.Position = 1
    //                     item.Pages =0
    //                     settemplates(current => [...current, item])
    //                 },3000)
    //                 }}
    //                 )
    //                 // let sortdata = _.sortBy(templates, 
    //                 //     [function(o) { return o.TemplateId; }]);
    //                 // settemplates(sortdata)
    //             })
    //            // setstateupdate(!stateupdate);
    //         }
               
    //         }
           
    //               })
    // }
    async function getmytemplatess(){
        settemplates([]);
       await TemplateServise.getmytemplates(mytemppayload).then((result)=>{
            if(result){
                if(result?.Entity.data.length>0){
                    settemplatePageCount(result?.Entity?.PageCount)
                    const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => new Date(obj.UpdatedOn===null?obj.CreatedDate:obj.UpdatedOn)], ['desc'])
                    settemplates(sortedArray)
                    let pages = 0;
                    for(let i=1;i<Math.ceil(sortedArray.length/10)+1;i++){
                        pages = i
                    }
                    setPageCount(pages)
                    if(pageNumber === null){
                        setperpage(sortedArray.slice(0,10));
                        setPageNumber(1)
                    }
                    else{
                        setperpage([])
                        setperpage(sortedArray.slice((pageNumber*10)-10,pageNumber*10));
                    }

                }
            }
        })
    }
    const onPdfView = (e,data) => {
        e.preventDefault();
        setviewPdf(true)
     let document1 = `${config.serverUrl + `adocuments/template/view/pdf/${data.TemplateId}/${userdata.UserId}`}`
        setSelectDocumentFile(document1)
        setSelectDocumentName(data.Title !== null?data.Title:data.OriginalFileName)
    }
    function popDocLoad(e) {
        setPopDocPages(e.numPages)
    }
    const ondocumentRename = (e, data, index) => {
        e.preventDefault();
        setselectDocument(data);
        setSelectDocumentName(data.Title);
        setdocumentRename(true);
    }
    function docNameChange(event) {
        setSelectDocumentName(event.target.value)
    }
const deletetemplate=(data)=>{
   let Ids=[data.TemplateId]
   let payload={"Ids":Ids}
            Swal.fire({
                title: 'Are you sure?',
                text: `Do you really want to delete -${data.Title !== null?data.Title:data.OriginalFileName}- template?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                //cancelButtonColor: '#d33',
                confirmButtonText: 'YES, DELETE IT',
                cancelButtonText:"CANCEL",
              }).then((result) => {
                if (result.isConfirmed) {
                    TemplateServise.Deletetemplatedocument(payload).then((result)=>{
                        if(result?.HttpStatusCode===200){
                            settemplates([]);
                            getmytemplatess();
                            // Swal.fire(
                            //     'Deleted!',
                            //     '',
                            //     'success'
                            //   )
                              Swal.fire({
                                icon: 'success',
                                title: 'Deleted!',
                                showConfirmButton: true,
                                confirmButtonText:"OKAY"
                                //timer: 2000
                              })
                              
                        }
                    })
                }
              })
}
const getDetails=(e,data)=>{
    e.preventDefault();
    setselectDocument(data);
   TemplateServise.GetTemplatedetails(data.TemplateId).then((result)=>{
     if(result){
        setTemplateDetails(result?.Entity);
        setShowDetailsModule(true)
     }
   })
}
const Bulksend=(data)=>{
    TemplateServise.GetTemplatedetails(data.TemplateId).then((result)=>{
        if(result){
           setTemplateDetails(result?.Entity);
        }
      })
    setselectDocument(data);
    setshowsharedocument(true);

}
const handilechangeinput =(index,e)=>{
    const values=[...Inputfields]
    if(e.target.name === "EmailAddress"){
        for(let i=0;i<values.length;i++){
            if(values[i].EmailAddress === e.target.value && index !== i){
                values[index].error =true
                setbulksendEmailerror("Email Already used")
                break
            }   
            else if(EmailValidator.validate(e.target.value) === false){
                values[index].error =true
                values[index][e.target.name]=e.target.value;
                setbulksendEmailerror("Email is not valid")
                break
            }
            else{
                setbulksendEmailerror("")
                values[index].error =false
            }
        }
        // setinputfield(values1)
        
    }
    // const values=[...Inputfields]
    values[index][e.target.name]=e.target.value;
    values[index].RecipientId=-(index+1)
    values[index].id=(`choice${(index+1)}`)
    //values[index][SendToUser]=true
   // values[index][IsDelete]=0
    setinputfield(values)
}
const addrecipients=()=>{
//Inputfields.map(item=>{
    //  if(item.EmailAddress==="" ||item.FirstName==="" ||item.LastName===""){
    //     Swal.fire("Unable to add a new recipient ","","info")
    //  }
    //  else{
        setinputfield([...Inputfields,{
            EmailAddress:"",
            FirstName: "",
            IsDelete: 0,
            LastName: "",
            RecipientId:null ,
            SendToUser: true,
            id: null }])
     //}
//})
}
const removeRecipients=(index,email)=>{
const values=[...Inputfields]
values.splice(index,1)
setinputfield(values)
const index1 = contacts.map(i => i.EmailAddress).indexOf(`${email}`);
contacts[index1].isChecked = false,
        setContacts([...contacts])
}
const addrecipientsfromcontacts=()=>{
    if(contacts.length === 0){
    getcontacts()}
  setShowContactspopup(true)
}
const handlePagination = (newPage) => {
    setcontactspageNumber(newPage);
    if(searchcontacts.length>0 && searchText !== ""){
        setsearchperpage([])
        setsearchperpage(searchcontacts.slice((newPage*10)-10,newPage*10));
    }
    else{
        setcontactsperpage([])
        setcontactsperpage(contacts.slice((newPage*10)-10,newPage*10));
    }
   
   }
   const handlePaginationtemp = (newPage) => {
    setPageNumber(newPage);
        setperpage([])
        setperpage(templates.slice((newPage*10)-10,newPage*10));
   }
const getcontacts=()=>{
    let input = {
        "ContactTypeId": null,
        "PageNumber": 1,
        "PageSize": 1000000,
        "SearchByText": ""
    }
    contactsService.contactList(input)
   
        .then((result) => {
            let data=result.data.map(e => { return { ...e, isChecked: false } })
            let resdata=data
            // setContacts(result.data.map(e => { return { ...e, isChecked: false } }));
            // setContacts(resdata)
            // setPageCount(result.PageCount);
            // setPageNumber(result.PageNumber);
           // checkSelectedContacts(result.data)
        //    const sortedArray = _.orderBy(data, [(obj) => new Date(obj.RegisteredOn)], ['desc'])
        setcontactsperpage(resdata.slice(0,10));
           setContacts(resdata)
           let pages = 0;
           for(let i=1;i<Math.ceil(resdata.length/10)+1;i++){
               pages = i
           }
           setContactsPageCount(pages)
        })
        .catch(error => {

        });
}

function checkedtemplate(event, data, index) {
    setselectDocument(data)
    if(event === true){
        setselectedtemplateData(data)
        setSelectedTemplates([{data}] )
    }
    if(event === false){
        setselectedtemplateData()
        setSelectedTemplates([])
    }
   
    // TemplateServise.GetTemplatedetailswithid(data.TemplateId).then((result)=>{
    //     if(result){
    //        // let data = result?.Data;
    //        let data=[]
    //         let datarespients = result?.Data.Recipients.length
    //         let Entityrespients = result?.Entity.Recipients.length
    //         console.log(datarespients,Entityrespients,"testlength")
    //         if(datarespients>=Entityrespients){
    //              data = result?.Data;
    //         }
    //         else{
    //              data = result?.Entity;
    //         }
    //         console.log(data,"111111111")
    //         let ownerdata = data.Recipients.find(item=>item.IsOwner === true || item.IsOwner === 1)
    //         if(ownerdata){
    //             // ownerdata.UserId=userdata.UserId
    //             // ownerdata.FirstName=userdata.FirstName
    //             // ownerdata.LastName=userdata.LastName
    //             // ownerdata.EmailAddress=userdata.EmailAddress
    //             // ownerdata.FullName=userdata.FullName
    //             ownerdata.UserId=userdata.UserId,
    //             ownerdata.EmailAddress=userdata.EmailAddress,
    //             ownerdata.FirstName=userdata.FirstName,
    //             ownerdata.LastName=userdata.LastName,
    //             ownerdata.FullName=userdata.FullName,
    //             ownerdata.AccountId=userdata.AccountId,
    //             ownerdata.ProfilePicture=userdata.ProfilePicture,
    //             ownerdata.IsGoogleDriveEnabled=userdata.IsGoogleDriveEnabled,
    //             ownerdata.IsOneDriveEnabled=userdata.IsOneDriveEnabled,
    //             ownerdata.IsDropBoxEnabled=userdata.IsDropBoxEnabled,
    //             ownerdata.ActiveUntill=userdata.ActiveUntill,
    //             ownerdata.DurationMode=userdata.DurationMode,
    //             ownerdata.IsAccountExpire=userdata.IsAccountExpire,
    //             ownerdata.UserUsedDocCount=userdata.UserUsedDocCount,
    //             ownerdata.PackageCode=userdata.PackageCode,
    //             ownerdata.LastLoginAttempt=userdata.LastLoginAttempt,
    //             ownerdata.PasswordExpiryDay=userdata.PasswordExpiryDay,
    //             ownerdata.IsSsoUser=userdata.IsSsoUser,
    //             ownerdata.CompanyUsedDocCount=userdata.CompanyUsedDocCount,
    //             ownerdata.IsSeqPositionEnabled=userdata.IsSeqPositionEnabled,
    //             ownerdata.DocumentLimit=userdata.DocumentLimit,
    //             ownerdata.company_id=userdata.company_id,
    //             ownerdata.user_id=userdata.user_id,
    //             ownerdata.email_address=userdata.email_address,
    //             ownerdata.uname=userdata.uname,
    //             ownerdata.company_name=userdata.company_name,
    //             ownerdata.logo=userdata.logo,
    //             ownerdata.IsSignatory=true,
    //             ownerdata.Color="att-ad-recipient-aqua"
    //         }
    //         let array = data.Recipients.filter(item=>item.IsOwner === false || item.IsOwner === 0)
    //         console.log(array,"array")
    //         let newarray = [ownerdata].concat(array)
    //         console.log(newarray,"newarray")
    //         data.Recipients = [...newarray]
    //         console.log(data,"final data")
    //        setTemplateDetailsfull(data);
    //     }
    //   })









    // const rand = 1 + Math.random() * (9999 - 1);
    // setsliderrand(rand)
    
//     let templateindex=templates.findIndex(function(i){
//         return i.TemplateId === data.TemplateId;
//    })
//     templates[templateindex].isChecked = event
//        // settemplates([templates[templateindex]])
//     if (event == false) {
//         const values=[...SelectedTemplates]
//         let index=values.findIndex(function(i){
//             return i.TemplateId === data.TemplateId;
//        })
//         values.splice(index)
//         setSelectedTemplates([])
       
//     } else if (event == true) {
//         setSelectedTemplates([{data}] )
//     }
}
    function clearselectedcontacts(){
        contacts.map((item,index)=>{
            contacts[index].isChecked = false
        })
    }
function checkedContact(event, data, index) {
    let contactindex=contacts.findIndex(function(i){
        return i.CompanyContactId === data.CompanyContactId;
   })
    contacts[contactindex].isChecked = event,
        setContacts([...contacts])
    if (event == false) {
        const values=[...Inputfields]
        let index=values.findIndex(function(i){
            return i.EmailAddress === data.EmailAddress;
       })
        values.splice(index)
        setinputfield(values)
       
    } else if (event == true) {
        setinputfield(current => [{
            EmailAddress:data.EmailAddress,
            FirstName: data.FirstName,
            IsDelete: 0,
            LastName: data.LastName,
            RecipientId:null ,
            SendToUser: true,
            id: data.CompanyContactId},...current] )
    }
    
    
    // setSelectedContacts(selectedContacts)

}
const contactssetasrecipients=()=>{
    setShowContactspopup(false);
}
const Sharedocument=()=>{
    let Recipientslist=[]
    let uploaddoc=(selectDocument.TotalRecipients+1)
    let d = new Date();
    let time = d.toLocaleTimeString(undefined, { hour12: false, timeZoneName: 'short' });
    let timeSplit = time.split(' ');
    let timeZone = timeSplit[1];
    TemplateDetails?.Recipients?.length>0&&TemplateDetails?.Recipients.map(item=>{
        item.RecipientPageNumbers = item.RecipientPageNumbers.toString().length >1 ? item.RecipientPageNumbers.split(',').map( n => parseInt(n, 10)):item.RecipientPageNumbers
        return(
            
            item.IsOwner !== 1&& Recipientslist.push({
            "RecipientId":item.RecipientId,
            "TemplateId":item.TemplateId,
            "IsOwner":item.IsOwner=== 0 ? false:true,
            "UserId":item.UserId,
            "IsSignatory":item.IsSignatory === 0? false:true,
            "FirstName":item.FirstName,
            "LastName":item.LastName,
            "EmailAddress":item.EmailAddress,
            "FullName":item.FullName === null?"  ":item.FullName,
            "SignatureUpload":item.SignatureUpload,
            "InitialUpload":item.InitialUpload,
            "SignatureName":item.SignatureName,
            "SignatureInitial":item.SignatureInitial,
            "SignatureFont":item.SignatureFont,
            "StatusId":item.StatusId,
            "StatusCode":"DRAFT",
            "Color":"att-ad-recipient-medium-green",
            "IsDelete":0,
            "Controls":item.Controls,
            "ProfilePicture":null,
            "OriginalFileName":null,
            "RecipientPageNumbers":item.RecipientPageNumbers.toString().length >1 ? item.RecipientPageNumbers:[item.RecipientPageNumbers],
            "IsInPerson":item.IsInPerson,
            "MobileNumber":item.MobileNumber,
            "DocumentOtp":item.DocumentOtp,
            "IsNotarizer":item.IsNotarizer,
            "IsFacialRecognition":0,
            "IsPhotoRecognition":0,
            "IsLocationTracking":0,
            "IsDSC":0,
            "IsDSCVerified":0,
            "SelectPage":null,
            "SignaturePosition":null,
            "TypeLevelAccess":null,
            "IsCCEnabled":0,
            "CurrentUser":false
        }
        
        ))})
    let payload ={ 
        "IsReact":1,
        "TemplateId":selectDocument.TemplateId,
        "ADocumentId":-1,
        "Title":selectDocument.Title,
        "Description":selectDocument.Description,
        "TotalRecipients":selectDocument.TotalRecipients,
        "OriginalFileName":selectDocument.OriginalFileName,
        "FileName":selectDocument.FileName,
        "Width":selectDocument.Width,
        "Height":selectDocument.Height,
        "StatusId":selectDocument.StatusId,
        "UploadedOn":selectDocument.UploadedOn,
        "CreatedDate":selectDocument.CreatedDate,
        "CreatedById":selectDocument.CreatedById,
        "CreatedBy":selectDocument.CreatedBy,
        "UpdatedOn":selectDocument.UpdatedOn,
        "TemplateCategoryId":selectDocument.TemplateCategoryId,
        "TemplateCategoryName":selectDocument.TemplateCategoryName,
         "Tags":selectDocument.Tags,
        "IsPublic":selectDocument.IsPublic,
        "IsSecured":selectDocument.IsSecured,
        "ControlsCount":selectDocument.ControlsCount,
        "FacialRecordCount":selectDocument.FacialRecordCount,
        "DigitalSignatureCount":selectDocument.DigitalSignatureCount,
        "updatedDate":selectDocument.CreatedDate,
        "UploadDocuments":Inputfields.length>0&& Inputfields.map((item,index)=>{
            // "UploadDocuments":[...Array(uploaddoc)].map((item,index)=>{
            return(
                {
                    // "ADocumentSplitId": (`-${(index+1)}`),
                    "ADocumentSplitId": -1,
                    "Height":selectDocument.Height,
                    "Width":selectDocument.Width,
                    "OriginalFileName":selectDocument.Title,
                    "FileName":selectDocument.FileName,
                    "Position":(index+1),
                    "DocumentSource":"template",
                    "IsDelete":false,
                    "TemplateId":selectDocument.TemplateId
                }
            )
        }),
        "Document":[
      
        ],
        "MultipleRecipients":
            Inputfields.length>0&& Inputfields.map((item,index)=>{
                return(
                    {
                        "RecipientId":(`choice${(Math.round(Math.random() * (99999- 10000) + 10000))}`),
                        "IsDelete":0,
                        "FirstName":item.FirstName,
                        "LastName":item.LastName,
                        "EmailAddress":item.EmailAddress,
                    }
                )
            })
        ,
        "Recipients":[
            {
                "UserId":userdata.UserId,
                "EmailAddress":userdata.EmailAddress,
                "FirstName":userdata.FirstName,
                "LastName":userdata.LastName,
                "FullName":userdata.FullName,
                "AccountId":userdata.AccountId,
                "ProfilePicture":userdata.ProfilePicture,
                "IsGoogleDriveEnabled":userdata.IsGoogleDriveEnabled,
                "IsOneDriveEnabled":userdata.IsOneDriveEnabled,
                "IsDropBoxEnabled":userdata.IsDropBoxEnabled,
                "ActiveUntill":userdata.ActiveUntill,
                "DurationMode":userdata.DurationMode,
                "TrialInDays":userdata.TrialInDays,
                "IsOwner":userdata.IsOwner===1?true:false,
                "IsAccountExpire":userdata.IsAccountExpire,
                "UserUsedDocCount":userdata.UserUsedDocCount,
                "PackageCode":userdata.PackageCode,
                "LastLoginAttempt":userdata.LastLoginAttempt,
                "PasswordExpiryDay":userdata.PasswordExpiryDay,
                "IsSsoUser":userdata.IsSsoUser,
                "CompanyUsedDocCount":userdata.CompanyUsedDocCount,
                "IsSeqPositionEnabled":userdata.IsSeqPositionEnabled,
                "DocumentLimit":userdata.DocumentLimit,
                "company_id":userdata.company_id,
                "user_id":userdata.user_id,
                "email_address":userdata.email_address,
                "uname":userdata.uname,
                "company_name":userdata.company_name,
                "logo":userdata.logo,
                "IsSignatory":true,
                "Color":"att-ad-recipient-aqua",
                "RecipientPageNumbers":[
                   
                ],
                "StatusCode":"DRAFT",
                "IsInPerson":0
             },...Recipientslist
             
        ],
        "DateTime":moment(new Date()).format('YYYY-MM-DD h:mm:ss'),
        // "TimeZone":`${new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1]}`
        "TimeZone":timeZone
        
        }
        TemplateServise.BulksendTemplates(payload).then((result)=>{
            if(result?.data?.HttpStatusCode === 200){
                setshowsharedocument(false)
               // Swal.fire("Successfully submitted the document.","","success");
                Swal.fire({
                    icon: 'success',
                    title: 'Successfully submitted the document.',
                    showConfirmButton: true,
                    confirmButtonText:"OKAY"
                    //timer: 2000
                  })
                
            }
        }).catch({

        })
        setinputfield([])    
}
const gotoadddocument=()=>{
    console.log(SelectedTemplates,"SelectedTemplates 22222")
    let tempid =  selectedtemplateData.TemplateId
    let templatealldetails={}
   
    TemplateServise.GetTemplatedetailswithid(tempid).then((result)=>{
        if(result){
           let data=[]
            let datarespients = result?.Data.Recipients.length
            let Entityrespients = result?.Entity.Recipients.length
            console.log(datarespients,Entityrespients,"testlength")
            if(datarespients>=Entityrespients){
                 data = result?.Data;
            }
            else{
                 data = result?.Entity;
            }
            let ownerdata = data.Recipients.find(item=>item.IsOwner === true || item.IsOwner === 1)
            if(ownerdata){
                ownerdata.UserId=userdata.UserId,
                ownerdata.EmailAddress=userdata.EmailAddress,
                ownerdata.FirstName=userdata.FirstName,
                ownerdata.LastName=userdata.LastName,
                ownerdata.FullName=userdata.FullName,
                ownerdata.AccountId=userdata.AccountId,
                ownerdata.ProfilePicture=userdata.ProfilePicture,
                ownerdata.IsGoogleDriveEnabled=userdata.IsGoogleDriveEnabled,
                ownerdata.IsOneDriveEnabled=userdata.IsOneDriveEnabled,
                ownerdata.IsDropBoxEnabled=userdata.IsDropBoxEnabled,
                ownerdata.ActiveUntill=userdata.ActiveUntill,
                ownerdata.DurationMode=userdata.DurationMode,
                ownerdata.IsAccountExpire=userdata.IsAccountExpire,
                ownerdata.UserUsedDocCount=userdata.UserUsedDocCount,
                ownerdata.PackageCode=userdata.PackageCode,
                ownerdata.LastLoginAttempt=userdata.LastLoginAttempt,
                ownerdata.PasswordExpiryDay=userdata.PasswordExpiryDay,
                ownerdata.IsSsoUser=userdata.IsSsoUser,
                ownerdata.CompanyUsedDocCount=userdata.CompanyUsedDocCount,
                ownerdata.IsSeqPositionEnabled=userdata.IsSeqPositionEnabled,
                ownerdata.DocumentLimit=userdata.DocumentLimit,
                ownerdata.company_id=userdata.company_id,
                ownerdata.user_id=userdata.user_id,
                ownerdata.email_address=userdata.email_address,
                ownerdata.uname=userdata.uname,
                ownerdata.company_name=userdata.company_name,
                ownerdata.logo=userdata.logo,
                ownerdata.IsSignatory=true,
                ownerdata.Color="att-ad-recipient-aqua"
            }
            let array = data.Recipients.filter(item=>item.IsOwner === false || item.IsOwner === 0)
            let newarray = [ownerdata].concat(array)
            data.Recipients = [...newarray]
            templatealldetails=data
           setTemplateDetailsfull(data);
           commonService.settemplateDocument(templatealldetails)
           usehistory.push('/account/adddocumentsstep')
        }
      })
      console.log(templatealldetails ,"&&&&&",TemplateDetailsfull)
    //   if(templatealldetails !== undefined){
    // commonService.settemplateDocument(templatealldetails)
    //     usehistory.push('/account/adddocumentsstep')}   
}
const handletemplatePagination=(newpage)=>{
    settemplatepageNumber(newpage)
}
const onPageLoadSuccess = (event, index, data, pageimage) => {
    if (!templates[index].canvasImageUrl && pageimage) {
        templates[index].canvasImageUrl = pageimage
    }

}
function getOnLoadSuccess(e, index) {
    data.process=false;
     const importPDFCanvas= document.querySelector('.import-pdf-page canvas');
     if (pageRef && pageRef.current && pageRef.current.pageElement && pageRef.current.pageElement.current
       && pageRef.current.pageElement.current.firstChild ) {
         onPageLoadSuccess(e, index, data, pageRef.current.pageElement.current.firstChild.toDataURL())
       } else {
         onPageLoadSuccess(e, index, data, importPDFCanvas.toDataURL())
       }
   
   }
// function onpreviousclick(type){
//     debugger
//         if(type == "Next" ){
//            settemplatepageNumber(templatepageNumber+1)
//         }
//         if(type == "Previous" ){
//             if(templatepageNumber>1){
//                 settemplatepageNumber(templatepageNumber-1)
//             }
            
//         }
// }
const useOutsideAlerter1 = (ref1) => {
    useEffect(() => {
        function handleClickOutside(event) {
            if (ref1.current && !ref1.current.contains(event.target) && event.target.id != 'filter_dropdown'&& event.target.id != 'wrapper_other') {
                dropdownclose()
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref1]);
}
useOutsideAlerter1(wrapperRef1);
 //Filter Dropdown
 function dropdown2() {
    var srcElement = document.getElementById('dropdownfilter');
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = "none";
        } else {
            srcElement.style.display = "block";
        }
        return false;
    }   
}
function dropdownclose() {
    var srcElement = document.getElementById('dropdownfilter');
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = "none";
        } 
        return false;
    }
}

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content manage_content managehome">
                <div className="main-content main-from-content signatures pl-0 pr-0">
                   <div className="row">
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <h3 style={{fontSize:35,fontWeight:300}}>Templates</h3>
						</div>
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
						 <div className="recipient-btn"> 
                         <button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt=""  style={{marginRight:"2px",width: "15px",height: "15px"}} /></button>
                         <button disabled={SelectedTemplates.length>0?false:true} type="button" style={{color:"#FFFFFF",backgroundColor:"#2F7FED",marginLeft:"12px"}} className="btn" onClick={()=>gotoadddocument()}>
                                    {/* <img src="src/images/plus_icon.svg" alt /> */}
                                    Add Document<span className='ml-2' style={{padding:5}}>{SelectedTemplates.length}</span>
                                </button>
                            </div>
							</div>
						</div>

                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="table__row__conts table-responsive" style={{marginTop:10}}>
                                <table className="table " >
                                    <thead className='first_head'>
                                        <tr>
                                            <th className='px-4'><div className="top" >
                                            <div className="Filters dropdown-toggle"><span><img style={{cursor:"context-menu"}} src="src/images/filters.svg" alt="" /></span> <a id="filter_dropdown" onClick={dropdown2}>Filters </a><span><img style={{cursor:"context-menu"}} src="src/images/down_arrow1.svg" alt="" /></span>
                                            <div id="dropdownfilter" className="filter_dropdown dropdown-menu " ref={wrapperRef1} style={{ display: 'none' }}>
                                                <ul >
                                                <li data-tip data-for="All" ><a><span className='-mt-3'><img src="src/images/templates_icon.svg"/></span>All</a></li>
                                                <ReactTooltip id="All" place="top" effect="solid">All Templates.</ReactTooltip>
                                                       <li data-tip data-for="MY" ><a><span className='-mt-3'><img src="src/images/templates_icon.svg"/></span>My Templates</a></li>
                                                        <ReactTooltip id="MY" place="top" effect="solid">My Templates.</ReactTooltip>
                                                        <li data-tip data-for="FREE" ><a><span className='-mt-3'><img src="src/images/templates_icon.svg"/></span>Free</a></li>
                                                        <ReactTooltip id="FREE" place="top" effect="solid">Free Templates.</ReactTooltip>
                                                        <li data-tip data-for="PREMIUM" ><a><span className='-mt-3'><img src="src/images/templates_icon.svg"/></span>Premium</a></li>
                                                        <ReactTooltip id="PREMIUM" place="top" effect="solid"> Premium Templates.</ReactTooltip>
                                                        </ul> 
                                                         </div>
                                                         </div></div></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                {/* 1-8 on <span>8</span> */}
                                                </th>
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th  style={{cursor:"auto",position:"relative",textAlign:"start",fontSize:14,paddingLeft:47}}>Document</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:32}}>Created By</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:33}}>Created On</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:33}}>Updated On</th>
                                            <th style={{position:"relative",textAlign:"start",fontSize:14,paddingLeft:33}}>Type</th> 
                                            <th  style={{position:"relative",fontSize:14}}>Select</th> 
                                            {/* <th rowSpan={2}>Last Modified <span><img src='src/images/down_arrow.svg' alt="" /></span></th> */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {perpage && perpage.length > 0?perpage.map((item,index)=>{
                                            
                                            return(
                                             <tr>
                                             <td  style={{position:"relative",textAlign:"start",width:"28%"}}>
                                               {/* <img onClick={()=>{dropdown4(item.ADocumentId)}} src='src/images/down_arrow.svg' alt=""  style={{padding:5,border:"1px solid #c7a9a9 ",backgroundColor:"#ededed"}}/> */}
                                             
                                                <button style={{padding:0}} id={`${"dropdownMenuButton1"+item?.TemplateId}`} data-bs-toggle="dropdown" aria-expanded="false" className="btn doc_btn"><img   src="src/images/plus_icon.svg" alt="" /></button>
                                                <label className='px-2' style={{cursor:"auto"}}  htmlFor="check1">
                                                {item?.Title!== null&&item?.Title.length>20?<Tooltip placement="top" title={item?.Title} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                         width: 180,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        marginBottom:"-4px"
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item?.Title}
                                                    </div>
                                                </Tooltip>:item?.Title}
                                                
                                                </label>
                                                
                                                {/* <div  id={item.ADocumentId} className=" dropdown-menu" style={{ display: 'none' }}> */}
                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                            <li><a style={{cursor:"pointer"}} className="dropdown-item" onClick={(e) => { onPdfView(e, item) }}>View</a></li>
                                                            <li><a style={{cursor:"pointer"}}  className="dropdown-item" onClick={(e) => { getDetails(e, item) }}>Details</a></li>
                                                            <li><a style={{cursor:"pointer"}}  className="dropdown-item" onClick={(e) => { e.preventDefault(); ondocumentRename(e, item, index) }}>Rename</a></li>
                                                            {item?.TotalRecipients === 2 && item?.ControlsCount > 0 &&
                                                                item?.DigitalSignatureCount === 0 && item?.FacialRecordCount === 0 &&
                                                                <li><a style={{cursor:"pointer"}}  className="dropdown-item" onClick={(e) => { e.preventDefault(); Bulksend(item) }} >Bulk Send</a></li>}
                                                            <li><a style={{cursor:"pointer"}}  className="dropdown-item" onClick={(e) => { e.preventDefault(); deletetemplate(item) }}>Delete</a></li>
                                                        </ul>
                                               {/* </div> */}
                                                </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item?.CreatedBy!== null&&item?.CreatedBy.length>10?
                                             <Tooltip placement="top" title={item?.CreatedBy} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        position:"relative",
                                                        textAlign:"start",
                                                        width: 80,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item?.CreatedBy}
                                                    </div>
                                                </Tooltip>:item?.CreatedBy !== null?item?.CreatedBy:"-"}
                                             </td>
                                             
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>
                                                <><div>{moment.utc(item?.CreatedDate).local().format('MM/DD/YYYY')}</div>
                                                <div>{moment.utc(item?.CreatedDate).local().format('h:mm A')}</div></>
                                                
                                            </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start",fontSize:14}}>
                                                
                                                <><div>{item?.UpdatedOn !== null ?(moment.utc(item?.UpdatedOn).local().format('MM/DD/YYYY')):(moment.utc(item?.CreatedDate).local().format('MM/DD/YYYY'))}</div>
                                                <div>{item?.UpdatedOn !== null ?(moment.utc(item?.UpdatedOn).local().format('h:mm A')):(moment.utc(item?.CreatedDate).local().format('h:mm A'))}</div></>
                                                </td>
                                             <td className='px-5' style={{position:"relative",textAlign:"start"}}>
                                             {item?.IsPublic!== null&&item?.IsPublic.length>15?
                                             <Tooltip placement="top" title={item?.IsPublic} arrow={true}
                                                >
                                                <div
                                                    style={{
                                                        width: 100,
                                                        overflow: "hidden",
                                                        whiteSpace: "nowrap",
                                                        textOverflow: "ellipsis",
                                                        textAlign:"start",
                                                        position:"relative"
                                                       // color: "#4E2C90",
                                                    }}
                                                    >
                                                    {item?.IsPublic}
                                                    </div>
                                                </Tooltip>:item?.IsPublic !== null?item?.IsPublic:"-"}
                                                </td>
                                             <td    >
                                                <div className='table_btn' >
                                                {/* <button  style={{cursor:"context-menu",margin:0, border:"0.5px solid black",background:"none"}}>{item.StatusCode ==="DRAFT"?<img src= 'src/images/draft.svg' title="Draft" style={{width:30,height:30,padding:3}}/>:item.StatusCode ==="SENT"?<img src= 'src/images/send.svg' title="Sent" style={{width:30,height:25,padding:4}}/>:item.StatusCode ==="REJECTED"?<img src= 'src/images/Rejected.svg' title="Rejected" style={{width:30,height:30,padding:3}}/>:item.StatusCode ==="CANCELLED"?<img src= 'src/images/Cancelled.svg' title="Cancelled" style={{width:30,height:30,padding:4}}/>:item.StatusCode ==="APPROVED"?<img src= 'src/images/Approved.svg' title="Approved" style={{width:30,height:30,padding:4}}/>:""}</button>
                                               */}
                                                <input className='selectCheckbox ' style={{width:20,height:20,margin:6}}
                                                                            type="checkbox"
                                                                             onChange={(event) => { checkedtemplate(event.target.checked, item, index) }}
                                                                             checked={selectedtemplateData?.TemplateId === item?.TemplateId?true:false}
                                                                            ></input> 
                                               </div></td> 
                                         </tr>)
                                        }):
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td
                                             className={"flex justify-content-center mt-4"}> No documents are found.</td>
                                        </tr>}
                                        <tr style={{height:perpage.length<4?180:0}}>
                                        </tr>
                                    </tbody>
                                    
                                </table>
                                
                            </div>
                        </div>
                    </div>
                    {PageCount>1&&
                    <div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
                                        {/* Searchstring.length>0?searchpagecount: */}
											<Pagination
												current={pageNumber}
												total={PageCount}
												onPageChange={(newPage) =>
                                                    handlePaginationtemp(newPage)}
                                                maxWidth={"80px"}
											/>
										</nav>
									</div>
								</div>
							</div>}
                 
                </div>
            </div>
            
            <Modal show={showsharedocument}  scrollable={true} 
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p>ADD RECIPIENT</p>
                            
                        </Modal.Title>
                        <button type="button" className="close" style={{backgroundColor:"#ffff"}}  onClick={()=>{setshowsharedocument(false),setbulksendEmailerror("")}}
                         >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <div className='flex justify-content-end -mt-2 mb-2'>
                        {/* <p className='-mt-1 mb-4' style={{fontSize:13,color:"#696969"}}>ADD RECIPIENT ( Selected recipients will receive a copy of the final approved document.)</p> */}
                        {/* <div onClick={addrecipients}><img className='-mt-2 mb-4'  src="src/images/plus_icon.svg"  /></div> */}
                        {/* <div className="recipient-btn">
                                <button className="btn">
                                    <img src="src/images/userss.svg" alt />
                                    Add From Contacts
                                </button>
                            </div> */}
                            <div className="recipient-btn flex " style={{border:"1.2px solid #2F7FED", borderRadius:"6px", color:"#2F7FED"}} 
                        onClick={addrecipientsfromcontacts} >
                                <button className="btn">
                                    <img className='mr-2' src="src/images/plus_icon.svg" alt />
                                    Add From Contacts
                                </button>
                            </div>
                        <div className="recipient-btn flex " style={{border:"1.2px solid #2F7FED", borderRadius:"6px", color:"#2F7FED"}} 
                        onClick={addrecipients} >
                                <button className="btn">
                                    <img className='mr-2' src="src/images/plus_icon.svg" alt />
                                    Add New Recipient
                                </button>
                            </div>
                            
                        </div>
                        
                       
                        {Inputfields.map((inputfield,index) =>{
                            console.log(inputfield,"values1")
                             return(
                            <div className={"flex px-5"}>
                            <div>
                            <input id="id" type="email" SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" autoComplete="off" name="EmailAddress" value={inputfield.EmailAddress} onChange={e=>handilechangeinput(index,e)} className={"docinput mb-1"} placeholder=' Email*'  />
                              <div>{inputfield?.error === true && bulksendEmailerror !=="" ? <p className='error-message' style={{fontSize:15,marginBottom:7}} >{bulksendEmailerror}</p> : null}</div>
                             </div>
                            <div>
                                <input id="id" type="text" SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="FirstName"  value={inputfield.FirstName} onChange={e=>handilechangeinput(index,e)}  className='docinput' placeholder=' First Name*'  />
                            </div>
                            <div>
                            <input id="id" type="text"  SendToUser="SendToUser" IsDelete="IsDelete"  RecipientId="RecipientId" name="LastName" value={inputfield.LastName} onChange={e=>handilechangeinput(index,e)}  className='docinput' placeholder=' Last Name*'  />
                            </div>
                            {<div style={{cursor:"pointer"}} onClick={()=>{removeRecipients(index,inputfield.EmailAddress)}}>
                            <img className='ml-2 mt-1'
								src='src/images/cancel_close_cross_delete_remove_icon.svg'
								width={"25px"} alt=''/>
                                </div> }
                             </div>)})}
                        
                        
                        
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button disabled={(Inputfields.length>0 ?false:true) ||(bulksendEmailerror !== "" ?true:false) } variant="primary" onClick={() => {
                            let allinputs = true
                            Inputfields.map(item=>{
                                if(item.EmailAddress==="" ||item.FirstName==="" ||item.LastName===""){
                                    allinputs=false;
                                  //  Swal.fire("Please fill all the details, then Send the document.' ","","info")
                                    Swal.fire({
                                        icon: 'info',
                                        title: 'Please fill all the details, then Send the document.',
                                        showConfirmButton: true,
                                        confirmButtonText:"OKAY"
                                        //timer: 2000
                                      })
                                 }
                            })
                            if(allinputs === true){
                                Sharedocument();
                             }
                        }}>SEND</Button> 
                         <Button variant="secondary" onClick={() => {
                           setshowsharedocument(false)
                           setbulksendEmailerror("")
                           setinputfield([])
                           setSelectedContacts([])
                           clearselectedcontacts()
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
            <Modal show={ShowDetailsModule} onHide={() => {
                    setShowDetailsModule(false); 
                }}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p>Details</p>
                        </Modal.Title>
                        <button type="button" className="close" style={{backgroundColor:"#ffff"}} onClick={() => {
                            setShowDetailsModule(false)
                        }} >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                    <Box
                    component="form"
                    sx={{
                        "& .MuiTextField-root": { m: 1, width: "30ch" }
                    }}
                    noValidate
                    autoComplete="off"
                    >
                    <div>
                        <TextField
                        id="standard-read-only-input"
                        label="Title"
                        defaultValue={TemplateDetails?.Title}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        <TextField
                        id="standard-read-only-input"
                        label="Created By"
                        defaultValue={selectDocument?.CreatedBy}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        <TextField
                        id="standard-read-only-input"
                        label="Created On"
                        defaultValue={(moment.utc(selectDocument?.CreatedDate !== null ?selectDocument?.CreatedDate:"-").local().format('MM/DD/YYYY,h:mm:ss A'))}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        <TextField
                        id="standard-read-only-input"
                        label="Recipients"
                        defaultValue={TemplateDetails?.Recipients?.length}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        <TextField
                        id="standard-read-only-input"
                        label="Owner"
                        defaultValue={TemplateDetails?.OwnerName}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        <TextField
                        id="standard-read-only-input"
                        label="Description"
                        defaultValue={selectDocument?.Description === null?"-":selectDocument.Description}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                    </div>
                    </Box>
                    {/* <h4 className='mt-2'>RECIPIENTS DETAILS</h4> */}
                    <div>
                    {TemplateDetails?.Recipients?.map((item,index)=>{
                         return(
                            <Box
                    component="form"
                    sx={{
                        "& .MuiTextField-root": { m: 1, width: "30ch" }
                    }}
                    noValidate
                    autoComplete="off"
                    >
                    <>
                    
                        <TextField
                        id="standard-read-only-input"
                        label="Recipient"
                        defaultValue={item.IsOwner===1?"Owner":`Recipient${index}`}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        <TextField
                        id="standard-read-only-input"
                        label="Permission"
                        defaultValue={item.RecipientPageNumbers}
                        InputProps={{
                            readOnly: true
                        }}
                        variant="standard"
                        />
                        </>
                        </Box>
                 )
                    })}
                    </div>
                        {/* <label htmlFor="docName">Name <span style={{ color: 'red' }}>*</span> </label>
                        <input type="text" id="docName" className='docinput' name="docName" value={selectDocumentName} onChange={docNameChange} /> */}
                    </Modal.Body>
                    <Modal.Footer>
                      
                        <Button variant="secondary" onClick={() => {
                            setShowDetailsModule(false)
                                
                        }}>CLOSE</Button>
                    </Modal.Footer>
                </Modal>
            <Modal show={documentRename} onHide={() => {
                    setdocumentRename(false); setSelectDocumentIndex('');
                    setSelectDocumentName('')
                }}
                    size=""
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header  >
                        <Modal.Title id="example-modal-sizes-title-lg">
                            <p>RENAME</p>
                        </Modal.Title>
                        <button type="button" className="close"  style={{backgroundColor:"#ffff"}} onClick={() => {
                            setdocumentRename(false), setSelectDocumentIndex(''),
                                setSelectDocumentName('')
                        }} >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </Modal.Header>
                    <Modal.Body className='error-modal-content'>
                        <label htmlFor="docName">Name <span style={{ color: 'red' }}>*</span> </label>
                        <input type="text" id="docName" className='docinput' name="docName" value={selectDocumentName} onChange={docNameChange} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" disabled = {selectDocument.Title === selectDocumentName?true:false} onClick={() => {
                            if(selectDocumentName.length>0){
                            selectDocument.Title=selectDocumentName
                            let payload=selectDocument
                            TemplateServise.Updatetemplatedocument(payload).then((result)=>{
                                 if(result){
                                    if(result.HttpStatusCode === 200){
                                       // Swal.fire("Template Renamed","","success")
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Template Renamed',
                                            showConfirmButton: true,
                                            confirmButtonText:"OKAY"
                                            //timer: 2000
                                          })
                                        getmytemplatess();
                                        setdocumentRename(false);
                                        setSelectDocumentName('');
                                        setselectDocument({});
                                    }
                                 }
                            }).catch({

                            })
                        }
                        else{
                           // Swal.fire(" Tittle Not Changed ","","info")
                            Swal.fire({
                                icon: 'info',
                                title: 'Tittle Not Changed',
                                showConfirmButton: true,
                                confirmButtonText:"OKAY"
                                //timer: 2000
                              })
                        }
                          
                        }}>RENAME</Button>
                        <Button variant="secondary" onClick={() => {
                            setdocumentRename(false), setSelectDocumentIndex(''),
                                setSelectDocumentName('')
                        }}>CANCEL</Button>
                    </Modal.Footer>
                </Modal>
            <Modal show={viewPdf} onHide={() => { setviewPdf(false) }}
                    size="lg"
                    dialogClassName="my-docpop"
                    aria-labelledby="example-modal-sizes-title-lg"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                            {selectDocumentName}
                        </Modal.Title>
                        <FullscreenIcon onClick={()=>{handle.enter()}} style={{cursor:"pointer",marginLeft:'20px'}}/>
                    </Modal.Header>
                    <Modal.Body style={{height:'430px',overflow:'hidden', paddingBottom:'8rem'}} >
                    <FullScreen handle={handle} >
                        <div className='pop-doc'  style={{maxHeight:handle.active?"100vh":null}}>
                            <Document className="row"
                                loading={loadSpinner}
                                file={selectDocumentFile}
                                onLoadSuccess={popDocLoad}>
                                {Array.apply(null, Array(popDocPages))
                                    .map((x, i) => i + 1)
                                    .map(page =>
                                        <>
                                            <Page scale={1.5} className="subdocument_intial mx-auto d-block" pageNumber={page} />
                                            <span className='pagenumber-flex'>page {page} of {popDocPages}</span>
                                        </>
                                    )}
                            </Document>
                        </div>
                        </FullScreen>
                    </Modal.Body>
                    {/* <Modal.Footer> */}
                        {/* <Button variant="primary" onClick={() => { setDocmodalShow(false) }}>Submit</Button> */}
                        {/* <Button variant="secondary" onClick={() => { setviewPdf(false) }}>Close</Button>
                    </Modal.Footer> */}
                </Modal>
                <Modal show={ShowContactspopup} onHide={() => { setShowContactspopup(false) }}
                size="lg"
                dialogClassName="doc_select_contacts_modal"
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Select Contact
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                <div className="main other_main contacts-table-popup" id="main">
			<div className=" wrapper_other main-content main-from-content contacts" >
				<div className="row">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div className="recipient-btn">
							<div className={"search-box"}>
								<div className="input-group" style={{ flex: 'none' }}>
									<input type="text" className="form-control"
										placeholder="Search"
										value={searchText}
										 onChange={(e) => handlesearchstring(e)}
										// onKeyPress={(event) => {
										// 	if (event.key === "Enter") {
										// 		event.preventDefault();
										// 		searchContact();
										// 	}
										// }}
									/>
									{
										searchText === "" ?
											<button type="button" className=" bg-transparent search-btn"
												style={{ zIndex: 100 }}
												// onClick={searchContact}
                                                >
												<img src='src/images/search_icon_blue.svg' alt='' />
											</button> :
											<button type="button" className=" bg-transparent search-btn"
												style={{ zIndex: 100 }}
                                                onClick={()=>setSearchText("")}
                                                >
												<img src='src/images/exit.png' alt='' />
											</button>
									}
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row align-items-center">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						{
							searchText === ""?contactsperpage.map((item, index) => {
								return (
									<div className='document_block' key={item.CompanyContactId} >
										<ul className='content_area' onChange={(event) => {
											// let isChecked = (item?.isChecked) ? false : true
											// checkedContact(isChecked, item, index)
										}} >

											<li className={'checkbox_li'} >
												<h6></h6>
												<input className='selectCheckbox'
													type="checkbox"
													onChange={(event) => { checkedContact(event.target.checked, item, index) }}
													checked={item?.isChecked}></input>
											</li>
											<li className={"w-20"} title={item.ContactName}><h6>Name</h6>
												<h5>
													<EllipsisText text={item.ContactName} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Email</h6><h5>
												<EllipsisText text={item.EmailAddress} length={20} />
											</h5></li>
											<li className={"w-20"} title={item.Designation ? item.Designation : "NA"}><h6>Designation</h6>
												<h5>
													<EllipsisText text={item.Designation ? item.Designation : 'NA'} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Contact Type</h6>
												<h5>
													<EllipsisText text={item.ContactType ? item.ContactType : 'NA'} length={20} />
												</h5>
											</li>
										</ul>
									</div>
								)
							}):
                            searchperpage && searchperpage.map((item, index) => {
								return (
									<div className='document_block' key={item.CompanyContactId} >
										<ul className='content_area' onChange={(event) => {
											// let isChecked = (item?.isChecked) ? false : true
											// checkedContact(isChecked, item, index)
										}} >

											<li className={'checkbox_li'} >
												<h6></h6>
												<input className='selectCheckbox'
													type="checkbox"
													onChange={(event) => { checkedContact(event.target.checked, item, index) }}
													checked={item?.isChecked}></input>
											</li>
											<li className={"w-20"} title={item.ContactName}><h6>Name</h6>
												<h5>
													<EllipsisText text={item.ContactName} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Email</h6><h5>
												<EllipsisText text={item.EmailAddress} length={20} />
											</h5></li>
											<li className={"w-20"} title={item.Designation ? item.Designation : "NA"}><h6>Designation</h6>
												<h5>
													<EllipsisText text={item.Designation ? item.Designation : 'NA'} length={20} />
												</h5>
											</li>
											<li className={"w-20"}><h6>Contact Type</h6>
												<h5>
													<EllipsisText text={item.ContactType ? item.ContactType : 'NA'} length={20} />
												</h5>
											</li>
										</ul>
									</div>
                                    
								)
							})

						}
						{
							searchText === ""?contacts.length === 0 &&
							<div className={"text-center mt-4"}> No records are found.</div>
                            :searchcontacts.length === 0 &&
							<div className={"text-center mt-4"}> No records are found.</div>
						}

					</div>
				</div>
				<div className="row">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div className="document-nagin">
							<nav aria-label="Page navigation">
								<Pagination
									// current={pageNumber}
									// total={pageCount}
									// onPageChange={(newPage) => handlePagination(newPage)}

                                    current={pageNumber}
									total={searchText.length>0?searchpagecount:PageCount}
									onPageChange={(newPage) => setTimeout(() => {
                                        handlePagination(newPage)
                                    }, 500)}
								/>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => {
                        contactssetasrecipients()
                        }}>ADD</Button>
                    <Button variant="secondary" onClick={() => { setSelectedContacts([]), setShowContactspopup(false) }}>CLOSE</Button>
                </Modal.Footer>
            </Modal> 
           
        </div>
    )
}

export { Template }; 