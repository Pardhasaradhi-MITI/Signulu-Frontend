import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function UserPreferences({ history, location }) {
    //Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content userpreferences">
                    {/* <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <h3>User Preferences</h3>
                            </div>
                        </div>
                    </div> */}
                      <div className="row">
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <h3>User Preferences</h3>
						</div>
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
						</div>
                        </div>

                    <div className='row align-items-center'>
                        <div className='references'>
                            <div className='tab'>
                                <ul className='navbar-tabs'>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'firstTab')}><button>General Preferences</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'secondTab')}><button>Email Preferences</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}><button>Date Format</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button>Signature Settings</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'fifthTab')}><button>Signature Font Size</button></li>
                                </ul>
                            </div>
                            <div id="firstTab" className='tab-content'>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    A signer tleenes to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the system
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    I have a document to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Another signer declines to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    The sender corrects a document
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the systern
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="secondTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    A signer tleenes to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the system
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    I have a document to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Another signer declines to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    The sender corrects a document
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the systern
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="thirdTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    A signer tleenes to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the system
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    I have a document to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Another signer declines to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    The sender corrects a document
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the systern
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    A signer tleenes to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the system
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    I have a document to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Another signer declines to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    The sender corrects a document
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the systern
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="fifthTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    A signer tleenes to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the system
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    I have a document to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Another signer declines to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    The sender corrects a document
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the systern
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { UserPreferences }; 