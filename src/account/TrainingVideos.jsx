import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function TrainingVideos({ history, location }) {
    const options = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 3,
            },
            480 : {
                items: 1,
            },
            768 : {
                items: 2,
            },
            1000: {
                items: 3,
            },
            1280: {
                items: 3,
            },
        },
    };
    const options4 = {
        responsive: {
            0: {
                items: 1,
            },
            599: {
                items: 1,
            },
            480 : {
                items: 1,
            },
            768 : {
                items: 2,
            },
            1000: {
                items: 3,
            },
            1280: {
                items: 1,
            },
        },
    };
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content training_videos">
                    <div className="row align-items-center">
                       <div className='heading'>
                       <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <h3>Training Videos</h3>
                        </div>
                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                            <div className="recipient-btn">
                                <a href='#' className='btn view_btn'>View All</a>
                            </div>
                        </div>
                       </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <OwlCarousel className='blog-carousel owl-carousel owl-theme' loop responsive={options.responsive} margin={10} dots={false} nav={true} items={3} responsiveClass={true} navText={["<img src='src/images/left-arrw.svg'>", "<img src='src/images/right-arrw.svg'>"]}>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <span class="play-btn"><img src="src/images/play-icon.svg" alt="" /></span>
                                    </div>
                                </div>
                            </OwlCarousel>
                        </div>
                    </div>
                    <div className="row align-items-center">
                       <div className='heading'>
                       <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <h3>Blogs</h3>
                        </div>
                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                            <div className="recipient-btn">
                                <a href='#' className='btn view_btn'>View All</a>
                            </div>
                        </div>
                       </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <OwlCarousel className='recipients-carousel owl-carousel owl-theme' loop responsive={options4.responsive} margin={10} dots={false} nav={true} items={3} responsiveClass={true} navText={["<img src='src/images/left-arrw.svg'>", "<img src='src/images/right-arrw.svg'>"]}>
                                <div className='item'>
                                    <div className='item'>
                                        <div className='blog_block'>
                                            <img src='src/images/carousel-img-1.jpg' />
                                        </div>
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='blog_block'>
                                        <img src='src/images/carousel-img-2.jpg' />
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='blog_block'>
                                        <img src='src/images/carousel-img-3.jpg' />
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='blog_block'>
                                        <img src='src/images/carousel-img-3.jpg' />
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='blog_block'>
                                        <img src='src/images/carousel-img-2.jpg' />
                                    </div>
                                </div>
                                <div className='item'>
                                    <div className='video_block'>
                                        <img src='src/images/carousel-img-1.jpg' />
                                    </div>
                                </div>
                            </OwlCarousel>
                        </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <h3>Documents</h3>
                        </div>
                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                            <div className='action_area'>
                                <ul>
                                    <li><img src='src/images/xls.svg' alt='' /></li>
                                    <li><img src='src/images/pdf.svg' alt='' /></li>
                                    <li><img src='src/images/csv.svg' alt='' /></li>
                                </ul>
                                <select className='hlfinput'>
                                    <option>December</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='table-responsive'>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><div><h6>Document Title</h6><h5>Verifies Ownership</h5></div></td>
                                        <td><div><h6>Last Updated</h6><h5>05/08/2020 - 5.30pm</h5></div></td>
                                        <td><div><h6>Invoice Amount</h6><h5>$180.00</h5></div></td>
                                        <td><div><h6>Status</h6><h5>Paid</h5></div></td>
                                        <td><div><button className='btn view_btn'>View</button><button className='btn download_btn'>Download</button></div></td>
                                    </tr>
                                    <tr>
                                        <td><div><h6>Document Title</h6><h5>Verifies Ownership</h5></div></td>
                                        <td><div><h6>Last Updated</h6><h5>05/08/2020 - 5.30pm</h5></div></td>
                                        <td><div><h6>Invoice Amount</h6><h5>$180.00</h5></div></td>
                                        <td><div><h6>Status</h6><h5>Paid</h5></div></td>
                                        <td><div><button className='btn view_btn'>View</button><button className='btn download_btn'>Download</button></div></td>
                                    </tr>
                                    <tr>
                                        <td><div><h6>Document Title</h6><h5>Verifies Ownership</h5></div></td>
                                        <td><div><h6>Last Updated</h6><h5>05/08/2020 - 5.30pm</h5></div></td>
                                        <td><div><h6>Invoice Amount</h6><h5>$180.00</h5></div></td>
                                        <td><div><h6>Status</h6><h5>Paid</h5></div></td>
                                        <td><div><button className='btn view_btn'>View</button><button className='btn download_btn'>Download</button></div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="document-nagin">
                                <nav aria-label="Page navigation">
                                    <ul className="pagination">
                                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                                        <li className="page-item">
                                            <a className="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">•••</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { TrainingVideos }; 