import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {Formik, Field, Form, ErrorMessage} from 'formik';
import * as Yup from 'yup';
// import SignatureCanvas from 'react-signature-canvas'
import SignaturePad from 'react-signature-canvas'

import {accountService, alertService} from '@/_services';
import {Sidebar} from './Common/Sidebar';
import {Topbar} from './Common/Topbar';
import {usersService} from "../_services/users.service";

//images
import upload from "../../src/images/upload.svg"
import {authService} from "../_services/auth.service";
import {SignatureService} from "../_services/signature.service";
import {createSlice} from "@reduxjs/toolkit";
import close_icon from "../images/cancel_close_cross_delete_remove_icon.svg"

function Signatures({history, location}) {

	const [getUserInfo, setUserInfo] = useState({})
	const [settingList,setSettingList]=useState({})
	const [trimmedDataURL1, settrimmedDataURL1] = useState('')
	const [trimmedDataURL2, settrimmedDataURL2] = useState('')
	const [strimmedDataURL1, setstrimmedDataURL1] = useState('')
	const [strimmedDataURL2, setstrimmedDataURL2] = useState('')
	const [defaultSignature, setDefaultSignature] = useState('')
	const [responseSing,setResponseSing]=useState([])
	const [checkbox,setCheckbox]=useState(0)
	const [endSignPad1,setendSignPad1]=useState(false)
	const [endSignPad2,setendSignPad2]=useState(false)
	const [signType,setSignType]=useState('')
	const [signValue1,setSignValue1]=useState('')
	const [signValue2,setSignValue2]=useState('')
	const [signValue3,setSignValue3]=useState(null)
	const [signValue4,setSignValue4]=useState(null)
	const [profilepicerror1,setprofilepicerror1]=useState('')
	const [profilepicerror2,setprofilepicerror2]=useState('')
	const [cancle,setCancle]=useState(false)
	const [mydataOrChangeData,setmydataOrChangeData]=useState(0)



	const [TypeCode,setTypeCode]=useState('')
	const [activeTage,setActiveTag]=useState("firstTab")
	const [count,setcount]=useState(0)
	const [fileerroe,setfileerror]=useState("")
	let sigPad1 = {}
	let sigPad2 = {}
	let dataArray = []
	// let token = localStorage.getItem('LoginToken');

	useEffect(() => {
		getUser()
		getdSettingList()
	}, []);

	//Vertical Tabs
	const getUser = () => {
		usersService.getCurrentUserData()
			.then((result) => {
				//      
				if (result) {
					setUserInfo(result.Data)
					SignatureService.getSing(result.Data.UserId)
						.then((result)=>{
							console.log("result GET SIGN",result)
							setResponseSing(result)
							setSignType(result.Entity[0].Type)
							if (result.Entity[0].Type==="Font"){

								// setSignValue1(result.Entity[0].TextValue)
								setActiveTag("thirdTab")
								switch (result.Entity[0].TextValue){
									case "att-ad-brush-script":
										setCheckbox(1)
										break;
									case  "att-ad-forte":
										setCheckbox(2)
										break;
									case  "att-ad-freestyle-script":
										setCheckbox(3)
										break;
									case  "att-ad-harlow-solid-italic":
										setCheckbox(4)
										break;
									case  "att-ad-blackadder-itc":
										setCheckbox(5)
										break;
									case  "att-ad-vivaldi":
										setCheckbox(6)
										break;

								}


							}else if(result.Entity[0].Type==="Sign"){

								setActiveTag("firstTab")
								setTypeCode(result.Entity[0].Code)
								settrimmedDataURL1(result.Entity[0].TextValue)
								settrimmedDataURL2(result.Entity[1].TextValue)
							}
							else{
								     
								setActiveTag("secondTab")
								setmydataOrChangeData(0)

								if (result.Entity.length === 1){
									setTypeCode(result.Entity[0].Code)
									if (result.Entity[0].Code === "SIGN_UPLOAD_FULL"){
										setSignValue3(result.Entity[0] !== undefined && result.Entity[0].TextValue)
										setSignValue1(result.Entity[0] !== undefined && result.Entity[0].TextValue)
									}else {
										setSignValue4(result.Entity[0] !== undefined && result.Entity[0].TextValue)
										setSignValue2(result.Entity[0] !== undefined && result.Entity[0].TextValue)
									}

								}else {
									setTypeCode(result.Entity[0].Code)
									setSignValue1(result.Entity[0] !== undefined && result.Entity[0].TextValue)
									setSignValue2(result.Entity[1] !== undefined && result.Entity[1].TextValue)
								}




								// if (result.Entity[1] === undefined){
								// 	     
								//
								// }else {
								//
								// }

							}
						})
						.catch(error => {

						});

				}
			})
			.catch(error => {

			});
	}

	function getdSettingList(){
		usersService.getSettingUser()
			.then((result) => {
				setSettingList(result.Entity)
			})
			.catch(error => {

			});
	}

	function readImgUrlAndPreview1(input) {
		     
		let reader
		if (input.target && input.target.files[0]) {
			let file=input.target.files[0]
			if (file.size > 102400){
				// setprofilepicerror1("The fill should be upto 50kb")
				setfileerror("File size more, please upload a file with a size less than or equal to 100 KB.")
				setcount(0)
				// setError(errors)
				return ""
			}
			// setprofilepicerror1('')
			setfileerror('')
			reader = new FileReader();
			reader.onload = function (e) {
				// let div = document.querySelector('#imagePreview1')
				// div.classList.remove('hide')
				setmydataOrChangeData(1)
				setCancle(false)
				setSignValue1(e.target.result)
				setSignValue3(null)
				setTypeCode("SIGN_UPLOAD_FULL")
				// div.src = (e.target.result)

			}
		};
		reader.readAsDataURL(input.target.files[0]);
	}

	function readImgUrlAndPreview2(input) {
		     
		let reader
		if (input.target && input.target.files[0]) {
			let file=input.target.files[0]

			if (file.size > 102400){
				// setprofilepicerror2("The fill should be upto 50kb")
				setfileerror("File size more, please upload a file with a size less than or equal to 100 KB.")
				setcount(0)

				// setError(errors)
				return ""
			}
			// setprofilepicerror2('')
			setfileerror('')
			reader = new FileReader();
			reader.onload = function (e) {
				     
				// let div1 = document.querySelector('#imagePreview2')
				// div1.classList.remove('hide')
				setmydataOrChangeData(1)
				setCancle(false)
				setSignValue2(e.target.result)
				setSignValue4(null)
				// div1.src = (e.target.result)
				setTypeCode("SIGN_UPLOAD_INITIAL")
			}
		}
		;
		reader.readAsDataURL(input.target.files[0]);
	}

	const save = () => {
		// let errors=erroe

		if (cancle){
			let usersettingID = settingList.SIGNATURE
			for (let x=0;x<usersettingID.length;x++){
				let temp1=usersettingID[x]
				temp1["resetToDefault"]=1
			}

			let tempsameObject = {"SIGNATURE": usersettingID}

			const requestApi = () => {

				// console.log(data.entries)
				if (usersettingID.length > 0) {

					SignatureService.update(tempsameObject)
						.then(() => {
							alertService.success("Saved Successfully")
							sigPad1.clear()
							sigPad2.clear()
							
								getUser()
								getdSettingList()
							

						})
						.catch((err) => {
							console.log(err)
						})
				} else {
					requestApi()
				}
			}
			requestApi()

		}else {
			if (activeTage === "firstTab"){
				if(settingList.SIGNATURE === undefined){
					let usersettingID=[]
					usersettingID.push({
						BagCode: "SIGNATURE",
						BagId: 1,
						BagName: "Signature Settings",
						BitValue: null,
						Code: "SIGN_DRAW_FULL",
						DataType: 3,
						DateTimeValue: null,
						IntValue: null,
						Name: "Signature Draw Full",
						PackageId: 1,
						SettingId: 4,
						TextValue:trimmedDataURL1 ,
						Type: "Sign",
						UserSettingId: -1,
					})
					usersettingID.push({
						BagCode: "SIGNATURE",
						BagId: 1,
						BagName: "Signature Settings",
						BitValue: null,
						Code: "SIGN_DRAW_INITIAL",
						DataType: 3,
						DateTimeValue: null,
						IntValue: null,
						Name: "Signature Draw Initial",
						PackageId: 1,
						SettingId: 5,
						TextValue:trimmedDataURL2,
						Type: "Sign",
						UserSettingId: -1,
					})

					let tempsameObject = {"SIGNATURE": usersettingID}

					const requestApi = () => {

						// console.log(data.entries)
						if (usersettingID.length > 0) {

							SignatureService.update(tempsameObject)
								.then(() => {
									alertService.success("Saved Successfully")
									sigPad1.clear()
									sigPad2.clear()
									
										getUser()
										getdSettingList()
									

								})
								.catch((err) => {
									console.log(err)
								})
						} else {
							requestApi()
						}
					}
					requestApi()
				}
				else if (strimmedDataURL1 !== "" || strimmedDataURL2 !== ""){
					     
					setfileerror("")
					let tempsamelist = settingList.SIGNATURE
					if (tempsamelist[0].Type=== "Sign"){
						tempsamelist[0].TextValue= strimmedDataURL1;
						tempsamelist[1].TextValue= strimmedDataURL2;
						let temp1=tempsamelist[0]
						temp1["base64"]=strimmedDataURL1
						let temp2=tempsamelist[1]
						temp2["base64"]=strimmedDataURL2
						let tempsameObject = {"SIGNATURE": tempsamelist}
						console.log(tempsameObject)
						const requestApi = () => {

							// console.log(data.entries)
							if (tempsamelist.length > 0) {

								SignatureService.update(tempsameObject)
									.then(() => {
										alertService.success("Saved Successfully")
										sigPad1.clear()
										sigPad2.clear()
										
											getUser()
											getdSettingList()
										

									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()

					}
					else {
						let usersettingID = settingList.SIGNATURE
						let temp1=usersettingID[0]
						let temp2=usersettingID[1]
						if (settingList.SIGNATURE.length === 1){
							temp1["resetToDefault"]=1
						}else {
							temp1["resetToDefault"]=1
							temp2["resetToDefault"]=1
						}
						console.log(usersettingID)
						usersettingID.push({
							BagCode: "SIGNATURE",
							BagId: 1,
							BagName: "Signature Settings",
							BitValue: null,
							Code: "SIGN_DRAW_FULL",
							DataType: 3,
							DateTimeValue: null,
							IntValue: null,
							Name: "Signature Draw Full",
							PackageId: 1,
							SettingId: 4,
							TextValue:strimmedDataURL1 ,
							Type: "Sign",
							UserSettingId: -1,
						})
						usersettingID.push({
							BagCode: "SIGNATURE",
							BagId: 1,
							BagName: "Signature Settings",
							BitValue: null,
							Code: "SIGN_DRAW_INITIAL",
							DataType: 3,
							DateTimeValue: null,
							IntValue: null,
							Name: "Signature Draw Initial",
							PackageId: 1,
							SettingId: 5,
							TextValue:strimmedDataURL2,
							Type: "Sign",
							UserSettingId: -1,
						})

						let tempsameObject = {"SIGNATURE": usersettingID}

						const requestApi = () => {

							// console.log(data.entries)
							if (usersettingID.length > 0) {

								SignatureService.update(tempsameObject)
									.then(() => {
										alertService.success("Saved Successfully")
										sigPad1.clear()
										sigPad2.clear()
										
											getUser()
											getdSettingList()
										

									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()




					}
				}else {
					setfileerror("Please sign the signature/Initial.")
					setcount(0)
				}


			}
			else if(activeTage==="secondTab"){
				     
				if(settingList.SIGNATURE === undefined){
					     
					let usersettingID=[]
					if (signValue1 !== ""){
						usersettingID.push({
							BagCode: "SIGNATURE",
							BagId: 1,
							BagName: "Signature Settings",
							BitValue: null,
							Code: "SIGN_UPLOAD_FULL",
							DataType: 3,
							DateTimeValue: null,
							IntValue: null,
							Name: "Signature Draw Full",
							PackageId: 1,
							SettingId: 2,
							TextValue:signValue1 ,
							Type: "Upload",
							UserSettingId: -1,
						})
					} else if(signValue2  !== ""){
						usersettingID.push({
							BagCode: "SIGNATURE",
							BagId: 1,
							BagName: "Signature Settings",
							BitValue: null,
							Code: "SIGN_UPLOAD_FULL",
							DataType: 3,
							DateTimeValue: null,
							IntValue: null,
							Name: "Signature Draw Initial",
							PackageId: 1,
							SettingId: 3,
							TextValue:signValue2,
							Type: "Upload",
							UserSettingId: -1,
						})
					}


					let tempsameObject = {"SIGNATURE": usersettingID}

					const requestApi = () => {

						// console.log(data.entries)
						if (usersettingID.length > 0) {

							SignatureService.update(tempsameObject)
								.then(() => {
									alertService.success("Saved Successfully")

									
										getUser()
										getdSettingList()
									

								})
								.catch((err) => {
									console.log(err)
								})
						} else {
							requestApi()
						}
					}
					requestApi()
				}
				else if (signValue1 !== "" || signValue2 !== ""){
					     
					setfileerror("")
					let tempsamelist = settingList.SIGNATURE

					if (tempsamelist[0].Type=== "Upload"){

						if (tempsamelist.length === 1 && signValue1 !== "" && signValue2 !== ""){
							if (tempsamelist[0].Code === "SIGN_UPLOAD_INITIAL"){

								let temp1textvalue=tempsamelist[0].TextValue
								let temp1textid=tempsamelist[0].UserSettingId

								let removedata=tempsamelist[0]
								removedata["resetToDefault"]=1
								if (signValue1 !== ""){
									// console.log(usersettingID)
									tempsamelist.push({
										BagCode: "SIGNATURE",
										BagId: 1,
										BagName: "Signature Settings",
										BitValue: null,
										Code: "SIGN_UPLOAD_FULL",
										DataType: 3,
										DateTimeValue: null,
										IntValue: null,
										Name: "Signature Draw Full",
										PackageId: 1,
										SettingId: 2,
										TextValue:signValue1 ,
										Type: "Upload",
										UserSettingId: -1,
									})
								}
								// if(signValue2 !==""){
								tempsamelist.push({
										BagCode: "SIGNATURE",
										BagId: 1,
										BagName: "Signature Settings",
										BitValue: null,
										Code: "SIGN_UPLOAD_INITIAL",
										DataType: 3,
										DateTimeValue: null,
										IntValue: null,
										Name: "Signature Draw Initial",
										PackageId: 1,
										SettingId: 3,
										TextValue:temp1textvalue,
										Type: "Upload",
										UserSettingId: temp1textid,
									})
								// }



								// tempsamelist[0].TextValue= signValue2;
								// let temp1=tempsamelist[0]
								// temp1["base64"]=signValue2

							}else {
								let temp1textvalue=tempsamelist[0].TextValue
								let temp1textid=tempsamelist[0].UserSettingId
								let removedata=tempsamelist[0]
								removedata["resetToDefault"]=1

								if (signValue1 !== ""){
									// console.log(usersettingID)
									temp1textvalue.push({
										BagCode: "SIGNATURE",
										BagId: 1,
										BagName: "Signature Settings",
										BitValue: null,
										Code: "SIGN_UPLOAD_FULL",
										DataType: 3,
										DateTimeValue: null,
										IntValue: null,
										Name: "Signature Draw Full",
										PackageId: 1,
										SettingId: 2,
										TextValue:temp1textvalue ,
										Type: "Upload",
										UserSettingId: temp1textid,
									})
								}
								// if(signValue2 !==""){
								temp1textvalue.push({
									BagCode: "SIGNATURE",
									BagId: 1,
									BagName: "Signature Settings",
									BitValue: null,
									Code: "SIGN_UPLOAD_INITIAL",
									DataType: 3,
									DateTimeValue: null,
									IntValue: null,
									Name: "Signature Draw Initial",
									PackageId: 1,
									SettingId: 3,
									TextValue: signValue2,
									Type: "Upload",
									UserSettingId: -1,
								})
								// }
							}
						}else {
							if (tempsamelist.length === 1){
								if (tempsamelist[0].Code === "SIGN_UPLOAD_INITIAL"){
									tempsamelist[0].TextValue= signValue2;
									let temp1=tempsamelist[0]
									temp1["base64"]=signValue2
								}else {
									tempsamelist[0].TextValue= signValue1;
									let temp1=tempsamelist[0]
									temp1["base64"]=signValue1
								}
							}
							else {
								tempsamelist[0].TextValue= signValue1;
								let temp1=tempsamelist[0]
								temp1["base64"]=signValue1
								tempsamelist[1].TextValue= signValue2;
								let temp2=tempsamelist[0]
								temp2["base64"]=signValue2
							}
						}





						// if (signValue1 !== "" && tempsamelist.length ===1){
						// 	tempsamelist[0].TextValue= signValue1;
						// 	let temp1=tempsamelist[0]
						// 	temp1["base64"]=signValue1
						// }
						// if(signValue2 !== "" && tempsamelist.length >1){
						// 	tempsamelist[1].TextValue= signValue2;
						// 	let temp2=tempsamelist[1]
						// 	temp2["base64"]=signValue2
						// }else {
						// 	if (signValue2 !== ""){
						// 		tempsamelist[0].TextValue= signValue2;
						// 		let temp1=tempsamelist[0]
						// 		temp1["base64"]=signValue2
						// 	}else{
						// 		tempsamelist[0].TextValue= signValue1;
						// 		let temp1=tempsamelist[0]
						// 		temp1["base64"]=signValue1
						// 	}
						// }
						// if (signValue1 === "" && tempsamelist.length>1){
						// 	let temp1=tempsamelist[0]
						// 	temp1["resetToDefault"]=1
						// }else {
						//
						// }
						// if (signValue2 === "" && tempsamelist.length>1){
						// 	let temp1=tempsamelist[1]
						// 	temp1["resetToDefault"]=1
						// }



						let tempsameObject = {"SIGNATURE": tempsamelist}
						console.log(tempsameObject)
						const requestApi = () => {

							// console.log(data.entries)
							if (tempsamelist.length > 0) {

								SignatureService.update(tempsameObject)
									.then(() => {
										alertService.success("Saved Successfully")

										
											getUser()
											getdSettingList()
										

									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()

					}
					else {
						let usersettingID = settingList.SIGNATURE
						let temp1=usersettingID[0]
						let temp2=usersettingID[1]
						if (settingList.SIGNATURE.length === 1){
							temp1["resetToDefault"]=1
						}else {
							temp1["resetToDefault"]=1
							temp2["resetToDefault"]=1
						}

						if (signValue1 !== ""){
							console.log(usersettingID)
							usersettingID.push({
								BagCode: "SIGNATURE",
								BagId: 1,
								BagName: "Signature Settings",
								BitValue: null,
								Code: "SIGN_UPLOAD_FULL",
								DataType: 3,
								DateTimeValue: null,
								IntValue: null,
								Name: "Signature Draw Full",
								PackageId: 1,
								SettingId: 2,
								TextValue:signValue1 ,
								Type: "Upload",
								UserSettingId: -1,
							})
						}if(signValue2 !==""){
							usersettingID.push({
								BagCode: "SIGNATURE",
								BagId: 1,
								BagName: "Signature Settings",
								BitValue: null,
								Code: "SIGN_UPLOAD_INITIAL",
								DataType: 3,
								DateTimeValue: null,
								IntValue: null,
								Name: "Signature Draw Initial",
								PackageId: 1,
								SettingId: 3,
								TextValue:signValue2,
								Type: "Upload",
								UserSettingId: -1,
							})
						}
						let tempsameObject = {"SIGNATURE": usersettingID}

						const requestApi = () => {

							// console.log(data.entries)
							if (usersettingID.length > 0) {

								SignatureService.update(tempsameObject)
									.then(() => {
										alertService.success("Saved Successfully")

										
											getUser()
											getdSettingList()
										

									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()
					}
				}else {
					setfileerror("Please upload the signature/Initial.")
					setcount(0)
				}



			}
			else if(activeTage==="thirdTab") {
				     
				if (defaultSignature !== ""){
					setfileerror("")


					     

					if (settingList && settingList.SIGNATURE !== undefined && settingList.SIGNATURE.length === 1) {

						let tempsamelist = settingList.SIGNATURE
						tempsamelist[0].TextValue= defaultSignature;
						let tempsameObject = {"SIGNATURE": tempsamelist}

						const requestApi = () => {

							// console.log(data.entries)
							if (tempsamelist.length > 0) {

								SignatureService.update(tempsameObject)
									.then(() => {
										alertService.success("Saved Successfully")
										
											getUser()
											getdSettingList()
										

									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()

					} else if(settingList.SIGNATURE === undefined){
						let tempsamplesetting=[]
						tempsamplesetting.push({
							BagCode: "SIGNATURE",
							BagId: 1,
							BagName: "Signature Settings",
							BitValue: null,
							Code: "SIGN_FONT",
							DataType: 3,
							DateTimeValue: null,
							IntValue: null,
							Name: "Signature Font",
							SettingId: 1,
							TextValue: defaultSignature,
							Type: "Font",
							UserId: getUserInfo && getUserInfo.user_id,
							UserSettingId: -1,
						})

						let finalObject = {"SIGNATURE": tempsamplesetting}

						const requestApi = () => {
							     
							console.log("finalObject",finalObject)
							// console.log(data.entries)
							if (tempsamplesetting.length > 0) {
								     
								SignatureService.update(finalObject)
									.then(() => {
										alertService.success("Saved Successfully")
										getUser()
										getdSettingList()
										setTypeCode('')
										setSignValue1('')
										setSignValue2('')
									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()

					}

					else {
						     
						let usersettingID = settingList.SIGNATURE && settingList.SIGNATURE[0].UserSettingId && settingList.SIGNATURE[0].UserSettingId
						if (settingList.SIGNATURE.length > 1) {
							usersettingID = -1
						}
						let tempsamplesetting = settingList.SIGNATURE
						tempsamplesetting.push({
							BagCode: "SIGNATURE",
							BagId: 1,
							BagName: "Signature Settings",
							BitValue: null,
							Code: "SIGN_FONT",
							DataType: 3,
							DateTimeValue: null,
							IntValue: null,
							Name: "Signature Font",
							SettingId: 1,
							TextValue: defaultSignature,
							Type: "Font",
							UserId: getUserInfo && getUserInfo.user_id,
							UserSettingId: usersettingID,
						})

						console.log(tempsamplesetting)
						     
						let temp=tempsamplesetting[0]
						temp["resetToDefault"]=1
						let temp1=tempsamplesetting[1]
						temp1["resetToDefault"]=1

						let finalObject = {"SIGNATURE": tempsamplesetting}

						const requestApi = () => {
							     
							console.log("finalObject",finalObject)
							// console.log(data.entries)
							if (tempsamplesetting.length > 0) {
								     
								SignatureService.update(finalObject)
									.then(() => {
										alertService.success("Saved Successfully")
										getUser()
										getdSettingList()
										setTypeCode('')
										setSignValue1('')
										setSignValue2('')
									})
									.catch((err) => {
										console.log(err)
									})
							} else {
								requestApi()
							}
						}
						requestApi()
					}


				}
				else {
					setfileerror("Please select any font style.")
					setcount(0)
				}







			}
		}






		// if (endSignPad1 &&  sigPad1.getTrimmedCanvas().toDataURL('image/png') !== "") {
		// 	dataArray.push({
		// 		BagCode: "SIGNATURE",
		// 		BagId: 1,
		// 		BagName: "Signature Settings",
		// 		BitValue: null,
		// 		Code: "SIGN_DRAW_FULL",
		// 		DataType: 3,
		// 		DateTimeValue: null,
		// 		IntValue: null,
		// 		Name: "Signature Draw Full",
		// 		// UserId: getUserInfo,
		// 		PackageId: 1,
		// 		SettingId: 4,
		// 		TextValue: sigPad1.getTrimmedCanvas().toDataURL('image/png'),
		// 		Type: "Sign",
		// 		UserSettingId: -1
		// 	})
		// }
		// if ( endSignPad2 && sigPad2.getTrimmedCanvas().toDataURL('image/png') !== "") {
		// 	dataArray.push({
		// 		BagCode: "SIGNATURE",
		// 		BagId: 1,
		// 		BagName: "Signature Settings",
		// 		BitValue: null,
		// 		Code: "SIGN_DRAW_INITIAL",
		// 		DataType: 3,
		// 		DateTimeValue: null,
		// 		IntValue: null,
		// 		Name: "Signature Draw Initial",
		// 		// UserId: getUserInfo,
		// 		PackageId: 2,
		// 		SettingId: 5,
		// 		TextValue: sigPad2.getTrimmedCanvas().toDataURL('image/png'),
		// 		Type: "Sign",
		// 		UserSettingId: -1,
		// 	})
		// }
		// if (defaultSignature !== "") {
		// 	dataArray.push({
		// 		BagCode: "SIGNATURE",
		// 		BagId: 1,
		// 		BagName: "Signature Settings",
		// 		BitValue: null,
		// 		Code: "SIGN_FONT",
		// 		DataType: 3,
		// 		DateTimeValue: null,
		// 		IntValue: null,
		// 		Name: "Signature Font",
		// 		// UserId: getUserInfo,
		// 		PackageId: 1,
		// 		SettingId: 1,
		// 		TextValue: defaultSignature,
		// 		Type: "Font",
		// 		UserSettingId: -1,
		// 	})
		// }

		// let data = {
		// 	"SIGNATURE": dataArray
		// }
		//      

		// console.log(sigPad1.getTrimmedCanvas().toDataURL('image/png'))


	}

	const clear=()=>{
		settrimmedDataURL1("")
		settrimmedDataURL2("")
		setstrimmedDataURL1("")
		setstrimmedDataURL2("")
		setSignValue1("")
		setSignValue2("")
		setSignValue3(null)
		setSignValue4(null)
		setDefaultSignature("")
		setCheckbox(0)
		sigPad1.clear()
		sigPad2.clear()
		setCancle(true)
	}

	// const AddSign = () => {
	// 	let input = {
	// 		"IsDomain": isDomain,
	// 		"IsActive": isActive,
	// 		"Email": emailAddress,
	// 		"Address1": addressLine1,
	// 		"FirstName": firstName,
	// 		"Address2": addressLine2,
	// 		"LastName": lastName,
	// 		"Phone": cellPhone,
	// 		"RoleId": roleType,
	// 		"Designation": designation,
	// 		"City": city,
	// 		"State": state,
	// 		"ZipCode": zipCode,
	// 		"UserAddProfilePic": {},
	// 		"ProfilePic": [{}],
	// 		"DurationMode": "Annual",
	// 		"PackageId": "1",
	// 		"TrialInDays": "14",
	// 		"CompanyId": "352"
	// 	}
	// 	if (validateForm()) {
	// 		usersService.addUser(profilePic, input)
	// 			.then((result) => {
	// 				if (result.HttpStatusCode === 200) {
	// 					setOpenSuccessModal(true);
	// 					getUsersList();
	// 					resetForm();
	// 				}
	// 			})
	// 			.catch(error => {
	//
	// 			});
	// 	}
	//
	// }
	const validateForm = () => {
		return true
	}

	const chageCheckFun=(index,value)=>{
		setCheckbox(index)
		setDefaultSignature(value)
		setCancle(false)
	}

	const sigpadEnd=(e)=>{
		if (e===1){
			setstrimmedDataURL1(sigPad1.getTrimmedCanvas().toDataURL('image/png'))
			setendSignPad1(true)
			setCancle(false)
			//setTypeCode("")
		}
		if (e===2){
			     
			setstrimmedDataURL2(sigPad2.getTrimmedCanvas().toDataURL('image/png'))
			setendSignPad2(true)
			setCancle(false)
			//setTypeCode("")
		}


	}

	const delete_sigpadEnd=(e)=>{
		     
		if (e===1){
			     
			if (activeTage!=="secondTab"){
				setendSignPad1(false)
				settrimmedDataURL1('')
				setstrimmedDataURL1('')
				sigPad1.clear()
			}else {
				setSignValue1('')
			}


			// setSignValue3('')


		}
		if (e===2){

			if (activeTage!=="secondTab"){
				setendSignPad2(false)
				settrimmedDataURL2('')
				setstrimmedDataURL2('')
				sigPad2.clear()
			}else {
				setSignValue2('')
			}


		}
		if(e===3){
			setSignValue3(null)
		}
		if(e===4){
			setSignValue4(null)
		}


	}

	const shortName=()=>{
		//      
		let tempFirstName=getUserInfo.FirstName
		let tempLastName=getUserInfo.LastName
		let shortname1=tempFirstName && tempFirstName.slice(0,1)
		let shortname2=tempLastName && tempLastName.slice(0,1)
		let stortName=shortname1+shortname2
		return stortName
	}



	return (
		<div className="dashboard_home wrapper_other" id="wrapper_other">
			<Sidebar/>

			<div className="main other_main" id="main">
				<Topbar/>

				<div className="main-content main-from-content signatures">
					<div className="row">
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
							{/* <h3>Signatures</h3> */}
							<h3 style={{fontSize:35,fontWeight:300}}>Signatures</h3>
						</div>
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
							<div className="recipient-btn">
							<button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}} /></button>
								
							</div>
						</div>
					</div>

					<div className='row'>
						<div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
							<div className="conditions_sect">
								<div className="row align-items-center">
									<div className="col-xl-3 col-lg-3 col-md-4 col-sm-12">
										<div className="item first-child">
											<h5>Create Signature</h5>
											<p>Select the signatory style / upload the signature.</p>
										</div>
										<div className='form-group'>
											<input type="text" className={'w-100'} placeholder={`${getUserInfo.FirstName?getUserInfo.FirstName:""}` + " " +`${getUserInfo.LastName?getUserInfo.LastName:""}`} readOnly/>

										</div>
									</div>
									<div className="col-xl-9 col-lg-9 col-md-8 col-sm-12">
										<div className="item second-child">
											<h5>Recommended Settings:</h5>
											<div className='content'>
												<p>Image dimensions: 670 X 280px.<br/> File Size: 100KB(Max).</p>
												<p>File Types: .jpg, .png, .jpeg.<br/> Note: Use transparent image for
													better result.</p>
											</div>
										</div>
										<div className='form-group'>
											<input type="text" className={'w-auto'} placeholder={getUserInfo.FirstName?shortName():""} readOnly/>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p className={"error-msg mt-2"}>{fileerroe}</p>

					<div className='row'>
						<div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
							<div className="tab">
								<button className={activeTage==="firstTab" ?"tablinks active":"tablinks"} onClick={(e) => setActiveTag('firstTab')}>SIGN
								</button>
								<button className={activeTage==="secondTab" ?"tablinks active":"tablinks"} onClick={(e) => setActiveTag('secondTab')}>UPLOAD</button>
								<button className={activeTage==="thirdTab" ?"tablinks active":"tablinks"} onClick={(e) => setActiveTag('thirdTab')}>CHOOSE</button>
							</div>
							<div  className="tabcontent" style={activeTage==='firstTab' ? {display:"block"}:{display:"none"}}>
								<div className={'row'}>
									<div className={'parent-canva col-7'}>
										<div style={ trimmedDataURL1 ?{height:380}:{height:'auto'}} className={'parent-tag-value-name'}>
											<div className={'tag-value-Name'}>Name</div>
											{/*<SignatureCanvas penColor='green'*/}
											{/*                 canvasProps={{className: 'sigCanvas1'}} ref={(ref) => { sigPad1 = ref }} />*/}

											<SignaturePad
												canvasProps={{className: 'sigCanvas1'}}
												ref={(ref) => {
													sigPad1 = ref
												}}
												onEnd={()=>sigpadEnd(1)}

											/>
											{trimmedDataURL1 !== ""
												? <div>
													<img alt='signature'
														 src={trimmedDataURL1}/>
													<img alt={'close'} src={close_icon} onClick={()=>delete_sigpadEnd(1)} className={"sign-close-icon-delete"}/>
												</div>
												: null}
										</div>
									</div>
									<div className={'parent-canva col-5'}>
										<div style={trimmedDataURL2?{height:380}:{height:'auto'}}className={'parent-tag-value-name'}>
											<div className={'tag-value-Name'}>Initial</div>
											{/*<SignatureCanvas penColor='green'*/}
											{/*                 canvasProps={{className: 'sigCanvas2'}} ref={(ref) => { sigPad2 = ref }} />*/}
                                           
											<SignaturePad
												canvasProps={{className: 'sigCanvas2'}}
												ref={(ref) => {
													sigPad2 = ref
												}}
												onEnd={()=>sigpadEnd(2)}
											/>
											
											{trimmedDataURL2 !== ""
												? <div>
													<img alt='signature'
														 src={trimmedDataURL2}/>
													<img alt={'close'} src={close_icon} onClick={()=>delete_sigpadEnd(2)} className={"sign-close-icon-delete"}/>
												</div>
												: null}
										</div>
									</div>
								</div>
							</div>
							<div  className="tabcontent" style={activeTage==='secondTab' ? {display:"block"}:{display:"none"}}>
								<div className={'row secondTab-parent-class'}>
									<div className={'col-8'}>
										<div style={ signValue1 !== "" || signValue3 !==null ?{height:380}:{height:'auto'}} className="file_input_wrap parent-tag-value-name">
											<input type="file" name="imageUpload" id="imageUpload" accept="image/png, image/jpg, image/jpeg"
											       onChange={(e) => readImgUrlAndPreview1(e)} className="hide"/>
											Name
											<label htmlFor="imageUpload" className="btn btn-large">
												<img src={upload} className={'sign_upload_icon'}/>
												Select file
											</label>
											<p className={"error-msg"}>{profilepicerror1}</p>
											{/*<div className="img_preview_wrap">*/}
											{/*	<img src="" id="imagePreview1" alt="Preview Image" width="200px"*/}
											{/*	     className="hide"/>*/}
											{/*</div>*/}
											{mydataOrChangeData === 0 ?
												<>
													{( signValue1 !== "" || signValue3 !==null)
														? <div className={'mt-3'}>
															<img className='resizeimgNameUpload' alt={`signature `}
																 src={ TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !==null ? signValue3 :signValue1}/>
															<img alt={'close'} src={close_icon} onClick={()=>delete_sigpadEnd(TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !==null ?3:1)} className={"sign-close-icon-delete"}/>
														</div>
														: null}
												</>
												:
												<>
													{( signValue1 !== "" || signValue3 !==null)
														? <div className={'mt-3'}>
															<img className='resizeimgNameUpload' alt={`signature `}
																 src={ TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !==null ? signValue3 :signValue1}/>
															<img alt={'close'} src={close_icon} onClick={()=>delete_sigpadEnd(TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !==null ?3:1)} className={"sign-close-icon-delete"}/>
														</div>
														: null}
												</>
                                              

											}
											{/*{(TypeCode === "SIGN_UPLOAD_INITIAL" || TypeCode === "SIGN_UPLOAD_FULL") && ( signValue1 !== "" || signValue3 !==null)*/}
											{/*	? <div className={'mt-3'}>*/}
											{/*		<img alt={`signature `}*/}
											{/*			 src={ TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !==null ? signValue3 :signValue1}/>*/}
											{/*		<img alt={'close'} src={close_icon} onClick={()=>delete_sigpadEnd(TypeCode === "SIGN_UPLOAD_FULL" && signValue3 !==null ?3:1)} className={"sign-close-icon-delete"}/>*/}
											{/*	</div>*/}
											{/*	: null}*/}
										</div>


									</div>
									<div className={'col-4'}>

										<div style={signValue2 !== "" || signValue4 !==null ?{height:380}:{height:'auto'}} className="file_input_wrap parent-tag-value-name">
											<input type="file" name="imageUpload" id="imageUpload1"  accept="image/png, image/jpg, image/jpeg"
											       onChange={(e) => readImgUrlAndPreview2(e)} className="hide"/>
											Initial
											<label htmlFor="imageUpload1" className="btn btn-large">
												<img src={upload} className={'sign_upload_icon'}/>
												Select file
											</label>
											<p className={"error-msg"}>{profilepicerror2}</p>
											{/*<div className="img_preview_wrap">*/}
											{/*	<img src="" id="imagePreview2" alt="Preview Image" width="200px"*/}
											{/*	     className="hide"/>*/}
											{/*</div>*/}
											{( signValue2 !== "" || signValue4 !==null)
												? <div className={'mt-3'}>
													{/*{signValue3}*/}
													<img className='resizeimgInitialUpload' alt={`signature ${signValue2}`}
														 src={ TypeCode === "SIGN_UPLOAD_INITIAL" && signValue4 !==null ? signValue4 : signValue2}/>
													<img alt={'close'} src={close_icon} onClick={()=>delete_sigpadEnd(TypeCode === "SIGN_UPLOAD_INITIAL" && signValue4 !==null ?4:2)} className={"sign-close-icon-delete"}/>
												</div>
												: null}
										</div>
									</div>
								</div>

							</div>
							<div  className="tabcontent" style={activeTage==='thirdTab' ? {display:"block"}:{display:"none"}}>
								<div  style={{cursor:"pointer"}}  className={'thirdTab thirdTab-font-singe'}>

									<div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1"  onClick={()=>chageCheckFun(1,'att-ad-brush-script')}>
										<span  style={{width:"25%",marginLeft:161}}><input className="form-check-input" style={{cursor:"pointer"}} type="radio" name="flexRadioDefault"
										       id="flexRadioDefault1" checked={checkbox===1}
										       /></span>
										<span  style={{width:"50%",textAlign:"left"}}><label className="form-check-label att-ad-brush-script"
										     style={{cursor:"pointer"}}   htmlFor="flexRadioDefault1"
										       >
											{getUserInfo.FirstName}{" "}{getUserInfo.LastName}
										</label></span>
										<span  style={{width:"25%"}}><label className="form-check-label att-ad-brush-script"
										       htmlFor="flexRadioDefault1" style={{cursor:"pointer"}} 
										       >
											{shortName()}
											{/*{getUserInfo.LastName}*/}
										</label></span>
									</div>

									<div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1"style={{cursor:"pointer"}} onClick={()=>chageCheckFun(2,'att-ad-forte')} >
									<span style={{width:"25%",marginLeft:161}}><input className="form-check-input" type="radio" name="flexRadioDefault"
											style={{cursor:"pointer"}}   id="flexRadioDefault6" checked={checkbox===2}
										/></span>
										<span  style={{width:"50%",textAlign:"left"}}><label className="form-check-label att-ad-forte" htmlFor="flexRadioDefault6" style={{cursor:"pointer"}}
										>
											{getUserInfo.FirstName}{" "}{getUserInfo.LastName}
										</label></span>
										<span  style={{width:"25%"}}><label className="form-check-label att-ad-forte" htmlFor="flexRadioDefault6" style={{cursor:"pointer"}}
										>
											{shortName()}
										</label></span>
									</div>

									<div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1"style={{cursor:"pointer"}}  onClick={()=>chageCheckFun(3,'att-ad-freestyle-script')}>
									<span style={{width:"25%",marginLeft:161}}><input className="form-check-input" type="radio" name="flexRadioDefault"
										    style={{cursor:"pointer"}}   id="flexRadioDefault2" checked={checkbox===3}
										       /></span>
									<span style={{width:"50%",textAlign:"left"}}>	<label className="form-check-label att-ad-freestyle-script" style={{cursor:"pointer"}}
										       htmlFor="flexRadioDefault2"
										       >
											{getUserInfo.FirstName}{" "}{getUserInfo.LastName}
										</label></span>
										<span style={{width:"25%"}}><label className="form-check-label att-ad-freestyle-script" style={{cursor:"pointer"}}
										       htmlFor="flexRadioDefault2"
										       >
											{shortName()}
										</label></span>
									</div>

									<div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" style={{cursor:"pointer"}} onClick={()=>chageCheckFun(4,'att-ad-harlow-solid-italic')}>
									<span  style={{width:"25%",marginLeft:161}}><input className="form-check-input" type="radio" name="flexRadioDefault" style={{cursor:"pointer"}}
										       id="flexRadioDefault3" checked={checkbox===4}
										       /></span>
										<span  style={{width:"50%",textAlign:"left"}}><label className="form-check-label att-ad-harlow-solid-italic" style={{cursor:"pointer"}}
										       htmlFor="flexRadioDefault3"
										       >
											{getUserInfo.FirstName}{" "}{getUserInfo.LastName}
										</label></span>
										<span  style={{width:"25%"}}><label className="form-check-label att-ad-harlow-solid-italic" style={{cursor:"pointer"}}
										       htmlFor="flexRadioDefault3"
										       >
											{shortName()}
										</label></span>
									</div>

									<div className="form-check d-flex  custome-d-flex justify-content-around p-2 mb-1" style={{cursor:"pointer"}} onClick={()=>chageCheckFun(5,'att-ad-blackadder-itc')} >
									<span  style={{width:"25%",marginLeft:161}}><input className="form-check-input" type="radio" name="flexRadioDefault" style={{cursor:"pointer"}}
										       id="flexRadioDefault4" checked={checkbox===5}
										      /></span>
											<span style={{width:"50%",textAlign:"left"}}><label className="form-check-label att-ad-blackadder-itc" style={{cursor:"pointer"}}
										       htmlFor="flexRadioDefault4"
										       >
											{getUserInfo.FirstName}{" "}{getUserInfo.LastName}
										</label></span>
										<span  style={{width:"25%"}}><label className="form-check-label att-ad-blackadder-itc" style={{cursor:"pointer"}}
										       htmlFor="flexRadioDefault4"
										      >
											{shortName()}
										</label></span>
									</div>

									<div className="form-check d-flex custome-d-flex justify-content-around p-2 mb-1" style={{cursor:"pointer"}} onClick={()=>chageCheckFun(6,'att-ad-vivaldi')} >
									<span  style={{width:"25%",marginLeft:161}}><input className="form-check-input" type="radio" name="flexRadioDefault" style={{cursor:"pointer"}}
										       id="flexRadioDefault5" checked={checkbox===6}/></span>
									<span  style={{width:"50%",textAlign:"left"}}><label className="form-check-label att-ad-vivaldi" style={{cursor:"pointer"}} htmlFor="flexRadioDefault5">
											{getUserInfo.FirstName}{" "}{getUserInfo.LastName}
										</label></span>	
										<span  style={{width:"25%"}}><label className="form-check-label att-ad-vivaldi" style={{cursor:"pointer"}} htmlFor="flexRadioDefault5">
											{shortName()}
										</label></span>
									</div>


								</div>
							</div>
						</div>
					</div>

					<div style={{display:"flex",justifyContent:"end",gap:"15px"}} className={"form-action mt-3"}>
				

								{/* <button className="btn clr-btn" onClick={()=>clear()}>CLEAR</button> */}
								{/* <button  style={{backgroundColor:"blue"}} className="btn save-btn" onClick={() => save()}>SAVE</button> */}
								<button type="button" className="btn btn-secondary" style={{width:105}}
									         onClick={()=>clear()}
									>
										CLEAR
									</button>
									<button type="button" className="btn save-btn" style={{color:"white",width:110,backgroundColor:"#2F7FED",border:"1px solid #2F7FED"}}
									       onClick={() => save()}
											>
											SAVE
										</button>
							
					</div>
				
				</div>
			</div>
		</div>
	)
}

export {Signatures};