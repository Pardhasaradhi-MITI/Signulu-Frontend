import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import {commonService} from "../_services/common.service";
import {usersService} from "../_services/users.service";
import * as EmailValidator from "email-validator";
import axios from "axios";
import {Avatar} from "@mui/material";
import config from 'config';
const API_END_POINT = config.serverUrl

function PersonalInformationsss({ history, location }) {
    const [profilePic1, setProfilePic1] = useState(null);
    const [profilePic2, setProfilePic2] = useState(null);
    const [END_URL, setEndUrl] = useState(commonService.getImageUrl())
    const [Prefix, setPrefix] = useState('');
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailAddress, setEmailAddress] = useState("");
    const [Title, setTitle] = useState('');
    const [addressLine1, setAddressLine1] = useState("");
    const [addressLine2, setAddressLine2] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zipCode, setZipCode] = useState();
    const [cellPhone, setCellPhone] = useState();
    const [Designation, setDesignation] = useState('');
    const [result,setResult]=useState({})
    const [error, setError] = useState({
        firstName: '',
        lastName: '',
        emailAddress: '',
        title:'',
        designation: '',
        zipCode: '',
        addressLine1: '',
        addressLine2: '',
        city: '',
        state: '',
        cell: ''
    });
    const [count,setcount]=useState(0)
    const [imageChanged,setImageChanged]=useState(1)




    useEffect(() => {
        selectUser()
    }, []);

    const selectUser = () => {

        usersService.getCurrentprofile()
            .then((result) => {
                let temp
                if (result) {
                    //       
                    setResult(result.Entity)
                    setProfilePic1(END_URL + result.Entity?.ProfilePicture)
                    setPrefix(result.Entity.Prefix)
                    setFirstName(result.Entity.FirstName)
                    setLastName(result.Entity.LastName)
                    setEmailAddress(result.Entity.Email)
                    setTitle(result.Entity.Title)
                    setAddressLine1(result.Entity.Address1)
                    setAddressLine2(result.Entity.Address2)
                    setCity(result.Entity.City)
                    setState(result.Entity.State)
                    // temp=result.Entity.PhoneInfo[0].Phone
                    // setCellPhone( temp.substr(1, 10))
                    setCellPhone(temp)
                    // temp=
                    // setZipCode(temp.substr(1, 6) )
                    setZipCode(result.Entity.ZipCode)
                    setDesignation(result.Entity.Designation)
                    temp=(result.Entity && result.Entity.PhoneInfo && result.Entity.PhoneInfo[0].Phone).match(/\d+/g)
                    setCellPhone(temp[0])
                    if (result.Entity.ZipCode){
                        temp=(result.Entity && result.Entity.ZipCode).match(/\d+/g)
                        setZipCode(temp[0])
                    }

                }
            })
            .catch(error => {

            });
    }

    const handleInputChange = (feild, e) => {
        //       
        let value = e.target.value;
        let errors = error;
        switch (feild) {
            case 'Prefix':
                setPrefix(value)
                break;
            case 'firstName':
                if (value === "") {
                    errors.firstName = 'First Name is required.';
                } else if (value.length < 3) {
                    errors.firstName = 'First Name must be atleast 3 characters long.';
                } else {
                    errors.firstName = ''
                }
                setFirstName(value);
                setError(errors);
                break;
            case 'lastName':
                if (value === "") {
                    errors.lastName = 'Last Name is required.';
                } else {
                    errors.lastName = ''
                }
                setLastName(value);
                setError(errors);
                break;
            case 'title':
                if (value===""){
                    errors.title = 'Title is required.';
                }else {
                    errors.title=''
                }
                setTitle(value)
                setError(errors)
                break;
            case 'emailAddress':
                if (value === "") {
                    errors.emailAddress = 'Email Address is required.';
                    setEmailAddress(value);
                    setError(errors);
                } else if (EmailValidator.validate(value) === false) {
                    errors.emailAddress = 'Enter correct email address.';
                    setEmailAddress(value);
                    setError(errors);
                } else if (EmailValidator.validate(value) === true) {
                    setEmailAddress(value);
                    checEmail(value).then(function (data) {
                        if (data === true) {

                            errors.emailAddress = 'Email already exist with another user';
                        } else {
                            errors.emailAddress = '';
                        }

                        setEmailAddress(value);
                        setError(errors);

                        // setError(errors);
                    })

                }

                break;
            case 'zipCode':
                errors.zipCode = value.length < 5 ? 'ZipCode must be atleast 5 digits long.' : '';
                setZipCode(value);
                setError(errors);
                break;
            case 'addressLine1':
                if (value.length < 3) {
                    errors.addressLine1 = 'Address Line #1 must be atleast 3 characters long.';
                } else {
                    errors.addressLine1 = ''
                }
                setAddressLine1(value);
                setError(errors);
                break;
            case 'addressLine2':
                if (value.length < 3) {
                    errors.addressLine2 = 'Address Line #2 must be atleast 3 characters long.';
                } else {
                    errors.addressLine2 = ''
                }
                setAddressLine2(value);
                setError(errors);
                break;
            case 'city':
                if (value.length < 3) {
                    errors.city = 'City must be atleast 2 characters.';
                } else {
                    errors.city = ''
                }
                setCity(value);
                setError(errors);
                break;
            case 'state':
                if (value.length < 3) {
                    errors.state = 'State must be atleast 2 characters.';
                } else {
                    errors.state = ''
                }
                setState(value);
                setError(errors);
                break;
            case 'designation':
                errors.designation = value === "" ? 'Designation is required.' : '';
                setDesignation(value);
                setError(errors);
                break;
            case 'cellPhone':
                if (cellPhone === ""){
                    errors.cell='Phone Number is required.'
                }else if(cellPhone.length < 10){
                    errors.cell='Phone Number is less then 12 digit.'
                }
                setCellPhone(value)
                setError(errors)
            default:
                break;
        }

    }

    const editUser = () => {
        //       
        let object = result
        object.Prefix=Prefix
        object.AddressInfo[0].Address1=addressLine1
        object.AddressInfo[0].Address2=addressLine2
        object.FirstName=firstName
        object.LastName=lastName
        object.Title=Title
        object.Designation=Designation
        object.AddressInfo[0].State=state
        object.AddressInfo[0].City=city
        object.ZipCode=zipCode
        object.PhoneInfo[0].Phone=cellPhone
        object.ProfilePicture=profilePic2 && profilePic2.name
        console.log(object)
        let input = {
            Address1: addressLine1,
            Address2: addressLine2,
            City: city,
            title:Title,
            Designation: Designation,
            Email: emailAddress,
            FirstName: firstName,
            LastName: lastName,
            Phone:cellPhone,
            // ProfilePicture: profilePic,
            State: state,
            ZipCode: zipCode,

        }
        const validateForm = () => {
            let count = 0;
            let tmp_errors = {...error};
            if (firstName === "") {
                tmp_errors.firstName = 'First Name is required.';
                count = count + 1;
            } else if (firstName.length < 3) {
                tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
                count = count + 1;
            }
            if (lastName === "") {
                tmp_errors.lastName = 'Last Name is required.';
                count = count + 1;
            }
            if (emailAddress === "") {
                tmp_errors.emailAddress = 'Email Address is required.';
                count = count + 1;
            } else if (EmailValidator.validate(emailAddress) === false) {
                tmp_errors.emailAddress = 'Enter correct email address.';
                count = count + 1;
            }
            if (Designation === "") {
                tmp_errors.designation = 'Designation is required.';
                count = count + 1;
            }
            if (Title === "") {
                tmp_errors.title = 'Title is required.';
                count = count + 1;
            }
            if (cellPhone === "") {
                tmp_errors.cell = 'cell is required.';
                count = count + 1;
            }
            if (count <= 0) {
                tmp_errors = {
                    firstName: '',
                    lastName: '',
                    emailAddress: '',
                    designation: '',
                    zipCode: '',
                    addressLine1: '',
                    addressLine2: '',
                    city: '',
                    state: '',
                    Title: '',
                    cell:''

                }
            }
            setError(tmp_errors);

            return count <= 0;
        }

        if (validateForm()) {


            let formData = new FormData();
            formData.append("uploads[]", profilePic2);

            let token = localStorage.getItem('LoginToken');
            let config;
            if (token !== null && token !== undefined && token !== "") {
                config = {
                    headers: {
                        'Accept': 'application/json',
                        'userauthtoken': token
                    }
                };
            } else {
                config = {
                    headers: {
                        'Accept': 'application/json'
                    }
                };
            }
            axios.post(`${API_END_POINT}users/upload`, formData, config)
                .then(function (response) {
                    //       
                    object.ProfilePicture=response.data.Data.FileName

                    usersService.updateCurrentprofile(object)
                        .then((result) => {
                            //       
                            if (result.HttpStatusCode === 200) {
                                // setOpenSuccessModal(true);
                                selectUser();
                                // resetForm();
                            }
                        })
                        .catch(error => {

                        });

                })
                .catch(function (err) {
                    // cogoToast.error(err, {position: 'top-center'});
                });









        }
    }

    const handleProfilepic=(e)=> {
        // let tmp_profile_data = {...profileDetail};
        let temp=result
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            setProfilePic2(file);
            setProfilePic1(reader.result);
            temp.ProfilePicture=reader.result
            setResult(temp)

            setImageChanged(1)
        }
        // tmp_profile_data['profile'] = {
        //     ...tmp_profile_data['profile'],
        //     profilePic: reader.result
        // }

        // setProfileDetail(tmp_profile_data);

        reader.readAsDataURL(file);

    }

    function stringAvatar() {
              
        let fullname= `${firstName} ${lastName}`
        let name = fullname ?fullname : 'N A'
        return {
            sx: {
                bgcolor: "#2F7FED",
                width:'47px',
                height:'47px'
            },
            children: (name.split(' ') && name.split(' ')[0] && name.split(' ')[1]) ? `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`: 'NA',
        };
    }

    const deleteProfilePic=()=>{
              
        let temp =result
        temp.ProfilePicture=null
        setResult(temp)
        setcount(count+1)
    }



    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content personalinformations">
                    <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <h3>Profile Information</h3>
                                <p>To configure what information is shared when you sign.</p>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
                                <div class="progress">
                                    <div class="progress-bar bar-width-md" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        <span class="value__bar_sm">25% Complete</span>
                                        <span class="value__bar_md">48% Complete</span>
                                        <span class="value__bar_lg">75% Complete</span>
                                        <span class="value__bar_xl">100% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='row align-items-center'>
                        <div className='profile-info-content'>
                            <div className='row'>
                                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                                    <div className='profile-information'>


                                        {/*<div className='user-profile-img'>*/}

                                        {/*    <img src='src/images/profile.jpg' />*/}
                                        {/*    <div class="crez-btn">*/}
                                        {/*        <input type="file" id="real-file" hidden="hidden" />*/}
                                        {/*        <button type="button" id="custom-button"><img src="src/images/camera.svg" alt="" /></button>*/}
                                        {/*    </div>*/}
                                        {/*</div>*/}
                                        {/*<span class="user-name">Edit Profile Picture</span>*/}
                                        <div className={'parent-profile-pic text-center'}>
                                            {  result.ProfilePicture !== null  ?<img src={profilePic1} alt="" className="user-picture" />: <Avatar {...stringAvatar()} />}
                                            <div className={'mt-3 mb-2'} onClick={()=>deleteProfilePic()}> X </div>
                                            <input className={"code uploadprofileimage"} id="file-input" type="file"
                                                   onChange={(e) => handleProfilepic(e)}/>
                                            <div className={'w-60 img_profile'}>
                                                <label htmlFor="file-input">
                                                    Change Profile pic
                                                </label>
                                            </div>
                                        </div>



                                    </div>
                                    <div className='tab-list-content'>
                                        <ul className='tab-list'>
                                            <li><button class="link-tab active done"><div class="li-count">1</div> <div class="text-btn">Personal Information</div> </button></li>
                                            <li><button class="link-tab active"><div class="li-count">2</div> <div class="text-btn">Company Information</div>  </button></li>
                                            <li><button class="link-tab"><div class="li-count">3</div> <div class="text-btn">Location Information</div> </button></li>
                                            <li><button class="link-tab"><div class="li-count">4</div> <div class="text-btn">Contact Information</div>  </button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                    <div className='tab-content'>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Personal Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="James" value={firstName} onChange={(e)=>handleInputChange('firstName',e)} />
                                                            <label for="">First Name<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" value={lastName} placeholder="Franco" onChange={(e)=>handleInputChange('lastName',e)} />
                                                            <label for="">Last Name<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" value={emailAddress} readOnly={true} onChange={(e)=>handleInputChange('emailAddress',e)} placeholder="info.james@gmail.com" />
                                                            <label for="">Email<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='company-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Company Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder=" California" value={Title} onChange={(e)=>handleInputChange('title',e)} />
                                                            <label for="">Job Title</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="Businessman"  value={Designation} onChange={(e)=>handleInputChange('designation',e)} />
                                                            <label for="">Designation<span>*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea />
                                                            <label for="">Additional Information</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Location Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea placeholder=' California'  value={addressLine1} onChange={(e)=>handleInputChange('addressLine1',e)}/>
                                                            <label for="">Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea placeholder=' California'  value={addressLine2} onChange={(e)=>handleInputChange('addressLine2',e)} />
                                                            <label for="">Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="San Francisco"  value={city} onChange={(e)=>handleInputChange('city',e)} />
                                                            <label for="">City<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" placeholder="Oakland" value={state} onChange={(e)=>handleInputChange('state',e)} />
                                                            <label for="">State<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={zipCode} onChange={(e)=>handleInputChange('zipCode',e)} />
                                                            <label for="">Zip Code<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='contact-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Contact Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" value={cellPhone} onChange={(e)=>handleInputChange('cellPhone',e)} />
                                                            <label for="">Phone<span>*</span> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <select>
                                                                <option style={{ backgroundImage: "url(src/images/instagram_icon.svg)" }}>Instagram</option>
                                                            </select>
                                                            <label for="">Select Social Media</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-8 col-lg-8 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" />
                                                            <label for="">Add Link</label>
                                                            <button className="btn add_link_btn">ADD</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="social-tags">
                                                        <a href="#" className="link"><img src="src/images/twitter.svg" alt="" /> Twitter <span className="hide">x</span></a>
                                                        <a href="#" className="link"><img src="src/images/linkedin.svg" alt="" /> Linkedin <span className="hide">x</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div class="subf_pagn_btn">
                                                    <button class="btn" onClick={()=>editUser()}>Save</button>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { PersonalInformationsss };