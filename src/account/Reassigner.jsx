import React, { useState } from "react";
import "../css/AadhaarInfo.css"
import Swal from "sweetalert2"
import { Button, ButtonToolbar, Modal, ListGroup, ListGroupItem } from 'react-bootstrap';

function Reassignsigner(location) {
    console.log(location, 'test location')
    const [rname,setname]=useState(location?.state !== undefined?location?.state?.Name:"")
    const [reassignsignerpopup, setReassignsignerpopup] = useState(true)
    return (
        <div style={{ overflowX: "hidden" }}>
            <div className="row coloradd1">
                <div className="column image2">
                    <img src="src/images/signulu_black_logo2.svg" alt="" className="img-fluid3 " />
                </div>
            </div>
            <h4 style={{ textAlign: "center", paddingTop: "40px" }}>DocuSign</h4>
            <div>
                <Modal show={reassignsignerpopup}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header >
                        <Modal.Title id="example-modal-sizes-title-lg">
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{ backgroundColor: "#F9F9F9" }}>
                        <div>
                            <h3 style={{ textAlign: "center", paddingBottom: "40px" }}>You've Reassigned The Document To {rname}</h3>
                        </div>
                        <div>
                            <div class='landing-icon'>
                                <img src="src/images/verified.png" alt="" />

                            </div>
                            <h5 style={{ textAlign: "center", paddingTop: "55px", border: "0.5px solid #aaa6a6", paddingBottom: "40px", color: "#484c48" }}>We've notoified the sender and new signer. You'll recieve an email copy once everyone has signed.</h5>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {/* <Button variant="secondary"
            //  onClick={() => {
            //   setshowdeletedocumentmodule(false)
            //   setSelecteddoc(null)
            //   setdeletedocreason("")
           // }}
            >CANCEL</Button> */}
                        {/* <Button
             // disabled={signersname.length<1 || signersemail.length<1}
             // onClick={()=>{reassignSigner()}}
            // disabled={deletedocreason.length>0 && deletedocreason !== null?false:true}  onClick={() => {
            //  deletedocument()
            //  setshowdeletedocumentmodule(false)
            //  setdeletedocreason("")
            // }}
            >OKAY</Button> */}
                    </Modal.Footer>
                </Modal>
            </div>
        </div>
    )
}
export { Reassignsigner }

