import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';

import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

// tag button active start //

function myFunc1() {
    var para = document.getElementById("conditional1");

    if (para.classList.contains("tagActive-1")) {
        para.classList.remove("tagActive-1");
    } else {
        para.classList.add("tagActive-1");
    }
}
function myFunc2() {
    var para = document.getElementById("conditional2");

    if (para.classList.contains("tagActive-2")) {
        para.classList.remove("tagActive-2");
    } else {
        para.classList.add("tagActive-2");
    }
}
function myFunc3() {
    var para = document.getElementById("conditional3");

    if (para.classList.contains("tagActive-3")) {
        para.classList.remove("tagActive-3");
    } else {
        para.classList.add("tagActive-3");
    }
}
function myFunc4() {
    var para = document.getElementById("conditional4");

    if (para.classList.contains("tagActive-4")) {
        para.classList.remove("tagActive-4");
    } else {
        para.classList.add("tagActive-4");
    }
}

function TemplateOther({ history, location }) {
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content">
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <h3 className="sec-title">Start From A Template</h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12">
                            <div className="tab-nav-list">
                                <nav>
                                    <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                        <button className="nav-link active" id="nav-all-tab" data-bs-toggle="tab" data-bs-target="#nav-all" type="button" role="tab" aria-controls="nav-all" aria-selected="true">All</button>
                                        <button className="nav-link" id="nav-template-tab" data-bs-toggle="tab" data-bs-target="#nav-template" type="button" role="tab" aria-controls="nav-template" aria-selected="false">My Templates</button>
                                        <button className="nav-link" id="nav-free-tab" data-bs-toggle="tab" data-bs-target="#nav-free" type="button" role="tab" aria-controls="nav-free" aria-selected="false">Free</button>
                                        <button className="nav-link" id="nav-premium-tab" data-bs-toggle="tab" data-bs-target="#nav-premium" type="button" role="tab" aria-controls="nav-premium" aria-selected="false">Premium</button>
                                    </div>
                                </nav>
                            </div>
                        </div>

                        <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                            <div className="recipient-btn">
                                <button className="btn">
                                    <img src="src/images/plus_icon.svg" alt />
                                    Add New Recipient
                                </button>
                            </div>
                        </div>

                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="document-content">
                                <div className="tab-content" id="nav-tabContent">

                                    <div className="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
                                        <div className="row slider-item light-bg-gray">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className="tag-listin">
                                                    <a href="javascript:void(0);" id>Insurance</a>
                                                    <a href="javascript:void(0);" id="conditional1" onClick={myFunc1}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Corporate
                                                    </a>
                                                    <a href="javascript:void(0);">Purchasing</a>
                                                    <a href="javascript:void(0);">Fleet</a>
                                                    <a href="javascript:void(0);" id="conditional2" onClick={myFunc2}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Human Resource
                                                    </a>
                                                    <a href="javascript:void(0);">Recruitment</a>
                                                    <a href="javascript:void(0);">Procedures</a>
                                                    <a href="javascript:void(0);">Reporting</a>
                                                    <a href="javascript:void(0);">Payroll</a>
                                                    <a href="javascript:void(0);">Tender Templates</a>
                                                    <a href="javascript:void(0);" id="conditional3" onClick={myFunc3}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Marketing
                                                    </a>
                                                    <a href="javascript:void(0);">Manuals</a>
                                                    <a href="javascript:void(0);">Accounts Receivable</a>
                                                    <a href="javascript:void(0);" id="conditional4" onClick={myFunc4}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Workcover
                                                    </a>
                                                </div>
                                                <div className="document-box">
                                                    <div className="row document-wrapper">
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_3.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="tab-pane fade" id="nav-template" role="tabpanel" aria-labelledby="nav-template-tab">
                                        <div className="row slider-item light-bg-gray">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className="tag-listin">
                                                    <a href="#" id>Insurance</a>
                                                    <a href="#" id="conditional1" onclick="myFunc1()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Corporate
                                                    </a>
                                                    <a href="#">Purchasing</a>
                                                    <a href="#">Fleet</a>
                                                    <a href="#" id="conditional2" onclick="myFunc2()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Human Resource
                                                    </a>
                                                    <a href="#">Recruitment</a>
                                                    <a href="#">Procedures</a>
                                                    <a href="#">Reporting</a>
                                                    <a href="#">Payroll</a>
                                                    <a href="#">Tender Templates</a>
                                                    <a href="#" id="conditional3" onclick="myFunc3()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Marketing
                                                    </a>
                                                    <a href="#">Manuals</a>
                                                    <a href="#">Accounts Receivable</a>
                                                    <a href="#" id="conditional4" onclick="myFunc4()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Workcover
                                                    </a>
                                                </div>
                                                <div className="document-box">
                                                    <div className="row document-wrapper">
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_3.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="tab-pane fade" id="nav-free" role="tabpanel" aria-labelledby="nav-free-tab">
                                        <div className="row slider-item light-bg-gray">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className="tag-listin">
                                                    <a href="#" id>Insurance</a>
                                                    <a href="#" id="conditional1" onclick="myFunc1()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Corporate
                                                    </a>
                                                    <a href="#">Purchasing</a>
                                                    <a href="#">Fleet</a>
                                                    <a href="#" id="conditional2" onclick="myFunc2()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Human Resource
                                                    </a>
                                                    <a href="#">Recruitment</a>
                                                    <a href="#">Procedures</a>
                                                    <a href="#">Reporting</a>
                                                    <a href="#">Payroll</a>
                                                    <a href="#">Tender Templates</a>
                                                    <a href="#" id="conditional3" onclick="myFunc3()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Marketing
                                                    </a>
                                                    <a href="#">Manuals</a>
                                                    <a href="#">Accounts Receivable</a>
                                                    <a href="#" id="conditional4" onclick="myFunc4()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Workcover
                                                    </a>
                                                </div>
                                                <div className="document-box">
                                                    <div className="row document-wrapper">
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_3.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="tab-pane fade" id="nav-premium" role="tabpanel" aria-labelledby="nav-premium-tab">
                                        <div className="row slider-item light-bg-gray">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className="tag-listin">
                                                    <a href="#" id>Insurance</a>
                                                    <a href="#" id="conditional1" onclick="myFunc1()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Corporate
                                                    </a>
                                                    <a href="#">Purchasing</a>
                                                    <a href="#">Fleet</a>
                                                    <a href="#" id="conditional2" onclick="myFunc2()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Human Resource
                                                    </a>
                                                    <a href="#">Recruitment</a>
                                                    <a href="#">Procedures</a>
                                                    <a href="#">Reporting</a>
                                                    <a href="#">Payroll</a>
                                                    <a href="#">Tender Templates</a>
                                                    <a href="#" id="conditional3" onclick="myFunc3()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Marketing
                                                    </a>
                                                    <a href="#">Manuals</a>
                                                    <a href="#">Accounts Receivable</a>
                                                    <a href="#" id="conditional4" onclick="myFunc4()">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width={15} height={15} fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                                        </svg>
                                                        Workcover
                                                    </a>
                                                </div>
                                                <div className="document-box">
                                                    <div className="row document-wrapper">
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm ftr-itm-other">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_2.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Project Proposal</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icon_3.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                                            <div className="dtl-card">
                                                                <div className="top-itm">
                                                                    <div className="dropdown">
                                                                        <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                                                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                                            </svg>
                                                                        </button>
                                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                            <li><a className="dropdown-item" href="#">Action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <img src="src/images/document_icon_lg.svg" alt="" />
                                                                    <div className="ftr-itm">
                                                                        <h6>Business Letter</h6>
                                                                        <div className="lst">
                                                                            <img src="src/images/document_icons_1.svg" alt="" />
                                                                            <span>Opened 6:20 PM</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="document-nagin">
                                <nav aria-label="Page navigation">
                                    <ul className="pagination">
                                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                                        <li className="page-item">
                                            <a className="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">•••</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export { TemplateOther }; 