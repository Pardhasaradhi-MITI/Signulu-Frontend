import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function MyTemplates({ history, location }) {

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content mytemplates">
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <h3>My Templates</h3>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div className="recipient-btn">
                                <button className="btn back_btn"><img src="src/images/back_btn.svg" alt="" />Back To Home</button>
                                <select className='hlfinput'>
                                    <option>December</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                            <div className='document_block'>
                                <ul className='content_area'>
                                    <li><h6>Document</h6><h5>4 Pages Document-Converted</h5></li>
                                    <li><h6>Created By</h6><h5>James Franco</h5></li>
                                    <li><h6>Is Public</h6><h5>Private</h5></li>
                                    <li><h6>Updated</h6><h5>10/06/2021</h5></li>
                                </ul>
                                <ul className='action_area'>
                                    <li><a href='#'><img src='src/images/eye.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/bluk_send.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/download.svg' alt='' /></a></li>
                                    <li><a href='#'><img src='src/images/delete_icon.svg' alt='' /></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="document-nagin">
                                <nav aria-label="Page navigation">
                                    <ul className="pagination">
                                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                                        <li className="page-item">
                                            <a className="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">•••</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { MyTemplates }; 