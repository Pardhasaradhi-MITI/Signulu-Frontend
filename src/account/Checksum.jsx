
import React, { useState, useEffect } from 'react';
import * as SparkMD5 from 'spark-md5';

const Checksum = () => {
    const [change, setChange] = useState(true);
    const [hash, sethash] = useState();
    const [hashKey, sethashKey] = useState();
    const [userinput, setuserinput] = useState('');
    const [fileArray, setfileArray] = useState([]);
    const [message, setmessage] = useState('');
    const [error, seterror] = useState('');
    const [myfile, setmyfile] = useState('');
    const [IsFileUploaded, setIsFileUploaded] = useState(false);
    const [className, setclassName] = useState('');
    const [successMessage, setsuccessMessage] = useState(false);
    const [failedMessage, setfailedMessage] = useState(false);

    useEffect(() => {
        getTags()
    },[])


      function clearFileInfo() {
        setfileArray([])
        setuserinput('')
        setmyfile('')
        setIsFileUploaded(false)
        setmessage()
        setsuccessMessage(false);
        setfailedMessage(false);
        setTimeout(() => {
            getTags()
        }, 1000);
        
      }
      function verifyFile() {
        setsuccessMessage(false);
        setfailedMessage(false);
        console.log(hash)
        if (hash) {
            if (userinput == hash) {
                setmessage('Document has verified successfully.');
                setsuccessMessage(true);
            } else {
                setmessage('Generated hash not match with uploaded document.');
                setfailedMessage(true);
            }
        } else {
            alert('Please upload the file.');
        }
    }
    function getTags() {
        document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
            const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
            // console.log(dropZoneElement.childNodes[0].div, 'dropZoneElementdropZoneElement')

            dropZoneElement.addEventListener("click", (e) => {
                if (e.target.className != 'file-add-text') {
                    inputElement.click();
                }
            });

            inputElement.addEventListener("change", (e) => {
                if (inputElement.files.length) {
                    for (let inputFile of inputElement.files) {
                        updateThumbnail(dropZoneElement, inputFile, false);
                    }
                    var inputVal = inputElement.value;
                }
            });
            dropZoneElement.addEventListener("dragover", (e) => {
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.add("drop-zone--over");
            });
            dropZoneElement.addEventListener('dragleave', function(e) {
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.remove("drop-zone--over");
            });

            ["dragleave", "dragend"].forEach((type) => {
                dropZoneElement.addEventListener(type, (e) => {
                });
            });

            dropZoneElement.addEventListener("drop", (e) => {
                if (e.dataTransfer.files.length) {
                    inputElement.files = e.dataTransfer.files;
                    let exist = false
                    if (e.target.className == 'file-add-text' || e.target.className == 'file-add use_templete drop-zone') {
                        exist = true
                    }
                    console.log({ e}, e.dataTransfer.files)
                    for (let inputFile of e.dataTransfer.files) {
                        updateThumbnail(dropZoneElement, inputFile, exist);
                    }
                }
                e.preventDefault();
                e.stopPropagation();
                dropZoneElement.classList.remove("drop-zone--over");
            });
        });

    }
    /**
 * Updates the thumbnail on a drop zone element.
 *
 * @param {HTMLElement} dropZoneElement
 * @param {File} file
 * 
 */
    function updateThumbnail(dropZoneElement, file, exist) {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
            //   dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }

        // First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
            thumbnailElement = document.createElement("div");
            //   thumbnailElement.classList.add("drop-zone__thumb");
            // dropZoneElement.appendChild(thumbnailElement);
        }
        // thumbnailElement.dataset.label = file.name;
        // Show thumbnail for image files
        if (file.type.startsWith("image/")) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                thumbnailElement.style.backgroundImage = `url('${reader.result}')`;

            };
        } else {
            thumbnailElement.style.backgroundImage = null;
        }
        saveFiles(file)
        console.log(file, '&&&&&&')
    }

    function saveFiles(files) {
        //     if (files.length > 1){ seterror("Only one file at time allow");
        // }else {
        //     error = "";
        process(files);
        // }
    }

    function process(file) {
        setfileArray(file);
        var ext = file.name.split('.');
        setclassName('')
        switch(ext[1]) {
            case 'pdf':
                setclassName('fa fa-file-pdf');
                break;
            case 'png':
            case 'jpg':
            case 'jpeg':
                setclassName('fa fa-picture');
                break;
            case 'xlx':
            case 'xlsx':
                setclassName('fa fa-file-excel');
                break;
            case 'doc':
            case 'docx':
                setclassName('fa fa-file-word');
                break;
            default:
                setclassName('fa fa-file');
                break;
        }
        sethashKey();
        setIsFileUploaded(true);
        setmessage('');
        setsuccessMessage(false);
        setfailedMessage(false);
        computeChecksumMd5(file)
    }

    /** MD5 logic */
    function computeChecksumMd5(file) {
        const chunkSize = 2097152;
        const spark = new SparkMD5.ArrayBuffer();
        const fileReader = new FileReader();

        let cursor = 0;
        fileReader.onerror = function() {
            return 'MD5 computation failed - error reading the file';
        };

        function processChunk(chunk_start) {
            const chunk_end = Math.min(file.size, chunk_start + chunkSize);
            fileReader.readAsArrayBuffer(file.slice(chunk_start, chunk_end));
        }
        fileReader.onload = function(e) {
            spark.append(e.target.result);
            cursor += chunkSize;

            if (cursor < file.size) {
                processChunk(cursor);
            } else {
                sethash(spark.end())
                return ;
            }
        };

        processChunk(0);
    }


    return (
        <div>
            <nav className='navbar' style={{ marginBottom: '0px !important', zIndex: '1', background: '#2f7fed' }}>
                <a href="" className='navbar-brand'>
                    <img src="src/images/signulu_black_logo.svg" style={{ marginLeft: '10px' }}></img>
                </a>
            </nav>
            <div>
                <div className='container'>
                    <div className='card row' style={{ marginTop: '10px' }}>
                        <div className='modal-header' style={{ background: ' #2f7fed', color: 'white' }}>
                            <h5>Select document to verify</h5>
                        </div>
                        <div className='modal-body chucksum' style={{ justifyContent: 'center', display: 'flex', minHeight: '300px' }}>
                            {!IsFileUploaded ? <div className='file-add file-upload use_templete drop-zone' style={{ outline: 'none', width: '300px', height: '300px' }}>
                                <div className="drop-zone__prompt">
                                    <input type="file" onClick={(event) => { event.target.value = null }} accept=".pdf" name="myFile" className="drop-zone__input dragUpload" />
                                </div>
                                <div className="file-add-img-wrapper">
                                    <img src="./src/images/file-add.svg" alt="file-add" className='file-add-img' style={{ height: '150%' }} />
                                </div>
                                <p className='file-add-text'>Drop your files here or</p>
                                <div style={{ cursor: 'pointer', position: 'relative' }}>
                                    <button className='file-upload-btn' onClick={(e) => {}} style={{marginTop:'-25px'}}>upload
                                        <span className='file-add-btn-arr'><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M5 10l7-7m0 0l7 7m-7-7v18" />
                                        </svg>
                                        </span>
                                    </button>
                                </div>
                            </div>: null }
                            {IsFileUploaded ?

                                <div>
                                    {(!successMessage && !failedMessage) ?
                                        <div style={{justifyContent:'center',padding:'90px'}} className="align">
                                            <i className={className} aria-hidden="true" style={{ zoom: '4',display:'flex',justifyContent:'center'}}></i>
                                            <p className='file-add-text' style={{ marginRight: '10px',marginTop:'10px' }}> {fileArray.name}</p>
                                        </div> : null}
                                    { successMessage ? <div>
                                        <div style={{ justifyContent: 'center', display: 'flex' }}>
                                            <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                                <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                                <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" /></svg>
                                        </div>
                                        <div style={{ justifyContent: 'center', display: 'flex' }}>
                                            <p style={{ lineHeight: '4', marginRight: '10px' }}>{fileArray.name}</p>
                                            <i className={className} aria-hidden="true" style={{ lineHeight: '2.5', zoom:'1.5' }}></i>
                                        </div>
                                        <p className='text-center' style={{ lineHeight:'1.5'}}>The document uploaded matches with the checksum number provided. The document uploaded has been verified for its authenticity.</p>
                                    </div> : null }

                                    { failedMessage ? <div>
                                        <div className='cross_check' style={{ justifyContent: 'center', display: 'flex' }}>
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                                <circle className="path circle" fill="none" stroke="#D06079" strokeWidth="6" strokeMiterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                                                <line className="path line" fill="none" stroke="#D06079" strokeWidth="6" strokeLinecap="round" strokeMiterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3" />
                                                <line className="path line" fill="none" stroke="#D06079" strokeWidth="6" strokeLinecap="round" strokeMiterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2" />
                                            </svg>
                                        </div>
                                        <div style={{ justifyContent: 'center', display: 'flex' }}>
                                            <p style={{ lineHeight: '4', marginRight: '10px' }}>{fileArray.name}</p>
                                            <i className={className} aria-hidden="true" style={{ lineHeight: '2.5', zoom:'1.5' }}></i>
                                        </div>
                                        <p className='text-center' style={{lineHeight:'1.5'}}>The document uploaded does not match with the checksum number provided. Hence the system cannot verify its authenticity. Kindly enter the right details for verification.</p>
                                    </div>: null }
                                </div> : null}

                        </div>
                    </div>
                    <div className='row'>
                        <label style={{ lineHeight: '2.5', padding: '0px' }}>Enter Document Checksum Value (32 Digit):</label>
                        <div className='col-md-12' style={{ padding: '0px' }}>
                            <input type="text" style={{ width: '100%' }} maxLength="32" value={userinput} onChange={(event) => { setuserinput(event.target.value) }} />
                        </div>
                    </div>
                    <div className={'slide-tab-btn lg:bottom-0 scroll-btn'} style={{ justifyContent: 'center', display: 'flex' }}>
                        <button className="btn next-btn" style={{border: '1px solid #858F9F', margin:'10px'}} onClick={() => { clearFileInfo()}}>CLEAR</button>
                        <button className="btn prev-btn" disabled={!userinput || !IsFileUploaded} style={{background:'#F06B30', color:'white', margin:'10px' }} onClick={() => {verifyFile() }}>VERIFY</button>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Checksum;