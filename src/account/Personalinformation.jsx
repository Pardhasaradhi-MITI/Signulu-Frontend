import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ProgressBar from 'react-bootstrap/ProgressBar';
import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import {commonService} from "../_services/common.service";
import {usersService} from "../_services/users.service";
import * as EmailValidator from "email-validator";
import axios from "axios";
import {Avatar} from "@mui/material";
import camera from "../../src/images/camera.svg"
import config from 'config';
const API_END_POINT = config.serverUrl

function PersonalInformations({ history, location }) {
    const [profilePic1, setProfilePic1] = useState(null);
    const [profilePicfortopbar, setprofilePicfortopbar] = useState(null);
    const [profilePic2, setProfilePic2] = useState(null);
    const [END_URL, setEndUrl] = useState(commonService.getImageUrl())
    const [Prefix, setPrefix] = useState('');
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailAddress, setEmailAddress] = useState("");
    const [Title, setTitle] = useState('');
    const [addressLine1, setAddressLine1] = useState("");
    const [addressLine2, setAddressLine2] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zipCode, setZipCode] = useState("");
    const [cellPhone, setCellPhone] = useState(null);
    const [Designation, setDesignation] = useState('');
    const [profilepicerror, setprofilepicerror] = useState('');
    const [updateimg,setupdateimg] = useState(true)

    const [result,setResult]=useState({})
    const [error, setError] = useState({
        firstName: '',
        lastName: '',
        emailAddress: '',
        title:'',
        designation: '',
        zipCode: '',
        addressLine1: '',
        addressLine2: '',
        city: '',
        state: '',
        cell: '',
        profilepic:""
    });
    const [count,setcount]=useState(0)
    const [imageChanged,setImageChanged]=useState(1)
    const [progress,setprogress]=useState()
    const [progress1,setprogress1]=useState(false)
    const [progress2,setprogress2]=useState(false)
    const [progress3,setprogress3]=useState(false)
    const [progress4,setprogress4]=useState(false)
    useEffect(() => {
        selectUser()
    }, []);
    const selectUser = () => {
        
        usersService.getCurrentprofile()
            .then((result) => {
                let temp
                let progressStates={
                    firstName: false,
                    lastName: false,
                    emailAddress: false,
                    profilpic:false,
                    title:false,
                    designation: false,
                    //AdditionalInformation:false,
                    addressLine1: false,
                    addressLine2: false,
                    city: false,
                    state: false,
                    zipCode: false,
                    cell: false,
                    //socialMedia:false,

                }
                if(result) {
                    setResult(result.Entity)
                    setProfilePic1(result.Entity?.ProfilePicture!== null?END_URL + result.Entity?.ProfilePicture:null)
                    setprofilePicfortopbar(result.Entity?.ProfilePicture!== null?END_URL + result.Entity?.ProfilePicture:null)
                    if (result.Entity?.ProfilePicture !== null){
                        progressStates.profilpic=true
                    }
                    setPrefix(result.Entity.Prefix)
                    setFirstName(result.Entity.FirstName)
                    if (result.Entity?.FirstName !== null){
                        progressStates.firstName=true
                    }
                    setLastName(result.Entity.LastName)
                    if (result.Entity?.LastName !== null){
                        progressStates.lastName=true
                    }
                    setEmailAddress(result.Entity.Email)
                    if (result.Entity?.Email !== null){
                        progressStates.emailAddress=true
                    }
                    setTitle(result.Entity.Title)
                    if (result.Entity?.Title !== null){
                        progressStates.title=true
                    }
                    setDesignation(result.Entity.Designation)
                    if (result.Entity?.Designation !== null){
                        progressStates.designation=true
                    }
                    setAddressLine1(result.Entity.Address1)
                    if (result.Entity?.Address1 !== null){
                        progressStates.addressLine1=true
                    }
                    setAddressLine2(result.Entity.Address2)
                    if (result.Entity?.Address2 !== null){
                        progressStates.addressLine2=true
                    }
                    setCity(result.Entity.City)
                    if (result.Entity?.City !== null){
                        progressStates.city=true
                    }
                    setState(result.Entity.State)
                    if (result.Entity?.State !== null){
                        progressStates.state=true
                    }

                    if(result.Entity?.PhoneInfo || result.PhoneInfo)
                    if(result.PhoneInfo){
                        temp=(result && result.PhoneInfo && result.PhoneInfo[0].Phone).match(/\d+/g)
                        setCellPhone(temp === null ? null :temp[0])
                    }
                    else{
                        temp=(result.Entity && result.Entity.PhoneInfo && result.Entity.PhoneInfo[0].Phone).match(/\d+/g)
                        setCellPhone(temp === null ? null :temp[0])
                    }
                  
                    if (temp!== null || temp[0] !== ""){
                        progressStates.cell=true
                    }
                    // temp=result.Entity.PhoneInfo[0].Phone
                    // setCellPhone( temp.substr(1, 10))
                    // setCellPhone(temp)
                    // temp=
                    // setZipCode(temp.substr(1, 6) )
                   
                    if (result.Entity?.ZipCode !== ""){
                        let temp=(result.Entity && result.Entity?.ZipCode).match(/\d+/g)
                        if(temp){
                        setZipCode(temp[0])
                        progressStates.zipCode=true}
                    }
              //Hareesh comment this 
                   
                    // if (result.Entity.ZipCode){
                    //     temp=(result.Entity && result.Entity.ZipCode).match(/\d+/g)
                    //     setZipCode(temp[0])
                    //     if (temp[0] !== ""){
                    //         progressStates.zipCode=true
                    //     }
                    // }
                    
                    // setprogress(progressStates)
                    // const filterObjects = (obj, value) => {
                    //            

                    let namessss = Object.values(progressStates);
                  
                    function test(namessss){

                        return namessss===true
                    }
                    let resulttt = namessss.filter(test);
                           
                    let countprogress=((((resulttt.length)*10)/12)*10)
                    setprogress(Math.trunc( countprogress ))
                    let progress1s=namessss.slice(0,4)
                    let progress2s=namessss.slice(4,6)
                    let progress3s=namessss.slice(6,11)
                    let progress4s=namessss.slice(12)
                    setprogress1((progress1s.every(element => element === true)))
                    setprogress2((progress2s.every(element => element === true)))
                    setprogress3((progress3s.every(element => element === true)))
                    setprogress4((progress4s.every(element => element === true)))
                    setcount(0)
                }
            })
            .catch(error => {

            });
    }

    const handleInputChange = (feild, e) => {
        let temp = result
        let value = e.target.value;
        let errors = error;
        switch (feild) {
            case 'Prefix':
                setPrefix(value)
                break;
            case 'firstName':
                if (value === "") {
                    errors.firstName = 'First Name is required.';
                } else if (value.length < 3) {
                    errors.firstName = 'First Name must be atleast 3 characters long.';
                } else {
                    errors.firstName = ''
                }
                setFirstName(value);
                setError(errors);
                break;
            case 'lastName':
                if (value === "") {
                    errors.lastName = 'Last Name is required.';
                } else {
                    errors.lastName = ''
                }
                setLastName(value);
                setError(errors);
                break;
            case 'title':
                if (value===""){
                    errors.title = 'Title is required.';
                }else {
                    errors.title=''
                }
                setTitle(value)
                setError(errors)
                break;
            case 'emailAddress':
                if (value === "") {
                    errors.emailAddress = 'Email Address is required.';
                    setEmailAddress(value);
                    setError(errors);
                } else if (EmailValidator.validate(value) === false) {
                    errors.emailAddress = 'Enter correct email address.';
                    setEmailAddress(value);
                    setError(errors);
                } else if (EmailValidator.validate(value) === true) {
                    setEmailAddress(value);
                    checEmail(value).then(function (data) {
                        if (data === true) {

                            errors.emailAddress = 'Email already exist with another user';
                        } else {
                            errors.emailAddress = '';
                        }

                        setEmailAddress(value);
                        setError(errors);

                        // setError(errors);
                    })

                }

                break;
            case 'zipCode':
                if(value.length < 5){
                    errors.zipCode = value.length < 5 ? 'ZipCode must be getter 5 digits long.' : '';
                }
                else if (value.length > 10){
                    errors.zipCode = value.length > 10 ? 'ZipCode must be least 10 digits long.' : '';
                }
                else if(value.length === 0 ){
                    errors.zipCode=""
                }
                else {
                    errors.zipCode=""
                }
                setZipCode(value);
                setError(errors);
                break;
                case 'addressLine1':

                    if (value === ""){
                        errors.addressLine1='Address is required.'
                    }
                    else if (value.length < 3) {
                        errors.addressLine1 = 'Address Line #1 must be least 3 characters long.';
                    } else {
                        errors.addressLine1 = ''
                    }
                    setAddressLine1(value);
                    setError(errors);
                           setError(errors)
                          
            // case 'addressLine1':
            //     if (value === "") {
            //         errors.firstName = 'First Name is required.';
            //     }
            //    else if (value.length < 3) {
            //         errors.addressLine1 = 'Address Line #1 must be least 3 characters long.';
            //     } else {
            //         errors.addressLine1 = ''
            //     }
            //     setAddressLine1(value);
            //     setError(errors);
                break;
            case 'addressLine2':
                if (value.length < 3) {
                    errors.addressLine2 = 'Address Line #2 must be least 3 characters long.';
                } else {
                    errors.addressLine2 = ''
                }
                setAddressLine2(value);
                setError(errors);
                break;
            case 'city':
                if (value.length < 2) {
                    errors.city = 'City must be atleast 2 characters.';
                } else {
                    errors.city = ''
                }
                setCity(value);
                setError(errors);
                break;
            case 'state':
                if (value.length < 2) {
                    errors.state = 'State must be atleast 2 characters.';
                } else {
                    errors.state = ''
                }
                setState(value);
                setError(errors);
                break;
            case 'designation':
                errors.designation = value === "" ? 'Designation is required.' : '';
                setDesignation(value);
                setError(errors);
                break;
            case 'cellPhone':
            //     if (value === ""){
            //         errors.cell='Mobile Number is required.'
            //     }
            //     else if(value && value.length < 10){
            //         errors.cell='Mobile Number Must at lest 10 digit.'
            //     }
            //     else if(value && value.length > 15){
            //         errors.cell='Mobile Number is less then 15 digit.'
            //     }
            //    else{
            //         errors.cell=""
            //     }
            if (value === ""){
                setCellPhone(value);
               errors.cell='Mobile Number is required.'
            }
            else if(value.length < 10){
                setCellPhone(value);
                errors.cell = 'Mobile Number must be at least 10 number long.';
            }else if(value.length > 15){
                setCellPhone(value);
                errors.cell = 'Mobile Number must be less then 15 number long.';
            }
            else {
                setCellPhone(value);
                setError(errors);
                errors.cell = ''
                if(temp.PhoneInfo[0]!== undefined){
                 temp.PhoneInfo[0].Phone = value
                setResult(temp)
                }
                
               
            }
             break;
                // setCellPhone(value)
                //        setError(errors)
                      
            //    if (value!== "e"){
            //        // value.slice(-1).which.preventDefault();
            //        if (value.length === 1 || value !== "" ){
            //            setCellPhone(value)
            //            setError(errors)
            //        }

            //    }


            default:
                break;
        }

    }

    const editUser = () => {
        
        let object = result
        object.AccountId=18093257
        object.Prefix = Prefix
        object.AddressInfo[0].Address1 = addressLine1
        object.AddressInfo[0].Address2 = addressLine2
        object.FirstName = firstName
        object.LastName = lastName
        object.Title = Title
        object.Designation = Designation
        object.AddressInfo[0].State = state
        object.AddressInfo[0].City = city
        if(zipCode !== null && zipCode !== "" ){
        let temp1=(zipCode).match(/\d+/g)
        if(temp1){
        object.ZipCode = temp1[0]}
        object.AddressInfo[0].ZipCode =temp1[0]   }
        else{
            object.ZipCode = null
            object.AddressInfo[0].ZipCode =null
        }
        object.PhoneInfo[0].Phone = cellPhone

        // let input = {
        //     Address1: addressLine1,
        //     Address2: addressLine2,
        //     City: city,
        //     title: Title,
        //     Designation: Designation,
        //     Email: emailAddress,
        //     FirstName: firstName,
        //     LastName: lastName,
        //     Phone: cellPhone,
        //     ProfilePicture: profilePic1,
        //     State: state,
        //     ZipCode: zipCode,

        // }
        const validateForm = () => {
            let temp = result
            let count = 0;
            let tmp_errors = {...error};
            if (firstName === "") {
                tmp_errors.firstName = 'First Name is required.';
                count = count + 1;
            } else if (firstName.length < 3) {
                tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
                count = count + 1;
            }
            if (lastName === "") {
                tmp_errors.lastName = 'Last Name is required.';
                count = count + 1;
            }
            if (emailAddress === "") {
                tmp_errors.emailAddress = 'Email Address is required.';
                count = count + 1;
            } else if (EmailValidator.validate(emailAddress) === false) {
                tmp_errors.emailAddress = 'Enter correct email address.';
                count = count + 1;
            }
            if (Designation === "") {
                tmp_errors.designation = 'Designation is required.';
                count = count + 1;
            }
            if (Title === "") {
                tmp_errors.title = 'Title is required.';
                count = count + 1;
            }
            // if (cellPhone === "") {
            //     tmp_errors.cell = 'mobile number is required.';
            //     count = count + 1;
            // }
            if (addressLine1 === "") {
                tmp_errors.addressLine1 = ' Address is required.';
                count = count + 1;
            }
            if(temp?.PhoneInfo !== undefined){
            if (temp?.PhoneInfo[0].Phone === ""||temp?.PhoneInfo[0].Phone== null) {
                tmp_errors.cell = 'Mobile number is required';
                count = count + 1;
            } else if(parseInt(temp?.PhoneInfo[0].Phone).length > 15){
                tmp_errors.cell = 'Mobile number must be least 15 characters long.';
                count = count + 1;
            }
            }
            else{
                if (cellPhone === ""||cellPhone== null ) {
                    tmp_errors.cell = 'Mobile number is required';
                    count = count + 1;
                }else if(parseInt(cellPhone).length < 10){
                    tmp_errors.cell = 'Mobile Number must be at least 10 number long.';
                    count = count + 1;
                }
                 else if(parseInt(cellPhone).length > 15){
                    tmp_errors.cell = 'Mobile Number must be less then 15 number long.';
                    count = count + 1;
                }
            }
                   
            // if (cellPhone && cellPhone.length > 15) {
            //     tmp_errors.cell = 'cell number must be less than equal to 15 digit.';
            //     count = count + 1;
            // }
            // if (zipCode === "") {
            //     tmp_errors.zipCode = 'zipCode  can\'t be empty.';
            //     count = count + 1;
            // }
            if (zipCode && zipCode.length > 10) {
                tmp_errors.zipCode = 'zipCode code must be lesser-than equal to 10 digit.';
                count = count + 1;
            }
                if (city && city.length < 2) {
                    tmp_errors.city = 'City must be least 2 characters.';
                    count = count + 1;
                }

                if (count <= 0) {
                    tmp_errors = {
                        firstName: '',
                        lastName: '',
                        emailAddress: '',
                        designation: '',
                        zipCode: '',
                        addressLine1: '',
                        addressLine2: '',
                        city: '',
                        state: '',
                        Title: '',
                        cell: ''

                    }
                }
                setError(tmp_errors);
                return count <= 0;
            }

            if (validateForm()) {
                let formData = new FormData();
                formData.append("uploads[]", profilePic2);
                let token = localStorage.getItem('LoginToken');
                let config;
                if (token !== null && token !== undefined && token !== "") {
                    config = {
                        headers: {
                            'Accept': 'application/json',
                            'userauthtoken': token
                        }
                    };
                } else {
                    config = {
                        headers: {
                            'Accept': 'application/json'
                        }
                    };
                }
                if(profilePic2 == null){
                    usersService.updateCurrentprofile(object).then(function (response){
                        if (response.HttpStatusCode === 200) {
                            // setOpenSuccessModal(true);
                            setprofilepicerror("")
                            selectUser();
                            
                            // resetForm();
                        }
                    })
                    .catch(error => {

                    }); 
                }
                else{
                    axios.post(`${API_END_POINT}users/upload`, formData, config)
                    .then(function (response) {
                         //   console.log(response)
                         object.ProfilePicture = response.data.Data.FileName
                         //  object.ProfilePicture = response.data.Data.OriginalFileName
                    usersService.updateCurrentprofile(object).then(function (response){
                                if (response.HttpStatusCode === 200) {
                                    // setOpenSuccessModal(true);
                                    setprofilepicerror("")
                                    selectUser();
                                    
                                    // resetForm();
                                }
                            })
                            .catch(error => {

                            });

                    })
                    .catch(function (err) {
                        // cogoToast.error(err, {position: 'top-center'});
                    });
                }
            }
        }
   

    const handleProfilepic=(e)=> {
        // let tmp_profile_data = {...profileDetail};
               
        let temp=result
        let reader = new FileReader();
        let file = e.target.files[0];

        if (file.size > 1054464){
            setprofilepicerror("File size more, please upload a file with a size less than or equal to 1 MB")

            setcount(0)
            // setError(errors)
            return ""
        }

        setprofilepicerror("")
        setcount(0)
        reader.onloadend = () =>{
                   
            setProfilePic2(file);
            setProfilePic1(reader.result);
            temp.ProfilePicture=reader.result
            setResult(temp)
           //console.log("temp.ProfilePicture",temp.ProfilePicture)

            //setImageChanged(1)
        }
        //  if(file.type  !== "image/jpeg" || file.type  !== "image/png" || file.type  !== "image/jpg" ){
        //     setprofilepicerror("Only upload the following file types .jpg,.png,.jpeg")
        //     setcount(0)
        //     // setError(errors)
        //     return ""
        // }
        
        // setprofilepicerror("")
        // setcount(0)
        // reader.onloadend = () =>{
                   
        //     setProfilePic2(file);
        //     setProfilePic1(reader.result);
        //     temp.ProfilePicture=reader.result
        //     setResult(temp)
        //    //console.log("temp.ProfilePicture",temp.ProfilePicture)

        //     //setImageChanged(1)
        // }

    
        
        
        // tmp_profile_data['profile'] = {
        //     ...tmp_profile_data['profile'],
        //     profilePic: reader.result
        // }

        // setProfileDetail(tmp_profile_data);

        reader.readAsDataURL(file);

    }

    function stringAvatar() {
        let name= firstName && lastName ? `${firstName} ${lastName}` : 'N A'
        // let name = firstName ?fullname : 'N A'
        return {
            sx: {
                bgcolor: "#2F7FED",
                width:'47px',
                height:'47px'
            },
            children: (name.split(' ') && name.split(' ')[0] && name.split(' ')[1]) ? `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`: 'U',
        };
    }

    const deleteProfilePic=()=>{
               
        let temp =result
        temp.ProfilePicture=null
        setResult(temp)
        setcount(count+1)
    }
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar profilePic12={profilePicfortopbar} />

                <div className="main-content main-from-content personalinformations">
                    
				<div className="main-content main-from-content signatures" style={{padding:"0 30px 30px 55px"}}>
                <div className="row">
						<div className="col-xl-7 col-lg-7 col-md-7 col-sm-12">
                                <h3 style={{fontSize:35,fontWeight:300}}>Profile Information</h3>
                                <p>To configure what information is shared when you sign.</p>
                            
						</div>
						<div className="col-xl-5 col-lg-5 col-md-5 col-sm-12">
							<div className="recipient-btn">
                            <button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}}/></button>
								
							</div>
						</div>
					</div>
                    <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12" style={{visibility:"hidden"}}>
                                <h3 style={{fontSize:35,fontWeight:300}}>Profile Information</h3>
                                <p>To configure what information is shared when you sign.</p>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
                                <div class="progress" >
                                    <div class="progress-bar bar-width-md" style={{width: progress+'%'}} role="progressbar" aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100">
                                        {/* {progress > 1 && progress < 25?<span class="value__bar_sm">{progress}</span>
                                         :progress > 25 && progress <50?<span class="value__bar_md">{progress}% Complete</span>
                                          :progress > 50 && progress <75?<span class="value__bar_lg">{progress}% Complete</span>
                                          :progress > 75 && progress <100?<span class="value__bar_xl">{progress}% Complete</span>
                                         :""
                                        } */}
                                        {/*<span class="value__bar_sm">25% Complete</span>*/}
                                        <span class="value__bar_md">{progress}% Complete</span>
                                        {/*<span class="value__bar_lg">75% Complete</span>*/}
                                        {/*<span class="value__bar_xl">100% Complete</span>*/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='row align-items-center'>
                        <div className='profile-info-content'>
                            <div className='row'>
                            <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12 r-space'>
                                    <div className='profile-information'>


                                        <div className='user-profile-img'>

                                        {/*    <img src='src/images/profile.jpg' />*/}
                                        {/*    <div class="crez-btn">*/}
                                        {/*        <input type="file" id="real-file" hidden="hidden" />*/}
                                        {/*        <button type="button" id="custom-button"><img src="src/images/camera.svg" alt="" /></button>*/}
                                        {/*    </div>*/}
                                        {/*</div>*/}
                                        {/*<span class="user-name">Edit Profile Picture</span>*/}
                                        {/*<div className={'parent-profile-pic '}>*/}

                                        {/*      */}


                                        {/*    /!*<div className={'mt-3 mb-2'} onClick={()=>deleteProfilePic()}> X </div>*!/*/}
                                        {/*    <input className={"code uploadprofileimage"} id="file-input" type="file"*/}
                                        {/*           onChange={(e) => handleProfilepic(e)}/>*/}
                                        {/*    <div className={'w-60 img_profile'}>*/}
                                        {/*        <label htmlFor="file-input " className={"parentcameracamer"}>*/}
                                        {/*            /!*<img src={camera} className={"cameracamera"}/>*!/*/}
                                        {/*            Change Photo*/}

                                        {/*        </label>*/}

                                        {/*    </div>*/}
                                        {/*    <P>Edit Profile Picture</P>*/}
                                        {/*</div>*/}


                                     
                                        <div className={'parent-profile-pic text-center parent-profile-piccc '}>
                                        {  (profilePic1 !== null || result.ProfilePicture !== null)? <img src={profilePic1} alt="" className="user-picture" /> : <Avatar {...stringAvatar()} />}
                                                {/*<div className={'mt-3 mb-2'} onClick={()=>deleteProfilePic()}> X </div>*/}
                                                <input className={"code uploadprofileimage"} id="file-input" type="file" accept="image/png,image/jpg,image/jpeg"
                                                       onChange={(e) => handleProfilepic(e)}/>
                                                <div className={'w-60 img_profile'}>
                                                    <label style={{cursor:"pointer"}} htmlFor="file-input" className={"parentcameracamer-1"}>
                                                        <img src={camera} className={"cameracamera"}/>
                                                        {/*Change Photo*/}
                                                    </label>
                                                    <lable className={"mt-2"} >Edit Profile Picture</lable>
                                                    {
                                                        profilepicerror !== "" &&
                                                        <p className={"error-msg"} style={{fontWeight:400,fontSize:"1rem",color:"red"}}>{profilepicerror}</p>
                                                    }
                                                </div>

                                            </div>

                                           
                                    <div className='tab-list-content'>
                                        <ul className='tab-list'>
                                            <li><div className={progress1?"link-tab active done":"link-tab"}><div style={{cursor:"context-menu"}} class="li-count">1</div> <div class="text-btn">Personal Information</div> </div></li>
                                            <li><div className={progress2?"link-tab active done":"link-tab"}><div style={{cursor:"context-menu"}} class="li-count">2</div> <div class="text-btn">Company Information</div>  </div></li>
                                            <li><div className={progress3?"link-tab active done":"link-tab"}><div style={{cursor:"context-menu"}} class="li-count">3</div> <div class="text-btn">Location Information</div> </div></li>
                                            <li><div className={progress4?"link-tab active done":"link-tab"}><div style={{cursor:"context-menu"}} class="li-count">4</div> <div class="text-btn">Contact Information</div>  </div></li>
                                        </ul>
                                    </div>
                                    </div>

                                 </div>
                                </div>
                                <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                    <div className='tab-content'>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Personal Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={firstName} onChange={(e)=>handleInputChange('firstName',e)} />
                                                            <label for="">First Name<span>*</span></label>
                                                            <p className={"error-msg"}>{error.firstName}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" value={lastName}  onChange={(e)=>handleInputChange('lastName',e)} />
                                                            <label for="">Last Name<span>*</span></label>
                                                            <p className={"error-msg"}>{error.lastName}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" value={emailAddress} readOnly={true} onChange={(e)=>handleInputChange('emailAddress',e)}  />
                                                            <label for="">Email<span>*</span></label>
                                                            <p className={"error-msg"}>{error.emailAddress}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='company-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Company Information</h3>

                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" value={Title} onChange={(e)=>handleInputChange('title',e)} />
                                                            <label for="">Job Title<span>*</span></label>

                                                            <p className={"error-msg"}>{error.title}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={Designation} onChange={(e)=>handleInputChange('designation',e)} />
                                                            <label for="">Designation<span>*</span></label>
                                                            <p className={"error-msg"}>{error.designation}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea />
                                                            <label for="">Additional Information</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Location Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea value={addressLine1} onChange={(e)=>handleInputChange('addressLine1',e)}/>
                                                            <label for="">Address #1<span>*</span></label>

                                                            <p className={"error-msg"}>{error.addressLine1}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea   value={addressLine2} onChange={(e)=>handleInputChange('addressLine2',e)} />
                                                            <label for="">Address #2</label>
                                                            <p className={"error-msg"}>{error.addressLine2}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"   value={city} onChange={(e)=>handleInputChange('city',e)} />
                                                            <label for="">City </label>
                                                            <p className={"error-msg"}>{error.city}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={state} onChange={(e)=>handleInputChange('state',e)} />
                                                            <label for="">State</label>
                                                            <p className={"error-msg"}>{error.state}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={zipCode} onChange={(e)=>handleInputChange('zipCode',e)} />
                                                            <label for="">Zip Code </label>
                                                            <p className={"error-msg"}>{error.zipCode}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='contact-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>Contact Information</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="number" autocomplete="off" value={cellPhone} onChange={(e)=>handleInputChange('cellPhone',e)} />
                                                            <label for="">Mobile<span>*</span></label>
                                                        </div>
                                                        <p className={"error-msg"}>{error.cell}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <select style={{cursor:"pointer"}}>
                                                                <option style={{backgroundImage: "url(src/images/instagram_icon.svg)" }}>Instagram</option>
                                                            </select>
                                                            <label  style={{cursor:"pointer"}} for="">Select Social Media</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-8 col-lg-8 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off" />
                                                            <label for="">Add Link</label>
                                                            <button className="btn add_link_btn">ADD</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="social-tags">
                                                        <a href="#" className="link"><img src="src/images/twitter.svg" alt="" /> Twitter <span className="hide">x</span></a>
                                                        <a href="#" className="link"><img src="src/images/linkedin.svg" alt="" /> Linkedin <span className="hide">x</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div class="subf_pagn_btn form-action mt-3">
                                                         <button style={{color:"white",width:110,backgroundColor:"#2F7FED",border:"1px solid #2F7FED",display:"flex",justifyContent:"center"}} class="btn" onClick={()=>editUser()}>SAVE</button>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

export { PersonalInformations };