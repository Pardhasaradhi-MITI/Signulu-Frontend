import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import ReactTooltip from 'react-tooltip';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { addDocStep2Options, colorTypes, GenerateOTP } from '../_services/model.service';
import { addDocumentService } from '../_services/adddocument.service';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useEffect, useState, useRef } from "react";
import { commonService } from '../_services/common.service';
import { Document, Page, pdfjs } from "react-pdf";
import config from 'config';
import * as EmailValidator from "email-validator";
import { alertService } from '@/_services';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
import * as Yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import '../css/documentselect.css';
import "../css/adddocumentstep1.css";
import { Button, Modal } from 'react-bootstrap';
import { DataTableComponent } from './Common/DataTable';
import { useSelector, useDispatch } from 'react-redux';
import { columnHeaderService } from '../_services/columHeader.service'
import { debounce } from "lodash";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { ContactsPopTable } from './Common/ContactsTable';
import Spinner from 'react-bootstrap/Spinner';
import {usersService} from "../_services/users.service"
import { Calendar } from 'primereact/calendar'
import moment from 'moment';
import { lineHeight } from '@mui/system';
let signatoryRespIndex = '';
let signatoryRecipientData={}
let submitData= {}
let noFacialImageResp=[]
let facialErrorExist = false
let photoErrorExist = false
let isNext = true
let facialWithNoProfilePic = false
let onlyNonFacialImageResp = []
let duplicateEmailRespients = []
let pendingColors = [...colorTypes]
function DocumentSelect() {
    //Vertical Tabs
    const usehistory = useHistory();
    const [count, setCount] = useState(1);
    const [step2Data, setStep2Data] = useState({});
    const [subNumPages, setSubNumPages] = useState();
    const [documentFile, setdocument] = useState('');
    const [selectedPageNumber, setselectedPageNumber] = useState(1);
    const [workflowTypes, setWorkflowTypes] = useState([]);
    const [selectedWorkFlowType, setSelectedWorkFlowType] = useState('');
    const [countryCodesArray, setCountryCodesArray] = useState([])
    const [modalShow, setModalShow] = useState(false)
    const [contactsModalShow, setContactsModalShow] = useState(false)
    const [deleteRecipient, setDeleteRecipient] = useState({})
    const [subDocumentFile, setSubDocumentFile] = useState('');
    const [validationExist, setValidationExist] = useState(false);
    const [checkBoxPages, setCheckBoxPages] = useState([]);
    const [activeRecipients, setActiveRecipients] = useState([]);
    const options = addDocStep2Options
    const [userList, setUsersList] = useState([]);
    let { id } = useParams();
    const dispatch = useDispatch()
    const Selector = useSelector(state => { return state })
    const [CCstatus, SetCCstatus] = useState(false)
    const [PopupDSCstatus, SetPopupDSCstatus] = useState(false)
    const wrapperRef1 = useRef(null);
    const [selectedContacts,setSelectedContacts]=useState([])
    const [contactsNotSelected,setContactsNotSelected]=useState(false)
    const [contactsEmailExist,setContactsEmailExist]=useState(false)
    const [selectedRecipients,setSelectedRecipients]=useState()
    const [mainselectedContacts,setmainselectedContacts]=useState([])
    const [whatsup,setwhatsup]=useState(false)
    const [userData,setUserData]=useState({})
    const [Enabledsc,setEnabledsc]=useState(false)
    const [EnableAadharekyc,setEnableAadharekyc]=useState(false)
    const [showSignatoryControlPopUp,setShowSignatoryControlPopUp]=useState(false)
    const [showFacialPopUP,setShowFacialPopUP]=useState(false)
    const [isOwnerSequential,setIsOwnerSequential]=useState(false)
    const [isOwnerPositionLastPopup,setIsOwnerPositionLastPopup]=useState(false)
    const [RecipientPersonHaveDSC,setRecipientPersonHaveDSC]=useState(false)
    const [DSCUsersValidPopup,setDSCUsersValidPopup]=useState(false)
    const [lastpositionofAadharEsign,setlastpositionofAadharEsign] = useState()
    const [IsAnyoneEnableAadharEsign,setIsAnyoneEnableAadharEsign] = useState(false)
    const [showmorethanfiverecipientsnotallowed,setshowmorethanfiverecipientsnotallowed] = useState(false)
    const [showdsccontrollesremovemessage,setshowdsccontrollesremovemessage]=useState(false)
    const [enabledeEkyc,setEnabledeEkyc]=useState(false)
    // form validation rules
    const validationSchema = Yup.object().shape({
        FirstName: Yup.string().required('First Name is required'),
        LastName: Yup.string().required('Last Name is required'),
        EmailAddress: Yup.string().required('Email Address is required').email('Email Address is invalid'),
        Designation: Yup.string(),
        MobileNumber: (!whatsup&&!step2Data?.IsSecured && step2Data?.IsSecured == 0 || CCstatus) ? Yup.string() : Yup.string()
            .required('Phone Number is required').matches(/^(\+\d{1,3}[- ]?)?\d{10,15}$/, { message: 'Invalid Phone Number' }),
        CountryId: (!whatsup&&!step2Data?.IsSecured && step2Data?.IsSecured == 0 || CCstatus) ? Yup.string() : Yup.string().required('Country Code is required'),
        Position: (step2Data?.IsParallelSigning || step2Data?.IsParallelSigning == 1) ? Yup.string() : Yup.string().required('Position is required'),
        Signatory: Yup.string(),
        DSC:Yup.boolean(),
        Aadhar:Yup.boolean(),
        IsCCEnabled: Yup.string(),
        FacialRecognition: Yup.string(),
        PhotoVerification: Yup.string(),
    });
    
    // get functions to build form with useForm() hook
    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState, setValue, getValues, clearErrors, setError } = useForm(formOptions);
    const { errors } = formState;
    useEffect(() => {
        pendingColors = [...colorTypes]
        setdocument(commonService.documentValue)
        setSubDocumentFile(commonService.documentValue)
        getWorkflowData()
        // getUserPreferences()
        getCurrentUserInfo()
        getDocumentInfoData()
        getCurrentUserData()
    }, [])
    const getCurrentUserData = ()=>{
        usersService.getUserDataCurrent()
        .then((result) => {
            if(result){
                setwhatsup(result.Entity.IsWhatsappEnabled===1?true:false)
                
                 //setuserdata(result.Data)
            }
        })
    }
    function getCurrentUserInfo() {
        if (commonService.userInfoObj) {
            setUserData(commonService.userInfoObj)
            if(commonService.userInfoObj.IsSeqPositionEnabled) {
                setIsOwnerSequential(true)
            }

        } else {
            commonService.userInfo().then((userData) => {
                if (userData && userData.Data) {
                    setUserData(userData.Data)
                    if(userData.Data.IsSeqPositionEnabled) {
                        setIsOwnerSequential(true)
                    }

                }
            });
        }

    }
    function getUserPreferences(){
        usersService.getSettingUser()
            .then((result) => {
                console.log(result, 'resultresult')
                // setResultsUser(result.Entity)
                console.log('user',result)
            })
            .catch(error => {

            });
    }
    // let deleteRepData = {}
    function getDocumentInfoData() {
        addDocumentService.getDocumentInfo(id , true).then(data => {
            if (data.Entity && data.Entity.Data) {
                data.Entity.Data.IsParallelSigning = data.Entity.Data.IsParallelSigning ? data.Entity.Data.IsParallelSigning : 0;
                data.Entity.Data.IsSecured = data.Entity.Data.IsSecured ? data.Entity.Data.IsSecured : 0;
                data.Entity.Data.recipientType = data.Entity.Data.recipientType ? data.Entity.Data.recipientType : '';
                if (data.Entity.Data.CCRecipients) {
                    data.Entity.Data.Recipients = [...data.Entity.Data.Recipients, ...data.Entity.Data.CCRecipients]
                }
                if (!data.Entity.Data.IsParallelSigning || data.Entity.Data.IsParallelSigning == 0 ) {
                    data.Entity.Data.Recipients.sort((firstData, secondData) => firstData.SortPosition - secondData.SortPosition)
                }
                data.Entity.Data.Recipients.forEach((element, index) => {
                    if (data.Entity.Data.IsParallelSigning) {
                        element.Position = index + 1
                    }
                    // element.Position = index + 1;
                    element.recipientType = '';
                    if (element.IsInPerson) {
                        element.recipientType = "inPerson"
                    } else if (element.IsNotarizer) {
                        element.recipientType = "isNotarizer"
                    }
                    if(element.IsDSC) {
                        setRecipientPersonHaveDSC(true)
                    }
                    if(pendingColors && pendingColors.length == 0){
                        pendingColors = [...colorTypes]
                    }
                    if(!element.Color) {
                        element.Color = pendingColors[0]
                        pendingColors.splice(0,1)
                    }
                    if(pendingColors.indexOf(element.Color) !== -1) {
                        pendingColors.splice(pendingColors.indexOf(element.Color), 1)
                    }
                })
                setStep2Data(data.Entity.Data)
                setEnabledsc(data.Entity.Data.IsDSCEnabled)
                setEnabledeEkyc(data?.Entity?.Data?.Features?.includes('EKYC'))
                setEnableAadharekyc(data.Entity.Data?.Features?.includes('AADHAAR'))
                setActiveRecipients(data.Entity.Data.Recipients)
                setCountryCodesArray(data.Entity.Countries.Entity)
                getParallelQuery(data.Entity.Data.IsParallelSigning)
                getSecureQuery(data.Entity.Data.IsSecured)
                setSelectedWorkFlowType(data.Entity.Data.WorkflowCode)
                setdocument(config.serverUrl + 'adocuments/download/' + data.Entity.Data.FileName)
                setSubDocumentFile(config.serverUrl + 'adocuments/download/' + data.Entity.Data.FileName)
            } else {

            }
        })
    }
    function ccgetvalue(event, index, data) {
        step2Data.Recipients[index].IsCCEnabled = event.target.checked
        let name ="cc"
        if (event.target.checked) {
            step2Data.Recipients[index].IsSignatory = false
            step2Data.Recipients[index].IsInPerson = false
            step2Data.Recipients[index].IsNotarizer = false
            step2Data.Recipients[index].recipientType ='';
            step2Data.Recipients[index].IsFacialRecognition = false
            step2Data.Recipients[index].IsPhotoRecognition = false
            step2Data.Recipients[index].IsLocationTracking = false
            step2Data.Recipients[index].IsAadhaarKYC = false
            selectDSC({target:{checked:false}}, index,name)
            if(step2Data.Recipients[index].IsAadhaarEsign == true){
            selectAadhar({target:{checked:false}}, index ,name)}

        }
        setStep2Data({ ...step2Data })
        const result = step2Data.Recipients.filter(resp => !resp.IsDelete);
        setActiveRecipients(result)
        if (result && result.length == 2) {
            const isCCResp = result.filter(resp => resp.IsCCEnabled);
            if (isCCResp && isCCResp.length > 0) {
                getSecured(0, false)
                setActiveRecipients(result)
            }
        }
        if(event.target.checked){
            checkBoxPages.forEach((e, i)=>{
                checkBoxPages[i].checked = true;
                checkBoxPages[i].disabled = true;
                let pageStatus=data.RecipientPageNumbers?.includes(e.pageNumber)
                if(!pageStatus){
                    data.RecipientPageNumbers.push(e.pageNumber)
                }
              }
            )
            data.RecipientPageNumbers.sort((a, b) => a - b)
        }
            else{
                checkBoxPages.forEach((e, i)=>{
                    checkBoxPages[i].checked = true;
                    checkBoxPages[i].disabled = false;
                    let pageStatus=data.RecipientPageNumbers?.includes(e.pageNumber)
                    if(!pageStatus){
                        data.RecipientPageNumbers.push(e.pageNumber)
                    }
                  }
                )
            }
            setSelectedRecipients({...data})
        //    const index2=step2Data.Recipients.findIndex(e=>e.RecipientId===data.RecipientId)
        //    step2Data.Recipients=step2Data.Recipients.map((e,i)=>{if(i==index2){return {...data}}else {return {...e}}})
        step2Data.Recipients[index] = data
          setStep2Data({...step2Data})
        setCheckBoxPages([...checkBoxPages])
    }
    function selectSignatory(event, index) {
        step2Data.Recipients[index].IsSignatory = event.target.checked
        setStep2Data({ ...step2Data })

    }
    function ReArrangofthetoggleforAadhar(){
        let lastposition ;
        const reversedstep2DataRecipients = [...step2Data.Recipients];
        for(let x = 0; x<reversedstep2DataRecipients.length;x++ ){
            if(reversedstep2DataRecipients[x].IsAadhaarEsign === true){
                setlastpositionofAadharEsign(reversedstep2DataRecipients[x].Position)
                lastposition = reversedstep2DataRecipients[x].Position
                break;
            }
        } 
        for(let a = 0; a<step2Data.Recipients.length;a++ ){
            if(lastposition){
                if(step2Data.Recipients[a].Position>lastposition)
                  {
                    //step2Data.Recipients[a].IsSignatory = false
                    step2Data.Recipients[a].IsFacialRecognition = false
                    step2Data.Recipients[a].IsPhotoRecognition = false
                    step2Data.Recipients[a].IsInPerson = false
                    step2Data.Recipients[a].IsNotarizer = false
                    step2Data.Recipients[a].recipientType ='';
                    if(step2Data.Recipients[a].IsDSC == true){
                    selectDSC({target:{checked:false}}, a)}
                  }
            }
        }
    }
    function selectAadhar(event, index,name) {
        let lastposition ;
        step2Data.Recipients[index].IsAadhaarEsign = event.target.checked
        // step2Data.Recipients[index].IsSignatory = event.target.checked
        if(event.target.checked === true){
            step2Data.Recipients[index].IsCCEnabled = false
            step2Data.Recipients[index].IsLocationTracking = false
            step2Data.Recipients[index].IsSignatory=true
        const reversedstep2DataRecipients = [...step2Data.Recipients];
        for(let x = 0; x<reversedstep2DataRecipients.length;x++ ){
            if(reversedstep2DataRecipients[x].IsAadhaarEsign === true){
                setlastpositionofAadharEsign(reversedstep2DataRecipients[x].Position)
                lastposition = reversedstep2DataRecipients[x].Position
                break;
            }
        } }
        else{
            setlastpositionofAadharEsign() 
            //setIsAnyoneEnableAadharEsign(false)
        }
        if(event.target.checked === true){
            // if(name !== 'fromdsc'){
            // selectDSC({target:{checked:false}}, index,name='fromadhaar')}
            setValue('IsCCEnabled', false)
        for(let a = 0; a<step2Data.Recipients.length;a++ ){
            if(lastposition){
                if(step2Data.Recipients[a].Position>lastposition)
                  {
                    //step2Data.Recipients[a].IsSignatory = false
                    step2Data.Recipients[a].IsFacialRecognition = false
                    step2Data.Recipients[a].IsPhotoRecognition = false
                    step2Data.Recipients[a].IsInPerson = false
                    step2Data.Recipients[a].IsNotarizer = false
                    step2Data.Recipients[a].recipientType ='';
                    
                    //selectDSC({target:{checked:false}}, a)
                  }
            }
        } }
        let Aadhaarenabled=''
        Aadhaarenabled = step2Data.Recipients.find(item => item.IsAadhaarEsign == 1 || item.IsAadhaarEsign == true && !item.IsDelete)
        if(Aadhaarenabled){
            setIsAnyoneEnableAadharEsign(true)
            getParallelAadharEsign(0, false)
            let FinalData1 = {}
            FinalData1 = [...step2Data.Recipients] 
            let nondeletedrecipients = []
            FinalData1.map(item=>{
               if(item.IsDelete !== 1 || item.IsDelete !== true){
                nondeletedrecipients.push(item)
               }
            })
            if(nondeletedrecipients.length>5){
                if(name !== "cc")
                setshowmorethanfiverecipientsnotallowed(true)}
         }
         else{
            const result = step2Data.Recipients.filter(resp => !resp.IsDelete)
            setIsAnyoneEnableAadharEsign(false)
            if(result.length < 3) {
                step2Data ? (step2Data.IsParallelSigning = 1) : false
                setStep2Data({ ...step2Data, ['IsParallelSigning']: 1 });
                getParallelQuery(1)}
           // getParallel(1,true)
            
         }

        setStep2Data({ ...step2Data })

    }
    function selectDSC(event, index,namea) {
        let name ='fromdsc'
       let documentData = {...step2Data}
       const DSCUSERS = documentData.Recipients.filter(data=>data.IsDSC)
        if(event.target.checked) {
            if(DSCUSERS && DSCUSERS.length==5) {
               setDSCUsersValidPopup(true)
               documentData.Recipients[index].IsDSC = false
               setStep2Data({ ...documentData })
            } else {
            setRecipientPersonHaveDSC(true) 
            documentData.Recipients[index].IsDSC = event.target.checked
            documentData.Recipients[index].IsCCEnabled = false
            documentData.Recipients[index].IsLocationTracking = false  
        for (let l = 0; l < documentData.Recipients.length; l++) {
            // if(index != l) {
                if( documentData.Recipients[l].IsOwner== true && (documentData.Recipients[l].IsDSC ==true || documentData.Recipients[l].IsDSC ==1)){
                    documentData.Recipients[l].IsSignatory = true
                   }
                   else{
                    documentData.Recipients[l].IsSignatory = false
                   }
                documentData.Recipients[l].IsInPerson = false;
                documentData.Recipients[l].recipientType ='';
                documentData.Recipients[l].IsNotarizer = false;
                documentData.Recipients[l].IsFacialRecognition = false;
                documentData.Recipients[l].IsPhotoRecognition = false;
                namea !== 'fromadhaar'?documentData.Recipients[l].IsAadhaarKYC = false:"";
            // }
            if(documentData.Recipients[l].IsDSC && !documentData.Recipients[l].IsCCEnabled) {
                documentData.Recipients[l].IsSignatory = true
            }
        }
     
        if(!documentData.Recipients[index].IsCCEnabled) {
        documentData.Recipients[index].IsSignatory = true
        }
        setStep2Data({ ...documentData })
        if(step2Data.IsSecured) {
            getSecured(0)
            alertService.warn("DSC and Secured cannot be enabled at the same time, if you select DSC, secured toggle will be disabled.")
        }
    }
    } else {
        console.log(DSCUSERS, DSCUSERS)
        if(DSCUSERS && DSCUSERS.length==1) {
            setRecipientPersonHaveDSC(false) 
         }
         if(documentData?.Recipients[index].Controls?.length>0){
            signatoryRecipientData = documentData.Recipients[index]
             setshowdsccontrollesremovemessage(true)
         }
         else{
        documentData.Recipients[index].IsDSC = event.target.checked
        namea !== 'fromadhaar' && namea !== 'cc'&&event.target.checked ?documentData.Recipients[index].IsSignatory = true:documentData.Recipients[index].IsSignatory = false
        if(documentData.Recipients[index].IsAadhaarEsign == true && namea !== 'fromadhaar'  && namea !== 'cc' ){
        selectAadhar({target:{checked:false}}, index ,name)}
        setStep2Data({ ...documentData })
         }
    }
    }
    function conformtooffdsctoggle(){
        setshowdsccontrollesremovemessage(false)
        let documentData = {...step2Data}
        const index1 = documentData.Recipients.findIndex(object => {
            return object.RecipientId === signatoryRecipientData?.RecipientId
          });
        documentData.Recipients[index1].IsDSC = false
        documentData.Recipients[index1].IsSignatory = false
        setStep2Data({ ...documentData })
    }

    function selecteKYC(event, index) {
        step2Data.Recipients[index].IsAadhaarKYC = event.target.checked
        if(event.target.checked) {
        step2Data.Recipients[index].IsSignatory = event.target.checked
        step2Data.Recipients[index].IsInPerson = false;
        step2Data.Recipients[index].recipientType ='';
        step2Data.Recipients[index].IsNotarizer = false;
        step2Data.Recipients[index].IsFacialRecognition = false;
        step2Data.Recipients[index].IsPhotoRecognition = false;
        step2Data.Recipients[index].IsLocationTracking = false
        }
        setStep2Data({ ...step2Data })
}
function selectLocationTracking(event, index) {
    if(event.target.checked) {
        // step2Data.Recipients[index].IsSignatory = event.target.checked
    }
    step2Data.Recipients[index].IsLocationTracking = event.target.checked
    setStep2Data({ ...step2Data })

}

    function selectFacialRecognition(event, index) {
        if(event.target.checked) {
            step2Data.Recipients[index].IsSignatory = event.target.checked
            step2Data.Recipients[index].IsInPerson = false;
            step2Data.Recipients[index].recipientType ='';
            step2Data.Recipients[index].IsNotarizer = false;
        }
        step2Data.Recipients[index].IsFacialRecognition = event.target.checked
        setStep2Data({ ...step2Data })

    }
    function selectIsPhotoRecognition(event, index) {
        if(event.target.checked) {
            step2Data.Recipients[index].IsSignatory = event.target.checked
            step2Data.Recipients[index].IsInPerson = false;
            step2Data.Recipients[index].recipientType =''
            step2Data.Recipients[index].IsNotarizer = false;
        }
        step2Data.Recipients[index].IsPhotoRecognition = event.target.checked
        setStep2Data({ ...step2Data })

    }

    const searchFirstName = debounce(
        (search) => {
            addDocumentService.getUserListInfo(search).then(data => {
                setUsersList(data.Entity)
                var dropElement = document.getElementById('userList');
                if (data.Entity && data.Entity.length > 0 && dropElement && dropElement.style.display == "none") {
                    dropElement.style.display = "block";
                } else if ((!data.Entity || data.Entity.lenght == 0) && (dropElement && dropElement.style.display == "block")) {
                    dropElement.style.display = "none";
                }
            })
        }, 500
    );
    function getDocumentFile(fileName) {
        addDocumentService.getDocumentFile(fileName).then(data => {
        })
    }
    //Document Full Width
    const Handelsidebar = () => {
        var element = document.getElementById("sidebar");
            element.classList.toggle("shrink");

            var MainElement = document.getElementById("mainrecipients");
            MainElement.classList.toggle("grow");

            document.getElementById("toggle").style.display = "none";
            document.getElementById("preview_btn").style.display = "block";
    }
    const Handelsidebar1 = () => {
        var element = document.getElementById("sidebar");
            element.classList.toggle("shrink");

            var MainElement = document.getElementById("mainrecipients");
            MainElement.classList.toggle("grow");

            document.getElementById("toggle").style.display = "block";
            document.getElementById("preview_btn").style.display = "none";
    }
      //Owl
      const options3 = {
        responsive: {
            0: {
                items: 4,
            },
            599: {
                items: 4,
            },
            1000: {
                items: 10,
            },
            1280: {
                items: 10,
                margin: 0
            },
            1366: {
                items: 10,
                margin: 0
            },
        },
    };
    //Owl
    function getWorkflowData() {
        addDocumentService.getWorkFlowTypes(true).then((types) => {
            setWorkflowTypes(types.Entity)
        })
    }
    function onDocumentLoadSuccess(e) {
        document.body.classList.remove('loading-indicator');
        selectedPageNumber ? setselectedPageNumber(selectedPageNumber) : setselectedPageNumber(1)
        setSubNumPages(e.numPages)
        let pagesArray = []
        for (let i = 1; i <= e.numPages; i++) {
            let page = {}
            page.checked = true,
                page.disabled = false
            page.pageNumber = i
            pagesArray.push(page)
        }
        setCheckBoxPages(pagesArray)
    }
    function subDocumentsLoadSuccess(e) {
        setSubNumPages(e.numPages)
        let pagesArray = []
        for (let i = 1; i <= e.numPages; i++) {
            let page = {}
            page.checked = true,
                page.disabled = false
            page.pageNumber = i
            pagesArray.push(page)
        }
        setCheckBoxPages(pagesArray)
    }
    function setOnMainDoc(pagenumber) {
        setselectedPageNumber(pagenumber)

    }
    function seletedRecipientCheckBoxs(pagenumber) {

    }

    /****** To Go to Back dashboard Page  */
    function backToDashboard() {
        getNext(false)
    }
    function getRandomNumberBetween(){
        return Math.floor(Math.random()*(18-0+1)+0);
    }
    //Add New Recipient Modal
    function recipient() {
        SetCCstatus(false);
        SetPopupDSCstatus(false)
        var srcElement = document.getElementById('new_recipient');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            const result = step2Data.Recipients.filter(resp => !resp.IsDelete);
            setActiveRecipients(result)
            let num = result[result.length - 1].Position
            let num2 = Number(num) + 1
            clearErrors()
            setValue('Position', num2)
            reset({
                FirstName: '',
                LastName: '',
                EmailAddress: '',
                Designation: '',
                MobileNumber: '',
                CountryId: '',
                Signatory: false,
                Dsc:false,
                IsCCEnabled: false,
                FacialRecognition: false,
                PhotoVerification: false,
                Aadhar:false
            })
            return false;
        }
    }
    function recipientAutoClose() {
        var srcElement = document.getElementById('new_recipient');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            }
            clearErrors()
            SetCCstatus(false);
            SetPopupDSCstatus(false)
            reset({
                FirstName: '',
                LastName: '',
                EmailAddress: '',
                Designation: '',
                MobileNumber: '',
                CountryId: '',
                Signatory: false,
                DSC:false,
                IsCCEnabled: false,
                FacialRecognition: false,
                PhotoVerification: false,
                Aadhar:false
            })
            return false;
        }
    }
    function closeUserDrop() {
        var dropElement = document.getElementById('userList');
        if (dropElement && dropElement.style.display == "block") {
            dropElement.style.display = "none";
        }
    }
    function changeFirstName(userObj) {
        if (userObj) {
            setValue('FirstName', userObj.FirstName)
            setValue('LastName', userObj.LastName)
            setValue('EmailAddress', userObj.EmailAddress)
        }
        closeUserDrop()
    }
    function addRecipient(data) {
        let emailDuplicate = ''
        let DSCUserList = []
        let respResult = []
        // if (!data.Position) {
            respResult = step2Data.Recipients.filter(resp => !resp.IsDelete);
        // }
        for (let index = 0; index < step2Data.Recipients.length; index++) {
            if(step2Data.Recipients[index].EmailAddress == data.EmailAddress) {
                emailDuplicate = step2Data.Recipients[index]
            }
            if(step2Data.Recipients[index].IsDSC) {
                DSCUserList.push(step2Data.Recipients[index])
            }
        }
        emailDuplicate = step2Data.Recipients.find(item => item.EmailAddress == data.EmailAddress && item.IsDelete == 0)
        if (emailDuplicate) {
            setError("EmailAddress", { type: "server", message: 'Email Address is already exist' });
        } else if(DSCUserList && DSCUserList.length == 5 && data.DSC) {
            setError("DSC", { type: "server", message: 'Enabling of DSC is allowed only for 5 recipients.'});
        } else {
            if(pendingColors && pendingColors.length == 0) {
                pendingColors = [...colorTypes]
            }
            addDocumentService.getUserDeatailsByEmail(data.EmailAddress).then((resDetails) => {
            const result = step2Data.Recipients.filter(resp => !resp.IsDelete);
            let addRecipientpages = [];
            for (let i = 1; i <= subNumPages; i++) {
                addRecipientpages.push(i)
            }
            data.FullName = data.FirstName + ' ' + data.LastName;
            data.IsSignatory = (data.Signatory == 'true') ? true : false;
            data.Color = pendingColors[0];
            data.UserId = null;
            data.Initial = 'TS';
            data.SignType = null;
            data.IsOwner = false;
            data.StatusCode = 'DRAFT';
            data.IsInPerson = '';
            data.IsDelete = 0;
            data.ProfilePicture = (resDetails && resDetails.Entity && resDetails.Entity[0] && resDetails.Entity[0].ProfilePicture) ? resDetails.Entity[0].ProfilePicture : '';
            data.IsNew = true;
            data.Position = data.Position ? data.Position : respResult.length + 1
            data.SignatureUpload = null;
            data.IsDSC = data.DSC
            // data.Dsc=(data.DSC === 'true') ? true:false;
            data.IsFacialRecognition = (data.FacialRecognition == 'true') ? true : false;
            data.IsPhotoRecognition = (data.PhotoVerification == 'true') ? true : false;
            data.IsLocationTracking = (data.IsLocationTracking == 'true') ? true : false;
            data.IsCCEnabled = (data.IsCCEnabled == 'true') ? true : false;
            data.RecipientPageNumbers = addRecipientpages;
            if (data.Position && data.Position <= respResult.length) {
                step2Data.Recipients.splice(data.Position - 1, 0, data);
                // step2Data.Recipients[data.Position - 1].Position
                getProperRecipientDeleteandPosition()
            } else {
                data.Position = respResult.length + 1
                pendingColors.splice(0,1)
                step2Data.Recipients.push(data)
                setStep2Data(step2Data);
            }
            if (result.length == 2) {
                getParallel(0, false);
            }
            if(result.length >= 2) {
                if(isOwnerSequential) {
                    getRecipientOnLast();
                    setIsOwnerPositionLastPopup(true);
                }
            }
            if(data.IsDSC) {
            DSCUserExistDataFormation()
            }
            recipient()
            clearErrors()
            reset({
                FirstName: '',
                LastName: '',
                EmailAddress: '',
                Designation: '',
                MobileNumber: '',
                CountryId: '',
                Signatory: false,
                DSC: false,
                IsCCEnabled: false,
                FacialRecognition: false,
                PhotoVerification: false
            })
            SetCCstatus(false);
            SetPopupDSCstatus(false);
        })
        }
    }
function  DSCUserExistDataFormation() {
    let documentData ={...step2Data}
    for (let l = 0; l < documentData.Recipients.length; l++) {
            documentData.Recipients[l].IsInPerson = false;
            documentData.Recipients[l].recipientType ='';
            documentData.Recipients[l].IsNotarizer = false;
            documentData.Recipients[l].IsFacialRecognition = false;
            documentData.Recipients[l].IsPhotoRecognition = false;
        if(documentData.Recipients[l].IsDSC && documentData.Recipients[l].IsCCEnabled) {
            documentData.Recipients[l].IsSignatory = true
        }
    }
    setStep2Data({ ...documentData })
}
   function getRecipientOnLast() {
    if (step2Data && step2Data.Recipients) {
        const nonDeleteResp = []
        const deleteResp = []
        for (let index = 0; index < step2Data.Recipients.length; index++) {
            if (step2Data.Recipients[index].IsDelete) {
                deleteResp.push(step2Data.Recipients[index])
            } else {
                nonDeleteResp.push(step2Data.Recipients[index])
                nonDeleteResp[nonDeleteResp.length - 1].Position = nonDeleteResp.length
                setActiveRecipients(nonDeleteResp)
            }
        }
        const ownerIndex = nonDeleteResp.findIndex(data=>data.IsOwner)
        console.log(ownerIndex, nonDeleteResp.length)
        if((ownerIndex || ownerIndex ==0) && nonDeleteResp.length > 1) {
            const [reorderedItem] = nonDeleteResp.splice(ownerIndex, 1);
            nonDeleteResp.splice(nonDeleteResp.length, 0, reorderedItem);
            for (let index = 0; index < nonDeleteResp.length; index++) {
                nonDeleteResp[index].Position = index + 1
            }
        }
        step2Data.Recipients = [...nonDeleteResp, ...deleteResp]
        setStep2Data({ ...step2Data })
    }
    }
function getSecured(event, recipientdelete) {
        const result = step2Data.Recipients.filter(resp => !resp.IsDelete)
        if(event==0){
            // console.log(result, 'resultresult')
            // if (recipientdelete && result && result.length < 2) {
            //     getSecureQuery(event);
            // } else if (result && result.length == 1) {
                getSecureQuery(event);
            // }
        } else{
            if(result.length > 1){
            //  if (recipientdelete && step2Data.Recipients && step2Data.Recipients.length < 2) {
            //         getSecureQuery(event);
            //     } else if (step2Data.Recipients && step2Data.Recipients.length > 1) {
                    getSecureQuery(event);
                // }
           }
           else{
                alertService.warn("Insufficient recipients to enable Secured signing workflow. Add 1 recipient to enable Secured mode.")
            }
        }       
    }
    function getSecureQuery(event) {
        if (event == 0) {
            if (step2Data && step2Data.Recipients) {
                setStep2Data({ ...step2Data, ['IsSecured']: event });
            }
            document.getElementById('Normal').classList.add('active')
            document.getElementById('Secured').classList.remove('active')
        } else {
            let result = []
            if (step2Data && step2Data.Recipients) {
                result = step2Data.Recipients.filter(resp => !resp.IsDelete);
            }
            if (result && result.length == 2 && result[1] && result[1].IsCCEnabled) {

            } else {
                document.getElementById('Secured').classList.add('active')
                document.getElementById('Normal').classList.remove('active')
                if (step2Data && step2Data.Recipients) {
                    let array = step2Data.Recipients;
                    let DSCExist = false
                    array.forEach(e => {
                        if(e.IsDSC){
                            DSCExist = true
                        }
                        e.IsDSC = false
                    })
                    if(DSCExist) {
                        alertService.warn("DSC and Secured cannot be enabled at the same time, if you select secured, DSC toggles will be disabled.")
                    }
                    setRecipientPersonHaveDSC(false)
                    setStep2Data({ ...step2Data, IsSecured: `${event}`, Recipients: [...array] })
                }
            }

        }
    }
    function getParallelAadharEsign(event, recipientdelete) {
        const result = step2Data.Recipients.filter(resp => !resp.IsDelete)
        if(result) {
            setActiveRecipients(result)
        }
        if(event == false || event == 0){
        step2Data ? (step2Data.IsParallelSigning = event) : false
            setStep2Data({ ...step2Data, ['IsParallelSigning']: event });
            getParallelQuery(0)
            if (!step2Data.IsParallelSigning) {
                for (let index = 0; index < step2Data.Recipients.length; index++) {
                    step2Data.Recipients[index].recipientType = ''
                    step2Data.Recipients[index].IsInPerson = false;
                    step2Data.Recipients[index].IsNotarizer = false;
                    setStep2Data({ ...step2Data });
    
                }
            }
        }
    }
    function getParallel(event, recipientdelete) {
        const result = step2Data.Recipients.filter(resp => !resp.IsDelete)
        if(result) {
            setActiveRecipients(result)
        }
        if (recipientdelete && result.length < 3) {
            step2Data ? (step2Data.IsParallelSigning = 1) : false
            setStep2Data({ ...step2Data, ['IsParallelSigning']: 1 });
            getParallelQuery(1)
        } else if (result.length >= 3) {
            step2Data ? (step2Data.IsParallelSigning = event) : false
            setStep2Data({ ...step2Data, ['IsParallelSigning']: event });
            getParallelQuery(event)
        } else {
            if(event == 0) {
            alertService.warn('insufficient recipients to enable Sequential signing workflow. Add 2 recipients(s) to enable sequential mode.')

            }
        }
        if (!step2Data.IsParallelSigning) {
            for (let index = 0; index < step2Data.Recipients.length; index++) {
                step2Data.Recipients[index].recipientType = ''
                step2Data.Recipients[index].IsInPerson = false;
                step2Data.Recipients[index].IsNotarizer = false;
                setStep2Data({ ...step2Data });

            }
        }

    }
    function getParallelQuery(event) {
        if (event == 0) {
            document.getElementById('Sequential').classList.add('active')
            document.getElementById('Parallel').classList.remove('active')
            getProperRecipientDeleteandPosition()
        } else {
            document.getElementById('Parallel').classList.add('active')
            document.getElementById('Sequential').classList.remove('active')
        }
    }
    function getProperRecipientDeleteandPosition() {
        if (step2Data && step2Data.Recipients) {
            const nonDeleteResp = []
            const deleteResp = []
            for (let index = 0; index < step2Data.Recipients.length; index++) {
                if (step2Data.Recipients[index].IsDelete == 1) {
                    deleteResp.push(step2Data.Recipients[index])
                } else {
                    nonDeleteResp.push(step2Data.Recipients[index])
                    nonDeleteResp[nonDeleteResp.length - 1].Position = nonDeleteResp.length
                    setActiveRecipients(nonDeleteResp)
                }
            }
            for (let index = 0; index < nonDeleteResp.length; index++) {
                nonDeleteResp[index].Position = index + 1
            }
            step2Data.Recipients = [...nonDeleteResp, ...deleteResp]
            setStep2Data({ ...step2Data })
            ReArrangofthetoggleforAadhar()
        }
    }
    function isValid(data) {
        if (data == '' || data == null || data == undefined || data == 'undefined' || data == 'null') {
            return false
        } else { return true }
    }
    function deleteRecipientOk() {
        const nonDeleteResp = []
        const deleteResp = []
        if (deleteRecipient && (deleteRecipient.Index || deleteRecipient.Index == 0) ) {
            if (deleteRecipient.RecipientId || deleteRecipient.CCRecipientId) {
                step2Data.Recipients[deleteRecipient.Index].IsDelete = 1
                step2Data.Recipients[deleteRecipient.Index].DeletionMode = 'deleteBoth'
                pendingColors.push(deleteRecipient.Color)
            } else {
                pendingColors.push(deleteRecipient.Color)
                step2Data.Recipients.splice(deleteRecipient.Index, 1)
            //     let contactindex=selectedContacts.findIndex(function(i){
            //         return i.CompanyContactId === deleteRecipient.CompanyContactId;
            //    })
            //    selectedContacts.splice(contactindex,1)
            }
            for (let index = 0; index < step2Data.Recipients.length; index++) {
                if (step2Data.Recipients[index].IsDelete == 1) {
                    deleteResp.push(step2Data.Recipients[index])
                } else {
                    nonDeleteResp.push(step2Data.Recipients[index])
                    nonDeleteResp[nonDeleteResp.length - 1].Position = nonDeleteResp.length
                    setActiveRecipients(nonDeleteResp)
                }

            }
            step2Data.Recipients = [...nonDeleteResp, ...deleteResp]
            setStep2Data({ ...step2Data })
            setDeleteRecipient({})
            setModalShow(false);
            // step2Data.Recipients.ma
            const result = step2Data.Recipients.filter(resp => !resp.IsDelete);
            if (result && result.length < 3) {
                getParallel(1, true)
            }
            if (result && result.length < 2) {
                getSecured(0, false)
            }
            if (result && result.length == 2) {
                const isCCResp = result.filter(resp => resp.IsCCEnabled);
                if (isCCResp && isCCResp.length > 0) {
                    getSecured(0, false)
                }
            }
        }
        //  else if (deleteRepData && deleteRepData.Index) {
        // step2Data.Recipients.splice(deleteRepData.Index)
        // step2Data.Recipients.splice(deleteRepData.Index)
        // setStep2Data({ ...step2Data })
        // deleteRepData = {}

        // }

    }
    function getPageCountforcheckBox(event, page, index) {
        let recipientData = JSON.parse(JSON.stringify(selectedRecipients))
          checkBoxPages[index].checked = event.target.checked;
            const index1=recipientData.RecipientPageNumbers.indexOf(checkBoxPages[index].pageNumber)
        if (index1 == -1) {
            recipientData.RecipientPageNumbers.push(checkBoxPages[index].pageNumber)
        }
        else {
            recipientData.RecipientPageNumbers.splice(index1, 1)
        }
        recipientData.RecipientPageNumbers.sort((a, b) => a - b)
        setSelectedRecipients({ ...recipientData })
        // const index2=step2Data.Recipients.findIndex(e=>e.RecipientId===selectedRecipients.RecipientId)
        // step2Data.Recipients=step2Data.Recipients.map((e,i)=>{if(i==index2){return {...selectedRecipients}}else {return {...e}}})
        step2Data.Recipients[recipientData.Index] = { ...recipientData }
          setStep2Data({...step2Data})
        setCheckBoxPages([...checkBoxPages])

    }
    function randomnumber(index) {
        const min = 1;
        const max = 100;
        return min + Math.random() * (max - min + index);
    }
    function redirectToNext() {
        let Aadhaarenabled1=''
        Aadhaarenabled1 = step2Data.Recipients.find(item => item.IsAadhaarEsign == 1 || item.IsAadhaarEsign == true)
        if(Aadhaarenabled1){
            setIsAnyoneEnableAadharEsign(true)
        // if(IsAnyoneEnableAadharEsign){
            let FinalData1 = {}
            FinalData1 = [...step2Data.Recipients] 
            let nondeletedrecipients = []
            FinalData1.map(item=>{
               if(item.IsDelete !== 1 || item.IsDelete !== true){
                nondeletedrecipients.push(item)
               }
            })
            if(nondeletedrecipients.length>5){
            setshowmorethanfiverecipientsnotallowed(true)}
            else{getNext(true)}
        }
        else{
            setIsAnyoneEnableAadharEsign(false)
            getNext(true) 
        }
    }
 function getNext(next) {
    setValidationExist(false)
            let invalid = false
            let FinalData = {}
            FinalData = { ...step2Data }
            noFacialImageResp = []
            onlyNonFacialImageResp = []
            facialErrorExist = false
            photoErrorExist = false
            facialWithNoProfilePic = false
            for (let index = 0; index < FinalData.Recipients.length; index++) {
                FinalData.Recipients[index].SortPosition = FinalData.Recipients[index].Position
                if((FinalData.Recipients[index].IsFacialRecognition || FinalData.Recipients[index].IsPhotoRecognition)
                 && !FinalData.Recipients[index].ProfilePicture && !FinalData.Recipients[index].IsDelete ){
                    noFacialImageResp.push(FinalData.Recipients[index])
                    if(FinalData.Recipients[index].IsFacialRecognition){
                        facialErrorExist = true
                        if(!FinalData.Recipients[index].ProfilePicture && !FinalData.Recipients[index].IsPhotoRecognition) {
                            facialWithNoProfilePic = true
                            onlyNonFacialImageResp.push(FinalData.Recipients[index])
                        }
                    }
                    if(FinalData.Recipients[index].IsPhotoRecognition){
                        photoErrorExist = true
                    }
                 }
                
                if(FinalData?.Recipients[index].RecipientPageNumbers.length < 1 && !FinalData.Recipients[index].IsDelete) {
                    setValidationExist(true);
                    invalid = true
                   }
                if (whatsup === true && !FinalData.Recipients[index].IsDelete && (!isValid(FinalData.Recipients[index].MobileNumber)|| !isValid(FinalData.Recipients[index].CountryId))){
                    step2Data.Recipients[index].errorExist = true;
                    step2Data.Recipients[index].errorMobileNumberExist =  !isValid(FinalData.Recipients[index].MobileNumber)? true : false;
                    step2Data.Recipients[index].errorConuntryCodeExist = !isValid(FinalData.Recipients[index].CountryId) ? true : false;
                    setValidationExist(true);
                    invalid = true 
                }
              if (FinalData.Recipients[index].IsAadhaarKYC && !FinalData.Recipients[index].IsDelete && (!isValid(FinalData.Recipients[index].MobileNumber)||!isValid(FinalData.Recipients[index].CountryId) || !isValid(FinalData.Recipients[index].DOB) )){
                    step2Data.Recipients[index].errorExist = true;
                    step2Data.Recipients[index].errorMobileNumberExist =  !isValid(FinalData.Recipients[index].MobileNumber)? true : false;
                    step2Data.Recipients[index].errorConuntryCodeExist =!isValid(FinalData.Recipients[index].CountryId) ? true : false;
                    step2Data.Recipients[index].errorDOBExist = !isValid(FinalData.Recipients[index].DOB) ? true : false  
                     setValidationExist(true);
                    invalid = true 
                }  
                if (FinalData.IsSecured == 1 || FinalData.IsSecured == true) {
                    if (whatsup === true && !FinalData.Recipients[index].IsDelete && (!isValid(FinalData.Recipients[index].MobileNumber)|| !isValid(FinalData.Recipients[index].CountryId))){
                        setValidationExist(true);
                        invalid = true 
                    }
                     if (!FinalData.Recipients[index].IsCCEnabled && !FinalData.Recipients[index].IsInPerson && !FinalData.Recipients[index].IsNotarizer && !FinalData.Recipients[index].IsOwner && !FinalData.Recipients[index].IsDelete && (!FinalData.Recipients[index].IsFacialRecognition && !FinalData.Recipients[index].IsPhotoRecognition)
                        && (!isValid(FinalData.Recipients[index].MobileNumber) 
                            || !isValid(FinalData.Recipients[index].CountryId))) {
                        step2Data.Recipients[index].errorExist = true;
                        step2Data.Recipients[index].errorMobileNumberExist = !isValid(FinalData.Recipients[index].MobileNumber) ? true : false;
                        step2Data.Recipients[index].errorConuntryCodeExist = !isValid(FinalData.Recipients[index].CountryId)  ? true : false;
                        setStep2Data({ ...step2Data })
                        setValidationExist(true);
                        invalid = true
    
                    }
                }
                if (!FinalData.Recipients[index].IsDelete && (!isValid(FinalData.Recipients[index].FirstName) || !isValid(FinalData.Recipients[index].LastName)
                    || !isValid(FinalData.Recipients[index].EmailAddress))) {
                    step2Data.Recipients[index].errorExist = true;
                    step2Data.Recipients[index].errorFirstNameExist = isValid(FinalData.Recipients[index].FirstName) ? false : true;
                    step2Data.Recipients[index].errorEmailAddressExist = isValid(FinalData.Recipients[index].EmailAddress) ? false : true;
                    step2Data.Recipients[index].errorLastNameExist = isValid(FinalData.Recipients[index].LastName) ? false : true;
                    setStep2Data({ ...step2Data })
                    setValidationExist(true);
                    invalid = true
                }
                for (let k = 0; k < step2Data.Recipients.length; k++) {
                    if(isValid(step2Data.Recipients[k].MobileNumber) && isValid(FinalData.Recipients[index].MobileNumber)
                    && !FinalData.Recipients[index].IsDelete
                     && !step2Data.Recipients[k].IsDelete
                     && (step2Data.Recipients[k].MobileNumber == FinalData.Recipients[index].MobileNumber)
                       && (k != index)) {
                        setValidationExist(true);
                        invalid = true 
                        step2Data.Recipients[k].errorExist = true;
                     }
                     if(isValid(step2Data.Recipients[k].EmailAddress) && isValid(FinalData.Recipients[index].EmailAddress)
                      &&  !FinalData.Recipients[index].IsDelete
                      && !step2Data.Recipients[k].IsDelete
                     && step2Data.Recipients[k].EmailAddress == FinalData.Recipients[index].EmailAddress
                      && k != index) {
                        setValidationExist(true);
                        invalid = true 
                        step2Data.Recipients[k].errorExist = true;
                     }
                     if(!FinalData.Recipients[index].IsDelete && EmailValidator.validate(FinalData.Recipients[k].EmailAddress) === false){
                        setValidationExist(true)
                        invalid = true
                        step2Data.Recipients[k].errorExist = true;
                    }
                }
        }
            
            if (invalid) {
            } else if (!invalid) {
                let workFlowObj = workflowTypes.find(r => r.WorkflowCode == selectedWorkFlowType)
                FinalData.WorkflowCode = workFlowObj.WorkflowCode
                FinalData.WorkflowTypeId = workFlowObj.WorkflowTypeId
                FinalData.WorkflowName = workFlowObj.WorkflowName
                const nonCCResp = []
                const CCResp = []
                for (let index = 0; index < FinalData.Recipients.length; index++) {
                    if( !FinalData.Recipients[index].DOB){
                        console.log(FinalData.Recipients[index].DOB,"ifinside")
                        FinalData.Recipients[index].DOB = null
                    }
                   let filterRecipient =  FinalData.Recipients[index].Controls ? FinalData.Recipients[index].Controls.filter(data=> data.includes('signature') || data.includes('initial') || data.includes('date')) : [];
                    if (!FinalData.Recipients[index].IsSignatory && filterRecipient && filterRecipient.length > 0) {
                        for (let index = 0; index < filterRecipient.length; index++) {
                            FinalData.Controls[filterRecipient[index]].IsDelete = true;
                        }
                    }
                    let DSCfilterRecipient =  FinalData.Recipients[index].Controls ? FinalData.Recipients[index].Controls.filter(data=> data.includes('digitalsignature')) : [];
                    if (!FinalData.Recipients[index].IsDSC && DSCfilterRecipient && DSCfilterRecipient.length > 0) {
                        for (let index = 0; index < DSCfilterRecipient.length; index++) {
                            FinalData.Controls[DSCfilterRecipient[index]].IsDelete = true;
                        }
                    }
                    let DSCenabled=''
                    DSCenabled = FinalData.Recipients.find(item => item.IsDSC == 1 || item.IsDSC == true && !item.IsDelete)
                    if(DSCenabled){
                        let nonDSCfilterRecipient =  FinalData.Recipients[index].Controls ? FinalData.Recipients[index].Controls.filter(data=> data.includes('signature','initial' ,'date','text','email','username','checkbox','username','companyname','image','aadhaar','dob','attachment','companyaddress','useraddress','checkbox')) : [];
                    //let nonDSCfilterRecipient =  FinalData.Recipients[index].Controls ? FinalData.Recipients[index].Controls.filter(data=> data.Code=='signature' || data.Code=='initial' || data.Code=='date'||data.Code=='text'||data.Code=='email'||data.Code=='username'||data.Code=='checkbox'||data.Code=='username'||data.Code=='companyname'||data.Code=='image'||data.Code=='aadhaar'||data.Code=='dob'||data.Code=='attachment'||data.Code=='companyaddress'||data.Code=='useraddress'||data.Code=='checkbox') : [];
                    if (nonDSCfilterRecipient  && nonDSCfilterRecipient.length > 0) {
                       let isdsc= nonDSCfilterRecipient.filter(data=>data.includes('digitalsignature'))
                       if(isdsc.length>0){// do nothing
                       }else{
                        for (let index = 0; index < nonDSCfilterRecipient.length; index++) {
                            FinalData.Controls[nonDSCfilterRecipient[index]].IsDelete = true;
                        }
                       }
                    }
                    }
                    if (FinalData.IsParallelSigning) {
                        FinalData.Recipients[index].Position = '';
                    }
                    if (!FinalData.IsSecured && whatsup === false &&  !FinalData.Recipients[index].IsAadhaarKYC) {
                        FinalData.Recipients[index].MobileNumber = '';
                        FinalData.Recipients[index].CountryId = '';
                        FinalData.Recipients[index].DOB = null;
                        FinalData.Recipients[index].DocumentOtp = '';
                    }
                    if((FinalData.Recipients[index].IsInPerson || FinalData.Recipients[index].IsNotarizer ) && whatsup === false) {
                        FinalData.Recipients[index].MobileNumber = '';
                        FinalData.Recipients[index].CountryId = '';
                        FinalData.Recipients[index].DocumentOtp = '';
                    }
                    if (FinalData.Recipients[index].IsCCEnabled) {
                        FinalData.Recipients[index].MobileNumber = whatsup === false?'':FinalData.Recipients[index].MobileNumber;
                        FinalData.Recipients[index].CountryId = whatsup === false?'':FinalData.Recipients[index].CountryId;
                        FinalData.Recipients[index].IsInPerson = false;
                        FinalData.Recipients[index].IsNotarizer = false;
                    }
                    if (FinalData.IsSecured == 1 || FinalData.IsSecured == true) {
                        // FinalData.Recipients[index].IsInPerson = false;
                        // FinalData.Recipients[index].IsNotarizer = false;
                        if(!FinalData.Recipients[index].DocumentOtp && !FinalData.Recipients[index].IsOwner &&
                             !FinalData.Recipients[index].IsInPerson && !FinalData.Recipients[index].IsNotarizer && !FinalData.Recipients[index].IsDSC
                              && !FinalData.Recipients[index].IsFacialRecognition && !FinalData.Recipients[index].IsPhotoRecognition) {
                            FinalData.Recipients[index].DocumentOtp = GenerateOTP()
                        }
                    }
                    if(FinalData.Recipients[index].IsFacialRecognition || FinalData.Recipients[index].IsPhotoRecognition || FinalData.Recipients[index].IsDSC) {
                        FinalData.Recipients[index].DocumentOtp = ''
                    }
                    if (FinalData.IsSequential == 1 || FinalData.IsSequential == true) {
                        FinalData.Recipients[index].IsInPerson = false;
                        FinalData.Recipients[index].IsNotarizer = false;
                    }
                    if (FinalData.Recipients[index].IsNew) {
                        if (!FinalData.Recipients[index].IsCCEnabled || FinalData.Recipients[index].IsCCEnabled == 0) {
                            FinalData.Recipients[index].RecipientId = -1;
                            nonCCResp.push(FinalData.Recipients[index]);
                        } else {
                            FinalData.Recipients[index].CCRecipientId = -1;
                            CCResp.push(FinalData.Recipients[index]);
                        }
                    } else {
                        if (!FinalData.Recipients[index].IsCCEnabled || FinalData.Recipients[index].IsCCEnabled == 0) {
                            if (FinalData.Recipients[index].CCRecipientId) {
                                FinalData.Recipients[index].RecipientId = -1;
                                nonCCResp.push(FinalData.Recipients[index]);
                                let respData = { ...FinalData.Recipients[index] };
                                respData.IsDelete = 1;
                                CCResp.push(respData);
                            } else {
                                nonCCResp.push(FinalData.Recipients[index]);
                            }
                        }
                        if (FinalData.Recipients[index].IsCCEnabled || FinalData.Recipients[index].IsCCEnabled == 1) {
                            if (FinalData.Recipients[index].RecipientId) {
                                FinalData.Recipients[index].CCRecipientId = -1;
                                CCResp.push(FinalData.Recipients[index]);
                                let nonRespData = { ...FinalData.Recipients[index] };
                                nonRespData.IsDelete = 1;
                                nonRespData.DeletionMode = 'deleteBoth'
                                nonCCResp.push(nonRespData);
                            } else {
                                CCResp.push(FinalData.Recipients[index]);
                            }
                        }
                    }
                }
                nonCCResp.forEach((data, index)=> data.Position = index + 1)
                FinalData.Recipients = nonCCResp;
                FinalData.CCRecipients = CCResp;
                submitData={...FinalData}
                isNext = next
                if(noFacialImageResp.length > 0){
                    setShowFacialPopUP(true)
                } else {
                    submitDoc()
                }
            }
    }
    function submitDoc() {
        console.log(submitData.ADocumentId, submitData)
        setShowFacialPopUP(false)
        addDocumentService.updateDocumentInfo(submitData.ADocumentId, submitData).then(data => {
            if (data) {
                if(isNext) {
                usehistory.push('/account/documentprepare/' + submitData.ADocumentId)
                } else {
                    usehistory.push({
                        pathname: `/account/adddocumentsstep/${step2Data.ADocumentId}`,
                        state: {DocumentID:`${step2Data.ADocumentId}`
                    }})
                }
            }

        })
        
    }
    function activateRecipient(data,divAct, i) {
        let controlExistPages=[]
        if(data.Controls && data.Controls.length>0){
            data.Controls.forEach(e=>{
                controlExistPages.push(step2Data.Controls[e].PageNumber)
             })
        }
        if (i || i == 0) {
            step2Data.Recipients[i].divActive = divAct ? false : true;
        }
        for (let index = 0; index < step2Data.Recipients.length; index++) {
            if (i != index) {
                step2Data.Recipients[index].divActive = false;
            }
        }
           checkBoxPages.forEach((e, i)=>{
            let IsControlExist = controlExistPages.includes(e.pageNumber) 
            let index=data.RecipientPageNumbers?.includes(e.pageNumber)
            if (index || data.IsInPerson || data.IsOwner) {
                checkBoxPages[i].checked = true;
                checkBoxPages[i].disabled = false;
                if (data.IsOwner == true || data.IsCCEnabled == true || IsControlExist || data.IsInPerson) {
                    checkBoxPages[i].disabled = true;
                }
            } else {
                checkBoxPages[i].checked = false;
                checkBoxPages[i].disabled = false;
            }
           })
        setCheckBoxPages([...checkBoxPages])
        setStep2Data({ ...step2Data });
        if(step2Data.Recipients[i].divActive) {
            setSelectedRecipients({...data})
        } else {
            setSelectedRecipients(null)
        }
    }
    function getContactSerchValue(event) {

    }
    function getOnLoadSuccess(e, index) {
        const importPDFCanvas = document.querySelector('.import-pdf-page canvas');
        const pdfAsImageSrc = importPDFCanvas.toDataURL();
        // checkBoxPages[index].canvasImageUrl = pdfAsImageSrc;
        // setCheckBoxPages([...checkBoxPages])

    }
    const useOutsideAlerter1 = (ref1) => {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref1.current && !ref1.current.contains(event.target) && event.target.id != 'new_recipient_add'&& event.target.id != 'wrapper_other') {
                    recipientAutoClose()
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref1]);
    }

    useOutsideAlerter1(wrapperRef1);
    function randomnumber(index){
        const min = 1;
        const max = 100;
        return  min + Math.random() * (max - min + index);
    }
   function onDragEnd(result) {
    if(result && result.destination && (result.destination.index || result.destination.index ==0)) {
    const [removed] = step2Data.Recipients.splice(result.source.index, 1);
    step2Data.Recipients.splice(result.destination.index, 0, removed);
    getProperRecipientDeleteandPosition()
    }
    }
    const setselectedcontactasrecipients=()=>{
        let EmailAddressIsExist = false
        let addRecipientpages = [];
        duplicateEmailRespients = []
       let nonDuplicateEmailRespients = []
        let nonDeleteRespResult = step2Data.Recipients.filter(resp => !resp.IsDelete);
        for (let i = 1; i <= subNumPages; i++) {
            addRecipientpages.push(i)
        }
        for (let r = 0; r < selectedContacts.length; r++) {
            for (let l = 0; l < nonDeleteRespResult.length; l++) {
                if (selectedContacts[r].EmailAddress == nonDeleteRespResult[l].EmailAddress) {
                    duplicateEmailRespients.push(selectedContacts[r])
                    break;
                }
            }
        }
        for (let j = 0; j < selectedContacts.length; j++) {
            for (let k = 0; k < nonDeleteRespResult.length; k++) {
                if (selectedContacts[j].EmailAddress === nonDeleteRespResult[k].EmailAddress) {
                    EmailAddressIsExist = true;
                    break;
                } else{
                    EmailAddressIsExist = false;
                }
            }
            if(pendingColors && pendingColors.length == 0) {
                pendingColors = [...colorTypes]
            }
            if(EmailAddressIsExist == false) {
                console.log(selectedContacts[j])
                let Obj = {...selectedContacts[j]}
                Obj.FullName = Obj.FirstName + ' ' + Obj.LastName
                Obj.IsSignatory = false;
                Obj.Color = pendingColors[0];
                Obj.UserId = null;
                Obj.Initial = '';
                Obj.SignType = null;
                Obj.IsOwner = false;
                Obj.StatusCode = 'DRAFT';
                Obj.IsInPerson = '';
                Obj.IsDelete = 0;
                Obj.IsNew = true;
                Obj.SignatureUpload = null;
                Obj.IsFacialRecognition = false;
                Obj.IsPhotoRecognition = false;
                Obj.IsCCEnabled = false;
                Obj.RecipientPageNumbers = addRecipientpages
                
                pendingColors.splice(0, 1)
                nonDuplicateEmailRespients.push(Obj)
            } else {
                // break;
            }
            }
        //if (selectedContacts.length==0) {
        //setContactsNotSelected(true)
        //} else if (EmailAddressIsExist) {
           // setContactsEmailExist(true)
        //} else {
            if (duplicateEmailRespients && duplicateEmailRespients.length > 0) {
                setContactsEmailExist(true)
             } 
            //  else {
        //    let data =[]
        //    {(step2Data.Recipients).length>0&&step2Data.Recipients.map(item=>{
        //      if(!item.IsDelete) {
        //         data.push(item)
        //      }
        //    })}
            step2Data.Recipients = [...step2Data.Recipients, ...nonDuplicateEmailRespients];
            let TotalRecipient = [...nonDuplicateEmailRespients, ...nonDeleteRespResult];
            if (TotalRecipient.length > 2) {
                getParallel(0, false);
            }
            getProperRecipientDeleteandPosition();
            if (step2Data.Recipients.length >= 3 && isOwnerSequential) {
                    getRecipientOnLast()
                    setIsOwnerPositionLastPopup(true)
            }
            setContactsModalShow(false);
            setSelectedContacts([]);
        // }
        setContactsModalShow(false);
    }
    function setInPersonCheckBoxes(data, index) {
        let pagesArray = []
        let recipientData = {...data}
        recipientData.RecipientPageNumbers = []
        for (let i = 1; i <= subNumPages; i++) {
            let page = {}
            page.checked = true,
            page.disabled = true
            page.pageNumber = i
            pagesArray.push(page)
            recipientData.RecipientPageNumbers.push(i)
        }
        step2Data.Recipients[index] = { ...recipientData }
          setStep2Data({...step2Data})
        setCheckBoxPages(pagesArray)
    }

    function changeRecipientCardSignatory(event, index, data) {
        signatoryRecipientData = data
        signatoryRespIndex = index
        let filterRecipientControls = data.Controls ? data.Controls.filter(control => control.includes('signature') || control.includes('initial') || control.includes('date')) : [];
        if (filterRecipientControls && filterRecipientControls.length > 0 && !event.target.checked) {
            setShowSignatoryControlPopUp(true)
        } else {
            step2Data.Recipients[index].IsSignatory = event.target.checked
            setStep2Data({ ...step2Data })
        }
    }
    function changeSignatorypopupSign() {
        if (signatoryRespIndex || signatoryRespIndex ==0) {
            step2Data.Recipients[signatoryRespIndex].IsSignatory = false
            setStep2Data({ ...step2Data })
            setShowSignatoryControlPopUp(false)
        }
    }

    function setUserProfileData(EmailAddress, data, index) {
        console.log(EmailAddress.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g), '1111valid')
        if(EmailAddress.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g)) {
            addDocumentService.getUserDeatailsByEmail(EmailAddress).then((resDetails) => {
                step2Data.Recipients[index].ProfilePicture = (resDetails && resDetails.Entity && resDetails.Entity[0] && resDetails.Entity[0].ProfilePicture) ? resDetails.Entity[0].ProfilePicture : '';
                setStep2Data({ ...step2Data })
            })
        }
        const regexMail =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
}
function dobonchange(event,index){
    step2Data.Recipients[index].errorDOBExist = false,
      step2Data.Recipients[index].DOB= moment(event.target.value).format('YYYY-MM-DD'),
      console.log(step2Data.Recipients[index].DOB)
      console.log(step2Data.Recipients,"123456")
      //setStep2Data({...step2Data.Recipients[index].DOB})
      setStep2Data({...step2Data})
     //console.log(step2Data.Recipients[index],"9999")

}
function changeRespPopDSC(event) {
    clearErrors('DSC')
    setValue("Signatory", event.target.checked);
    if(event.target.checked) {
        setValue('IsCCEnabled', false)
        setValue("Signatory", true);
        setValue("FacialRecognition", false);
        setValue("PhotoVerification", false);
    }
}
function changeRespPopAadhar(event) {
    clearErrors('Aadhar')
    setValue("Signatory", event.target.checked);
    //setValue("Aadhar", event.target.checked);
    if(event.target.checked) {
       // getParallelAadharEsign(1,false)
       setValue("Aadhar", true);
        setValue("Signatory", true);
        setValue("FacialRecognition", false);
        setValue("PhotoVerification", false);
        setValue("Dsc",false);
        setValue('IsCCEnabled', false)
    }
}

function autooff(id){
 let element=document.getElementById(id)
 console.log(element,"test")
 element.setAttribute('autoComplete','off')
}

 const loadSpinner = () => {
        return <div style={{ position: "absolute", display: "flex", top: "10%", left: "30%", alignContent: 'center', alignItems: "center", justifyContent: "center", marginTop: '30%' }}>
            <Spinner variant="primary"  animation="border" /></div>
               }
               const loadSpinner_pages=()=>{
                return <div style={{marginLeft:"15rem",alignContent:'center',alignItems: "center",justifyContent: "center"}}>
                <Spinner variant="primary"  animation="border"/></div>}
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />
                <div className="main-content wizard">
                    <div className='row'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div id="wizard_step">
                                <div className="step-wizard" role="navigation">
                                    <ul>
                                        <li className={count === 0 || count < 4 ? 'active done' : ''}>
                                            <button>
                                                <div className="step">1</div>
                                                <div className="title">Add</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 1 || (count < 4 && count != 0) ? 'active done' : ''}>
                                            <button id="step2">
                                                <div className="step">2</div>
                                                <div className="title">Select</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 2 || (count < 4 && count != 1 && count != 0) ? 'active done' : ''}>
                                            <button id="step3">
                                                <div className="step">3</div>
                                                <div className="title">Prepare</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                        <li className={count === 3 ? 'active done' : ''}>
                                            <button id="step4">
                                                <div className="step">4</div>
                                                <div className="title">Review</div>
                                            </button>
                                            <div className="progressbar"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div className='wizard-form'>
                                    <div className='wizard-panel' id="wizard-pane-2" >
                                        <div className='content'>
                                            <div className='recipients_content'>
                                                <div className='row'>
                                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                    <div className='main_heading'>
                                                    <h2 className='document-select-overflowdots' >Select recipients to <span data-tip data-for='title'> {step2Data?.Title}</span></h2>
                                                    {/* <button type="button" onClick={Handelsidebar} id="toggle" className="side-arrow"><img src="src/images/right-arrw.svg" /></button> */}
                                                    <button type="button" onClick={Handelsidebar1} id="preview_btn" className="Preview" style={{display: "none"}}>Recipients</button>
                                                          <ReactTooltip id="title" place="top" effect="solid">
                                                           {step2Data?.Title}</ReactTooltip>

                                                    </div>
                                                        
                                                    </div>
                                                </div>

                                                <div className='row recipientsmain' id="mainrecipients">
                                                    <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 recipients-left-part'>
                                                        <div className='recipients_slider_cont'>
                                                            <div className='main_doc docuselect'>
                                                                <div className='doc-pagination checkbox_select' style={{display: 'flex', overflow: 'auto hidden'}}>
                                                                    {subNumPages ?
                                                                        <>
                                                                            {Array.apply(null, Array(subNumPages))
                                                                                .map((x, i) => i + 1)
                                                                                .map((page, index) => {
                                                                                    return <>
                                                                                        {checkBoxPages && checkBoxPages.length > 0 ?
                                                                                            <div className='item' style={{margin: '0.5rem'}}>
                                                                                                <p style={{ color: '#5aa0ff', cursor: 'pointer' }} onClick={() => { setOnMainDoc(page) }}
                                                                                                    htmlFor="2" className="checkbox-label">{page}</p>
                                                                                                <input type="checkbox" id={page} name={page} disabled={(!selectedRecipients || (selectedRecipients && selectedRecipients.IsInPerson) || checkBoxPages[index].disabled)}
                                                                                                    checked={(checkBoxPages[index].checked || !selectedRecipients)} onChange={function (event) { getPageCountforcheckBox(event, checkBoxPages[index], index) }}
                                                                                                /> </div> : null}
                                                                                    </>
                                                                                })}
                                                                        </>
                                                                        : null}

                                                            </div>
                                                                <Document id="mainDoc"
                                                                loading={loadSpinner}
                                                                noData={''}
                                                                    file={documentFile}
                                                                    onLoadSuccess={onDocumentLoadSuccess}>
                                                                    <Page className="mx-auto d-block document_select_intial" noData={''} scale={2} pageNumber={selectedPageNumber} />
                                                                </Document>
                                                                {/* <p>Page {pageNumber} of {numPages}</p> */}
                                                                {/* <img src="src/images/l_document_icon_4.jpg" alt="" className='img-fluid' /> */}
                                                            </div>
                                                            {/* <div className='page_num'>Total Pages: {subNumPages}</div>
                                                            <div className='carousel-item_mg'> */}

                                                                {/* {(checkBoxPages && data?.canvasImageUrl) ? <img src={data?.canvasImageUrl} alt="" /> : */}
                                                                {/* <Document className="" id="subDoc"
                                                                    file={subDocumentFile}
                                                                    loading={loadSpinner_pages}
                                                                    noData={''}
                                                                    onLoadSuccess={subDocumentsLoadSuccess}>
                                                                    {subNumPages ?
                                                                        <>
                                                                            {Array.apply(null, Array(subNumPages))
                                                                                .map((x, i) => i + 1)
                                                                                .map((page, index) => {
                                                                                    return <div className='item document-select col-3'>
                                                                                        <div  >
                                                                                            {checkBoxPages && checkBoxPages.length > 0 ? <input disabled={(!selectedRecipients || checkBoxPages[index].disabled)} id="remeberme" type="checkbox" className=''
                                                                                                checked={(checkBoxPages[index].checked || !selectedRecipients)} onChange={function (event) { getPageCountforcheckBox(event, checkBoxPages[index], index) }} /> : null}
                                                                                            <Page noData={''} onRenderSuccess={(e) => getOnLoadSuccess(e, index, data)}  key={page} scale={2} className="sub_document_select_intial" pageNumber={page} onClick={() => {setOnMainDoc(page)} } />

                                                                                        </div>
                                                                                        <span className='span-page-number'>page: {page}</span>
                                                                                    </div>
                                                                                })}
                                                                        </>
                                                                        : null}
                                                                </Document> */}
                                                                {/* } */}
                                                            {/* </div> */}
                                                        </div>
                                                    </div>
                                                    <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 recipients-right-part' id="sidebar">
                                                    <button type="button" onClick={Handelsidebar} id="toggle" className="side-arrow"><img src="src/images/right-arrw.svg" /></button>
                                                        <div className='recipients_scroll'>
                                                            <div className='top_sec'>
                                                                <div className='workflow'>
                                                                    <label>Workflow :</label>
                                                                    <select onChange={(event) => { setSelectedWorkFlowType(event.target.value) }} value={selectedWorkFlowType ? selectedWorkFlowType : ''} className='hlfinput'>
                                                                        {
                                                                            workflowTypes ? workflowTypes.map(el => <>{ el.WorkflowCode=="SIGNCAMP"?null:<option value={el?.WorkflowCode} key={el?.WorkflowCode}> {el?.WorkflowName} </option>}</>) : null
                                                                        }
                                                                    </select>
                                                                </div>
                                                                <div className='page_number add-pagenumber'>
                                                                    <label >Add Page Number :</label>
                                                                    <label className="switch">
                                                                        <input type="checkbox" defaultChecked={step2Data?.IsIncludePageNumber} onClick={(event) => {
                                                                            step2Data.IsIncludePageNumber = event.target.checked
                                                                        }} />
                                                                        <span className="slider round"></span>
                                                                    </label>
                                                                    <div> <img data-tip data-for="addPageNumber" src="src/images/info.svg" alt='' /></div>
                                                                    <ReactTooltip id="addPageNumber" place="top" effect="solid">
                                                                        <div>Select this option to add page number to the final signed document (pdf)</div>  that will be sent to your registered email ID
                                                                    </ReactTooltip>

                                                                </div>
                                                            </div>
                                                            <div className='middle_sec'>
                                                                <h2>Recipients</h2>
                                                                <div className="btn-lg-scet">
                                                                    <button className="btn" onClick={() => { setContactsModalShow(true),setSelectedContacts([]) }}>ADD FROM CONTACTS</button>
                                                                    <button className="btn" id="new_recipient_add" onClick={recipient}>Add New Recipient</button>
                                                                </div>
                                                                <div id="new_recipient" className='new_recipient' ref={wrapperRef1} style={{ display: 'none' }}>
                                                                    <form onSubmit={handleSubmit(addRecipient)}>

                                                                        <div className='recipient_content'>
                                                                            <div className='top_bar'>
                                                                                <h2>Add New Recipient </h2>
                                                                                <button type='submit' className='add_btn'>Add</button>
                                                                                {/* <button type='button' onClick={recipient} className='add_btn'>Cancel</button> */}

                                                                            </div>
                                                                            <div className='main-form'>
                                                                                <div className='row'>
                                                                                    <div className='col-lg-6 col-md-6 col-sm-12' >
                                                                                        <div className='form-group'>
                                                                                            <label>First Name<span style={{color:"red"}}>*</span></label>
                                                                                            <input type="text" name="FirstName" id="FirstName" {...register('FirstName', {
                                                                                                onChange: (e) => {
                                                                                                    e.target.value = e.target.value ? e.target.value[0].toUpperCase() + e.target.value.slice(1) : '';
                                                                                                    var dropElement = document.getElementById('userList');
                                                                                                    if (e.target.value && e.target.value.length > 2) {
                                                                                                        searchFirstName(e.target.value)
                                                                                                        if ((!userList || userList.lenght == 0)) {
                                                                                                            closeUserDrop()
                                                                                                        }
                                                                                                    } else if ((!userList || userList.lenght == 0 || e.target.value == '') && (dropElement && dropElement.style.display == "block")) {
                                                                                                        dropElement.style.display = "none";
                                                                                                    }
                                                                                                }
                                                                                            })} autoComplete="off"
                                                                                                />
                                                                                            {(userList && userList.length > 0) ? <div className="dropdown">
                                                                                                <div id="userList" className="dropdown-content" style={{ display: 'block' }}> {userList.map((data, index) => {
                                                                                                    return <> <option className='drop-option' onClick={(event) => { changeFirstName(data) }} >{data?.UserName}</option>
                                                                                                    </>
                                                                                                })}  </div></div> : null}
                                                                                             {errors.FirstName?.message ? <span className='error-message'>{errors.FirstName?.message}</span> : null}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className='col-lg-6 col-md-6 col-sm-12'>
                                                                                        <div className='form-group'>
                                                                                        <label>Last Name<span style={{color:"red"}}>*</span></label>
                                                                                            <input type="text" name="LastName" id="LastName" {...register('LastName', { onChange:(e)=>{
                                                                                             e.target.value = e.target.value ? e.target.value[0].toUpperCase() + e.target.value.slice(1) : '';

                                                                                            }})} autoComplete="off"  onClick={closeUserDrop} />
                                                                                            {errors.LastName?.message ? <span className='error-message'>{errors.LastName?.message}</span> : null}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className='row'>
                                                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                                                        <div className='form-group'>
                                                                                        <label>Email Address<span style={{color:"red"}}>*</span></label>
                                                                                            <input type="email" name="EmailAddress" id="EmailAddress" {...register('EmailAddress')} autoComplete="nope" onClick={closeUserDrop}  />
                                                                                            {errors.EmailAddress?.message ? <span className='error-message' >{errors.EmailAddress?.message}</span> : null}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                {whatsup === true && <p style={{color:"#9595f1",marginTop:3}}>Ensure valid mobile no for WhatsApp notification.</p>}
                                                                                {((step2Data?.IsSecured) && (!CCstatus)) || whatsup===true ? <div className="row">
                                                                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                                        <div className="form-group top_wrrp">
                                                                                            <div className="input-wrapper">
                                                                                                <select name="CountryId" id="CountryId" {...register('CountryId')}
                                                                                                    className="hlfinput" style={{color:'#6c757d'}}>
                                                                                                    <option value={''} >Country Code</option>
                                                                                                    {countryCodesArray.map(obj => <option value={obj.CountryId} key={obj.CountryId}>{obj.CountryCode + ' (+' + obj.PhoneCode + ')'}</option>)}
                                                                                                </select>
                                                                                                {errors.CountryId?.message ? <span className='error-message' >{errors.CountryId?.message}</span> : null}

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                                        <div className="form-group">
                                                                                            <div className="input-wrapper">
                                                                                                <input type="tel" maxLength={15} name="MobileNumber" id="MobileNumber" {...register('MobileNumber')}
                                                                                                    autoComplete="nope" placeholder='Phone' />
                                                                                                {errors.MobileNumber?.message ? <span className='error-message' >{errors.MobileNumber?.message}</span> : null}

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div> : null}
                                                                                <div className='row'>
                                                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                                                        <div className='form-group'>
                                                                                            <input type="text" id="Designation" name="Designation"  {...register('Designation')} autoComplete="nope" onClick={closeUserDrop} placeholder="Designation" />
                                                                                            {errors.Designation?.message ? <span className='error-message'>{errors.Designation?.message}</span> : null}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                {step2Data?.IsParallelSigning ? null : <div className='row'>
                                                                                    <div className='col-lg-12 col-md-12 col-sm-12'>
                                                                                        <div className="paging-count d-flex">
                                                                                            <span className="text">Position: <input type="text" id="Position" name="Position" placeholder=''  {...register('Position')}
                                                                                            onClick={closeUserDrop} />
                                                                                                </span>
                                                                                                {errors.Position?.message ? <span className='error-message'>{errors.Position?.message}</span> : null}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>}
                                                                            </div>
                                                                        </div>
                                                                        <div className='content_bottom'>
                                                                            <div className="frt-tgl">
                                                                                <div className="item">
                                                                                    {/* <div className="form-group">
                                                                                    <input type="checkbox" id="signatory" checked />
                                                                                    <label htmlFor="signatory"></label>
                                                                                </div> */}
                                                                                    {/* <h6>Signatory</h6>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" id="Signatory" name="Signatory" {...register('Signatory')} disabled={CCstatus || PopupDSCstatus  }
                                                                                            onChange={(event) => { console.log(event.target.checked) }} onClick={() => { closeUserDrop() }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label> */}
                                                                                </div>
                                                                                {/* {(Enabledsc === 1 || Enabledsc === true) ? 
                                                                                <>
                                                                                <div className="item">
                                                                                     <div style={{display:'flex'}}> <h6>DSC</h6> <img style={{marginLeft: '5px', marginBottom: '6px'}} data-tip data-for="OwnerDSC" src="src/images/info.svg" alt='' /></div>
                                                                                    <ReactTooltip id="OwnerDSC" place="top" effect="solid">
                                                                                        <div>Digital Signature Certificate issued by E-Mudra</div>
                                                                                    </ReactTooltip>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" id="DSC" name="DSC" {...register('DSC')}
                                                                                            onChange={(event) => { console.log(event.target.checked), changeRespPopDSC(event) }} onClick={() => { closeUserDrop() }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div>
                                                                                {errors.DSC?.message ? <span style={{paddingLeft: '10px'}} className='error-message' >{errors.DSC?.message}</span> : null}
                                                                                </>:""}
                                                                                {(EnableAadharekyc === 1 || EnableAadharekyc === true) ? 
                                                                                <>
                                                                                <div className="item">
                                                                                     <div style={{display:'flex'}}> <h6>Aadhar</h6> <img style={{marginLeft: '5px', marginBottom: '6px'}} data-tip data-for="OwnerAadhar" src="src/images/info.svg" alt='' /></div>
                                                                                    <ReactTooltip id="OwnerAadhar" place="top" effect="solid">
                                                                                        <div>Aadhar e-Sign Signature Certificate issued by NSDL</div>
                                                                                    </ReactTooltip>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" id="Aadhar" name="Aadhar" {...register('Aadhar')} 
                                                                                            onChange={(event) => { console.log(event.target.checked), changeRespPopAadhar(event) }} onClick={() => { closeUserDrop() }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div>
                                                                                {errors.Aadhar?.message ? <span style={{paddingLeft: '10px'}} className='error-message' >{errors.Aadhar?.message}</span> : null}
                                                                                </>:""} */}

                                                                                {/* <div className="item">
                                                                                <h6>eKYC</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox"  />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div> */}
                                                                              {/* <div className="item" >
                                                                                    <h6>CC</h6>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" id="IsCCEnabled" name="IsCCEnabled" {...register('IsCCEnabled')}
                                                                                            onClick={() => { setValue("Signatory", false); setValue("FacialRecognition", false); setValue("PhotoVerification", false); closeUserDrop() }} onChange={(event) => {
                                                                                                console.log(event.target.checked);
                                                                                                SetCCstatus(event.target.checked)
                                                                                                setValue("DSC", false);
                                                                                                setValue("Aadhar", false);
                                                                                            }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div> */}
                                                                                {/* { userData && userData.company_id ?
                                                                            <><div className="item">
                                                                                    <h6>Facial Recognition</h6>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" id="FacialRecognition" name="FacialRecognition" {...register('FacialRecognition')} disabled={CCstatus || ((step2Data?.IsSecured) ? true : false)}
                                                                                            onChange={(event) => { console.log(event.target.checked) }} onClick={() => { closeUserDrop() }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div>
                                                                                <div className="item" style={{ borderRight: '1px solid #E2E2E2' }}>
                                                                                    <h6>Photo Verification</h6>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" id="PhotoVerification" name="PhotoVerification" {...register('PhotoVerification')} disabled={CCstatus || ((step2Data?.IsSecured) ? true : false)}
                                                                                            onChange={(event) => { console.log(event.target.checked) }} onClick={() => { closeUserDrop() }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div></>:null} */}
                                                                            </div>
                                                                        </div>
                                                                    </form>

                                                                </div>
                                                                <div className="toggle_scroll toggle-space" >
                                                                    <div className="sect-btn-item"data-tip data-for="sequerandnormal">
                                                                        
                                                                            <button id="Normal"onClick={(event) => { getSecured(0, false) }} className='tog-ttl active'>Normal</button>
                                                                        
                                                                        
                                                                            <button id="Secured" data-tip data-for="securedButton" onClick={(event) => { getSecured(1, false) }} className='tog-ttl'>Secured</button>
                                                                           
                                                                        {/* {(activeRecipients.length < 2) ? <ReactTooltip id="sequerandnormal" place="top" effect="solid">
                                                                        Insufficient recipients to enable Secured signing workflow. Add 1 recipient to enable Secured mode.
                                                                                </ReactTooltip> : null} */}
                                                                    </div>
                                                                    <div className="sect-btn-item"  data-tip data-for="ParallelSequentialButton">
                                                                       
                                                                            <button id="Sequential" onClick={(event) => { getParallel(0, false) }} className='tog-ttl active'>Sequential</button>
                                                                            
                                                                            <button id="Parallel" disabled={IsAnyoneEnableAadharEsign == true?true:false} onClick={(event) => { getParallel(1, false) }} className='tog-ttl'>Parallel</button>
                                                                            {/* {(activeRecipients.length < 3) ? <ReactTooltip id="ParallelSequentialButton" place="top" effect="solid">
                                                                        Insufficient recipients to enable Sequential signing workflow. Add 2 recipient(s) to enable sequential mode.
                                                                                </ReactTooltip> : null} */}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <DragDropContext onDragEnd={onDragEnd}>
                                                                 <Droppable droppableId="documentSelectRecipientsDroppable">
                                                             {(provided, snapshot) => (
                                                            <div className='bottom_sec recipients_select_flow'  {...provided.droppableProps}
                                                            ref={provided.innerRef}>
                                                                {step2Data?.Recipients?.map((data, index) => {
                                                                    if(data.Position) {
                                                                        data.Index = data.Position - 1;
                                                                    }
                                                                    if(!data.randomNumber) {
                                                                        data.randomNumber = index + randomnumber(index).toString();
                                                                    }
                                                                    return (data.IsOwner ? (
                                                                        <Draggable autoscroll={false} isDragDisabled={(step2Data?.IsParallelSigning ==1 || step2Data?.IsParallelSigning) ? true : false } key={index.toString() + data?.EmailAddress + data?.FullName} draggableId={index + data?.EmailAddress} index={index}>
                                                                        {(provided, snapshot)=> (
                                                                    <div    ref={provided.innerRef}
                                                                    {...provided.draggableProps}
                                                                    {...provided.dragHandleProps} className={data.divActive ? 'srll-item first-srll-item respactive' + ((data?.errorExist)  ? ' error-recipient-border' : '') : 'srll-item first-srll-item' + ((data?.errorExist)  ? ' error-recipient-border' : '')}  onClick={() => { activateRecipient(data,data.divActive, index) }}>
                                                                        <div className="top_wrrp">
                                                                            <div className="headl">
                                                                                <h5>{data?.FullName}</h5>
                                                                                
                                                                                <p className="mail_txt">{data?.EmailAddress}</p>
                                                                                {whatsup === true && <p style={{color:"#9595f1",marginTop:3}}>Ensure valid mobile no for WhatsApp notification.</p>} 
                                                                                  {whatsup===true ? <div className="row doc_prepare_resp_country_phone">
                                                                                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 selectpad doc_prepare_resp_country">
                                                                                        <div className="form-group">
                                                                                            <div className="input-wrapper">
                                                                                                <select name="CountryCode" style={{color:'#6c757d'}} value={(data.CountryId) ? (data.CountryId) : ''} className={(data?.errorConuntryCodeExist) ? 'highlight-recipient-error hlfinput' : 'hlfinput'}
                                                                                                    onChange={(event) => { data.CountryId = event.target.value, step2Data.Recipients[index].CountryId = event.target.value, step2Data.Recipients[index].errorConuntryCodeExist = false, setStep2Data({ ...step2Data }) }} >
                                                                                                    <option value={''} disabled hidden>Country Code</option>
                                                                                                    {countryCodesArray.map(obj => <option value={obj.CountryId} key={obj.CountryId}>{obj.CountryCode + ' (+' + obj.PhoneCode + ')'}</option>)}
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-xl-6 col-lg-4 col-md-4 col-sm-12 selectpad doc_prepare_resp_phone">
                                                                                        <div className="form-group">
                                                                                            <div className="input-wrapper">
                                                                                                <input onFocus={()=>autooff("phone")} type="tel" maxLength={15} style={{width:"65%"}} value={(data?.MobileNumber) ? data.MobileNumber : ''} className={(data?.errorMobileNumberExist) ? 'highlight-recipient-error' : ''} id="phone" name="Phone"
                                                                                                    onChange={(event) => {
                                                                                                        let numberValid =/^(\+\d{1,3}[- ]?)?\d{0,15}$/
                                                                                                        if (numberValid.test(event.target.value)) {
                                                                                                            data.MobileNumber = event.target.value
                                                                                                            step2Data.Recipients[index].errorMobileNumberExist = false,
                                                                                                                step2Data.Recipients[index].MobileNumber = event.target.value, setStep2Data({ ...step2Data })
                                                                                                        } else {
                                                                                                            return false;
                                                                                                        }
                                                                                                    }

                                                                                                    }  placeholder='Phone' />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div> : null}
                                                                            </div>
                                                                            <div className="headr">
                                                                                <div>
                                                                                    <h6>Signatory</h6>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox"key={data.Index} disabled={RecipientPersonHaveDSC || data.IsAadhaarEsign === 1 || data.IsAadhaarEsign == true?true:false} checked={data.IsSignatory ? data.IsSignatory : false} onChange={(event) => { changeRecipientCardSignatory(event, index, data) }} />
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div>
                                                                                {(Enabledsc===1 ||Enabledsc === true)&&<div>
                                                                                    <div style={{display:'flex'}}> <h6>DSC</h6> <img style={{marginLeft: '5px', marginBottom: '6px'}} data-tip data-for="OwnerDSC" src="src/images/info.svg" alt='' /></div>
                                                                                    <ReactTooltip id="OwnerDSC" place="top" effect="solid">
                                                                                        <div>Digital Signature Certificate issued by E-Mudra</div>
                                                                                    </ReactTooltip>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" disabled={IsAnyoneEnableAadharEsign == true||data.IsAadhaarEsign === 1 || data.IsAadhaarEsign == true?true:false } key={data.Index} checked={data.IsDSC ? data.IsDSC : ''} onChange={(event) => { selectDSC(event, index) }}/>
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div>}
                                                                                {(EnableAadharekyc===1 ||EnableAadharekyc === true)&&<div>
                                                                                    <div style={{display:'flex'}}> <h6>Aadhar</h6> <img style={{marginLeft: '5px', marginBottom: '6px'}} data-tip data-for="OwnerAadhar" src="src/images/info.svg" alt='' /></div>
                                                                                    <ReactTooltip id="OwnerAadhar" place="top" effect="solid">
                                                                                    <div>Aadhar e-Sign Signature Certificate issued by NSDL</div>
                                                                                    </ReactTooltip>
                                                                                    <label className="switch">
                                                                                        <input type="checkbox" disabled={(data.IsDSC === 1 || data.IsDSC == true)?true:false } key={data.Index} checked={data.IsAadhaarEsign ? data.IsAadhaarEsign : ''} onChange={(event) => { selectAadhar(event, index) }}/>
                                                                                        <span className="slider round"></span>
                                                                                    </label>
                                                                                </div>}
                                                                            </div>
                                                                            </div>
                                                                            
                                                                        <div className="pageing-re d-flex" style={{overflow:'hidden'}}>
                                                                            <div className='pages-adjust-width'>
                                                                                <div data-tip data-for="initiatorPages" className='page_number threedots'>Pages: {data?.RecipientPageNumbers?.map((pagenumber, index) => {
                                                                                    if (index == (data.RecipientPageNumbers.length - 1)) {
                                                                                        return <span data-tip data-for={`Pages${data.Index}`}>{pagenumber} </span>
                                                                                    }
                                                                                    else {
                                                                                        return <span data-tip data-for={`Pages${data.Index}`}>{pagenumber}, </span>
                                                                                    }
                                                                                }
                                                                                )}
                                                                                    <ReactTooltip id={`Pages${data.Index}`} place="top" effect="solid">
                                                                                        {data?.RecipientPageNumbers?.map((pagenumber, index) => {
                                                                                            if (index == (data.RecipientPageNumbers.length - 1)) {
                                                                                                return <>{pagenumber}</>
                                                                                            }
                                                                                            else {
                                                                                                return <>{pagenumber}, </>
                                                                                            }
                                                                                        }
                                                                                        )}
                                                                                    </ReactTooltip>
                                                                                </div>
                                                                            </div>
                                                                            {step2Data.IsParallelSigning ? null : <div className="page-count count-position">
                                                                                <span> Position: {data?.Position}</span>
                                                                            </div>}
                                                                        </div>
                                                                    </div> )}
                                                                    </Draggable>) : ((data.IsDelete != 1) ? 
                                                                    <Draggable autoscroll={false} isDragDisabled={(step2Data?.IsParallelSigning ==1 || step2Data?.IsParallelSigning) ? true : false } key={index.toString() + data?.randomNumber} draggableId={index + data?.randomNumber} index={index}>
                                                                         {(provided, snapshot)=> (
                                                                    <div    ref={provided.innerRef}
                                                                    {...provided.draggableProps}
                                                                    {...provided.dragHandleProps} className={data.divActive ? ' srll-item first-srll-item respactive' + ((data?.errorExist)  ? ' error-recipient-border' : '') : 'srll-item first-srll-item' + ((data?.errorExist)  ? ' error-recipient-border' : '')} onClick={() => { activateRecipient(data,data.divActive, index) }}>
                                                                        <div className="top_wrrp top_wrrp_othr">
                                                                            <div className="headl">
                                                                                {!data.isEditEnable ?
                                                                                    (<>
                                                                                        <h5>{data?.FullName}</h5>
                                                                                        <p className="mail_txt">{data?.EmailAddress}</p>

                                                                                    </>)
                                                                                    :
                                                                                    (<span>
                                                                                        <div className='flex' style={{ display: 'flex' }}>
                                                                                            <input type="text" className={(data?.errorFirstNameExist) ? 'highlight-recipient-error docinput1' : 'docinput1'} name="FirstName" id="FirstName"
                                                                                                autoComplete="off" placeholder="First Name" value={data.FirstName}
                                                                                                onChange={(event) => {
                                                                                                    step2Data.Recipients[index].FirstName = event.target.value ?  event.target.value[0].toUpperCase() + event.target.value.slice(1) : '';
                                                                                                    step2Data.Recipients[index].errorFirstNameExist = false;
                                                                                                    step2Data.Recipients[index].FullName = event.target.value + ' ' + step2Data.Recipients[index].LastName;
                                                                                                    setStep2Data({ ...step2Data })
                                                                                                }} />


                                                                                            <input type="text" name="LastName" id="LastName" className={(data?.errorLastNameExist) ? 'highlight-recipient-error docinput1' : 'docinput1'}
                                                                                                autoComplete="off" placeholder="Last Name" value={data.LastName}
                                                                                                onChange={(event) => {
                                                                                                    data.LastName = event.target.value ?  event.target.value[0].toUpperCase() + event.target.value.slice(1) : '';
                                                                                                        step2Data.Recipients[index].LastName = event.target.value ?  event.target.value[0].toUpperCase() + event.target.value.slice(1) : '';
                                                                                                    step2Data.Recipients[index].errorLastNameExist = false;
                                                                                                    step2Data.Recipients[index].FullName = step2Data.Recipients[index].FirstName + ' ' + event.target.value,
                                                                                                        setStep2Data({ ...step2Data })
                                                                                                }} />

                                                                                        </div>
                                                                                        <input onBlur={(event) => {setUserProfileData(event.target.value, data, index)}} className={(data?.errorEmailAddressExist) ? 'highlight-recipient-error' : ''} type="email" name="EmailAddress" id="EmailAddress" value={data.EmailAddress} autoComplete="nope" placeholder="Email Address"
                                                                                            onChange={(event) => {
                                                                                                data.EmailAddress = event.target.value,
                                                                                                    step2Data.Recipients[index].errorEmailAddressExist = false;
                                                                                                step2Data.Recipients[index].EmailAddress = event.target.value, setStep2Data({ ...step2Data })
                                                                                            }} />

                                                                                    </span>)
                                                                                }
                                                                                  {whatsup === true && <p style={{color:"#9595f1",marginTop:3}}>Ensure valid mobile no for WhatsApp notification.</p>}
                                                                                {(((step2Data?.IsSecured && !data.IsInPerson && !data.IsNotarizer && !data?.IsFacialRecognition && !data.IsPhotoRecognition) || data?.IsAadhaarKYC ) && (!data?.IsCCEnabled)) || whatsup === true ? <div className="row doc_prepare_resp_country_phone">
                                                                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 selectpad doc_prepare_resp_country">
                                                                                        <div className="form-group">
                                                                                            <div className="input-wrapper">
                                                                                                <select name="CountryCode" style={{color:'#6c757d'}} value={(data.CountryId) ? (data.CountryId) : ''} className={(data?.errorConuntryCodeExist) ? 'highlight-recipient-error hlfinput' : 'hlfinput'}
                                                                                                    onChange={(event) => { data.CountryId = event.target.value, step2Data.Recipients[index].CountryId = event.target.value, step2Data.Recipients[index].errorConuntryCodeExist = false, setStep2Data({ ...step2Data }) }} >
                                                                                                    <option value={''} disabled hidden>Country Code</option>
                                                                                                    {countryCodesArray.map(obj => <option value={obj.CountryId} key={obj.CountryId}>{obj.CountryCode + ' (+' + obj.PhoneCode + ')'}</option>)}
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 selectpad doc_prepare_resp_phone">
                                                                                        <div className="form-group">
                                                                                            <div className="input-wrapper">
                                                                                                <input  autoComplete="nope" type="tel" maxLength={15} value={(data?.MobileNumber) ? data.MobileNumber : ''} className={(data?.errorMobileNumberExist) ? 'highlight-recipient-error' : ''} id="phone" name="Phone"
                                                                                                    onChange={(event) => {
                                                                                                        let numberValid = /^(\+\d{1,3}[- ]?)?\d{0,15}$/
                                                                                                        if (numberValid.test(event.target.value)) {
                                                                                                            data.MobileNumber = event.target.value
                                                                                                            step2Data.Recipients[index].errorMobileNumberExist = false,
                                                                                                                step2Data.Recipients[index].MobileNumber = event.target.value, setStep2Data({ ...step2Data })
                                                                                                        } else {
                                                                                                            return false;
                                                                                                        }
                                                                                                    }

                                                                                                    }  placeholder='Phone' />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                  {data?.IsAadhaarKYC ? <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 selectpad doc_prepare_resp_country">
                                                                                        <div className="form-group">
                                                                                            <div className="input-wrapper">
                                                                                            <Calendar placeholder={data?.DOB !== ""?data?.DOB : "DOB"}   readOnlyInput  maxDate={new Date()}  value={(data?.DOB) ? (data?.DOB): ''}  className={(data?.errorDOBExist) ? 'highlight-recipient-error' : ''}  id="dob" name="DOB" onChange={(event) => {
                                                                                                        dobonchange(event,index)}}  autoComplete="off" dateFormat="dd-mm-yy" showIcon  />
                                                                                                {/* <Calendar readOnlyInput maxDate={new Date()}  value={(data?.DOB) ? new Date(data.DOB): ''}  className={(data?.errorDOBExist) ? 'highlight-recipient-error' : ''}  id="dob" name="DOB" onChange={(event) => {
                                                                                                     step2Data.Recipients[index].errorDOBExist = false,
                                                                                                     step2Data.Recipients[index].DOB = moment(event.target.value).format('MM-DD-YYYY'),
                                                                                                      setStep2Data({...step2Data})

                                                                                                }}  autoComplete="nope" placeholder='DOB'/> */}
                                                                                                <br/>
                                                                                                 </div>
                                                                                            <p style={{color:"green",marginTop:10}}>Enter DOB as per Aadhaar Card.</p>
                                                                                        </div>
                                        
                                                                                    </div> : null}
                                                                                </div> : null}
                                                                             </div>
                                                                            <div className="asd">
                                                                                <select className="hlfinput" disabled={!step2Data.IsParallelSigning || (data?.IsCCEnabled)|| (data?.IsFacialRecognition || data?.IsPhotoRecognition) || RecipientPersonHaveDSC || data?.IsAadhaarKYC} value={data?.recipientType} onChange={(event) => {
                                                                                    data.recipientType = event.target.value;
                                                                                    data.IsInPerson = false;
                                                                                    data.IsNotarizer = false;
                                                                                    if (event.target.value == 'inPerson') {
                                                                                        data.IsInPerson = true;
                                                                                        data.IsNotarizer = false;
                                                                                        setInPersonCheckBoxes(data, index)
                                                                                    } else if (event.target.value == 'isNotarizer') {
                                                                                        data.IsInPerson = false;
                                                                                        data.IsNotarizer = true;
                                                                                    }
                                                                                    step2Data.Recipients[index].recipientType;
                                                                                    setStep2Data({ ...step2Data });
                                                                                }} >
                                                                                    <option value={''} >Select</option>
                                                                                    <option value={'inPerson'} key={'inPerson'}>In Person</option>
                                                                                    <option value={'isNotarizer'} key={'isNotarizer'}>Notary</option>
                                                                                </select>
                                                                                <div className="chos-icon">
                                                                                    <>
                                                                                    <img data-tip data-for="Delete" style={{ marginRight: '0.5rem' }} src="src/images/delete_icon.svg" alt=""
                                                                                        onClick={(event) => {
                                                                                            setDeleteRecipient(data), setModalShow(true);
                                                                                        }} />
                                                                                    <ReactTooltip id="Delete" place="top" effect="solid">
                                                                                        Delete
                                                                                    </ReactTooltip>
                                                                                    </>
                                                                                    <>
                                                                                    <img data-tip data-for="Edit" src="src/images/edit_icon.svg" alt="" onClick={(event) => {
                                                                                        ReactTooltip.hide()
                                                                                        if (data.isEditEnable) {
                                                                                            if(data.previousPosition && data.Position && data.NewPosition && data.previousPosition != data.NewPosition){
                                                                                               const respResultArray = step2Data.Recipients.filter(resp => !resp.IsDelete);
                                                                                                if(data.NewPosition <= respResultArray.length) {
                                                                                                const [removed] = step2Data.Recipients.splice(index, 1);
                                                                                                removed.Position = removed.NewPosition
                                                                                                step2Data.Recipients.splice(removed.NewPosition -1, 0, removed);
                                                                                                step2Data.Recipients[removed.NewPosition -1].isEditEnable = false;
                                                                                                getProperRecipientDeleteandPosition();
                                                                                                }
                                                                                            }
                                                                                            step2Data.Recipients[index].isEditEnable = false;
                                                                                            setStep2Data({ ...step2Data })
                                                                                        } else {
                                                                                            step2Data.Recipients[index].previousPosition = step2Data.Recipients[index].Position;
                                                                                            step2Data.Recipients[index].NewPosition = step2Data.Recipients[index].Position;
                                                                                            step2Data.Recipients[index].isEditEnable = true;
                                                                                            setStep2Data({ ...step2Data })
                                                                                        }
                                                                                            
                                                                                    }} />
                                                                                    <ReactTooltip id="Edit" place="top" effect="solid">
                                                                                        Edit
                                                                                    </ReactTooltip>
                                                                                    </>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="pageing-re pageing-re_othr d-flex">
                                                                            <div className='pages-adjust-width'>
                                                                                <div className='page_number threedots' data-tip data-for="pages">Pages: {data?.RecipientPageNumbers?.map((pagenumber, index) => {
                                                                                    if (index == (data.RecipientPageNumbers.length - 1)) {
                                                                                        return <span data-tip data-for={`Pages${data.Index}`}>{pagenumber}</span>
                                                                                    }
                                                                                    else {
                                                                                        return <span data-tip data-for={`Pages${data.Index}`}>{pagenumber}, </span>
                                                                                    }
                                                                                }
                                                                                )}
                                                                                    <ReactTooltip id={`Pages${data.Index}`} place="top" effect="solid">
                                                                                        {data?.RecipientPageNumbers?.map((pagenumber, index) => {
                                                                                            if (index == (data.RecipientPageNumbers.length - 1)) {
                                                                                                return <>{pagenumber}</>
                                                                                            }
                                                                                            else {
                                                                                                return <>{pagenumber}, </>
                                                                                            }
                                                                                        }
                                                                                        )}
                                                                                    </ReactTooltip>
                                                                                </div>
                                                                            </div>
                                                                            {step2Data?.IsParallelSigning ? null : <div className={(data?.errorPositionExist) ? 'highlight-recipient-error page-count count-position paging-count d-flex' : 'page-count count-position paging-count d-flex'}>
                                                                                Position:  {(data?.isEditEnable) ? <input className='position-box ml-2'  type="text" id="Position" value= {data?.NewPosition} name="Position" placeholder=''
                                                                                onChange={(event) => {step2Data.Recipients[index].NewPosition = event.target.value; setStep2Data({ ...step2Data }) }}
                                                                                />
                                                                                :   <span className='ml-2'> {data?.Position} </span>}
                                                                            </div>}
                                                                        </div>
                                                                        <div className="frt-tgl flex_wrap_wrap">
                                                                            <div className="item">
                                                                                <h6>Signatory</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" checked={data.IsSignatory} disabled={data?.IsCCEnabled || RecipientPersonHaveDSC || data.Position > lastpositionofAadharEsign} onChange={(event) => { changeRecipientCardSignatory(event, index, data) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div>
                                                                            {(Enabledsc===1 ||Enabledsc === true)?<div className="item">
                                                                            <div style={{display:'flex'}}> <h6>DSC</h6> <img style={{marginLeft: '5px', marginBottom: '6px'}} data-tip data-for="OwnerDSC" src="src/images/info.svg" alt='' /></div>
                                                                                    <ReactTooltip id="OwnerDSC" place="top" effect="solid">
                                                                                        <div>Digital Signature Certificate issued by E-Mudra</div>
                                                                                    </ReactTooltip>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" disabled={(IsAnyoneEnableAadharEsign == true|| data.IsAadhaarEsign == 1 || data.IsAadhaarEsign == true)?true:false} checked={data.IsDSC} onChange={(event) => { selectDSC(event, index) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div>:""}
                                                                            {(EnableAadharekyc===1 ||EnableAadharekyc === true)?<div className="item">
                                                                            <div style={{display:'flex'}}> <h6>Aadhar</h6> <img style={{marginLeft: '5px', marginBottom: '6px'}} data-tip data-for="OwnerAadhar" src="src/images/info.svg" alt='' /></div>
                                                                                    <ReactTooltip id="OwnerAadhar" place="top" effect="solid">
                                                                                    <div>Aadhar e-Sign Signature Certificate issued by NSDL</div>
                                                                                    </ReactTooltip>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" disabled={(data.IsDSC === 1 || data.IsDSC == true|| RecipientPersonHaveDSC)?true:false } checked={data.IsAadhaarEsign} onChange={(event) => { selectAadhar(event, index) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div>:""}
                                                                            {/* <div className="item">
                                                                                <h6>eKYC</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" checked={data?.IsAadhaarKYC}  onChange={(event) => { selecteKYC(event, index) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div> */}
                                                                            <div className="item">
                                                                                <h6>Location Tracking</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" checked={data?.IsLocationTracking} disabled={data?.IsCCEnabled ||RecipientPersonHaveDSC|| data?.IsAadhaarKYC|| data.Position > lastpositionofAadharEsign || data.IsAadhaarEsign === 1 || data.IsAadhaarEsign === true } onChange={(event) => { selectLocationTracking(event, index) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div>
                                                                            <div className="item">
                                                                                <h6>CC</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" checked={data?.IsCCEnabled} onChange={(event) => { ccgetvalue(event, index, data) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div>
                                                                           { userData && userData.company_id ?
                                                                            <> <div className="item">
                                                                                <h6>Facial Recognition</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" checked={data?.IsFacialRecognition} disabled={data?.IsCCEnabled || RecipientPersonHaveDSC|| data?.IsAadhaarKYC || data.Position > lastpositionofAadharEsign || data.IsAadhaarEsign === 1 || data.IsAadhaarEsign === true} onChange={(event) => { selectFacialRecognition(event, index) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div>
                                                                            <div className="item">
                                                                                <h6>Photo Verification</h6>
                                                                                <label className="switch">
                                                                                    <input type="checkbox" checked={data?.IsPhotoRecognition} disabled={data?.IsCCEnabled || RecipientPersonHaveDSC || data?.IsAadhaarKYC || data.Position > lastpositionofAadharEsign || data.IsAadhaarEsign === 1 || data.IsAadhaarEsign === true } onChange={(event) => { selectIsPhotoRecognition(event, index) }} />
                                                                                    <span className="slider round"></span>
                                                                                </label>
                                                                            </div></>:null}
                                                                        </div>
                                                                    </div>
                                                                    )}
                                                                    </Draggable> : null))
                                                                })}

                                                            </div>
                                                             )}
                                                             </Droppable>
                                                           </DragDropContext>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className='row'>
                                                    <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                        <div className="slide-tab-btn">
                                                            {count > 0 && <button className="btn prev-btn" onClick={() => { backToDashboard(), localStorage.setItem("addDocumentPage", count - 1) }}>BACK</button>}
                                                            {count <= 3 && <button className="btn next-btn" onClick={() => { redirectToNext(), localStorage.setItem("addDocumentPage", count + 1) }}>NEXT</button>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* -------- recipients Delete PopUp Start--------------*/}

            <Modal show={modalShow} onHide={() => { setModalShow(false), setDeleteRecipient({}) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='delete-modal-content'>
                    {(deleteRecipient?.Controls && deleteRecipient?.Controls?.length > 0) ? (<p>
                        Would you like to delete the recipient <b>{deleteRecipient?.FullName}</b> ? All placed fields will be lost
                    </p>) :
                        (<p>
                            Would you like to delete the recipient <b>{deleteRecipient?.FullName}</b>?
                        </p>                       
                        )}
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    <Button variant="secondary" onClick={() => { setModalShow(false), setDeleteRecipient({}) }}>CANCEL</Button>
                    <Button className="button_modal_ok" onClick={() => { deleteRecipientOk() }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            {/* -------- Submit All fields Validation PopUp Start--------------*/}
            <Modal show={validationExist} onHide={() => { setValidationExist(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='delete-modal-content'>
                    <h3 className="alignment-text-modal">Some of the recipients have validation issues. Such recipients are
                        marked with red border.
                    </h3>
                    <br />
                    <div><p style={{textAlign: 'left'}}>Following might be the possible reasons :</p></div>
                    <ul className="alignment-text-modal">
                        
                        <li>Email Address/First Name/Last Name/Mobile Number/Country Code/DOB might be missing</li>
                        <li>Email Address might be duplicated</li>
                        <li>Email Address might be invalid</li>
                        <li>Mobile Number might be duplicated</li>
                        <li>Permissions for pages are not granted (Atleast one page should be assigned)</li>
                    </ul>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    {/* <Button variant="secondary" onClick={() => { setModalShow(false), setDeleteRecipient({}) }}>Cancel</Button> */}
                    <Button className="button_modal_ok" onClick={() => { setValidationExist(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            {/* -------- Contacts PopUp Start--------------*/}
            <Modal show={contactsModalShow} onHide={() => { setContactsModalShow(false), setSelectedContacts([])}}
                size="lg"
                dialogClassName="doc_select_contacts_modal"
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Select Contact
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                <ContactsPopTable selectedContacts={selectedContacts} setSelectedContacts={setSelectedContacts} />
                    {/* <DataTableComponent searchEvent={getContactSerchValue}
                        headers={columnHeaderService.documentSelectContactHeaders()}
                        tableData={contactsData} isCheckbocEnable={true} isMultiCheckbocEnable={false} /> */}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => {
                        setselectedcontactasrecipients()
                        setmainselectedContacts([...selectedContacts])
                      }}>ADD</Button>
                    <Button variant="secondary" onClick={() => {
                         setSelectedContacts([]),
                        // setSelectedContacts([...mainselectedContacts])
                        //  setselectedcontactasrecipients()
                          setContactsModalShow(false) }}>CLOSE</Button>
                </Modal.Footer>
            </Modal>
            {/* <ContactsAddModal
                show={contactsModalShow}
                onHide={(event) => { setContactsModalShow(false) }}

            /> */}
            <Modal show={contactsNotSelected} onHide={() => { setContactsNotSelected(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='delete-modal-content'>
                    <p>Please select atleast one record!</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    <Button variant="primary" onClick={() => { setContactsNotSelected(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={contactsEmailExist} onHide={() => { setContactsEmailExist(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='dsc-modal-content'>
                    <p>({ (duplicateEmailRespients && duplicateEmailRespients.length > 0) ? duplicateEmailRespients && duplicateEmailRespients?.map((person, index) => {
                                            if (index == (duplicateEmailRespients.length - 1)) {
                                                return <> {person.FirstName + ' ' + person.LastName}</>
                                            }
                                            else {
                                                return <> {person.FirstName + ' ' + person.LastName}, </>
                                            }
                                        })
                                     : null}) Email {duplicateEmailRespients.length > 1 ? 'Addresses' : 'Address' } already exist</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    <Button variant="primary" onClick={() => { setContactsEmailExist(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showSignatoryControlPopUp} onHide={() => { setShowSignatoryControlPopUp(false)}}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='delete-modal-content'>
                <p style={{fontSize: '16px', lineHeight:'1.5'}}>Would you like to unselect Signatory for the recipient <b>{signatoryRecipientData?.FullName}</b> ? All placed Signatory controls will be lost.</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    {/* <Button variant="primary" onClick={() => { setShowSignatoryControlPopUp(false) }}>CANCEL</Button>
                    <Button variant="primary" onClick={() => { changeSignatorypopupSign() }}>OKAY</Button> */}
                    <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { setShowSignatoryControlPopUp(false) }}
                        className='submit_cancel_btn'>CANCEL</Button>
                    <Button  className='submit_send_btn' style={{ width: '7rem',background: '#F06B30', border: 'none'}} onClick={() => { changeSignatorypopupSign() }}
                    >OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showdsccontrollesremovemessage} onHide={() => { setshowdsccontrollesremovemessage(false)}}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='delete-modal-content'>
                <p style={{fontSize: '16px', lineHeight:'1.5'}}>Would you like to unselect DSC for the recipient <b>{signatoryRecipientData?.FullName}</b> ? All placed DSC  controls will be lost.</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    {/* <Button variant="primary" onClick={() => { setShowSignatoryControlPopUp(false) }}>CANCEL</Button>
                    <Button variant="primary" onClick={() => { changeSignatorypopupSign() }}>OKAY</Button> */}
                    <Button variant="secondary" style={{ width: '7rem' }} onClick={() => { setshowdsccontrollesremovemessage(false) }}
                        className='submit_cancel_btn'>CANCEL</Button>
                    <Button  className='submit_send_btn' style={{ width: '7rem',background: '#F06B30', border: 'none'}} onClick={() => { conformtooffdsctoggle() }}
                    >OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={isOwnerPositionLastPopup} onHide={() => { setIsOwnerPositionLastPopup(false)}}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body>
                <p style={{fontSize: '16px', lineHeight:'1.5'}}>Based on the user preferences the position of the intiator has been moved to the last position</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    <Button  className='submit_send_btn' style={{ width: '7rem',background: '#F06B30', border: 'none'}} onClick={() => {setIsOwnerPositionLastPopup(false) }}
                    >OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showFacialPopUP} onHide={() => {  setShowFacialPopUP(false) }}
                size=""
                dialogClassName=""
                // backdrop="static"
                // keyboard={false}
                aria-labelledby="example-modal-sizes-title-lg"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                    {/* <Modal.Title id="example-modal-sizes-title-lg" style={{}}>
                        Success
                    </Modal.Title> */}
                </Modal.Header>
                <Modal.Body style={{ height: facialWithNoProfilePic ? '140px': '230px', overflow: "hidden auto" }} >
                    <div className="container-fluid">
                        <div className="">
                            <div className="example-card ">
                                <div className="row">
                                    <div className="col-md-12 add-recipients-div">
                                        <p style={{ lineHeight: '1.5', padding: '6px' }}>Due to non-availability of reference image of the recipients ({
                                        (onlyNonFacialImageResp && onlyNonFacialImageResp.length > 0) ?
                                        onlyNonFacialImageResp && onlyNonFacialImageResp?.map((person, index) => {
                                            if (index == (noFacialImageResp.length - 1)) {
                                                return <> {person.FirstName + ' ' + person.LastName}</>
                                            }
                                            else {
                                                return <> {person.FirstName + ' ' + person.LastName}, </>
                                            }
                                        }) : noFacialImageResp && noFacialImageResp?.map((person, index) => {
                                            if (index == (noFacialImageResp.length - 1)) {
                                                return <> {person.FirstName + ' ' + person.LastName}</>
                                            }
                                            else {
                                                return <> {person.FirstName + ' ' + person.LastName}, </>
                                            }
                                        })}), the {facialErrorExist && photoErrorExist && !facialWithNoProfilePic ? 'Facial Recognition and photo verification' : !facialWithNoProfilePic && photoErrorExist ? 'photo verification' : 'Facial Recognition'} algorithm cannot be applied.</p>
                                        {!facialWithNoProfilePic ? <p style={{ padding: '6px', lineHeight:'1.5' }}>Hence,the system prints the actual image capured during signing process.</p> : null }
                                        { (facialErrorExist && facialWithNoProfilePic) ? null :  <p style={{ padding: '6px'}}>Click OKAY to proceed.</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className='justify-content-center' style={{border: 'none'}}>
                   { (facialErrorExist && facialWithNoProfilePic) ? null :  <Button variant="secondary" style={{ background: '#F06B30', border: 'none', }} onClick={() => { submitDoc() }}>OKAY</Button>}
                </Modal.Footer>
            </Modal>

            <Modal show={DSCUsersValidPopup} onHide={() => { setDSCUsersValidPopup(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='dsc-modal-content'>
                    <p>Enabling of DSC is allowed only for 5 recipients.</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    <Button variant="primary" onClick={() => { setDSCUsersValidPopup(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showmorethanfiverecipientsnotallowed} onHide={() => { setshowmorethanfiverecipientsnotallowed(false) }}
                size=""
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header className='modal-header-border-none' closeButton>
                </Modal.Header>
                <Modal.Body className='dsc-modal-content'>
                    <p>Enabling of Aadhar is allowed only for 5 recipients.</p>
                </Modal.Body>
                <Modal.Footer className='modal-footer-border-none justify-content-center'>
                    <Button variant="primary" onClick={() => { setshowmorethanfiverecipientsnotallowed(false) }}>OKAY</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export { DocumentSelect };
function ContactsAddModal(props) {

    return (
        <Modal
            {...props}
            size="lg"
            dialogClassName="my-modal"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Select Contact
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <DataTableComponent />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={props.onHide}>SUBMIT</Button>
                <Button variant="secondary" onClick={props.onHide}>CLOSE</Button>
            </Modal.Footer>
        </Modal>
    );
}
