import React, { useEffect, useState,useRef } from "react";
import '../../css/controls.css'
import { Rnd } from "react-rnd";
import SettingsIcon from '@mui/icons-material/Settings';
import { ContextMenu, MenuItem, ContextMenuTrigger, hideMenu } from "react-contextmenu";
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import ContentCutIcon from '@mui/icons-material/ContentCut';
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import ContentPasteGoIcon from '@mui/icons-material/ContentPasteGo';
import UndoIcon from '@mui/icons-material/Undo';
import DeleteIcon from '@mui/icons-material/Delete';
import RedoIcon from '@mui/icons-material/Redo';
import ReactTooltip from 'react-tooltip';
import ForwardIcon from '@mui/icons-material/Forward';


export default function Textbox({ index, updateControl, IsDragDisabled, ControlObj, OnDragEnd, OnResizeEnd, IsResizeDisabled, onChangeValue, setControlPopupShow, selectedControlObj,deleteHandler, getSelectedControl,pageType, selectedRecipientObj,viewpage }) {
    const [textValue, setTextValue] = useState('');
    const [ControlSizes, setControlSizes] = useState({Width:155, Height:30});
    const [doubleClickEnable, setDoubleClickEnable] = useState(false);
    const changeValueTimeOut = useRef(null);
    const changesizeTimeOut = useRef(null);

  function getchangeValue(event) {
    setTextValue(event)
    changeValueUpdate(event)
  }

function changeValueUpdate(event) {
  if(changeValueTimeOut.current) {
    clearTimeout(changeValueTimeOut.current)
  }
  changeValueTimeOut.current= setTimeout(() => {
  onChangeValue(ControlObj, event)
}, 700);
}
function sizeAutoAdject(ref) {
  if(changesizeTimeOut.current) {
    clearTimeout(changesizeTimeOut.current)
  }
  changesizeTimeOut.current= setTimeout(() => {
    OnResizeEnd({}, {}, {}, ref, {}, ControlObj)
}, 500);
}


    useEffect(() => {
        const textAreaData = ControlObj.Value ? ControlObj.Value : ''
        setTextValue(textAreaData)
        setControlSizes(ControlObj)
    }, [])
    function getDragPositionAndMouseEvent(e, d) {
        OnDragEnd(e, d, ControlObj)
    }
    const controlHandler=()=>{
        setControlPopupShow(true)
    }
    const select=()=> {
      if(pageType && ControlObj.enableControl) {
        getSelectedControl(ControlObj)
        }
    }
   function getResizeAndReferenceControl(e, direction, ref, delta, position) {
    let Sizes = ControlSizes ? ControlSizes : ControlObj
    Sizes.Width = typeof ref.style.width == 'string'? Number(ref.style.width.replace('px', '')) : ref.style.width
    Sizes.Height =   typeof ref.style.height == 'string'? Number(ref.style.height.replace('px', '')) : ref.style.height
    setControlSizes(Sizes)
    OnResizeEnd(delta, e, direction, ref,position, ControlObj)
    }
    const resiableSettings = {
        top: false, right: true,
        bottom: true, left: false, topRight: false,
        bottomRight: true, bottomLeft: false, topLeft: false
    }
    useEffect(() => {
        // console.log(ControlObj)
    }, [ControlObj])
    const style = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        cursor: IsDragDisabled ? 'pointer' : 'move'
    };
    function getTextWidth(input, fontfamily=null) {
      // var html_org = input;
      
      let splitString = input.split("\n");
      let maxStrLen =Math.max(...(splitString.map(el => el.length)))
      let html_org=splitString.find((e)=> e.length==maxStrLen);

      var s = $('<span>'+html_org+'</span>');
     
      if (fontfamily) {
        $(s).addClass(fontfamily);
      }
      s.appendTo(document.body);
      var width = s.width();
      s.remove();
      return width;
  };

  function getTextHeight(input, fixedWidth) {
      var html_org = input;
      var s = $('<div>'+html_org+'</div>');
      s.appendTo(document.body);
      $(s).css('word-wrap','break-all');
      var height = s.height();
      s.remove();
      return height;
  };
  function autoheight(a) {
    if (!$(a).prop('scrollTop')) {
        do {
            var b = $(a).prop('scrollHeight');
            var h = $(a).height();
            $(a).height(h - 10);
        }
        while (b && (b != $(a).prop('scrollHeight')));
    }
    // $(a).height($(a).prop('scrollHeight'));
    
    return $(a).prop('scrollHeight');
};


function autosize(e) {
  console.log(e,"======");
  var pdf = document.getElementsByClassName('react-pdf__Page')[ControlObj.PageNumber - 1].firstChild
  var pdfwidth = Number(pdf.style.width.replace('px', ''))
  var pdfheight = Number(pdf.style.height.replace('px', ''))
  var totalWidth = pdfwidth - ControlObj.Left -10;
  var totalHeigth = pdfheight - ControlObj.Top -10;
  var textWidth = getTextWidth($('textarea[name='+ControlObj.CtrlId+']').val())
  if (e.target.value) {
    if ($('textarea[name='+ControlObj.CtrlId+']').val().split().length == 1 && totalWidth > textWidth + 20) {
      if(textWidth < 155){
        textWidth = 155
      }
      // $('textarea[name='+ControlObj.CtrlId+']').height(height);
        // $('textarea[name='+ControlObj.CtrlId+']').width(textWidth);
        
                var height = autoheight($('textarea[name='+ControlObj.CtrlId+']'));
                $('textarea[name='+ControlObj.CtrlId+']').height(height)
                $('textarea[name='+ControlObj.CtrlId+']').width(textWidth);
                const ref = {
                  style:{
                      height: ControlSizes.Height +'px',
                      width:textWidth +'px',
                  }
              }
  let Sizes = {}
  Sizes.Width = textWidth
  Sizes.Height = height
  setControlSizes(Sizes)
  sizeAutoAdject(ref)
  getchangeValue(e.target.value)
    } else {
      var textOrgWidth = 0;
      var splittextwidth;
      var textOrgHeight = 0;
      var text = ''
							for (var i = 0; i < $('textarea[name='+ControlObj.CtrlId+']').val().split().length; i++) {
								splittextwidth = $('textarea[name='+ControlObj.CtrlId+']').val().split('\n')[i];
								if (getTextWidth(splittextwidth) > textOrgWidth) {
									textOrgWidth = getTextWidth(splittextwidth);
								}
                // let Textform = text +  $('textarea[name='+ControlObj.CtrlId+']').val().split('\n')[i]
                // if (autoheight(splittextwidth) < totalHeigth) {
                //   if(i==0){
                //     text = $('textarea[name='+ControlObj.CtrlId+']').val().split('\n')[i]

                //   } else {
                //     text = $('textarea[name='+ControlObj.CtrlId+']').val().split('\n')[i-1]
                //   }
                  
                //   // textOrgHeight = autoheight(splittextwidth)
                //   // console.log($('textarea[name='+ControlObj.CtrlId+']').val().split('\n')[i-1])
                //   // break
                // }
							}
      // textOrgWidth = getTextWidth($('textarea[name='+ControlObj.CtrlId+']').val())
      if(textOrgWidth < 155) {
        textOrgWidth = 155
      }
      if (textOrgWidth >= totalWidth ) {
        textOrgWidth = totalWidth;
      }
       var height = autoheight($('textarea[name='+ControlObj.CtrlId+']'));
       $('textarea[name='+ControlObj.CtrlId+']').width(textOrgWidth);
       if (height >= totalHeigth) {
        // height = totalHeigth - 10
        getchangeValue(e.target.value)
       } else {
        getchangeValue(e.target.value)
      }
        $('textarea[name='+ControlObj.CtrlId+']').height(height)
        const ref = {
          style:{
              height:height +'px',
              width:textOrgWidth  +'px',
          }
      }
  let Sizes = {}
  Sizes.Width = textOrgWidth
  Sizes.Height = height
  setControlSizes(Sizes)
  sizeAutoAdject(ref)
  
    // }
    }
    } else {
      const ref = {
        style:{
            height:30 +'px',
            width:155 +'px',
        }
    }
      let Sizes = {}
      Sizes.Width = 155
      Sizes.Height = 30
      setControlSizes(Sizes)
      sizeAutoAdject(ref)
      getchangeValue(e.target.value)
    }


}

function outSideClick(event){
  // console.log(event, '&&&&')
  console.log(event, '&&&&')

  
}
function getdynamicResizel(e, direction, ref, delta, position){
  // let Sizes = ControlSizes ? ControlSizes : ControlObj
  // Sizes.Width = typeof ref.style.width == 'string'? Number(ref.style.width.replace('px', '')) : ref.style.width
  // Sizes.Height =   typeof ref.style.height == 'string'? Number(ref.style.height.replace('px', '')) : ref.style.height
  // setControlSizes(Sizes)
  }

    return (
        <>
        <ContextMenuTrigger  id="contextmenu" holdToDisplay={-1} >
            <Rnd onClick={(e) => { hideMenu(true), select()}} data-tip={ControlObj.CtrlId} data-for={ControlObj.CtrlId}
                style={style}
                minWidth={30}
                minHeight={20}
                // maxWidth={}
                // maxHeight={}
                className={ ((pageType && ControlObj.enableControl) || !pageType) ? (ControlObj?.MandatoryErrorExist) ? "control_common control_Mandatory_error_exist" : "control_common" + ' ' + ControlObj?.Color : "control_common" + ' disabled_control'}
                // bounds=".prepare-slide_pln"
            // default={{
            //     x: (ControlObj.LeftByPage) ? ControlObj.LeftByPage : 230,
            //     y: (ControlObj.TopByPage) ? ControlObj.LeftByPage : 227,
            //     width: (ControlObj.Width) ? ControlObj.Width : 75,
            //     height: (ControlObj.Height) ? ControlObj.Height : 45
            // }}
            size={{ width: ControlSizes?.Width, height: ControlSizes?.Height }}
            position={{ x: ControlObj.Left, y: ControlObj.Top }}
            onDragStop={(e, d) => { getDragPositionAndMouseEvent(e, d)}}
            onResizeStop={(e, direction, ref, delta, position) => {
                getResizeAndReferenceControl(e, direction, ref, delta, position)
            }}
            onResize={(e, direction, ref, delta, position) => {
              getdynamicResizel(e, direction, ref, delta, position)
          }}
                disableDragging={IsDragDisabled ? IsDragDisabled : false}
                enableResizing={IsResizeDisabled ? !IsResizeDisabled : resiableSettings}
            >
                {(selectedControlObj && selectedControlObj.CtrlId == ControlObj.CtrlId && selectedRecipientObj.RecipientId == ControlObj.RecipientId) && pageType ? <span className="forward_span"><ForwardIcon style={{}} className='forward_icon'/></span>: null}
              { ControlObj.IsMandatory ? <span className={"control_Mandatory"}>*</span>: null}
              {(((selectedRecipientObj && selectedRecipientObj.RecipientId == ControlObj.RecipientId) && pageType) || ControlObj.enableControl
               || (selectedRecipientObj && selectedRecipientObj.RecipientId == ControlObj.RecipientId && ControlObj.IsOwner)) && selectedControlObj && selectedControlObj.CtrlId == ControlObj.CtrlId ?
                <textarea onFocus={(e)=>autosize(e)} onBlur={outSideClick} placeholder="Text box" name={ControlObj.CtrlId} id={ControlObj.CtrlId} className="text_area_control" style={{width:ControlSizes?.Width, height:ControlSizes?.Height-2, overflowY:'hidden'}} value={textValue}
                 readOnly={(pageType && !ControlObj.enableControl)} onChange={(e)=>{autosize(e)}} ></textarea> : 
                 <textarea  name={ControlObj.CtrlId} id={ControlObj.CtrlId}  readOnly className="adjust_control_textarea" style={{width:ControlSizes?.Width-5, height:ControlSizes?.Height-2,wordBreak: 'break-all',overflowY:'hidden'}} value={textValue ? textValue: ' Text box' }></textarea>}
                {(selectedControlObj && (selectedControlObj.CtrlId === ControlObj.CtrlId) && ((pageType && ControlObj.enableControl) || !pageType)) ?
                <>
                <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot top_left control_Mandatory_error_exist' :"control_dot top_left" + ' ' + ControlObj?.Color}></span>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot top_right control_Mandatory_error_exist' :"control_dot top_right" + ' ' + ControlObj?.Color}></span>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot bottom_left control_Mandatory_error_exist' :"control_dot bottom_left" + ' ' + ControlObj?.Color}></span>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot bottom_right control_Mandatory_error_exist' :"control_dot bottom_right" + ' ' + ControlObj?.Color}></span>
                {!pageType ? <SettingsIcon onClick={()=>{controlHandler()}} className="setting" />: null}</> : null }
                {/* TextBox */}
                </Rnd>
                { ((pageType && ControlObj.enableControl) || !pageType || viewpage) ? null  : <ReactTooltip id={ControlObj.CtrlId} place="top" effect="solid">
               You don't have access to this control </ReactTooltip>}
        </ContextMenuTrigger>

        {ControlObj && selectedControlObj && ControlObj.CtrlId==selectedControlObj.CtrlId && !pageType ?

<ContextMenu id="contextmenu">
<MenuItem >
  <ContentCutIcon sx={{color:'blue'}}/><span>Cut</span>
</MenuItem>
<MenuItem >
<ContentCopyIcon sx={{color:'skyblue'}}/> <span>Copy</span>
</MenuItem>
<MenuItem >
  <ContentPasteIcon sx={{color:'green'}}/><span>Paste</span>
</MenuItem>
<MenuItem>
 <ContentPasteGoIcon sx={{color:'rebeccapurple'}}/> <span>Paste To Location</span>
</MenuItem>
<MenuItem>
  <UndoIcon sx={{color:'rgb(41, 230, 141)'}}/><span>Undo</span>
</MenuItem>
<MenuItem>
  <RedoIcon sx={{color:'#F06B30'}}/><span>Redo</span>
</MenuItem>
<MenuItem onClick={ ()=>{if(ControlObj.CtrlId==selectedControlObj.CtrlId){deleteHandler()}}}>
  <DeleteIcon sx={{color:'red'}}/><span>Delete</span>
</MenuItem>
</ContextMenu>:null
}

        </>
    )
}