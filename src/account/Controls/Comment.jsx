import React, { useState, useEffect } from 'react';
import '../../css/controls.css'
import SmsIcon from '@mui/icons-material/Sms';
export default function Comment({ commentObj, selectedComment}) {
    const [enableborder, setenableborder] = useState(false);

    useEffect(() => {
            if (commentObj.AddCommentId != -1 && selectedComment.AddCommentId && commentObj.AddCommentId == selectedComment.AddCommentId) {
                setenableborder(true)
                console.log('true')
            } else if (commentObj.referId && selectedComment.referId && commentObj.referId == selectedComment.referId) {
                setenableborder(true)
                console.log('true')
            } else {
                setenableborder(false)
                console.log('false')
            }

    }, [selectedComment])
    return (
        <>
        
        <SmsIcon style={{left:commentObj?.Left - 15, top:commentObj?.Top - 35, border: enableborder ? '2px solid #ff00eb' : 'none'}} className="control_Comment"></SmsIcon>

        </>
    )



}
