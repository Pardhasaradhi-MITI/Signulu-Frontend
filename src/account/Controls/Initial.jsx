import React from "react";
import '../../css/controls.css'
import { Rnd } from "react-rnd";
import SettingsIcon from '@mui/icons-material/Settings';
import GetAppIcon from '@mui/icons-material/GetApp';
import { ContextMenu, MenuItem, ContextMenuTrigger, hideMenu } from "react-contextmenu";
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import ContentCutIcon from '@mui/icons-material/ContentCut';
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import ContentPasteGoIcon from '@mui/icons-material/ContentPasteGo';
import UndoIcon from '@mui/icons-material/Undo';
import DeleteIcon from '@mui/icons-material/Delete';
import RedoIcon from '@mui/icons-material/Redo';
import ReactTooltip from 'react-tooltip';
import ForwardIcon from '@mui/icons-material/Forward';

export default function Initial({ IsDragDisabled, ControlObj, OnDragEnd, OnResizeEnd, IsResizeDisabled, onChangeValue,setSignaturePopShow, selectedControlObj,deleteHandler, getSelectedControl,pageType, setControlPopupShow, selectedRecipientObj,viewpage }) {
    function getchangeValue(event) {
        onChangeValue(event)
    }
    function getDragPositionAndMouseEvent(e, d) {
        console.log({e, d})
        OnDragEnd(e, d, ControlObj)
    }
    const select=()=> {
        if(pageType && ControlObj.enableControl) {
        getSelectedControl(ControlObj)
        }
    }
   function getResizeAndReferenceControl(e, direction, ref, delta, position) {
    console.log({delta, e, direction, ref,position, ControlObj})
    OnResizeEnd(delta, e, direction, ref,position, ControlObj)
    }
    const style = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        cursor: IsDragDisabled ? 'pointer' : 'move'
    };
    const resiableSettings = {
        top: false, right: true,
        bottom: true, left: false, topRight: false,
        bottomRight: true, bottomLeft: false, topLeft: false
    }
    const controlHandler=()=>{
        if(!pageType && (selectedControlObj.IsOwner || selectedControlObj.IsInPerson) || pageType) {
            setSignaturePopShow(true)
        } else {
            setControlPopupShow(true)
        }
        
    }
    return (
        <>
        <ContextMenuTrigger  id="contextmenu" holdToDisplay={-1} >
        <Rnd onClick={(e) => { hideMenu(true), select()}} data-tip={ControlObj.CtrlId} data-for={ControlObj.CtrlId}
            style={style}
            className={ ((pageType && ControlObj.enableControl) || !pageType) ? (ControlObj?.MandatoryErrorExist) ? "control_common control_Mandatory_error_exist" : "control_common" + ' ' + ControlObj?.Color : "control_common" + ' disabled_control'}
            minWidth={30}
            minHeight={20}
            maxWidth={300}
            maxHeight={150}
            // bounds=".prepare-slide_pln"
            // default={{
            //     x: (ControlObj.LeftByPage) ? ControlObj.LeftByPage : 230,
            //     y: (ControlObj.TopByPage) ? ControlObj.LeftByPage : 227,
            //     width: (ControlObj.Width) ? ControlObj.Width : 75,
            //     height: (ControlObj.Height) ? ControlObj.Height : 45
            // }}
            size={{ width: ControlObj.Width, height: ControlObj.Height }}
            position={{ x: ControlObj.Left, y: ControlObj.Top }}
            onDragStop={(e, d) => { getDragPositionAndMouseEvent(e, d)}}
            onResizeStop={(e, direction, ref, delta, position) => {
                getResizeAndReferenceControl(e, direction, ref, delta, position)
            }}
            disableDragging={IsDragDisabled ? IsDragDisabled : false}
            enableResizing={IsResizeDisabled ? !IsResizeDisabled : resiableSettings}
        >
                {(selectedControlObj && selectedControlObj.CtrlId == ControlObj.CtrlId) && pageType ? <span className="forward_span"><ForwardIcon style={{}} className='forward_icon'/></span>: null}
             { ControlObj.IsMandatory ? <span className={"control_Mandatory"}>*</span>: null}
            {(selectedControlObj && (selectedControlObj.CtrlId === ControlObj.CtrlId) && ((pageType && ControlObj.enableControl) || !pageType)) ?
                <>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot top_left control_Mandatory_error_exist' :"control_dot top_left" + ' ' + ControlObj?.Color}></span>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot top_right control_Mandatory_error_exist' :"control_dot top_right" + ' ' + ControlObj?.Color}></span>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot bottom_left control_Mandatory_error_exist' :"control_dot bottom_left" + ' ' + ControlObj?.Color}></span>
            <span className={(ControlObj?.MandatoryErrorExist) ? 'control_dot bottom_right control_Mandatory_error_exist' :"control_dot bottom_right" + ' ' + ControlObj?.Color}></span>
                            <SettingsIcon className="setting" onClick={() => { controlHandler() }} /></> : null}
                            {/* <div className="Signature_intial_div" draggable={false} style={{width: ControlObj.Width, height: ControlObj.Height}} > */}

                    {(ControlObj.SignatureFont || ControlObj.InitialUpload)?
                        <>
                            {ControlObj.InitialUpload ? <img draggable={false} alt={`initial `} className='Signature_Img' src={ControlObj.InitialUpload} />
                                : null}
                            {ControlObj.SignatureFont ? <div  style={{width: ControlObj.Width-4, height: ControlObj.Height-2}} draggable={false} className={ControlObj.SignatureFont} > {ControlObj.SignatureInitial}</div> : null}
                        </> :
                        <>
                        <textarea placeholder="Intial" name={ControlObj.CtrlId} id={ControlObj.CtrlId}  readOnly className="adjust_control_textarea" style={{width:ControlObj.Width-4, height:ControlObj.Height-2,wordBreak: 'break-all'}}></textarea>
                            {/* <p> Intial</p>
                            <GetAppIcon /> */}
                        </>
                    }
                          {/* </div> */}
                          { ((pageType && ControlObj.enableControl) || !pageType || viewpage) ? null  : <ReactTooltip id={ControlObj.CtrlId} place="top" effect="solid">
               You don't have access to this control </ReactTooltip>}
        </Rnd>
        </ContextMenuTrigger>

        {ControlObj && selectedControlObj && ControlObj.CtrlId==selectedControlObj.CtrlId && !pageType?

<ContextMenu id="contextmenu">
<MenuItem >
  <ContentCutIcon sx={{color:'blue'}}/><span>Cut</span>
</MenuItem>
<MenuItem >
<ContentCopyIcon sx={{color:'skyblue'}}/> <span>Copy</span>
</MenuItem>
<MenuItem >
  <ContentPasteIcon sx={{color:'green'}}/><span>Paste</span>
</MenuItem>
<MenuItem>
 <ContentPasteGoIcon sx={{color:'rebeccapurple'}}/> <span>Paste To Location</span>
</MenuItem>
<MenuItem>
  <UndoIcon sx={{color:'rgb(41, 230, 141)'}}/><span>Undo</span>
</MenuItem>
<MenuItem>
  <RedoIcon sx={{color:'#F06B30'}}/><span>Redo</span>
</MenuItem>
<MenuItem onClick={ ()=>{if(ControlObj.CtrlId==selectedControlObj.CtrlId){deleteHandler()}}}>
  <DeleteIcon sx={{color:'red'}}/><span>Delete</span>
</MenuItem>
</ContextMenu>:null
}

        </>
    )
}