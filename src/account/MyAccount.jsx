import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import {commonService} from "../_services/common.service";
import {usersService} from "../_services/users.service";
import axios from "axios";
import {Avatar} from "@mui/material";
import camera from "../../src/images/camera - Copy.svg";
import config from 'config';
const API_END_POINT = config.serverUrl

function MyAccount({ history, location }) {
    const [result, setResult] = useState({})
    const [count, setcount] = useState(0)
    const [Phonenumber, setPhonenumber] = useState("")
    const [PhonenumberExt, setPhonenumberExt] = useState("")
    const [fax, setFax] = useState("")
    const [zipcode, setZipcode] = useState("")
    const [profilePic1, setProfilePic1] = useState(null);
    const [profilePic2, setProfilePic2] = useState(null);
    const [profilepicerror, setprofilepicerror] = useState('');

    const [error, setError] = useState({
        city: '',
        state: '',
        zipcode: '',
        phone:'',
        phoneExt: '',
        Fax: '',
        addressLine1: '',
        addressLine2: '',
        website: '',
    });

    useEffect(() => {
        companuDetails()
    }, [])

    function companuDetails() {
        usersService.getCompanyDetails()
            .then((result) => {
                    if (result) {
                        setResult(result.Data)
                        //   console.log(`${API_END_POINT}company/logo?fileName=${result.Data?.Logo}`)
                        if(result?.Data?.PhoneInfo[0].Phone !== null){
                            let temp=(result.Data && result.Data?.PhoneInfo  &&result.Data?.PhoneInfo[0].Phone).match(/\d+/g)
                            if(temp){
                            setPhonenumber(temp[0])}
                            let temp1=(result.Data && result.Data?.PhoneInfo  &&result.Data?.PhoneInfo[0].PhoneExt).match(/\d+/g)
                            if(temp1){
                                setPhonenumberExt(temp1[0])}
                        }
                        if(result?.Data?.PhoneInfo[1]!==undefined){
                        if(result?.Data?.PhoneInfo[1].Phone !== null){
                            let temp=(result.Data && result.Data?.PhoneInfo  &&result.Data?.PhoneInfo[1].Phone).replace(/"/g, '')
                            if(temp){
                                setFax(temp)}
                        }
                      }
                        let zip = result.Data.ZipCode.match(/\d+/g)
                        setZipcode(zip)
                        setProfilePic1(`${API_END_POINT}company/logo?fileName=${result.Data?.Logo}`)
                    }
                }
            )
    }

    function handleInputChange(field, e) {
          
        let temp = result
        let value = e.target.value
        let errors=error
        switch (field) {
            case "Address1":
                // if ( value === "") {
                //     errors.addressLine1 = 'Address Line #1 is required';
                //
                // } else
                    if(value.length < 3){
                    errors.addressLine1 = 'Address Line #1 must be atleast 3 characters long.';
                }else {
                    errors.addressLine1 = ''
                }
                temp.Address1 = value
                setResult(temp)
                setError(errors);
                setcount(count => count + 1)
                break;
            case "Address2":
                // if ( value === "") {
                //     errors.addressLine2 = 'Address Line #2 is required';
                //
                // } else
                    if(value.length < 3){
                    errors.addressLine2 = 'Address Line #2 must be atleast 3 characters long.';
                }else {
                    errors.addressLine2 = ''
                }
                temp.Address2 = value
                setResult(temp)
                setError(errors);
                setcount(count => count + 1)
                break;
            case "City":
                // if ( value === "") {
                //     errors.city = 'City is required';
                //
                // } else
                 if(value.length < 2){
                    errors.city = 'City must be atleast 2 characters long.';
                }else {
                    errors.city = ''
                }
                temp.City = value
                setResult(temp)
                setError(errors);
                setcount(count => count + 1)
                break;
            case "State":
                // if ( value === "") {
                //     errors.state = 'State is required';
                //
                // } else
                    if(value.length < 2){
                    errors.state = 'State must be atleast 2 characters long.';
                }else {
                    errors.state = ''
                }
                temp.State = value
                setResult(temp)
                setError(errors);
                setcount(count => count + 1)
                break;
            case "ZipCode":
                if (value.length < 5){
                    errors.zipcode = 'Zipcode must be least 5 characters long.';
                }
                else {
                    errors.zipcode = ''
                }
                if (value.length < 11){
                    temp.ZipCode = value
                    setResult(temp)
                    setZipcode(value)
                    setError(errors);
                    setcount(count => count + 1)
                }


                break;
            case "Phone":
                 if(value.length < 10){
                    errors.phone = 'Mobile Number must be least 10 number long.';
                }else {
                    errors.phone = ''
                }
                if (value.length < 16){
                    temp.PhoneInfo[0].Phone = value
                    setResult(temp)
                    setError(errors);
                    setPhonenumber(value)
                    setcount(count => count + 1)
                }
                break;
            case "PhoneExt":
               if(value.length <=4){
                temp.PhoneInfo[0].PhoneExt = value
                setResult(temp)
                setError(errors);
                setPhonenumberExt(value)
                setcount(count => count + 1)
            }
                break;
            case "Fax":
                if(temp.PhoneInfo.length>1){
                temp.PhoneInfo[1].Phone = value}
                setResult(temp)
                setFax(value)
                setcount(count => count + 1)
                break;
            case "WebSite":
                temp.WebSite = value
                setResult(temp)
                setcount(count => count + 1)
                break;
        }
    }

    const validation=()=>{
        
        let errors=error;
        let temp =result;
        if(temp.Address1 === ""){
            errors.addressLine1 ="Address #1 is required";
            return true
        }
        if(temp.Address2 === ""){
            errors.addressLine2 ="Address #2 is required";
            return true
        }
        if ( temp.PhoneInfo[0].Phone === "") {
            errors.phone = 'Phone is required';
            return true
        } else if(parseInt(temp.PhoneInfo[0].Phone).length > 15){
            errors.phone = 'Phone must be least 15 characters long.';
            return true
        }
        if(temp.ZipCode === ""){
            errors.zipcode = 'zipcode is required';
            return true
        }
        if(temp.City === ""){
            errors.zipcode = 'City is required';
            return true
        }
        if(temp.State === ""){
            errors.state = 'state is required';
            return true
        }
        if(temp.phoneExt === ""){
            errors.phoneExt = 'phoneExt is required';
            return true
        }
       return false
    }


    function uploadData(){
         //let valid = validation()
         let valid = false
         if(valid === false){
        
        document.body.classList.add('loading-indicator');
        let formData = new FormData();
        if(result?.PhoneInfo[0].Phone !== null){
        let phone11 =(result?.PhoneInfo[0].Phone).match(/\d+/g)
         if(phone11){
            result.PhoneInfo[0].Phone = phone11[0]
         }
          let phoneext = (result?.PhoneInfo[0].PhoneExt).match(/\d+/g)
          if(phoneext){
        result.PhoneInfo[0].PhoneExt = phoneext[0]}
          }
        let temp=(result?.ZipCode).match(/\d+/g)
        if(temp){
        result.ZipCode = temp[0]}
        
      
        formData.append("model",JSON.stringify(result) );
        formData.append("CompanyProfilePicture[]", profilePic2);
        let token = localStorage.getItem('LoginToken');
        let config;
        if (token !== null && token !== undefined && token !== "") {
            config = {
                headers: {
                    'Accept': 'application/json',
                    'userauthtoken': token
                }
            };
        } else {
            config = {
                headers: {
                    'Accept': 'application/json'
                }
            };
        }
        axios.post(`${API_END_POINT}company/profile`, formData, config)
            .then(function (response) {
                  
                result.Logo=response.data.Logo
                companuDetails()
                document.body.classList.remove('loading-indicator');

            })
            .catch(error => {

            });
        }

    }





    const handleProfilepic=(e)=> {
        debugger
        // let tmp_profile_data = {...profileDetail};
         let temp=result
        let reader = new FileReader();
        let file = e.target.files[0];
      
        if (file?.size > 1054464){
            setprofilepicerror("File size more, please upload a file with a size less than or equal to 1 MB")
            setcount(0)
            // setError(errors)
            return ""
        }
        else{
            setprofilepicerror("")
        reader.onload = function () {
            setProfilePic2(file);
             setProfilePic1(reader.result);
            temp.Logo=reader.result
            setResult(temp)
        
  // setImageChanged(1)
        }
        // tmp_profile_data['profile'] = {
        //     ...tmp_profile_data['profile'],
        //     profilePic: reader.result
        // }

        // setProfileDetail(tmp_profile_data);

        reader.readAsDataURL(file);
       // uploadData()
    }

    }

    function stringAvatar() {
          
        let name = result?.CompanyName ?result.CompanyName : 'N A'
        return {
            sx: {
                bgcolor: "#2F7FED",
                width:'175.38px',
                height:'175.38px',
                padding:'10px'
            },
            children: (name.split(' ') && name.split(' ')[0] && name.split(' ')[1]) ? `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`: 'NA',
        };
    }

    const deleteProfilePic=()=>{
        let temp =result
        setProfilePic1(null)
        temp.Logo=null
        setResult(temp)
    }


    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content accountprofile">
                    {/* <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <h3>Account Profile</h3>
                            </div>
                        </div>
                    </div> */}
                    <div className="main-content main-from-content signatures" style={{padding:"0 30px 30px 55px"}}>
                   <div className="row">
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        {/* <h3>Account Profile</h3> */}
                        <h3 style={{fontSize:35,fontWeight:300}}>Account Profile</h3>
						</div>
						<div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
						 <div className="recipient-btn"> 
                         <button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",width: "15px",height: "15px"}}/></button>
                            </div>
							</div>
						</div>
                      
                    <div className='row align-items-center'>
                        <div className='profile-info-content'>
                            <div className='row'>
                                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12 r-space'>
                                    <div className='profile-information'>
                                        <div className='user-profile-img'>

                                            <div className={'parent-profile-pic text-center parent-profile-piccc '}>
                                                {result?.Logo !== null || profilePic1 !== null  ?<img src={profilePic1} alt="" className="user-picture" /> : <Avatar {...stringAvatar()} />}
                                                {/*<div className={'mt-3 mb-2'} onClick={()=>deleteProfilePic()}> X </div>*/}
                                                <input className={"code uploadprofileimage"} id="file-input" type="file"
                                                       onChange={(e) => handleProfilepic(e)}/>
                                                <div className={'w-60 img_profile'}>
                                                    <label htmlFor="file-input" className={"parentcameracamer-1"}>
                                                        <img src={camera} className={"cameracamera"}/>
                                                        {/*Change Photo*/}
                                                    </label>
                                                    <lable className={"mt-2"} >Edit Profile Picture</lable>
                                                    {
                                                        profilepicerror !== "" &&
                                                        <p className={"error-msg"}>{profilepicerror}</p>
                                                    }
                                                </div>

                                            </div>

                                            {/*<img className='img-thumbnail' src='src/images/profile2.jpg' />*/}
                                            {/*<div class="crez-btn">*/}
                                            {/*    <input type="file" id="real-file" hidden="hidden" />*/}
                                            {/*    <button type="button" id="custom-button"><img src="src/images/camera.svg" alt="" /></button>*/}
                                            {/*</div>*/}
                                        </div>
                                        {/*<span class="user-name">Edit Profile Picture</span>*/}
                                    </div>
                                </div>
                                <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12 r-space'>
                                    <div className='tab-content'>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <h3>{result && result?.CompanyName}</h3>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea  value={result && result?.Address1} onChange={(e)=>handleInputChange('Address1',e)} />
                                                            <label for="">Address #1</label>
                                                        </div>
                                                        {
                                                            error.addressLine1 !== "" && <p className={'error-message'}>{error.addressLine1}</p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <textarea  value={result && result?.Address2} onChange={(e)=>handleInputChange('Address2',e)} />
                                                            <label for="">Address #2</label>
                                                        </div>
                                                        {
                                                            error.addressLine2 !== "" && <p className={'error-message'}>{error.addressLine2}</p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"   value={result && result?.City} onChange={(e)=>handleInputChange('City',e)} />
                                                            <label for="">City </label>
                                                        </div>
                                                        {
                                                            error.city !== "" && <p className={'error-message'}>{error.city}</p>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={result && result?.State} onChange={(e)=>handleInputChange('State',e)}/>
                                                            <label for="">State </label>
                                                        </div>
                                                        {
                                                            error.state !== "" && <p className={'error-message'}>{error.state}</p>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="on"  value={zipcode} onChange={(e)=>handleInputChange('ZipCode',e)} />
                                                            <label for="">Zip Code </label>
                                                        </div>
                                                        {
                                                            error.zipcode !== "" && <p className={'error-message'}>{error.zipcode}</p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='row'>
                                                <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="number" autocomplete="off" className={'m-0'}  value={Phonenumber} onChange={(e)=>handleInputChange('Phone',e)} />
                                                            <label for="">Mobile</label>
                                                        </div>
                                                        {
                                                            error.phone !== "" && <p className={'error-message'}>{error.phone}</p>
                                                        }
                                                    </div>
                                                </div>
                                                <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={result && result?.WebSite} onChange={(e)=>handleInputChange('WebSite',e)} />
                                                            <label for="">Website</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="number" autocomplete="off"  value={PhonenumberExt} onChange={(e)=>handleInputChange('PhoneExt',e)} />
                                                            <label for="">Phone Ext</label>
                                                        </div>
                                                        {
                                                            error.phoneExt !== "" && <p className={'error-message'}>{error.phoneExt}</p>
                                                        }
                                                    </div>
                                                </div> */}
                                            </div>
                                            <div className='row'>
                                                {/* <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12'>
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input type="text" autocomplete="off"  value={fax}  onChange={(e)=>handleInputChange('Fax',e)}  />
                                                            <label for="">Fax</label>
                                                        </div>
                                                    </div>
                                                </div> */}
                                                
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div class="subf_pagn_btn">
                                                    <button style={{color:"white",width:110,backgroundColor:"#2F7FED",border:"1px solid #2F7FED",display:"flex",justifyContent:"center"}} class="btn" onClick={()=>uploadData()}>SAVE</button>
                                                        {/* <button class="btn" onClick={() => uploadData()}>Save</button> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div> 
    )
}

export { MyAccount };