import React, {useEffect , useState} from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import {Range} from "react-range";
import {usersService} from "../_services/users.service";

function UserPreferencesss({ history, location }) {
    const [values, setValues] = React.useState([10])
    const [resultsUser,setResultsUser]=useState()
    const [resultsList,setResultsList]=useState()
    const [fullResults,setFullResults]=useState()
    const [count,setcount]=useState(0)
   const [sendNotificationArr,setsendNotificationArr]=useState([
       {
           No:1,
           Name:"SEND_NOTIFY_SELECT_ALL",
           Value:true
       },
       {
           No:2,
           Name:"SEND_ENVELOPE_APPROVED",
           Value:true
       },
       {
            No:3,
           Name:"SEND_ENVELOPE_REJECT",
           Value:true
       },
       {
           No:4,
           Name:"SEND_ENVELOPE_DELETE",
           Value:true
       }
   ])
    //Tabs
    function openTab(event, divID) {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }

    useEffect(() => {
        // getdSettingUser()
        getdSettingList()
    }, []);



    function getdSettingList(){
        usersService.getSettingList()
            .then((result) => {
                setResultsList(result.Entity)
                console.log('list',result)
                getdSettingUser(result.Entity)
            })
            .catch(error => {

            });
    }

    function getdSettingUser(result1){
        usersService.getSettingUser()
            .then((result) => {
                setResultsUser(result.Entity)
                console.log('user',result)
                compareListAndUser(result1,result.Entity)
            })
            .catch(error => {

            });
    }

    function compareListAndUser(result1,result2){

        let temp_list=result1
        let temp_user=result2



        // temp_list=temp_user
        setFullResults({...temp_list,...temp_user})
        setcount(count+1)

        for (let y=0 ; y< sendNotificationArr.length;y++){
            for (let x=0;x < result2.SENDING_NOTIFICATIONS.length;x++){
                if(sendNotificationArr[y].Name===result2.SENDING_NOTIFICATIONS[x].Code){
                       
                    let temp=sendNotificationArr
                    temp[y].Value=false
                    setsendNotificationArr(temp)
                }
            }
            setcount(count+1)
        }


        // sendNotificationArr.map((item,index)=>{
        //     console.log(index)
        //
        //
        //
        // })





        // console.log('final',{})
        //    
       // for(let x=0;x<result2.SENDING_NOTIFICATIONS.length;x++){
       //     for (let y=0;y<result1.SENDING_NOTIFICATIONS.length;y++){
       //         if (result2.SENDING_NOTIFICATIONS[x].Code===result1.SENDING_NOTIFICATIONS[y].Code){
       //             result1.SENDING_NOTIFICATIONS[x].BitValue=result2.SENDING_NOTIFICATIONS[y].BitValue
       //         }
       //     }
       // }
       //     
       // let temp_SenderNotification=result1.SENDING_NOTIFICATIONS
       //  let templ_list=fullResults
       // templ_list.SENDING_NOTIFICATIONS=temp_SenderNotification

    }

    const handleChanger=(field,e)=>{
           
        let temp=fullResults
        console.log(e)
        console.log(fullResults)
        switch (field){
            case "GENERALPREFERENCES":
                   
                console.log("false",!e)
                temp.GENERAL_PREFERENCES[0].BitValue=(!e)
                setFullResults(temp)
                setcount(count+1)
                break;
            case "WHATSAPP":
                   
                console.log("false",!e)
                temp.WHATSAPP[0].BitValue=(!e)
                setFullResults(temp)
                setcount(count+1)
                break;
            case "SENDER_SEQ_POSITION":
                   
                console.log("false",!e)
                temp.SENDER_SEQ_POSITION[0].BitValue=(!e)
                setFullResults(temp)
                setcount(count+1)
                break;
            case "DATE_FORMAT":
                   
                // let templist=temp.DATE_FORMAT
                if(templist.length>1){
                    if (e===0){
                        temp.DATE_FORMAT[1].BitValue=false;
                        temp.DATE_FORMAT[0].BitValue=true;

                        setFullResults(temp)
                        setcount(count+1)
                    }else if (e===1){
                        temp.DATE_FORMAT[0].BitValue=false;
                        temp.DATE_FORMAT[1].BitValue=true;

                        setFullResults(temp)
                        setcount(count+1)
                    }
                }else {
                    if (e===0){
                        temp.DATE_FORMAT[0].BitValue=true;
                        temp.DATE_FORMAT[0].TextValue="MM-DD-YYYY";
                        setFullResults(temp)
                        setcount(count+1)
                    }else {
                        temp.DATE_FORMAT[0].BitValue=true;
                        temp.DATE_FORMAT[0].TextValue="DD-MM-YYYY";
                        setFullResults(temp)
                        setcount(count+1)
                    }
                }
            case "SIGNATURE_SETTINGS":
                // let tempSignSet=temp.SIGNATURE_SETTINGS
                switch (e){
                    
                    case 0:
                    
                        temp.SIGNATURE_SETTINGS[1].BitValue=false
                        temp.SIGNATURE_SETTINGS[2].BitValue=false
                        temp.SIGNATURE_SETTINGS[3].BitValue=false
                        temp.SIGNATURE_SETTINGS[4].BitValue=false
                        temp.SIGNATURE_SETTINGS[5].BitValue=false
                        temp.SIGNATURE_SETTINGS[0].BitValue=0
                        setFullResults(temp)
                        setcount(count+1)
                        break;
                    case 1:
                        temp.SIGNATURE_SETTINGS[0].BitValue=false
                        temp.SIGNATURE_SETTINGS[2].BitValue=false
                        temp.SIGNATURE_SETTINGS[3].BitValue=false
                        temp.SIGNATURE_SETTINGS[4].BitValue=false
                        temp.SIGNATURE_SETTINGS[5].BitValue=false
                        temp.SIGNATURE_SETTINGS[1].BitValue=0
                        setFullResults(temp)
                        setcount(count+1)
                        break;
                    case 2:
                        temp.SIGNATURE_SETTINGS[1].BitValue=false
                        temp.SIGNATURE_SETTINGS[0].BitValue=false
                        temp.SIGNATURE_SETTINGS[3].BitValue=false
                        temp.SIGNATURE_SETTINGS[4].BitValue=false
                        temp.SIGNATURE_SETTINGS[5].BitValue=false
                        temp.SIGNATURE_SETTINGS[2].BitValue=0
                        setFullResults(temp)
                        setcount(count+1)
                        break;
                    case 3:
                        temp.SIGNATURE_SETTINGS[1].BitValue=false
                        temp.SIGNATURE_SETTINGS[2].BitValue=false
                        temp.SIGNATURE_SETTINGS[0].BitValue=false
                        temp.SIGNATURE_SETTINGS[4].BitValue=false
                        temp.SIGNATURE_SETTINGS[5].BitValue=false
                        temp.SIGNATURE_SETTINGS[3].BitValue=0
                        setFullResults(temp)
                        setcount(count+1)
                        break;
                    case 4:
                        temp.SIGNATURE_SETTINGS[1].BitValue=false
                        temp.SIGNATURE_SETTINGS[2].BitValue=false
                        temp.SIGNATURE_SETTINGS[3].BitValue=false
                        temp.SIGNATURE_SETTINGS[0].BitValue=false
                        temp.SIGNATURE_SETTINGS[5].BitValue=false
                        temp.SIGNATURE_SETTINGS[4].BitValue=0
                        setFullResults(temp)
                        setcount(count+1)
                        break;
                    case 5:
                        temp.SIGNATURE_SETTINGS[1].BitValue=false
                        temp.SIGNATURE_SETTINGS[2].BitValue=false
                        temp.SIGNATURE_SETTINGS[3].BitValue=false
                        temp.SIGNATURE_SETTINGS[4].BitValue=false
                        temp.SIGNATURE_SETTINGS[0].BitValue=false
                        temp.SIGNATURE_SETTINGS[5].BitValue=0
                        setFullResults(temp)
                        setcount(count+1)
                        break;
                }
                break
            case "SENDING_NOTIFICATIONS":

                break


        }
    }


    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main" id="main">
                <Topbar />

                <div className="main-content main-from-content userpreferences">
                    <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <h3>User Preferences</h3>
                            </div>
                        </div>
                    </div>

                    <div className='row align-items-center'>
                        <div className='references'>
                            <div className='tab'>
                                <ul className='navbar-tabs'>
                                    <li className="tablinks active" onClick={(e) => openTab(e, 'firstTab')}><button>General Preferences</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'secondTab')}><button>Email Preferences</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}><button>Date Format</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button>Signature Settings</button></li>
                                    <li className="tablinks" onClick={(e) => openTab(e, 'fifthTab')}><button>Signature Font Size</button></li>
                                </ul>
                            </div>
                            <div id="firstTab" className='tab-content'>
                                {/*<div className='heading'>*/}
                                {/*    <h4>GENERAL PREFERENCES</h4>*/}
                                {/*</div>*/}
                                <div className='check-section'>
                                    <h5>GENERAL PREFERENCES</h5>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check" >
                                                <input class="form-check-input" onClick={(e)=>handleChanger('GENERALPREFERENCES',(fullResults && fullResults.GENERAL_PREFERENCES[0].BitValue))} type="checkbox" checked={(fullResults && fullResults.GENERAL_PREFERENCES[0].BitValue)} id="GENERALPREFERENCES" />
                                                <label class="form-check-label" for="GENERALPREFERENCES">
                                                    Include audit trail in pdf
                                                </label>
                                            </div>
                                        </li>
                                      </ul>
                                </div>
                                <div className='check-section'>
                                    <h5>WHATSAPP NOTIFICATIONS</h5>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" onClick={(e)=>handleChanger('WHATSAPP',(fullResults && fullResults.WHATSAPP[0].BitValue))} checked={(fullResults && fullResults.WHATSAPP[0].BitValue)}  id="WHATSAPPNOTIFICATIONS" />
                                                <label class="form-check-label" for="WHATSAPPNOTIFICATIONS">
                                                    Enable
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <h5>DOCUMENT INITIATOR POSITION(DEFAULT POSITION IS NO.1)</h5>
                                    <ul className='checkList'>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="checkbox" onClick={(e)=>handleChanger('SENDER_SEQ_POSITION',(fullResults && fullResults.SENDER_SEQ_POSITION[0].BitValue))} checked={(fullResults && fullResults.SENDER_SEQ_POSITION[0].BitValue)}
                                                       id="DOCUMENTINITIATORPOSITION"/>
                                                <label className="form-check-label" htmlFor="DOCUMENTINITIATORPOSITION">
                                                    Move the default position of the sender to the last in the workflow
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div id="secondTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>Email Preferences</h4>
                                    <p>Set the default email notification preferences for new users. Users can modify these settings in their My Preferences.</p>
                                </div>
                                <div className='check-section'>
                                    <h5>Sender Notifications</h5>
                                    <p>Notify me when i am the sender and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked={sendNotificationArr[0].Value}/>
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked={sendNotificationArr[1].Value} />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked={sendNotificationArr[2].Value}/>
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    A signer tleenes to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked={sendNotificationArr[3].Value} />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the system
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className='check-section'>
                                    <p>Notify me when i am the recipient and:</p>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Select All
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"  />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    I have a document to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    When the document is completed
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"  />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Another signer declines to sign
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    The sender corrects a document
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Documents will be purged from the systern
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="thirdTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>DATE FORMAT</h4>
                                    <p>Apply the following formats to the 'Date Signed' field.</p>
                                </div>
                                <div className='check-section'>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check" >
                                                <input class="form-check-input" onClick={()=>handleChanger("DATE_FORMAT",0)} type="radio" checked={(fullResults && fullResults.DATE_FORMAT && fullResults.DATE_FORMAT[0].TextValue==="MM-DD-YYYY"  && fullResults.DATE_FORMAT[0].BitValue===true )} id="DATEFORMAT1" name={"DATEFORMAT"} />
                                                <label class="form-check-label" for="DATEFORMAT1">
                                                    MM-DD-YYYY
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" onClick={()=>handleChanger("DATE_FORMAT",1)} type="radio" checked={(fullResults && fullResults.DATE_FORMAT && fullResults.DATE_FORMAT.length >1 ? fullResults.DATE_FORMAT[1].TextValue==="DD-MM-YYYY" && fullResults.DATE_FORMAT[1].BitValue===true : fullResults && fullResults.DATE_FORMAT[0].TextValue==="DD-MM-YYYY"  && fullResults.DATE_FORMAT[0].BitValue===true)} id="DATEFORMAT2" name={"DATEFORMAT"}  />
                                                <label class="form-check-label" for="DATEFORMAT2">
                                                    DD-MM-YYYY
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>SIGNATURE SETTINGS</h4>
                                    <p>Customize your signature as per your requirement by using the below options.</p>
                                </div>
                                <div className='check-section'>
                                    <ul className='checkList'>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" value="" id="SIGNATURESETTINGS1" name={"SIGNATURESETTINGS"}
                                                onClick={()=>handleChanger("SIGNATURE_SETTINGS",0)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS[0].Code==="INC_FIRST_LAST_NAME" && fullResults.SIGNATURE_SETTINGS[0].BitValue===true)}
                                                />
                                                <label class="form-check-label" for="SIGNATURESETTINGS1">
                                                    Include First and Last Name
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS2" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",1)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS[1].Code==="INC_FIRST_NAME" && fullResults.SIGNATURE_SETTINGS[1].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS2">
                                                    Include First Name only
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS3" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",2)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS[2].Code==="INC_LAST_NAME" && fullResults.SIGNATURE_SETTINGS[2].BitValue===true)}

                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS3">
                                                    Include Last Name only
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS4" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",3)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS[3].Code==="INC_DESIGNATION" && fullResults.SIGNATURE_SETTINGS[3].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS4">
                                                    Include Designation
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS5" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",4)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS[4].Code==="INC_PHONE_NUMBER" && fullResults.SIGNATURE_SETTINGS[4].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS5">
                                                    Include Phone Number
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" value=""
                                                       id="SIGNATURESETTINGS6" name={"SIGNATURESETTINGS"}
                                                       onClick={()=>handleChanger("SIGNATURE_SETTINGS",5)}
                                                       checked={(fullResults && fullResults.SIGNATURE_SETTINGS[5].Code==="INC_LOCATION" && fullResults.SIGNATURE_SETTINGS[5].BitValue===true)}
                                                />
                                                <label className="form-check-label" htmlFor="SIGNATURESETTINGS6">
                                                    Include Location (City)
                                                </label>
                                            </div>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                            <div id="fifthTab" className='tab-content' style={{ display: "none" }}>
                                <div className='heading'>
                                    <h4>SIGNATURE FONT SIZE</h4>
                                    <p>Customize your signature font size as per your requirement by using the below scale.</p>
                                </div>
                                <div>
                                    <p>Input slider:</p>
                                    <div className={'customer-range'}>
                                        <Range
                                            step={1}
                                            min={7}
                                            max={13}
                                            values={values}
                                            onChange={(values) => {
                                                setValues(values)
                                            }}
                                            renderTrack={({ props, children }) => (
                                                <div
                                                    {...props}
                                                    className="w-full slider-line-heigth h-3 pr-2 my-4 bg-gray-200 rounded-md"
                                                >
                                                    {children}
                                                </div>
                                            )}
                                            renderThumb={({ props }) => (
                                                <div
                                                    {...props}
                                                    className="w-5 h-5 transform slider-line-pointer translate-x-10 bg-indigo-500 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                                >
                                                    <div className={'slider-line-valuse'}>{values[0]}{values[0] === 10 && ' (Default)'}</div>
                                                </div>
                                            )}
                                        />
                                        <div className={'d-flex justify-content-between mt--18'}>
                                            <div>7</div>
                                            <div>13</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="subf_pagn_btn text-right">
                            <button className="btn" >RESET</button>
                            <button className="btn" >SAVE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { UserPreferencesss };