import React, { useState, useEffect } from 'react';
import { Link,useLocation,useHistory } from 'react-router-dom';

import queryString from 'query-string';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Reaptcha from 'reaptcha';
import { accountService, alertService } from '@/_services';
import * as CryptoJS from 'crypto-js';
import { Modal } from 'react-bootstrap';

function ResetPassword({ history }) {
    console.log(history,"history")
    const usehistory = useHistory();
    const[auserid,setuserid]=useState()
    const[auseremail,setuseremail]=useState()
    const [validCaptcha,setvalidCaptcha]=useState()
    const [errormessage,setErrormessage]=useState("")
    console.log(auseremail,"userEmail")
            console.log(auserid,"userid")
    const TokenStatus = {
        Validating: 'Validating',
        Valid: 'Valid',
        Invalid: 'Invalid'
    }
    const search = useLocation().search;
    const searchParams = new URLSearchParams(search);
    useEffect(()=>{
        const URLPARAM = window.atob(searchParams.get("q"))
        const splitData = URLPARAM.split('&')
        console.log(splitData,"splitData")
        if (splitData && splitData[0] && splitData[1]) {
            const a = splitData[0].split('=');
            const b = splitData[1].split('=');
            const userEmail = a[1];
            const userid = b[1];
            console.log(userEmail,"userEmail")
            console.log(userid,"userid")
            setuserid(userid);
            setuseremail(userEmail);
        }
    },[])
   
    const [token, setToken] = useState(null);
    const [tokenStatus, setTokenStatus] = useState(TokenStatus.Validating);

    useEffect(() => {
        const { token } = queryString.parse(location.search);

        // remove token from url to prevent http referer leakage
        history.replace(location.pathname);

        accountService.validateResetToken(token)
            .then(() => {
                setToken(token);
                setTokenStatus(TokenStatus.Valid);
            })
            .catch(() => {
                setTokenStatus(TokenStatus.Invalid);
            });
    }, []);

    function getForm() {
        const initialValues = {
            password: '',
            confirmPassword: '',
            recaptcha:''
        };

        const validationSchema = Yup.object().shape({
            password: Yup.string()
                .min(6, 'Password must be at least 6 characters')
                .required('Password is required'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required('Confirm Password is required'),
              
        });
        function cryptedString(string) {
            let ckey = CryptoJS.enc.Utf8.parse('1234567890123456');
            let encrypt = CryptoJS.AES.encrypt(string, ckey, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            })
            return encrypt.ciphertext.toString()
        
        }

        function onSubmit({ password, confirmPassword }, { setSubmitting }) {
            alertService.clear();
            const newpassword=cryptedString(password)
            const newconfirmPassword=cryptedString(confirmPassword)
            let payload={"Password":newpassword,"UserId":auserid}
            let payload1={"ConfirmPassword":newconfirmPassword,"Email":auseremail,"NewPassword":newpassword,"UserId":auserid,"IsReact":1}
            validCaptcha=== true && accountService.resetcheckpassword(payload)
                .then((result) => {
                    if(result.Messages[0]!=="Success"){
                      setErrormessage(result.Messages[0])
                    }
                    else{
                        accountService.resetupdatepassword(payload1)
                        .then((result1) => {
                            if(result1){
                                console.log(result1)
                                alertService.success('Password Updated Successfully', { keepAfterRouteChange: true });
                                history.push('login');
                            }
                            
                            
                        }).catch(error => {
                            
                             alertService.error(error);
                         });
                    }
                    //if(result)
                   
                    //alertService.success('Password Updated Successfully', { keepAfterRouteChange: true });
                    //history.push('login');
                })
                .catch(error => {
                   // setSubmitting(false);
                    alertService.error(error);
                });
        }

        const handleCancel = () => {
            usehistory.push(`/account/login`)
        };
        
        function onCaptchaVerify(recaptchaResponse) {
            //validCaptcha = true
            setvalidCaptcha(true)
           
        };
        function onCaptchaExpire(recaptchaResponse) {
            //validCaptcha = false
            setvalidCaptcha(false)
        }  

        return (
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                {({ errors, touched, isSubmitting }) => (
                    <Form>
                        <p style={{ color: "red", textAlign: "left" }}>{errormessage}</p>
                        <div className="form-group">
                            <label>Password</label>
                            <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                            <ErrorMessage name="password" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label>Confirm Password</label>
                            <Field name="confirmPassword" type="password" className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
                            <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
                        </div>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div className="form-group mt-4">
                                            <div className="captcha">
                                            <Reaptcha id="recaptcha" name="recaptcha"
                                                    sitekey="6LcljLYUAAAAANiWkP0pM3hMy-wakNZ0T0pYakKO"
                                                    onVerify={onCaptchaVerify}
                                                    onExpire={onCaptchaExpire} />
                                         {validCaptcha ===false  && <span className='error-message'>Captcha is required</span>}
                                                {/* {state && state.errors && state.errors.recaptcha ?
                                             <span className='error-message'>{state.errors.recaptcha}</span>
                                              : null} */}
                                        </div>
                                    </div>
                                </div>
                        <div className="form-row">
                            <div className="form-group col">
                            <button type="submit" className="btn btn-secondary " style={{margin:7,marginLeft:-10}} onClick={handleCancel}>CANCEL</button>
                                <button type="submit" className="btn btn-primary" onClick={()=>{validCaptcha !== true?setvalidCaptcha(false):""}}>
                                    {/* {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>} */}
                                    RESET PASSWORD
                                </button>
                                {/* <Link to="login" className="btn btn-link">CANCEL</Link> */}
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        );
    }

    function getBody() {
        return getForm();
        // switch (tokenStatus) {
        //     case TokenStatus.Valid:
        //         return getForm();
        //     case TokenStatus.Invalid:
        //         return <div>Token validation failed, if the token has expired you can get a new one at the <Link to="forgot-password">forgot password</Link> page.</div>;
        //     case TokenStatus.Validating:
        //         return <div>Validating token...</div>;
        // }
    }

    return (
        <Modal show={true}>
        <div>
            <h3 className="card-header">Reset Password</h3>
            <div className="card-body">{getBody()}</div>
        </div>
        </Modal>
    )
}

export { ResetPassword }; 