import { configureStore } from '@reduxjs/toolkit'
import { createApi } from '@reduxjs/toolkit/query/react'
import { TableReducer} from './reducers/TableReducer'

 const Store = configureStore({
  reducer: {
    // Define a top-level state field named `table`, handled by `tableReducer`
    table:TableReducer
  },
});
 export default Store