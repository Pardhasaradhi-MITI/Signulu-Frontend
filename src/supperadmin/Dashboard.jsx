import React, {useEffect, useState}from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    PieChart, Pie, Cell,ResponsiveContainer,
    LineChart,
    Line,
    LabelList,
  } from "recharts";
import { accountService, alertService } from '@/_services';
import {dashboardService} from '../_superadminservices/dashbordservice';
import {reportsservice} from './../_services/ReportsServices';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from '../account/Common/Topbar';
import { ContentOne } from './Content/ContentOne';
import { ContentTWO } from './Content/ContentTWO';
import { ContentTHree } from './Content/ContentTHree';
import { CollectionsOutlined } from '@material-ui/icons';

function Dashboard({ history, location }) {
   const [Userstatictcts,setUserstatictcts] = useState([]);
   const [monthlyActiveUsers,setMonthlyActiveUsers] = useState([]);
   const [DailyActiveUsers,setDailyActiveUsers] = useState([]);
   const [month,setmonth]=useState(new Date().getMonth() + 1)
   const [dyear,setdyear]=useState(new Date().getFullYear())
   const [monthdyear,setmonthdyear]=useState('' +0+month + dyear)
   const [myear,setmyear]=useState(new Date().getFullYear())
   const [dmonthname,setdmonthname]=useState(toMonthName(month))
   const [subscriptionsummary,setsubscriptionsummary]=useState([])
   const [documentsprocesseddata,setdocumentsprocesseddata]=useState([])
   const [docyear,setdocyear]=useState(new Date().getFullYear())
   const [documenttractchartyears,setdocumenttractchartyears]=useState([])
   useEffect(()=>{
    getuserstaticts()
    getmonthllyactiveusers(myear)
    getdailyactiveusers(monthdyear)
    getsubscriptionsummary()
    getdocumentsprocesseddata(docyear)
    gettrackchartyeardropdown()
   },[])
   useEffect(()=>{
    setdmonthname(toMonthName(month))
    setmonthdyear('' +0+month + dyear)
    getdailyactiveusers('' +0+month + dyear)
   },[month,dyear])
  useEffect(()=>{
    getmonthllyactiveusers(myear)
  },[myear])
  useEffect(()=>{
    getdocumentsprocesseddata(docyear)
  },[docyear])
   const getuserstaticts=()=>{
    dashboardService.getUserStatistics().then((result) => {
        setUserstatictcts(result.Entity);
    })
    .catch(error => {
    });
   }
   //Getting monthly active users data  and data to be prepare like obj 
    const getmonthllyactiveusers = (myear) =>{
        dashboardService.getMonthllyactiveusers(myear).then((result) => {
            setMonthlyActiveUsers(result.Entity);
        }).catch(error => {
        });
   }
   const gettrackchartyeardropdown=()=>{
    reportsservice.getdocumenttractchart().then(result=>{
        if(result?.Entity){
         setdocumenttractchartyears(result?.Entity?.Years)
         let length=result?.Entity?.Years.length-1
         setSelectedyear(result?.Entity?.Years[length])
        }
    })
}
      
      const Munthlydata =[]
      if(monthlyActiveUsers.length!==0)
       for(let i=0;i<=monthlyActiveUsers.Months.length-1;i++){
        let obj={name:monthlyActiveUsers.Months[i],Users:monthlyActiveUsers.UserChartDetails[i]}
        Munthlydata.push(obj)
       }
       //Getting daily active users data  and data to be prepare like obj 
       const getdailyactiveusers=(data)=>{
        dashboardService.getDailyActiveusers(data).then((result) => {
            setDailyActiveUsers(result.Entity);
        })
        .catch(error => {
        });
       }  
       const Dailydata =[]
       if(DailyActiveUsers.length!==0)
       for(let i=0;i<=DailyActiveUsers.Date.length-1;i++){
        let obj={name:(DailyActiveUsers.Date[i]).slice(0,2),Users:DailyActiveUsers.UserChartDetails[i]}
        Dailydata.push(obj)
       }
       //for chnage the month number to month name
       function toMonthName(monthNumber) {
        const date = new Date();
        date.setMonth(monthNumber - 1);
      
        // 👇️ using visitor's default locale
        return date.toLocaleString([], {
          month: 'long',
        });
      }
      //getting the data for subscription summary
      const getsubscriptionsummary=()=>{
            dashboardService.getSubscriptionSummary().then((result)=>{
                setsubscriptionsummary(result.Entity)
            })
            .catch(error => {
            });
      }
      const Subscriptiondata =[]
      if(subscriptionsummary.length!==0)
       for(let i=0;i<=subscriptionsummary.length-1;i++){
        let obj={name:subscriptionsummary[i].PackageCode,value:subscriptionsummary[i].TotalUsers,Packagename:subscriptionsummary[i].PackageName}
        Subscriptiondata.push(obj)
       }
      // these colors for pi chart for Summary
      const COLORS = ["rgb(93, 98, 181)","rgb(41, 195, 190)", "#FFBB28","#FF8042","#0088FE", "#00C49F", ];
      //these is for documentsprocesseddata
      const getdocumentsprocesseddata=(docyear)=>{
        dashboardService.getDocumentsProcessedData(docyear).then((result)=>{
            setdocumentsprocesseddata(result.Entity)
        }).catch(error =>{
        });
      }
      const Documentsproceseddata =[]
      if(documentsprocesseddata.length!==0)
       for(let i=0;i<=documentsprocesseddata.Months.length-1;i++){
        let obj={name:documentsprocesseddata.Months[i],Documents:documentsprocesseddata.DocumentChartDetails[i]}
        Documentsproceseddata.push(obj)
       }
// for Custom tooltip for graphs
const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
      return (
        <p style={{
        border:"1px solid #C9CBCE",
        backgroundColor:"white",
        padding:10,
        color:"#737B88",

        }}>
         {`${payload[0].dataKey}: ${payload[0].value}`}
        </p>
      );
    }
  
    return null;
  };
  const CustomLegend =(text)=>{
    return(<p style={{color:"#5f5f5fbd",padding:"10px"}}>{text}</p>)
  }
  const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index
}) => {
  const radius = 25+innerRadius + (outerRadius - innerRadius);
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
    x={x}
    y={y}
    fill="#212529"
    textAnchor={x > cx ? "start" : "end"}
    dominantBaseline="central"
    >
      {`${(percent * 100).toFixed(0)}%`}
      {/* {data[index].name} ({value}) */}
    </text>
  );
};
    //Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main super_admin_main" id="main">
                <Topbar />
                <div className="main-content super_admin_dashboard">
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className=' flex'>
                            <h3>Dashboard</h3>
                            <h3><button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"
                              style={{padding:"10px 10px",height:"40px",fontSize:"12",border:"1px solid #d0e1fd"}}
                            ><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",marginBottom:"6px",width: "15px",height: "15px"}} /></button>
                            
                            </h3></div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item customer">
                                    <div className='content' style={{cursor: "default",userSelect:"none"}}>
                                        <img src="src/images/customer.svg" alt="" />
                                        <h5>{Userstatictcts?.TotalCustomers?(Userstatictcts.TotalCustomers).toLocaleString():""}</h5>
                                        <p>No. Of Customers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item user">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/userss.svg" alt="" />
                                        <h5>{Userstatictcts?.TotalUsers?(Userstatictcts.TotalUsers).toLocaleString():""}</h5>
                                        <p>No. Of Users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item users">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/DailyActiveUsers.svg" alt="" />
                                        <h5>{Userstatictcts?.TotalDailyActiveUsers?(Userstatictcts.TotalDailyActiveUsers).toLocaleString():""}</h5>
                                        <p>Daily Active Users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item musers">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/MonthlyActiveUsers.svg" alt="" />
                                        <h5>{Userstatictcts?.TotalMonthlyActiveUsers?(Userstatictcts.TotalMonthlyActiveUsers).toLocaleString():""}</h5>
                                        <p>Monthly Active Users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item documents">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/DocumentsProcessed.svg" alt="" />
                                        <h5>{Userstatictcts?.TotalSignedDocuments?(Userstatictcts.TotalSignedDocuments).toLocaleString():""}</h5>
                                        <p>No. Of Documents Processed</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='dashboard_item'>
                                <div className="item duser">
                                    <div className='content' style={{cursor:"default",userSelect:"none"}}>
                                        <img src="src/images/DocumentsUser.svg" alt="" />
                                        <h5>{Userstatictcts?.TotalDocsPerUser?(Userstatictcts.TotalDocsPerUser).toLocaleString():""}</h5>
                                        <p>Documents/User</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='references'>
                                <div className='tab'>
                                    <ul className='navbar-tabs'>
                                        <li className="tablinks active" onClick={(e) => openTab(e, 'firstTab')}><button>Monthly Active Users </button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'secondTab')}><button>Daily Active Users</button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}><button>Subscription Summary</button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button> Documents Processed</button></li>
                                    </ul>
                                </div>
                                <div id="firstTab" className='tab-content' style={{ display: "block" }}>
                                    <div className='report_content'>
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='heading'>
                                                    <h3 style={{fontSize:22}}>Monthly Active Users</h3>
                                                    <p>No.of active users, for the period of {myear}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='report_content_block'>
                                                    <div className='flex'>
                                                        <h4></h4>
                                                        <div className=''>
                                                        <select className="hlfinput" value={myear}
												        onChange={(e) => setmyear(e.target.value)}>
                                                            <option value={0}>Select Year</option>
                                                            {documenttractchartyears.map(item=>{
                                                                return( <option value={item}>{item}</option>)
                                                            })}
                                                            
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div className='report_chart'>
                                                    {Munthlydata.length!==0?<BarChart
                                                        width={900}
                                                        height={350}
                                                        data={Munthlydata}
                                                        margin={{
                                                            top: 5,
                                                            right: 30,
                                                            left: 20,
                                                            bottom: 5
                                                        }}
                                                        barSize={25}
                                                        >
                                                        <XAxis  dataKey="name" scale="point" padding={{ left:30, right:10 }} />
                                                        <YAxis type='number' padding={{right:30}} label={{ value: 'NO OF USERS', angle: -90, position: 'insideLeft',fill:"#5f5f5fbd"}}/>
                                                        <Tooltip content={CustomTooltip}  />
                                                        {/* <Legend  /> */}
                                                        {/* <Legend layout='horizontal' verticalAlign="top" height={36} align="left" /> */}
                                                        {/* <CartesianGrid strokeDasharray="1" /> */}
                                                        <Bar dataKey="Users" fill="#2F7FED" background={{ fill: "#F6F6F6" }} />
                                                    </BarChart>:<img src='src/images/report_chart.jpg' alt='' />}
                                                        {/* <img src='src/images/report_chart.jpg' alt='' /> */}
                                                    </div>
                                                    {Munthlydata.length!==0 ?<div className='flex justify-content-center' style={{color:"#5f5f5fbd"}}>MONTH OF THE YEAR</div>:""}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="secondTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='report_content'>
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='heading'>
                                                    <h3 style={{fontSize:22}}>Daily Active Users</h3>
                                                    <p>No.of active users, for the period of {dmonthname}{"-"}{dyear}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='report_content_block'>
                                                <div className='flex'>
                                                        <h4></h4>
                                                        <div className='flex'>
                                                        <select className="hlfinput" value={month}
												        onChange={(e) => setmonth(e.target.value)}>
                                                            <option value={0}>Select Month</option>
                                                            <option value={1}>Janury</option>
                                                            <option value={2}>February</option>
                                                            <option value={3}>March</option>
                                                            <option value={4}>April</option>
                                                            <option value={5}>May</option>
                                                            <option value={6}>June</option>
                                                            <option value={7}>July</option>
                                                            <option value={8}>August</option>
                                                            <option value={9}>September</option>
                                                            <option value={10}>October</option>
                                                            <option value={11}>November</option>
                                                            <option value={12}>December</option>
                                                        </select>
                                                        <select className="hlfinput" value={dyear}
												        onChange={(e) => setdyear(e.target.value)}>
                                                            <option value={0}>Select Year</option>
                                                            {documenttractchartyears.map(item=>{
                                                                return( <option value={item}>{item}</option>)
                                                            })}
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div className='report_chart'>
                                                    {Dailydata.length!==0?<BarChart
                                                        width={930}
                                                        height={350}
                                                        data={Dailydata}
                                                        margin={{
                                                            top: 5,
                                                            right: 30,
                                                            left: 20,
                                                            bottom: 5
                                                        }}
                                                        barSize={35}
                                                        >
                                                        <XAxis dataKey="name" scale="point" padding={{ left:30, right:10}}/>
                                                        <YAxis type='number' padding={{right:30}} label={{ value: 'NO OF USERS', angle: -90, position: 'insideLeft',fill:"#5f5f5fbd"}}/>
                                                        <Tooltip content={CustomTooltip}/>
                                                        {/* <Legend content={CustomLegend("YEAR OF MONTHS")}/> */}
                                                        {/* <CartesianGrid strokeDasharray="1" /> */}
                                                        <Bar dataKey="Users" fill="#2F7FED" background={{ fill: "#F6F6F6" }} />
                                                    </BarChart>
                                                    
                                                    :<img src='src/images/report_chart.jpg' alt='' />}
                                                    </div>
                                                   {Dailydata.length!==0 ?<div className='flex justify-content-center' style={{color:"#5f5f5fbd"}}>DAY OF THE MONTH</div>:""}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="thirdTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='report_content'>
                                        {/* <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='heading'>
                                                    <h3>Monthly Active Users</h3>
                                                    <p>No.of active users, for the period of 2022</p>
                                                </div>
                                            </div>
                                        </div> */}
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='report_content_block'>
                                                    <div className='flex justify-content-space-between'>
                                                        <h4>No. of Subscribers</h4>
                                                        <h4>Percentage Mix</h4>
                                                        {/* <div className='align'>
                                                            <select className='hlfinput'>
                                                                <option>2021</option>
                                                            </select>
                                                        </div> */}
                                                    </div>
                                                    <div className='report_chart'>
                                                       
                                                    <div  style={{ width: "100%", height: 350 }}>
                                                        <div className='flex' style={{ width: "100%", height: 350 }}>
                                                        <ResponsiveContainer>
                                                            <PieChart className='flex' width={900} height={450}>
                                                            <Pie dataKey="value" data={Subscriptiondata} fill="#8884d8" label >
                                                            {Subscriptiondata.map((entry, index) => (
                                                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                                                ))}
                                                            </Pie>
                                                            
                                                            {/* <Tooltip/> */}
                                                            {/* <Legend/> */}
                                                            </PieChart>
                                                        </ResponsiveContainer>
                                                        <ResponsiveContainer>
                                                            <PieChart className='flex' width={900} height={450}>
                                                            <Pie dataKey="value" data={Subscriptiondata} fill="#8884d8" label={renderCustomizedLabel} >
                                                            {Subscriptiondata.map((entry, index) => (
                                                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                                                ))}
                                                            </Pie>
                                                            
                                                            {/* <Tooltip/> */}
                                                            {/* <Legend/> */}
                                                            </PieChart>
                                                        </ResponsiveContainer>
                                                        </div>
                                                        </div>
                                                        <div className='flex justify-content-center'>
                                                      <span style={{height:10,width:25,borderRadius:50,display:"inline-block",backgroundColor:COLORS[0]}}/><span className='-mt-2' style={{color:COLORS[0],fontSize:18,fontWeight:600}}>TRIAL</span>
                                                      <span style={{height:10,width:25,borderRadius:50,display:"inline-block",backgroundColor:COLORS[1]}}/><span className='-mt-2' style={{color:COLORS[1],fontSize:18,fontWeight:600}}>COMPANY</span>
                                                      <span style={{height:10,width:25,borderRadius:50,display:"inline-block",backgroundColor:COLORS[2]}}/><span className='-mt-2' style={{color:COLORS[2],fontSize:18,fontWeight:600}}>INDIVIDUVAL</span>
                                                      <span style={{height:10,width:25,borderRadius:50,display:"inline-block",backgroundColor:COLORS[3]}}/><span className='-mt-2' style={{color:COLORS[3],fontSize:18,fontWeight:600}}>POWERPACK</span>
                                                      <span style={{height:10,width:25,borderRadius:50,display:"inline-block",backgroundColor:COLORS[4]}}/><span className='-mt-2' style={{color:COLORS[4],fontSize:18,fontWeight:600}}>ENTERPRISE</span> 
                                                            </div> 
                                                        {/* <img src='src/images/report_chart.svg' alt='' /> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='report_content'>
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='heading'>
                                                    <h3></h3>
                                                    <p>No.of Documents Processed, for the period of {docyear}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div className='report_content_block'>
                                                <div className='flex'>
                                                        <h4 ></h4>
                                                        <div className=''>
                                                        <select className="hlfinput" value={docyear}
												        onChange={(e) => setdocyear(e.target.value)}>
                                                            <option value={0}>Select Year</option>
                                                            {documenttractchartyears.map(item=>{
                                                                return( <option value={item}>{item}</option>)
                                                            })}
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div className='report_chart'>
                                                    <LineChart
                                                        width={900}
                                                        height={350}
                                                        data={Documentsproceseddata}
                                                        margin={{
                                                            top: 5,
                                                            right: 30,
                                                            left: 20,
                                                            bottom: 25
                                                        }}
                                                        >
                                                        {/* <CartesianGrid strokeDasharray="" /> */}
                                                        <XAxis dataKey="name" padding={{left:15}}/>
                                                        <YAxis  label={{ value: 'NO. OF DOCUMENTS', angle: -90, position: 'insideLeft',fill:"#5f5f5fbd"}}
                                                            tickFormatter={(number) => {
                                                                if(number>=1000){
                                                            let num = number / 1000;
                                                            const afterDot = num.toString().split(".")[1];
                                                            if (afterDot && afterDot.length > 2) {
                                                                num = num.toFixed(2);
                                                            }
                                                            return num + "k";
                                                           }
                                                           else{
                                                            return number;
                                                           }
                                                            }}
                                                            //label={{ value: 'NO. Of Documents Processed', angle: -90, position: 'insideLeft',fill:"#5f5f5fbd"}}
                                                        />
                                                       <Tooltip content={CustomTooltip}  />
                                                        {/* <Legend content={CustomLegend("YEAR OF MONTHS")}/> */}
                                                        <Line
                                                            type="monotone"
                                                            dataKey="Documents"
                                                            stroke="#5d62b5"
                                                            activeDot={{ r: 8 }}
                                                            strokeWidth={2}
                                                            isAnimationActive={false}
                                                        > <LabelList dataKey={'Documents'} position={'top'} padding={{top:10}}/></Line>
                                                        </LineChart>
                                                        <div className='flex justify-content-center -mt-3' style={{color:"#5f5f5fbd"}}>MONTH OF THE YEAR</div>
                                                        {/* <img src='src/images/report_chart.svg' alt='' /> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Dashboard }; 