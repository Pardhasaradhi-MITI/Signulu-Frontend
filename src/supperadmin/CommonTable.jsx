import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';

import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';
import { ContentOne } from './Content/ContentOne';
import { ContentTWO } from './Content/ContentTWO';
import { ContentTHree } from './Content/ContentTHree';

function CommonTable({ history, location }) {
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main super_admin_main" id="main">
                <Topbar />
                <div className="main-content super_admin_dashboard">
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='CommonTable'>
                                <div className='table-responsive'>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">User Name</th>
                                                <th scope="col">Designation</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Contact No</th>
                                                <th scope="col">User Type</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Matt Damonv</td>
                                                <td>Lawyer</td>
                                                <td>hello.damonv@gmail.com</td>
                                                <td>9876543210</td>
                                                <td>Administrator</td>
                                                <td>Active</td>
                                                <td>
                                                    <div className='action_sec'>
                                                        <ul>
                                                            <li><img src='src/images/users.svg' /></li>
                                                            <li> <label className="switch">
                                                                <input type="checkbox" checked />
                                                                <span className="slider round"></span>
                                                            </label>
                                                            </li>
                                                            <li><img src='src/images/delete_icon.svg' /></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Liam Crowe</td>
                                                <td>Lawyer</td>
                                                <td>hi.hiam@gmail.com</td>
                                                <td>8796324569</td>
                                                <td>Administrator</td>
                                                <td>Inactive</td>
                                                <td>
                                                    <div className='action_sec'>
                                                        <ul>
                                                            <li><img src='src/images/users.svg' /></li>
                                                            <li> <label className="switch">
                                                                <input type="checkbox" />
                                                                <span className="slider round"></span>
                                                            </label>
                                                            </li>
                                                            <li><img src='src/images/delete_icon.svg' /></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Daniel Farrell</td>
                                                <td>Lawyer</td>
                                                <td>hello.farrell@gmail.com</td>
                                                <td>7896321478</td>
                                                <td>Administrator</td>
                                                <td>Inactive</td>
                                                <td>
                                                    <div className='action_sec'>
                                                        <ul>
                                                            <li><img src='src/images/users.svg' /></li>
                                                            <li> <label className="switch">
                                                                <input type="checkbox" />
                                                                <span className="slider round"></span>
                                                            </label>
                                                            </li>
                                                            <li><img src='src/images/delete_icon.svg' /></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Chris Pine</td>
                                                <td>Lawyer</td>
                                                <td>info.chris@gmail.com</td>
                                                <td>6290776842</td>
                                                <td>Administrator</td>
                                                <td>Active</td>
                                                <td>
                                                    <div className='action_sec'>
                                                        <ul>
                                                            <li><img src='src/images/users.svg' /></li>
                                                            <li> <label className="switch">
                                                                <input type="checkbox" checked/>
                                                                <span className="slider round"></span>
                                                            </label>
                                                            </li>
                                                            <li><img src='src/images/delete_icon.svg' /></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Matt Damonv</td>
                                                <td>Lawyer</td>
                                                <td>hello.damonv@gmail.com</td>
                                                <td>7831889645</td>
                                                <td>Administrator</td>
                                                <td>Active</td>
                                                <td>
                                                    <div className='action_sec'>
                                                        <ul>
                                                            <li><img src='src/images/users.svg' /></li>
                                                            <li> <label className="switch">
                                                                <input type="checkbox" checked/>
                                                                <span className="slider round"></span>
                                                            </label>
                                                            </li>
                                                            <li><img src='src/images/delete_icon.svg' /></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Natalie Stone</td>
                                                <td>Lawyer</td>
                                                <td>info.stone@gmail.com</td>
                                                <td>8974225647</td>
                                                <td>Administrator</td>
                                                <td>Inactive</td>
                                                <td>
                                                    <div className='action_sec'>
                                                        <ul>
                                                            <li><img src='src/images/users.svg' /></li>
                                                            <li> <label className="switch">
                                                                <input type="checkbox"/>
                                                                <span className="slider round"></span>
                                                            </label>
                                                            </li>
                                                            <li><img src='src/images/delete_icon.svg' /></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
        </div>
    )
}

export { CommonTable }; 