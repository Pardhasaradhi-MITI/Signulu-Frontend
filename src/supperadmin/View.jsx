import React, { useState,useEffect } from 'react';
import { Link,useHistory } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import _ from "lodash";
import { accountService, alertService } from '@/_services';
import Pagination from "react-responsive-pagination";   
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
// import { Topbar } from './Common/Topbar';
import { Topbar } from '../account/Common/Topbar';
import { CustomermanagementService } from '../_superadminservices/CustomerManagementservice';
import { Search } from '@syncfusion/ej2-react-dropdowns';
import { AssignmentReturnSharp } from '@material-ui/icons';

function View({ history, location }) {
   // const history = useHistory();
   const [userslist,setuserslist]=useState([])
   const [searchuserslist,setsearchuserslist]=useState([])
   const [pageNumber,setPageNumber]=useState(1)
   const [pageSize,setPageSize]=useState(10)
   const [PageCount,setPageCount]=useState(1)
   const [perpage,setperpage]=useState([])
   const [searchperpage,setsearchperpage]=useState([])
   const [searchpagecount,setsearchpagecount]=useState(1)
   const [Searchstring,setsearchstring]=useState("")
   const keys=["CompanyId","CompanyName","RegisteredOn","PackageCode","DurationMode","EmailAddress","FirstName","LastName"]
   let data = {"PageNumber": 1, "PageSize": 1000000, "SortDir": "a", "SortKey": "name" };
   useEffect(()=>{
    getalluserslist(data)
   },[])
   useEffect(()=>{
    setsearchuserslist([])
    if(Searchstring !== ""){
        search(userslist)
    }
   },[Searchstring])
    const search=(userslist)=>{
        setsearchuserslist([])
       const list= userslist.length>0 && userslist.filter((item)=>keys.some((key)=>_.includes(_.lowerCase(item[key]),_.lowerCase(Searchstring))))
        setsearchuserslist(list);
        setsearchperpage(list.length>10?list.slice(0,10):list);
        let pages = 0;
        for(let i=1;i<Math.ceil(list.length/10)+1;i++){
            pages = i
        }
        setsearchpagecount(pages)
       
    }

   const getalluserslist=(data)=>{
    CustomermanagementService.getalluserslist(data).then((result) => {
        //setPageCount(result?.Entity?.PageCount)
        const sortedArray = _.orderBy(result?.Entity?.data, [(obj) => new Date(obj.RegisteredOn)], ['desc'])
        setperpage(sortedArray.slice(0,10));
        setuserslist(sortedArray);
        let pages = 0;
        for(let i=1;i<Math.ceil(result?.Entity?.data.length/10)+1;i++){
            pages = i
        }
        setPageCount(pages)
        //setsearchuserslist(result?.Entity?.data);
    }).catch(error => {
    });
   }
   const handlePagination = (newPage) => {
    setPageNumber(newPage);
    if(searchuserslist.length>0 && Searchstring !== ""){
        setsearchperpage([])
        setsearchperpage(searchuserslist.slice((newPage*10)-10,newPage*10));
    }
    else{
        setperpage([])
        setperpage(userslist.slice((newPage*10)-10,newPage*10));
    }
   
   }
   const handlesearchstring = (e)=>{
    setsearchstring(e.target.value)
   }
   

    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content main-from-content customerbilling">
                    <div className='row align-items-center'>
                    <div className='heading'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div className='flex'>
                                <div><h3>ACCOUNT VIEW/MODIFY</h3></div>
                                <div className='flex'>
                                    {/* <h3><button onClick={()=>history.push('/supperadmin/dashboard')} className="btn back_btn"
                              style={{padding:9,border:"1px solid #d0e1fd"}}
                            ><img src="src/images/back_btn.svg"  alt="" />Back</button>
                            </h3> */}
                            <h3><button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"
                              style={{padding:"10px 10px",border:"1px solid #d0e1fd",height:"40px",fontSize:"12px"}}
                            ><img src="src/images/home_icon.svg" alt=""style={{marginRight:"2px",marginBottom:"5px",width: "15px",height: "15px"}} /></button>
                            </h3></div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="table-pay-select">
                                <form >
                                    <div className="text-fil">
                                        <div className="search">
                                            <input type="text" onChange={(e)=>handlesearchstring(e)} placeholder="Search" />
                                            <button type="submit" className="btn" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="table-pay table-responsive ">
                                <table class="table ">
                                    <thead className='second_head'>
                                        <tr>
                                            <th ><div className="head-ttl " style={{fontSize:15}}>ID</div></th>
                                            <th ><div className="head-ttl -ml-2" style={{fontSize:15}}>Organization</div></th>
                                            <th ><div className="head-ttl -ml-2" style={{fontSize:15}}>Registered On</div></th>
                                            <th ><div className="head-ttl -ml-2" style={{fontSize:15}}>Subscription</div></th>
                                            <th ><div className="head-ttl -ml-2" style={{fontSize:15}}>Duration</div></th>
                                            <th ><div className="head-ttl -ml-2 " style={{fontSize:15}}>Status</div></th>
                                            <th style={{maxWidth:80,width:"9%"}} ><div className="head-ttl -ml-2 mr-4" >Action</div></th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                    {/* Searchstring.length > 0 ? searchperpage:  */}
                                    
                                        { 
                                        Searchstring !== ""? searchperpage.map(item=>{
                                      return(
                                        <tr key={item.UserId}>
                                            <td style={{minWidth:80,fontSize:17}}  className='px-1 py-2'>{item.UserId}</td>
                                            <td style={{minWidth:80,fontSize:17}}  className='px-1 py-2 '>{item.CompanyName!==null?item.CompanyName:"-"}</td>
                                            <td style={{minWidth:150,fontSize:17}}  className='px-1 py-2'>{item.RegisteredOn}</td>
                                            <td style={{minWidth:150,fontSize:17}}  className='px-1 py-2'>{item.Package}</td>
                                            <td style={{minWidth:100,fontSize:17}}  className='px-1 py-2'>{item.DurationMode}</td>
                                            <td style={{minWidth:80,fontSize:17}}  className='px-1 py-2' color='red'>{item.is_active===1?"Active":"InActive"}</td>
                                            <td style={{maxWidth:80,fontSize:17}} className='px-1 py-2' >
                                            <button onClick={()=> history.push(`viewmodify/${item.CompanyId}/${item.PackageCode}/${item.DurationMode}/${item.UserId}/${item.NumberOfUsers}`)} style={{maxWidth:80}} className="btn btn-paid ">Details</button>
                                            </td>
                                        </tr>)}):perpage.map(item=>{
                                      return(
                                        <tr key={item.UserId}>
                                            <td style={{minWidth:80,fontSize:17}}  className='px-1 py-2'>{item.UserId}</td>
                                            <td style={{minWidth:80,fontSize:17}}  className='px-1 py-2 '>{item.CompanyName !== null ?item.CompanyName:"-"}</td>
                                            <td style={{minWidth:150,fontSize:17}}  className='px-1 py-2'>{item.RegisteredOn}</td>
                                            <td style={{minWidth:150,fontSize:17}}  className='px-1 py-2'>{item.Package}</td>
                                            <td style={{minWidth:100,fontSize:17}}  className='px-1 py-2'>{item.DurationMode}</td>
                                            <td style={{minWidth:80,fontSize:17}}  className='px-1 py-2' color='red'>{item.is_active===1?"Active":"InActive"}</td>
                                            <td style={{maxWidth:80,fontSize:17}} className='px-1 py-2' >
                                            <button onClick={()=> history.push(`viewmodify/${item.CompanyId}/${item.PackageCode}/${item.DurationMode}/${item.UserId}/${item.NumberOfUsers}`)} style={{maxWidth:80}} className="btn btn-paid ">Details</button>
                                            </td>
                                        </tr>)})}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {/* <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div class="document-nagin"><nav aria-label="Page navigation"><ul class="pagination"><li class="page-item"><a class="page-link active" href="#">1</a></li><li class="page-item"><a class="page-link" href="#">2</a></li><li class="page-item"><a class="page-link" href="#">3</a></li><li class="page-item"><a class="page-link" href="#">4</a></li><li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">•••</span></a></li></ul></nav></div>
                        </div>
                    </div> */}
                    <div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
                                        {/* Searchstring.length>0?searchpagecount: */}
											<Pagination
												current={pageNumber}
												total={Searchstring.length>0?searchpagecount:PageCount}
												onPageChange={(newPage) => setTimeout(() => {
                                                    handlePagination(newPage)
                                                }, 500)}
                                                maxWidth={"80px"}
											/>
										</nav>
									</div>
								</div>
							</div>
                </div>
            </div>
        </div>
    )
}

export { View }; 