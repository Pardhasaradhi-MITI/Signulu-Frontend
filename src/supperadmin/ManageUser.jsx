import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function ManageUser({ history, location }) {
    //Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content main-from-content customermanagement">
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='references'>
                                <div className='tab'>
                                    <ul className='navbar-tabs'>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'firstTab')}><button>Account Details</button></li>
                                        <li className="tablinks active" onClick={(e) => openTab(e, 'secondTab')}><button>Manage User</button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}><button>Manage Subscription</button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button>Performance</button></li>
                                    </ul>
                                </div>
                                <div id="firstTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Account Details</h4>
                                        <div className='edit-btns'>
                                            <button class="btn edit_btn"><span><img src='src/images/edit_icon.svg' /></span>Edit</button>
                                        </div>
                                    </div>
                                    <div className='account-details-table table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Customer Name:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Daniel Farrell</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>hello.farrell@gmail.com</strong></span><span>Phone On:</span></div><div></div></td>
                                                    <td className='text-right'><strong>9831000555</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Organization:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>MITI TEAM</strong></span><span>Subscription:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Active</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>20-Nov-2022</strong></span><span>Role:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Administrator</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                            <h3>User Details</h3>
                                        </div>
                                        <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                            <div class="recipient-btn">
                                                <div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div>
                                                <div class="add_user">
                                                    <button class="btn"><img src="src/images/plus_icon.svg" />Add User</button>
                                                </div>
                                                <div class="action_area">
                                                    <ul>
                                                        <li><img src="src/images/xls.svg" alt="" /></li>
                                                        <li><img src="src/images/pdf.svg" alt="" /></li>
                                                        <li><img src="src/images/csv.svg" alt="" /></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                            <div className="user-details-table table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>User Name </th>
                                                            <th>Designation</th>
                                                            <th>Email</th>
                                                            <th>Contact No</th>
                                                            <th>User Type</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Matt Damonv
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                hello.damon@gmail.com
                                                            </td>
                                                            <td>9876543210</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Matt Damonv
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                hello.damon@gmail.com
                                                            </td>
                                                            <td>9876543210</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Matt Damonv
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                hello.damon@gmail.com
                                                            </td>
                                                            <td>9876543210</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Matt Damonv
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                hello.damon@gmail.com
                                                            </td>
                                                            <td>9876543210</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="document-nagin">
                                                <nav aria-label="Page navigation">
                                                    <ul class="pagination">
                                                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                        <li class="page-item">
                                                            <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">•••</span></a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>

                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='heading'>
                                            <h4>Activities</h4>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                            <div className='document_block'>
                                                <div className='spcingg'>
                                                    <h3><strong>Users Status</strong></h3>
                                                    <div className="align">
                                                        <select className="hlfinput">
                                                            <option>Dec 21</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className='chart'>
                                                    <div className='chart_picture'>
                                                        <img src='src/images/chart_10.svg' alt='' />
                                                    </div>
                                                    <div className='chart_content'>
                                                        <h6>Total Users</h6>
                                                        <h5>564</h5>
                                                        <ul>
                                                            <li><span className='active'></span>Added New Users</li>
                                                            <li><span className='rejected'></span>Active Users</li>
                                                            <li><span className='cancelled'></span>Inactive Users</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                            <div className='document_block'>
                                                <div className='spcingg'>
                                                    <h3><strong>Document Status</strong></h3>
                                                    <div className="align">
                                                        <select className="hlfinput">
                                                            <option>Dec 21</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className='chart'>
                                                    <div className='chart_picture'>
                                                        <img src='src/images/chart_11.svg' alt='' />
                                                    </div>
                                                    <div className='chart_content'>
                                                        <h6>Total Document</h6>
                                                        <h5>956</h5>
                                                        <div className='spacing'>
                                                            <ul>
                                                                <li><span className='review'></span>Draft</li>
                                                                <li><span className='rejected1'></span>Pending</li>
                                                                <li><span className='active1'></span>Approved</li>
                                                            </ul>
                                                            <ul>
                                                                <li><span className='rejected2'></span>Rejected</li>
                                                                <li><span className='cancelled1'></span>Cancelled</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='heading'>
                                            <h4>Document Process Comparison </h4>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className="align">
                                            <select className="hlfinput">
                                                <option>Dec 21</option>
                                            </select>
                                            <select className="hlfinput">
                                                <option>Jan 21</option>
                                            </select>
                                        </div>
                                        <div className='line-chart'>
                                            <img src='src/images/line-chart.svg' />
                                        </div>
                                    </div>
                                </div>

                                <div id="secondTab" className='tab-content' style={{ display: "block" }}>
                                    <div className='row'>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                            <h3>Manage User</h3>
                                        </div>
                                        <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                            <div class="recipient-btn">
                                                <div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div>
                                                <div class="add_user">
                                                    <button class="btn"><img src="src/images/plus_icon.svg" />Add New Customer</button>
                                                </div>
                                                <div class="action_area">
                                                    <ul>
                                                        <li><img src="src/images/xls.svg" alt="" /></li>
                                                        <li><img src="src/images/pdf.svg" alt="" /></li>
                                                        <li><img src="src/images/csv.svg" alt="" /></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                            <div className="CommonTable table-responsive mt-4">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">User Name</th>
                                                            <th scope="col">Designation</th>
                                                            <th scope="col">Email</th>
                                                            <th scope="col">Contact No</th>
                                                            <th scope="col">User Type</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Matt Damonv</td>
                                                            <td>Lawyer</td>
                                                            <td>hello.damonv@gmail.com</td>
                                                            <td>9876543210</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" checked />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Liam Crowe</td>
                                                            <td>Lawyer</td>
                                                            <td>hi.hiam@gmail.com</td>
                                                            <td>8796324569</td>
                                                            <td>Administrator</td>
                                                            <td>Inactive</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Daniel Farrell</td>
                                                            <td>Lawyer</td>
                                                            <td>hello.farrell@gmail.com</td>
                                                            <td>7896321478</td>
                                                            <td>Administrator</td>
                                                            <td>Inactive</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Chris Pine</td>
                                                            <td>Lawyer</td>
                                                            <td>info.chris@gmail.com</td>
                                                            <td>6290776842</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" checked />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matt Damonv</td>
                                                            <td>Lawyer</td>
                                                            <td>hello.damonv@gmail.com</td>
                                                            <td>7831889645</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" checked />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Natalie Stone</td>
                                                            <td>Lawyer</td>
                                                            <td>info.stone@gmail.com</td>
                                                            <td>8974225647</td>
                                                            <td>Administrator</td>
                                                            <td>Inactive</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matt Damonv</td>
                                                            <td>Lawyer</td>
                                                            <td>hello.damonv@gmail.com</td>
                                                            <td>7458945612</td>
                                                            <td>Administrator</td>
                                                            <td>Inactive</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matt Damonv</td>
                                                            <td>Lawyer</td>
                                                            <td>hello.matt@gmail.com</td>
                                                            <td>9831547893</td>
                                                            <td>Administrator</td>
                                                            <td>Inactive</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" checked />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Chris Pine</td>
                                                            <td>Lawyer</td>
                                                            <td>hello.pine@gmail.com</td>
                                                            <td>7879887522</td>
                                                            <td>Administrator</td>
                                                            <td>Inactive</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matt Damonv</td>
                                                            <td>Lawyer</td>
                                                            <td>info.matt@gmail.com</td>
                                                            <td>9876543210</td>
                                                            <td>Administrator</td>
                                                            <td>Active</td>
                                                            <td>
                                                                <div className='action_sec'>
                                                                    <ul>
                                                                        <li> <label className="switch">
                                                                            <input type="checkbox" checked/>
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="document-nagin">
                                                <nav aria-label="Page navigation">
                                                    <ul class="pagination">
                                                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                        <li class="page-item">
                                                            <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">•••</span></a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div id="thirdTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Account Details</h4>
                                        <div className='edit-btns'>
                                            <button class="btn edit_btn"><span><img src='src/images/edit_icon.svg' /></span>Edit</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Customer Name:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Daniel Farrell</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>hello.farrell@gmail.com</strong></span><span>Phone On:</span></div><div></div></td>
                                                    <td className='text-right'><strong>9831000555</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Organization:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>MITI TEAM</strong></span><span>Subscription:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Active</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>20-Nov-2022</strong></span><span>Role:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Administrator</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Account Details</h4>
                                        <div className='edit-btns'>
                                            <button class="btn edit_btn"><span><img src='src/images/edit_icon.svg' /></span>Edit</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Customer Name:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Daniel Farrell</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>hello.farrell@gmail.com</strong></span><span>Phone On:</span></div><div></div></td>
                                                    <td className='text-right'><strong>9831000555</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Organization:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>MITI TEAM</strong></span><span>Subscription:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Active</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>20-Nov-2022</strong></span><span>Role:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Administrator</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    )
}

export { ManageUser }; 