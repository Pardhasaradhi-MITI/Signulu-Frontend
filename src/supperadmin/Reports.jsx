import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function Reports({ history, location }) {

    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content main-from-content report">
                <div className='row align-items-center'>
                    <div className='heading'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className='flex-end'>
                                
                                <div className='flex d-flex justify-content-end'>
                                    {/* <h3><button onClick={()=>history.push('/supperadmin/dashboard')} className="btn back_btn"
                              style={{padding:9,border:"1px solid #d0e1fd"}}
                            ><img src="src/images/back_btn.svg"  alt="" />Back</button>
                            </h3> */}
                            <h3><button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"
                              style={{padding:"10px 10px",border:"1px solid #d0e1fd",height:"40px",fontSize:"12px"}}
                            ><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",marginBottom:"6px",width: "15px",height: "15px"}} /></button>
                            </h3></div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div className='report_content'>
                    
                    
                        <div className="row">
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div className='heading'>
                                    <h3>Active Users</h3>
                                    <p>No.of active users, for the period of 2022</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div className='report_content_block'>
                                    <div className='heading'>
                                        <h4>Statistics</h4>
                                        <div className='align'>
                                            <select className='hlfinput'>
                                                <option>Jan 2022</option>
                                            </select>
                                            <div className='download'>
                                                <img src='src/images/dwn_arrow.svg' alt='' />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='report_chart'>
                                        <img src='src/images/report_chart.jpg' alt='' />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Reports }; 