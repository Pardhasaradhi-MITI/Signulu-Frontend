import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { accountService } from '@/_services';

import { Dashboard } from './Dashboard';
import { CustomerManagement } from './CustomerManagement';
import { Reports } from './Reports';
import { NewAccountCustomer } from './NewAccountCustomer';
import { CustomerBilling } from './CustomerBilling';
import { Manage } from './manage';
import { ViewModify } from './ViewModify';
import { ManageUser } from './ManageUser';
import { CommonTable } from './CommonTable';
import { View } from './View';

function Supperadmin({ history, match }) {
    const { path } = match;

    useEffect(() => {
        // redirect to home if already logged in
        if (accountService.userValue) {
            history.push('/');
        }
    }, []);

    return (
        <Switch>
       
        <Route path={`${path}/dashboard`} component={Dashboard} />
        <Route path={`${path}/reports`} component={Reports} />
        <Route path={`${path}/customermanagement`} component={CustomerManagement} />
        <Route path={`${path}/newaccountcustomer`} component={NewAccountCustomer} />
        <Route path={`${path}/customerBilling`} component={CustomerBilling} />
        <Route path={`${path}/manage`} component={Manage} />
        <Route path={`${path}/viewmodify/:CompanyId/:PackageCode/:PDurationMode/:UserId/:NumberOfUsers`} component={ViewModify} />
        <Route path={`${path}/manageuser`} component={ManageUser} />
        <Route path={`${path}/commontable`} component={CommonTable} />
        <Route path={`${path}/view`} component={View} />
    </Switch>
    );
}

export { Supperadmin };