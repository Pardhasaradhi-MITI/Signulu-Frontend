import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
//import {useRoute} from '@react-navigation/native';
//const location = useLocation();
//const route = useRoute();
//console.log(route.name);


function Sidebar() {
    const location = useLocation();
    const locationname = location.pathname;

    //var adminmenu=0;
    //Filter Dropdown
    function dropdown9() {
        var srcElement = document.getElementById('dropdown9');
        var subelement = document.getElementById('sub_admin'); 
        var ul = document.getElementById("main_nav");
        var li= ul.childNodes;
        console.log(li.length)
         for (var i = 0; i < li.length; i++) {
               li[i].classList.remove("active");
         }
         subelement.classList.add("active");

        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }

    $('#sub_admin').click(function (e) {
        if ($(this).hasClass("drop")) {
            $(this).removeClass("drop");
        }
        else {
            $(this).addClass("drop");
        }
    });
    

   
    return (
        <div className="sidebar super_admin" id="sidebar-nav">
            <div className="sidebar-scroll">
                <div className='side'>
                    <div className="logo">
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i className="fas fa-align-left text-warning"></i>
                        </button>
                        <img src="src/images/signulu_black_logo.svg" alt="" className="logo1" />
                        <img src="src/images/signulu_small_black_logo.svg" alt="" className="logo2" />
                    </div>
                    <div className="quise_btn">
                        <button className="toggle_btn" onClick={slideToggle}><img src="src/images/toggle-menu.svg" /></button>
                    </div>
                </div>
                <nav className="navbar navbar-expand-lg">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav nav" id='main_nav'>
                            <li className={locationname == '/supperadmin/dashboard' ? 'item active' : 'item'}>
                                <Link to="dashboard" ><img src="src/images/dashboard_icon.svg" alt="" /><span>Dashboard</span></Link>
                            </li>
                            <li id="sub_admin" className={(locationname == '/' || locationname == '/supperadmin/newaccountcustomer' || locationname == '/supperadmin/view') ? 'item sub_drop-list active' : 'item sub_drop-list'}   >
                                <a href="javascript:void(0);" className="sub_toggle_btn arrow_down" onClick={dropdown9}><img src="src/images/customer-management_icon.svg" alt="" /><span>Customer Management</span></a>
                                <div className="sub_toggle_list active" id="dropdown9" style={{ display: 'none' }}>
                                    <div className="card card-body">
                                        <ul className="m-0 p-0">
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/supperadmin/newaccountcustomer" >New Account Creation</Link></li>
                                            <li> <img src="src/images/li-right.svg" alt="" /> <Link to="/supperadmin/view" >View / Modify</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li className={locationname == '/supperadmin/customerBilling' ? 'item active' : 'item'}>
                                <Link to="customerBilling" ><img src="src/images/billing_icon.svg" alt="" /><span>Billing</span></Link>
                            </li>
                            <li className={locationname == '/supperadmin/reports' ? 'item active' : 'item'}>
                                <Link to="reports" ><img src="src/images/reportss_icon.svg" alt="" /><span>Reports</span></Link>
                            </li>                           
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export { Sidebar }; 