import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';

function Topbar({ history, location }) {
    //Dropdown Menu User
    function userDropdown() {
        var srcElement = document.getElementById('dropdown_menu');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    return (

        <div className="topbar super-admin_top">
            <div className="top_info top_info_others">
                <div className="others">
                    <div className="users users-neo">
                        <div className="nav-item dropdown">
                            <a href="javascript:void(0)" className="dropdown-toggle nav-link user" onClick={userDropdown}>
                                <img src="src/images/user.png" alt="" className="user-picture" />
                                <span className="active"></span>
                            </a>
                            <div id="dropdown_menu" className="dropdown-menu">
                                <div className="top_list">
                                    <ul className="d-flex m-0 p-0">
                                        <li>
                                            <a href="javascript:void(0)">Account: <span> #16043285</span></a>
                                        </li>
                                        <li><a href="javascript:void(0)">Granite Factory</a></li>
                                        <li>
                                            <a href="javascript:void(0)">Total Documents: <span> 239/500</span></a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="setup_list">
                                    <ul>
                                        <li><a href="javascript:void(0)">My Documents: 234</a></li>
                                        <li><a href="javascript:void(0)">Manage Profile</a></li>
                                        <li><a href="javascript:void(0)">Feedback</a></li>
                                        <li><a href="javascript:void(0)">Update Password</a></li>
                                        <li><a href="javascript:void(0)">Cancel Account</a></li>
                                        <li><a href="javascript:void(0)">Log Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Topbar }; 