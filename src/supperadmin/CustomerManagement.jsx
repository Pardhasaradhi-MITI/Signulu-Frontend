import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function CustomerManagement({ history, location }) {
    //Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content main-from-content customermanagement">
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='references'>
                                <div className='tab'>
                                    <ul className='navbar-tabs'>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'firstTab')}><button>Account Details</button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'secondTab')}><button> Manage Use</button></li>
                                        <li className="tablinks active" onClick={(e) => openTab(e, 'thirdTab')}><button>Manage Subscription</button></li>
                                        <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button>Performance</button></li>
                                    </ul>
                                </div>
                                <div id="firstTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Manage Subscription</h4>
                                        <div className='pay-btn'>
                                            <p>Account Status</p>
                                            <button class="btn pay_btn">Activate</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Organization: </td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Oriana</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Inactive</strong></span><span>Subscriptin:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong> 31-Jan-2022</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div id="secondTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Manage Subscription</h4>
                                        <div className='pay-btn'>
                                            <p>Account Status</p>
                                            <button class="btn pay_btn">Activate</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Organization: </td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Oriana</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Inactive</strong></span><span>Subscriptin:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong> 31-Jan-2022</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div id="thirdTab" className='tab-content' style={{ display: "block" }}>
                                    <div className='heading'>
                                        <h4>Manage Subscription</h4>
                                        <div className='pay-btn'>
                                            <p>Account Status</p>
                                            <button class="btn pay_btn">Activate</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Organization: </td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Oriana</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Inactive</strong></span><span>Subscriptin:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong> 31-Jan-2022</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Manage Subscription</h4>
                                        <div className='pay-btn'>
                                            <p>Account Status</p>
                                            <button class="btn pay_btn">Activate</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Organization: </td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Oriana</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Inactive</strong></span><span>Subscriptin:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong> 31-Jan-2022</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className='row align-items-center'>
                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='action_area'>
                                <ul>
                                    <li><div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div></li>
                                    <li><img src='src/images/xls.svg' alt='' /></li>
                                    <li><img src='src/images/pdf.svg' alt='' /></li>
                                    <li><img src='src/images/csv.svg' alt='' /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="table-pay table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><div className="head-ttl">Date</div></th>
                                            <th><div className="head-ttl">Invoice Number</div></th>
                                            <th><div className="head-ttl">Payment Method</div></th>
                                            <th><div className="head-ttl">Status</div></th>
                                            <th><div className="head-ttl">Amount</div></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text hizl-item">
                                                    <p>$250</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text hizl-item">
                                                    <p>$250</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { CustomerManagement }; 