import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function Manage({ history, location }) {
    //Filter Dropdown
    function dropdown2() {
        var srcElement = document.getElementById('dropdown');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content super_admin_dashboard">
                    <div className="row align-items-center">
                        <div className='headings'>
                            <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                                <h3>Account View/Modify</h3>
                            </div>
                            <div className='col-xl-8 col-lg-8 col-md-12 col-sm-12'>
                                <div class="recipient-btn">
                                    <div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div>
                                    <div class="add_user">
                                        <button class="btn"><img src="src/images/plus_icon.svg" />Add New Customer</button>
                                    </div>
                                    <div class="action_area">
                                        <ul>
                                            <li><img src="src/images/xls.svg" alt="" /></li>
                                            <li><img src="src/images/pdf.svg" alt="" /></li>
                                            <li><img src="src/images/csv.svg" alt="" /></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="table__row__conts table-responsive">
                                <table className="table">
                                    <thead className='first_head'>
                                        <tr>
                                            <th><div className="top"> <div className="check_sec"> <input className="check" id="check3" type="checkbox" value="value3" /><label for="check3"></label><span><img src="src/images/down_arrow1.svg" alt="" /></span> </div><div className="Filters dropdown-toggle" onClick={dropdown2}> <span><img src="src/images/filters.svg" alt="" /></span>Filters <span><img src="src/images/down_arrow1.svg" alt="" /></span> <div id="dropdown" className="filter_dropdown dropdown-menu" style={{ display: 'none' }}> <ul> <li><a href="#"><span className="sign"></span>To Sign</a></li><li><a href="#"><span className="drafts"></span>Drafts</a></li><li><a href="#"><span className="invitation"></span>Invitation</a></li><li><a href="#"><span className="pending"></span>Pending</a></li><li><a href="#"><span className="completed"></span>Completed</a></li></ul> </div></div></div></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>1-4 on <span>856</span></th>
                                        </tr>
                                    </thead>
                                    <thead className='second_head'>
                                        <tr>
                                            <th>Id</th>
                                            <th>Organization</th>
                                            <th>Registered On</th>
                                            <th>Subscription</th>
                                            <th>Duration</th>
                                            <th> Status </th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Individual</td>
                                            <td>Monthly</td>
                                            <td>Active</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Company</td>
                                            <td>Annual</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Individual</td>
                                            <td>Monthly</td>
                                            <td>Active</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" checked />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input className="check" id="check1" type="checkbox" value="value1" /><label for="check1"> 2457</label></td>
                                            <td>Legal Network</td>
                                            <td>10/06/2021</td>
                                            <td>Enterprise</td>
                                            <td>Trial</td>
                                            <td>Inactive</td>
                                            <td>
                                                <div className='action_sec'>
                                                    <ul>
                                                        <li><img src='src/images/users.svg' /></li>
                                                        <li> <label className="switch">
                                                            <input type="checkbox" />
                                                            <span className="slider round"></span>
                                                        </label>
                                                        </li>
                                                        <li><img src='src/images/delete_icon.svg' /></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className="document-nagin">
                                <nav aria-label="Page navigation">
                                    <ul className="pagination">
                                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                                        <li className="page-item">
                                            <a className="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">•••</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Manage }; 