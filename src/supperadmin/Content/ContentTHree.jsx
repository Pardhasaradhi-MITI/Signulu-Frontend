import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import { TemplatesCard } from '../Content/Card/TemplatesCard';

function ContentTHree({ history, location }) {
    function explore_all_data() {
        console.log('content heare')
    }

  

    return (
        <div>
            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <h3>Recently Modified(Latest 10 Records)</h3>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div className="table__row__conts table-responsive">
                        <table className="table">
                            <tbody>
                                <TemplatesCard id="1" title="4 Pages Document-Converted" client="James Franco" date="10/06/2021" user="Tom Hanks" cssclass="invitation-btn" status="Invitation" detailsurl="#" />
                                <TemplatesCard id="2" title="4 Pages Document-Converted" client="James Franco" date="10/06/2021" user="Tom Hanks" status="Pending" cssclass="pending-btn" detailsurl="#" />
                                <TemplatesCard id="3" title="4 Pages Document-Converted" client="James Franco" date="10/06/2021" user="Tom Hanks" status="To Sign" cssclass="sign-btn" detailsurl="#" />
                                <TemplatesCard id="4" title="4 Pages Document-Converted" client="James Franco" date="10/06/2021" user="Tom Hanks" status="Completed" cssclass="completed-btn" detailsurl="#" />
                                <TemplatesCard id="5" title="4 Pages Document-Converted" client="James Franco" date="10/06/2021" user="Tom Hanks" status="Draft" cssclass="draft-btn" detailsurl="#" />
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <button type="button" className="explore_btn" onClick={explore_all_data}>Explore All</button>
                </div>
            </div>



        </div>
    )
}

export { ContentTHree }; 