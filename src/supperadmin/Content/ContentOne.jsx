import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';

function ContentOne({ history, location }) {
   //Upload
    function uploadDiv() {
        var srcElement = document.getElementById('upload');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    // Drag and Drop File Upload //
    document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
        const dropZoneElement = inputElement.closest(".use_templete.drop-zone");
      
        dropZoneElement.addEventListener("click", (e) => {
          inputElement.click();
        });
      
        inputElement.addEventListener("change", (e) => {
          if (inputElement.files.length) {
            updateThumbnail(dropZoneElement, inputElement.files[0]);
          }
        });
      
        dropZoneElement.addEventListener("dragover", (e) => {
          e.preventDefault();
          dropZoneElement.classList.add("drop-zone--over");
        });
      
        ["dragleave", "dragend"].forEach((type) => {
          dropZoneElement.addEventListener(type, (e) => {
            dropZoneElement.classList.remove("drop-zone--over");
          });
        });
      
        dropZoneElement.addEventListener("drop", (e) => {
          e.preventDefault();
      
          if (e.dataTransfer.files.length) {
            inputElement.files = e.dataTransfer.files;
            updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
          }
      
          dropZoneElement.classList.remove("drop-zone--over");
        });
      });
      
      /**
       * Updates the thumbnail on a drop zone element.
       *
       * @param {HTMLElement} dropZoneElement
       * @param {File} file
       */
      function updateThumbnail(dropZoneElement, file) {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
      
        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
          dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }
      
        // First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
          thumbnailElement = document.createElement("div");
          thumbnailElement.classList.add("drop-zone__thumb");
          dropZoneElement.appendChild(thumbnailElement);
        }
      
        thumbnailElement.dataset.label = file.name;
      
        // Show thumbnail for image files
        if (file.type.startsWith("image/")) {
          const reader = new FileReader();
      
          reader.readAsDataURL(file);
          reader.onload = () => {
            thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
          };
        } else {
          thumbnailElement.style.backgroundImage = null;
        }
      }

   
   
   
    return (
        <div>
            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <h3>What would you like to do?</h3>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
                    <div className="create-card-box-item">
                        <img src="src/images/document-1_icon.svg" alt="" />
                        <h6>Create Document</h6>
                        <p>Start a new document from scratch with enhanced editors with rich features.</p>
                    </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
                    <div className="create-card-box-item">
                        <img src="src/images/document-2_icon.svg" alt="" />
                        <h6>Create From Template</h6>
                        <p>Create a document by selecting a suitable template from a list of pre-defined templates.</p>
                    </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
                    <div className="create-card-box-item" onClick={uploadDiv}>
                        <img src="src/images/document-3_icon.svg" alt="" />
                        <h6>Upload Your Files</h6>
                        <p>Use Template, Google Drive, One Drive And Drop Box.</p>
                    </div>


              <div className="upload_sec" id="upload" style={{display: "none"}}>
                <div className="content">
                    <h4>Upload Your Files</h4>
                    <h5>Use Template, Google Drive, One Drive And Drop Box</h5>
                    <div className="upload_block">
                        <div className="use_templete">
                            <img src="src/images/folder.svg" alt='' />
                            <h3>Use Template</h3>
                            <h4>1,235 Files </h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/google_drive.svg" alt='' />
                            <h3>Google Drive</h3>
                            <h4>1,357</h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/one_drive.svg" alt='' />
                            <h3>One Drive</h3>
                            <h4>2,143 Files</h4>
                        </div>
                        <div className="use_templete">
                            <img src="src/images/drop_box.svg" alt='' />
                            <h3>Drop Box</h3>
                            <h4>1,457 Files</h4>
                        </div>
                        <div className="use_templete drop-zone">
                            <img src="src/images/folder_1.svg" alt className="img-fluid mx-auto d-block" />
                            <h4 className="drop-zone__prompt">Click To Upload or Drag &amp; Drop Your File Here</h4>
                            <input type="file" name="myFile" className="drop-zone__input" />
                        </div>
                    </div>
                    <div className="back">
                        <h6><span><img src="src/images/back.svg" alt='' /></span>Back</h6>
                    </div>
                </div>
            </div>
                </div>
            </div>
        </div>

    )


}



export { ContentOne }; 