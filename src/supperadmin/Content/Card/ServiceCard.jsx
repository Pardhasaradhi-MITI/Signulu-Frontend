import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';

function ServiceCard(props){
    return (
        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">
        <div className="create-card-box-item">
            <img src={props.image} alt="" />
            <h6>{props.title}</h6>
            <p>{props.description}</p>
        </div>
    </div>

    )
}
export { ServiceCard }; 