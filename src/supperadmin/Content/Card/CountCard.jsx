import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';

function CountCard(props){
    return (
            <div className="item">
            <img src={props.image} alt="" />
            <h5>{props.counter}</h5>
            <p>{props.title}</p>
           </div>

    )
}
export { CountCard }; 