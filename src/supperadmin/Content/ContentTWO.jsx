import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { accountService, alertService } from '@/_services';
import {CountCard } from '../Content/Card/CountCard';

function ContentTWO({ history, location }) {    
    return (
        <div>
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <h3>Dashboard</h3>
                    </div>
                </div>
                <div className="dashboard_item">

                <CountCard title="Action Required" image="src/images/document-4_icon.svg" counter="33"/>
                <CountCard title="pending" image="src/images/document-5_icon.svg" counter="46"/> 
                <CountCard title="Rejected Documents" image="src/images/document-6_icon.svg" counter="14"/> 
                <CountCard title="Draft Documents" image="src/images/document-7_icon.svg" counter="12"/> 
                <CountCard title="Completed" image="src/images/document-8_icon.svg" counter="58"/> 
                </div>
            </div>     

    )
}

export { ContentTWO }; 