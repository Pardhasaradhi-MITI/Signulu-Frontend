import React, {useState, useEffect } from 'react';
import { Link,useParams } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import Pagination from "react-responsive-pagination";
import * as EmailValidator from "email-validator";
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
// import { Topbar } from './Common/Topbar';
import { Topbar } from '../account/Common/Topbar';
import { CustomermanagementService } from '../_superadminservices/CustomerManagementservice';
import Tooltip from "@material-ui/core/Tooltip";
import moment from 'moment';
import Swal from 'sweetalert2';

function ViewModify({ history, location }) {
    const { CompanyId,PackageCode,PDurationMode ,UserId,NumberOfUsers} = useParams();
    const [userslist,setuserslist]=useState([])
    const [Accountdetails,setAccountdetails]=useState([])
    const [pageNumber,setPageNumber]=useState(1)
    const [pageSize,setPageSize]=useState(10)
    const [PageCount,setPageCount]=useState(1)
    const [Userdetails,setUserdetails]=useState([])
    const [PaymentDetails,setPaymentDetails]=useState([])
    //const [mpageNumber,setmPageNumber]=useState(1)
    //
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailAddress, setEmailAddress] = useState("");
    const [phone,setphone]=useState("");
    const [Role,setRole]=useState("");
    const [Designation,setDesignation] = useState("")
    const [Roleslist,setroleslist]=useState([])
    const [is_active,setis_active]=useState()
    const [error, setError] = useState({
        firstName: '',
        lastName: '',
        emailAddress: '',
        phone:'',
        Designation:'',
        Role: '',
    });
    let Individata = {"CompanyId":CompanyId, "UserId":UserId, "PageNumber": pageNumber, "PageSize": pageSize, "SortDir": "a", "SortKey": "name" };
    let data = { "CompanyId":CompanyId, "PageNumber": pageNumber, "PageSize": pageSize, "SortDir": "a", "SortKey": "name" };
    let obj = CompanyId === "null"? Individata:data
    let Paymentdatapayload = {"CompanyId":CompanyId==="null"?0:CompanyId, "UserId":UserId, "PageNumber": pageNumber, "PageSize": pageSize, "SortDir": "a", "SortKey": "name" };
    useEffect(()=>{
        getAccountDetails( )
        getpaymentdetails()
        getRoles()
    },[])
    const getRoles=()=>{
        CustomermanagementService.getRoles().then((result) => {
            setroleslist(result.Entity);
        })
        .catch(error => {
        });
       } 

    const getAccountDetails=(data)=>{
        CustomermanagementService.getcompanyuserslist(obj).then((result) => {
            setPageCount(result?.Entity?.PageCount)
            setAccountdetails((result?.Entity?.data).filter(obj =>{
               return obj.IsOwner === 1
            }))
            setuserslist((result?.Entity?.data).filter(obj =>{
                return obj.IsOwner !== 1
             }))
        }).catch(error => {
        });
    }
    const getpaymentdetails = ()=>{
        CustomermanagementService.getPaymentdetails(Paymentdatapayload).then((result)=>{
            if(result){
            setPaymentDetails(result?.Entity)}
        }).catch({

        })
    }

    //Tabs
    function openTab(event, divID) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(divID).style.display = "block";
        event.currentTarget.className += " active";
    }
    //Share Document Hide Show 
    function shareDoc() {
        var srcElement = document.getElementById('sharedoc');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    function addnewuser() {
        resetForm()
        var srcElement = document.getElementById('adduser');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    function Checkuser(){
          ;
        if(NumberOfUsers === "null" || parseInt(NumberOfUsers) === 1 ||  parseInt(NumberOfUsers) <= userslist.length ){
            Swal.fire({
                icon: 'info',
                title: 'No User Creadits ',
                showConfirmButton: true,
                confirmButtonText:"OKAY"
                //timer: 2000
              })
        }
        else{
            addnewuser();
        }
       
        
    }
    const settinguserdata=(item)=>{
        if(data!== ""){
        setFirstName(item.FirstName)
        setLastName(item.LastName)
        setEmailAddress(item.EmailAddress)
        setDesignation(item.Designation)
        setphone(item.Phone)
        setRole(item.RoleName)
        }
        Editnewuser();
    }
    function Editnewuser() {
        // if(item !==""){
        // setFirstName(item.firstName)
        // setLastName(item.LastName)
        // setEmailAddress(item.EmailAddress)
        // setDesignation(item.Designation)
        // setphone(item.phone)
        // setRole(item.Role)
        // }
        var srcElement = document.getElementById('Edituser');
        if (srcElement != null) {
            if (srcElement.style.display == "block") {
                srcElement.style.display = "none";
            } else {
                srcElement.style.display = "block";
            }
            return false;
        }
    }
    const handleUserInputChange = (feild, e) => {
        //  
        let value = e.target.value;
        let errors = error;
        switch (feild) {
            case 'firstName':
                if (value === "") {
                    errors.firstName = 'First Name is required.';
                } else if (value.length < 3) {
                    errors.firstName = 'First Name must be atleast 3 characters long.';
                } else {
                    errors.firstName = ''
                }
                setFirstName(value);
                setError(errors);
                break;
            case 'lastName':
                if (value === "") {
                    errors.lastName = 'Last Name is required.';
                } else {
                    errors.lastName = ''
                }
                setLastName(value);
                setError(errors);
                break;
            case 'emailAddress':
                //  
                if (value === "") {
                    errors.emailAddress = 'Email Address is required.';
                    setEmailAddress(value.toLowerCase());
                    setError(errors);
                } else if (EmailValidator.validate(value) === false) {
                    errors.emailAddress = 'Enter correct email address.';
                    setEmailAddress(value.toLowerCase());
                    setError(errors);
                } else if (EmailValidator.validate(value) === true) {
                    setEmailAddress(value.toLowerCase());
                    setError(errors);
                   CustomermanagementService.checkUserWithEmail(value).then(function (data) {
                        if (data.Entity.EmailAddress === value) {
                            errors.emailAddress = 'Email already exist with another user';
                        } else {
                            errors.emailAddress = '';
                        }

                        setEmailAddress(value.toLowerCase());
                        setError(errors);

                        // setError(errors);
                    })

                }

                break;
            case 'Designation':
                    if (value === "") {
                        errors.Designation = 'Designation is required.';
                    } else {
                        errors.Designation = ''
                    }
                    setDesignation(value);
                    setError(errors);
                    break;
            case 'Phone':
               //   
            if (value === ""){
                    errors.phone='Phone Number is required.'
                }
                else if(value && value.length < 8){
                    errors.phone='Phone Number Must at lest 8 digit.'
                }
                else if(value && value.length > 15){
                    errors.phone='Phone Number is less then 15 digit.'
                }
               else{
                    errors.phone=""
                }
                setphone(value)
                       setError(errors)
                 break;
                case 'Role':
                    if (value === "") {
                        errors.Role = 'Role is required.';
                    } else {
                        errors.Role = ''
                    }
                    setRole(value);
                    setError(errors);
                    break;
            default:
                break;
        }

    }
    const updateUser=()=>{
          
        let object ={
            FirstName: firstName,
            LastName: lastName,
            Password: "23706da299e3e29db9da58749e6969d6",
            ConfirmPassword: "23706da299e3e29db9da58749e6969d6",
            Email: emailAddress,
            Phone: phone,
            DurationMode: PDurationMode,
            RoleName: Role,
            PackageCode: PackageCode,
            Designation:Designation,
            CompanyId:CompanyId,
            IsActive:true,
            IsDomain: false,
            RoleId:1,
            UserId:UserId
           // RoleName: 'User'
        }
        const validateForm = () => {
           //   
            let count = 0;
            let tmp_errors = {...error};
            // if (firstName === "") {
            //     tmp_errors.firstName = 'First Name is required.';
            //     count = count + 1;
            // } else if (firstName.length < 3) {
            //     tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
            //     count = count + 1;
            // }
            // if (lastName === "") {
            //     tmp_errors.lastName = 'Last Name is required.';
            //     count = count + 1;
            // }
            // if (emailAddress === "") {
            //     tmp_errors.emailAddress = 'Email Address is required.';
            //     count = count + 1;
            // } else if (EmailValidator.validate(emailAddress) === false) {
            //     tmp_errors.emailAddress = 'Enter correct email address.';
            //     count = count + 1;
            // }
            if(phone === ""){
                tmp_errors.phone = "Phone is required.";
                count = count + 1;
            }else if(phone.length>=15){
                tmp_errors.phone='phone number must be less than equal to 15 digit.'
                count = count + 1;
            }
            if(Designation===""){
                tmp_errors.Designation = "Designation is required.";
                count = count + 1;
            }
            if(Role === ""){
                tmp_errors.Role = "Role is required."
                count = count + 1;
            }
           
            if (count <= 0) {
                tmp_errors = {
                    phone:'',
                    Designation:'',
                    Role: '',

                }
            }
            setError(tmp_errors);
            //  
            return count <= 0;
        }
        if(validateForm()){
            //  
            CustomermanagementService.edituserdetails(object).then(function (response){
                  
                if (response.HttpStatusCode === 200) {
                    alertService.success("Updated Successfully")
                       getAccountDetails()   
                       resetForm();
                       Editnewuser();
                }
                
            })
            .catch((err) => {
                console.log(err)
            })
        }

    }

    const SaveUser=()=>{
        //  
        let object ={
            FirstName: firstName,
            LastName: lastName,
            Password: "23706da299e3e29db9da58749e6969d6",
            ConfirmPassword: "23706da299e3e29db9da58749e6969d6",
            Email: emailAddress,
            Phone: phone,
            DurationMode: PDurationMode,
            RoleName: Role,
            PackageCode: PackageCode,
            Designation:Designation,
            CompanyId:CompanyId,
            IsActive:true,
            IsDomain: false,
            RoleId:1
           // RoleName: 'User'
        }
        const validateForm = () => {
           //   
            let count = 0;
            let tmp_errors = {...error};
            if (firstName === "") {
                tmp_errors.firstName = 'First Name is required.';
                count = count + 1;
            } else if (firstName.length < 3) {
                tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
                count = count + 1;
            }
            if (lastName === "") {
                tmp_errors.lastName = 'Last Name is required.';
                count = count + 1;
            }
            if (emailAddress === "") {
                tmp_errors.emailAddress = 'Email Address is required.';
                count = count + 1;
            } else if (EmailValidator.validate(emailAddress) === false) {
                tmp_errors.emailAddress = 'Enter correct email address.';
                count = count + 1;
            }
            if(phone === ""){
                tmp_errors.phone = "Phone is required.";
                count = count + 1;
            }else if(phone.length>=15){
                tmp_errors.phone='phone number must be less than equal to 15 digit.'
                count = count + 1;
            }
            if(Designation===""){
                tmp_errors.Designation = "Designation is required.";
                count = count + 1;
            }
            if(Role === ""){
                tmp_errors.Role = "Role is required."
                count = count + 1;
            }
           
            if (count <= 0) {
                tmp_errors = {
                    firstName: '',
                    lastName: '',
                    emailAddress: '',
                    phone:'',
                    Designation:'',
                    Role: '',

                }
            }
            setError(tmp_errors);
            //  
            return count <= 0;
        }
        if(validateForm()){
            //  
            CustomermanagementService.saveUser(object).then(function (response){
                //  
                if (response.HttpStatusCode === 200) {
                    alertService.success("Saved Successfully")
                       getAccountDetails(data)   
                       resetForm();
                       addnewuser();
                }
                
            })
            .catch((err) => {
                console.log(err)
            })
        }

    }
    const resetForm=()=>{
        setFirstName("")
        setLastName("")
        setEmailAddress("")
        setphone("")
        setDesignation("")
        setRole("")
    }
    const handlePagination = (newPage) => {
        setPageNumber(newPage);
       }
       const mhandlePagination = (newPage) => {
        setmPageNumber(newPage);
       }
    const Activation =(item)=>{
        let obj={
            IsActive:item.is_active ===1 ?0:1,
            IsOwner:item.IsOwner,
            RoleName:item.RoleName,
            RoleId:item.RoleId,
           // IsAdminAccount:1,
        }
        CustomermanagementService.ActivateAndDeactiveateUser(item.UserId,item.CompanyId,obj).then(res=>{
                    getAccountDetails(data)

        }).catch({

        })
    }
//get User info based on UserID
  const GetUserdetails =(item)=>{
      CustomermanagementService.getuserdetails(item.UserId).then(result=>{
        setUserdetails(result?.Entity)
      }).catch({

      });
  }

    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />
                <div className="back" style={{cursor:"pointer" }}>
                       <a onClick={history.goBack}> <h6 className='ml-7 mb-5' ><span ><img  src="src/images/back.svg" alt='' /></span>Back</h6></a>
                    </div>
                <div className="main-content main-from-content customermanagement">
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className='references'>
                                <div className='tab'>
                                    <ul className='navbar-tabs'>
                                        <li className="tablinks active" onClick={(e) => openTab(e, 'firstTab')}><button>Account Details</button></li>
                                        {PackageCode !=="IDIVIDUALUSER" && <li className="tablinks" onClick={(e) => openTab(e, 'secondTab')}><button>Manage User</button></li>}
                                        <li className="tablinks" onClick={(e) => openTab(e, 'thirdTab')}><button>Manage Subscription</button></li>
                                        {/* <li className="tablinks" onClick={(e) => openTab(e, 'fourthTab')}><button>Performance</button></li> */}
                                    </ul>
                                </div>
                                <div id="firstTab" className='tab-content' style={{ display: "block" }}>
                                    <div className='heading'>
                                        <h4>Account Details</h4>
                                        {/* <div className='edit-btns'>
                                            <button class="btn edit_btn" onClick={shareDoc}><span><img src='src/images/edit_icon.svg' /></span>Edit</button>
                                        </div> */}
                                    </div>
                                    <div className='account-details-table table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Customer Name:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].FirstName+" "+Accountdetails[0].LastName:"-"}</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td style={{textAlign:"end"}} ><strong>{Accountdetails.length>0?Accountdetails[0].RegisteredOn:"-"}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].EmailAddress:"-"}</strong></span><span>Phone On:</span></div><div></div></td>
                                                    <td className='text-right'><strong>{Accountdetails.length>0?Accountdetails[0].Phone:"-"}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Organization:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].CompanyName:"-"}</strong></span><span>Subscription:</span></div><div></div></td>
                                                    <td className='text-right'><strong>{Accountdetails.length>0?Accountdetails[0].Package:"-"}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].is_active === 1?"Active":"InActive":"-"}</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>{Accountdetails.length>0?Accountdetails[0].DurationMode:"-"}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].EffectiveUntil:"-"}</strong></span><span>Role:</span></div><div></div></td>
                                                    <td className='text-right'><strong>{Accountdetails.length>0?Accountdetails[0].RoleName:"-"}</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    {/* <div id="sharedoc" className='share_document' style={{ display: 'none',maxWidth:1000 }}>
                                        <div className='flex'>
                                        <p>User Details</p>
                                        <div style={{backgroundColor:"#E0E1E3",maxHeight:20,borderRadius:6,width:20}} className={"modal-heading d-flex justify-content-end"}>
                                            <a href='javascript:void(0)'
                                            onClick={shareDoc}
                                            >
                                                <img className='p-1 -mt-1' src='src/images/close_icon.svg' alt=''/>

                                            </a>
                                        </div>
                                        <div className='edit-btns'>
                                            <button class="btn edit_btn" onClick={shareDoc}><span><img src='src/images/edit_icon.svg' /></span></button>
                                        </div>
                                        </div>
                                        <div className='row'>
                                            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                <div className='tab-content'>
                                                    <div className='personal-information'>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder=' First Name*' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder=' Last Name*' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder='Email Address*' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder=' Phone*' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder='Designation' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <select>
                                                                            <option>User</option>
                                                                        </select>
                                                                        <label for="">Role<span>*</span> </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                <div class="subf_pagn_btn">
                                                                    <button class="btn">Register<span><img src="src/images/right_arrow.svg" /></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                    <div className='row align-items-center'>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                            <h3>User Details</h3>
                                        </div>
                                        <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                            <div class="recipient-btn">
                                                {/* <div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div> */}
                                                {/* <div class="add_user">
                                                    <button class="btn" onClick={addnewuser}><img src="src/images/plus_icon.svg" />Add User</button>
                                                </div> */}
                                                <div class="action_area">
                                                    <ul>
                                                        <li><img src="src/images/xls.svg" alt="" /></li>
                                                        <li><img src="src/images/pdf.svg" alt="" /></li>
                                                        <li><img src="src/images/csv.svg" alt="" /></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                        {userslist.length>0? <div className="user-details-table table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th className='px-4 py-0' style={{fontSize:15}}>User Name </th>
                                                            <th className='px-4 py-0' style={{fontSize:15}}>Designation</th>
                                                            <th className='px-4 py-0' style={{fontSize:15}}>Email</th>
                                                            <th className='px-4 py-0' style={{fontSize:15}}>Contact No</th>
                                                            <th className='px-4 py-0' style={{fontSize:15}}>User Type</th>
                                                            <th className='px-4 py-0' style={{fontSize:15,color:"#515762",fontWeight:400}}>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {userslist.length>0?userslist.map(item=>{
                                                           return(
                                                            <tr>
                                                            <td className='px-4' style={{fontSize:17}}>
                                                                {item.FirstName+" "+ item.LastName}
                                                            </td>
                                                            <td className='px-4' style={{fontSize:17}}>{item.Designation}</td>
                                                            <td className='px-4' style={{fontSize:17}}>
                                                                {item.EmailAddress}
                                                            </td>
                                                            <td className='px-4' style={{fontSize:17}}>{item.Phone}</td>
                                                            <td className='px-4' style={{fontSize:17}}>{item.RoleName}</td>
                                                            <td className='px-4' style={{fontSize:17}}>{item.is_active === 1?"Active":"InActive"}</td>
                                                        </tr>
                                                           )
                                                        })
                                                        :
                                                       ""}
                                                      
                                                    </tbody>
                                                </table>
                                            </div>: <div className={"text-center mt-4"}> No records are found.</div>}
                                        </div>
                                    </div>
                                    {/* <div id="adduser1" className='share_document1 ' style={{ display: 'none',maxWidth:1000,zIndex:4}}>
                                    <h2>Add New User</h2>
                                        <div className='flex'>
                                        <p>User Details</p>
                                        <div style={{backgroundColor:"#E0E1E3",maxHeight:20,borderRadius:6,width:20}} className={"modal-heading d-flex justify-content-end"}>
                                            <a href='javascript:void(0)'
                                            onClick={addnewuser}
                                            >
                                                <img className='p-1 -mt-1' src='src/images/close_icon.svg' alt=''/>

                                            </a>
                                        </div>
                                        </div>
                                        
                                        <div className='row '>
                                            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                <div className='tab-content'>
                                                    <div className='personal-information'>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                    <input autoComplete="off" type="text"placeholder=' First Name*'  value={firstName}  onChange={(e)=>handleUserInputChange('firstName',e)} />
                                                                    <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.firstName}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                      
                                                                        <input autoComplete="off" type="text"placeholder=' Last Name*'  value={lastName}  onChange={(e)=>handleUserInputChange('lastName',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.lastName}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                       
                                                                        <input autoComplete="off" type="text" placeholder=' Email Address*'  value={emailAddress}  onChange={(e)=>handleUserInputChange('emailAddress',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.emailAddress}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder=' Phone*' />
                                                                        <input autoComplete="off" type="number"placeholder=' Phone*' value={phone}  onChange={(e)=>handleUserInputChange('Phone',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.phone}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        <input type="text" placeholder='Designation' />
                                                                        <input autoComplete="off" type="text"placeholder=' Designation*'  value={Designation}  onChange={(e)=>handleUserInputChange('Designation',e)} />
                                                                         <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.Designation}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                    <select value={Role}
                                                                    onChange={(e)=>handleUserInputChange('Role',e)}
												               
                                                                >
                                                                <option>Choose...</option>
                                                                {Roleslist.map(item=>{
                                                                    return(
                                                                <option value={item.RoleCode}>{item.RoleName}</option>)
                                                                })}
                                                            </select>
                                                                        <label for="">Role<span>*</span> </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                <div class="subf_pagn_btn">
                                                                    <button onClick={SaveUser} class="btn">SaveUser<span><img src="src/images/right_arrow.svg" /></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                    {userslist.length>0?<div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
											<Pagination
												current={pageNumber}
												total={PageCount}
												onPageChange={(newPage) => handlePagination(newPage)}
                                                maxWidth={"80px"}
											/>
										</nav>
									</div>
								</div>
							</div>:""}
                                    {/* <div className='row align-items-center'>
                                        <div className='heading'>
                                            <h4>Activities</h4>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <div className='document_block'>
                                                <div className='spcingg'>
                                                    <h3><strong>Users Status</strong></h3>
                                                    <div className="align">
                                                        <select className="hlfinput">
                                                            <option>Dec 21</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className='chart'>
                                                    <div className='chart_picture'>
                                                        <img src='src/images/chart_10.svg' alt='' />
                                                    </div>
                                                    <div className='chart_content'>
                                                        <h6>Total Users</h6>
                                                        <h5>564</h5>
                                                        <ul>
                                                            <li><span className='active'></span>Added New Users</li>
                                                            <li><span className='rejected'></span>Active Users</li>
                                                            <li><span className='cancelled'></span>Inactive Users</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <div className='document_block'>
                                                <div className='spcingg'>
                                                    <h3><strong>Document Status</strong></h3>
                                                    <div className="align">
                                                        <select className="hlfinput">
                                                            <option>Dec 21</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className='chart'>
                                                    <div className='chart_picture'>
                                                        <img src='src/images/chart_11.svg' alt='' />
                                                    </div>
                                                    <div className='chart_content'>
                                                        <h6>Total Document</h6>
                                                        <h5>956</h5>
                                                        <div className='spacing'>
                                                            <ul>
                                                                <li><span className='review'></span>Draft</li>
                                                                <li><span className='rejected1'></span>Pending</li>
                                                                <li><span className='active1'></span>Approved</li>
                                                            </ul>
                                                            <ul>
                                                                <li><span className='rejected2'></span>Rejected</li>
                                                                <li><span className='cancelled1'></span>Cancelled</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='heading'>
                                            <h4>Document Process Comparison </h4>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className="align">
                                            <select className="hlfinput">
                                                <option>Dec 21</option>
                                            </select>
                                            <select className="hlfinput">
                                                <option>Jan 21</option>
                                            </select>
                                        </div>
                                        <div className='line-chart'>
                                            <img src='src/images/line-chart.svg' />
                                        </div>
                                    </div> */}
                                </div>

                                <div id="secondTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='row'>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                            <h3>Manage User</h3>
                                        </div>
                                        <div className='col-xl-8 col-lg-8 col-md-8 col-sm-12'>
                                            <div class="recipient-btn">
                                                {/* <div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div> */}
                                                <div class="add_user">
                                                    <button class="btn" onClick={Checkuser} ><img src="src/images/plus_icon.svg" />Add New User</button>
                                                </div>
                                                <div class="action_area">
                                                    <ul>
                                                        <li><img src="src/images/xls.svg" alt="" /></li>
                                                        <li><img src="src/images/pdf.svg" alt="" /></li>
                                                        <li><img src="src/images/csv.svg" alt="" /></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row align-items-center'>
                                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                        {userslist.length>0?  <div className="CommonTable table-responsive mt-4">
                                            <table class="table">
                                                    <thead>
                                                        <tr >
                                                            <th className='px-4 py-3'  style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">User Name</th>
                                                            <th className='px-4 py-3' style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">Designation</th>
                                                            <th className='px-4 py-3' style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">Email</th>
                                                            <th className='px-4 py-3' style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">Contact No</th>
                                                            <th className='px-4 py-3' style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">User Type</th>
                                                            <th className='px-4 py-3' style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">Status</th>
                                                            <th className='px-4 py-3' style={{border:"1px solid #d0d0d0", backgroundColor:"#DEDEE766",fontSize:15}} scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {userslist.length>0?userslist.map(item=>{
                                                            return(
                                                             <tr>
                                                             <td style={{fontSize:17 ,paddingLeft:16}}>{item.FirstName + " "+item.LastName}</td>
                                                             <td style={{fontSize:17 ,paddingLeft:16}}>{item.Designation}</td>
                                                             <td style={{fontSize:17 ,paddingLeft:16}}>{item.EmailAddress}</td>
                                                             <td style={{fontSize:17 ,paddingLeft:16}}>{item.Phone}</td>
                                                             <td style={{fontSize:17 ,paddingLeft:16}}>{item.RoleCode}</td>
                                                             <td style={{fontSize:17 ,paddingLeft:16}}>{item.is_active === 1?"Active":"InActive"}</td>
                                                             <td style={{fontSize:17 ,paddingLeft:16}} className='flex'>
                                                             <Tooltip title="Edit" placement="left" style={{fontSize:18,pading:4}}>
                                                             <div onClick={()=>{Editnewuser,settinguserdata(item)}} className='edit-btns' style={{cursor:"pointer"}}>
                                                                <img  src='src/images/edit_icon.svg' />
                                                            </div>
                                                             </Tooltip>
                                                             
                                                                 <div className='action_sec mr-2'>
                                                                     <ul>
                                                                         <li> <label className="switch">
                                                                             <input onClick={()=>Activation(item)} type="checkbox"  checked={item.is_active === 1 ? 'checked' : ''} />
                                                                             <span className="slider round"></span>
                                                                         </label>
                                                                         </li>
                                                                     </ul>
                                                                 </div>
                                                             </td>
                                                           
                                                         </tr>)
                                                        })
                                                        
                                                        :""}
                                                    </tbody>
                                                </table>
                                               
                                            </div>
                                            :<div className={"text-center mt-4"}> No records are found.</div>}
                                        </div>
                                    </div>
                                    <div id="adduser" className='share_document' style={{ display: 'none',maxWidth:1000,zIndex:4}}>
                                        {/* <h2>Add New User</h2> */}
                                        <>
                                        <div className='flex'>
                                        <p>User Details</p>
                                        <div style={{backgroundColor:"rgb(153 156 160)",maxHeight:30,borderRadius:15,width:30}} className={"modal-heading d-flex justify-content-end"}>
                                            <a href='javascript:void(0)'
                                            onClick={addnewuser}
                                            >
                                                <img className='p-2 ' src='src/images/close_icon.svg' alt=''/>

                                            </a>
                                        </div>
                                        </div>
                                        
                                        <div className='row displyblock'>
                                            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                <div className='tab-content' >
                                                    <div className='personal-information'>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                    <input autoComplete="off" type="text"placeholder=' First Name*'  value={firstName}  onChange={(e)=>handleUserInputChange('firstName',e)} />
                                                                    <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.firstName}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder=' Last Name*' /> */}
                                                                        <input autoComplete="off" type="text"placeholder=' Last Name*'  value={lastName}  onChange={(e)=>handleUserInputChange('lastName',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.lastName}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder='Email Address*' /> */}
                                                                        <input autoComplete="off" type="text" placeholder=' Email Address*'  value={emailAddress}  onChange={(e)=>handleUserInputChange('emailAddress',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.emailAddress}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder=' Phone*' /> */}
                                                                        <input autoComplete="off" type="number"placeholder=' Phone*' value={phone}  onChange={(e)=>handleUserInputChange('Phone',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.phone}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder='Designation' /> */}
                                                                        <input autoComplete="off" type="text"placeholder=' Designation*'  value={Designation}  onChange={(e)=>handleUserInputChange('Designation',e)} />
                                                                         <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.Designation}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                    <select value={Role}
                                                                    onChange={(e)=>handleUserInputChange('Role',e)}
												               // onChange={(e) => setRole(e.target.value)}
                                                                >
                                                                <option>Choose...</option>
                                                                {Roleslist.map(item=>{
                                                                    return(
                                                                <option  disabled={`${item.RoleId === 3 ||item.RoleId === 4 ||item.RoleId === 5?"disabled":""}`} value={item.RoleName}>{item.RoleName}</option>)
                                                                })}
                                                            </select>
                                                                        <label for="">Role<span>*</span> </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                <div class="subf_pagn_btn">
                                                                    <button onClick={SaveUser} class="btn">SAVE<span><img src="src/images/right_arrow.svg" /></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </>
                                    </div>
                                    <div id="Edituser" className='share_document' style={{ display: 'none',maxWidth:1000,zIndex:4}}>
                                        {/* <h2>Add New User</h2> */}
                                        <>
                                        <div className='flex'>
                                        <p>User Details</p>
                                        <div style={{backgroundColor:"rgb(153 156 160)",maxHeight:30,borderRadius:15,width:30}} className={"modal-heading d-flex justify-content-end"}>
                                            <a href='javascript:void(0)'
                                            onClick={Editnewuser}
                                            >
                                                <img className='p-2 ' src='src/images/close_icon.svg' alt=''/>

                                            </a>
                                        </div>
                                        </div>
                                        
                                        <div className='row displyblock'>
                                            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                                <div className='tab-content' >
                                                    <div className='personal-information'>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                    <input  style={{backgroundColor:"#e9ecef"}} disabled = {true} autoComplete="off" type="text"placeholder=' First Name*'  value={firstName} />
                                                                    <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.firstName}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder=' Last Name*' /> */}
                                                                        <input style={{backgroundColor:"#e9ecef"}} disabled = {true} autoComplete="off" type="text"placeholder=' Last Name*'  value={lastName}  />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.lastName}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder='Email Address*' /> */}
                                                                        <input style={{backgroundColor:"#e9ecef"}} disabled = {true} autoComplete="off" type="text" placeholder=' Email Address*'  value={emailAddress} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.emailAddress}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder=' Phone*' /> */}
                                                                        <input autoComplete="off" type="number"placeholder=' Phone*' value={phone}  onChange={(e)=>handleUserInputChange('Phone',e)} />
                                                                        <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.phone}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                        {/* <input type="text" placeholder='Designation' /> */}
                                                                        <input autoComplete="off" type="text"placeholder=' Designation*'  value={Designation}  onChange={(e)=>handleUserInputChange('Designation',e)} />
                                                                         <p style={{lineHeight:2,fontSize:15}} className={"error-msg"}>{error.Designation}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                                <div className="form-group">
                                                                    <div className="input-wrapper">
                                                                    <select value={Role}
                                                                    onChange={(e)=>handleUserInputChange('Role',e)}
												               // onChange={(e) => setRole(e.target.value)}
                                                                >
                                                                <option>Choose...</option>
                                                                {Roleslist.map(item=>{
                                                                    return(
                                                                <option  disabled={`${item.RoleId === 3 ||item.RoleId === 4 ||item.RoleId === 5?"disabled":""}`} value={item.RoleName}>{item.RoleName}</option>)
                                                                })}
                                                            </select>
                                                                        <label for="">Role<span>*</span> </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                <div class="subf_pagn_btn">
                                                                    <button onClick={updateUser} class="btn">UPDATE<span><img src="src/images/right_arrow.svg" /></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </>
                                    </div>
                                    {userslist.length>0? <div className="row">
								<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div className="document-nagin">
										<nav aria-label="Page navigation">
											<Pagination
												current={pageNumber}
												total={PageCount}
												onPageChange={(newPage) => handlePagination(newPage)}
                                                maxWidth={"80px"}
											/>
										</nav>
									</div>
								</div>
							</div>:""}
                                    {/* <div className='row align-items-center'>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="document-nagin">
                                                <nav aria-label="Page navigation">
                                                    <ul class="pagination">
                                                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                        <li class="page-item">
                                                            <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">•••</span></a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>

                                    </div> */}
                                </div>

                                <div id="thirdTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Manage Subscription</h4>
                                        <div className='pay-btn'>
                                            <p className='-ml-2'>Account Status</p>
                                            <button class="btn pay_btn">ACTIVATE</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Organization: </td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].CompanyName:"-"}</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td style={{textAlign:"end"}} ><strong>{Accountdetails.length>0?Accountdetails[0].RegisteredOn:"-"}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].is_active === 1?"Active":"InActive":"-"}</strong></span><span>Subscriptin:</span></div><div></div></td>
                                                    <td className='text-right'><strong>{Accountdetails.length>0?Accountdetails[0].Package:"-"}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>{Accountdetails.length>0?Accountdetails[0].EffectiveUntil:"-"}</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>{Accountdetails.length>0?Accountdetails[0].DurationMode:"-"}</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className='row align-items-center'>
                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                        </div>
                        <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12'>
                            <div className='action_area'>
                                <ul>
                                    <li><div class="search"><img src="src/images/search_icon_blue.svg" alt="" /></div></li>
                                    <li><img src='src/images/xls.svg' alt='' /></li>
                                    <li><img src='src/images/pdf.svg' alt='' /></li>
                                    <li><img src='src/images/csv.svg' alt='' /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="table-pay table-responsive">
                            {PaymentDetails.length>0?
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><div className="head-ttl">Date</div></th>
                                            <th><div className="head-ttl">Invoice Number</div></th>
                                            <th><div className="head-ttl">Validity</div></th>
                                            <th><div className="head-ttl">Status</div></th>
                                            <th><div className="head-ttl">Amount</div></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {PaymentDetails.length>0?PaymentDetails.map(item =>{
                                            return(
                                                <tr>
                                                <td>
                                                    <div className="text -my-1" >
                                                        <p>{moment(item.PaymentDate).format('Do MMM YYYY')}</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="text -my-1">
                                                        <p>{item.ReferenceNum}</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="text -my-1">
                                                        {/* <img src="src/images/visa_table.svg" alt="img" /> */}
                                                        <p>{moment(item.BillFrom).format('Do MMM YYYY')} - {moment(item.BillTo).format('Do MMM YYYY')}</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="text -my-1">
                                                        <button style={{width:"100%"}} className="btn btn-paid">{item.PaymentStatus}</button>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="text hizl-item -my-1">
                                                        <p>{(String.fromCharCode(parseInt(item.UnicodeText)))+" "+item.BillAmount}</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            )
                                        })
                                       
                                        :""}
                                        {/* <tr>
                                            <td>
                                                <div className="text -my-1">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text -my-1">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text -my-1">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text -my-1">
                                                    <button style={{width:"100%"}} className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text hizl-item -my-1">
                                                    <p>$250</p>
                                                </div>
                                            </td>
                                        </tr> */}
                                    </tbody>
                                </table>:
                                <div className={"text-center mt-4"}> No records are found.</div>}
                            </div>
                        </div>
                    </div>
                                </div>

                                <div id="fourthTab" className='tab-content' style={{ display: "none" }}>
                                    <div className='heading'>
                                        <h4>Account Details</h4>
                                        <div className='edit-btns'>
                                            <button class="btn edit_btn"><span><img src='src/images/edit_icon.svg' /></span>EDIT</button>
                                        </div>
                                    </div>
                                    <div className='table-responsive'>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Customer Name:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Daniel Farrell</strong></span><span>Registered On:</span></div><div></div></td>
                                                    <td><strong>26-Jun-2021</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>hello.farrell@gmail.com</strong></span><span>Phone On:</span></div><div></div></td>
                                                    <td className='text-right'><strong>9831000555</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Organization:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>MITI TEAM</strong></span><span>Subscription:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Company</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Account Status:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>Active</strong></span><span>Subscription Duration:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Yearly</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry:</td>
                                                    <td colSpan={2}><div className='register_sec'><span><strong>20-Nov-2022</strong></span><span>Role:</span></div><div></div></td>
                                                    <td className='text-right'><strong>Administrator</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    )
}

export { ViewModify }; 