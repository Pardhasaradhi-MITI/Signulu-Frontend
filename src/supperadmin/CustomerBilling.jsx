import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
import { Topbar } from './Common/Topbar';

function CustomerBilling({ history, location }) {
    return (


        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content main-from-content customerbilling">
                    <div className='row align-items-center'>
                    <div className='heading'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div className='flex'>
                                <div><h3>Customer Billing</h3></div>
                             <div className='flex'>
                             {/*   <h3><button onClick={()=>history.push('/supperadmin/dashboard')} className="btn back_btn"
                              style={{padding:9,border:"1px solid #d0e1fd"}}
                            ><img src="src/images/back_btn.svg"  alt="" />Back</button> 
                            </h3>*/}
                            <h3><button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"
                              style={{padding:"10px 10px",border:"1px solid #d0e1fd",height:"40px",fontSize:"12px"}}
                            ><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",marginBottom:"6px",width: "15px",height: "15px"}}/></button>
                            </h3></div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div className="table-pay-select">
                                <form action="#">
                                    <div className="date_fil">
                                        <div className='from-group hlfinput'>
                                            {/* <input type="text" placeholder='22-12-21' />
                                            <span className='icon'><img src='src/images/date.svg' alt='' /></span> */}
                                            <select className="hlfinput">
                                                <option selected>22-12-21</option>
                                                <option value={1}>23-12-21</option>
                                                <option value={2}>24-12-21</option>
                                                <option value={3}>25-12-21</option>
                                            </select>
                                            <span className='icon'><img src='src/images/date.svg' alt='' /></span>
                                        </div>
                                        <span>to</span>
                                        <div className='from-group hlfinput'>
                                            {/* <input type="text" placeholder='28-01-22' /> */}
                                            <select className="hlfinput">
                                                <option selected>28-12-21</option>
                                                <option value={1}>29-12-21</option>
                                                <option value={2}>30-12-21</option>
                                                <option value={3}>31-12-21</option>
                                            </select>
                                            <span className='icon'><img src='src/images/date.svg' alt='' /></span>
                                        </div>
                                    </div>
                                    <div className="text-fil">
                                        <input type="text" name id placeholder="Invoice Number" />
                                        <select className="hlfinput">
                                            <option selected>More Filters</option>
                                            <option value={1}>One</option>
                                            <option value={2}>Two</option>
                                            <option value={3}>Three</option>
                                        </select>
                                        <div className="search">
                                            <input type="text" placeholder="Search" />
                                            <button type="submit" className="btn" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="table-pay table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><div className="head-ttl" style={{textAlign:"start",paddingLeft:6}}>Date</div></th>
                                            <th><div className="head-ttl">Invoice Number</div></th>
                                            <th><div className="head-ttl">Payment Method</div></th>
                                            <th><div className="head-ttl">Status</div></th>
                                            <th><div className="head-ttl">Amount</div></th>
                                            <th><div className="head-ttl">Action</div></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="text">
                                                    <p>28th Jan 22</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <p>C9E6C569913</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <img src="src/images/visa_table.svg" alt="img" />
                                                    <p>******6605</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="text">
                                                    <button className="btn btn-paid">Paid</button>
                                                </div>
                                            </td>
                                            <td>
                                                <p>$250</p>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li><img src="src/images/eye.svg" alt="" /></li>
                                                    <li><img src="src/images/download.svg" alt="" /></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className='row align-items-center'>
                        <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                            <div class="document-nagin"><nav aria-label="Page navigation"><ul class="pagination"><li class="page-item"><a class="page-link active" href="#">1</a></li><li class="page-item"><a class="page-link" href="#">2</a></li><li class="page-item"><a class="page-link" href="#">3</a></li><li class="page-item"><a class="page-link" href="#">4</a></li><li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">•••</span></a></li></ul></nav></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { CustomerBilling }; 