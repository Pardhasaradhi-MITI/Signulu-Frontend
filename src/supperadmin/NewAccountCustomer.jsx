import React, { useState,useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';
import * as EmailValidator from "email-validator";
import{CustomermanagementService} from "../_superadminservices/CustomerManagementservice"
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Sidebar } from './Common/Sidebar';
// import { Topbar } from './Common/Topbar';
import { Topbar } from '../account/Common/Topbar';

function NewAccountCustomer({ history, location }) {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailAddress, setEmailAddress] = useState("");
    const [phone,setphone]=useState("");
    const [Organization,setOrganization]=useState("");
    const [SubscriptionDuration,setSubscriptionDuration]=useState("");
    const [SubscriptionType,setSubscriptionType]=useState(""); 
    const [Role,setRole]=useState("");
    const [subscriptiontypelist,setsubscriptiontypelist ] = useState([])
    const [Roleslist,setroleslist]=useState([])
    const [error, setError] = useState({
        firstName: '',
        lastName: '',
        emailAddress: '',
        phone:'',
        Organization: '',
        SubscriptionDuration: '',
        SubscriptionType: '',
        Role: '',
    });

    useEffect(()=>{
        getSubscriptionType(),
        getRoles()
    },[])
    const getSubscriptionType=()=>{
        CustomermanagementService.getSubscriptionType().then((result) => {
            setsubscriptiontypelist(result.Entity);
        })
        .catch(error => {
        });
       }  
       const getRoles=()=>{
        CustomermanagementService.getRoles().then((result) => {
            setroleslist(result.Entity);
        })
        .catch(error => {
        });
       }  
 
    const handleInputChange = (feild, e) => {
        //    
        let value = e.target.value;
        let errors = error;
        switch (feild) {
            case 'firstName':
                if (value === "") {
                    errors.firstName = 'First Name is required.';
                } else if (value.length < 3) {
                    errors.firstName = 'First Name must be atleast 3 characters long.';
                } else {
                    errors.firstName = ''
                }
                setFirstName(value);
                setError(errors);
                break;
            case 'lastName':
                if (value === "") {
                    errors.lastName = 'Last Name is required.';
                } else {
                    errors.lastName = ''
                }
                setLastName(value);
                setError(errors);
                break;
            case 'emailAddress':
                //    
                if (value === "") {
                    errors.emailAddress = 'Email Address is required.';
                    setEmailAddress(value.toLowerCase());
                    setError(errors);
                } else if (EmailValidator.validate(value) === false) {
                    errors.emailAddress = 'Enter correct email address.';
                    setEmailAddress(value.toLowerCase());
                    setError(errors);
                } else if (EmailValidator.validate(value) === true) {
                    setEmailAddress(value.toLowerCase());
                    setError(errors);
                   CustomermanagementService.checkUserWithEmail(value).then(function (data) {
                        if (data.Entity.EmailAddress === value) {
                            errors.emailAddress = 'Email already exist with another user';
                        } else {
                            errors.emailAddress = '';
                        }

                        setEmailAddress(value.toLowerCase());
                        setError(errors);

                        // setError(errors);
                    })

                }

                break;
            case 'Phone':
                if (value === ""){
                    errors.phone='Phone Number is required.'
                }
                else if(value && value.length < 10){
                    errors.phone='Phone Number Must at lest 10 digit.'
                }
                else if(value && value.length > 15){
                    errors.phone='Phone Number is less then 15 digit.'
                }
               else{
                    errors.phone=""
                }
                setphone(value)
                       setError(errors)
                 break;
            //    if (value!== "e"){
            //        // value.slice(-1).which.preventDefault();
            //        if (value.length === 1 || value !== "" ){
            //            setCellPhone(value)
            //            setError(errors)
            //        }

            //    }
            case 'oraganization':
                if (value === "") {
                    errors.Organization = 'Organization is required.';
                } else {
                    errors.Organization = ''
                }
                setOrganization(value);
                setError(errors);
                break;
                case 'SubscriptionDuration':
                if (value === "") {
                    errors.SubscriptionDuration = 'SubscriptionDuration is required.';
                } else {
                    errors.SubscriptionDuration = ''
                }
                setSubscriptionDuration(value);
                if(value=== "Trial"){
                    setSubscriptionType("IDIVIDUALUSER");
                    setRole("user");
                }
                setError(errors);
                break;
                case 'SubscriptionType':
                if (value === "") {
                    errors.SubscriptionType = 'SubscriptionType is required.';
                } else {
                    errors.SubscriptionType = ''
                }
                if(value === "COMPANY" || value === "ENTERPRISE"){
                    setRole("administrator");
                }
                if(value === "IDIVIDUALUSER"){
                    setRole("user");
                }
                setSubscriptionType(value);
                setError(errors);
                break;
                case 'Role':
                    if (value === "") {
                        errors.Role = 'Role is required.';
                    } else {
                        errors.Role = ''
                    }
                    setRole(value);
                    setError(errors);
                    break;


            default:
                break;
        }

    }

    const SaveRegister=()=>{
        //    
        let object ={
            FirstName: firstName,
            LastName: lastName,
            Password: "23706da299e3e29db9da58749e6969d6",
            ConfirmPassword: "23706da299e3e29db9da58749e6969d6",
            Email: emailAddress,
            Phone: phone,
            CompanyName: Organization,
            DurationMode: SubscriptionDuration,
            Role: Role,
            PackageCode: SubscriptionType
        }
        
        const validateForm = () => {
           //     
            let count = 0;
            let tmp_errors = {...error};
            if (firstName === "") {
                tmp_errors.firstName = 'First Name is required.';
                count = count + 1;
            } else if (firstName.length < 3) {
                tmp_errors.firstName = 'First Name must be atleast 3 characters long.';
                count = count + 1;
            }
            if (lastName === "") {
                tmp_errors.lastName = 'Last Name is required.';
                count = count + 1;
            }
            if (emailAddress === "") {
                tmp_errors.emailAddress = 'Email Address is required.';
                count = count + 1;
            } else if (EmailValidator.validate(emailAddress) === false) {
                tmp_errors.emailAddress = 'Enter correct email address.';
                count = count + 1;
            }
            if(phone === ""){
                tmp_errors.phone = "Phone is required.";
                count = count + 1;
            }else if(phone.length>=15){
                tmp_errors.phone='phone number must be less than equal to 15 digit.'
                count = count + 1;
            }
            if(SubscriptionType!=="IDIVIDUALUSER" && Organization === ""){
                tmp_errors.Organization = "Organization is required."
                count = count + 1;
            }
            if(SubscriptionDuration === ""){
                tmp_errors.SubscriptionDuration = "Subscription Duration is required."
                count = count + 1;
            }
            if(SubscriptionType === ""){
                tmp_errors.SubscriptionType = "Subscription is required."
                count = count + 1;
            }
            if(Role === ""){
                tmp_errors.Role = "Role is required."
                count = count + 1;
            }
           
            if (count <= 0) {
                tmp_errors = {
                    firstName: '',
                    lastName: '',
                    emailAddress: '',
                    phone:'',
                    Organization: '',
                    SubscriptionDuration: '',
                    SubscriptionType: '',
                    Role: '',

                }
            }
            setError(tmp_errors);
            //    
            return count <= 0;
        }
        if(validateForm()){
            //    
            CustomermanagementService.saveRegistration(object).then(function (response){
                //    
                if (response.HttpStatusCode === 200) {
                    alertService.success("Registered Successfully")
                   // setprofilepicerror("")
                    //selectUser();
                    // resetForm();
                       resetForm();
                }
                
            })
            .catch((err) => {
                console.log(err)
            })
        }

    }
    const resetForm=()=>{
        setFirstName("")
        setLastName("")
        setEmailAddress("")
        setphone("")
        setOrganization("")
        setSubscriptionDuration("")
        setSubscriptionType("")
        setRole("")
    }


    return (

        <div className="dashboard_home wrapper_other" id="wrapper_other">
            <Sidebar />

            <div className="main other_main super_admin_main" id="main">
                <Topbar />

                <div className="main-content main-from-content newaccountcustomer">
                    <div className="row align-items-center">
                        <div className='heading'>
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div className='flex'>
                                <div><h3>Add New Customer</h3></div> 
                                <div className='flex'>
                                    {/* <h3><button onClick={()=>history.push('/supperadmin/dashboard')} className="btn back_btn"
                              style={{padding:9,border:"1px solid #d0e1fd"}}
                            ><img src="src/images/back_btn.svg"  alt="" />Back</button>
                            </h3> */}
                            <h3><button onClick={()=>history.push('/account/dashboard')} className="btn back_btn"
                              style={{padding:"10px 10px",border:"1px solid #d0e1fd",height:"40px",fontSize:"12px"}}
                            ><img src="src/images/home_icon.svg" alt="" style={{marginRight:"2px",marginBottom:"6px",width: "15px",height: "15px"}}/></button>
                            </h3></div>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div className='row align-items-center'>
                        <div className='profile-info-content'>
                            <div className='row'>
                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12'>
                                    <div className='tab-content'>
                                        <div className='personal-information'>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input autoComplete="off" type="text"placeholder=' First Name*'  value={firstName}  onChange={(e)=>handleInputChange('firstName',e)} />
                                                            <p className={"error-msg"}>{error.firstName}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input autoComplete="off" type="text"placeholder=' Last Name*' value={lastName}  onChange={(e)=>handleInputChange('lastName',e)} />
                                                            <p className={"error-msg"}>{error.lastName}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input autoComplete="off" type="text"placeholder=' Email Address*' value={emailAddress}  onChange={(e)=>handleInputChange('emailAddress',e)} />
                                                            <p className={"error-msg"}>{error.emailAddress}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input autoComplete="off" type="number"placeholder=' Phone*' value={phone}  onChange={(e)=>handleInputChange('Phone',e)} />
                                                            <p className={"error-msg"}>{error.phone}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                        <select className="hlfinput" value={SubscriptionDuration}
												       // onChange={(e) => setSubscriptionDuration(e.target.value)}
                                                        onChange={(e)=>handleInputChange('SubscriptionDuration',e)}
                                                        >
                                                            <option >Choose... </option>
                                                            <option value={"Trial"}>Trial</option>
                                                            <option value={"Monthly"}>Monthly</option>
                                                            <option value={"Annual"}>Annual</option>
                                                        </select>
                                                            <label for="">Subscription Duration<span>*</span> </label>
                                                            <p className={"error-msg"}>{error.SubscriptionDuration}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                        <select  value={SubscriptionType}
												       // onChange={(e) => setSubscriptionType(e.target.value)}
                                                        disabled = {SubscriptionDuration === "Trial"?true:false}
                                                         style={{backgroundColor:SubscriptionDuration === "Trial"?"#e9ecef":""}}
                                                        onChange={(e)=>handleInputChange('SubscriptionType',e)}
                                                        >
                                                                <option>Choose...</option>
                                                                {subscriptiontypelist.map(item=>{
                                                                    return(
                                                                <option disabled={`${item.PackageCode === "ENTERPRISE"?"disabled":""}`} value={item.PackageCode}>{item.PackageName}</option>)
                                                                })}

                                                            </select>
                                                            <label for="">Subscription<span>*</span> </label>
                                                            <p className={"error-msg"}>{error.SubscriptionType}</p>
                                                              {(SubscriptionDuration === "Monthly" ||SubscriptionDuration === "Annual" || SubscriptionType ==="ENTERPRISE") && SubscriptionType !== ""? <p className='mt-2' style={{color:"rgb(4 202 28)",fontSize:15}}> Documents limit for signature is {" "}
                                                                {SubscriptionDuration === "Monthly" && SubscriptionType==="IDIVIDUALUSER"?10
                                                                :SubscriptionDuration === "Annual" && SubscriptionType==="IDIVIDUALUSER"?120
                                                                :SubscriptionDuration === "Monthly" && SubscriptionType==="COMPANY"?40
                                                                :SubscriptionDuration === "Annual" && SubscriptionType==="COMPANY"?480
                                                                :SubscriptionDuration === "Monthly" && SubscriptionType==="ENTERPRISE"?999
                                                                :SubscriptionDuration === "Annual" && SubscriptionType==="ENTERPRISE"?9999
                                                                :""
                                                                } 
                                                                </p>:""}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='row'>
                                               <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <input autoComplete="off" 
                                                             style={{backgroundColor:SubscriptionType==="IDIVIDUALUSER"?"#e9ecef":""}}
                                                             disabled={SubscriptionType==="IDIVIDUALUSER"?true:false}  type="text"placeholder=' Organization*'  value={Organization}  onChange={(e)=>handleInputChange('oraganization',e)} />
                                                            <p className={"error-msg"}>{error.Organization}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                                    <div className="form-group">
                                                        <div className="input-wrapper">
                                                            <select value={Role}
                                                            disabled={true}
                                                            style={{backgroundColor:"#e9ecef"}}
                                                            onChange={(e)=>handleInputChange('Role',e)}
												               // onChange={(e) => setRole(e.target.value)}
                                                                >
                                                                <option>Choose...</option>
                                                                {Roleslist.map(item=>{
                                                                    return(
                                                                <option disabled={`${item.RoleId === 3 ||item.RoleId === 4 ||item.RoleId === 5?"disabled":""}`} value={item.RoleCode}>{item.RoleName}</option>)
                                                                })}
                                                            </select>
                                                            <label for="">Role<span>*</span> </label>
                                                            <p className={"error-msg"}>{error.Role}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                    <div class="subf_pagn_btn">
                                                        <button class="btn"  onClick={()=>SaveRegister()}>Register<span><img src="src/images/right_arrow.svg" /></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { NewAccountCustomer }; 