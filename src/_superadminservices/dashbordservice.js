import {authService} from "../_services/auth.service";

export const dashboardService = {
    getUserStatistics:getUserStatistics,
    getMonthllyactiveusers:getMonthllyactiveusers,
    getDailyActiveusers:getDailyActiveusers,
    getSubscriptionSummary:getSubscriptionSummary,
    getDocumentsProcessedData:getDocumentsProcessedData,
}

function getUserStatistics() {
    return authService.get('dashboard/user/summary/report').then(res => {
        return res;
    });
}
function getMonthllyactiveusers(myear) {
    return authService.get(`dashboard/user/monthly/chart/${myear}`).then(res => {
        return res;
    });
}
function getDailyActiveusers(id) {
    return authService.get(`dashboard/user/daily/chart/${id}`).then(res => {
        return res;
    });
}
function getSubscriptionSummary() {
    return authService.get(`dashboard/package/summary`).then(res => {
        return res;
    });
}
function getDocumentsProcessedData(docyear) {
    return authService.get(`dashboard/documents/monthly/chart/${docyear}`).then(res => {
        return res;
    });
}
