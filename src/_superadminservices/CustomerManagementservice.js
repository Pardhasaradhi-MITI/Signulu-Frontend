import {authService} from "../_services/auth.service";

export const CustomermanagementService = {
    getSubscriptionType:getSubscriptionType,
    getRoles:getRoles,
    checkUserWithEmail:checkUserWithEmail,
    saveRegistration:saveRegistration,
    getalluserslist:getalluserslist,
    getcompanyuserslist:getcompanyuserslist,
    saveUser:saveUser,
    ActivateAndDeactiveateUser:ActivateAndDeactiveateUser,
    getuserdetails:getuserdetails,
    edituserdetails:edituserdetails,
    getPaymentdetails:getPaymentdetails,
}

function getSubscriptionType() {
    return authService.get('users/unauth/packages').then(res => {
        return res;
    });
}
function getRoles() {
    return authService.post(`users/company/roles`).then(res => {
        return res;
    });
}
function checkUserWithEmail(Email) {
    return authService.get(`registration/user/${Email}`).then(res => {
        return res;
    });
}
function saveRegistration(data) {
    return authService.post(`users/unauth/register`,data).then(res => {
        return res;
    });
}
function getalluserslist(data) {
    return authService.post(`users/all/list`,data).then(res => {
        return res;
    });
}
function getcompanyuserslist(data) {
    return authService.post(`users/company/list`,data).then(res => {
        return res;
    });
}
// function getCompanyStatistics(id) {
//     return authService.get(`users/bill/statistics/${id}`).then(res => {
//         return res;
//     });
// }
function saveUser(data) {
    return authService.post(`users/add/user`,data).then(res => {
        return res;
    });
}
function ActivateAndDeactiveateUser(Userid,Companyid,data){
    return authService.put(`users/master/update/${Userid}/${Companyid}`,data).then(res => {
        return res;
    });
}   
function getuserdetails(Userid){
    return authService.get(`user/${Userid}`).then(res => {
        return res;
    });
}
function edituserdetails(data){
    return authService.put(`users/master/update/${data.UserId}/${data.CompanyId}`,data)
}
function getPaymentdetails(data){
    return authService.get(`billing/payment/history/${data.CompanyId}/${data.UserId}`) 
}


