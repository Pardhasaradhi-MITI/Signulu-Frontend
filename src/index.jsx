import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { render } from 'react-dom';
import { history } from './_helpers';
import { accountService } from './_services';
import { App } from './app';
import "./index.css";
import './styles.less';
import './responsive.css';
import './newstyles.less';
import './newresponsive.css';
import { CookiesProvider } from "react-cookie";
import './i18nextConf';
import "primereact/resources/themes/lara-light-blue/theme.css";  //theme
import "primereact/resources/primereact.min.css";                  //core css
import "primeicons/primeicons.css";  
// setup fake backend
import PrimeReact from 'primereact/api';
PrimeReact.inputStyle = 'filled';
PrimeReact.ripple = true;
import { configureFakeBackend } from './_helpers';
import { Provider } from 'react-redux'
import Store from './redux/store'
import ReactTooltip from 'react-tooltip';

configureFakeBackend();

// attempt silent token refresh before startup
accountService.refreshToken().finally(startApp);

function startApp() { 
    render(
        <Router history={history}>
              <Provider store={Store}>
               <CookiesProvider>
            <App />
            <ReactTooltip backgroundColor={'#fff'} effect="solid" textColor={' #3A3A3A'} className="tooltip" html={true} multiline={true}/>
            </CookiesProvider>,
            </Provider>,
        </Router>,
        document.getElementById('app')
    );
}