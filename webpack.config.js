var HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
console.log(process.env.NODE_ENV, 'NodeDev')
module.exports = {
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            }, {
                test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.less$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'less-loader' }
                ]
            }
        ]
    },
    resolve: {
        mainFiles: ['index', 'Index'],
        extensions: ['.js', '.jsx'],
        alias: {
            '@': path.resolve(__dirname, 'src/'),
        }
    },
    plugins: [new HtmlWebpackPlugin({
        template: './src/index.html'
    }),
    new CopyPlugin
    ({ patterns: [{ from: 'src/images', to: 'src/images' }, { from: 'src/locales', to: 'src/locales' }, {from:'src/assets', to:'src/assets'}] })

    ],
    devServer: {
        historyApiFallback: true
    },
    externals: {

        // global app config object
        config: JSON.stringify(process.env.NODE_ENV === 'production' ? {
            faceRecApiUrl:"https://facialrecognition.signulu.com/FacialRecognitionAPIAZ/api/",
            serverUrl: "https://www.signulu.com/index1.php/api/",
            fileServerUrl:'https://www.signulu.com/index1.php/api/users/profilepic?fileName=',
            redirectserverUrl: "https://www.signulu.com",
          } :process.env.NODE_ENV === 'qa' ? {
            faceRecApiUrl: "https://slu-facial.azurewebsites.net/api/",
            serverUrl: "https://apiqa.signulu.com/index1.php/api/",
            redirectserverUrl: "https://apiqa.signulu.com",
            sandboxserverUrl: "https://sandbox.signulu.com/index1.php/api/",
            fileServerUrl:'https://apiqa.signulu.com/index1.php/api/users/profilepic?fileName='
        }: {
            faceRecApiUrl: "https://slu-facial.azurewebsites.net/api/",
            serverUrl: "https://apidev.signulu.com/index1.php/api/",
            redirectserverUrl: "https://apidev.signulu.com",
            sandboxserverUrl: "https://sandbox.signulu.com/index1.php/api/",
            fileServerUrl:'https://apidev.signulu.com/index1.php/api/users/profilepic?fileName=',
          })
    },
    // loaders: [{
    //     test: /.jsx?$/,
    //     loader: 'babel-loader',
    //     exclude: /node_modules/
    // }, {
    //     test: /\.css$/,
    //     loader: "style-loader!css-loader"
    // }, {
    //     test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
    //     loader: 'url-loader?limit=100000'
    // }]
}